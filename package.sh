#!/bin/bash

exit_code=$?

echo "downloading jetpack from artifactory"

wget -O jetpack http://artifactory.myntra.com/artifactory/packages/jetpack

if [ $exit_code -eq 0 ]; then

	/var/lib/jenkins/tools/hudson.tasks.Maven_MavenInstallation/Maven3.3.3/bin/mvn -Dmaven.repo.local=${WORKSPACE}/.repository -P SuiteRuns clean compile -DskipTests
	
	if [ $exit_code -eq 0 ]; then
		tar --exclude='.git' --exclude='jetpack' -zcvf "${REPO}-${BRANCH}.tgz" .
		chmod +x jetpack
		${WORKSPACE}/jetpack "${REPO}-${BRANCH}.tgz" "/"
		rm -rf "${REPO}-${BRANCH}.tgz"
	else
		echo "maven build is not successful"
	fi

else
	echo "downloading jetpack failed"

fi