FROM maven:3.5-jdk-8-alpine
MAINTAINER myntra

RUN /bin/sh -c "apk update && apk upgrade && apk add --no-cache bash bash-completion curl redis"

RUN mkdir -p /tf/
ADD start.sh /usr/bin/start.sh

ARG REPO
ARG BRANCH

ENV REPO=${REPO}
ENV BRANCH=${BRANCH}

RUN chmod +x /usr/bin/start.sh
RUN mkdir -p /root/.m2 /root/.gradle

CMD ["/bin/bash", "-e", "/usr/bin/start.sh"]
