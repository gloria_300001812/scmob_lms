package com.myntra.apiTests.erpservices.returnComplete.validators;


import com.myntra.lms.client.response.OrderTrackingResponse;
import com.myntra.lms.client.response.OrderTrackingResponseV2;
import com.myntra.lms.client.status.CourierCreationStatus;
import com.myntra.lms.client.status.DeliveryStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import io.qameta.allure.Step;
import org.testng.Assert;

/**
 * Created by Gloria.R on 24/07/18.
 */
public class OrderTrackingValidator {

    OrderTrackingResponseV2 orderTrackingResponseV2;
    OrderTrackingResponse orderTrackingResponse;

    public OrderTrackingValidator(OrderTrackingResponseV2 orderTrackingResponseV2) {
        this.orderTrackingResponseV2 = orderTrackingResponseV2;
        this.orderTrackingResponse = null;
    }

    public OrderTrackingValidator(OrderTrackingResponse orderTrackingResponse) {
        this.orderTrackingResponse = orderTrackingResponse;
        this.orderTrackingResponseV2 = null;
    }

    @Step("Validate the pickup id in Order tracking responsev2")
    public void validatePickupId(Long pickupId) {
        Assert.assertTrue(orderTrackingResponseV2.getOrderTrackingEntry().getPickupId().equals(pickupId), "The pickup id in OrderTracking response is not as expected");
    }

    @Step("Validate the order Tracking detail shipment status in responsev2")
    public void validateOrderDetailsStatus(ShipmentStatus orderTrackingStatus) {
        Assert.assertTrue(orderTrackingResponseV2.getOrderTrackingEntry().getShipmentStatus().name().equals(orderTrackingStatus.name()), "The order tracking status is not -" + orderTrackingStatus.name());
    }

    @Step("Validate OrderTracking deliveryStatus  status in OrderTrackingResponse ")
    public void validateOrderTrackingStatus(DeliveryStatus deliveryStatus) {
        Assert.assertTrue(orderTrackingResponse.getOrderTrackings().get(0).getDeliveryStatus().name().equals(deliveryStatus.name()), "The delivery status of orderTracking is not as expected - " + deliveryStatus.name());
    }


    @Step("Validate OrderTracking courierCreationStatus status in OrderTrackingResponse ")
    public void validateCourierCreationStatus(CourierCreationStatus courierCreationStatus) {
        Assert.assertTrue(orderTrackingResponse.getOrderTrackings().get(0).getCourierCreationStatus().name().equals(courierCreationStatus.name()), "The CourierCreationStatus status of orderTracking is not as expected - " + courierCreationStatus.name());
    }

    @Step("Validate OrderTracking trackingNumber in OrderTrackingResponse ")
    public void validateTrackingNumber(String trackingNumber) {
        Assert.assertTrue(orderTrackingResponse.getOrderTrackings().get(0).getTrackingNumber().equals(trackingNumber), "The CourierCreationStatus status of orderTracking is not as expected  ");
    }

    @Step("Validate ShipmentType  in OrderTrackingResponse ")
    public void validateOrderShipmentType(ShipmentType shipmentType) {
        Assert.assertTrue(orderTrackingResponse.getOrderTrackings().get(0).getShipmentType().name().equals(shipmentType.name()), "The CourierCreationStatus status of orderTracking is not as expected  ");
    }

    @Step("Validate Id  in OrderTrackingResponse ")
    public void validateId(String orderTrackingId) {
        Assert.assertTrue(orderTrackingResponse.getOrderTrackings().get(0).getId().equals(orderTrackingId), "The orderTrackingId  of orderTracking is not as expected  " + orderTrackingId);
    }
    @Step("Validating before manifestation")
    public void validateOrderDetailsB4Manifest(){
        Assert.assertEquals(orderTrackingResponseV2.getOrderTrackingEntry().getOrderTrackingDetails().size(),1,"The size of order tracking detail should be 1 before manifesting");
        Assert.assertEquals(orderTrackingResponseV2.getOrderTrackingEntry().getOrderTrackingDetails().get(0).getActivityType(),"PICKUP_CREATED","The activityType is not  PICKUP_CREATED before manifesting");
        Assert.assertEquals(orderTrackingResponseV2.getOrderTrackingEntry().getOrderTrackingDetails().get(0).getActivityResult(),"SUCCESS","The activityResult is not SUCCESS 1 before manifesting");
        Assert.assertEquals(orderTrackingResponseV2.getOrderTrackingEntry().getOrderTrackingDetails().get(0).getFromStatus(),"PICKUP_CREATED","The from-status is not PICKUP_CREATED before manifesting");
        Assert.assertEquals(orderTrackingResponseV2.getOrderTrackingEntry().getOrderTrackingDetails().get(0).getToStatus(),"PICKUP_CREATED","Th to-status is not PICKUP_CREATED before manifesting");
        Assert.assertEquals(orderTrackingResponseV2.getOrderTrackingEntry().getShipmentStatus().name(),ShipmentStatus.PICKUP_CREATED.name(),"The shipmentStatus is not PICKUP_CREATED before manifesting");
        Assert.assertEquals(orderTrackingResponseV2.getOrderTrackingEntry().getShipmentStatus().getShipmentStatusCode(),"RIT","The shipment Status Code is not RIT before manifesting");
        Assert.assertEquals(orderTrackingResponseV2.getOrderTrackingEntry().getShipmentStatus().getOrderTrackingStatusCode(),"RIT","The Order Tracking Status Code is not RIT before manifesting");

    }

}
