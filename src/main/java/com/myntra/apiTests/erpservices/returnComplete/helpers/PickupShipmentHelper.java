package com.myntra.apiTests.erpservices.returnComplete.helpers;

import com.myntra.apiTests.erpservices.returnComplete.client.OrderTrackingClient;
import com.myntra.apiTests.erpservices.returnComplete.client.PickupShipmentClient;
import com.myntra.apiTests.erpservices.returnComplete.client.RMSClient;
import com.myntra.apiTests.erpservices.returnComplete.client.ReturnShipmentClient;
import com.myntra.apiTests.erpservices.returnComplete.validators.OrderTrackingValidator;
import com.myntra.apiTests.erpservices.returnComplete.validators.PickupShipmentValidator;
import com.myntra.lms.client.domain.response.PickupResponse;
import com.myntra.lms.client.response.OrderTrackingResponseV2;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.returns.entry.ReturnEntry;
import com.myntra.returns.response.ReturnResponse;
import io.qameta.allure.Step;

/**
 * Created by Gloria.R on 24/07/18.
 */
public class PickupShipmentHelper {

    PickupShipmentClient pickupShipmentClient;
    String clientId;
    String tenantId;
    ReturnShipmentClient returnShipmentClient;
    RMSClient rmsClient;
    OrderTrackingClient orderTrackingClient;


    public PickupShipmentHelper(String URL, String clientId, String tenantId) {
        this.pickupShipmentClient = new PickupShipmentClient(URL, clientId, tenantId);
        this.returnShipmentClient = new ReturnShipmentClient(URL, clientId, tenantId);
        orderTrackingClient = new OrderTrackingClient(URL, clientId, tenantId);

        this.clientId = clientId;
        this.tenantId = tenantId;
        rmsClient = new RMSClient();

    }

}
