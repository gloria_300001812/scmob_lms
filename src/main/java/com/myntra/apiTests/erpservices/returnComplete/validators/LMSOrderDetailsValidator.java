package com.myntra.apiTests.erpservices.returnComplete.validators;

import com.myntra.lms.client.domain.shipment.PickupShipment;
import com.myntra.lms.client.domain.shipment.ReturnShipment;
import com.myntra.lms.client.response.OrderAdditionalInfoEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentItemEntry;
import com.myntra.lms.client.status.ReturnType;
import com.myntra.lms.client.status.ShipmentType;
import org.testng.Assert;

import java.util.List;

/**
 * Created by Gloria.R on 24/07/18.
 */
/*curl -X GET \
        'http://lms.scmqa.myntra.com/myntra-lms-service/platform/v3/order/orderId/2147613921767223?tenantId=4019&clientId=2297' \
        -H 'accept: application/json' \
        -H 'authorization: Basic ZGQ6ZnNlZnJn' \
        -H 'cache-control: no-cache' \
        -H 'content-type: application/json' \
        -H 'postman-token: 9d3ac04c-a37c-154e-238c-16131d7f3ede'*/

//TODO :Have divided the order response into different sections, since the response is too big and has multiple entities being returned. Write validation for every entity

public class LMSOrderDetailsValidator {
    OrderResponse lmsOrderResponse;
    PickupShipment exchangePickupDetails;
    ReturnShipment exchangeReturnDetails;
    List<ShipmentItemEntry> shipmentItemDetails;
    OrderAdditionalInfoEntry orderAdditionalDetails;

    //TODO : since the api logs wont be there in automation logs, put assertions in the construtor itself, so that it stops at the right place
    public LMSOrderDetailsValidator(OrderResponse lmsOrderResponse) {

        this.lmsOrderResponse = lmsOrderResponse;
        if (lmsOrderResponse.getOrders() == null || lmsOrderResponse.getOrders().isEmpty()) {
            Assert.fail("Could not find order in LMS");
        }
        try {
            exchangePickupDetails = lmsOrderResponse.getOrders().get(0).getExchangePickupShipment();
            exchangeReturnDetails = lmsOrderResponse.getOrders().get(0).getExchangePickupShipment().getReturnShipments().get(0);
        } catch (NullPointerException e) {
            System.out.println("Only in case of exchange these details will be populated, so ignore null pointer");
        }
        try {
            shipmentItemDetails = lmsOrderResponse.getOrders().get(0).getShipmentItemEntries();
        } catch (NullPointerException e) {
            Assert.fail("Shipment Item details not present for the order");

        }
        try {
            orderAdditionalDetails = lmsOrderResponse.getOrders().get(0).getOrderAdditionalInfo();
        } catch (NullPointerException e) {
            Assert.fail("Order Additional details not present for the order");

        }

    }

    public void validateIsTrynBuyOrder() {
        Assert.assertTrue(lmsOrderResponse.getOrders().get(0).getShipmentType().getName().equals(ShipmentType.TRY_AND_BUY.getName()), "The shipment - " + lmsOrderResponse.getOrders().get(0).getOrderId() + " is not a TRY_AND_BUY shipmentType");
        Assert.assertFalse(lmsOrderResponse.getOrders().get(0).getIsExchangeOrder());
        Assert.assertNull(lmsOrderResponse.getOrders().get(0).getExchangePickupShipment());

    }

    public void validateIsExchangeOrder() {
        Assert.assertTrue(lmsOrderResponse.getOrders().get(0).getShipmentType().getName().equals(ShipmentType.DL.getName()), "The shipment - " + lmsOrderResponse.getOrders().get(0).getOrderId() + " is not a DL shipmentType");
        Assert.assertTrue(lmsOrderResponse.getOrders().get(0).getIsExchangeOrder());
        Assert.assertNotNull(lmsOrderResponse.getOrders().get(0).getExchangePickupShipment());
        Assert.assertTrue(exchangeReturnDetails.getReturnType().name().equals(ReturnType.EXCHANGE_PICKUP.name()), "The return type of the exchange is not EXCHANGE_PICKUP");
    }

    public void validateOrderAdditionalInfo() {

    }

    public void validateExchangePickupDetails() {

    }

    public void validateExchangeReturnDetails() {

    }

    public void validateShipmentItemDetails() {

    }


}

