package com.myntra.apiTests.erpservices.returnComplete.validators;

import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import io.qameta.allure.Step;
import org.testng.Assert;

import static java.lang.Long.parseLong;


public class RMSValidators {
    ReturnResponse rmsReturnResponse;

    public RMSValidators(ReturnResponse rmsReturnResponse) {
        this.rmsReturnResponse = rmsReturnResponse;
    }

    @Step("Validate return created in RMS is in the correct returnMode")
    public void validateReturnMode(ReturnMode returnMode) {
        Assert.assertTrue(rmsReturnResponse.getData().get(0).getReturnMode().name().equals(returnMode.name()), "The return is not an OPEN_BOX_PICKUP type");
    }

    @Step("Validate return created in RMS")
    public void validateReturnCreatedRMS() {
        Assert.assertNotNull(rmsReturnResponse.getData().get(0).getReturnLineEntries(), "The return is not created in RMS");
    }

    @Step("Validate client and tenant in RMS")
    public void validateClientTenant(String storePartnerId, String logisticsPartnerId) {
        // Assert.assertTrue(rmsReturnResponse.getData().get(0).getStorePartnerId(storePartnerId);
    }

    @Step("Validate courierCode")
    public void validateCourierCode(String courierCode) {

    }

    public void validateReturnStatus(ReturnStatus returnStatus) {
        Assert.assertTrue(rmsReturnResponse.getData().get(0).getStatusCode().name().equals(returnStatus.name()), "The return status in RMS is not " + returnStatus.name());
    }
}
