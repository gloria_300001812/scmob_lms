package com.myntra.apiTests.erpservices.returnComplete.validators;

import com.myntra.lms.client.domain.response.ReturnResponse;
import com.myntra.lms.client.domain.shipment.ReturnShipment;
import com.myntra.lms.client.status.ReturnType;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import org.testng.Assert;
import io.qameta.allure.*;

/**
 * Created by Gloria.R on 24/07/18.
 */
/*curl -X GET \
        'http://lms.scmqa.myntra.com/myntra-lms-service/platform/v3/return/sourceReturnId/7969?sourceId=2297&tenantId=4019' \
        -H 'authorization: Basic ZGQ6ZnNlZnJn' \
        -H 'cache-control: no-cache' \
        -H 'postman-token: bbe9a960-824d-2448-b46d-db50f2ed4b38'*/

//The reponse of the above api has ReturnResponse which internally has Return Shipment response
public class ReturnShipmentValidator {
    ReturnResponse returnResponse;
    ReturnShipment returnShipment;

    public ReturnShipmentValidator(ReturnResponse returnShipmentResponse) {
        this.returnResponse = returnShipmentResponse;
        try {
            returnShipment = returnResponse.getDomainReturnShipments().get(0);

        } catch (NullPointerException e) {
            Assert.fail("The return is not created in LMS ");
        }
    }

    @Step("Validate Return is created in LMS - return_shipment table")
    public void validateReturnCreatedInLMS() {
        if (returnShipment.getOrderId() == null || returnShipment.getOrderId().isEmpty()) {
            Assert.fail("Return is not created in LMS");
        }

    }

    @Step("Validate courier in LMS : return_shipment")
    public void validateCourierOfReturn(String courier) {
        Assert.assertTrue(returnShipment.getCourierCode().equals(courier), "Courier assigned to the return is not as expected - " + courier);
    }

    @Step("Validate returnMOde in LMS : return_shipment")
    public void validateReturnModeLMS(ReturnType returnType) {
        Assert.assertTrue(returnShipment.getReturnType().equals(returnType), "Return Type of the return is not as expected in LMS - " + returnType.name());
    }
    @Step("Validate trakcing number in LMS : return_shipment")
    public void validateReturnTrackingLMS(String trackingNumber) {
        Assert.assertTrue(returnShipment.getTrackingNumber().equals(trackingNumber), "TrackingNumber of the return is not as expected in LMS - " + trackingNumber);
    }


    @Step("Validate returnShipment status in LMS ")
    public void validateReturnShipmentStatus(ShipmentStatus returnStatus,com.myntra.lms.client.status.OrderStatus status) {
        Assert.assertTrue(returnShipment.getShipmentStatus().name().equals(returnStatus.name()), "Return Status of the return is not as expected in LMS - " + returnStatus.name());
        Assert.assertTrue(returnShipment.getStatus().toString().equals(status.toString()), "Return Status of the return is not as expected in LMS - " + status.name());
    }

    public void validateClientTenantSourceIntegration(String clientId, String tenantId) {
        String sourcePath = (clientId == "2297" ? "MYNTRA" : "JABONG");
        Assert.assertTrue(returnShipment.getClientId().equals(clientId), "Client Id of  the return is not as expected - " + clientId);
        Assert.assertTrue(returnShipment.getIntegrationId().equals(clientId), "Integration Id of the return is not same as the clientId - " + clientId);
        Assert.assertTrue(returnShipment.getSourcePath().name().equals(sourcePath), " Source Path the return is not as expected - " + sourcePath);
        Assert.assertTrue(returnShipment.getTenantId().equals(tenantId), "Tenant id of the return is not as expected - " + tenantId);
        Assert.assertTrue(returnShipment.getSourceId().equals(clientId), "Source id of the return is not as same as the client Id expected - " + clientId);
    }

    @Step("Validate parent Order Id(PacketId) ")
    public void validateParentOrderIdOfReturn(String orderId) {
        Assert.assertTrue(returnShipment.getOrderId().equals(orderId), "The parent orderId (packetId) is incorrect");
    }

    @Step("Validate return  Id of RMS is sourceReferenceId of return_shipment ")
    public void validateReturnId(String returnId) {
        Assert.assertTrue(returnShipment.getSourceReferenceId().equals(returnId), " Return  Id of RMS is sourceReferenceId of return_shipment");
    }

    @Step("Validate customer details of the return in LMS")
    public void validateCustomerDetails(String firstName, String address, String city, String state, String country, String zipcode, String mobile, String email, String addressId) {
        Assert.assertTrue(returnShipment.getRecipientName().equals(firstName), "The First name is not as expected");
        Assert.assertTrue(returnShipment.getRecipientAddress().equals(address), "The Address is not as expected");
        Assert.assertTrue(returnShipment.getCity().equals(city), "The city is not as expected");
        Assert.assertTrue(state.toLowerCase().contains(returnShipment.getStateCode().toLowerCase()), "The state  code in returnShipment is not as expected");
        Assert.assertTrue(returnShipment.getCountry().equals(country), "The coutnry is not as expected");
        Assert.assertTrue(returnShipment.getPincode().equals(zipcode), "The zipcode is not as expected");
        Assert.assertTrue(returnShipment.getRecipientContactNumber().equals(mobile), "The  mobile is not as expected");
        Assert.assertTrue(returnShipment.getEmail().equals(email), "The email is not as expected");
        Assert.assertTrue(returnShipment.getAddressId().equals(addressId), "The addressId is not as expected");

    }

    @Step("Validate return warehouse id and the return hub code of the return in LMS")
    public void validateWarehouseOfReturn(String warehouseId, String hubCode) {
        Assert.assertTrue(returnShipment.getReturnFacilityId().equals(warehouseId), "The return warehouseId is not as expected in LMS return shipment");
        Assert.assertTrue(returnShipment.getReturnHubCode().equals(hubCode), "The return hubCode is not as expected in LMS return-shipment");
    }

    @Step("Validate delivery  center of the return in LMS")
    public void validateDeliveryCentre(String deliveryCentre) {
        Assert.assertTrue(returnShipment.getDeliveryCenterId().toString().equals(deliveryCentre), "The delivery centre is not as expected");
    }

    @Step("Validate sourceReturnId of the return in LMS")
    public void validatesourceReturnId(String sourceReturnId) {
        Assert.assertTrue(returnShipment.getShipmentValue().equals(sourceReturnId), "The sourceReturnId is not as expected");
    }

    @Step("Validate orderLine details stamped correctly in ReturnItem details")
    public void validateLineDetails(String sku, String styleId, String itemValue) {
        if (returnResponse.getDomainReturnShipments().get(0).getReturnItems() == null || returnResponse.getDomainReturnShipments().get(0).getReturnItems().isEmpty()) {
            Assert.fail("Return items not present in the return");
        }
        Assert.assertTrue(returnResponse.getDomainReturnShipments().get(0).getReturnItems().get(0).getItemValue().toString().equals(itemValue), "The item value from Order Line does not match item Value of Return Item entries");
        Assert.assertTrue(returnResponse.getDomainReturnShipments().get(0).getReturnItems().get(0).getSkuId().equals(sku), "The skuId from Order Line does not match skuId of Return Item entries");
        Assert.assertTrue(returnResponse.getDomainReturnShipments().get(0).getReturnItems().get(0).getProductStyleId().equals(styleId), "The styleId from Order Line does not match styleId of Return Item entries");
        Assert.assertTrue(returnResponse.getDomainReturnShipments().get(0).getReturnItems().get(0).getProductStyleId().equals(styleId), "The styleId from Order Line does not match styleId of Return Item entries");

    }

//TODO : catalog details and seller details verification

}
