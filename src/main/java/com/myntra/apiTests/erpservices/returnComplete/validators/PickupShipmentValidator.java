package com.myntra.apiTests.erpservices.returnComplete.validators;

import com.myntra.lms.client.domain.response.PickupResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import io.qameta.allure.Step;
import org.testng.Assert;

/**
 * Created by Gloria.R on 24/07/18.
 */
public class PickupShipmentValidator {
    PickupResponse pickupResponse;

    public PickupShipmentValidator(PickupResponse pickupResponse) {
        this.pickupResponse = pickupResponse;
    }

    @Step("Validate the pickupShipment status")
    public void validatePickupStatus(ShipmentStatus pickupStatus) {
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().name().equals(pickupStatus.name()), "The pickup is not created in " + pickupStatus.name() + " status in LMS");
    }

    @Step("Validate the return Details  inside pickupShipment response")
    public void validateReturnId(String returnId, String trackingNumber) {
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getReturnShipments().get(0).getSourceReferenceId().equals(returnId), "The returnId in pickupShipment does not match");
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getReturnShipments().get(0).getTrackingNumber().equals(trackingNumber), "The tracking number of the returnDetails in pickupShipment does not match");
    }

    @Step("Validate the pickup isManifested=false")
    public void validatePickupNotManifested(){
        Assert.assertFalse(pickupResponse.getDomainPickupShipments().get(0).getManifested(),"The pickup is manifested in LMS");
    }

    @Step("Validate the pickup isManifested=true")
    public void validatePickupManifested(){
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getManifested(),"The pickup is NOT manifested in LMS");
    }

    @Step("Validate the pickup shipment Type")
    public void validatePickupShipmentType(ShipmentType shipmentType){
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getShipmentType().name().equals(shipmentType.name()),"The pickup shipment Type is not matching");
    }

    @Step("Validate customer details of the return in LMS")
    public void validateCustomerDetails( String address, String city, String state, String country, String zipcode, String mobile, String email, String addressId) {
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getRecipientAddress().equals(address), "The Address in pickup shipmentis not as expected");
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getCity().equals(city), "The city in pickup shipment is not as expected");
        Assert.assertTrue(state.toLowerCase().contains(pickupResponse.getDomainPickupShipments().get(0).getStateCode().toLowerCase()), "The state  code in pickup shipment is not as expected");
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getCountry().equals(country), "The coutnry in pickup shipmentis not as expected");
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getPincode().equals(zipcode), "The zipcode in pickup shipment is not as expected");
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getRecipientContactNumber().equals(mobile), "The  mobile  in pickup shipment is not as expected");
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getEmail().equals(email), "The email in pickup shipment is not as expected");
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getAddressId().equals(addressId), "The addressId in pickup shipment is not as expected");

    }

    public void validateClientTenantSourceIntegration(String clientId, String tenantId) {
        String sourcePath = (clientId == "2297" ? "MYNTRA" : "JABONG");
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getClientId().equals(clientId), "Client Id of  the pickup is not as expected - " + clientId);
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getIntegrationId().equals(clientId), "Integration Id of the pickup is not same as the clientId - " + clientId);
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getSourcePath().name().equals(sourcePath), " Source Path the pickup is not as expected - " + sourcePath);
//TODO : Uncomment - bug
        //  Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getTenantId().equals(tenantId), "Tenant id of the pickup is not as expected - " + tenantId);
        Assert.assertTrue(pickupResponse.getDomainPickupShipments().get(0).getSourceId().equals(clientId), "Source id of the pickup is not as same as the client Id expected - " + clientId);
    }
}
