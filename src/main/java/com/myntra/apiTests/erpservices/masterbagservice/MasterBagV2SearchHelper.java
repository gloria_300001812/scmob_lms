package com.myntra.apiTests.erpservices.masterbagservice;

import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.logistics.masterbag.entry.MasterbagEntry;
import com.myntra.logistics.masterbag.response.MasterbagResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.testng.Assert;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MasterBagV2SearchHelper {

    /**
     * Build the Api query param
     *
     * @param searchParam
     * @return
     */
    public String buildQueryParam(String... searchParam) {
        String finalQueryParam = "?q=";
        for (int i = 0; i <= searchParam.length - 1; i++) {


            if (searchParam.length == 1) {
                finalQueryParam = finalQueryParam + searchParam[i];
            } else if (i == searchParam.length - 1) {
                finalQueryParam = finalQueryParam + searchParam[i];
            } else {
                finalQueryParam = finalQueryParam + searchParam[i] + "___";
            }
        }
        return finalQueryParam;
    }

    /**
     * Get data from db using giving query
     *
     * @param query
     * @return
     */
    public static List<Map<String, Object>> getDbData(String query) {
        List<Map<String, Object>> mapList = DBUtilities.exSelectQuery(query, "myntra_lms");
        return mapList;
    }

    /**
     * Compare Api data and DB data
     *
     * @param masterbagResponse
     * @param dbValues
     */
    public void validateDBandAPIData(MasterbagResponse masterbagResponse, List<Map<String, Object>> dbValues) throws ParseException {
        List<MasterbagEntry> masterbagEntries = masterbagResponse.getMasterbagEntries();
        Assert.assertEquals(masterbagResponse.getStatus().getTotalCount(), dbValues.size(),
                MessageFormat.format("total count from api data and DB data is not matching,api data is {0} and DB data is {1}",
                        String.valueOf(masterbagResponse.getStatus().getTotalCount()), String.valueOf(dbValues.size())));
        for (int i = 0; i < masterbagEntries.size(); i++) {
            if (i >= 20) {
                break;
            }
            System.out.println("Iteration is - "+i);
            Assert.assertEquals(masterbagEntries.get(i).getId().toString(), dbValues.get(i).get("id").toString(),
                    MessageFormat.format("MasterBag id is not matching, found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getId().toString(), dbValues.get(i).get("id").toString(), i));
            Assert.assertEquals(masterbagEntries.get(i).getOriginPremisesId().toString(), dbValues.get(i).get("origin_premises_id").toString(),
                    MessageFormat.format("origin_premises_id is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getOriginPremisesId().toString(), dbValues.get(i).get("origin_premises_id").toString(), i));
            Assert.assertEquals(masterbagEntries.get(i).getDestinationPremisesId().toString(), dbValues.get(i).get("dest_premises_id").toString(),
                    MessageFormat.format("destination premises id is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getDestinationPremisesId().toString(), dbValues.get(i).get("dest_premises_id").toString(), i));
            Assert.assertEquals(masterbagEntries.get(i).getOriginPremisesType().toString(), dbValues.get(i).get("origin_premises_type").toString(),
                    MessageFormat.format("origin_premises_type is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getOriginPremisesType().toString(), dbValues.get(i).get("origin_premises_type").toString(), i));
            Assert.assertEquals(masterbagEntries.get(i).getDestinationPremisesType().toString(), dbValues.get(i).get("dest_premises_type").toString(),
                    MessageFormat.format("destination_premises_type is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getDestinationPremisesType().toString(), dbValues.get(i).get("dest_premises_type").toString(), i));
            Assert.assertEquals(masterbagEntries.get(i).getStatus().toString(), dbValues.get(i).get("status").toString(),
                    MessageFormat.format("master bag status is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getStatus().toString(), dbValues.get(i).get("status").toString(), i));
            long epochTime = convertDateToEpochTime(String.valueOf(dbValues.get(i).get("last_modified_on")), "yyyy-MM-dd HH:mm:ss");
            Assert.assertEquals(convertDateToEpochTime(String.valueOf(masterbagEntries.get(i).getLastModifiedOn()), "E MMM dd HH:mm:ss Z yyyy"),
                    epochTime, MessageFormat.format("last modified on date is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getLastModifiedOn().toString(), dbValues.get(i).get("last_modified_on").toString(), i));

        }
    }

    /**
     * Compare Api data and DB data without Shipped on date validation
     *
     * @param masterbagResponse
     * @param dbValues
     */
    public void validate_DBandAPIData_Without_ShippedOnDate(MasterbagResponse masterbagResponse, List<Map<String, Object>> dbValues) throws ParseException {
        List<MasterbagEntry> masterbagEntries = masterbagResponse.getMasterbagEntries();
        Assert.assertEquals(masterbagResponse.getStatus().getTotalCount(), dbValues.size(), "total count from api data and DB data is not matching ");
        for (int i = 0; i < masterbagEntries.size(); i++) {
            if (i >= 20) {
                break;
            }
            Assert.assertEquals(masterbagEntries.get(i).getId().toString(), dbValues.get(i).get("id").toString(),
                    MessageFormat.format("MasterBag id is not matching, found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getId().toString(), dbValues.get(i).get("id").toString(), i));
            Assert.assertEquals(masterbagEntries.get(i).getOriginPremisesId().toString(), dbValues.get(i).get("origin_premises_id").toString(),
                    MessageFormat.format("origin_premises_id is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getOriginPremisesId().toString(), dbValues.get(i).get("origin_premises_id").toString(), i));
            Assert.assertEquals(masterbagEntries.get(i).getDestinationPremisesId().toString(), dbValues.get(i).get("dest_premises_id").toString(),
                    MessageFormat.format("destination premises id is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getDestinationPremisesId().toString(), dbValues.get(i).get("dest_premises_id").toString(), i));
            Assert.assertEquals(masterbagEntries.get(i).getOriginPremisesType().toString(), dbValues.get(i).get("origin_premises_type").toString(),
                    MessageFormat.format("origin_premises_type is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getOriginPremisesType().toString(), dbValues.get(i).get("origin_premises_type").toString(), i));
            Assert.assertEquals(masterbagEntries.get(i).getDestinationPremisesType().toString(), dbValues.get(i).get("dest_premises_type").toString(),
                    MessageFormat.format("destination_premises_type is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getDestinationPremisesType().toString(), dbValues.get(i).get("dest_premises_type").toString(), i));
            Assert.assertEquals(masterbagEntries.get(i).getStatus().toString(), dbValues.get(i).get("status").toString(),
                    MessageFormat.format("master bag status is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getStatus().toString(), dbValues.get(i).get("status").toString(), i));
            long epochTime = convertDateToEpochTime(String.valueOf(dbValues.get(i).get("last_modified_on")), MasterBagConstants.datePattern2);
            Assert.assertEquals(convertDateToEpochTime(String.valueOf(masterbagEntries.get(i).getLastModifiedOn()), MasterBagConstants.datePattern1),
                    epochTime, MessageFormat.format("last modified on date is not matching found api value is- {0} and DB value is - {1} in- {2}-th iteration",
                            masterbagEntries.get(i).getLastModifiedOn().toString(), dbValues.get(i).get("last_modified_on").toString(), i));
        }
    }

    /**
     * Check the given date is in range
     *
     * @param fromdate
     * @param toDate
     * @param resultDate
     * @throws ParseException
     */
    public boolean checkDateInRange(String fromdate, String toDate, String resultDate,String pattern) throws ParseException {
        Boolean bFlag = false;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date1 = simpleDateFormat.parse(fromdate);
        Date date2 = simpleDateFormat.parse(toDate);
        Date date3 = simpleDateFormat.parse(resultDate);
        if (date3.after(date1) && date3.before(date2)) {
            bFlag = true;
        }
        return bFlag;
    }

    /**
     * This method is used to convert date value as Epoch time Value : pattern : yyyy-MM-dd HH:mm:ss and E MMM dd HH:mm:ss Z yyyy
     *
     * @param inputDate
     * @return
     * @throws ParseException
     */
    public long convertDateToEpochTime(String inputDate, String pattern) throws ParseException {
        if(inputDate=="null"){
            Assert.fail("input date value found as Null");
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        long epoch = simpleDateFormat.parse(inputDate).getTime();
        // System.out.println("--" + epoch);
        return epoch;
    }

    /**
     * It will verify the given dates are coming in range
     *
     * @param fromDate
     * @param toDate
     * @param masterbagResponse
     * @throws ParseException
     */
    public void validateGivenDateInRange(String fromDate, String toDate, MasterbagResponse masterbagResponse) throws ParseException {
        List<MasterbagEntry> masterbagEntries = masterbagResponse.getMasterbagEntries();
        Assert.assertFalse(masterbagEntries.isEmpty(), "Given master bag response doesn't have data");
        for (int i = 0; i < masterbagEntries.size(); i++) {
            if (i >= 20) {
                break;
            }
            Assert.assertTrue(checkDateInRange(fromDate, toDate, DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern1,
                    MasterBagConstants.datePattern2, masterbagEntries.get(i).getLastModifiedOn().toString()),MasterBagConstants.datePattern2), "The Date " +
                    masterbagEntries.get(i).getLastModifiedOn().toString() + " - not in givin date range");
        }
    }
}
