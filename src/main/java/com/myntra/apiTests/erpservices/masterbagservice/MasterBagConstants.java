package com.myntra.apiTests.erpservices.masterbagservice;

public class MasterBagConstants {

    public static final String originHubCode="DH-BLR";
    public static final String destHubCode = "ELC";
    public static final String originHubType = "HUB";
    public static final String destHubType = "DC";
    public static final String originHubId = "18";
    public static final String destHubId = "5";
    public static final String datePattern1="E MMM dd HH:mm:ss Z yyyy";
    public static final String datePattern2="yyyy-MM-dd HH:mm:ss.S";
    public static final String datePattern3="yyyy-MM-dd";
}
