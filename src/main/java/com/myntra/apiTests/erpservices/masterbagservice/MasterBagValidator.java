package com.myntra.apiTests.erpservices.masterbagservice;

import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClientV2_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.core.MasterbagType;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.entry.MasterbagEntry;
import com.myntra.logistics.masterbag.entry.MasterbagShipmentAssociationEntry;
import com.myntra.logistics.masterbag.response.MasterbagResponse;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponseCode;
import com.myntra.tms.domain.TMSMasterbagStatus;
import com.myntra.tms.statemachine.masterbag.MasterbagUpdateEvent;
import com.myntra.tms.track.TmsTrackingDetailResponse;
import com.myntra.tms.track.TmsTrackingEntry;
import org.testng.Assert;

import java.util.Iterator;
import java.util.List;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class MasterBagValidator {
    MasterBagServiceHelper masterBagServiceHelper;
    LmsServiceHelper lmsServiceHelper;
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();

    public MasterBagValidator() {
        masterBagServiceHelper = new MasterBagServiceHelper();
        lmsServiceHelper = new LmsServiceHelper();
    }

    public void validateMasterBagStatus(Long masterBagId, MasterbagStatus expectedStatus) throws InterruptedException {
        Thread.sleep(5000);
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertEquals(masterbagResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getStatus().toString(), expectedStatus.toString(), "Invalid MasterBag status");
    }

    public void validateCreateMasterBag(MasterbagDomain masterbagDomain, String originHubCode, String destinationHubCode, ShippingMethod shippingMethod, String courierCode, String tenantId) {
        Assert.assertEquals(originHubCode, masterbagDomain.getOriginHubCode(), "Invalid Hub code found in create MB Response!");
        Assert.assertEquals(destinationHubCode, masterbagDomain.getDestinationHubCode(), "Invalid destinationHubCode found in create MB Response!");
        Assert.assertEquals(shippingMethod, masterbagDomain.getShippingMethod(), "Invalid shippingMethod found in create MB Response!");
        Assert.assertEquals(courierCode, masterbagDomain.getCourierCode(), "Invalid courierCode found in create MB Response!");
        Assert.assertEquals(tenantId, masterbagDomain.getTenantId(), "Invalid tenantId found in create MB Response!");
        Assert.assertNotNull(masterbagDomain.getId(), "MasterBag Id is NULL on creation!");
        Assert.assertEquals(masterbagDomain.getMasterbagStatus().toString(), MasterbagStatus.NEW.toString());
        Assert.assertNull(masterbagDomain.getShipmentList());
        //validate Shipment table
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterbagDomain.getId());
        Assert.assertEquals(masterbagResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getStatus().toString(), MasterbagStatus.NEW.toString());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getId(), masterbagDomain.getId());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getOriginPremisesCode(), masterbagDomain.getOriginHubCode(), "Invalid Hub code found in create MB Response!");
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getDestinationPremisesCode(), masterbagDomain.getDestinationHubCode(), "Invalid destinationHubCode found in create MB Response!");
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getShippingMethod(), masterbagDomain.getShippingMethod(), "Invalid shippingMethod found in create MB Response!");
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getCourier(), masterbagDomain.getCourierCode(), "Invalid courierCode found in create MB Response!");
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getTenantId(), masterbagDomain.getTenantId(), "Invalid tenantId found in create MB Response!");
    }

    public void validateCreateMasterBag(MasterbagDomain mbCreateResponse, String originHubCode, String destinationHubCode, ShippingMethod shippingMethod, String courierCode, String tenantId, Integer capacity) {
        validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        if (capacity > 0)
            Assert.assertTrue(mbCreateResponse.getCapacity() == capacity, "Invalid capacity");
        else
            Assert.assertTrue(mbCreateResponse.getCapacity() == 300, "Invalid capacity");
    }

    public void validateCreateMasterBag(MasterbagDomain createMasterbagPayload, MasterbagDomain mbCreateResponse) {
        Assert.assertEquals(mbCreateResponse.getOriginHubCode(), createMasterbagPayload.getOriginHubCode(), "Invalid Hub code found in create MB Response!");
        Assert.assertEquals(mbCreateResponse.getDestinationHubCode(), createMasterbagPayload.getDestinationHubCode(), "Invalid destinationHubCode found in create MB Response!");
        Assert.assertEquals(mbCreateResponse.getShippingMethod(), createMasterbagPayload.getShippingMethod(), "Invalid shippingMethod found in create MB Response!");
        Assert.assertEquals(mbCreateResponse.getCourierCode(), createMasterbagPayload.getCourierCode(), "Invalid courierCode found in create MB Response!");
        Assert.assertEquals(mbCreateResponse.getTenantId(), createMasterbagPayload.getTenantId(), "Invalid tenantId found in create MB Response!");
        Assert.assertEquals(mbCreateResponse.getMasterbagStatus().toString(), MasterbagStatus.NEW.toString());
        Assert.assertNotNull(mbCreateResponse.getId(), "MasterBag Id is NULL on creation!");
        Assert.assertNull(mbCreateResponse.getShipmentList());
        Assert.assertEquals((createMasterbagPayload.getCapacity() == null ? "300" : createMasterbagPayload.getCapacity()).toString(), mbCreateResponse.getCapacity().toString());
    }

    public void validateMasterBagDetails(Long masterBagId, String originHubCode, String destinationHubCode, ShippingMethod shippingMethod, String courierCode, String tenantId) {
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        MasterbagEntry masterbagEntry = masterbagResponse.getMasterbagEntries().get(0);
        Assert.assertEquals(originHubCode, masterbagEntry.getOriginPremisesCode(), "Invalid Hub code found in create MB Response!");
        Assert.assertEquals(destinationHubCode, masterbagEntry.getDestinationPremisesCode(), "Invalid destinationHubCode found in create MB Response!");
        Assert.assertEquals(shippingMethod.toString(), masterbagEntry.getShippingMethod().toString(), "Invalid shippingMethod found in create MB Response!");
        Assert.assertEquals(courierCode, masterbagEntry.getCourier(), "Invalid courierCode found in create MB Response!");
        Assert.assertEquals(tenantId, masterbagEntry.getTenantId(), "Invalid tenantId found in create MB Response!");
        Assert.assertNotNull(masterbagEntry.getId(), "MasterBag Id is NULL on creation!");
    }

    public void validateCloseMasterBag(MasterbagUpdateResponse closeMBResponse, Long masterBagId) {
        Assert.assertEquals(closeMBResponse.getStatus().getStatusType().toString(), "SUCCESS");
        Assert.assertEquals(closeMBResponse.getMasterbagId(), masterBagId);
        //validate Shipment table
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getStatus().toString(), MasterbagStatus.CLOSED.toString());
    }

    public void validateCloseMasterBag(MasterbagUpdateResponse closeMBResponse, Long masterBagId, String trackingNumber) {
        Assert.assertEquals(closeMBResponse.getStatus().getStatusType().toString(), "SUCCESS");
        Assert.assertEquals(closeMBResponse.getMasterbagId(), masterBagId);
        //validate Shipment table
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getStatus().toString(), MasterbagStatus.CLOSED.toString());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries().get(0).getTrackingNumber(), trackingNumber);
    }

    public void validateCloseEmptyMasterBag(MasterbagUpdateResponse masterbagUpdateResponse) {
        Assert.assertEquals(masterbagUpdateResponse.getStatus().getStatusType().toString(),"ERROR");
        Assert.assertEquals(masterbagUpdateResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());
        Assert.assertEquals(masterbagUpdateResponse.getErrorMessage(), "Operation cannot be performed on an empty masterbag");
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterbagUpdateResponse.getMasterbagId());
        Assert.assertEquals(masterbagResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getStatus().toString(), MasterbagStatus.NEW.toString());
    }

    public void validateAddSameSipmentToMasterBag(MasterbagUpdateResponse masterbagUpdateResponse) {
        Assert.assertEquals(masterbagUpdateResponse.getStatus().getStatusType().toString(),"ERROR");
        Assert.assertEquals(masterbagUpdateResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());
        Assert.assertTrue(masterbagUpdateResponse.getErrorMessage().contains("is already present in Masterbag"));
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusMessage().contains("event SHIPMENTS_ADDED failed"));
        //validate mlShipment table
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterbagUpdateResponse.getMasterbagId());
        Assert.assertTrue(masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries().size() == 1, "");
    }


    public void validateAddExcessShipmentToMasterBag(MasterbagUpdateResponse masterbagUpdateResponse, Long masterBagId, List<String> trackingNumbers, Integer masterBagCapacity) {
        Assert.assertEquals(masterbagUpdateResponse.getStatus().getStatusType().toString(),"ERROR");
        Assert.assertEquals(masterbagUpdateResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());
        Assert.assertEquals(masterbagUpdateResponse.getErrorMessage(), "Can not add shipment to masterbag as capacity exceeded.");
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusMessage().contains("event SHIPMENTS_ADDED failed"));
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertTrue(masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries().size() == masterBagCapacity, "Invalid number of shipments in masterbag");
        List<MasterbagShipmentAssociationEntry> masterbagShipmentAssociationEntries = masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries();
        boolean foundTrackingNumber = false;
        if (trackingNumbers.size() == masterbagShipmentAssociationEntries.size()) {
            for (MasterbagShipmentAssociationEntry masterbagShipmentAssociationEntry : masterbagShipmentAssociationEntries) {
                foundTrackingNumber = trackingNumbers.contains(masterbagShipmentAssociationEntry.getTrackingNumber());
                if (foundTrackingNumber)
                    break;
            }
            Assert.assertTrue(foundTrackingNumber, String.format("trackingnumbers[%s] not found in masterbag[%s]", trackingNumbers, masterBagId));
        } else {
            Assert.fail(String.format("Number of shipments found in masterbag[%s] is invalid", masterBagId));
        }
    }

    public void validateAddShipmentToMasterBag(MasterbagUpdateResponse addShipmentToMbResponse, Long masterBagId, String trackingNumber) throws Exception {
        MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, LMS_CONSTANTS.TENANTID);
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), "SUCCESS", "Add shipment to MasterBag Failed");
        Assert.assertEquals(addShipmentToMbResponse.getMasterbagId(), masterBagId);
        if (mlShipmentResponse.getMlShipmentEntries() != null) {
            Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getTrackingNumber(), trackingNumber);
            if (masterbagResponse.getMasterbagEntries().get(0).getType().toString().equalsIgnoreCase(MasterbagType.DC_TO_DC.toString()))
                Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), MLDeliveryShipmentStatus.ASSIGNED_TO_OTHER_DC.toString());
            else
                Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        }
        //validate shipment table, order to ship
        Assert.assertEquals(masterbagResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getStatus().toString(), MasterbagStatus.NEW.toString());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getId(), masterBagId);
        boolean foundTrackingNumber = false;
        for (MasterbagShipmentAssociationEntry masterbagShipmentAssociationEntry : masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries()) {
            foundTrackingNumber = masterbagShipmentAssociationEntry.getTrackingNumber().equalsIgnoreCase(trackingNumber);
            if (foundTrackingNumber)
                break;
        }
        Assert.assertTrue(foundTrackingNumber, String.format("trackingnumber[%s] not found in masterbag[%s]", trackingNumber, masterBagId));
    }

    public void validateMultipleShipmentsAddedToMasterBag(Long masterBagId, List<String> trackingNumbers) {
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertEquals(masterbagResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getStatus().toString(), MasterbagStatus.NEW.toString());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getId(), masterBagId);
        List<MasterbagShipmentAssociationEntry> masterbagShipmentAssociationEntries = masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries();
        boolean foundTrackingNumber = false;
        if (trackingNumbers.size() == masterbagShipmentAssociationEntries.size()) {
            for (MasterbagShipmentAssociationEntry masterbagShipmentAssociationEntry : masterbagShipmentAssociationEntries) {
                foundTrackingNumber = trackingNumbers.contains(masterbagShipmentAssociationEntry.getTrackingNumber());
                if (foundTrackingNumber)
                    break;
            }
            Assert.assertTrue(foundTrackingNumber, String.format("trackingnumbers[%s] not found in masterbag[%s]", trackingNumbers, masterBagId));
        } else {
            Assert.fail(String.format("Number of shipments found in masterbag[%s] is invalid/different than the #shipments added.", masterBagId));
        }
    }

    public void validateMultipleShipmentsInMasterBag(Long masterBagId, List<String> trackingNumbers) {
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertEquals(masterbagResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getId(), masterBagId);
        List<MasterbagShipmentAssociationEntry> masterbagShipmentAssociationEntries = masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries();
        boolean foundTrackingNumber = false;
        if (trackingNumbers.size() == masterbagShipmentAssociationEntries.size()) {
            for (MasterbagShipmentAssociationEntry masterbagShipmentAssociationEntry : masterbagShipmentAssociationEntries) {
                foundTrackingNumber = trackingNumbers.contains(masterbagShipmentAssociationEntry.getTrackingNumber());
                if (foundTrackingNumber)
                    break;
            }
            Assert.assertTrue(foundTrackingNumber, String.format("trackingnumbers[%s] not found in masterbag[%s]", trackingNumbers, masterBagId));
        } else {
            Assert.fail(String.format("Number of shipments found in masterbag[%s] is invalid/different than the #shipments added.", masterBagId));
        }
    }

    public void validateRemoveShipmentFromMasterBag(MasterbagUpdateResponse masterbagUpdateResponse, Long masterBagId, String trackingNumber) {
        Assert.assertEquals(masterbagUpdateResponse.getStatus().getStatusType().toString(), "SUCCESS");
        Assert.assertEquals(masterbagUpdateResponse.getResponseCode().toString(), "SUCCESS");
        Assert.assertEquals(masterbagUpdateResponse.getMasterbagId(), masterBagId);
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        List<MasterbagShipmentAssociationEntry> masterbagShipmentAssociationEntries = masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries();
        boolean foundTrackingNumber = false;
        for (MasterbagShipmentAssociationEntry masterbagShipmentAssociationEntry : masterbagShipmentAssociationEntries) {
            foundTrackingNumber = masterbagShipmentAssociationEntry.getTrackingNumber().equalsIgnoreCase(trackingNumber);
            if (foundTrackingNumber)
                break;
        }
        Assert.assertFalse(foundTrackingNumber, String.format("trackingnumber[%s] found in masterbag[%s] even after removing it", trackingNumber, masterBagId));
    }

    public void validateIsEmptyMasterBag(Long masterBagId) {
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertTrue((masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries().size() == 0), "Masterbag is not Empty");
    }

    public void validateRemoveShipmentFromEmptyMasterBag(MasterbagUpdateResponse masterbagUpdateResponse, Long masterBagId) {
        Assert.assertEquals(masterbagUpdateResponse.getStatus().getStatusType().toString(),"ERROR");
        Assert.assertEquals(masterbagUpdateResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());
        Assert.assertTrue(masterbagUpdateResponse.getErrorMessage().contains("not present in Masterbag " + masterBagId));
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusMessage().contains("event SHIPMENTS_REMOVED failed"));
    }

    public void validateClosedMasterBagClose(MasterbagUpdateResponse masterbagUpdateResponse, Long masterBagId) {
        Assert.assertEquals(masterbagUpdateResponse.getStatus().getStatusType().toString(),"ERROR");
        Assert.assertEquals(masterbagUpdateResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_CONFIGURED.toString());
    }

    public void validateAddInvalidShipmentToMasterBag(MasterbagUpdateResponse addShipmentToMbResponse, Long masterBagId, String trackingNumber) {
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), StatusResponse.Type.ERROR.toString());
        Assert.assertEquals(addShipmentToMbResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertTrue((masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries().size() == 0), "Masterbag is not Empty");
    }

    public void validateEditShipment(MasterbagDomain editShipmentResponse, Integer newCapacity, String newDestination, String originHubCode, String courierCode, String tenantId, ShippingMethod orderShippingMethod) {
        Assert.assertTrue(editShipmentResponse.getCapacity() == newCapacity);
        Assert.assertEquals(editShipmentResponse.getDestinationHubCode(), newDestination);
        Assert.assertEquals(editShipmentResponse.getOriginHubCode(), originHubCode);
        Assert.assertEquals(editShipmentResponse.getShippingMethod(), orderShippingMethod);
        Assert.assertEquals(editShipmentResponse.getCourierCode(), courierCode);
        Assert.assertEquals(editShipmentResponse.getTenantId(), tenantId);
        Assert.assertEquals(editShipmentResponse.getMasterbagType(), MasterbagType.HUB_TO_DC);
    }

    public void validateAddSameSipmentToNewMasterBag(MasterbagUpdateResponse masterbagUpdateResponse, Long masterBagId, Long newMasterBagId) {
        Assert.assertEquals(masterbagUpdateResponse.getStatus().getStatusType().toString(), StatusResponse.Type.WARNING.toString());
        Assert.assertEquals(masterbagUpdateResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.WARNING_DURING_TRANSITION.toString());
        Assert.assertTrue(masterbagUpdateResponse.getErrorMessage().contains("is/are already present in another masterbag"));
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusMessage().contains("Update to Masterbag masterBagId with event SHIPMENTS_ADDED failed.".replace("masterBagId", String.valueOf(newMasterBagId))));
        //validate mlShipment table
        MasterbagResponse oldMasterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertTrue(oldMasterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries().size() == 1, "");
        MasterbagResponse newMasterbagResponse = masterBagServiceHelper.searchMasterBagById(newMasterBagId);
        Assert.assertTrue(newMasterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries().size() == 0, "");
    }

    public void validateAddShipmentToClosedMasterBag(MasterbagUpdateResponse masterbagUpdateResponse, Long masterBagId) {
        Assert.assertEquals(masterbagUpdateResponse.getStatus().getStatusType().toString(), StatusResponse.Type.ERROR.toString());
        Assert.assertEquals(masterbagUpdateResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_CONFIGURED.toString());
        Assert.assertTrue(masterbagUpdateResponse.getErrorMessage().contains("SHIPMENTS_ADDED not defined for Masterbag Id: masterBagId in status CLOSED".replace("masterBagId", String.valueOf(masterBagId))));
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusMessage().contains("Update to Masterbag masterBagId with event SHIPMENTS_ADDED failed.".replace("masterBagId", String.valueOf(masterBagId))));
        //validate mlShipment table
        MasterbagResponse newMasterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertTrue(newMasterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries().size() == 1, "");
    }

    public void validateTMSMasterBag(String tenantId, Long masterBagId, MasterbagUpdateEvent expectedMasterBagStatus) throws Exception {
        Thread.sleep(5000);
        TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
        TmsTrackingDetailResponse tmsTrackingDetailResponse = (TmsTrackingDetailResponse) tmsServiceHelper.getMasterbagTrackingDetail.apply(tenantId, masterBagId);
        //get current status - List maintains insertion order - Last is the latest (to do optmize - get last index by size)
        Iterator<TmsTrackingEntry> tmsTrackingEntryIterator = tmsTrackingDetailResponse.getTmsTrackingEntryList().iterator();
        TmsTrackingEntry currentTrackingEntry = null;
        while (tmsTrackingEntryIterator.hasNext()) {
            currentTrackingEntry = tmsTrackingEntryIterator.next();
        }
        Assert.assertEquals(currentTrackingEntry.getActivityType().toString(), expectedMasterBagStatus.toString(), "Invalid MasterBag Status at TMS");
        //also validate state transition
        switch (expectedMasterBagStatus) {
            case CREATE_MASTERBAG:
                Assert.assertEquals(currentTrackingEntry.getToStatus().toString(), TMSMasterbagStatus.NEW.toString(), "Invalid MasterBag State Transition at TMS");
                break;
            case INSCAN:
                Assert.assertEquals(currentTrackingEntry.getToStatus().toString(), TMSMasterbagStatus.RECEIVED_AT_TRANSPORT_HUB.toString(), "Invalid MasterBag State Transition at TMS");
                break;
            case ADD_TO_CONTAINER:
                Assert.assertEquals(currentTrackingEntry.getToStatus().toString(), TMSMasterbagStatus.ADDED_TO_CONTAINER.toString(), "Invalid MasterBag State Transition at TMS");
                break;
            case SHIP:
                Assert.assertEquals(currentTrackingEntry.getToStatus().toString(), TMSMasterbagStatus.IN_TRANSIT.toString(), "Invalid MasterBag State Transition at TMS");
                break;
            case IN_TRANSIT:
                Assert.assertEquals(currentTrackingEntry.getToStatus().toString(), TMSMasterbagStatus.IN_TRANSIT.toString(), "Invalid MasterBag State Transition at TMS");
                break;
            case RECEIVE:
                Assert.assertEquals(currentTrackingEntry.getToStatus().toString(), TMSMasterbagStatus.RECEIVED.toString(), "Invalid MasterBag State Transition at TMS");
                break;
        }
    }
}
