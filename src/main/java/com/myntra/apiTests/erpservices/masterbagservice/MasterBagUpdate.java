package com.myntra.apiTests.erpservices.masterbagservice;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.testng.Assert;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;

public class MasterBagUpdate {

    /**
     * Used to Reopen the Masterbag which is in closed state
     * @param masterBagId
     * @return
     */
    public String reopenMasterBag(Long masterBagId) {
        String response = null;
        String payload = MessageFormat.format(" <shipmentResponse>\n" +
                "    <data>\n" +
                "        <shipment>\n" +
                "            <id>{0}</id>\n" +
                "        </shipment>\n" +
                "    </data>\n" +
                "</shipmentResponse>", String.valueOf(masterBagId));
        try{
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.REOPENMASTERBAG, new String[]{""},
                    SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
            response = service.getResponseBody();
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail("Unable to reopen masterbag");
        }
        return response;
    }



    /**
     * This method uses Admin Correction to update MB Status, Used to mark MB status as INtransit from CLOSED status
     * @param masterBagId
     * @return
     */
    public String markMasterbagStatusInTransit(Long masterBagId){
        String response = null;
        String shipmentPayload = MessageFormat.format("<shipment>\n" +
                "    <id>{0}</id>\n" +
                "</shipment>", String.valueOf(masterBagId));

        try{
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.MASTERBAGINTRANSIT, new String[]{""},
                    SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.PUT, shipmentPayload, Headers.getLmsHeaderXML());
            response = service.getResponseBody();
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail("Unable to mark masterbag status as IN_TRANSIT");
        }
        return response;
    }

    /**
     * mark master bag as lost
     * @param masterBagId
     * @return
     * @throws UnsupportedEncodingException
     */
    public String updateMasterBagAsLost(Long masterBagId) throws UnsupportedEncodingException {
        String FGPathparam= MessageFormat.format( "?masterbagIds[]={0}",String.valueOf(masterBagId));
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_MASTERBAG.UPDATE_MASTERBAG_LOST, new String[]{FGPathparam} , SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.PUT, null, Headers.getLmsHeaderJSON());
        return APIUtilities.getElement(service.getResponseBody(), "status.statusMessage", "json");
    }

    /**
     * Used to Ship the Masterbag which is in RECEIVED state
     * @param masterBagId
     * @return
     */
    public String shipMasterBag(Long masterBagId) {
        String response = null;
        String payload = MessageFormat.format(" <shipmentResponse>\n" +
                "    <data>\n" +
                "        <shipment>\n" +
                "            <id>{0}</id>\n" +
                "        </shipment>\n" +
                "    </data>\n" +
                "</shipmentResponse>", String.valueOf(masterBagId));

        try{
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.SHIP_MASTER_BAG, new String[]{""},
                    SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
            response = service.getResponseBody();
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail("Unable to ship the given master bag");
        }
        return response;

    }
}
