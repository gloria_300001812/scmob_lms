package com.myntra.apiTests.erpservices.masterbagservice;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.DB.IQueries;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.client.MLShipmentClient;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lms.client.domain.response.PickupResponse;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.masterbag.core.MasterbagShipment;
import com.myntra.logistics.masterbag.core.MasterbagType;
import com.myntra.logistics.masterbag.core.MasterbagUpdate;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagResponse;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import lombok.SneakyThrows;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class MasterBagServiceHelper extends MasterBagUpdate{
    MasterBagService masterBagService;
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    String env = getEnvironment();
    public MasterBagServiceHelper(){
        masterBagService = new MasterBagService();
    }

    public MasterbagDomain createMasterBag(MasterbagDomain masterbagDomain)
            throws Exception {
        MasterbagDomain masterbagResponse;
        masterbagResponse = masterBagService.createMasterbag(masterbagDomain);
        return masterbagResponse;
    }

    public MasterbagDomain createMasterBag(long sourcePremisesID, long destinationPremisesID, ShippingMethod shippingMethod, String courierCode) throws Exception {
        String sourcePremisesCode = getHubCodeByPremiseId(sourcePremisesID);
        String destinationPremisesCode = getHubCodeByPremiseId(destinationPremisesID);
        return createMasterBag(sourcePremisesCode,  destinationPremisesCode, shippingMethod, courierCode, LMS_CONSTANTS.TENANTID);
    }

    public MasterbagDomain createMasterBag(Long sourcePremisesID, Long destinationPremisesID, ShippingMethod shippingMethod, String courierCode, String tenantId)
            throws Exception {
        String sourcePremisesCode = getHubCodeByPremiseId(sourcePremisesID);
        String destinationPremisesCode = getHubCodeByPremiseId(destinationPremisesID);
        return createMasterBag(sourcePremisesCode,  destinationPremisesCode, shippingMethod, courierCode, tenantId);
    }

    public MasterbagDomain createMasterBag(Long dcId, String wareHouseID, ShippingMethod shippingMethod, String courierCode) throws Exception {
        LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCDetails(""+dcId);
        HubWareHouseConfigResponse hubWareHouseConfigResponse = lmsServiceHelper.getHubFromWarehouse(wareHouseID, ShipmentType.DL, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        String originHubCode = hubWareHouseConfigResponse.getHubWarehouseConfigEntries().get(0).getHubCode();
        String destinationHubCode = deliveryCenterResponse.getDeliveryCenters().get(0).getDeliveryHubCode();
        return createMasterBag(originHubCode,  destinationHubCode, shippingMethod, courierCode, LMS_CONSTANTS.TENANTID);
    }

    public MasterbagDomain createMasterBag(String originHubCode, String destinationHubCode, ShippingMethod shippingMethod, String courierCode, String tenantId) {
        MasterbagDomain masterbagResponse;
        MasterbagDomain masterbagDomain = MasterbagDomain.builder()
        .originHubCode(originHubCode)
        .originHubType(lmsServiceHelper.searchHubByCode(originHubCode).getHub().get(0).getType().name())
        .destinationHubCode(destinationHubCode)
        .destinationHubType(lmsServiceHelper.searchHubByCode(destinationHubCode).getHub().get(0).getType().name())
        .shippingMethod(shippingMethod)
        .courierCode(courierCode)
        .tenantId(tenantId)
        .build();
        masterbagResponse = masterBagService.createMasterbag(masterbagDomain);
        return masterbagResponse;
    }

    public MasterbagDomain createMasterBag(String originHubCode, String destinationHubCode, ShippingMethod shippingMethod, String courierCode,Integer capacity, String tenantId) {
        MasterbagDomain masterbagResponse;
        MasterbagDomain masterbagDomain = MasterbagDomain.builder()
        .originHubCode(originHubCode)
        .originHubType(lmsServiceHelper.searchHubByCode(originHubCode).getHub().get(0).getType().name())
        .destinationHubCode(destinationHubCode)
        .destinationHubType(lmsServiceHelper.searchHubByCode(destinationHubCode).getHub().get(0).getType().name())
        .shippingMethod(shippingMethod)
        .courierCode(courierCode)
        .capacity(capacity)
        .tenantId(tenantId)
        .build();
        masterbagResponse = masterBagService.createMasterbag(masterbagDomain);
        return masterbagResponse;
    }

    public void createMasterBag(String originHubCode, String destinationHubCode, ShippingMethod shippingMethod, MasterbagType masterbagType, String courierCode, String tenantId) {
        MasterbagDomain masterbagResponse;
        MasterbagDomain masterbagDomain = MasterbagDomain.builder()
        .originHubCode(originHubCode)
        .originHubType(lmsServiceHelper.searchHubByCode(originHubCode).getHub().get(0).getType().name())
        .destinationHubCode(destinationHubCode)
        .destinationHubType(lmsServiceHelper.searchHubByCode(destinationHubCode).getHub().get(0).getType().name())
        .shippingMethod(shippingMethod)
        .masterbagType(masterbagType)
        .courierCode(courierCode)
        .tenantId(tenantId)
        .build();
        masterbagResponse = masterBagService.createMasterbag(masterbagDomain);
    }

    public void createMasterBag(String originHubCode, String destinationHubCode, ShippingMethod shippingMethod, MasterbagType masterbagType, String courierCode,Integer capacity, String tenantId) {
        MasterbagDomain masterbagResponse;
        MasterbagDomain masterbagDomain = MasterbagDomain.builder()
        .originHubCode(originHubCode)
        .originHubType(lmsServiceHelper.searchHubByCode(originHubCode).getHub().get(0).getType().name())
        .destinationHubCode(destinationHubCode)
        .destinationHubType(lmsServiceHelper.searchHubByCode(destinationHubCode).getHub().get(0).getType().name())
        .shippingMethod(shippingMethod)
        .masterbagType(masterbagType)
        .capacity(capacity)
        .courierCode(courierCode)
        .tenantId(tenantId)
        .build();
        masterbagResponse = masterBagService.createMasterbag(masterbagDomain);
    }

    public MasterbagDomain editMasterBag(Long masterBagId, MasterbagDomain masterbagDomain) {
        MasterbagDomain masterbagResponse;
        masterbagResponse = masterBagService.editMasterBag(masterBagId, masterbagDomain);
        return masterbagResponse;
    }

    public MasterbagDomain editMasterBag(Long masterBagId, String originHubCode, String destinationHubCode, ShippingMethod shippingMethod, String courierCode, Integer capacity, String tenantId) {
        MasterbagDomain masterbagResponse;
        MasterbagDomain masterbagDomain = MasterbagDomain.builder()
        .originHubCode(originHubCode)
        .originHubType(lmsServiceHelper.searchHubByCode(originHubCode).getHub().get(0).getType().name())
        .destinationHubCode(destinationHubCode)
        .destinationHubType(lmsServiceHelper.searchHubByCode(destinationHubCode).getHub().get(0).getType().name())
        .shippingMethod(shippingMethod)
        .capacity(capacity)
        .courierCode(courierCode)
        .tenantId(tenantId)
        .build();
        masterbagResponse = masterBagService.editMasterBag(masterBagId, masterbagDomain);
        return masterbagResponse;
    }

    public MasterbagUpdateResponse addShipmentsToMasterBag(Long shipmentId, String trackingNumber, ShipmentType shipmentType , String tenantId){
        MasterbagUpdateResponse masterbagUpdateResponse;
        MasterbagUpdate masterbagUpdate = new MasterbagUpdate();
        masterbagUpdate.setId(shipmentId);
        masterbagUpdate.setTenantId(tenantId);
        Set<MasterbagShipment> masterbagShipments = new HashSet<>();
        MasterbagShipment masterbagShipment = new MasterbagShipment();
        masterbagShipment.setTrackingNumber(trackingNumber);
        masterbagShipment.setShipmentType(shipmentType);
        masterbagShipments.add(masterbagShipment);
        masterbagUpdate.setMasterbagShipments(masterbagShipments);
        masterbagUpdateResponse = masterBagService.addShipmentsToMasterBag(masterbagUpdate);
        return masterbagUpdateResponse;
    }

    /**
     * If shipmentType is DL, then pass sourceReturnId = null, else pass sourceReturnId
     */
    public MasterbagUpdateResponse addShipmentsToMasterBagWithShipmentType(Long shipmentId, String trackingNumber, ShipmentType shipmentType , String tenantId) throws Exception {
        MasterbagUpdateResponse masterbagUpdateResponse;
        MasterbagUpdate masterbagUpdate = new MasterbagUpdate();
        masterbagUpdate.setId(shipmentId);
        masterbagUpdate.setTenantId(tenantId);
        Set<MasterbagShipment> masterbagShipments = new HashSet<>();
        MasterbagShipment masterbagShipment = new MasterbagShipment();
        LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
        MLShipmentClient_QA mlShipmentClient_qa=new MLShipmentClient_QA();
        MLShipmentResponse mlShipmentResponse;
        PickupResponse pickupResponse;
        String orderId = null;
        String sourceReturnId = null;
        if(shipmentType==ShipmentType.DL) {
            mlShipmentResponse = mlShipmentClient_qa.getMLShipmentDetailsByTrackingNumber(trackingNumber);
            orderId = (mlShipmentResponse.getMlShipmentEntries()!=null) ? mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId() : null;
            if(orderId==null){
                //TODO This loop will execute when courier code is not equal to ML
                Map<String, Object> masterBagDetails = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.get_OrderToShip_data, trackingNumber), "myntra_lms");
                orderId= String.valueOf(masterBagDetails.get("order_id"));
            }
        }
        if(shipmentType==ShipmentType.PU) {
            pickupResponse = lmsServiceHelper.findPickupByTrackingNumber(trackingNumber);
            sourceReturnId = (pickupResponse!=null) ? pickupResponse.getDomainPickupShipments().get(0).getReturnShipments().get(0).getSourceReferenceId() : null;
        }
        masterbagShipment.setTrackingNumber(trackingNumber);
        masterbagShipment.setOrderId(orderId);
        masterbagShipment.setSourceReturnId(sourceReturnId);
        masterbagShipment.setShipmentType(shipmentType);
        masterbagShipments.add(masterbagShipment);
        masterbagUpdate.setMasterbagShipments(masterbagShipments);
        masterbagUpdateResponse = masterBagService.addShipmentsToMasterBag(masterbagUpdate);
        return masterbagUpdateResponse;
    }


    public MasterbagUpdateResponse removeShipmentsFromMasterBag(Long shipmentId, String trackingNumber, ShipmentType shipmentType,  String tenantId){
        MasterbagUpdateResponse masterbagUpdateResponse;
        MasterbagUpdate masterbagUpdate = new MasterbagUpdate();
        masterbagUpdate.setId(shipmentId);
        masterbagUpdate.setTenantId(tenantId);
        masterbagUpdate.setForceUpdate(true);
        Set<MasterbagShipment> masterbagShipments = new HashSet<>();
        MasterbagShipment masterbagShipment = new MasterbagShipment();
        masterbagShipment.setTrackingNumber(trackingNumber);
        masterbagShipment.setShipmentType(shipmentType);
        masterbagShipments.add(masterbagShipment);
        masterbagUpdate.setMasterbagShipments(masterbagShipments);
        masterbagUpdateResponse = masterBagService.removeShipmentsFromMasterBag(masterbagUpdate);
        return masterbagUpdateResponse;
    }



    public MasterbagUpdateResponse closeMasterBag(Long shipmentId, String tenantId){
        MasterbagUpdateResponse masterbagUpdateResponse;
        MasterbagUpdate masterbagUpdate = new MasterbagUpdate();
        masterbagUpdate.setId(shipmentId);
        masterbagUpdate.setTenantId(tenantId);
        masterbagUpdateResponse = masterBagService.closeMasterBag(masterbagUpdate);
        return masterbagUpdateResponse;
    }

    public MasterbagResponse searchMasterBagById(Long masterBagId){
        MasterbagResponse masterbagResponse = masterBagService.findMasterBagById(masterBagId);
        return masterbagResponse;
    }

    public String getHubCodeByPremiseId(Long premiseId) throws Exception {
        LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
        String premisesHubCode = null;
        HubResponse hubResponse = lmsServiceHelper.searchHubById(premiseId);
        if(hubResponse.getHub().size()>0){
            premisesHubCode = hubResponse.getHub().get(0).getCode();
        } else
        {
            DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCDetails(""+premiseId);
            premisesHubCode = deliveryCenterResponse.getDeliveryCenters().get(0).getDeliveryHubCode();
        }
        return premisesHubCode;
    }

    /**
     * search master bag using query param
     * @param queryParam
     * @return
     */
    public MasterbagResponse searchMasterbagUsingQueryParam(String queryParam){
        MasterbagResponse masterbagResponse = null;
        try{
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_MASTERBAG.CRUD_MASTERBAG_SEARCH, new String[]{queryParam},
                    SERVICE_TYPE.MASTERBAG_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
            masterbagResponse = (MasterbagResponse) APIUtilities.getJsontoObject(service.getResponseBody(),
                    new MasterbagResponse());
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail("Unable to search masterbag");
        }
        return masterbagResponse;
    }

    /**
     * Used to add order into mb using forceUpdate functionality
     * @param shipmentId
     * @param trackingNumber
     * @param shipmentType
     * @param tenantId
     * @param forceUpdate
     * @return
     */
    public MasterbagUpdateResponse addShipmentsToMasterBagWithForceUpdate(Long shipmentId, String trackingNumber, ShipmentType shipmentType , String tenantId,boolean forceUpdate){
        MasterbagUpdateResponse masterbagUpdateResponse;
        MasterbagUpdate masterbagUpdate = new MasterbagUpdate();
        masterbagUpdate.setId(shipmentId);
        masterbagUpdate.setTenantId(tenantId);
        masterbagUpdate.setForceUpdate(forceUpdate);
        Set<MasterbagShipment> masterbagShipments = new HashSet<>();
        MasterbagShipment masterbagShipment = new MasterbagShipment();
        masterbagShipment.setTrackingNumber(trackingNumber);
        masterbagShipment.setShipmentType(shipmentType);
        masterbagShipments.add(masterbagShipment);
        masterbagUpdate.setMasterbagShipments(masterbagShipments);
        masterbagUpdateResponse = masterBagService.addShipmentsToMasterBag(masterbagUpdate);
        return masterbagUpdateResponse;
    }

    @SneakyThrows
    public ShipmentResponse masterbagInscan(Long shipmentId, ShipmentStatus shipmentStatus, Long lastScanPremisesId, PremisesType premisesType){

        ShipmentEntry shipmentEntry = new ShipmentEntry();
        shipmentEntry.setId(shipmentId);
        shipmentEntry.setStatus(shipmentStatus);
        shipmentEntry.setLastScannedCity("Electronics City (ELC)-DC");
        shipmentEntry.setLastScannedPremisesId(lastScanPremisesId);
        shipmentEntry.setLastScannedPremisesType(premisesType);
        shipmentEntry.setArrivedOn(new Date());
        shipmentEntry.setLastScannedOn(new Date());

        String payload = APIUtilities.convertXMLObjectToString(shipmentEntry);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.MASTER_BAG, new String[]{Long.toString(shipmentId)},
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        ShipmentResponse shipmentResponse = (ShipmentResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new ShipmentResponse());
        return shipmentResponse;
    }

    @SneakyThrows
    public ShipmentResponse markOrderAsShortage(String trackingNumber, long masterBagId, ShipmentType shipmentType, OrderShipmentAssociationStatus orderShipmentAssociationStatus,
                                                ShipmentStatus shipmentStatus, PremisesType premisesType, Long lastScannedPremisesId){

        OrderShipmentAssociationEntry orderShipmentAssociationEntries = new OrderShipmentAssociationEntry();
        orderShipmentAssociationEntries.setTrackingNumber(trackingNumber);
        orderShipmentAssociationEntries.setShipmentType(shipmentType);
        orderShipmentAssociationEntries.setStatus(orderShipmentAssociationStatus);

        List<OrderShipmentAssociationEntry> orderShipmentAssociation = new ArrayList<>();
        orderShipmentAssociation.add(orderShipmentAssociationEntries);

        ShipmentEntry shipmentEntryPayload = new ShipmentEntry();
        shipmentEntryPayload.setId(masterBagId);
        shipmentEntryPayload.setStatus(shipmentStatus);
        shipmentEntryPayload.setLastScannedCity("Bangalore returns hub-HUB");
        shipmentEntryPayload.setLastScannedPremisesId(lastScannedPremisesId);
        shipmentEntryPayload.setLastScannedPremisesType(premisesType);
        shipmentEntryPayload.setArrivedOn(new Date());
        shipmentEntryPayload.setLastScannedOn(new Date());
        shipmentEntryPayload.setOrderShipmentAssociationEntries(orderShipmentAssociation);

        String payload = APIUtilities.convertXMLObjectToString(shipmentEntryPayload);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.MASTER_BAG, new String[]{Long.toString(masterBagId)},
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        ShipmentResponse shipmentResponse = (ShipmentResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new ShipmentResponse());
        return shipmentResponse;
    }


}
