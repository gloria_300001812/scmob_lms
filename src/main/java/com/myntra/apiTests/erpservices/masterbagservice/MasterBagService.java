package com.myntra.apiTests.erpservices.masterbagservice;


import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.client.DeliveryCenterClient;
import com.myntra.logistics.masterbag.core.MasterbagUpdate;
import com.myntra.logistics.masterbag.core.MasterbagUpdateEvent;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagResponse;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.testng.Assert;
import org.testng.log4testng.Logger;


/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class MasterBagService {

    static Logger log = Logger.getLogger(DeliveryCenterClient.class);
    /**
     * Masterbag operations without using client. Created this as there was bug with masterbag client.
     */
    public MasterbagDomain createMasterbag(MasterbagDomain masterbagDomain){
        MasterbagDomain masterbagResponse = null;
        try{
            System.out.println("masterbagEntry="+masterbagDomain);
            String param = "";
            String payload = APIUtilities.convertJavaObjectToJsonUsingGson(masterbagDomain);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_MASTERBAG.CRUD_MASTERBAG, new String[]{param},
                    SERVICE_TYPE.MASTERBAG_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
            masterbagResponse = (MasterbagDomain) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(),
                    MasterbagDomain.class, false);
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail("Unable to create masterbag");
        }
        return masterbagResponse;
    }

    //http://rolling-masterbag.dockins.myntra.com/masterbag-service/crud/masterbag/255661
    public MasterbagDomain editMasterBag(MasterbagDomain masterbagDomain){
        MasterbagDomain masterbagResponse = null;
        try{
            System.out.println("masterbagEntry="+masterbagDomain);
            String param = masterbagDomain.getId().toString();
            String payload = APIUtilities.convertJavaObjectToJsonUsingGson(masterbagDomain);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_MASTERBAG.CRUD_MASTERBAG, new String[]{param},
                    SERVICE_TYPE.MASTERBAG_SVC.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
            masterbagResponse = (MasterbagDomain) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(),
                    MasterbagDomain.class, false);
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to Edit masterbag");
        }
        return masterbagResponse;
    }

    public MasterbagDomain editMasterBag(Long masterBagId, MasterbagDomain masterbagDomain){
        MasterbagDomain masterbagResponse = null;
        try{
            System.out.println("masterbagEntry="+masterbagDomain);
            String param = String.valueOf(masterBagId);
            String payload = APIUtilities.convertJavaObjectToJsonUsingGson(masterbagDomain);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_MASTERBAG.CRUD_MASTERBAG, new String[]{param},
                    SERVICE_TYPE.MASTERBAG_SVC.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
            masterbagResponse = (MasterbagDomain) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(),
                    MasterbagDomain.class, false);
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to Edit masterbag");
        }
        return masterbagResponse;
    }


    public MasterbagUpdateResponse addShipmentsToMasterBag(MasterbagUpdate masterbagUpdate){
        MasterbagUpdateResponse masterbagUpdateResponse=null;
        String param = "";
        try{
            System.out.println("masterbagUpdate="+masterbagUpdate);
            masterbagUpdate.setEvent(MasterbagUpdateEvent.SHIPMENTS_ADDED);
            String payload = APIUtilities.convertJavaObjectToJsonUsingGson(masterbagUpdate);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_MASTERBAG.UPDATE_MASTERBAG, new String[]{param},
                    SERVICE_TYPE.MASTERBAG_SVC.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
            masterbagUpdateResponse = (MasterbagUpdateResponse) APIUtilities.getJsontoObject(service.getResponseBody(),
                    new MasterbagUpdateResponse());
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to add shipments to masterbag");
        }
        return masterbagUpdateResponse;
    }

    public MasterbagUpdateResponse removeShipmentsFromMasterBag(MasterbagUpdate masterbagUpdate){
        MasterbagUpdateResponse masterbagUpdateResponse = null;
        String param = "";
        try{
            System.out.println("masterbagUpdate="+masterbagUpdate);
            masterbagUpdate.setEvent(MasterbagUpdateEvent.SHIPMENTS_REMOVED);
            String payload = APIUtilities.convertJavaObjectToJsonUsingGson(masterbagUpdate);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_MASTERBAG.UPDATE_MASTERBAG, new String[]{param},
                    SERVICE_TYPE.MASTERBAG_SVC.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
            masterbagUpdateResponse = (MasterbagUpdateResponse) APIUtilities.getJsontoObject(service.getResponseBody(),
                    new MasterbagUpdateResponse());
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to remove shipments from masterbag");
        }
        return masterbagUpdateResponse;
    }

    public MasterbagUpdateResponse closeMasterBag(MasterbagUpdate masterbagUpdate){
        MasterbagUpdateResponse masterbagUpdateResponse = null;
        String param = "";
        try{
            System.out.println("masterbagUpdate="+masterbagUpdate);
            masterbagUpdate.setEvent(MasterbagUpdateEvent.CLOSE);
            String payload = APIUtilities.convertJavaObjectToJsonUsingGson(masterbagUpdate);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_MASTERBAG.UPDATE_MASTERBAG, new String[]{param},
                    SERVICE_TYPE.MASTERBAG_SVC.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
            masterbagUpdateResponse = (MasterbagUpdateResponse) APIUtilities.getJsontoObject(service.getResponseBody(),
                    new MasterbagUpdateResponse());
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to close masterbag");
        }
        return masterbagUpdateResponse;
    }

    public MasterbagResponse findMasterBagById(Long masterBagId){
        MasterbagResponse masterbagResponse = null;
        try{
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_MASTERBAG.CRUD_MASTERBAG, new String[]{String.valueOf(masterBagId)},
                    SERVICE_TYPE.MASTERBAG_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
            masterbagResponse = (MasterbagResponse) APIUtilities.getJsontoObject(service.getResponseBody(),
                    new MasterbagResponse());
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to search masterbag");
        }
        return masterbagResponse;
    }

    public MasterbagResponse filteredSearch(int start, int fetchSize, String  sortBy, String  sortOrder, boolean  distinct, String  q, String  f ){
        MasterbagResponse masterbagResponse = null;
        try{
            //masterbagResponse = masterbagServiceClient.filteredSearch(start, fetchSize, sortBy, sortOrder, distinct, q, f, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to search masterbag");
        }
        return masterbagResponse;
    }

    public MasterbagResponse filteredSearch(int start, int fetchSize, String  sortBy, String  sortOrder, boolean  distinct, String  q, String  f , boolean encodedSearchTerms){
        MasterbagResponse masterbagResponse = null;
        try{
            //masterbagResponse = masterbagServiceClient.filteredSearch(start, fetchSize, sortBy, sortOrder, distinct, q, f, encodedSearchTerms,  MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to search masterbag");
        }
        return masterbagResponse;
    }



}
