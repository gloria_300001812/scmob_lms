package com.myntra.apiTests.erpservices.masterbagservice;

public enum MasterBagV2Search {
    MASTER_BAG_ID("id.eq:{0}"),
    MULTIPLE_MB_ID("id.in:{0}"),
    STATUS("status.eq:{0}"),
    ORIGIN_LOCATION("originPremisesType.eq:{0}___originPremisesId.eq:{1}"),
    DEST_LOCATION("destinationPremisesType.eq:{0}___destinationPremisesId.eq:{1}"),
    SHIPPING_METHOD("shippingMethod.eq:{0}"),
    SHIPPED_ON("shippedOn.ge:{0}___shippedOn.le:{1}"),
    LAST_MODIFIED_ON("lastModifiedOn.ge:{0}___lastModifiedOn.le:{1}"),

    ;
    private String fieldValue;

    MasterBagV2Search(String fieldValue) {
        this.fieldValue =fieldValue;
    }
    public String getFieldValue(){
        return this.fieldValue;
    }
}
