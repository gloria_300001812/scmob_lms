package com.myntra.apiTests.erpservices.sortation.dataset;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class BeanToCSV {
    public static void main(String[] args) {

        // name of generated csv
        final String CSV_LOCATION = "Employees.csv";

        try {

            // Creating writer class to generate
            // csv file
            FileWriter writer = new FileWriter(CSV_LOCATION);

            // create a list of employee
            List<SortLocationBean> sortLocationList = new ArrayList<SortLocationBean>();
            SortLocationBean sortLocation1 = new SortLocationBean("4019","DH-BLR","DELHI","DH-DEL","EXTERNAL","Square","BLUE","1");
            SortLocationBean sortLocation2 = new SortLocationBean("4019","DH-DEL","DDC","DELHIS","EXTERNAL","Square","BLUE","1");

            sortLocationList.add(sortLocation1);
            sortLocationList.add(sortLocation2);

            // Create Mapping Strategy to arrange the
            // column name in order
            ColumnPositionMappingStrategy mappingStrategy =  new ColumnPositionMappingStrategy();
            mappingStrategy.setType(SortLocationBean.class);

            // Arrange column name as provided in below array.
            String[] columns = new String[] {"Tenant Id","Current Hub","Bin Label","Destination Location Hub Code","Type","Shape","Color","Is Active"};
            mappingStrategy.setColumnMapping(columns);
            System.out.println(mappingStrategy.getColumnName(1));

            // Createing StatefulBeanToCsv object
            StatefulBeanToCsvBuilder<SortLocationBean> builder = new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv beanWriter = builder.build();

            // Write list to StatefulBeanToCsv object
            beanWriter.write(sortLocationList);

            // closing the writer object
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}