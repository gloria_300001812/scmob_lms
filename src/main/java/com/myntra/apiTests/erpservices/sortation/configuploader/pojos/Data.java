package com.myntra.apiTests.erpservices.sortation.configuploader.pojos;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class Data
{
    private String expires;

    private Form form;

    private Fields[] fields;

    public String getExpires ()
    {
        return expires;
    }

    public void setExpires (String expires)
    {
        this.expires = expires;
    }

    public Form getForm ()
    {
        return form;
    }

    public void setForm (Form form)
    {
        this.form = form;
    }

    public Fields[] getFields ()
    {
        return fields;
    }

    public void setFields (Fields[] fields)
    {
        this.fields = fields;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [expires = "+expires+", form = "+form+", fields = "+fields+"]";
    }
}