package com.myntra.apiTests.erpservices.sortation.configuploader;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.sortation.configuploader.pojos.Fields;
import com.myntra.apiTests.erpservices.sortation.configuploader.pojos.UploadMetadataResponse;
import com.myntra.lms.client.status.BulkJobStatus;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class SortationConfigUploader {
    private HttpURLConnection conn;
    String successUrl = "http://love.stage.myntra.com/module/app/utils/UploadOrderStatusForm.php";

    public String getFetchMetadataURL() {
        String UploadServiceURL = "http://upload.stage.myntra.com/s3/form";
        String queryParam = "?successUrl=" + successUrl + "&expires=6000";
        return UploadServiceURL + queryParam;
    }

    public String getAmazonWebServiceURL() {
        String awsURL = "https://myntrapartnercontent.s3.amazonaws.com/";
        return awsURL;
    }

    /**
     * Here we are using UI API for upload
     *
     * @return
     */
    public UploadMetadataResponse getMetadataForUpload() throws Exception {
        String statusType, data;
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpGet httpget = null;
        httpget = new HttpGet(getFetchMetadataURL());
        httpget.addHeader("Authorization", "Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==");
        httpget.addHeader("Content-Type", "application/json");
        HttpResponse response = httpclient.execute(httpget);
        HttpEntity resEntity = response.getEntity();
        InputStream is = resEntity.getContent();
        System.out.println(is.toString());
        String responseBody = EntityUtils.toString(response.getEntity());
        System.out.println("responseBody" + responseBody);
        statusType = APIUtilities.getElement(responseBody, "status.statusType", "json");
        Assert.assertEquals(statusType, "SUCCESS", "Failed to fetch upload Metadata");
        data = APIUtilities.getElement(responseBody, "data.fields", "json");
        System.out.println("data" + data);
        UploadMetadataResponse uploadMetadataResponse = (UploadMetadataResponse) APIUtilities.getJsontoObject(responseBody, new UploadMetadataResponse());
        System.out.println("uploadMetadataResponse" + uploadMetadataResponse);
        return uploadMetadataResponse;
    }

    public String getMetadataForUpload(UploadMetadataResponse uploadMetadataResponse, String key) throws Exception {
        String value = null;
        for (Fields field : uploadMetadataResponse.getData().getFields()) {
            if (field.getName().equalsIgnoreCase(key))
                value = field.getValue();
        }
        return value;
    }

    public String uploadFileToAWS(String filePath, UploadMetadataResponse uploadMetadataResponse) throws Exception {
        HttpClient httpClient = HttpClientBuilder.create().build();
        String fileName = new File(filePath).getName();
        String awsFileUrl = null;
        int uploadStatusCounter = 0;
        HttpEntity entity = MultipartEntityBuilder
                .create()
                .addTextBody("success_action_redirect", getMetadataForUpload(uploadMetadataResponse, "success_action_redirect"))
                .addTextBody("acl", getMetadataForUpload(uploadMetadataResponse, "acl"))
                .addTextBody("key", getMetadataForUpload(uploadMetadataResponse, "key").trim().replace("${filename}", fileName))
                .addTextBody("policy", getMetadataForUpload(uploadMetadataResponse, "policy"))
                .addTextBody("x-amz-algorithm", getMetadataForUpload(uploadMetadataResponse, "x-amz-algorithm"))
                .addTextBody("x-amz-credential", getMetadataForUpload(uploadMetadataResponse, "x-amz-credential"))
                .addTextBody("x-amz-date", getMetadataForUpload(uploadMetadataResponse, "x-amz-date"))
                .addTextBody("x-amz-meta-user", getMetadataForUpload(uploadMetadataResponse, "x-amz-meta-user"))
                .addTextBody("x-amz-signature", getMetadataForUpload(uploadMetadataResponse, "x-amz-signature"))
                .addBinaryBody("file", new File(filePath), ContentType.create("application/octet-stream"), "filename")
                .build();

        HttpPost httpPost = new HttpPost(getAmazonWebServiceURL());
        httpPost.setEntity(entity);
        HttpResponse response = httpClient.execute(httpPost);
        System.out.println(response.getEntity().getContent());
        String responseBody = EntityUtils.toString(response.getEntity());
        while (!responseBody.contains("File Upload Complete")) {
            Thread.sleep(1000);
            if (uploadStatusCounter++ > 60) {
                System.out.println("File Upload Failed!");
                break;
            }
        }
        awsFileUrl = uploadMetadataResponse.getData().getForm().getAction() +
                getMetadataForUpload(uploadMetadataResponse, "key").trim().replace("${filename}", fileName);
        return awsFileUrl;
    }

    public Integer submitS3Job(String awsFileUrl, String filePath, String bulkJobType) throws IOException {
        String jobStatus = null, responseStatusMessage, jobId;
        String s3JobPayload = "<s3JobEntry>\n" +
                "    <fileUrl>%s</fileUrl>\n" +
                "    <fileName>%s</fileName>\n" +
                "    <bulkJobType>%s</bulkJobType>\n" +
                "    <createdBy>scmadmin</createdBy>\n" +
                "</s3JobEntry>";
        s3JobPayload = String.format(s3JobPayload, awsFileUrl, new File(filePath).getName(), bulkJobType);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.SUBMITS3_BULKJOB, null,
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, s3JobPayload, Headers.getLmsHeaderXML());
        responseStatusMessage = APIUtilities.getElement(service.getResponseBody(), "bulkJobResponse.status.statusMessage", "xml");
        jobStatus = APIUtilities.getElement(service.getResponseBody(), "bulkJobResponse.data.record.status", "xml");
        jobId = APIUtilities.getElement(service.getResponseBody(), "bulkJobResponse.data.record.id", "xml");
        Assert.assertEquals(responseStatusMessage, "Bulk Job Created", "Submit job failed");
        Assert.assertEquals(jobStatus, BulkJobStatus.CREATED.toString(), "Submit job failed");
        System.out.println("Bulk jobId=" + jobId);
        return Integer.parseInt(jobId);
    }

    public String getJobStatus(Integer jobId) throws IOException {
        String jobStatus = null, responseStatusType, jobResult;
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_BULKJOB, new String[]{String.valueOf(jobId)},
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        responseStatusType = APIUtilities.getElement(service.getResponseBody(), "bulkJobResponse.status.statusType", "xml");
        jobStatus = APIUtilities.getElement(service.getResponseBody(), "bulkJobResponse.data.record.status", "xml");
        jobResult = APIUtilities.getElement(service.getResponseBody(), "bulkJobResponse.data.record.result", "xml");
        System.out.println(String.format("Bulk job:[%s] Status:[%s] JobResult:[jobResult]", jobId, jobStatus, jobResult));
        return jobStatus;
    }

    public Boolean getJobFinalStatus(Integer jobId) throws Exception {
        boolean bFlag = false;
        int maxWaitTimeInSeconds = 120, retryCount = 0;
        String jobStatus=null;
        while (true) {
            jobStatus = getJobStatus(jobId);
            if(jobStatus.equalsIgnoreCase(BulkJobStatus.COMPLETE.toString()))
                break;
            Thread.sleep(1000);
            if (retryCount++ > maxWaitTimeInSeconds)
                break;
        }
        if (jobStatus.equalsIgnoreCase(BulkJobStatus.COMPLETE.toString()))
            bFlag = true;
        return bFlag;
    }


}
