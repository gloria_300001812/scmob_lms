package com.myntra.apiTests.erpservices.sortation.configuploader.pojos;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class UploadMetadataResponse
{
    private Data data;

    private Status status;

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    public Status getStatus ()
    {
        return status;
    }

    public void setStatus (Status status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "UploadMetadataResponse [data = "+data+", status = "+status+"]";
    }
}