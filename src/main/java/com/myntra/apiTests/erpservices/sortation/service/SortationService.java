package com.myntra.apiTests.erpservices.sortation.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.sortation.domain.ShipmentInfo;
import com.myntra.sortation.domain.SortLocationType;
import com.myntra.sortation.response.ShipmentInfoResponse;
import com.myntra.sortation.response.SortationConfigResponse;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.testng.Assert;
import org.testng.log4testng.Logger;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class SortationService {
    static Logger log = Logger.getLogger(SortationService.class);

    /**
     * Fetch Entire path for shipping label
     */
    public SortationPathResponse getComputedPath(String origin, String destination, ShippingMethod shippingMethod, String courierCode, String sourceClientId , String clientId, String tenantId){
        SortationPathResponse sortationPathResponse = null;
        String queryParms = "?clientId=%s&origin=%s&destination=%s&sourceClientId=%s&shippingMethod=%s&courierCode=%s";
        String pathParam = "";
        try{
            queryParms = String.format(queryParms ,clientId ,origin ,destination,sourceClientId, shippingMethod,courierCode);
            pathParam = String.format("tenant/%s/"+ Constants.LMS_SORTATION.COMPUTE_PATH,tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathParam+queryParms, null,
                    SERVICE_TYPE.SORTATION_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
            sortationPathResponse = (SortationPathResponse) APIUtilities.getJsontoObject(service.getResponseBody(), new SortationPathResponse());
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to get/fetch entire path for shipping label");
        }
        return sortationPathResponse;
    }

    /**
     * Fetch next sort location
     */
    public SortationConfigResponse getNextSortLocation(String trackingNumber, String currentLocation, SortLocationType sortLocationType  , String clientId, String tenantId){
        SortationConfigResponse sortationConfigResponse = null;
        String queryParms = "?clientId=%s&trackingNumber=%s&currentLocation=%s&sortLocationType=%s&tenantId=%s";
        String pathParam = "";
        try{
            queryParms = String.format(queryParms ,clientId ,trackingNumber ,currentLocation,sortLocationType,tenantId);
            pathParam = String.format("tenant/%s/"+ Constants.LMS_SORTATION.SHIPMENT_CONFIG,tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathParam+queryParms, null,
                    SERVICE_TYPE.SORTATION_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
            sortationConfigResponse = (SortationConfigResponse) APIUtilities.getJsontoObject(service.getResponseBody(), new SortationConfigResponse());
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to Fetch next sort location");
        }
        return sortationConfigResponse;
    }

    /**
     * Fetch next sort location without providing SortLocationType explictly.
     * This is the way to identify the next sort location type.
     */
    public SortationConfigResponse getNextSortLocation(String trackingNumber, Integer sortLevel, String currentLocation  , String clientId, String tenantId){
        SortationConfigResponse sortationConfigResponse = null;
        String queryParms = "?clientId=%s&trackingNumber=%s&sortLevel=%s&currentLocation=%s";
        String pathParam = "";
        try{
            queryParms = String.format(queryParms ,clientId ,trackingNumber ,sortLevel,currentLocation);
            pathParam = String.format("tenant/%s/"+ Constants.LMS_SORTATION.SHIPMENT_CONFIG,tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathParam+queryParms, null,
                    SERVICE_TYPE.SORTATION_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
            sortationConfigResponse = (SortationConfigResponse) APIUtilities.getJsontoObject(service.getResponseBody(), new SortationConfigResponse());
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to Fetch next sort location");
        }
        return sortationConfigResponse;
    }

    /**
     * Manifest Order in Sortation Platform
     */
    public ShipmentInfoResponse manifestShipmentInSortationPlatform(ShipmentInfo shipmentInfo){
        ShipmentInfoResponse shipmentInfoResponse = null;
        try{
            System.out.println("shipmentInfo="+shipmentInfo);
            String param = "";
            String payload = APIUtilities.convertJavaObjectToJsonUsingGson(shipmentInfo);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_SORTATION.SHIPMENT_MANIFEST, new String[]{param},
                    SERVICE_TYPE.SORTATION_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
            shipmentInfoResponse = (ShipmentInfoResponse) APIUtilities.getJsontoObject(service.getResponseBody(),
                    new ShipmentInfoResponse());
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            Assert.fail("Unable to Manifest Order in Sortation Platform");
        }
        return shipmentInfoResponse;
    }
}
