package com.myntra.apiTests.erpservices.sortation.service.Pojos;

import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.returns.common.enums.ReturnType;

import java.util.List;
import java.util.Map;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class OrderData {
    String originHubCode;
    String destinationHubCode;
    String rtOriginHubCode;
    String rtDestinationHubCode;
    String RHDDestinationHubCode;
    String courierCode;
    String rtoHubCode;
    Map<String, Object> orderMap;
    Map<String, Object> returnOrderMap;
    Map<String, Object> exchangeOrderMap;
    List<String> trackingNumbers, packetIds, orderIds;
    List<String> rtTrackingNumbers, rtPacketIds, rtOrderIds;
    List<String> exchangeTrackingNumbers, exchangePacketIds, exchangeOrderIds;
    String currentHub;
    String nextHub;
    String zipcode;
    String wareHouseId;
    ShippingMethod shippingMethod;
    String paymentMode;
    boolean isMultiSeller;
    boolean isTryAndBuy;
    String tenantId;
    String clientId;
    ReturnType returnType;
    Map<String, SortationData> sortConfigForward;
    Map<String, SortationData> sortConfigReturn;
    Map<String, SortationData> sortConfigB2B;
    Map<String, SortationData> sortConfigRHD;

    public OrderData() {
        paymentMode = "cod";
        clientId = "2297";
        isMultiSeller = false;
        isTryAndBuy = false;
    }

    public ReturnType getReturnType() {
        return returnType;
    }

    public void setReturnType(ReturnType returnType) {
        this.returnType = returnType;
    }

    public String getRtOriginHubCode() {
        return rtOriginHubCode;
    }

    public void setRtOriginHubCode(String rtOriginHubCode) {
        this.rtOriginHubCode = rtOriginHubCode;
    }

    public String getRtDestinationHubCode() {
        return rtDestinationHubCode;
    }

    public void setRtDestinationHubCode(String rtDestinationHubCode) {
        this.rtDestinationHubCode = rtDestinationHubCode;
    }

    public String getRHDDestinationHubCode() {
        return RHDDestinationHubCode;
    }

    public void setRHDDestinationHubCode(String RHDDestinationHubCode) {
        this.RHDDestinationHubCode = RHDDestinationHubCode;
    }

    public List<String> getRtTrackingNumbers() {
        return rtTrackingNumbers;
    }

    public void setRtTrackingNumbers(List<String> rtTrackingNumbers) {
        this.rtTrackingNumbers = rtTrackingNumbers;
    }

    public List<String> getRtPacketIds() {
        return rtPacketIds;
    }

    public void setRtPacketIds(List<String> rtPacketIds) {
        this.rtPacketIds = rtPacketIds;
    }

    public List<String> getRtOrderIds() {
        return rtOrderIds;
    }

    public void setRtOrderIds(List<String> rtOrderIds) {
        this.rtOrderIds = rtOrderIds;
    }

    public boolean isMultiSeller() {
        return isMultiSeller;
    }

    public void setMultiSeller(boolean multiSeller) {
        isMultiSeller = multiSeller;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getOriginHubCode() {
        return originHubCode;
    }

    public void setOriginHubCode(String originHubCode) {
        this.originHubCode = originHubCode;
    }

    public String getDestinationHubCode() {
        return destinationHubCode;
    }

    public void setDestinationHubCode(String destinationHubCode) {
        this.destinationHubCode = destinationHubCode;
    }

    public String getCourierCode() {
        return courierCode;
    }

    public void setCourierCode(String courierCode) {
        this.courierCode = courierCode;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public List<String> getExchangeTrackingNumbers() {
        return exchangeTrackingNumbers;
    }

    public void setExchangeTrackingNumbers(List<String> exchangeTrackingNumbers) {
        this.exchangeTrackingNumbers = exchangeTrackingNumbers;
    }

    public List<String> getExchangePacketIds() {
        return exchangePacketIds;
    }

    public void setExchangePacketIds(List<String> exchangePacketIds) {
        this.exchangePacketIds = exchangePacketIds;
    }

    public List<String> getExchangeOrderIds() {
        return exchangeOrderIds;
    }

    public void setExchangeOrderIds(List<String> exchangeOrderIds) {
        this.exchangeOrderIds = exchangeOrderIds;
    }

    public Map<String, Object> getOrderMap() {
        return orderMap;
    }

    public void setOrderMap(Map<String, Object> orderMap) {
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        this.orderMap = orderMap;
    }

    public Map<String, Object> getReturnOrderMap() {
        return returnOrderMap;
    }

    public void setReturnOrderMap(Map<String, Object> rtOrderMap) {
        rtTrackingNumbers = (List<String>) rtOrderMap.get("returnTrackingNumbers");
        rtPacketIds = (List<String>) rtOrderMap.get("returnPacketIds");
        this.returnOrderMap = rtOrderMap;
    }

    public Map<String, Object> getExchangeOrderMap() {
        return exchangeOrderMap;
    }

    public void setExchangeOrderMap(Map<String, Object> exchangeOrderMap) {
        setExchangeTrackingNumbers((List<String>) exchangeOrderMap.get("exTrackingNumbers"));
        setExchangePacketIds((List<String>) exchangeOrderMap.get("exPacketIds"));
        setExchangeOrderIds((List<String>) exchangeOrderMap.get("exchangeOrderIds"));
        this.exchangeOrderMap = exchangeOrderMap;
    }

    public List<String> getTrackingNumbers() {
        return trackingNumbers;
    }

    public void setTrackingNumbers(List<String> trackingNumbers) {
        this.trackingNumbers = trackingNumbers;
    }

    public List<String> getPacketIds() {
        return packetIds;
    }

    public void setPacketIds(List<String> packetIds) {
        this.packetIds = packetIds;
    }

    public List<String> getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(List<String> orderIds) {
        this.orderIds = orderIds;
    }

    public String getCurrentHub() {
        return currentHub;
    }

    public void setCurrentHub(String currentHub) {
        this.currentHub = currentHub;
    }

    public String getNextHub() {
        return nextHub;
    }

    public void setNextHub(String nextHub) {
        this.nextHub = nextHub;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(String wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public ShippingMethod getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public boolean isTryAndBuy() {
        return isTryAndBuy;
    }

    public void setTryAndBuy(boolean tryAndBuy) {
        isTryAndBuy = tryAndBuy;
    }

    public Map<String, SortationData> getSortConfigForward() {
        return sortConfigForward;
    }

    public void setSortConfigForward(Map<String, SortationData> sortConfigForward) {
        this.sortConfigForward = sortConfigForward;
    }

    public Map<String, SortationData> getSortConfigReturn() {
        return sortConfigReturn;
    }

    public void setSortConfigReturn(Map<String, SortationData> sortConfigReturn) {
        this.sortConfigReturn = sortConfigReturn;
    }

    public Map<String, SortationData> getSortConfigB2B() {
        return sortConfigB2B;
    }

    public void setSortConfigB2B(Map<String, SortationData> sortConfigB2B) {
        this.sortConfigB2B = sortConfigB2B;
    }

    public Map<String, SortationData> getSortConfigRHD() {
        return sortConfigRHD;
    }

    public void setSortConfigRHD(Map<String, SortationData> sortConfigRHD) {
        this.sortConfigRHD = sortConfigRHD;
    }

    public String getRtoHubCode() {
        return rtoHubCode;
    }

    public void setRtoHubCode(String rtoHubCode) {
        this.rtoHubCode = rtoHubCode;
    }
}
