package com.myntra.apiTests.erpservices.sortation.helpers;

import com.myntra.apiTests.common.Utils.csvutility.domain.SortConfigUpload;
import com.myntra.apiTests.common.Utils.csvutility.domain.SortLocationUpload;
import com.myntra.apiTests.erpservices.lms.DB.SortationDAO;
import org.testng.asserts.SoftAssert;

import java.util.Map;

public class SortationUploadValidator {
    SoftAssert softAssert;

    /**
     * Method will validate the records which we uploaded through CSV file and return if the updated data is not there
     * in sortation_config DB
     *
     * @param sortConfigUpload
     */
    public void validateSortConfigUploadSuccessful(SortConfigUpload sortConfigUpload) {
        softAssert = new SoftAssert();
        for (int i = 1; i <= sortConfigUpload.getOrigin().size() - 1; i++) {
            Map<String, Object> sortConfigurationUpload = SortationDAO.getSortationConfigData(sortConfigUpload.getTenantId().get(i), sortConfigUpload.getClientId().get(i), sortConfigUpload.getOrigin().get(i), sortConfigUpload.getDestination().get(i), sortConfigUpload.getSortLevel().get(i));
            if (sortConfigurationUpload == null || sortConfigurationUpload.size() == 0) {
                softAssert.fail("the following uploaded data is not present in sortation_config DB- DATA : " + sortConfigUpload.getTenantId().get(i) + "," + sortConfigUpload.getClientId().get(i) + "," + sortConfigUpload.getOrigin().get(i) + "," + sortConfigUpload.getDestination().get(i) + "," +
                        sortConfigUpload.getSortLevel().get(i) + "," + sortConfigUpload.getSourceClientId().get(i) + "," + sortConfigUpload.getCourierCode().get(i) + "," +
                        sortConfigUpload.getShippingMethod().get(i) + "," + sortConfigUpload.getNextLocation().get(i) + "," + sortConfigUpload.getIsActive().get(i));
            }
        }
        softAssert.assertAll();
    }


    /**
     * Method will validate the records which we uploaded through CSV file and return if the updated data is not there in
     * sort_Location DB
     *
     * @param sortLocationUpload
     */
    public void validateSortLocationUploadSuccessful(SortLocationUpload sortLocationUpload) {
        softAssert = new SoftAssert();
        for (int i = 1; i <= sortLocationUpload.getDestinationLocationHubCode().size() - 1; i++) {
            Map<String, Object> sortConfigurationUpload = SortationDAO.getSortLocationData(sortLocationUpload.getTenantId().get(i), sortLocationUpload.getCurrentHub().get(i), sortLocationUpload.getBinLabel().get(i));
            if (sortConfigurationUpload == null || sortConfigurationUpload.size() == 0) {
                softAssert.fail("the following uploaded data is not present in sort_Location DB - DATA : " + sortLocationUpload.getTenantId().get(i) + "," + sortLocationUpload.getCurrentHub().get(i) + "," + sortLocationUpload.getBinLabel().get(i));
            }
        }
        softAssert.assertAll();
    }

}
