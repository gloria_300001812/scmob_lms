package com.myntra.apiTests.erpservices.sortation.service;


import com.jayway.jsonpath.JsonPath;
import com.myntra.apiTests.erpservices.lastmile.service.OrderTrackingClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.OrderTrackingClient;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.sortation.domain.SortLocationType;
import com.myntra.sortation.response.SortationConfigResponse;
import org.apache.log4j.Logger;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class SortationValidator {

    String env = getEnvironment();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private static Logger log = Logger.getLogger(SortationValidator.class);
    private OrderTrackingClient orderTrackingClient = new OrderTrackingClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    OrderTrackingClient_QA orderTrackingClient_qa=new OrderTrackingClient_QA();

    /**
     * myntra_sortation:shipment_info.shipment_details
     */
    public void validateShipmentDetails() {
        String shipmentDetailsString = "{\"courierCode\": \"ML\", \"shippingMethod\": \"NORMAL\", \"sourceClientId\": \"2297\"}";
        System.out.println("veri");
        String courierCode = JsonPath.read(shipmentDetailsString, "courierCode").toString();
        String shippingMethod = JsonPath.read(shipmentDetailsString, "shippingMethod").toString();
        String sourceClientId = JsonPath.read(shipmentDetailsString, "sourceClientId").toString();
        System.out.println(courierCode + ", " + shippingMethod + " , " + sourceClientId);
    }

    /**
     * Upon inscanning the order, we need to validate the state machine in all below 3 tables
     * Validate : shipment_tracking , order_to_ship.shipment_status table , ml_shipment.shipment_status
     */
    public void validateOrderInscan(String inScanResponse, long pincode, String tenantId) {

        Assert.assertEquals(APIUtilities.getElement(inScanResponse, "shipment.shipmentStatus", "json"), ShipmentStatus.INSCANNED.name(), "Invalid status while OrderInscan");

        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.dispatchHubCode", "json"));
        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.rtoHubCode", "json"));
        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.destinationHubCode", "json"));
        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.rtoWarehouseId", "json"));
        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.shippingMethod", "json"));
        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.inScanLocID", "json"));
        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.lastScanPremisesId", "json"));
        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.lastScanPremisesType", "json"));

        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.stateCode", "json"));
        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.pincode", "json"));

        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.shipmentType", "json"));
        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.courierCode", "json"));
        System.out.println(APIUtilities.getElement(inScanResponse, "shipment.trackingNumber", "json"));
    }

    public void validateEmptyMasterBag() {

    }

    public void validateOrderInscan(OrderData orderData, String inScanResponse, SortationConfigResponse nextSortationLocation, SortConfig.SortConfigType sortConfigType) {
        Map<String, SortationData> sortConfig = null;
        switch (sortConfigType){
            case FORWARD:
                sortConfig = orderData.getSortConfigForward();
                Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("sourceClientId"), orderData.getClientId());
                break;
            case RETURN:
                sortConfig = orderData.getSortConfigReturn();
                Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("sourceClientId"), orderData.getClientId());
                break;
            case B2B:
                sortConfig = orderData.getSortConfigB2B();
                break;
            case RHD:
                sortConfig = orderData.getSortConfigRHD();
                Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("sourceClientId"), orderData.getClientId());
                break;
        }
        String binLabel = sortConfig.get(orderData.getOriginHubCode()).binLabel;
        String nextLocation = sortConfig.get(orderData.getOriginHubCode()).sortLocationdestination;
        String binColor = sortConfig.get(orderData.getOriginHubCode()).color;
        String binShape = sortConfig.get(orderData.getOriginHubCode()).shape;

        //Todo add tenant Id validation and computepath
        //sortationConfigList
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getOriginLocation(), orderData.getOriginHubCode(), "Invalid originHubCode");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getDestinationLocation(), orderData.getDestinationHubCode(), "Invalid destinationHubCode");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLevel(), (Integer)1, "Invalid sort Level");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("courierCode"), orderData.getCourierCode());
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("shippingMethod"), orderData.getShippingMethod().name());

        //sortLocation
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceHub(), orderData.getOriginHubCode(), "Invalid originHubCode");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getName(), binLabel);
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode(), nextLocation, "Invalid next Sort Location");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getType(), SortLocationType.EXTERNAL.name(), "Invalid Sort Location Type");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getColor().toString(), binColor, "Invalid bin color");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getShape().toString(), binShape, "Invalid binShape");

        System.out.println("Next sortation bin Label : "+nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getName());
        System.out.println("Next sortation hub : "+nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode());
    }

    public void validateMasterBagOperation(Long masterBagId, String originHubCode, String nextHub, List<String> trackingNumbers, ShippingMethod shippingMethod, String tenantId) throws UnsupportedEncodingException, JAXBException {
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, tenantId);
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        while(shipmentEntryItr.hasNext()){
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            Assert.assertEquals(shipmentEntry.getId(), masterBagId);
            Assert.assertEquals(shipmentEntry.getOriginPremisesCode(), originHubCode);
            Assert.assertEquals(shipmentEntry.getDestinationPremisesCode(), nextHub);
            Assert.assertEquals(shipmentEntry.getShippingMethod(), shippingMethod);
            Assert.assertEquals(shipmentEntry.getOrderCount(), trackingNumbers.size());
            List<OrderShipmentAssociationEntry> orderShipmentAssociationEntries = shipmentEntry.getOrderShipmentAssociationEntries();
            int index =0;
            for(OrderShipmentAssociationEntry orderShipmentAssociationEntry : orderShipmentAssociationEntries){
                Assert.assertEquals(orderShipmentAssociationEntry.getShipmentId(), masterBagId);
                Assert.assertEquals(orderShipmentAssociationEntry.getTrackingNumber(), trackingNumbers.get(index++));
            }
        }
    }

    public void validateMasterBagInScanV2(OrderData orderData, ShipmentResponse shipmentResponse, SortationConfigResponse nextSortationLocation, SortConfig.SortConfigType sortConfigType) {
        Map<String, SortationData> sortConfig = null;
        switch (sortConfigType){
            case FORWARD:
                sortConfig = orderData.getSortConfigForward();
                Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getDestinationLocation(), orderData.getDestinationHubCode(), "Invalid destinationHubCode");
                break;
            case RETURN:
                sortConfig = orderData.getSortConfigReturn();
                Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getDestinationLocation(), orderData.getRtDestinationHubCode(), "Invalid destinationHubCode");
                break;
            case B2B:
                sortConfig = orderData.getSortConfigB2B();
                Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getDestinationLocation(), orderData.getDestinationHubCode(), "Invalid destinationHubCode");
                break;
            case RHD:
                sortConfig = orderData.getSortConfigRHD();
                Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getDestinationLocation(), orderData.getDestinationHubCode(), "Invalid destinationHubCode");
                break;
        }

        String binLabel = sortConfig.get(orderData.getCurrentHub()).binLabel;
        String nextLocation = sortConfig.get(orderData.getCurrentHub()).sortLocationdestination;
        String binColor = sortConfig.get(orderData.getCurrentHub()).color;
        String binShape = sortConfig.get(orderData.getCurrentHub()).shape;

        //Todo add tenant Id validation and computepath
        //sortationConfigList
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getOriginLocation(), orderData.getCurrentHub(), "Invalid originHubCode");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLevel(), (Integer)1, "Invalid sort Level");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("courierCode"), orderData.getCourierCode());
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("shippingMethod"), orderData.getShippingMethod().name());
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("sourceClientId"), orderData.getClientId());

        //sortLocation
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceHub(), orderData.getCurrentHub(), "Invalid originHubCode");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getName(), binLabel);
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode(), nextLocation, "Invalid next Sort Location");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getType(), SortLocationType.EXTERNAL.name(), "Invalid Sort Location Type");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getColor().toString(), binColor, "Invalid bin color");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getShape().toString(), binShape, "Invalid binShape");

        System.out.println("Next sortation bin Label : "+nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getName());
        System.out.println("Next sortation hub : "+nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode());
    }

    public void validateMasterBagInScanV2RHD(OrderData orderData, ShipmentResponse shipmentResponse, SortationConfigResponse nextSortationLocation) {
        Map<String, SortationData> sortConfig = null;
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().equals(StatusResponse.Type.SUCCESS), "Invalid shipment Insacn response");

        //sortLocation
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceHub(), null, "Invalid originHubCode");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getName(), orderData.getRHDDestinationHubCode());
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode(), orderData.getRHDDestinationHubCode(), "Invalid next Sort Location");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getType(), null, "Invalid Sort Location Type");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getColor(), null, "Invalid bin color");
        Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getShape(), null, "Invalid binShape");
    }

    public void validateOrderTrackingDetail(String trackingNumber, ShipmentStatus shipmentStatus, ShipmentUpdateEvent shipmentUpdateEvent, String courierCode,OrderTrackingDetailLevel level,String tenantId,String clientId) throws InterruptedException {
        OrderTrackingResponseV2 orderTrackingResponseV2 = orderTrackingClient_qa.getOrderTrackingDetailV2(trackingNumber,courierCode,level,tenantId,clientId);
        Assert.assertEquals(orderTrackingResponseV2.getOrderTrackingEntry().getShipmentStatus(), shipmentStatus);
        OrderTrackingDetailEntryV2 orderTrackingDetailEntry = orderTrackingResponseV2.getOrderTrackingEntry().getOrderTrackingDetails().get(0);
        //Assert.assertEquals(orderTrackingDetailEntry.getToStatus(), shipmentStatus.name());
        //Assert.assertEquals(orderTrackingDetailEntry.getActivityResult(), "SUCCESS");
        //Assert.assertEquals(orderTrackingDetailEntry.getActivityType(), shipmentUpdateEvent.name());
    }

    public void validateOrderTrackingDetail(List<String> trackingNumbers, ShipmentStatus shipmentStatus, ShipmentUpdateEvent shipmentUpdateEvent, String courierCode,OrderTrackingDetailLevel level,String tenantId,String clientId) throws InterruptedException {
        for(String trackingNumber : trackingNumbers){
            Thread.sleep(5000);
            validateOrderTrackingDetail(trackingNumber, shipmentStatus, shipmentUpdateEvent, courierCode,level,tenantId,clientId);
        }
    }


    public void validateOrderTrackingDetailMultiStatus(String trackingNumber, List<ShipmentStatus> shipmentStatuses, ShipmentUpdateEvent shipmentUpdateEvent, String courierCode,OrderTrackingDetailLevel level,String tenantId,String clientId) throws InterruptedException {
        boolean isValidStatus = false;
        OrderTrackingResponseV2 orderTrackingResponseV2 = orderTrackingClient_qa.getOrderTrackingDetailV2(trackingNumber, courierCode,level,tenantId,clientId);
        for(ShipmentStatus shipmentStatus : shipmentStatuses){
            if(orderTrackingResponseV2.getOrderTrackingEntry().getShipmentStatus().toString().equalsIgnoreCase(shipmentStatus.toString())){
                isValidStatus = true;
            }
        }
        Assert.assertTrue(isValidStatus, "Invalid Order Tracking detail. Expected:"+shipmentStatuses+" ,Actual:"+orderTrackingResponseV2.getOrderTrackingEntry().getShipmentStatus());
    }

    public void validateOrderTrackingDetailMultiStatus(List<String> trackingNumbers, List<ShipmentStatus> shipmentStatuses, ShipmentUpdateEvent shipmentUpdateEvent, String courierCode,OrderTrackingDetailLevel level,String tenantId,String clientId) throws InterruptedException {
        for(String trackingNumber : trackingNumbers){
            Thread.sleep(5000);
            validateOrderTrackingDetailMultiStatus(trackingNumber, shipmentStatuses, shipmentUpdateEvent, courierCode,level,tenantId,clientId);
        }
    }

}