package com.myntra.apiTests.erpservices.sortation.helpers;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.lms.Helper.HubToTransportHubConfigResponse;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.commons.exception.ManagerException;
import com.myntra.lms.client.response.TransporterResponse;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneEntry;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import com.myntra.tms.statemachine.masterbag.MasterbagUpdateEvent;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class TMSOperations {
    static String env = getEnvironment();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    SortationHelper sortationHelper = new SortationHelper();

    /**
     * Recieve MasterBag -> Create Container -> add MB -> Ship Container -> Recieve Container
     * @return
     */
    public Map<String, String> createAndShipForwardContainer(Long masterBagId, OrderData orderData, String nextHub, LaneType laneType)
            throws Exception {
        TMSMasterbagReponse tmsMasterbagReponse;
        Long laneId;
        long transporterId, containerId;
        Map<String, String> containerDetails = new HashMap<>();
        String originTransportHubCode = getNextTransportHub(orderData.getCurrentHub());
        String nextTransportHubCode = getNextTransportHub(nextHub);
        if (originTransportHubCode.equalsIgnoreCase(nextTransportHubCode)) {
            //when there is no transport hub configured/available for the next destination hub, we use destination hub/DC lane itself to recieve MB.
            //Masterbag can be recieved in DC directly
            nextTransportHubCode = nextHub;
        }
        laneId = getLaneId(originTransportHubCode, nextTransportHubCode, laneType);
        if(laneId==null) {
            //if lane not found from transport hub, then look for direct DC to DC, Hub to Hub lane present
            originTransportHubCode = orderData.getCurrentHub();
            laneId = getLaneId(originTransportHubCode, nextTransportHubCode, laneType);
        }
        System.out.println(String.format("originTransportHubCode:%s , nextTransportHubCode:[%s] , laneId:[%s]", originTransportHubCode, nextTransportHubCode, laneId));
        transporterId = getTransporterId(laneId);
        receiveMasterBag(masterBagId, originTransportHubCode, orderData.getTenantId());
        containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(originTransportHubCode, nextTransportHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println(String.format("containerId[%s] created from %s to %s", containerId, originTransportHubCode, nextTransportHubCode));
        addMasterBagToContainer(masterBagId, containerId, orderData.getTenantId());
        shipContainer(containerId);
        masterBagValidator.validateTMSMasterBag(orderData.getTenantId(), masterBagId, MasterbagUpdateEvent.SHIP);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
        receiveContainer(masterBagId, containerId, nextTransportHubCode, orderData.getTenantId());
        containerDetails.put("containerID", String.valueOf(containerId));
        containerDetails.put("originHubcode", originTransportHubCode);
        containerDetails.put("destinationHubcode", nextTransportHubCode);
        return containerDetails;
    }


    public Map<String, String> createAndShipForwardContainer(Long masterBagId, String currentHub, String nextHub, String tenantId)
            throws Exception {
        TMSMasterbagReponse tmsMasterbagReponse;
        Long laneId;
        long transporterId, containerId;
        Map<String, String> containerDetails = new HashMap<>();
        String originTransportHubCode = getNextTransportHub(currentHub);
        String nextTransportHubCode = getNextTransportHub(nextHub);
        if (originTransportHubCode.equalsIgnoreCase(nextTransportHubCode)) {
            //when there is no transport hub configured/available for the next destination hub, we use destination hub/DC lane itself to recieve MB.
            //Masterbag can be recieved in DC directly
            nextTransportHubCode = nextHub;
        }
        laneId = getLaneId(originTransportHubCode, nextTransportHubCode);
        if(laneId==null) {
            //if lane not found from transport hub, then look for direct DC to DC, Hub to Hub lane present
            originTransportHubCode = currentHub;
            laneId = getLaneId(originTransportHubCode, nextTransportHubCode);
        }
        System.out.println(String.format("originTransportHubCode:%s , nextTransportHubCode:[%s] , laneId:[%s]", originTransportHubCode, nextTransportHubCode, laneId));
        transporterId = getTransporterId(laneId);
        receiveMasterBag(masterBagId, originTransportHubCode, tenantId);
        containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(originTransportHubCode, nextTransportHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println(String.format("containerId[%s] created from %s to %s", containerId, originTransportHubCode, nextTransportHubCode));
        addMasterBagToContainer(masterBagId, containerId, tenantId);
        shipContainer(containerId);
        masterBagValidator.validateTMSMasterBag(tenantId, masterBagId, MasterbagUpdateEvent.SHIP);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
        receiveContainer(masterBagId, containerId, nextTransportHubCode, tenantId);
        containerDetails.put("containerID", String.valueOf(containerId));
        containerDetails.put("originHubcode", originTransportHubCode);
        containerDetails.put("destinationHubcode", nextTransportHubCode);
        return containerDetails;
    }


    /**
     * Create Container and ship. No recive container.
     */
    public Map<String, String> createAndShipContainer(Long masterBagId, OrderData orderData, String nextHub, LaneType laneType)
            throws Exception {
        TMSMasterbagReponse tmsMasterbagReponse;
        Long laneId;
        long transporterId, containerId;
        Map<String, String> containerDetails = new HashMap<>();
        String originTransportHubCode = getNextTransportHub(orderData.getCurrentHub());
        String nextTransportHubCode = getNextTransportHub(nextHub);
        if (originTransportHubCode.equalsIgnoreCase(nextTransportHubCode)) {
            //when there is no transport hub configured/available for the next destination hub, we use destination hub/DC lane itself to recieve MB.
            //Masterbag can be recieved in DC directly
            nextTransportHubCode = nextHub;
        }
        laneId = getLaneId(originTransportHubCode, nextTransportHubCode, laneType);
        if(laneId==null) {
            //if lane not found from transport hub, then look for direct DC to DC, Hub to Hub lane present
            originTransportHubCode = orderData.getCurrentHub();
            laneId = getLaneId(originTransportHubCode, nextTransportHubCode, laneType);
        }
        System.out.println(String.format("originTransportHubCode:%s , nextTransportHubCode:[%s] , laneId:[%s]", originTransportHubCode, nextTransportHubCode, laneId));
        transporterId = getTransporterId(laneId);
        receiveMasterBag(masterBagId, originTransportHubCode, orderData.getTenantId());
        containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(originTransportHubCode, nextTransportHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println(String.format("containerId[%s] created from %s to %s", containerId, originTransportHubCode, nextTransportHubCode));
        addMasterBagToContainer(masterBagId, containerId, orderData.getTenantId());
        shipContainer(containerId);
        masterBagValidator.validateTMSMasterBag(orderData.getTenantId(), masterBagId, MasterbagUpdateEvent.SHIP);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
        containerDetails.put("containerID", String.valueOf(containerId));
        containerDetails.put("originHubcode", originTransportHubCode);
        containerDetails.put("destinationHubcode", nextTransportHubCode);
        return containerDetails;
    }

    /**
     * Recieve MasterBag -> Create Container -> add MB -> Ship Container -> Recieve Container
     * @return
     */
    public Map<String, String> createAndShipReturnContainer(Long masterBagId, OrderData orderData, String nextHub, LaneType laneType)
            throws Exception {
        TMSMasterbagReponse tmsMasterbagReponse;
        Map<String, String> containerDetails = new HashMap<>();
        Long laneId;
        long transporterId, containerId;
        String originTransportHubCode = getNextTransportHub(orderData.getCurrentHub());
        String nextTransportHubCode = getNextTransportHub(nextHub);
        if (originTransportHubCode.equalsIgnoreCase(nextTransportHubCode)) {
            //when there is no transport hub configured/available for the next destination hub, we use destination hub/DC lane itself to recieve MB.
            //Masterbag can be recieved in DC directly
            System.out.println(String.format("originTransportHubCode[%s] is same as nextTransportHubCode[%s], identifying by supported lanes", originTransportHubCode, nextTransportHubCode));
            //incase of DELHIS to RT-DEL - there is no lane, then we use destination with transport hub.
            if(orderData.getCurrentHub().equalsIgnoreCase(getNextTransportHub(nextHub))){
                nextTransportHubCode = nextHub;
            }else {
                nextTransportHubCode = getNextHubBySupportedLane(orderData.getCurrentHub(), getNextTransportHub(nextHub));
            }
            if (originTransportHubCode.equalsIgnoreCase(nextTransportHubCode)) {
                //when there is no transport hub configured/available for the next destination hub, we use destination hub/DC lane itself to recieve MB.
                //this can happen in DC to DC case where DC does not have TH
                originTransportHubCode = orderData.getCurrentHub();
            }
        }
        laneId = getLaneId(originTransportHubCode, nextTransportHubCode, laneType);
        transporterId = getTransporterId(laneId);
        if(transporterId==0){
            //if no lane found, then use source as DC
            laneId = getLaneId(orderData.getCurrentHub(), nextTransportHubCode, laneType);
            transporterId = getTransporterId(laneId);
        }
        System.out.println(String.format("final originTransportHubCode:%s , nextTransportHubCode:%s ", originTransportHubCode, nextTransportHubCode));
        receiveMasterBag(masterBagId, originTransportHubCode, orderData.getTenantId());
        containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(originTransportHubCode, nextTransportHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println(String.format("containerId[%s] created from %s to %s", containerId, originTransportHubCode, nextTransportHubCode));
        addMasterBagToContainer(masterBagId, containerId, orderData.getTenantId());
        shipContainer(containerId);
        receiveContainer(masterBagId, containerId, nextTransportHubCode, orderData.getTenantId());
        containerDetails.put("containerID", String.valueOf(containerId));
        containerDetails.put("originHubcode", originTransportHubCode);
        containerDetails.put("destinationHubcode", nextTransportHubCode);
        return containerDetails;
    }

    public Map<String, String> createAndShipReturnContainer(Long masterBagId, String currentHub, String nextHub, String tenantId)
            throws Exception {
        TMSMasterbagReponse tmsMasterbagReponse;
        Map<String, String> containerDetails = new HashMap<>();
        Long laneId;
        long transporterId, containerId;
        String originTransportHubCode = getNextTransportHub(currentHub);
        String nextTransportHubCode = getNextTransportHub(nextHub);
        if (originTransportHubCode.equalsIgnoreCase(nextTransportHubCode)) {
            //when there is no transport hub configured/available for the next destination hub, we use destination hub/DC lane itself to recieve MB.
            //Masterbag can be recieved in DC directly
            System.out.println(String.format("originTransportHubCode[%s] is same as nextTransportHubCode[%s], identifying by supported lanes", originTransportHubCode, nextTransportHubCode));
            //incase of DELHIS to RT-DEL - there is no lane, then we use destination with transport hub.
            if(currentHub.equalsIgnoreCase(getNextTransportHub(nextHub))){
                nextTransportHubCode = nextHub;
            }else {
                nextTransportHubCode = getNextHubBySupportedLane(currentHub, getNextTransportHub(nextHub));
            }
            if (originTransportHubCode.equalsIgnoreCase(nextTransportHubCode)) {
                //when there is no transport hub configured/available for the next destination hub, we use destination hub/DC lane itself to recieve MB.
                //this can happen in DC to DC case where DC does not have TH
                originTransportHubCode = currentHub;
            }
        }
        laneId = getLaneId(originTransportHubCode, nextTransportHubCode);
        transporterId = getTransporterId(laneId);
        if(transporterId==0){
            //if no lane found, then use source as DC
            laneId = getLaneId(currentHub, nextTransportHubCode);
            transporterId = getTransporterId(laneId);
        }
        System.out.println(String.format("final originTransportHubCode:%s , nextTransportHubCode:%s ", originTransportHubCode, nextTransportHubCode));
        receiveMasterBag(masterBagId, originTransportHubCode, tenantId);
        containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(originTransportHubCode, nextTransportHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println(String.format("containerId[%s] created from %s to %s", containerId, originTransportHubCode, nextTransportHubCode));
        addMasterBagToContainer(masterBagId, containerId, tenantId);
        shipContainer(containerId);
        receiveContainer(masterBagId, containerId, nextTransportHubCode, tenantId);
        containerDetails.put("containerID", String.valueOf(containerId));
        containerDetails.put("originHubcode", originTransportHubCode);
        containerDetails.put("destinationHubcode", nextTransportHubCode);
        return containerDetails;
    }

    /**
     * Recieve MasterBag -> Create Container -> add MB -> Ship Container -> Recieve Container
     * @return
     */
    public Map<String, String> createAndShipReturnContainer(Long masterBagId, String currentHub, String nextHub, LaneType laneType, String tenantId)
            throws Exception {
        OrderData orderData = new OrderData();
        orderData.setCurrentHub(currentHub);
        orderData.setTenantId(tenantId);
        return createAndShipReturnContainer(masterBagId, orderData, nextHub, laneType);
    }

    private void receiveMasterBag(Long masterBagId, String originTransportHubCode, String tenantId) throws Exception {
        TMSMasterbagReponse tmsMasterbagReponse;
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(originTransportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        System.out.println(String.format("Recieved masterbag[%s] at %s",masterBagId, originTransportHubCode));
        //masterBagValidator.validateTMSMasterBag(tenantId, masterBagId, MasterbagUpdateEvent.INSCAN);
        //masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED_AT_TRANSPORTER_HUB);
    }

    //VAlidate MasterBAgStatus here
    public void receiveContainer(Long masterBagId, long containerId, String nextTransportHubCode, String tenantId) throws Exception {
        Thread.sleep(5000);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, nextTransportHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        masterBagValidator.validateTMSMasterBag(tenantId, masterBagId, MasterbagUpdateEvent.IN_TRANSIT);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
        System.out.println("Container Intransit Scan completed");
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(nextTransportHubCode, masterBagId, containerId);
        System.out.println(String.format("Recieved masterbag:[%s] from container:[%s] at hub:[%s]", masterBagId, containerId, nextTransportHubCode));
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
    }

    private void shipContainer(long containerId) throws ManagerException, IOException, JAXBException, JSONException, XMLStreamException, InterruptedException {
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
    }

    private void addMasterBagToContainer(Long masterBagId, long containerId, String tenantId) throws Exception {
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        System.out.println(String.format("Added MasterBAg[%s] to container[%s]", masterBagId, containerId));
        masterBagValidator.validateTMSMasterBag(tenantId, masterBagId, MasterbagUpdateEvent.ADD_TO_CONTAINER);
    }

    private String getNextTransportHub(String originHubCode) throws Exception {
        HubToTransportHubConfigResponse originHubToTransportHubConfig = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        System.out.println("originTransportHubCode=" + originHubToTransportHubConfig);
        try {
            return originHubToTransportHubConfig.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        }catch (NullPointerException e){
            //incase Hub to transport hub config not found for hub, we can still send it from hub itself
            return originHubCode;
        }
    }

    //Find Transporter Id
    public long getTransporterId(Long laneId) throws IOException, JAXBException, JSONException, XMLStreamException, InterruptedException, ManagerException {
        long transporterId = 0;
        TransporterResponse transporterResponse;
        transporterResponse = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId));
        transporterId = (transporterResponse!=null) ? transporterResponse.getTransporterEntries().get(0).getId() : 0;
        return transporterId;
    }

    //find laneID for given path
    public Long getLaneId(String originTransportHubCode, String nextTransportHubCode, LaneType laneType) throws IOException, JAXBException, InterruptedException, ManagerException, XMLStreamException, JSONException {
        Long laneId = null;
        List<LaneEntry> laneEntries = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(originTransportHubCode, nextTransportHubCode)).getLaneEntries();
        for (LaneEntry laneEntry : laneEntries) {
            if (laneType.name().equalsIgnoreCase(laneEntry.getType().name()) &&
                    laneEntry.getType() != LaneType.MISROUTE &&
                    laneEntry.getActive() &&
                    laneEntry.getSourceHubCode().equalsIgnoreCase(originTransportHubCode) &&
                    laneEntry.getDestinationHubCode().equalsIgnoreCase(nextTransportHubCode)) {
                laneId = laneEntry.getId();
                break;
            }
        }
        System.out.println(String.format("laneId:%s for originTransportHubCode:%s, nextTransportHubCode:%s", laneId, originTransportHubCode, nextTransportHubCode));
        return laneId;
    }

    //find laneID for given path
    public Long getLaneId(String originTransportHubCode, String nextTransportHubCode) throws IOException, JAXBException, InterruptedException, ManagerException, XMLStreamException, JSONException {
        Long laneId = null;
        List<LaneEntry> laneEntries = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(originTransportHubCode, nextTransportHubCode)).getLaneEntries();
        for (LaneEntry laneEntry : laneEntries) {
            if (laneEntry.getType() != LaneType.MISROUTE &&
                    laneEntry.getActive() &&
                    laneEntry.getSourceHubCode().equalsIgnoreCase(originTransportHubCode) &&
                    laneEntry.getDestinationHubCode().equalsIgnoreCase(nextTransportHubCode)) {
                laneId = laneEntry.getId();
                break;
            }
        }
        System.out.println(String.format("laneId:%s for originTransportHubCode:%s, nextTransportHubCode:%s", laneId, originTransportHubCode, nextTransportHubCode));
        return laneId;
    }


    /**
     * Get next hub based on supported lanes.
     * Used incase of sourcetransportHub is same as destinationTransportHub.
     */
    public String getNextHubBySupportedLane(String originHubCode, String nextHubCode) throws IOException, JAXBException, InterruptedException, ManagerException, XMLStreamException, JSONException {
        String destinationHubCode = null;
        List<LaneEntry> laneEntries = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(originHubCode, nextHubCode)).getLaneEntries();
        for (LaneEntry laneEntry : laneEntries) {
            if (laneEntry.getActive() &&
                    laneEntry.getSourceHubCode().equalsIgnoreCase(originHubCode) &&
                    laneEntry.getDestinationHubCode().equalsIgnoreCase(nextHubCode) &&
                    laneEntry.getType() != LaneType.MISROUTE) {
                destinationHubCode = laneEntry.getDestinationHubCode();
                break;
            }
        }
        System.out.println(String.format("originHubCode:%s, nextHubCode:%s nextTransportHubCode:%s", originHubCode, nextHubCode, destinationHubCode));
        return destinationHubCode;
    }

}
