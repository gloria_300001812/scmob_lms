package com.myntra.apiTests.erpservices.sortation.csv;

import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.sortation.domain.SortLocationType;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class SortConfig {
    /**
     * This class interprets the csv file's of sortation configuration, which will be used in validation throught the testing
     */

    public Map<String, SortationData> sortConfig;
    String sortLocationFile;
    String sortConfigFile;
    String[] sortLocationRows;
    String[] sortConfigRows;

    public static enum SortConfigType {
        FORWARD,
        RETURN,
        RHD,
        B2B,
        FWCOMBINED,
        RTCOMBINED
    }

    public SortConfig(SortConfigType SortConfigType) {
        sortConfig = new HashMap<>();
        switch(SortConfigType){
            case FORWARD:
                sortLocationRows = Stream.of("4019,DH-BLR,NORTH,DH-DEL,EXTERNAL,SQUARE,RED,1",
                        "4019,DH-DEL,DDC,DELHIS,EXTERNAL,CIRCLE,BLUE,1").toArray(String[]::new);
                sortConfigRows = Stream.of("4019,4019,DH-BLR,DELHIS,1,2297,ML,NORMAL,NORTH,1",
                        "4019,4019,DH-DEL,DELHIS,1,2297,ML,NORMAL,DDC,1").toArray(String[]::new);
                break;
            case RETURN:
                sortLocationRows = Stream.of("4019,DELHIS,RT-NORTH-DEL,RT-DEL,EXTERNAL,RECTANGLE,PURPLE,1",
                        "4019,RT-DEL,RT-SOUTH-BLR,RT-BLR,EXTERNAL,RECTANGLE,BLACK,1").toArray(String[]::new);
                sortConfigRows = Stream.of("4019,4019,DELHIS,RT-BLR,1,2297,ML,NORMAL,RT-NORTH-DEL,1",
                        "4019,4019,RT-DEL,RT-BLR,1,2297,ML,NORMAL,RT-SOUTH-BLR,1").toArray(String[]::new);
                break;
            case RHD:
                sortLocationRows = Stream.of("4019,DH-BLR,NORTH-RHD,DH-DEL,EXTERNAL,SQUARE,RED,1",
                        "4019,DH-DEL,DEL-RHD,RHD,EXTERNAL,SQUARE,RED,1").toArray(String[]::new);
                sortConfigRows = Stream.of("4019,4019,DH-BLR,RHD,1,2297,DE,NORMAL,NORTH-RHD,1",
                        "4019,4019,DH-DEL,RHD,1,2297,DE,NORMAL,DEL-RHD,1").toArray(String[]::new);
                break;
            case B2B:
                sortLocationRows = Stream.of("4019,DH-BLR,EAST-B2B,DH-BBSR,EXTERNAL,SQUARE,GREEN,1",
                        "4019,DH-BBSR,NORTH-B2B,RT-DEL,EXTERNAL,SQUARE,GREEN,1").toArray(String[]::new);
                sortConfigRows = Stream.of("4019,4019,DH-BLR,RT-DEL,1,9999,ML,NORMAL,EAST-B2B,1",
                        "4019,4019,DH-BBSR,RT-DEL,1,9999,ML,NORMAL,NORTH-B2B,1").toArray(String[]::new);
                break;
        }
    }

    public SortConfig setSortLocationFile(String sortLocationFile) {
        this.sortLocationFile = sortLocationFile;
        return this;
    }

    public SortConfig setSortConfigFile(String sortConfigFile) {
        this.sortConfigFile = sortConfigFile;
        return this;
    }

    public Map<String, SortationData> getSortConfig(String sortLocationFile, String sortConfigFile) {
        //Read CSV File -- will do this part later - for now, we are using this static configuration which has been tested manually

        for (String sortLocationRow : sortLocationRows) {
            SortationData sortHub = sortHub = new SortationData();
            String[] columns = sortLocationRow.split(",");
            sortHub.source = columns[1];
            sortHub.binLabel = columns[2];
            sortHub.sortLocationdestination = columns[3];
            sortHub.sortLocationType = SortLocationType.valueOf(columns[4]);
            sortHub.shape = columns[5];
            sortHub.color = columns[6];
            sortHub.sortLocationActive = (columns[7]).equals("0") ? false : true;
            sortConfig.put(sortHub.source, sortHub);
        }

        for (String sortConfigRow : sortConfigRows) {
            String[] columns = sortConfigRow.split(",");
            SortationData sortHub = sortConfig.get(columns[2]);
            sortHub.sortConfigdestination = columns[3];
            sortHub.sortLevel = columns[4];
            sortHub.sourceClientId = columns[5];
            sortHub.courierCode = columns[6];
            sortHub.shippingMethod = ShippingMethod.valueOf(columns[7]);
            sortHub.nextLocation = columns[8];
            sortHub.sortConfigActive = (columns[9]).equals("0") ? false : true;
            sortConfig.put(columns[2], sortHub);
        }

        System.out.println(sortConfig);
        return sortConfig;
    }

    public static void main(String[] args) {
        SortConfig sortConfig = new SortConfig(SortConfigType.RETURN);
        Map<String, SortationData> config = sortConfig.getSortConfig(null, null);
        System.out.println(config.get("DH-BLR"));
    }

}
