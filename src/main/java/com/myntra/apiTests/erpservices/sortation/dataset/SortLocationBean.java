package com.myntra.apiTests.erpservices.sortation.dataset;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class SortLocationBean {
    String TenantId;
    String CurrentHub;
    String BinLabel;
    String DestinationLocationHubCode;
    String Type;
    String Shape;
    String Color;
    String IsActive;

    public SortLocationBean(String tenantId, String currentHub, String binLabel, String destinationLocationHubCode, String type, String shape, String color, String isActive) {
        TenantId = tenantId;
        CurrentHub = currentHub;
        BinLabel = binLabel;
        DestinationLocationHubCode = destinationLocationHubCode;
        Type = type;
        Shape = shape;
        Color = color;
        IsActive = isActive;
    }

    public String getTenantId() {
        return TenantId;
    }

    public void setTenantId(String tenantId) {
        TenantId = tenantId;
    }

    public String getCurrentHub() {
        return CurrentHub;
    }

    public void setCurrentHub(String currentHub) {
        CurrentHub = currentHub;
    }

    public String getBinLabel() {
        return BinLabel;
    }

    public void setBinLabel(String binLabel) {
        BinLabel = binLabel;
    }

    public String getDestinationLocationHubCode() {
        return DestinationLocationHubCode;
    }

    public void setDestinationLocationHubCode(String destinationLocationHubCode) {
        DestinationLocationHubCode = destinationLocationHubCode;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getShape() {
        return Shape;
    }

    public void setShape(String shape) {
        Shape = shape;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    @Override
    public String toString() {
        return "SortLocationBean{" +
                "TenantId='" + TenantId + '\'' +
                ", CurrentHub='" + CurrentHub + '\'' +
                ", BinLabel='" + BinLabel + '\'' +
                ", DestinationLocationHubCode='" + DestinationLocationHubCode + '\'' +
                ", Type='" + Type + '\'' +
                ", Shape='" + Shape + '\'' +
                ", Color='" + Color + '\'' +
                ", IsActive='" + IsActive + '\'' +
                '}';
    }
}
