package com.myntra.apiTests.erpservices.sortation.helpers;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.lastmile.client.DeliveryCenterClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.service.DeliveryCenterClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.ReturnShipmentClient;
import com.myntra.apiTests.erpservices.returnComplete.validators.ReturnShipmentValidator;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.commons.exception.ManagerException;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.OrderShipmentAssociationEntry;
import com.myntra.lms.client.response.ShipmentEntry;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.OrderStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.sortation.domain.SortLocationType;
import com.myntra.sortation.response.SortationConfigResponse;
import com.myntra.test.commons.service.Svc;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class LMSOperations {
    static String env = getEnvironment();
    LMSHelper lmsHelper = new LMSHelper();
    //TripClient tripClient = new TripClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    ReturnShipmentClient returnShipmentClient = new ReturnShipmentClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();

    public long getDeliveryCenterID(String zipcode) {
        long deliveryCenterId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(zipcode, LMS_CONSTANTS.TENANTID);
        return deliveryCenterId;
    }

    public Map<String, Object> createMockOrders(OrderData orderData, String toShipmentStatus, Integer numberOfShipments) throws Exception {
        return createMockOrders(numberOfShipments, toShipmentStatus, orderData.getWareHouseId(), orderData.getZipcode(), orderData.getShippingMethod().name(), orderData.getCourierCode(), orderData.isTryAndBuy(), orderData.getPaymentMode());
    }

    public Map<String, Object> createMockOrders(Integer numberOfShipments, String toShipmentStatus, String wareHouseId, String zipcode, String
            shippingMethod, String courierCode, boolean isTryAndBuy, String paymentMode) throws Exception {
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        Map<String, Object> orderMap = new HashMap<>();
        List<String> trackingNumbers = new ArrayList<>();
        List<String> packetIds = new ArrayList<>();
        List<String> orderIds = new ArrayList<>();
        TripResponse tripAssignmentResponse;
        //Create packets
        for (int i = 0; i < numberOfShipments; i++) {
            String orderId = lmsHelper.createMockOrder(toShipmentStatus, zipcode, courierCode, wareHouseId, shippingMethod, paymentMode, isTryAndBuy, true);
            String packetId = omsServiceHelper.getPacketId(orderId);
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);
            orderIds.add(orderId);
            trackingNumbers.add(trackingNumber);
            packetIds.add(packetId);
            System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
            /*if(toShipmentStatus.equalsIgnoreCase(EnumSCM.SH)) {
                tripAssignmentResponse = tripClient.getActiveTripForOrder(packetId, LASTMILE_CONSTANTS.TENANT_ID);
                Assert.assertEquals(tripAssignmentResponse.getStatus().getTotalCount(), 0, "Trip is active without assignment");
            }*/
        }
        orderMap.put("orderIds", orderIds);
        orderMap.put("packetIds", packetIds);
        orderMap.put("trackingNumbers", trackingNumbers);
        return orderMap;
    }

    public Map<String, Object> createMockReturnOrders(List<String> orderIds, String zipCode, ReturnType returnType) throws Exception {
        Map<String, Object> returnOrderMap = new HashMap<>();
        RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
        LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
        List<String> forwardDLTrackingNumbers = new ArrayList<>();
        List<String> returnTrackingNumbers = new ArrayList<>();
        List<String> returnPacketIds = new ArrayList<>();
        List<String> returnIds = new ArrayList<>();
        String forwardDLTrackingNumber, returnTrackingNumber, returnPacketId;
        for (String orderID : orderIds) {
            OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
            OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
            ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), returnType, ReturnMode.OPEN_BOX_PICKUP,
                    1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071",
                    zipCode, "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
            Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
            Long returnID = returnResponse.getData().get(0).getId();
            String returnOrderID = String.valueOf(returnResponse.getData().get(0).getOrderId());
            returnPacketId = omsServiceHelper.getPacketId(returnOrderID);
            forwardDLTrackingNumber = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));

            ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
            Thread.sleep(5000);
            lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
            //Manifest return order
            lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));
            OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
            returnTrackingNumber = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
            forwardDLTrackingNumbers.add(forwardDLTrackingNumber);
            returnTrackingNumbers.add(returnTrackingNumber);
            returnPacketIds.add(returnPacketId);
            returnIds.add(String.valueOf(returnID));
            System.out.println("forwardDLTrackingNumber=" + forwardDLTrackingNumber + " - returnTrackingNumber=" + returnTrackingNumber);
        }

        returnOrderMap.put("forwardDLTrackingNumbers", forwardDLTrackingNumbers);
        returnOrderMap.put("returnTrackingNumbers", returnTrackingNumbers);
        returnOrderMap.put("returnPacketIds", returnPacketIds);
        returnOrderMap.put("returnIds", returnIds);
        return returnOrderMap;
    }

    public Map<String, Object> createMockExchangeOrders(OrderData orderData, String toShipmentStatus) throws Exception {
        return createMockExchangeOrders(orderData.getOrderIds(), toShipmentStatus, orderData.getZipcode(), orderData.getCourierCode(), orderData.getWareHouseId(), orderData.getShippingMethod(), orderData.getPaymentMode());
    }

    public Map<String, Object> createMockExchangeOrders(List<String> orderIds, String status, String zipCode, String courierCode, String wareHouseId, ShippingMethod shippingMethod, String paymentMode) throws Exception {
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        Map<String, Object> exchangeOrderMap = new HashMap<>();
        List<String> trackingNumbers = new ArrayList<>();
        List<String> packetIds = new ArrayList<>();
        List<String> exchangeOrderIds = new ArrayList<>();
        TripResponse tripAssignmentResponse;

        //Create packets
        for (String orderID : orderIds) {
            String exchangeOrder = lms_createOrder.createMockOrderExchange(status, zipCode, courierCode, wareHouseId, shippingMethod.toString(), paymentMode, false, true, omsServiceHelper.getReleaseId(orderID));
            String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrder);
            String exTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

            exchangeOrderIds.add(exchangeOrder);
            packetIds.add(exchangePacketId);
            trackingNumbers.add(exTrackingNumber);
        }
        exchangeOrderMap.put("exchangeOrderIds", exchangeOrderIds);
        exchangeOrderMap.put("exPacketIds", packetIds);
        exchangeOrderMap.put("exTrackingNumbers", trackingNumbers);
        return exchangeOrderMap;
    }

    public List<String> createB2BShipments(OrderData orderData, Integer numberOfShipments){
        List<String> trackingNumbers = new ArrayList<>();
        String trackingNumber;
        for(int i=0;i<numberOfShipments; i++){
            trackingNumber = createB2BShipment(orderData.getWareHouseId(), orderData.getZipcode(), orderData.getShippingMethod(), orderData.getCourierCode(), orderData.getDestinationHubCode());
            trackingNumbers.add(trackingNumber);
        }
        return trackingNumbers;
    }

    public String createB2BShipment(String warehouseId, String pincode, ShippingMethod shippingMethod, String courierCode, String destinationPremisesId) {
        String clientId = LMS_CONSTANTS.B2B_CLIENTID;
        String shipmentType = "B2B";
        String rtoWarehouseId = warehouseId;
        destinationPremisesId = "28"; // need to check
        String integrationId = "2297";
        String sourcePath = "WMS";
        boolean isCod = false;  // COD not supported, since it is B2B
        int noOfItem = 2;
        String cod = "";
        String trackingNumber = "";
        if (isCod) {
            cod = "cod";
        }
        trackingNumber = lmsServiceHelper.getTrackingNumber(courierCode, warehouseId, cod, pincode, "").getTrackingNumberEntry().getTrackingNumber();
        Svc service = tmsServiceHelper.createShipment(clientId, trackingNumber, warehouseId, rtoWarehouseId, integrationId, sourcePath, pincode, shippingMethod, courierCode, noOfItem, isCod, destinationPremisesId, shipmentType);
        lmsServiceHelper.validateCreateShipment(service, "Shipment Creation Successful", EnumSCM.SUCCESS);
        if (APIUtilities.getElement(service.getResponseBody(), "status.statusMessage", "json").equalsIgnoreCase(EnumSCM.SUCCESS)) {
            //Data from DB Validation Using API
            OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsUsingTrackingNumber(trackingNumber);
            Assert.assertEquals(orderResponse.getOrders().get(0).getTrackingNumber(), trackingNumber, "Tracking number mismatch");
            Assert.assertEquals(orderResponse.getOrders().get(0).getWarehouseId(), warehouseId, "Warehouse mismatch");
            Assert.assertEquals(orderResponse.getOrders().get(0).getRtoWarehouseId(), rtoWarehouseId, "RTO Ware house Id mismatch");
        }
        System.out.println(String.format("B2B trackingNumber[%s]", trackingNumber));
        return trackingNumber;
    }


    //Order Inscan V2
    public String orderInscanV2(String trackingNumber, String wareHouseId, boolean forceInscan) throws IOException, JAXBException, JSONException, XMLStreamException, InterruptedException, ManagerException {
        String inScanResponse;
        inScanResponse = lmsServiceHelper.orderInscanByTrackingNumber(trackingNumber, wareHouseId, forceInscan, LMS_CONSTANTS.TENANTID);
        System.out.println("response=" + inScanResponse);
        Assert.assertEquals(APIUtilities.getElement(inScanResponse,"status.statusMessage","json"), "Order in-scanned successfully");
        Assert.assertEquals(APIUtilities.getElement(inScanResponse,"status.statusType","json"), "SUCCESS");
        Assert.assertEquals(APIUtilities.getElement(inScanResponse,"status.totalCount","json"), "1"); //will update later fro multiorders


        return inScanResponse;
    }

    public List<String> orderInscanV2(List<String> trackingNumbers, String wareHouseId, boolean forceInscan) throws IOException, JAXBException, JSONException, XMLStreamException, InterruptedException, ManagerException {
        List<String> inScanResponses = new ArrayList<>();
        for (String trackingNumber : trackingNumbers) {
            orderInscanV2(trackingNumber, wareHouseId, forceInscan);
        }
        return inScanResponses;
    }

    public String orderInscanV2(OrderData orderData, Boolean forceInscan, SortConfig.SortConfigType sortConfigType) throws IOException, JAXBException, JSONException, XMLStreamException, InterruptedException, ManagerException {
        List<String> trackingNumbers = orderData.getTrackingNumbers();
        return ordersInScan(orderData, forceInscan, sortConfigType, trackingNumbers);
    }

    public String orderInscanV2ForExchangeOrders(OrderData orderData, Boolean forceInscan, SortConfig.SortConfigType sortConfigType) throws IOException, JAXBException, JSONException, XMLStreamException, InterruptedException, ManagerException {
        List<String> exchangeTrackingNumbers = orderData.getExchangeTrackingNumbers();
        return ordersInScan(orderData, forceInscan, sortConfigType, exchangeTrackingNumbers);
    }

    /**
     * Order Inscan V2 & Sortation - Since both are chain operation.
     * Validation is also combination of Inscan & Sortation
     * @return nextHub (all packet next hub will be same)
     */
    private String ordersInScan(OrderData orderData, Boolean forceInscan, SortConfig.SortConfigType sortConfigType, List<String> trackingNumbers) throws IOException, JAXBException, JSONException, XMLStreamException, InterruptedException, ManagerException {
        String inScanResponse;
        SortationConfigResponse nextSortationLocation;
        String nextHub = null;
        String wareHouseId = orderData.getWareHouseId();
        String currentHub = orderData.getCurrentHub();
        for (String trackingNumber : trackingNumbers) {
            inScanResponse = orderInscanV2(trackingNumber, wareHouseId, forceInscan);
            nextSortationLocation = sortationHelper.getNextSortLocation(trackingNumber, currentHub, SortLocationType.EXTERNAL);
            sortationValidator.validateOrderInscan(orderData, inScanResponse, nextSortationLocation, sortConfigType);
            nextHub = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
        }
        return nextHub;
    }

    //MasterBag V2
    public Long masterBagV2Operations(OrderData orderData, List<String> trackingNumbers, String currentHub, String nextHub) throws Exception {
        Long masterBagId;
        masterBagId = masterBagV2Operations(trackingNumbers, orderData.getShippingMethod(), orderData.getCourierCode(), currentHub , nextHub, orderData.getTenantId());
        return masterBagId;
    }

    public Long masterBagV2Operations(List<String> trackingNumbers, ShippingMethod shippingMethod, String courierCode, String currentHub, String nextHub, String tenantId) throws Exception {
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse masterbagUpdateResponse;
        MasterbagUpdateResponse closeMBResponse;
        Long masterBagId;
        mbCreateResponse = masterBagServiceHelper.createMasterBag(currentHub, nextHub, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, currentHub, nextHub, shippingMethod, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println(String.format("Created MasterBag[%s] from %s to %s", masterBagId, currentHub, nextHub));
        for (String trackingNumber : trackingNumbers) {
            masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            masterBagValidator.validateAddShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumber);
        }
        masterBagValidator.validateMultipleShipmentsAddedToMasterBag(masterBagId,trackingNumbers);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(closeMBResponse, masterBagId);
        return masterBagId;
    }

    //MasterBag V2 for Reverse Masterbag
    public Long masterBagV2OperationsReverse(OrderData orderData, List<String> trackingNumbers, String currentHub, String nextHub) throws Exception {
        Long masterBagId;
        masterBagId = masterBagV2OperationsReverse(trackingNumbers, orderData.getCurrentHub() , orderData.getNextHub(),  orderData.getShippingMethod(), orderData.getCourierCode(), orderData.getTenantId());
        return masterBagId;
    }

    //MasterBag V2 for Reverse Masterbag
    public Long masterBagV2OperationsReverse(List<String> trackingNumbers, String currentHub, String nextHub,  ShippingMethod shippingMethod, String courierCode, String tenantId) throws Exception {
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse masterbagUpdateResponse;
        MasterbagUpdateResponse closeMBResponse;
        Long masterBagId;
        mbCreateResponse = masterBagServiceHelper.createMasterBag(currentHub, nextHub, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, currentHub, nextHub, shippingMethod, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println(String.format("Created MasterBag[%s] from %s to %s", masterBagId, currentHub, nextHub));
        for (String trackingNumber : trackingNumbers) {
            masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.PU, LMS_CONSTANTS.TENANTID);
            //enable later
            //masterBagValidator.validateAddShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumber);
        }
        masterBagValidator.validateMultipleShipmentsAddedToMasterBag(masterBagId,trackingNumbers);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(closeMBResponse, masterBagId);
        return masterBagId;
    }

    //MasterBag In-scan V2
    public String masterBagInScanV2(Long masterBagId, OrderData orderData, SortConfig.SortConfigType sortConfigType) throws Exception {
        System.out.println(String.format("MasterBag[%s] Inscan at %s", masterBagId, orderData.getCurrentHub()));
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, orderData.getTenantId());
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        ShipmentResponse shipmentResponse;
        SortationConfigResponse nextSortationLocation;
        String nextHub = null;
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
        while (shipmentEntryItr.hasNext()) {
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            for (OrderShipmentAssociationEntry orderShipmentAssociationEntry : shipmentEntry.getOrderShipmentAssociationEntries()) {
                String trackingNumber = orderShipmentAssociationEntry.getTrackingNumber();
                ShipmentType shipmentType = orderShipmentAssociationEntry.getShipmentType();
                lmsServiceHelper.masterBagInScanV2Update(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
                shipmentResponse = lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), trackingNumber,
                        shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId(),
                        shipmentType, shipmentEntry.getLastScannedPremisesType());
                lmsServiceHelper.masterBagInScanV2RecieveShipment(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
                nextSortationLocation = sortationHelper.getNextSortLocation(trackingNumber, orderData.getCurrentHub(), SortLocationType.EXTERNAL);
                if(nextSortationLocation.getSortationConfigList().size()!=0) {
                    sortationValidator.validateMasterBagInScanV2(orderData, shipmentResponse, nextSortationLocation, sortConfigType);
                    nextHub = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
                }else{
                    System.out.println(String.format("WARNING: Next sort location for [%s] is Null",orderData.getCurrentHub()));
                }
            }
        }
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        return nextHub;
    }


    //MasterBag In-scan V2
    public String masterBagInScanV2(Long masterBagId, String currentHub, SortConfig.SortConfigType sortConfigType, String tenantId) throws Exception {
        System.out.println(String.format("MasterBag[%s] Inscan at %s", masterBagId, currentHub));
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, tenantId);
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        ShipmentResponse shipmentResponse;
        SortationConfigResponse nextSortationLocation;
        String nextHub = null;
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
        while (shipmentEntryItr.hasNext()) {
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            for (OrderShipmentAssociationEntry orderShipmentAssociationEntry : shipmentEntry.getOrderShipmentAssociationEntries()) {
                String trackingNumber = orderShipmentAssociationEntry.getTrackingNumber();
                ShipmentType shipmentType = orderShipmentAssociationEntry.getShipmentType();
                lmsServiceHelper.masterBagInScanV2Update(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
                shipmentResponse = lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), trackingNumber,
                        shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId(),
                        shipmentType, shipmentEntry.getLastScannedPremisesType());
                lmsServiceHelper.masterBagInScanV2RecieveShipment(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
                nextSortationLocation = sortationHelper.getNextSortLocation(trackingNumber, currentHub, SortLocationType.EXTERNAL);
                if(nextSortationLocation.getSortationConfigList().size()!=0) {
                    //sortationValidator.validateMasterBagInScanV2(orderData, shipmentResponse, nextSortationLocation, sortConfigType);
                    nextHub = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
                }else{
                    System.out.println(String.format("WARNING: Next sort location for [%s] is Null",currentHub));
                }
            }
        }
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        return nextHub;
    }


    /**
     * MasterBag In-scan V2 for RHD
     * In RHD case, sortation location is not defined as once it reached Reagional handover center, rest is offline.
     * Hence Validation changes
     */
    public String masterBagInScanV2RHD(Long masterBagId, OrderData orderData) throws Exception {
        System.out.println(String.format("MasterBag[%s] Inscan at %s", masterBagId, orderData.getCurrentHub()));
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, orderData.getTenantId());
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        ShipmentResponse shipmentResponse;
        SortationConfigResponse nextSortationLocation;
        String nextHub = null;
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
        while (shipmentEntryItr.hasNext()) {
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            for (OrderShipmentAssociationEntry orderShipmentAssociationEntry : shipmentEntry.getOrderShipmentAssociationEntries()) {
                String trackingNumber = orderShipmentAssociationEntry.getTrackingNumber();
                ShipmentType shipmentType = orderShipmentAssociationEntry.getShipmentType();
                lmsServiceHelper.masterBagInScanV2Update(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
                shipmentResponse = lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), trackingNumber,
                        shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId(),
                        shipmentType, shipmentEntry.getLastScannedPremisesType());
                lmsServiceHelper.masterBagInScanV2RecieveShipment(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
                nextSortationLocation = sortationHelper.getNextSortLocation(trackingNumber, orderData.getCurrentHub(), SortLocationType.EXTERNAL);
                sortationValidator.validateMasterBagInScanV2RHD(orderData, shipmentResponse, nextSortationLocation);
                nextHub = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
            }
        }
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED_AT_HANDOVER_CENTER);
        return nextHub;
    }

    public String getNextSortationLocation(List<String> trackingNumbers, OrderData orderData) {
        SortationConfigResponse nextSortationLocation;
        String nextHub;
        nextSortationLocation = sortationHelper.getNextSortLocation(trackingNumbers.get(0), orderData.getCurrentHub(), SortLocationType.EXTERNAL);
        nextHub = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
        System.out.println(String.format("For given current hub[%s] , next sort location will be [%s]", orderData.getCurrentHub(), nextHub));
        return nextHub;
    }


    public void validateReturnStatus(List<String> returnIds, ShipmentStatus shipmentStatus, OrderStatus orderStatus) throws Exception {
        com.myntra.lms.client.domain.response.ReturnResponse returnShipmentLMSObject;
        for (String returnId : returnIds) {
            returnShipmentLMSObject = lmsServiceHelper.getReturnStatusInLMS(returnId, LMS_CONSTANTS.TENANTID ,Long.parseLong(LMS_CONSTANTS.CLIENTID));
            ReturnShipmentValidator lmsReturnShipmentValidator = new ReturnShipmentValidator(returnShipmentLMSObject);
            lmsReturnShipmentValidator.validateReturnShipmentStatus(shipmentStatus, orderStatus);
        }
    }
}
