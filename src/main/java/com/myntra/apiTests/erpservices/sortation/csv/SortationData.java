package com.myntra.apiTests.erpservices.sortation.csv;

import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.sortation.domain.SortLocationType;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class SortationData {
    public String source;
    public String sortLocationdestination;
    public String sortConfigdestination;
    public String binLabel;
    public SortLocationType sortLocationType;
    public Integer hopNumber;
    public String color;
    public String shape;
    public String TenantId;
    public String sortLevel;
    public String sourceClientId;
    public String courierCode;
    public ShippingMethod shippingMethod;
    public String nextLocation;
    public boolean sortLocationActive;
    public boolean sortConfigActive;

    @Override
    public String toString() {
        return "SortHub{" +
                "source='" + source + '\'' +
                ", sortLocationdestination='" + sortLocationdestination + '\'' +
                ", sortConfigdestination='" + sortConfigdestination + '\'' +
                ", binLabel='" + binLabel + '\'' +
                ", sortLocationType=" + sortLocationType +
                ", hopNumber=" + hopNumber +
                ", color='" + color + '\'' +
                ", shape='" + shape + '\'' +
                ", TenantId='" + TenantId + '\'' +
                ", sortLevel='" + sortLevel + '\'' +
                ", sourceClientId='" + sourceClientId + '\'' +
                ", courierCode='" + courierCode + '\'' +
                ", shippingMethod=" + shippingMethod +
                ", nextLocation='" + nextLocation + '\'' +
                ", sortLocationActive=" + sortLocationActive +
                ", sortConfigActive=" + sortConfigActive +
                '}';
    }
}