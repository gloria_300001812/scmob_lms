package com.myntra.apiTests.erpservices.sortation.helpers;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.client.DeliveryCenterClient;
import com.myntra.apiTests.erpservices.lastmile.client.TripOrderAssignmentClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.mnm.helpers.MobileNumberMaskingHelper;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClientV2_QA;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripOrderAssignmentClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.TripResponseValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.client.MLShipmentClient;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.TripOrderAssignementEntry;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.response.ItemEntry;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.logistics.platform.domain.TryAndBuyItemStatus;
import com.myntra.logistics.platform.domain.TryAndBuyNotBoughtReason;
import com.myntra.lordoftherings.boromir.DBUtilities;
import edu.emory.mathcs.backport.java.util.Arrays;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class LastmileOperations {
    static String env = getEnvironment();
    static LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    MLShipmentClient mlShipmentClient = new MLShipmentClient();
    LMSOperations lmsOperations = new LMSOperations();
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripClient_QA tripClient_qa=new TripClient_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa=new TripOrderAssignmentClient_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa=new MLShipmentClientV2_QA();
    MLShipmentClient_QA mlShipmentClient_qa=new MLShipmentClient_QA();

    /**
     * Trip Planning Operations
     */

    public Map<String, String> createTrip(Long deliveryCenterId) throws Exception {
        Map<String, String> tripDetails = new HashMap<>();
        TripResponseValidator lmsTripValidator = null;
        TripResponse tripResponse;
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(deliveryCenterId);
        tripResponse = tripClient_qa.createTrip(deliveryCenterId, deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        lmsTripValidator = new TripResponseValidator(tripResponse);
        lmsTripValidator.validateTripStatus(EnumSCM.CREATED);
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        tripDetails.put("tripId", String.valueOf(tripId));
        tripDetails.put("tripNumber", tripNumber);
        tripDetails.put("deliveryStaffID", String.valueOf(deliveryStaffID));
        return tripDetails;
    }

    public void assignMultipleOrdersToTripByOrderIds(List<String> packetIds, long tripId) throws WebClientException {
        String trackingNumber;
        for (String packetId : packetIds) {
            trackingNumber = lmsHelper.getTrackingNumber(packetId);
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
            TripResponse tripAssignmentResponse1 = tripClient_qa.getActiveTripForOrder(packetId, LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
            Assert.assertNotNull(tripAssignmentResponse1.getTrips().get(0).getId());
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        }
    }

    public void assignOrderToTripByOrderId(String packetId, long tripId) throws WebClientException {
        assignMultipleOrdersToTripByOrderIds(Arrays.asList(new String[]{packetId}), tripId);
    }

    public void assignMultipleOrdersToTripByTrackingNumbers(List<String> trackingNumbers, long tripId) throws WebClientException {
        String body;
        for (String trackingNumber : trackingNumbers) {
            TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip \n"+tripOrderAssignmentResponse);
        }
    }

    public void assignOrderToTripByTrackingNumber(String trackingNumber, long tripId) throws WebClientException {
        assignMultipleOrdersToTripByTrackingNumbers(Arrays.asList(new String[]{trackingNumber}), tripId);
    }

    public void scanTrackingNumberInTrip(Long tripId, String trackingNumber) throws IOException, JAXBException {
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNumber);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
    }

    public void scanTrackingNumbersInTrip(Long tripId, List<String> trackingNumbers) throws IOException, JAXBException {
        for(String trackingNumber : trackingNumbers){
            scanTrackingNumberInTrip(tripId, trackingNumber);
        }
    }

    /**
     * Trip Update Operations
     */

    public List<TripOrderAssignmentResponse> startTrip(Long tripId, List<String> trackingNumbers) throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        MobileNumberMaskingHelper mobileNumberMaskingHelper=new MobileNumberMaskingHelper();
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses = new ArrayList<>();
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to perform startTrip");
        for (String trackingNumber : trackingNumbers) {
            mobileNumberMaskingHelper.generateVirtualNumberByTrackingId(trackingNumber);
            TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.getTripDetails(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, tripOrderAssignmentResponse.getStatus().getStatusMessage());
            Assert.assertNotNull(tripOrderAssignmentResponse.getTripOrders().get(0).getId());
            Assert.assertTrue(tripOrderAssignmentResponse.getData().get(0).getCorrespondingTripStatus().toString().equalsIgnoreCase( EnumSCM.OUT_FOR_DELIVERY));
            //commenting below as order Id not found in response
            //Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + tripOrderAssignmentResponse.getData().get(0).getOrderId(), EnumSCM.OUT_FOR_DELIVERY, 2));
            //Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + tripOrderAssignmentResponse.getData().get(0).getOrderId(), EnumSCM.OUT_FOR_DELIVERY, 2));
            tripOrderAssignmentResponses.add(tripOrderAssignmentResponse);
        }
        System.out.println(String.format("tripId:[%s] , trackinuNumbers:[%s]",tripId,trackingNumbers));
        return tripOrderAssignmentResponses;
    }

    public TripOrderAssignmentResponse startTrip(Long tripId, String trackingNumber) throws Exception {
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponse = startTrip(tripId, Arrays.asList(new String[]{trackingNumber}));
        return tripOrderAssignmentResponse.get(0);
    }

    public void updateDeliveryTripByTrackingNumber(String trackingNumber) throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        Long tripOrderAssignemntId = ((TripOrderAssignmentResponse) lmsServiceHelper.getTripsDetailForTrackingNumber.apply(trackingNumber)).getTripOrders().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignemntId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(3000);
    }

    public void updateDeliveryTripTrackingNumbers(List<String> trackingNumbers) throws Exception {
        for (String trackingNumber : trackingNumbers) {
            updateDeliveryTripByTrackingNumber(trackingNumber);
        }
    }

    public void updateDeliveryTripByOrderId(String tripId, String packetId) throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(3000);
    }

    public void updateDeliveryTrip(String tripId, List<String> packetIds) throws Exception {
        for (String packetId : packetIds) {
            updateDeliveryTripByOrderId(tripId, packetId);
        }
    }

    public Map<String, Object> updateTryAndBuyTrip(String tripId , OrderData orderData , TryAndBuyItemStatus tryAndBuyItemStatus) throws Exception {
        return updateTryAndBuyTrip(tripId, orderData.getTrackingNumbers(), orderData.getPacketIds(), orderData.getOrderIds(), tryAndBuyItemStatus, lmsOperations.getDeliveryCenterID(orderData.getZipcode()));
    }

    public Map<String, Object> updateTryAndBuyTrip(String tripId , List<String> trackingNumbers , List<String> packetIds, List<String> orderIds , TryAndBuyItemStatus tryAndBuyItemStatus
    , Long deliveryCenterId) throws Exception {
        OrderEntry orderEntry;
        ItemEntry itemEntry;
        Integer packetIdIndex =0;
        Integer trackingNumberIndex =0;
        String packetId , trackingNumber;
        String mlShipmentId , returnTrackingNumber;
        List<String> returnTrackingNumbers = new ArrayList<>();
        List<String> returnIds = new ArrayList<>();
        Map<String, Object> returnData = new HashMap<>();
        for(String orderID : orderIds) {
            orderEntry = new OrderEntry();
            itemEntry = new ItemEntry();
            packetId = packetIds.get(packetIdIndex);
            trackingNumber = trackingNumbers.get(trackingNumberIndex);
            long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
            OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);

            com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
            Double amount = orderEntryDetails.getFinalAmount();
            Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            orderEntry.setOrderId(packetId);
            orderEntry.setDeliveryCenterId(deliveryCenterId);
            orderEntry.setCodAmount(amount);
            itemEntry.setId(Id);
            itemEntry.setStatus(tryAndBuyItemStatus);
            itemEntry.setRemarks(tryAndBuyItemStatus.toString());
            if(tryAndBuyItemStatus== TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT){
                itemEntry.setQcStatus(ItemQCStatus.PASSED);
                itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
            }
            List<ItemEntry> entries = new ArrayList<>();
            entries.add(itemEntry);
            orderEntry.setItemEntries(entries);
            TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, Long.valueOf(tripId), orderEntry);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            if(tryAndBuyItemStatus== TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT) {
                mlShipmentId = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
                returnTrackingNumber = lmsHelper.updateOperationalTrackingNumForTryAndBuy(mlShipmentId);
                returnIds.add(tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getItemEntries().get(0).getReturnId());
                returnTrackingNumbers.add(returnTrackingNumber);
            }
            packetIdIndex++;
            trackingNumberIndex++;
        }
        returnData.put("returnIds",returnIds);
        returnData.put("returnTrackingNumbers",returnTrackingNumbers);
        return returnData;
    }


    public List<String> getPickupTripOrderAssignmentId(Long tripId){
        List<String> toaIds = new ArrayList<>();
        List<Map<String, String>> toaIdResultSet = DBUtilities.exSelectQuery("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        for(Map record : toaIdResultSet) {
            toaIds.add(record.get("id").toString());
        }
        return toaIds;
    }

    public void updatePickupTrip(Long tripId , String returnTrackingNumber, String tripOrderAssignmentId) throws Exception {
        TripOrderAssignmentResponse updateTripResponse = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
        Assert.assertEquals(updateTripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
    }

    public void updatePickupTrip(Long tripId , List<String> returnTrackingNumbers) throws Exception {
        List<String> tripOrderAssignmentIds = getPickupTripOrderAssignmentId(tripId);
        int index =0;
        for(String returnTrackingNumber : returnTrackingNumbers){
            updatePickupTrip(tripId, returnTrackingNumber , tripOrderAssignmentIds.get(index));
            index++;
        }
    }

    public void updateFailedTrip(Long tripId , String trackingNumber, String tripOrderAssignmentId) throws Exception {
        TripOrderAssignmentResponse updateTripResponse = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.NOT_REACHABLE_UNAVAILABLE, EnumSCM.UPDATE);
        Assert.assertEquals(updateTripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        lmsHelper.updateOperationalTrackingNum(trackingNumber);
    }

    public void updateFailedTrip(Long tripId , List<String> trackingNumber){

    }

    public void recieveFailedDeliveryOrders(Long tripId, List<String> trackingNumbers) throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse;
        List<String>  tripOrderAssignmentIds = getPickupTripOrderAssignmentId(tripId);
        int index =0;
        for(String trackingNumber : trackingNumbers) {
            tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive "+tripOrderAssignmentResponse.getStatus().getStatusMessage());
            Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNumber), EnumSCM.FAILED_DELIVERY, "shipment status mismatch");
            Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentIds.get(index)), "true", "Wrong status in trip_order_assignment");
            index++;
        }
    }

    public void recieveTripOrders(Long tripId, List<String> trackingNumbers) throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse;
        List<String>  tripOrderAssignmentIds = getPickupTripOrderAssignmentId(tripId);
        int index =0;
        for(String trackingNumber : trackingNumbers) {
            tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive "+tripOrderAssignmentResponse.getStatus().getStatusMessage());
            Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNumber), EnumSCM.PICKUP_SUCCESSFUL, "shipment status mismatch");
            Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentIds.get(index)), "true", "Wrong status in trip_order_assignment");
            index++;
        }
    }

    public void receiveTryAndBuyTripOrder(String returnTrackingNumber) throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(returnTrackingNumber.substring(2, returnTrackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        //TODO uncommet it later, this validation is needed
        //Long tripOrderAssignmentIdForReturn = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(returnTrackingNumber);
        //Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForReturn)), "true", "Wrong status in trip_order_assignment");
        //Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(returnTrackingNumber), EnumSCM.PICKUP_SUCCESSFUL, "Status not updated to received in DC");
    }

    public void receiveTryAndBuyTripOrders(List<String> returnTrackingNumbers) throws Exception {
        for(String returnTrackingNumber : returnTrackingNumbers){
            System.out.println("Recieveing Shipments at DC "+returnTrackingNumber);
            receiveTryAndBuyTripOrder(returnTrackingNumber);
        }
    }

    public void completeReturnTrip(Long tripId) throws Exception {
        long tripOrderAssignmentId = Long.valueOf(getPickupTripOrderAssignmentId(tripId).get(0));
        TripOrderAssignmentResponse updatePickupInTripResponse = lmsServiceHelper.updatePickupInTrip(tripOrderAssignmentId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(updatePickupInTripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
    }


    /**
     * Update all the shipments with DELIVERED and Complete the trip
     */
    public void completeTrip(Long tripId, List<String> packetIds) throws Exception {
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap;
        for (String packetId : packetIds) {
            dataMap = new HashMap<>();
            dataMap.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
            dataMap.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
            tripData.add(dataMap);
        }
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        for (String packetId : packetIds) {
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(packetId, EnumSCM.DELIVERED, 4));
            Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(packetId, EnumSCM.D, 4));
        }
    }

    public void completeTrip(List<String> trackingNumbers) throws Exception {
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap;
        Long tripOrderAssignemntId;
        MLShipmentResponse mlShipmentResponse;
        for (String trackingNumber : trackingNumbers) {
            dataMap = new HashMap<>();
            tripOrderAssignemntId = ((TripOrderAssignmentResponse) lmsServiceHelper.getTripsDetailForTrackingNumber.apply(trackingNumber)).getTripOrders().get(0).getId();
            dataMap.put("trip_order_assignment_id", tripOrderAssignemntId);
            dataMap.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
            tripData.add(dataMap);
        }
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        for (String trackingNumber : trackingNumbers) {
            mlShipmentResponse = mlShipmentClient_qa.getMLShipmentDetailsByTrackingNumber(trackingNumber);
            Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), EnumSCM.DELIVERED, "Invalid Trip Status");
            Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId(), EnumSCM.D, 4));
        }
    }

    public void completeTrip(String trackingNumber) throws Exception {
        completeTrip(Arrays.asList(new String[]{trackingNumber}));
    }

    public void completeFailedTrip(List<String> trackingNumbers) throws Exception {
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap;
        Long tripOrderAssignemntId;
        MLShipmentResponse mlShipmentResponse;
        for (String trackingNumber : trackingNumbers) {
            dataMap = new HashMap<>();
            tripOrderAssignemntId = ((TripOrderAssignmentResponse) lmsServiceHelper.getTripsDetailForTrackingNumber.apply(trackingNumber)).getTripOrders().get(0).getId();
            dataMap.put("trip_order_assignment_id", tripOrderAssignemntId);
            dataMap.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
            tripData.add(dataMap);
        }
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        for (String trackingNumber : trackingNumbers) {
            mlShipmentResponse = mlShipmentClient_qa.getMLShipmentDetailsByTrackingNumber(trackingNumber);
            Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), EnumSCM.DELIVERED, "Invalid Trip Status");
            Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId(), EnumSCM.D, 4));
        }
    }

    public void completeExchangeTrip(Long tripId, List<String> packetIds, List<String> exchangeOrders) throws Exception {
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap;
        int exOrderIndex =0;
        for(String packetId : packetIds){
            dataMap = new HashMap<>();
            dataMap.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
            dataMap.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
            dataMap.put("exchangeOrderId", exchangeOrders.get(exOrderIndex));
            exOrderIndex++;
            tripData.add(dataMap);
        }
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        for(String packetId : packetIds) {
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(packetId, EnumSCM.DELIVERED, 4));
            //Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(packetId, EnumSCM.C, 4));
        }
    }

    public void completeExchangeTrip(Long tripId, String packetId, String exchangeOrders) throws Exception {
        completeExchangeTrip(tripId, Arrays.asList(new String[]{packetId}), Arrays.asList(new String[]{exchangeOrders}));
    }

    public List<OrderEntry> setOperationalTrackingIds(List<String> trackingNumbers){
        List<OrderEntry> orderEntries = new ArrayList<>();
        OrderEntry orderEntry;
        for(String trackingNumber : trackingNumbers) {
            orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            orderEntries.add(orderEntry);
        }
        return orderEntries;
    }

    public TripOrderAssignmentResponse updateExchangeTrip(Long tripId, String packetId , OrderEntry orderEntry) throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        String exchangeOrderId;
        TripOrderAssignmentResponse tripOrderAssignmentResponse;
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId ,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry);
        String responsestatus = tripOrderAssignmentResponse.getStatus().getStatusType().toString();
        Assert.assertEquals(responsestatus, EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        return tripOrderAssignmentResponse;
    }

    public Map<String, Object> updateExchangeTrip(Long tripId, List<String> packetIds, List<OrderEntry> orderEntries) throws Exception {
        int orderEntryIndex =0;
        TripOrderAssignmentResponse tripOrderAssignmentResponse;
        List<String> returnIds = new ArrayList<>();
        List<String> returnHubCodes = new ArrayList<>();
        List<String> returnTrackingNumbers = new ArrayList<>();
        Map<String, Object> returnData = new HashMap<>();
        for(String packetId : packetIds){
            tripOrderAssignmentResponse = updateExchangeTrip(tripId, packetId, orderEntries.get(orderEntryIndex));
            returnIds.add(tripOrderAssignmentResponse.getTripOrders().get(0).getSourceReturnId());
            returnHubCodes.add(tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getReturnHubCode());
            returnTrackingNumbers.add(tripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber());
            ++orderEntryIndex;
        }
        returnData.put("returnIds",returnIds);
        returnData.put("returnTrackingNumbers",returnTrackingNumbers);
        return returnData;
    }

    public void recieveExchangeTripOrders(List<String> trackingNumbers) throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse;
        for(String trackingNumber : trackingNumbers) {
            tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        }
    }

    /**
     * Composite operations
     */
    public void deliverOrders(String zipCode, List<String> trackingNumbers) throws Exception {
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        System.out.println(lmsOperations.getDeliveryCenterID(zipCode));
        long tripId = Long.valueOf(createTrip(lmsOperations.getDeliveryCenterID(zipCode)).get("tripId"));
        assignMultipleOrdersToTripByTrackingNumbers(trackingNumbers, tripId);
        startTrip(tripId, trackingNumbers);
        updateDeliveryTripTrackingNumbers(trackingNumbers);
        completeTrip(trackingNumbers);
    }
}
