package com.myntra.apiTests.erpservices.sortation.service;

import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathResponse;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.sortation.domain.SortLocationType;
import com.myntra.sortation.response.SortationConfigResponse;

import java.util.*;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class SortationHelper {
    SortationService sortationService = new SortationService();

    public SortationConfigResponse getNextExternalSortationLocation(String trackingNumber, String currentLocation){
        SortLocationType sortLocationType = SortLocationType.EXTERNAL;
        SortationConfigResponse sortationConfigResponse = sortationService.getNextSortLocation(trackingNumber, currentLocation, sortLocationType  , LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.TENANTID);
        return sortationConfigResponse;
    }

    public SortationConfigResponse getNextExternalSortationLocation(String trackingNumber, String currentLocation, String clientId, String tenantId){
        SortLocationType sortLocationType = SortLocationType.EXTERNAL;
        SortationConfigResponse sortationConfigResponse = sortationService.getNextSortLocation(trackingNumber, currentLocation, sortLocationType  , LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.TENANTID);
        return sortationConfigResponse;
    }

    public SortationConfigResponse getNextInternalSortationLocation(String trackingNumber, String currentLocation){
        SortLocationType sortLocationType = SortLocationType.INTERNAL;
        SortationConfigResponse sortationConfigResponse = sortationService.getNextSortLocation(trackingNumber, currentLocation, sortLocationType  , LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.TENANTID);
        return sortationConfigResponse;
    }

    public SortationConfigResponse getNextInternalSortationLocation(String trackingNumber, String currentLocation, String clientId, String tenantId){
        SortLocationType sortLocationType = SortLocationType.INTERNAL;
        SortationConfigResponse sortationConfigResponse = sortationService.getNextSortLocation(trackingNumber, currentLocation, sortLocationType  , LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.TENANTID);
        return sortationConfigResponse;
    }

    public SortationConfigResponse getNextSortLocation(String trackingNumber, String currentLocation,Integer sortLevel,  String clientId, String tenantId){
        SortationConfigResponse sortationConfigResponse = sortationService.getNextSortLocation(trackingNumber, sortLevel, currentLocation  , clientId, tenantId);
        return sortationConfigResponse;
    }

    public SortationConfigResponse getNextSortLocation(String trackingNumber, String currentLocation,Integer sortLevel){
        SortationConfigResponse sortationConfigResponse = sortationService.getNextSortLocation(trackingNumber, sortLevel, currentLocation  , LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.TENANTID);
        return sortationConfigResponse;
    }

    public SortationConfigResponse getNextSortLocation(String trackingNumber, String currentLocation,SortLocationType sortLocationType){
        SortationConfigResponse sortationConfigResponse = sortationService.getNextSortLocation(trackingNumber, currentLocation, sortLocationType  , LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.TENANTID);
        return sortationConfigResponse;
    }

    public SortationPathResponse getCompleteSortedPath(String origin, String destination, ShippingMethod shippingMethod, String courierCode, String sourceClientId , String clientId, String tenantId){
        SortationPathResponse sortationPathResponse = sortationService.getComputedPath( origin, destination, shippingMethod, LMSConstants.IN_HOUSE_COURIER, sourceClientId , clientId, tenantId);
        return sortationPathResponse;
    }

    public SortationPathResponse getCompleteSortedPath(String origin, String destination, ShippingMethod shippingMethod, String sourceClientId){
        SortationPathResponse sortationPathResponse = sortationService.getComputedPath( origin, destination, shippingMethod, LMSConstants.IN_HOUSE_COURIER, sourceClientId , LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.TENANTID);
        return sortationPathResponse;
    }

    public Boolean isNextSortLocationFound(String trackingNumber,String currentHub, SortLocationType sortLocationType){
        SortationConfigResponse nextSortationLocation = getNextSortLocation(trackingNumber, currentHub, sortLocationType);
        if(nextSortationLocation.getSortationConfigList().size()==0){
            System.out.println("Next sortation location is not present");
            return false;
        }
        return true;
    }

    /**
     * This method prints/Returns the compleate Path of the packet flow based on the sortation configuration.
     * This is just for understanding the packet flow prior starting the testcase & also for debuging purpose.
     */
    public void computeAndPrintCompletePath(String origin, String destination, String sourceClientId, String courierCode){
        StringBuilder completePath = new StringBuilder();
        String lane = " ---> " , label = "Label(%s)", hopNumber = "(hop=%s,type=%s)";
        SortationPathResponse sortationPathResponse = sortationService.getComputedPath( origin, destination, ShippingMethod.NORMAL, courierCode, sourceClientId , "4019", "4019");
        com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry sortationPathEntry = sortationPathResponse.getData().get(0);
        String destinationHub = sortationPathResponse.getData().get(0).getDestination();
        //convert Set to List - and sort it by hop number
        List<com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail> Hoplist = new ArrayList(sortationPathEntry.getPath());
        Collections.sort(Hoplist, new Comparator<com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail>() {
            public int compare(com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail data1, com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail data2) {
                if (data1.getHopNumber() < data2.getHopNumber()) return -1;
                if (data1.getHopNumber() > data2.getHopNumber()) return 1;
                return 0;
            }});

        Iterator<com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail> pathIterator = Hoplist.iterator();
        while(pathIterator.hasNext()){
            com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail hopDetail = pathIterator.next();
            completePath.append(hopDetail.getOrigin());
            completePath.append(lane);
            completePath.append(String.format(hopNumber, hopDetail.getHopNumber(),hopDetail.getLocationType()));
            completePath.append(lane);
            completePath.append(String.format(label, hopDetail.getNextLocation()));
            if(pathIterator.hasNext()){
                completePath.append(lane);
            }else{
                completePath.append(lane);
                completePath.append(destinationHub);
            }
        }
        System.out.println(String.format("\nComputed Path for origin[%s] & destination[%s]: \n[%s]\n\n",origin,destination, completePath));
    }

    /**
     * @return: It returns the map of the computed path <FROM,TO>.
     * This will be used for verification of next sort location.
     */
    public Map<String, String> getCompleteCompletedPath(String origin, String destination){
        Map<String, String> pathMap = new HashMap<>();
        SortationPathResponse sortationPathResponse = sortationService.getComputedPath( origin, destination, ShippingMethod.NORMAL, "ML", "2297" , "4019", "4019");
        Iterator<com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail> pathIterator = sortationPathResponse.getData().get(0).getPath().iterator();
        while(pathIterator.hasNext()){
            com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail hopDetail = pathIterator.next();
            pathMap.put(hopDetail.getOrigin(), hopDetail.getNextLocation());
        }
        System.out.println("Computed pathMap="+pathMap);
        return pathMap;
    }

    /**
     * Using next sortation location API, identify the final destination from the current location.
     * @return
     */
    public String getDestinationSortLocation(String trackingNumber, String currentLocation){
        String nextSortLocation = currentLocation;
        SortationConfigResponse nextSortationLocation;
        boolean nextLocationFound = true;
        while(nextLocationFound) {
            nextSortationLocation = getNextSortLocation(trackingNumber, nextSortLocation, SortLocationType.EXTERNAL);
            if(nextSortationLocation.getSortationConfigList().size()!=0) {
                nextSortLocation = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
            }else{
                nextLocationFound = false;
            }
        }
        System.out.println("final Destination="+nextSortLocation);
        return nextSortLocation;
    }

    public static void main(String[] args) throws Exception {
        String trackingNumber = "ML00100034000999",  currentLocation = "BLR_DH",  clientId = "4019", tenantId = "4019";
        new SortationHelper().computeAndPrintCompletePath("DH-BLR", "DELHIS", "2297" , "ML");
        new SortationHelper().getCompleteCompletedPath("DH-BLR", "DELHIS");
        //new SortationHelper().parseShipmentLabel("ML0001415192", "4019" ,  "2297");
    }
}
