package com.myntra.apiTests.erpservices.sortation.configuploader.pojos;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class Form
{
    private String enctype;

    private String method;

    private String action;

    public String getEnctype ()
    {
        return enctype;
    }

    public void setEnctype (String enctype)
    {
        this.enctype = enctype;
    }

    public String getMethod ()
    {
        return method;
    }

    public void setMethod (String method)
    {
        this.method = method;
    }

    public String getAction ()
    {
        return action;
    }

    public void setAction (String action)
    {
        this.action = action;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [enctype = "+enctype+", method = "+method+", action = "+action+"]";
    }
}