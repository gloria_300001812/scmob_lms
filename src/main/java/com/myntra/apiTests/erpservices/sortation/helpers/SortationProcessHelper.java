package com.myntra.apiTests.erpservices.sortation.helpers;

import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.sortation.domain.SortLocationType;
import com.myntra.sortation.response.SortationConfigResponse;

import java.util.List;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Jul-2019
 */
public class SortationProcessHelper {

    String env = getEnvironment();
    LMSOperations lmsOperations = new LMSOperations();
    TMSOperations tmsOperations = new TMSOperations();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    MasterBagValidator masterBagValidator = new MasterBagValidator();

    public void processFromIStoSH(final String presentHub, List<String> trackingNumbers, ShippingMethod shippingMethod, String courierCode, OrderTrackingDetailLevel level, String tenantId, String clientId ) throws Exception {
        String nextHub = null;
        String currentHub = presentHub;
        Long masterBagId, containerId = null;
        Integer hopCount = 0;
        while (nextHub != null || sortationHelper.isNextSortLocationFound(trackingNumbers.get(0), currentHub, SortLocationType.EXTERNAL)) {
            if (nextHub == null) {
                SortationConfigResponse sortationConfigResponse = sortationHelper.getNextSortLocation(trackingNumbers.get(0), currentHub, SortLocationType.EXTERNAL);
                nextHub = sortationConfigResponse.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
            }

            //MasterBag V2
            masterBagId = lmsOperations.masterBagV2Operations(trackingNumbers, shippingMethod, courierCode, currentHub, nextHub, tenantId);
            sortationValidator.validateMasterBagOperation(masterBagId, currentHub, nextHub, trackingNumbers, shippingMethod, tenantId);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
            if (hopCount > 0) //on second hop - add to MB , packet shipmentStatus will be shipped
                sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, courierCode,level,tenantId,clientId);
            else
                sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.INSCAN, courierCode,level,tenantId,clientId);

            //TMS Operations
            tmsOperations.createAndShipForwardContainer(masterBagId, currentHub, nextHub, tenantId);
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, courierCode,level,tenantId,clientId);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

            //save current node (origin at current hop)
            currentHub = nextHub;

            //MasterBag In-scan V2
            nextHub = lmsOperations.masterBagInScanV2(masterBagId, currentHub, SortConfig.SortConfigType.FORWARD, tenantId);
            System.out.println(String.format("currentHub=%s , nextHub=%s", currentHub, nextHub));
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, courierCode,level,tenantId,clientId);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
            hopCount++;
        }
    }
}
