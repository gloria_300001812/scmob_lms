package com.myntra.apiTests.erpservices.sortation.service.Pojos;

import java.util.List;

public class SortationPathResponse {
    private List<SortationPathEntry> data;

    public SortationPathResponse() {
    }

    public List<SortationPathEntry> getData() {
        return this.data;
    }

    public void setData(List<SortationPathEntry> data) {
        this.data = data;
    }

    public String toString() {
        return "SortationPathResponse(data=" + this.getData() + ")";
    }
}
