package com.myntra.apiTests.erpservices.sortation.service.Pojos;


import java.util.Map;
import java.util.Set;

public class SortationPathEntry {
    private String tenantId;
    private String clientId;
    private String origin;
    private String destination;
    private Map<String, String> clientData;
    private Set<HopDetail> path;

    public String getTenantId() {
        return this.tenantId;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getOrigin() {
        return this.origin;
    }

    public String getDestination() {
        return this.destination;
    }

    public Map<String, String> getClientData() {
        return this.clientData;
    }

    public Set<HopDetail> getPath() {
        return this.path;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setClientData(Map<String, String> clientData) {
        this.clientData = clientData;
    }

    public void setPath(Set<HopDetail> path) {
        this.path = path;
    }

    public String toString() {
        return "SortationPathEntry(tenantId=" + this.getTenantId() + ", clientId=" + this.getClientId() + ", origin=" + this.getOrigin() + ", destination=" + this.getDestination() + ", clientData=" + this.getClientData() + ", path=" + this.getPath() + ")";
    }

    public SortationPathEntry() {
    }

    public SortationPathEntry(String tenantId, String clientId, String origin, String destination, Map<String, String> clientData, Set<HopDetail> path) {
        this.tenantId = tenantId;
        this.clientId = clientId;
        this.origin = origin;
        this.destination = destination;
        this.clientData = clientData;
        this.path = path;
    }

    public static class HopDetail {
        private String origin;
        private String nextLocation;
        private String locationType;
        private Integer hopNumber;


        public HopDetail(){ }

        public HopDetail(String origin, String nextLocation, String locationType, Integer hopNumber) {
            this.origin = origin;
            this.nextLocation = nextLocation;
            this.locationType = locationType;
            this.hopNumber = hopNumber;
        }

        public String getOrigin() {
            return this.origin;
        }

        public String getNextLocation() {
            return this.nextLocation;
        }

        public String getLocationType() {
            return this.locationType;
        }

        public Integer getHopNumber() {
            return this.hopNumber;
        }

        public void setOrigin(String origin) {
            this.origin = origin;
        }

        public void setNextLocation(String nextLocation) {
            this.nextLocation = nextLocation;
        }

        public void setLocationType(String locationType) {
            this.locationType = locationType;
        }

        public void setHopNumber(Integer hopNumber) {
            this.hopNumber = hopNumber;
        }

        public String toString() {
            return "SortationPathEntry.HopDetail(origin=" + this.getOrigin() + ", nextLocation=" + this.getNextLocation() + ", locationType=" + this.getLocationType() + ", hopNumber=" + this.getHopNumber() + ")";
        }
    }
}
