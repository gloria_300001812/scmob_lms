package com.myntra.apiTests.erpservices.sortation.configuploader.pojos;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class Status
{
    private String statusType;

    private String statusMessage;

    private String statusCode;

    public String getStatusType ()
    {
        return statusType;
    }

    public void setStatusType (String statusType)
    {
        this.statusType = statusType;
    }

    public String getStatusMessage ()
    {
        return statusMessage;
    }

    public void setStatusMessage (String statusMessage)
    {
        this.statusMessage = statusMessage;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusType = "+statusType+", statusMessage = "+statusMessage+", statusCode = "+statusCode+"]";
    }
}
