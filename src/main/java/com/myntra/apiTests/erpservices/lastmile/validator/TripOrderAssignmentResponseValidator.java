/**
 * Created by Abhishek Jaiswal on 15/11/18.
 */
package com.myntra.apiTests.erpservices.lastmile.validator;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import org.testng.Assert;

public class TripOrderAssignmentResponseValidator {
    TripOrderAssignmentResponse lmsTripOrderAssignmentResponse;

    public TripOrderAssignmentResponseValidator(TripOrderAssignmentResponse lmsTripOrderAssignmentResponse) {
        this.lmsTripOrderAssignmentResponse = lmsTripOrderAssignmentResponse;
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        if (lmsTripOrderAssignmentResponse.getTripOrders() == null || lmsTripOrderAssignmentResponse.getTripOrders().isEmpty())
            Assert.fail("Could not find trip order in LMS");
    }
    /* BASE MODULAR FUNCTIONS FOR Trip Order Assignment Response*/

    public void validateTripOrderStatus(String tripStatus)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getTripOrderStatus().toString(), tripStatus, "Trip Status Not matching");
    }

    public void validateSourceReturnId(String returnId)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getSourceReturnId(), returnId, "Invalid Return Id");
    }

    public void validateSourceReturnId(String returnId, int index)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(index).getSourceReturnId(), returnId, "Invalid Return Id");
    }

    public void validateTripId(Long tripId)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getTripId(), tripId,"Trip Id mismatch");
    }

    public void validateTripId(String tripId, int index)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(index).getTripId(), tripId, "Trip Id mismatch"+index);
    }

    public void validateShipmentType(String shipmentStatus)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getShipmentType().toString(), shipmentStatus, "Unexpected Shipment Type");
    }

    public void validateOrderId(String orderID)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getOrderId(), orderID, "orderId is different from the assigned data");
    }

    public void validateOrderId(String orderID, int index)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(index).getOrderId(), orderID, "orderId is different from the assigned data");

    }

    public void validateIsScanned(int val)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getIsOutScanned(), val, "Value mismatch of IsScanned");
    }

    public void validateTrackingNum(String trackingNum)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber(), trackingNum, "Mismatch in tracking number");
    }

    public void validateTrackingNum(String trackingNum, int index)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(index).getTrackingNumber(), trackingNum, "Mismatch in tracking number");
    }


    public void validateShippingMethod(String shippingMethod)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getShippingMethod().toString(), shippingMethod, "Shipping Method Differs");
    }

    public void validateTripOrderStatusValue(String tripOrderStatusValue)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getData().get(0).getTripStatusValue(), tripOrderStatusValue, "trip Order status value differs");
    }

    public void validateTripNumber(String tripNumber)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getTripNumber(), tripNumber, "Trip Number Mismatch");
    }

    public void validateTripNumber(String tripNumber, int index)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(index).getTripNumber(), tripNumber, "Trip Number Mismatch");
    }


    public void validateAttemptReasonCode(String reason)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getAttemptReasonCode().toString(), reason, "Attempt reason code invalid");
    }

    public void validateAttemptReasonCode(String reason, int index)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(index).getAttemptReasonCode().toString(), reason, "Attempt reason code invalid");
    }

    public void validateCorrespondingTripStatus(String status)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getCorrespondingTripStatus().toString(), status, "Invalid corresponding status");
    }

    public void validateExchangeOrderId(String exchangeOrderId)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getExchangeOrderId(), exchangeOrderId, "Invalid Exchange Order Id");
    }

    public void validateCorrespondingTripStatus(String status, int index)
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(index).getCorrespondingTripStatus().toString(), status, "Invalid corresponding status");
    }

    public void validateId(String Id)//tripOrderId
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getId().toString(), Id, "Invalid Trip Id");
    }

    public void validateId(String Id ,int index)//tripOrderId
    {
        Assert.assertEquals(lmsTripOrderAssignmentResponse.getTripOrders().get(index).getId().toString(), Id, "Invalid Trip Id");
    }

    // NOT NULL
    public void isNotNullTotalOrderForDelivery() {
        Assert.assertNotNull(lmsTripOrderAssignmentResponse.getData());
    }

    public void isNotNullTotalCodAmount() {

    }

    public void isNotNullTripId()
    {
        Assert.assertNotNull(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getTripId(), "Trip ID is null");
    }

    public void isNotNullCreatedOn()
    {
        Assert.assertNotNull(lmsTripOrderAssignmentResponse.getTripOrders().get(0).getCreatedOn(), "Created On Date data doesn't exist");
    }




    // NOT NULL ENDS
    /*BASE MODULAR FUNCTIONS FOR Trip Order Assignment Ends*/


    //Validations

    public void validateGetTripDetailsByTrackingNumber(String tripStatus, String shipmentStatus, String orderId)
    {
        isNotNullCreatedOn();
        isNotNullTripId();
        validateTripOrderStatus(tripStatus);
        validateShipmentType(shipmentStatus);
        validateOrderId(orderId);
    }

    public void validateGetTripOrderByTripId(String tripStatus, String orderId)
    {
        validateTripOrderStatus(tripStatus);
        validateOrderId(orderId);
    }

    public void validateGetTripOrderByTripNumber(String trackingNum, String OrderId, String shippingMethod, String tripOrderStatusValue)
    {
        validateTrackingNum(trackingNum);
        validateOrderId(OrderId);
        validateShippingMethod(shippingMethod);
        validateTripOrderStatusValue(tripOrderStatusValue);
    }


}


