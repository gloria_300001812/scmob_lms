package com.myntra.apiTests.erpservices.lastmile.client;

import com.myntra.apiTests.erpservices.returnComplete.client.BaseClient;
import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.client.DeliveryStaffServiceV2Client;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.scm.utils.EnvironmentUtil;
import org.springframework.http.MediaType;
import org.testng.Assert;
import org.testng.log4testng.Logger;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class DeliveryStaffClient implements BaseClient {
    DeliveryStaffServiceV2Client deliveryStaffServiceClient;
    String tenantId;
    String clientId;
    String serviceURL;
    static Logger log = Logger.getLogger(DeliveryStaffClient.class);

    public DeliveryStaffClient(String env, String clientId, String tenantId) {
        EnvironmentUtil environmentUtil = null;
        try {
            environmentUtil = new EnvironmentUtil();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        try {
            this.serviceURL = environmentUtil.formUrl(env, "lastmile") + "lastmile-service";

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.tenantId = tenantId;
        this.clientId = clientId;

        deliveryStaffServiceClient = new DeliveryStaffServiceV2Client(this.serviceURL, 5, 10);
    }

    public DeliveryStaffResponse findDeliveryStaffByMobNo(String mobile) {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            deliveryStaffResponse = deliveryStaffServiceClient.searchDeliveryStaff(mobile, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lastmile client");
        }
        if (deliveryStaffResponse.getStatus().getStatusMessage().toLowerCase().contains("ERROR".toLowerCase())) {
            Assert.fail("Error while fetchin Delivery staff details");
        }
        try {
            Assert.assertEquals(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile(),mobile,"The delivery staff is not matching with the mobile number");
        } catch (NullPointerException e) {
            Assert.fail("No delivery staff found for the mobile number");
        }
        return deliveryStaffResponse;
    }


    public DeliveryStaffResponse create(DeliveryStaffEntry arg0) throws WebClientException {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            deliveryStaffResponse = deliveryStaffServiceClient.create(arg0, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to create deliveryStaff / check lastmile is up");
        }
        return deliveryStaffResponse;
    }

    public DeliveryStaffResponse update(DeliveryStaffEntry arg0 , long SDA) throws WebClientException {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            deliveryStaffResponse = deliveryStaffServiceClient.update(arg0,SDA, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to update deliveryStaff / check lastmile is up");
        }
        return deliveryStaffResponse;
    }

    public DeliveryStaffResponse findById(final Long arg0) throws WebClientException {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            deliveryStaffResponse = deliveryStaffServiceClient.findById(arg0, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to find deliveryStaff by deliveryStaff Id / check lastmile is up");
        }
        return deliveryStaffResponse;
    }

    public Long searchDeliveryStaffByDeliveryCenterId(long deliveryCenterId) {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            deliveryStaffResponse = deliveryStaffServiceClient.searchDeliveryStaffByDeliveryCenterId(deliveryCenterId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lastmile client");
        }
        return  deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
    }


    public Long findDeliveryStaffByMobNo2(String mobile) {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            deliveryStaffResponse = deliveryStaffServiceClient.searchDeliveryStaff(mobile, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lastmile client");
        }
        if (deliveryStaffResponse.getStatus().getStatusMessage().toLowerCase().contains("ERROR".toLowerCase())) {
            Assert.fail("Error while fetchin Delivery staff details");
        }
        try {
            Assert.assertTrue(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile().equals(mobile), "The delivery staff is not matching with the mobile number");
        } catch (NullPointerException e) {
            Assert.fail("No delivery staff found for the mobile number");
        }
        return deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
    }
//
//    public Long searchDeliveryStaffByDeliveryCenterId2(long deliveryCenterId) {
//        DeliveryStaffResponse deliveryStaffResponse = null;
//        try {
//            deliveryStaffResponse = deliveryStaffServiceClient.searchDeliveryStaffByDeliveryCenterId(deliveryCenterId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
//        } catch (WebClientException e) {
//            e.printStackTrace();
//            Assert.fail("Unable to connect to lastmile client");
//        }
//        return  deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
//    }


    public  DeliveryStaffResponse searchDeliveryStaffbyDeliveryStaffId(long deliveryStaffId){
        DeliveryStaffResponse deliveryStaffResponse = null;
        try{
            deliveryStaffResponse = deliveryStaffServiceClient.findById(deliveryStaffId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        }catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to Connect to Lastmile client");
        }
        return  deliveryStaffResponse;

    }
    public DeliveryStaffResponse searchDeliveryStaffByDeliveryCenterId2(long deliveryCenterId) {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            deliveryStaffResponse = deliveryStaffServiceClient.searchDeliveryStaffByDeliveryCenterId(deliveryCenterId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lastmile client");
        }
        return  deliveryStaffResponse;
    }
}


