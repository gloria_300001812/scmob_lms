package com.myntra.apiTests.erpservices.lastmile.model;

import com.myntra.logistics.platform.domain.Premise;
import lombok.Data;

@Data
public class EventLocationPremise {
    private String premiseId;

    private Premise.PremiseType premisesType;


}
