package com.myntra.apiTests.erpservices.lastmile.helpers;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.lastmile.client.client.TripServiceV2Client;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.scm.utils.EnvironmentUtil;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class LastmileTripResultSearchHelper {

    TripServiceV2Client tripServiceV2Client;
    String tenantId;
    String clientId;
    String serviceURL;


    public LastmileTripResultSearchHelper(String env, String clientId, String tenantId) {
        EnvironmentUtil environmentUtil = null;
        try {
            environmentUtil = new EnvironmentUtil();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        try {
            this.serviceURL = environmentUtil.formUrl(env, "lastmile") + "lastmile-service";

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.tenantId = tenantId;
        this.clientId = clientId;

        tripServiceV2Client = new TripServiceV2Client(this.serviceURL, 5, 10);
    }

    /**
     * this method gives the trip response for given combination of query params
     *
     * @param
     * @return
     */
    public OrderResponse tripResultSerchGetAllFailedFilterSearch(String queryparam) throws UnsupportedEncodingException, JAXBException {
        Svc service = HttpExecutorService.executeHttpService(LASTMILE_CONSTANTS.Trip_Result_ServiceURL.GETALLFAILEDATTEMPTS_LASTMILE, new String[]{queryparam}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        OrderResponse orderResponse = (OrderResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new OrderResponse());
        return orderResponse;
    }

    /**
     * this method gives the trip response for given combination of query params
     *
     * @param
     * @return
     */
    public OrderResponse tripResultSerchgetAllOnHoldOrdersSearch(String queryparam) throws UnsupportedEncodingException, JAXBException {
        Svc service = HttpExecutorService.executeHttpService(LASTMILE_CONSTANTS.Trip_Result_ServiceURL.GETALLONHOLDORDERS_LASTMILE, new String[]{queryparam}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        OrderResponse orderResponse = (OrderResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new OrderResponse());
        return orderResponse;
    }

    /**
     * this method for retrive complete data for given query from database
     *
     * @param
     * @return
     */
    public static List<Map<String, Object>> getDbData(String query) {
        List<Map<String, Object>> tripIdCompleteQuery = DBUtilities.exSelectQuery(query, "lms");
        return tripIdCompleteQuery;
    }

    /**
     * this method for creating the new trip
     *
     * @param
     * @return
     */
    public TripResponse createTrip(long deliveryCenterId, long deliveryStaffId) {
        TripEntry tripEntry = new TripEntry();
        tripEntry.setTripDate(new Date());
        tripEntry.setDeliveryCenterId(deliveryCenterId);
        tripEntry.setDeliveryStaffId(deliveryStaffId);
        tripEntry.setCreatedBy("Automation");
        tripEntry.setIsInbound(false);
        tripEntry.setIsCardEnabled(false);
        TripResponse tripResponse = null;

        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_CREATEVersion2;

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(tripEntry), Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to create trip and connect to lastmile client");
        }
        return tripResponse;
    }


}
