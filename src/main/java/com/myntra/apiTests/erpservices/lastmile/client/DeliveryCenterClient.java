package com.myntra.apiTests.erpservices.lastmile.client;

import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.returnComplete.client.BaseClient;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.client.DeliveryCenterServiceClient;
import com.myntra.lastmile.client.entry.DeliveryCenterEntry;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.scm.utils.EnvironmentUtil;
import org.springframework.http.MediaType;
import org.testng.Assert;
import org.testng.log4testng.Logger;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Gloria.R on 24/10/18.
 */
//TODO : ask dev : all paths are having "//" so not working-fixed in lastmile client 1.6

public class DeliveryCenterClient implements BaseClient {
    DeliveryCenterServiceClient deliveryCenterServiceClient;
    String tenantId;
    String clientId;
    String serviceURL;
    static Logger log = Logger.getLogger(DeliveryCenterClient.class);

    public DeliveryCenterClient(String env, String clientId, String tenantId) {
        EnvironmentUtil environmentUtil = null;
        try {
            environmentUtil = new EnvironmentUtil();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        try {
            this.serviceURL = environmentUtil.formUrl(env, "lastmile") + "lastmile-service";

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.tenantId = tenantId;
        this.clientId = clientId;

        deliveryCenterServiceClient = new DeliveryCenterServiceClient(this.serviceURL, 5, 10);
    }

    public DeliveryCenterResponse createDeliveryCenter(DeliveryCenterEntry deliveryCenterEntry) {
        DeliveryCenterResponse deliveryCenterResponse = null;
        try {
            deliveryCenterResponse = deliveryCenterServiceClient.create(deliveryCenterEntry, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            Assert.fail("Unable to create Delivery Center, unable to call lastmile client");
        }
        Assert.assertNotNull(deliveryCenterResponse);
        return deliveryCenterResponse;
    }

    public DeliveryCenterResponse updateDeliveryCenter(DeliveryCenterEntry deliveryCenterEntry,Long DCId) {
        DeliveryCenterResponse deliveryCenterResponse = null;
        try {
            deliveryCenterResponse = deliveryCenterServiceClient.update(deliveryCenterEntry, DCId , MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            Assert.fail("Unable to update Delivery Center, unable to call lastmile client");
        }
        Assert.assertNotNull(deliveryCenterResponse);
        return deliveryCenterResponse;
    }

    //TODO : to ask,: In client Tenant Id not there, also this gives correct response Address:
    //            http://lastmile.scmqa.myntra.com/lastmile-service/deliveryCenter/search?q=pincode.like:600107&tenantId.eq:4019&start=0&fetchSize=20&sortBy=code&sortOrder=ASC
    // CLient API :http://lastmile.scmqa.myntra.com/lastmile-service/deliveryCenter/search/?q=pincode.eq%3A600107&fetchSize=20&f=&sortOrder=ASC&start=0&distinct=false&sortBy=code
    public DeliveryCenterResponse searchDCByParam(int start, int fetchSize, String sortBy, String sortOrder, Boolean distinct, String q, String f) {
        DeliveryCenterResponse deliveryCenterResponse = null;
        try {
            deliveryCenterResponse = deliveryCenterServiceClient.filteredSearch(start, fetchSize, sortBy, sortOrder, distinct, q, f, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            Assert.fail("Unable to search Delivery Center, unable to call lastmile client");
        }
        Assert.assertNotNull(deliveryCenterResponse);
        return deliveryCenterResponse;
    }

    public long getOriginPremiseIdOfDC(String pincode, String tenantId) {
        String searchParams = "pincode.like:" + pincode + "___tenantId.eq:" + LMS_CONSTANTS.TENANTID;
        DeliveryCenterResponse dcResponse = searchDCByParam(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Long originPremiseId = dcResponse.getDeliveryCenters().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        return originPremiseId;
    }

    public String getCityOfDC(String pincode, String tenantId) {
        String searchParams = "pincode.like:" + pincode + "___tenantId.eq:" + LMS_CONSTANTS.TENANTID;
        DeliveryCenterResponse dcResponse = searchDCByParam(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        log.info("The city  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + dcResponse.getDeliveryCenters().get(0).getCity());
        System.out.println("The city  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + dcResponse.getDeliveryCenters().get(0).getCity());

        return dcResponse.getDeliveryCenters().get(0).getCity();
    }

   /* public TripUpdateDashboardInfoResponse getTripUpdateDashboardInfoV2(){

    }*/
   /* public DeliveryCenterResponse searchWithoutClient(String pincode) {
        LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
        DeliveryCenterResponse DCResponse = null;
        try {
            DCResponse = lmsServiceHelper.getDC("search?q=pincode.like:" + pincode + "&tenantId.eq:" + LMS_CONSTANTS.TENANTID + "&start=0&fetchSize=20&sortBy=code&sortOrder=ASC");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return DCResponse;
    }*/

  /*  public DeliveryCenterResponse createDeliveryCenterWithoutClient(DeliveryCenterEntry deliveryCenterEntry) {
        String payload = null;
        try {
            payload = APIUtilities.getObjectToJSON(deliveryCenterEntry);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Svc service = null;
        try {
            service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.CREATE_DC, null, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        DeliveryCenterResponse response = null;
        try {
            response = (DeliveryCenterResponse) APIUtilities.getJsontoObject(service.getResponseBody() ,new StoreResponse());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
}



