package com.myntra.apiTests.erpservices.lastmile.constants;

import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lms.client.status.PaymentMode;

public class LASTMILE_CONSTANTS {

    public static final Boolean USE_INITIATE_START_TRIP = true;
    public static final String DC_PINCODE = "600107";
    public static final String DC_ROLLING_PINCODE = "600107";
    public static final String DC_EOD_PINCODE = "600107";
    public static final String dateFormat = "yyyy-MM-dd HH:mm:ss";

    public static final int REDIS_DB_INDEX = 4;
    public static final String STORE_PINCODE = "560068";
    public static final String MENSA_OLD_STORE_DC_CODE = "ELC";
    public static final String EOD_DC_CODE = "HSR";
    public static final String ROLLING_DC_CODE = "ELC";
    public static final String EOD_PINCODE = "560102";
    public static final String ROLLING_PINCODE = "560068";
    public static final String STORE_DC_CODE_FG_NOT_ENABLED = "HSR";
    public static final String TENANT_ID = "4019";
    public static final String CLIENT_ID = "2297";
    public static final ReconType DEFAULT_RECON_TYPE = ReconType.EOD_RECON;
    public static final ReconType ROLLING_RECON_TYPE = ReconType.ROLLING_RECON;
    public static final Long MAX_TIMEOUT_VIRTUAL_NUMBER_GEN = 180000l;

    //By :Abhishek Jaiswal
    public static String NORTH_CGH_DCID = "42";
    public static String DELIVERY_HUB_CODE_ELC = "ELC";
    public static String ML_BLR_DCID = "5";
    public static String COURIER_CODE_ML = "ML";
    public static String HSR_ML = "1";
    public static final String WAREHOUSE_36 = "36";
    public static String STATUS_CODE_1003 = "1003";
    public static String STATUS_CODE_803 = "803";
    public static String STATUS_CODE_914 = "914";
    public static String STATUS_CODE_3 = "3";
    public static String STATUS_CODE_813 = "813";
    public static String STATUS_CODE_6661 = "6661";


    public static String TRIP_SUCCESSFUL = "Trip(s) retrieved successfully";
    public static String DEFAULT_STAFF_ID = "3742";
    public static String DELIVERY_CENTER_CODE_ELC = "ELC DC";
    public static String SORT_ORDER_DESC = "DESC";
    public static String SORT_ORDER_ASC = "ASC";
    public static String SORT_BY_ID = "id";
    public static String SORT_BY_TRIPDATE = "tripDate";
    public static String SORT_BY_ZIPCODE = "zipcode";
    public static String SORT_BY_CREATED_ON = "createdOn";
    public static String MOBILE_NUM = "6546565555";
    public static String SORT_BY_MOBILE = "mobile";
    public static String TRIP_ORDER_STATUS_SUCCESSFUL_PICKUP = "Pickup Successful";
    public static String MOBILE_NUMBER_RETURN = "0123456789";
    public static String LAST_MILE_PARTNER = "LAST_MILE_PARTNER";
    public static String SORT_BY_SOURCE_ID = "sourceId";
    public static String SORT_BY_PROMISE_DATE = "promiseDate";
    public static int START_INDEX = 0;
    public static int FETCH_SIZE = 20;
    public static String SUCCESS_STATUS_MESSAGE = "Success";
    public static String FAILURE_STATUS_MESSAGE_NO_TRIP_FOUND = "No trip details found";


    public static String PAYMENT_MODE_COD = PaymentMode.cod.toString();
    public static String PAYMENT_MODE_CARD_ON_DELIVERY = PaymentMode.CARD_ON_DELIVERY.toString();

    public static String SORT_BY_CODE = "code";
    public static String SORT_BY_CONTACTNUM = "contactNumber";
    public static String CONTACT_NO = "contact_no";
    public static String FAILURE_FORCE_UPDATE_MESSAGE = "RMS does not allow this transition - please check for refund/new return/status for returnId";

    //CASH RECON
    public class CASH_RECON_CONSTANTS {
        public static final String CASH_RECON_INSTAKART_ID = "4019";
        public static final String CASH_RECON_MYNTRA_ID = "2297";
        public static final String CASH_RECON_DC_ID = "5";
        public static final String CASH_RECON_EKART_ID = "49394";
        public static final String CASH_RECON_DELHIVERY_ID = "49376";
        public static final String CASH_RECON_EVENT_TYPE_PAYMENT = "PAYMENT";
        public static final String CASH_RECON_EVENT_TYPE_ADDED_TO_REVERSE_BAG = "ADDED_TO_REVERSE_BAG";
        public static final String CASH_RECON_PAYMENT_TYPE_DEBIT_CARD = "DEBIT_CARD";
        public static final String CASH_RECON_EVENT_TYPE_DELIVERED_STATUS_CORRECTION = "DELIVERED_STATUS_CORRECTION";
        public static final String CASH_RECON_DELIVERED_STATUS_CORRECTION = "DELIVERED_STATUS_CORRECTION";
    }

    //Return management
    public static class Lastmile_ReturnManagement_constants {

        public static final Long wait_Time = 3000l;
        public static final String warehouseId = "36";
        public static final String courierType = "ML";
        public static final String openBoxPincode = "560068";
        public static final String closeBoxPincode = "400053";
        public static final String selfShipPincode = "560063";

    }
    public static class Trip_Planning_End_URL{
        public static final String DCID_SDAID_END_URL = "?q=deliveryCenterId.eq:{0}___deliveryStaffId.eq:{1}___tenantId.eq:{2}&sortBy=id&sortOrder=ASE";
        public static final String DCID_TRIPSTATUS_ASOFD_END_URL = "?q=deliveryCenterId.eq:{0}___showAllDeliveryStaff.eq:{1}___tripStatus.eq:{2}___tenantId.eq:{3}";
        public static final String DCID_TRIPSTATUS_AS_CREATED_END_URL = "?q=deliveryCenterId.eq:{0}___showAllDeliveryStaff.eq:{1}___tripStatus.eq:{2}___tenantId.eq:{3}";
        public static final String DCID_TRIPSTATUS_AS_COMPLETED_END_URL = "?q=deliveryCenterId.eq:{0}___tripStatus.eq:{1}";
        public static final String DCID_SHOW_DELIVERY_STAFF_TRUE_IS_CARD_ENABLED_BOTH_END_URL = "?q=deliveryCenterId.eq:{0}___showAllDeliveryStaff.eq:{1}___isDeleted.eq:{2}___isInbound.eq:{3}";
        public static final String DCID_SHOW_DELIVERY_STAFF_TRUE_IS_DELETED_NO_END_URL = "?q=deliveryCenterId.eq:{0}___showAllDeliveryStaff.eq:{1}___isDeleted.eq:{2}";
        public static final String DCID_SHOW_DELIVERY_STAFF_TRUE_IS_DELETED_YES_END_URL = "?q=deliveryCenterId.eq:{0}___showAllDeliveryStaff.eq:{1}___isDeleted.eq:{2}___tenantId.eq:{3}";
        public static final String DCID_CREATED_ON_DATE_TRIP_START_TRIP_END_DATE_END_URL = "?q=deliveryCenterId.eq:{0}___showAllDeliveryStaff.eq:{1}___tripStatus.eq:{2}___createdOn.ge:{3}___" +
                "createdOn.le:{4}___tripStartTime.ge:{5}___tripStartTime.le:{6}___tripEndTime.ge:{7}___tripEndTime.le:{8}";
        public static final String SEARCH_WITH_ALLFIELDS_END_URL="?q=deliveryCenterId.eq:{0}___showAllDeliveryStaff.eq:{1}___tripNumber.in:{2}___tripStatus.eq:{3}___isDeleted.eq:{4}___" +
                "isInbound.eq:{5}___tenantId.eq:{6}&start=0&fetchSize=20&sortBy=id&sortOrder=DESC";
    }


    public static class Trip_Result_End_URL{
        public static final String TRIP_RESULT_INVALID_DATE_RANGE_END_URL="?tenantId={0}&shipmentType={1}&startDate={2}&endDate={3}&start=0&sortBy=createdOn&dir=DESC";
        public static final String DCID_CREATED_DATE_ONHOLD_PICKUP_API_END_URL = "?tenantId={0}&shipmentType={1}&startDate={2}&endDate={3}";
        public static final String DCID_AND_CREATED_DATE_FAILED_PICKUP_API_END_URL = "?tenantId={0}&shipmentType={1}&startDate={2}&endDate={3}&start=0&sortBy=createdOn&limit=100&dir=DESC";
        public static final String DCID_AND_CREATED_DATE_FAILED_DELIVERY_API_END_URL = "?tenantId={0}&shipmentType={1}&startDate={2}&endDate={3}&start=0&sortBy=createdOn&dir=DESC";
        public static final String DCID_AND_CREATED_DATE_TRY_AND_BUY_API_END_URL = "?tenantId={0}&shipmentType={1}&startDate={2}&endDate={3}&start=0&sortBy=createdOn&dir=DESC";
        public static final String DCID_AND_CREATED_DATE_FAILED_EXCHANGE_API_END_URL = "?tenantId={0}&shipmentType={1}&startDate={2}&endDate={3}&start=0&sortBy=createdOn&dir=DESC";


    }

    public static class Trip_Planning_ServiceURL{
        public static final String FILTER_SEARCH_LASTMILE="trip/v2/filteredSearch";
    }

    public static class Trip_Result_ServiceURL{
        public static final String GETALLFAILEDATTEMPTS_LASTMILE="mlShipmentService/v2/getAllFailedAttempts/deliveryCenter/5";
        public static final String GETALLONHOLDORDERS_LASTMILE="mlShipmentService/v2/getAllOnHoldOrders/deliveryCenter/5";

    }


}
