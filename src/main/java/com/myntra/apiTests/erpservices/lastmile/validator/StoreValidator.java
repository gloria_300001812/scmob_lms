package com.myntra.apiTests.erpservices.lastmile.validator;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

import org.testng.Assert;

import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.ShipmentStatus;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;

public class StoreValidator {


   // @Step("validate given variable is null or not")
    public void isNull(String variable){
        if(variable==null){
            Assert.fail("variable value is null");
        }
    }

   // @Step("Validate Master Bag status")
    public void validateMasterBagStatus(ShipmentResponse shipmentResponse,ShipmentStatus expectedStatus) throws InterruptedException {
        if(!(shipmentResponse.getEntries().isEmpty()) ){
            Assert.assertEquals(shipmentResponse.getEntries().get(0).getStatus(),expectedStatus, "MasterBag status is not expected");
        }else{
            Assert.fail("Shipment entries was empty");
        }
    }

   // @Step("Validate ML shipment status for Dc and Store")
    public void validateMLShipmentStatus(MLShipmentResponse mlShipmentResponse, String expectedStatus) throws InterruptedException {
        if (!(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus()==null)){
            Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(),expectedStatus,"Ml shipment status is not matching");
        }else {
            Assert.fail("MLShipment Response was null");
        }
    }

   // @Step("Validate order status")
    public void validateOrderStatus(OrderResponse orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus expectedStatus){
        if (!(orderResponse.getOrders().get(0).getPlatformShipmentStatus()==null)){
            Assert.assertEquals(orderResponse.getOrders().get(0).getPlatformShipmentStatus().toString(),expectedStatus.toString(),"Order status is not expected" );
        }else {
            Assert.fail("Order response was null");
        }
    }

   // @Step("validate trip status")
    public void validateTripStatus(TripResponse tripResponse, TripStatus tripStatus){
        if (!(tripResponse.getTrips().get(0).getTripStatus()==null)){
            Assert.assertEquals(tripResponse.getTrips().get(0).getTripStatus().toString(),tripStatus.toString(),"Trip status after trip order assignment not correct.");
        }else {
            Assert.fail("Trip response was null");
        }

    }

   // @Step("Validate return order status in RMS")
    public void validateReturnOrderStatusInRMS(ReturnResponse returnResponse, ReturnStatus returnStatus){
        if (!(returnResponse.getData().get(0).getStatusCode()==null)){
            Assert.assertEquals(returnResponse.getData().get(0).getStatusCode().toString(),returnStatus.toString(),"Return order status is not expected");
        }else {
            Assert.fail("Return response was null");
        }

    }

   // @Step("Validate return order status in LMS")
    public void validateReturnOrderStatusInLMS(com.myntra.lms.client.domain.response.ReturnResponse returnResponse, com.myntra.lastmile.client.status.ShipmentStatus returnStatus){
        if (!(returnResponse.getDomainReturnShipments().get(0).getShipmentStatus()==null)){
            Assert.assertEquals(returnResponse.getDomainReturnShipments().get(0).getShipmentStatus().toString(),returnStatus.toString(),"Return order status is not expected");
        }else {
            Assert.fail("Return response was null");
        }
    }

   // @Step("Validate pickup shipment status in LMS")
    public void validatePickupShipmentStatusInLMS(String pickupResponse,com.myntra.logistics.platform.domain.ShipmentStatus expectedStatus){
       if( !(pickupResponse==null)){
            Assert.assertEquals(pickupResponse,expectedStatus.toString(),"Pickup order status is not expected ");
       }else {
           Assert.fail("Pickup response was null");
       }

    }


}
