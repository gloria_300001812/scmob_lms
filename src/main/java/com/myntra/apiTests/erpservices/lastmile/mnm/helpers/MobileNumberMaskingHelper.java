package com.myntra.apiTests.erpservices.lastmile.mnm.helpers;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.DB.LastMileDAO;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_ReturnHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.VirtualNumberAllocationStatus;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.VirtualNumberResponse;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.ItemEntry;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.logistics.platform.domain.TryAndBuyItemStatus;
import com.myntra.logistics.platform.domain.TryAndBuyNotBoughtReason;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Mar-2019
 */
public class MobileNumberMaskingHelper {
    static String env = getEnvironment();
    public static HashMap<String, String> PincodeDCMap;
    static DeliveryCenterClient_QA deliveryCenterClient_qa = new DeliveryCenterClient_QA();
    TripClient_QA tripClient_qa = new TripClient_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa = new TripOrderAssignmentClient_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa = new MLShipmentClientV2_QA();
    static LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    MobileNumberMaskingValidator mobileNumberMaskingValidator = new MobileNumberMaskingValidator();
    LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();

    public static String deliveryCenterId;

    static {
        pincodeDcMapping();
        deliveryCenterId = PincodeDCMap.get(LMS_PINCODE.ML_BLR);
    }

    public MobileNumberMaskingHelper() {
    }

    //creating HashMap to maintain pincode and deliveryCenterId
    private static void pincodeDcMapping() {
        PincodeDCMap = new HashMap<>();
        PincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        PincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        PincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
    }

    //Create/Get SDA
    public String getDeliveryStaffID(String deliveryCenterId) throws Exception {
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(deliveryCenterId));
        return String.valueOf(deliveryStaffID);
    }

    public String createMockOrderAndGetTrackingNumber() throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", LASTMILE_CONSTANTS.WAREHOUSE_36, EnumSCM.NORMAL, "cod", false, true));
        return lmsHelper.getTrackingNumber(packetId);
    }

    public Map<String, String> createMockOrderAndAssignShipmentToTrip() throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        Map<String, String> orderMap = new HashMap<>();
        Map<String, String> tripDetails = null;
        String trackingNumber;
        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", LASTMILE_CONSTANTS.WAREHOUSE_36, EnumSCM.NORMAL, "cod", false, true));
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        deliveryCenterId = PincodeDCMap.get(LMS_PINCODE.ML_BLR);
        tripDetails = createTrip(deliveryCenterId);
        long tripId = Long.parseLong(tripDetails.get("tripId"));
        String tripNumber = tripDetails.get("tripNumber");
        System.out.println("trackingNumber="+trackingNumber);
        System.out.println("tripNumber="+tripNumber);
        assignOrderToTrip(packetId, tripId);
        orderMap.put("tripId", String.valueOf(tripId));
        orderMap.put("packetId", packetId);
        orderMap.put("trackingNumber", trackingNumber);
        orderMap.put("tripNumber", tripNumber);
        return orderMap;
    }

    public Map<String, Object> createMockOrderMultipleShipments(Integer numberOfShipments, String toShipmentStatus, boolean isTryAndBuy) throws Exception {
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        Map<String, Object> orderMap = new HashMap<>();
        List<String> trackingNumbers = new ArrayList<>();
        List<String> packetIds = new ArrayList<>();
        List<String> orderIds = new ArrayList<>();
        deliveryCenterId = PincodeDCMap.get(LMS_PINCODE.ML_BLR);
        TripResponse tripAssignmentResponse;

        //Create packets
        for (int i = 0; i < numberOfShipments; i++) {
            String orderId = lmsHelper.createMockOrder(toShipmentStatus, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, EnumSCM.NORMAL, "cod", isTryAndBuy, true);
            String packetId = omsServiceHelper.getPacketId(orderId);
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);
            orderIds.add(orderId);
            trackingNumbers.add(trackingNumber);
            packetIds.add(packetId);
            if(toShipmentStatus.equalsIgnoreCase(EnumSCM.SH)) {
                tripAssignmentResponse = tripClient_qa.getActiveTripForOrder(packetId, LASTMILE_CONSTANTS.TENANT_ID);
                Assert.assertEquals(tripAssignmentResponse.getStatus().getTotalCount(), 0, "Trip is active without assignment");
            }
        }

        orderMap.put("orderIds", orderIds);
        orderMap.put("packetIds", packetIds);
        orderMap.put("trackingNumbers", trackingNumbers);
        return orderMap;
    }


    public Map<String, Object> createMockExchangeOrderMultipleShipments(List<String> orderIds) throws Exception {
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        Map<String, Object> exchangeOrderMap = new HashMap<>();
        List<String> trackingNumbers = new ArrayList<>();
        List<String> packetIds = new ArrayList<>();
        List<String> exchangeOrderIds = new ArrayList<>();
        deliveryCenterId = PincodeDCMap.get(LMS_PINCODE.ML_BLR);
        TripResponse tripAssignmentResponse;

        //Create packets
        for (String orderID : orderIds) {
            String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
            String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrder);
            String exTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

            exchangeOrderIds.add(exchangeOrder);
            packetIds.add(exchangePacketId);
            trackingNumbers.add(exTrackingNumber);
        }

        exchangeOrderMap.put("exchangeOrderIds", exchangeOrderIds);
        exchangeOrderMap.put("exPacketIds", packetIds);
        exchangeOrderMap.put("trackingNumbers", trackingNumbers);
        return exchangeOrderMap;
    }

    public Map<String, Object> createMockReturnOrderMultipleShipments(List<String> orderIds) throws Exception {
        Map<String, Object> returnOrderMap = new HashMap<>();
        RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
        LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
        List<String> forwardDLTrackingNumbers = new ArrayList<>();
        List<String> returnTrackingNumbers = new ArrayList<>();
        List<String> returnPacketIds = new ArrayList<>();
        String forwardDLTrackingNumber , returnTrackingNumber, returnPacketId;
        for(String orderID : orderIds) {
            OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
            OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
            ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
            Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
            Long returnID = returnResponse.getData().get(0).getId();
            String returnOrderID = String.valueOf(returnResponse.getData().get(0).getOrderId());
            returnPacketId = omsServiceHelper.getPacketId(returnOrderID);
            forwardDLTrackingNumber = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));

            ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
            lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
            //Manifest return order
            lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));
            OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
            returnTrackingNumber = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
            forwardDLTrackingNumbers.add(forwardDLTrackingNumber);
            returnTrackingNumbers.add(returnTrackingNumber);
            returnPacketIds.add(returnPacketId);
            System.out.println("forwardDLTrackingNumber=" + forwardDLTrackingNumber + " - returnTrackingNumber=" + returnTrackingNumber);
        }

        returnOrderMap.put("forwardDLTrackingNumbers" , forwardDLTrackingNumbers);
        returnOrderMap.put("returnTrackingNumbers" , returnTrackingNumbers);
        returnOrderMap.put("returnPacketIds" , returnPacketIds);
        return returnOrderMap;
    }

    /**
     * All Trip related operations
     */

    public Map<String, String> createTrip(String deliveryCenterId) throws Exception {
        Map<String, String> tripDetails = new HashMap<>();
        //Create Trip
        TripResponse tripResponse;
        String deliveryStaffID = getDeliveryStaffID(deliveryCenterId);
        tripResponse = tripClient_qa.createTrip(Long.parseLong(deliveryCenterId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        mobileNumberMaskingValidator.validateTripStatus(tripResponse, EnumSCM.CREATED);
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        tripDetails.put("tripId", String.valueOf(tripId));
        tripDetails.put("tripNumber", tripNumber);
        tripDetails.put("deliveryStaffID", deliveryStaffID);
        return tripDetails;
    }

    public OrderEntry setOperationalTrackingId(String trackingNumber){
        OrderEntry orderEntry = new OrderEntry();
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        return orderEntry;
    }

    public List<OrderEntry> setOperationalTrackingIds(List<String> trackingNumbers){
        List<OrderEntry> orderEntries = new ArrayList<>();
        OrderEntry orderEntry;
        for(String trackingNumber : trackingNumbers) {
            orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            orderEntries.add(orderEntry);
        }
        return orderEntries;
    }

    public void assignMultipleOrdersToTrip(List<String> packetIds, long tripId) throws WebClientException {
        String trackingNumber;
        for(String packetId : packetIds) {
            trackingNumber = lmsHelper.getTrackingNumber(packetId);
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
            TripResponse tripAssignmentResponse1 = tripClient_qa.getActiveTripForOrder(packetId, LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
            Assert.assertNotNull(tripAssignmentResponse1.getTrips().get(0).getId());
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        }
    }

    private void assignOrderToTrip(String packetId, long tripId) throws WebClientException {
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        TripResponse tripAssignmentResponse1 = tripClient_qa.getActiveTripForOrder(packetId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(tripAssignmentResponse1.getTrips().get(0).getId());
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
    }

    public void scanTrackingNumberInTrip(String tripId, String trackingNumber) throws IOException, JAXBException {
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(Long.valueOf(tripId), trackingNumber);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
    }

    public void scanTrackingNumbersInTrip(String tripId, List<String> trackingNumbers) throws IOException, JAXBException {
        for(String trackingNumber : trackingNumbers){
            scanTrackingNumberInTrip(tripId, trackingNumber);
        }
    }

    public TripResponse startTrip(String tripId, String packetId) throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        Assert.assertEquals(tripClient_qa.initiateStartTripForMNM(Long.valueOf(tripId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to perform startTrip");
        TripResponse tripAssignmentResponse = tripClient_qa.getActiveTripForOrder(packetId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(tripAssignmentResponse.getTrips().get(0).getId());
        return tripAssignmentResponse;
    }

    public List<TripOrderAssignmentResponse> startTrip(String tripId, List<String> trackingNumbers) throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses = new ArrayList<>();
        Assert.assertEquals(tripClient_qa.initiateStartTripForMNM(Long.valueOf(tripId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to perform startTrip");
        for(String trackingNumber : trackingNumbers) {
            TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.getTripDetails(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS , tripOrderAssignmentResponse.getStatus().getStatusMessage());
            Assert.assertNotNull(tripOrderAssignmentResponse.getTripOrders().get(0).getId());
            tripOrderAssignmentResponses.add(tripOrderAssignmentResponse);
        }
        return tripOrderAssignmentResponses;
    }

    //Make it delivery trip
    public void updateDeliveryTrip(String tripId, String packetId) throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(3000);
    }

    public void updateDeliveryTrip(String tripId, List<String> packetIds) throws Exception {
        for(String packetId : packetIds){
            updateDeliveryTrip(tripId, packetId);
        }
    }

    public void updateExchangeTrip(String tripId,  String packetId , OrderEntry orderEntry) throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        String exchangeOrderId;
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId ,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, Long.valueOf(tripId), orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
    }

    public void updateExchangeTrip(String tripId, List<String> packetIds, List<OrderEntry> orderEntries) throws Exception {
        int orderEntryIndex =0;
        for(String packetId : packetIds){
            updateExchangeTrip(tripId, packetId, orderEntries.get(orderEntryIndex));
            ++orderEntryIndex;
        }
    }

    public void updatePickupTrip(String tripId , String returnTrackingNumber, String tripOrderAssignmentId) throws Exception {
        TripOrderAssignmentResponse updateTripResponse = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
        Assert.assertEquals(updateTripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
    }

    public void updatePickupTrip(String tripId , List<String> returnTrackingNumbers) throws Exception {
        List<String> tripOrderAssignmentIds = getPickupTripOrderAssignmentId(tripId);
        int index =0;
        for(String returnTrackingNumber : returnTrackingNumbers){
            updatePickupTrip(tripId, returnTrackingNumber , tripOrderAssignmentIds.get(index));
            index++;
        }
    }

    public List<String> updateTryAndBuyTrip(String tripId , List<String> trackingNumbers , List<String> packetIds, List<String> orderIds , TryAndBuyItemStatus tryAndBuyItemStatus) throws Exception {
        OrderEntry orderEntry;
        ItemEntry itemEntry;
        Integer packetIdIndex =0;
        Integer trackingNumberIndex =0;
        String packetId , trackingNumber;
        String mlShipmentId , returnTrackingNumber;
        List<String> returnTrackingNumbers = new ArrayList<>();
        for(String orderID : orderIds) {
            orderEntry = new OrderEntry();
            itemEntry = new ItemEntry();
            packetId = packetIds.get(packetIdIndex);
            trackingNumber = trackingNumbers.get(trackingNumberIndex);
            long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
            OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);

            com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
            Double amount = orderEntryDetails.getFinalAmount();
            Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            orderEntry.setOrderId(packetId);
            orderEntry.setDeliveryCenterId(Long.valueOf(deliveryCenterId));
            orderEntry.setCodAmount(amount);
            itemEntry.setId(Id);
            itemEntry.setStatus(tryAndBuyItemStatus);
            itemEntry.setRemarks(tryAndBuyItemStatus.toString());
            if(tryAndBuyItemStatus== TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT){
                itemEntry.setQcStatus(ItemQCStatus.PASSED);
                itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
            }
            List<ItemEntry> entries = new ArrayList<>();
            entries.add(itemEntry);
            orderEntry.setItemEntries(entries);
            TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, Long.valueOf(tripId), orderEntry);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            if(tryAndBuyItemStatus== TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT) {
                mlShipmentId = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
                returnTrackingNumber = lmsHelper.updateOperationalTrackingNumForTryAndBuy(mlShipmentId);
                returnTrackingNumbers.add(returnTrackingNumber);
            }
            packetIdIndex++;
            trackingNumberIndex++;
        }
        return returnTrackingNumbers;
    }

    public List<String> getPickupTripOrderAssignmentId(String tripId){
        List<String> toaIds = new ArrayList<>();
        List<Map<String, String>> toaIdResultSet = DBUtilities.exSelectQuery("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        for(Map record : toaIdResultSet) {
            toaIds.add(record.get("id").toString());
        }
        return toaIds;
    }

    public void recieveTripOrder(String tripId,String trackingNumber) throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse;
        tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNumber), EnumSCM.PICKUP_SUCCESSFUL, "shipment status mismatch");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(getPickupTripOrderAssignmentId(tripId).get(0)), "true", "Wrong status in trip_order_assignment");
    }

    public void recieveTripOrders(String tripId, List<String> trackingNumbers) throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse;
        List<String>  tripOrderAssignmentIds = getPickupTripOrderAssignmentId(tripId);
        int index =0;
        for(String trackingNumber : trackingNumbers) {
            tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive "+tripOrderAssignmentResponse.getStatus().getStatusMessage());
            Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNumber), EnumSCM.PICKUP_SUCCESSFUL, "shipment status mismatch");
            Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentIds.get(index)), "true", "Wrong status in trip_order_assignment");
            index++;
        }
    }

    public void recieveExchangeTripOrders(List<String> trackingNumbers) throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse;
        for(String trackingNumber : trackingNumbers) {
            tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        }
    }

    public void receiveTryAndBuyTripOrder(String returnTrackingNumber) throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(returnTrackingNumber.substring(2, returnTrackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        //TODO uncommet it later, this validation is needed
        //Long tripOrderAssignmentIdForReturn = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(returnTrackingNumber);
        //Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForReturn)), "true", "Wrong status in trip_order_assignment");
        //Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(returnTrackingNumber), EnumSCM.PICKUP_SUCCESSFUL, "Status not updated to received in DC");
    }

    public void receiveTryAndBuyTripOrders(List<String> returnTrackingNumbers) throws Exception {
        for(String returnTrackingNumber : returnTrackingNumbers){
            receiveTryAndBuyTripOrder(returnTrackingNumber);
        }
    }

    public void completeTrip(String tripId, String packetId) throws Exception {
        Long tripOrderAssignmentId = (long)lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId , EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    public static String getDeliveryStaffMobileNumber(String deliveryStafId) throws Exception {
        DeliveryStaffResponse deliveryStaffResponse = lmsServiceHelper.deliveryStaff(deliveryStafId);
        return deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile();
    }

    /**
     * Update all the shipments with DELIVERED and Complete the trip
     */
    public void completeTrip(String tripId, List<String> packetIds) throws Exception {
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap;
        for(String packetId : packetIds){
            dataMap = new HashMap<>();
            dataMap.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
            dataMap.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
            tripData.add(dataMap);
        }
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        for(String packetId : packetIds) {
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(packetId, EnumSCM.DELIVERED, 4));
            Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(packetId, EnumSCM.D, 4));
        }
    }

    /**
     * Update all the shipments with DELIVERED and Complete the trip
     */
    public void completeExchangeTrip(String tripId, List<String> packetIds, List<String> exchangeOrders) throws Exception {
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap;
        int exOrderIndex =0;
        for(String packetId : packetIds){
            dataMap = new HashMap<>();
            dataMap.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
            dataMap.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
            dataMap.put("exchangeOrderId", exchangeOrders.get(exOrderIndex));
            exOrderIndex++;
            tripData.add(dataMap);
        }
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        for(String packetId : packetIds) {
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(packetId, EnumSCM.DELIVERED, 4));
            Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(packetId, EnumSCM.D, 4));
        }
    }

    public void completeExchangeTrip(String tripId, String packetId, OrderEntry orderEntry) throws Exception {
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, Long.valueOf(tripId), orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    /**
     * Update all the shipments with DELIVERED and Complete the trip

     public void completeExchangeTrip(String tripId, List<String> packetIds, List<OrderEntry> orderEntries) throws Exception {
     int exOrderEntryIndex =0;
     for(String packetId : packetIds){
     completeExchangeTrip(tripId, packetId, orderEntries.get(exOrderEntryIndex));
     }
     }*/

    public void completeReturnTrip(String tripId) throws Exception {
        long tripOrderAssignmentId = Long.valueOf(getPickupTripOrderAssignmentId(tripId).get(0));
        TripOrderAssignmentResponse updatePickupInTripResponse = lmsServiceHelper.updatePickupInTrip(tripOrderAssignmentId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(updatePickupInTripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
    }

    /**
     * Virtual Number related helpers
     */

    public Map<String, String> generateVirtualNumberByTrackingId(String trackingNumber) throws Exception {
        Map<String, String> postedVNentries = new HashMap<>();
        LastmileServiceHelper lastmileServiceHelper = new LastmileServiceHelper();
        String event = "LMS_MASKING";
        String clientReferenceId;
        String connectionId = String.valueOf(new Random().nextInt(9999));
        String virtualNumber = LastMileDAO.GenerateRandomPhoneNumber();
        String partyOnePins = String.valueOf(new Random().nextInt(9999));
        String partyTwoPins = String.valueOf(new Random().nextInt(9999));
        clientReferenceId = LastMileDAO.GetVirtualNumberRequestIdDB(trackingNumber);
        //Update connectionId upfront before generation VN
        LastMileDAO.updateCallBridgeConnectionId(trackingNumber, connectionId);
        VirtualNumberResponse virtualNumberResponse = lastmileServiceHelper.genrateVirtualNumbersAndPin(event, clientReferenceId, connectionId, virtualNumber, partyOnePins, partyTwoPins);
        mobileNumberMaskingValidator.validateVirtualNumberResponseBasic(virtualNumberResponse);
        postedVNentries.put("event", event);
        postedVNentries.put("clientReferenceId", clientReferenceId);
        postedVNentries.put("connectionId", connectionId);
        postedVNentries.put("virtualNumber", virtualNumber);
        postedVNentries.put("partyOnePins", partyOnePins);
        postedVNentries.put("partyTwoPins", partyTwoPins);
        return postedVNentries;
    }

    public Map<String, String> generateVirtualNumberAndValidate(String trackingNumber) throws Exception {
        Map<String, String> postedVNentries;
        postedVNentries = generateVirtualNumberByTrackingId(trackingNumber);
        //check generated Virtual number using API and verify with DB
        mobileNumberMaskingValidator.validateVirtualNumberEntriesDB(trackingNumber, postedVNentries);
        mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
        return postedVNentries;
    }

    public void generateVirtualNumberAndValidate(List<String> trackingNumbers) throws Exception {
        for (String trackingNumber : trackingNumbers) {
            generateVirtualNumberAndValidate(trackingNumber);
        }
    }

}