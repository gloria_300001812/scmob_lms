package com.myntra.apiTests.erpservices.lastmile.client;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.mnm.helpers.MobileNumberMaskingHelper;
import com.myntra.apiTests.erpservices.lastmile.service.LastmileServiceHelper;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.BaseClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.client.TripServiceV2Client;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.DeliveryStaffType;
import com.myntra.lastmile.client.code.utils.TripAction;
import com.myntra.lastmile.client.code.utils.UpdatedVia;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.entry.TripOrderAssignementEntry;
import com.myntra.lastmile.client.entry.TripShipmentAssociationEntry;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.masterbag.core.MasterbagShipment;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.scm.utils.EnvironmentUtil;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.codehaus.jettison.json.JSONException;
import org.springframework.http.MediaType;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class TripClient implements BaseClient {
    TripServiceV2Client tripServiceV2Client;
    String tenantId;
    String clientId;
    String serviceURL;
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    String env = getEnvironment();

    private MasterBagClient masterBagClient=new MasterBagClient(env,LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    public TripClient(String env, String clientId, String tenantId) {
        EnvironmentUtil environmentUtil = null;
        try {
            environmentUtil = new EnvironmentUtil();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        try {
            this.serviceURL = environmentUtil.formUrl(env, "lastmile") + "lastmile-service";

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.tenantId = tenantId;
        this.clientId = clientId;

        tripServiceV2Client = new TripServiceV2Client(this.serviceURL, 5, 10);
    }

    public TripResponse createTrip(long deliveryCenterId, long deliveryStaffId) {
        TripEntry tripEntry = new TripEntry();
        tripEntry.setTripDate(new Date());
        tripEntry.setDeliveryCenterId(deliveryCenterId);
        tripEntry.setDeliveryStaffId(deliveryStaffId);
        tripEntry.setCreatedBy("Automation");
        tripEntry.setIsInbound(false);
        tripEntry.setTenantId(tenantId);
        tripEntry.setIsCardEnabled(false);
        TripResponse tripResponse = null;

        try {
            tripResponse = tripServiceV2Client.createByRouteTripMap(tripEntry, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to create trip and connect to lastmile client");
        }
        return tripResponse;
    }

    public TripShipmentAssociationResponse addStoreBagToTrip(String tripId, String storeMBId) {

        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        TripShipmentAssociationResponse response = new TripShipmentAssociationResponse();
        ArrayList<TripShipmentAssociationEntry> tripShipmentAssociationEntry = new ArrayList<>();

        TripShipmentAssociationEntry entry = new TripShipmentAssociationEntry();
        entry.setTripId(tripId);
        entry.setShipmentId(storeMBId);
        entry.setShipmentType(ShipmentType.FORWARD_BAG);
        tripShipmentAssociationEntry.add(entry);

        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntry);
        try {
            response = tripServiceV2Client.assignMasterbagToTrip(tripShipmentAssociationResponse, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to add the store bag to trip, unable to connect to lastmile client");
        }
        if (response.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.fail("Unable to add masterBag : " + storeMBId + " to trip - " + tripId + " .Reason : " + response.getStatus().getStatusMessage());
        }
        return response;
    }

    public TripOrderAssignmentResponse startTrip(Long tripId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            TripOrderAssignmentResponse tripOrderAssignmentResponse1=lmsServiceHelper.getTripOrderDetails(tripId);
            List<TripOrderAssignementEntry> tripOrderAssignementEntries=tripOrderAssignmentResponse1.getTripOrders();
            for(int i=0;i<=tripOrderAssignementEntries.size()-1;i++){
                MobileNumberMaskingHelper mobileNumberMaskingHelper=new MobileNumberMaskingHelper();
                mobileNumberMaskingHelper.generateVirtualNumberByTrackingId(tripOrderAssignementEntries.get(i).getTrackingNumber());
            }
            tripOrderAssignmentResponse = tripServiceV2Client.startTrip(tripId, null, "gloria", 12l, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 15000, 15000, 3, Context.getContextInfo());
        } catch (WebClientException e1) {
            e1.printStackTrace();
            Assert.fail("Unable to connect to lastmile client and start the trip");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equals("Trip updated successfully"), "Start Trip failed of failure ");

        return tripOrderAssignmentResponse;
    }

    /**
     * Start Trip API has been changed after introducing mobile number masking.
    // * @param tripId
     * @return TripOrderAssignmentResponse
     * Assertion not required. Since this API behaviour is quite different as status message changes time to time (initial=PROCESSING, after 2mins=OFD)
     */
//    public TripOrderAssignmentResponse initiateStartTrip(String tripId) {
//        LastmileServiceHelper lastmileServiceHelper = new LastmileServiceHelper();
//        TripOrderAssignmentResponse tripOrderResponse = null;
//        try {
//            tripOrderResponse = lastmileServiceHelper.initiateStartTrip(tripId);
//        } catch (Exception e) {
//            e.printStackTrace();
//            Assert.fail("Unable to connect to lastmile client and start the trip");
//        }
//        return tripOrderResponse;
//    }

    public TripResponse searchTripByParam(int start, int fetchSize, String sortBy, String sortOrder, Boolean distinct, String q, String f) {

        TripResponse tripResponse = null;
        try {
            tripResponse = tripServiceV2Client.filteredSearch(start, fetchSize, sortBy, sortOrder, distinct, q, f, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        if (tripResponse.getStatus().getStatusMessage().equals("Unable to filter, so returning non filtered results.null"))
            Assert.fail("Unable to search trip: ");
        return tripResponse;

    }

    public TripResponse searchByTripId(long tripId) {
        TripResponse tripResponse = null;
        try {
            tripResponse = tripServiceV2Client.findById(tripId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        if (tripResponse.getStatus().getStatusMessage().equals("Unable to searchTrip by tripId"))
            Assert.fail("Unable to search trip by TripId: "+tripId);
        return tripResponse;

    }

    public TripShipmentAssociationResponse deliverStoreBagToStore(String masterBagId, List<String> trackingNumberList, String tripId, AttemptReasonCode attemptReasonCode) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();
        for (String trackingNumber : trackingNumberList) {
            MasterbagShipment shipmentItem = new MasterbagShipment();
            shipmentItem.setShipmentType(ShipmentType.DL);
            shipmentItem.setTrackingNumber(trackingNumber);
            shipmentItemList.add(shipmentItem);

        }
        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.FORWARD_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.DL);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            response = tripServiceV2Client.updateTripShipments(tripShipmentAssociationResponse, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();

        }
        return response;
    }

    public TripShipmentAssociationResponse deliverMensaOldStoreBagToStore(String masterBagId, String tripId, AttemptReasonCode attemptReasonCode) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();

        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.FORWARD_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.DL);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            response = tripServiceV2Client.updateTripShipments(tripShipmentAssociationResponse, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();

        }
        return response;
    }

    public TripShipmentAssociationResponse FaileddeliverStoreBagToStore(String masterBagId, List<String> trackingNumberList, String tripId, AttemptReasonCode attemptReasonCode) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();
        for (String trackingNumber : trackingNumberList) {
            MasterbagShipment shipmentItem = new MasterbagShipment();
            shipmentItem.setShipmentType(ShipmentType.DL);
            shipmentItem.setTrackingNumber(trackingNumber);
            shipmentItemList.add(shipmentItem);

        }
        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.FORWARD_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.FD);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            response = tripServiceV2Client.updateTripShipments(tripShipmentAssociationResponse, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
            if (response.getStatus().getStatusType().toString().equals("ERROR"))
                Assert.fail("Error while delivering the Store bag to the Store . Reason : " + response.getStatus().getStatusMessage());
        } catch (WebClientException e) {
            e.printStackTrace();

        }
        return response;
    }

    public TripShipmentAssociationResponse pickupReverseBagFromStore(String masterBagId, List<String> trackingNumberList, String tripId, AttemptReasonCode attemptReasonCode, int totalFailedDeliveriesPicked, int totalSuccessfulReturnsPicked, float amountCollected) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();
        for (String trackingNumber : trackingNumberList) {
            MasterbagShipment shipmentItem = new MasterbagShipment();
            shipmentItem.setShipmentType(ShipmentType.DL);
            shipmentItem.setTrackingNumber(trackingNumber);
            shipmentItemList.add(shipmentItem);


        }
        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.REVERSE_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.PS);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);

        //Specific to reverseBag
        tripShipmentAssociationEntry.setTotalFailedDeliveriesPicked(totalFailedDeliveriesPicked);
        tripShipmentAssociationEntry.setTotalSuccessfulReturnsPicked(totalSuccessfulReturnsPicked);
        tripShipmentAssociationEntry.setAmountCollected(amountCollected);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            response = tripServiceV2Client.updateTripShipments(tripShipmentAssociationResponse, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
            if (response.getStatus().getStatusType().toString().equals("ERROR"))
                Assert.fail("Error while delivering the Store bag to the Store . Reason : " + response.getStatus().getStatusMessage());
        } catch (WebClientException e) {
            e.printStackTrace();

        }
        return response;
    }


    public TripShipmentAssociationResponse pickupReverseBagFromStoreOnlyCash(String masterBagId, String tripId, AttemptReasonCode attemptReasonCode, int totalFailedDeliveriesPicked, int totalSuccessfulReturnsPicked, float amountCollected) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();

        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.REVERSE_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.PS);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);

        //Specific to reverseBag
        tripShipmentAssociationEntry.setTotalFailedDeliveriesPicked(totalFailedDeliveriesPicked);
        tripShipmentAssociationEntry.setTotalSuccessfulReturnsPicked(totalSuccessfulReturnsPicked);
        tripShipmentAssociationEntry.setAmountCollected(amountCollected);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            response = tripServiceV2Client.updateTripShipments(tripShipmentAssociationResponse, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
            if (response.getStatus().getStatusType().toString().equals("ERROR"))
                Assert.fail("Error while delivering the Store bag to the Store . Reason : " + response.getStatus().getStatusMessage());
        } catch (WebClientException e) {
            e.printStackTrace();

        }
        return response;
    }


    public TripShipmentAssociationResponse pickupMensaOldReverseBagFromStore(String masterBagId, String tripId, AttemptReasonCode attemptReasonCode, int totalFailedDeliveriesPicked, int totalSuccessfulReturnsPicked, float amountCollected) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();

        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.REVERSE_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.PS);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);

        //Specific to reverseBag
        tripShipmentAssociationEntry.setTotalFailedDeliveriesPicked(totalFailedDeliveriesPicked);
        tripShipmentAssociationEntry.setTotalSuccessfulReturnsPicked(totalSuccessfulReturnsPicked);
        tripShipmentAssociationEntry.setAmountCollected(amountCollected);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            response = tripServiceV2Client.updateTripShipments(tripShipmentAssociationResponse, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();

        }
        return response;
    }

    public List<String> getListOfForwardUnattemptedOrders(Long storeDcId, ShipmentType shipmentType) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            tripOrderAssignmentResponse = tripServiceV2Client.getAllUnattemptedShipments(storeDcId, 0, 100, "id", "DESC", MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());

        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lasmtile client");
        }
        List<String> trackingNo = new ArrayList<>();
        for (TripOrderAssignementEntry entry : tripOrderAssignmentResponse.getData()) {
            if (entry.getOrderEntry().getShipmentType().name().equals(shipmentType.name()))
                trackingNo.add(entry.getOrderEntry().getTrackingNumber());
        }

        return trackingNo;
    }

    public List<String> getListOfExchangeUnattemptedOrders(Long storeDcId, ShipmentType shipmentType) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            tripOrderAssignmentResponse = tripServiceV2Client.getAllUnattemptedShipments(storeDcId, 0, 100, "id", "DESC", MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());

        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lasmtile client");
        }
/*
        tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getTrackingNumber()
-tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getExchangeOrderId()*/
        List<String> trackingNo = new ArrayList<>();
        for (TripOrderAssignementEntry entry : tripOrderAssignmentResponse.getData()) {
            if (entry.getOrderEntry().getShipmentType().name().equals(shipmentType.name()))
                trackingNo.add(entry.getOrderEntry().getTrackingNumber());
        }

        return trackingNo;
    }

    public TripOrderAssignmentResponse getUnattemptedShipmets(Long storeDcId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            tripOrderAssignmentResponse = tripServiceV2Client.getAllUnattemptedShipments(storeDcId, 0, 100, "id", "DESC", MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lasmtile client");
        }
        return tripOrderAssignmentResponse;
    }

    public String base64Encoder(String imagePath) {
        String base64Image = "";
        File file = new File(imagePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a Image file from file system
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
            base64Image = Base64.getEncoder().encodeToString(imageData);
        } catch (FileNotFoundException fnf) {
            System.out.println("Image File not found" + fnf);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
        }
        return base64Image;
    }

    public TripOrderAssignmentResponse deliverStoreOrders(Long storeTripId, String storeMLId, Long
            tripOrderAssignmentId, String storeTenantId, String payment) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        TripOrderAssignmentResponse request = new TripOrderAssignmentResponse();
        List<TripOrderAssignementEntry> entryList = new ArrayList<>();
        for(String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripOrderAssignmentId(tripOrderAssignmentId)) {
        TripOrderAssignementEntry entry = new TripOrderAssignementEntry();
        entry.setId(tripOrderAssignmentId);
        entry.setTripId(storeTripId);
        entry.setTripAction(TripAction.UPDATE);
        entry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
        entry.setOrderId(storeMLId);
        entry.setTenantId(storeTenantId);
        entry.setPaymentType(payment);
        entry.setReceivedAt("HOME");
        entry.setReceivedBy("SELF");
            entry.setTrackingNumber(trackingNumber);
            // entry.setAmountCollected(200f);
        entry.setAttemptReasonCode(AttemptReasonCode.DELIVERED);
        String signature = base64Encoder("./Data/lms/signature_request.jpeg");
        entry.setCustomerSignature(signature);
        entryList.add(entry);
        }
        request.setTripOrderAssignmentEntries(entryList);

        try {
            tripOrderAssignmentResponse = tripServiceV2Client.updateTripOrderAssignment(request, storeTenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 10000, 10000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        if (tripOrderAssignmentResponse.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.fail("Delivering of shipment to customer failed : " + tripOrderAssignmentResponse.getStatus().getStatusMessage());
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse failedDeliverStoreOrders(Long storeTripId, String storeMLId, Long
            tripOrderAssignmentId, String storeTenantId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        TripOrderAssignmentResponse request = new TripOrderAssignmentResponse();
        List<TripOrderAssignementEntry> entryList = new ArrayList<>();
        for(String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripOrderAssignmentId(tripOrderAssignmentId)) {
        TripOrderAssignementEntry entry = new TripOrderAssignementEntry();
        entry.setId(tripOrderAssignmentId);
        entry.setTripId(storeTripId);
        entry.setTripAction(TripAction.UPDATE);
        entry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
        entry.setOrderId(storeMLId);
        entry.setTenantId(storeTenantId);
        entry.setAttemptReasonCode(AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        entry.setCustomerSignature(storeTenantId + "_Gloria");
        entry.setOrderEntry(null);
            entry.setTrackingNumber(trackingNumber);
        entryList.add(entry);
        }
        request.setTripOrderAssignmentEntries(entryList);

        try {
            tripOrderAssignmentResponse = tripServiceV2Client.updateTripOrderAssignment(request, storeTenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 10000, 10000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }

        if (tripOrderAssignmentResponse.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.fail("Marking FD of shipment failed : " + tripOrderAssignmentResponse.getStatus().getStatusMessage());
        }
        return tripOrderAssignmentResponse;

    }

    public TripOrderAssignmentResponse getActiveTripOrdersByDeliveryStaffMobile(String deliveryStaffMobile, String
            tenantId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;

        try {
            tripOrderAssignmentResponse = tripServiceV2Client.getActiveTripOrdersByDeliveryStaffMobile(deliveryStaffMobile, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }

        return tripOrderAssignmentResponse;
    }


    public TripOrderAssignmentResponse closeStoreTrip(Long storeTripId, HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap, String storeTenantId) {

        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        TripOrderAssignmentResponse request = new TripOrderAssignmentResponse();

        Iterator it = tripOrderIdAttemptReasonMap.entrySet().iterator();
        List<TripOrderAssignementEntry> entryList = new ArrayList<>();
        for (Map.Entry<Long, AttemptReasonCode> mapEntry : tripOrderIdAttemptReasonMap.entrySet()) {

            for(String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripId(storeTripId)) {
            TripOrderAssignementEntry entry = new TripOrderAssignementEntry();
            entry.setId(mapEntry.getKey());
            entry.setTripId(storeTripId);
            entry.setTripAction(TripAction.TRIP_COMPLETE);
            entry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
            entry.setTenantId(storeTenantId);
            entry.setAttemptReasonCode(mapEntry.getValue());
            entry.setCustomerSignature(storeTenantId + "_Gloria");
            entry.setOrderEntry(null);
                entry.setTrackingNumber(trackingNumber);
            entryList.add(entry);
            }

        }
        request.setTripOrderAssignmentEntries(entryList);

        try {
            tripOrderAssignmentResponse = tripServiceV2Client.updateTripOrderAssignment(request, storeTenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 15000, 15000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }

        return tripOrderAssignmentResponse;


    }
    public TripShipmentAssociationResponse getShipmentIdUsingTripId(Long tripId,String tenantId) throws UnsupportedEncodingException, JAXBException, JAXBException {
        String FGPathparam= "?tenantId="+tenantId;
        Svc service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.FIND_SHIPMENTS_BY_TRIP_ID+"/"+tripId, new String[]{FGPathparam} , SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        TripShipmentAssociationResponse tripShipmentAssociationResponse = (TripShipmentAssociationResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());
        return tripShipmentAssociationResponse;
    }

    public TripOrderAssignmentResponse closeMyntraTripWithStoreReverseBag(String myntraTenantId, Long myntraTripId) throws IOException, JAXBException, InterruptedException, XMLStreamException, ManagerException, JSONException, WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        TripOrderAssignmentResponse request = new TripOrderAssignmentResponse();
        TripResponse response = (TripResponse) lmsServiceHelper.getTrip.apply(myntraTripId.toString());// cannot use client
        String tripNumber = response.getTrips().get(0).getTripNumber();
        TripShipmentAssociationResponse tripShipmentAssociationResponse=findShipmentByTripNumber(tripNumber,ShipmentType.REVERSE_BAG,myntraTenantId);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");

        String shipmentId=tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId();
        ShipmentResponse shipmentOrderMapResponse=masterBagClient.getShipmentOrderMap(Long.valueOf(shipmentId));
        String trackingNumber=shipmentOrderMapResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getTrackingNumber();
        List<TripOrderAssignementEntry> entryList = new ArrayList<>();
        TripOrderAssignementEntry entry = new TripOrderAssignementEntry();
        entry.setId(myntraTripId);
        entry.setTripId(myntraTripId);
        entry.setTripAction(TripAction.TRIP_COMPLETE);
        entry.setShipmentType(ShipmentType.TRIP);
        entryList.add(entry);

        request.setTripOrderAssignmentEntries(entryList);
        try {
            tripOrderAssignmentResponse = tripServiceV2Client.updateTripOrderAssignment(request, myntraTenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }

        if (tripOrderAssignmentResponse.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.fail("Marking FD of shipment failed : " + tripOrderAssignmentResponse.getStatus().getStatusMessage());
        }
        return tripOrderAssignmentResponse;

    }


    public TripOrderAssignmentResponse closeMyntraTripWithStoreBag(String myntraTenantId, Long myntraTripId) throws UnsupportedEncodingException, JAXBException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        TripOrderAssignmentResponse request = new TripOrderAssignmentResponse();
        TripShipmentAssociationResponse tripShipmentAssociationResponse=getShipmentIdUsingTripId(myntraTripId,myntraTenantId);
        String shipmentId=tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId();
        ShipmentResponse shipmentOrderMapResponse=masterBagClient.getShipmentOrderMap(Long.valueOf(shipmentId));
        String trackingNumber=shipmentOrderMapResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getTrackingNumber();
        List<TripOrderAssignementEntry> entryList = new ArrayList<>();
        TripOrderAssignementEntry entry = new TripOrderAssignementEntry();
        entry.setId(myntraTripId);
        entry.setTripId(myntraTripId);
        entry.setTripAction(TripAction.TRIP_COMPLETE);
        entry.setShipmentType(ShipmentType.TRIP);
            entry.setTrackingNumber(trackingNumber);
        entryList.add(entry);

        request.setTripOrderAssignmentEntries(entryList);
        try {
            tripOrderAssignmentResponse = tripServiceV2Client.updateTripOrderAssignment(request, myntraTenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }

        if (tripOrderAssignmentResponse.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.fail("Marking FD of shipment failed : " + tripOrderAssignmentResponse.getStatus().getStatusMessage());
        }
        return tripOrderAssignmentResponse;

    }

    public String assignReverseBagToTrip(Long reverseTripId, Long storeHlPId, String tenantId) {
        TripShipmentAssociationResponse request = new TripShipmentAssociationResponse();

        TripShipmentAssociationResponse response = null;
        TripShipmentAssociationEntry entry = new TripShipmentAssociationEntry();
        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        entry.setTripId(String.valueOf(reverseTripId));
        entry.setStoreDCId(storeHlPId);
        entry.setShipmentType(ShipmentType.REVERSE_BAG);
        tripShipmentAssociationEntryList.add(entry);
        request.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        try {
            response = tripServiceV2Client.assignReverseBagToTrip(request, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }

        return response.getTripShipmentAssociationEntryList().get(0).getShipmentId();
    }

    public TripShipmentAssociationResponse getReverseShipmentBagDetails(String tripNumber, ShipmentType
            reverseBag, String tenantId) {
        TripShipmentAssociationResponse response = null;

        try {
            response = tripServiceV2Client.findShipmentByTripNumber(tripNumber, reverseBag, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 15000, 15000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }

        return response;
    }


    public TripResponse findAllActiveTripWithStore(Long originPremiseId) {
        TripResponse response = null;

        try {
            response = tripServiceV2Client.findAllActiveTripWithStore(originPremiseId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }

        return response;
    }


    public TripOrderAssignmentResponse updateTripOrderAssignment(TripOrderAssignmentResponse response, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            tripOrderAssignmentResponse = tripServiceV2Client.updateTripOrderAssignment(response, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }


    public TripOrderAssignmentResponse getTripsDetail(final Long dcId, final String startDate) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            tripOrderAssignmentResponse = tripServiceV2Client.getTripsDetail(dcId, startDate, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse getTripDetails(final String trackingNumber, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            tripOrderAssignmentResponse = tripServiceV2Client.getTripDetails(trackingNumber, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse findOrdersByTrip(final String tripNumber, final ShipmentType shipment_type, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            tripOrderAssignmentResponse = tripServiceV2Client.findOrdersByTrip(tripNumber, shipment_type, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse autoAssignmentOfOrderToTrip(final String orderIds, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            tripOrderAssignmentResponse = tripServiceV2Client.autoAssignmentOfOrderToTrip(orderIds, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripResponse update(TripEntry arg0, final Long id) throws WebClientException {
        TripResponse tripResponse = null;
        try {
            tripResponse = tripServiceV2Client.update(arg0, id, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    //TODO : pass object response once mapping is done
    public TripUpdateDashboardInfoResponse getTripUpdateDashboardInfoByTripNumber(final String tripNumber) throws WebClientException {
        TripUpdateDashboardInfoResponse tripUpdateDashboardInfoResponse = null;
        try {

            tripServiceV2Client.getTripUpdateDashboardInfoByTripNumber(tripNumber, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripUpdateDashboardInfoResponse;
    }

    //    //TODO : pass object response once mapping is done
    public TripUpdateDashboardInfoResponse getTripUpdateDashboardInfoV2(final Long dcId, final String startDate) throws WebClientException {
        TripUpdateDashboardInfoResponse tripUpdateDashboardInfoResponse = null;

        try {

            tripServiceV2Client.getTripUpdateDashboardInfoV2(dcId, startDate, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripUpdateDashboardInfoResponse;
    }

    //TODO : pass object response once mapping is done
    public TripUpdateDashboardInfoResponse getTripUpdateDashboardInfo(final Long tripId) throws WebClientException {
        TripUpdateDashboardInfoResponse tripUpdateDashboardInfoResponse = null;
        try {

            tripServiceV2Client.getTripUpdateDashboardInfo(tripId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripUpdateDashboardInfoResponse;
    }


    public TripResponse getAllAvailableTripsForDC(final Long deliveryCenterId) throws WebClientException {
        TripResponse tripResponse = null;
        try {

            tripResponse = tripServiceV2Client.getAllAvailableTripsForDC(deliveryCenterId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 15000, 15000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public StoreTripResponse getStoreTripDetails(final Long tripId) throws WebClientException {
        StoreTripResponse storeTripResponse = null;
        try {

            storeTripResponse = tripServiceV2Client.getStoreTripDetails(tripId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return storeTripResponse;
    }

    public FinanceReportResponse getFinanceReport(final String startDate, final String endDate, final String tenantId) throws WebClientException {
        FinanceReportResponse financeReportResponse = null;
        try {

            financeReportResponse = tripServiceV2Client.getFinanceReport(startDate, endDate, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return financeReportResponse;
    }

    public OrderResponse getAllFailedExchanges(final Long deliveryCenterId, final Integer start, final Integer limit, final String sortBy, final String dir, final String tenantId) throws WebClientException {
        OrderResponse orderResponse = null;
        try {

            orderResponse = tripServiceV2Client.getAllFailedExchanges(deliveryCenterId, start, limit, sortBy, dir, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return orderResponse;
    }

    public OrderResponse getAllFailedOrdersForDCBaedOnTripDate(TripEntry entry) throws WebClientException {
        OrderResponse orderResponse = null;
        try {

            orderResponse = tripServiceV2Client.getAllFailedOrdersForDCBaedOnTripDate(entry, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return orderResponse;
    }

    public OrderResponse getAllOnHoldOrdersForDCBasedOnFilter(TripEntry entry) throws WebClientException {
        OrderResponse orderResponse = null;
        try {

            orderResponse = tripServiceV2Client.getAllOnHoldOrdersForDCBasedOnFilter(entry, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return orderResponse;
    }

    public TripResponse isAnyTripOpenedForToday(final Long tripId) throws WebClientException {
        TripResponse tripResponse = null;
        try {

            tripResponse = tripServiceV2Client.isAnyTripOpenedForToday(tripId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public TripResponse getOpenedTripsOnStaff(final Long tripId) throws WebClientException {
        TripResponse tripResponse = null;
        try {

            tripResponse = tripServiceV2Client.getOpenedTripsOnStaff(tripId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public TripResponse autoCardEnabled(final Long dcId, final Long dfId) throws WebClientException {
        TripResponse tripResponse = null;
        try {

            tripResponse = tripServiceV2Client.autoCardEnabled(dcId, dfId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public TripResponse getActiveTripForOrder(final String orderId, final String tenantId) throws WebClientException {
        TripResponse tripResponse = null;
        try {

            tripResponse = tripServiceV2Client.getActiveTripForOrder(orderId, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public TripOrderAssignmentResponse assignOrderToTrip(TripOrderAssignmentResponse d, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {

            tripOrderAssignmentResponse = tripServiceV2Client.assignOrderToTrip(d, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripResponse updateOdometerReading(final Long tripId, final Long startOdometerReading, final Long endOdometerReading, final String createdBy) throws WebClientException {
        TripResponse tripResponse = null;
        try {

            tripResponse = tripServiceV2Client.updateOdometerReading(tripId, startOdometerReading, endOdometerReading, createdBy, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public TripOrderAssignmentResponse unassignOrderFromTrip(final String tripOrderId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {

            tripOrderAssignmentResponse = tripServiceV2Client.unassignOrderFromTrip(tripOrderId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }


    public TripOrderAssignmentResponse unassignOrderFromTripThroughTripId(final String orderId, final String trackingNumber, final Long tripId, final ShipmentType shipmentType) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {

            tripOrderAssignmentResponse = tripServiceV2Client.unassignOrderFromTrip(orderId, trackingNumber, tripId, shipmentType, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse addAndOutscanNewOrderToTrip(final String trackingNo, final Long tripId, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {

            tripOrderAssignmentResponse = tripServiceV2Client.addAndOutscanNewOrderToTrip(trackingNo, tripId, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse findExchangesByTrip(final String tripNumber, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {

            tripOrderAssignmentResponse = tripServiceV2Client.findExchangesByTrip(tripNumber, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse findExchangesByTrip(final Long tripId, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {

            tripOrderAssignmentResponse = tripServiceV2Client.findExchangesByTrip(tripId, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse unassignExchangeFromTrip(final String orderId, final String returnId, final Long tripId, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {

            tripOrderAssignmentResponse = tripServiceV2Client.unassignExchangeFromTrip(orderId, returnId, tripId, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripResponse getAllAvailableTripsForDCByStaffType(final Long deliveryCenterId, final DeliveryStaffType staffType) throws WebClientException {
        TripResponse tripResponse = null;
        try {

            tripResponse = tripServiceV2Client.getAllAvailableTripsForDCByStaffType(deliveryCenterId, staffType, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

//    public OrderResponse getAllIncompleteOrdersForDC(final Long deliveryCenterId, final ShipmentType shipment_type, final int start, final int limit, final String sortBy, final String dir, final String tenantId) throws WebClientException {
//        OrderResponse orderResponse = null;
//        try {
//
//            orderResponse = tripServiceV2Client.getAllIncompleteOrdersForDC(deliveryCenterId, shipment_type, start, limit, sortBy, dir, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
//        } catch (WebClientException e) {
//            e.printStackTrace();
//        }
//        return orderResponse;
//    }
//
//    public OrderResponse getAllIncompleteExchangesForDC(final Long deliveryCenterId, final int start, final int limit, final String sortBy, final String dir, final String tenantId) throws WebClientException {
//        OrderResponse orderResponse = null;
//        try {
//
//            orderResponse = tripServiceV2Client.getAllIncompleteExchangesForDC(deliveryCenterId, start, limit, sortBy, dir, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
//        } catch (WebClientException e) {
//            e.printStackTrace();
//        }
//        return orderResponse;
//    }

//    public OrderResponse getAllIncompleteScheduledShipmentsForDC(final Long deliveryCenterId, final ShipmentType shipment_type, final int start, final int limit, final String sortBy, final String dir, final String tenantId) throws WebClientException {
//        OrderResponse orderResponse = null;
//        try {
//
//            orderResponse = tripServiceV2Client.getAllIncompleteScheduledShipmentsForDC(deliveryCenterId, shipment_type, start, limit, sortBy, dir, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
//        } catch (WebClientException e) {
//            e.printStackTrace();
//        }
//        return orderResponse;
//    }

    public TripShipmentAssociationResponse findShipmentByTripNumber(final String tripNumber, final ShipmentType shipmentType, final String tenantId) throws WebClientException {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = null;
        try {

            tripShipmentAssociationResponse = tripServiceV2Client.findShipmentByTripNumber(tripNumber, shipmentType, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripShipmentAssociationResponse;
    }


    public TripResponse findById(final Long arg0) throws WebClientException {
        TripResponse tripResponse = null;
        try {

            tripResponse = tripServiceV2Client.findById(arg0, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripResponse;
    }


    public String receiveShipmentFromAdmin(String trackingNumber) {
        String pathParam = "?operationalTrackingId=" + trackingNumber + "&tenantId=" + LMS_CONSTANTS.TENANTID + "&adminReceive=true";
        String response="";
        try {
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.RECEIVE_SHIPMENT_BEFORE_TRIP_COMPLETE, new String[]{pathParam}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, null, Headers.getLmsHeaderJSON());
            response = APIUtilities.getElement(service.getResponseBody(), "tripOrderResponse.status.statusType", "json");
        }catch (Exception e){
            e.printStackTrace();
        }
//        TripOrderResponse tripOrderResponse=(TripOrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderResponse());
        return response;
    }




}


