package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.OrderTrackingResponse;
import com.myntra.lms.client.response.OrderTrackingResponseV2;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.springframework.http.MediaType;
import org.testng.Assert;

import java.text.MessageFormat;

public class OrderTrackingClient_QA {

    public OrderTrackingResponseV2 getOrderTrackingDetailV2(String trackingNumber, String courierCode,OrderTrackingDetailLevel level,String tenantId,String clientId ) {
        OrderTrackingResponseV2 orderTrackingResponseV2 = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH. getOrderTrackingDetailV2;
            String pathParm= MessageFormat.format("?courierOperator={0}&trackingNumber={1}&level={2}&tenantId={3}&clientId={4}",courierCode,trackingNumber,level.name(),tenantId,clientId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            orderTrackingResponseV2 = (OrderTrackingResponseV2) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderTrackingResponseV2());



        } catch (Exception e) {
            Assert.fail("Unable to call api - get Order tracking details");
        }
        return orderTrackingResponseV2;
    }

    public OrderTrackingResponseV2 getOrderTrackingDetailV2(String trackingNumber, String courierCode) {
        OrderTrackingResponseV2 orderTrackingResponseV2 = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH. getOrderTrackingDetailV2;
            String pathParm= MessageFormat.format("?courierOperator={0}&trackingNumber={1}&level={2}&tenantId={3}&clientId={4}",courierCode,trackingNumber,OrderTrackingDetailLevel.LEVEL2, String.valueOf(4019), String.valueOf(2297));

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            orderTrackingResponseV2 = (OrderTrackingResponseV2) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderTrackingResponseV2());



        } catch (Exception e) {
            Assert.fail("Unable to call api - get Order tracking details");
        }
        return orderTrackingResponseV2;
    }

    public OrderTrackingResponse getOrderTrackingResponseById(String orderTrackingId) {
        OrderTrackingResponse orderTrackingResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH. findById;

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{orderTrackingId}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            orderTrackingResponse = (OrderTrackingResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderTrackingResponse());



        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderTrackingResponse;
    }

    public OrderTrackingResponse getOrderTrackingId(String trackingNumber) {
        String searchParams = "trackingNumber.eq:" + trackingNumber;
        OrderTrackingResponse orderTrackingResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH. filteredSearch;
            String pathParm= MessageFormat.format("?fetchSize={0}&q={1}&sortOrder={2}&distinct={3}&sortBy={4}&f={5]&start={6}",-1,searchParams,"asc",null,"id","",0);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            orderTrackingResponse = (OrderTrackingResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderTrackingResponse());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderTrackingResponse;
    }
}
