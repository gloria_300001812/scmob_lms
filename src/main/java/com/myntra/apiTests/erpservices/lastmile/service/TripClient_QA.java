package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.mnm.helpers.MobileNumberMaskingHelper;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.exception.ManagerException;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.DeliveryStaffType;
import com.myntra.lastmile.client.code.utils.TripAction;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.code.utils.UpdatedVia;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.entry.TripOrderAssignementEntry;
import com.myntra.lastmile.client.entry.TripShipmentAssociationEntry;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.masterbag.core.MasterbagShipment;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import lombok.SneakyThrows;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TripClient_QA {
    /*
     * createTrip
     * Http method:- POST
     * */
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    MasterBagClient_QA masterBagClient_qa = new MasterBagClient_QA();

    public TripResponse createTrip(long deliveryCenterId, long deliveryStaffId) {
        TripEntry tripEntry = new TripEntry();
        tripEntry.setTripDate(new Date());
        tripEntry.setDeliveryCenterId(deliveryCenterId);
        tripEntry.setDeliveryStaffId(deliveryStaffId);
        tripEntry.setCreatedBy("Automation");
        tripEntry.setIsInbound(false);
        tripEntry.setIsCardEnabled(false);
        TripResponse tripResponse = null;

        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_CREATEVersion2;

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(tripEntry), Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to create trip and connect to lastmile client");
        }
        return tripResponse;
    }

    /*
     * assignMasterbagToTrip
     * Http method:- POST
     * */
    public TripShipmentAssociationResponse addStoreBagToTrip(String tripId, String storeMBId, String tenantId) {

        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        TripShipmentAssociationResponse response = new TripShipmentAssociationResponse();
        ArrayList<TripShipmentAssociationEntry> tripShipmentAssociationEntry = new ArrayList<>();
        TripShipmentAssociationEntry entry = new TripShipmentAssociationEntry();
        entry.setTripId(tripId);
        entry.setShipmentId(storeMBId);
        entry.setShipmentType(ShipmentType.FORWARD_BAG);
        tripShipmentAssociationEntry.add(entry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntry);
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Assign_Masterbag_Trip;
            String URIPath="?tenantId={0}";
            String pathParm = MessageFormat.format(URIPath, tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(tripShipmentAssociationResponse), Headers.getLmsHeaderXML());

            response = (TripShipmentAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to add the store bag to trip, unable to connect to lastmile client");
        }
        if (response.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.fail("Unable to add masterBag : " + storeMBId + " to trip - " + tripId + " .Reason : " + response.getStatus().getStatusMessage());
        }
        return response;
    }
    /*
     * StartTrip
     * Http method:- POST
     * */

    public TripOrderAssignmentResponse startTripOld(Long tripId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_STARTVersion2;
            String tripIds=Long.toString(tripId);
            String pathParam = MessageFormat.format("{0}/{1}/{2}/{3}", tripIds, null, "gloria", 12l);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParam}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        } catch (Exception e1) {
            e1.printStackTrace();
            Assert.fail("Unable to connect to lastmile client and start the trip");
        }
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equals("Trip updated successfully"), "Start Trip failed of failure ");

        return tripOrderAssignmentResponse;
    }


    /*
     * filteredSearch
     * Http method:- GET
     * */

    public TripResponse searchTripByParam(int start, int fetchSize, String sortBy, String sortOrder, Boolean distinct, String q, String f) {

        TripResponse tripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_FilterSearch;
            String urIpath ="?q={5}&f={6}&fetchSize={1}&sortOrder={3}&start={0}&distinct={4}&sortBy={2}";
            String pathParams = MessageFormat.format(urIpath, start, fetchSize, sortBy, sortOrder, distinct, q, f);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (tripResponse.getStatus().getStatusMessage().equals("Unable to filter, so returning non filtered results.null"))
            Assert.fail("Unable to search trip: ");
        return tripResponse;
    }


    /*
     * findById
     * Http method:- GET
     * */
    public static TripResponse searchByTripId(long tripId) {
        TripResponse tripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_Find_By_Id;
            String pathParm = Long.toString(tripId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (tripResponse.getStatus().getStatusMessage().equals("Unable to searchTrip by tripId"))
            Assert.fail("Unable to search trip by TripId: " + tripId);
        return tripResponse;
    }

    /*
     * deliverStoreBagToStore
     * Http method:- POST
     * */
    public TripShipmentAssociationResponse deliverStoreBagToStore(String masterBagId, List<String> trackingNumberList, String tripId, AttemptReasonCode attemptReasonCode, String tenantId) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();
        for (String trackingNumber : trackingNumberList) {
            MasterbagShipment shipmentItem = new MasterbagShipment();
            shipmentItem.setShipmentType(ShipmentType.DL);
            shipmentItem.setTrackingNumber(trackingNumber);
            shipmentItemList.add(shipmentItem);

        }
        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.FORWARD_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.DL);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.UPDATE_TRIP_SHIPMENTS;
            String pathParm = MessageFormat.format("?tenantId={0}", tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(tripShipmentAssociationResponse), Headers.getLmsHeaderXML());
            response = (TripShipmentAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());

        } catch (Exception e) {
            e.printStackTrace();

        }
        return response;
    }

    /*
     * deliverMensaOldStoreBagToStore
     * Http method:- POST
     * */
    public TripShipmentAssociationResponse deliverMensaOldStoreBagToStore(String masterBagId, String tripId, AttemptReasonCode attemptReasonCode, String tenantId) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();

        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.FORWARD_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.DL);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.UPDATE_TRIP_SHIPMENTS;

            String pathParm = MessageFormat.format("?tenantId={0}", tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(tripShipmentAssociationResponse), Headers.getLmsHeaderXML());
            response = (TripShipmentAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());

        } catch (Exception e) {
            e.printStackTrace();

        }
        return response;
    }
    /*
     * FaileddeliverStoreBagToStore
     * Http method:- POST
     * */

    public TripShipmentAssociationResponse FaileddeliverStoreBagToStore(String masterBagId, List<String> trackingNumberList, String tripId, AttemptReasonCode attemptReasonCode, String tenantId) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();
        for (String trackingNumber : trackingNumberList) {
            MasterbagShipment shipmentItem = new MasterbagShipment();
            shipmentItem.setShipmentType(ShipmentType.DL);
            shipmentItem.setTrackingNumber(trackingNumber);
            shipmentItemList.add(shipmentItem);

        }
        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.FORWARD_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.FD);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.UPDATE_TRIP_SHIPMENTS;
            String pathParm = MessageFormat.format("?tenantId={0}", tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST,APIUtilities.convertXMLObjectToString(tripShipmentAssociationResponse), Headers.getLmsHeaderXML());
            response = (TripShipmentAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());

            if (response.getStatus().getStatusType().toString().equals("ERROR"))
                Assert.fail("Error while delivering the Store bag to the Store . Reason : " + response.getStatus().getStatusMessage());
        } catch (Exception e) {
            e.printStackTrace();

        }
        return response;
    }


    public TripShipmentAssociationResponse pickupReverseBagFromStore(String masterBagId, List<String> trackingNumberList, String tripId, AttemptReasonCode attemptReasonCode, int totalFailedDeliveriesPicked, int totalSuccessfulReturnsPicked, float amountCollected, String tenantId) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();
        for (String trackingNumber : trackingNumberList) {
            MasterbagShipment shipmentItem = new MasterbagShipment();
            shipmentItem.setShipmentType(ShipmentType.DL);
            shipmentItem.setTrackingNumber(trackingNumber);
            shipmentItemList.add(shipmentItem);


        }
        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.REVERSE_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.PS);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);

        //Specific to reverseBag
        tripShipmentAssociationEntry.setTotalFailedDeliveriesPicked(totalFailedDeliveriesPicked);
        tripShipmentAssociationEntry.setTotalSuccessfulReturnsPicked(totalSuccessfulReturnsPicked);
        tripShipmentAssociationEntry.setAmountCollected(amountCollected);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.UPDATE_TRIP_SHIPMENTS;
            String pathParm = MessageFormat.format("?tenantId={0}", tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(tripShipmentAssociationResponse), Headers.getLmsHeaderXML());
            response = (TripShipmentAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());
            if (response.getStatus().getStatusType().toString().equals("ERROR"))
                Assert.fail("Error while delivering the Store bag to the Store . Reason : " + response.getStatus().getStatusMessage());
        } catch (Exception e) {
            e.printStackTrace();

        }
        return response;
    }


    public TripShipmentAssociationResponse pickupReverseBagFromStoreOnlyCash(String masterBagId, String tripId, AttemptReasonCode attemptReasonCode, int totalFailedDeliveriesPicked, int totalSuccessfulReturnsPicked, float amountCollected, String tenantId) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();

        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.REVERSE_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.PS);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);

        //Specific to reverseBag
        tripShipmentAssociationEntry.setTotalFailedDeliveriesPicked(totalFailedDeliveriesPicked);
        tripShipmentAssociationEntry.setTotalSuccessfulReturnsPicked(totalSuccessfulReturnsPicked);
        tripShipmentAssociationEntry.setAmountCollected(amountCollected);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.UPDATE_TRIP_SHIPMENTS;
            String pathParm = MessageFormat.format("?tenantId={0}", tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(tripShipmentAssociationResponse), Headers.getLmsHeaderXML());
            response = (TripShipmentAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());

            if (response.getStatus().getStatusType().toString().equals("ERROR"))
                Assert.fail("Error while delivering the Store bag to the Store . Reason : " + response.getStatus().getStatusMessage());
        } catch (Exception e) {
            e.printStackTrace();

        }
        return response;
    }

    public TripShipmentAssociationResponse pickupMensaOldReverseBagFromStore(String masterBagId, String tripId, AttemptReasonCode attemptReasonCode, int totalFailedDeliveriesPicked, int totalSuccessfulReturnsPicked, float amountCollected, String tenantId) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();

        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.REVERSE_BAG);
        tripShipmentAssociationEntry.setStatus(TripOrderStatus.PS);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);

        //Specific to reverseBag
        tripShipmentAssociationEntry.setTotalFailedDeliveriesPicked(totalFailedDeliveriesPicked);
        tripShipmentAssociationEntry.setTotalSuccessfulReturnsPicked(totalSuccessfulReturnsPicked);
        tripShipmentAssociationEntry.setAmountCollected(amountCollected);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.UPDATE_TRIP_SHIPMENTS;
            String pathParm = MessageFormat.format("?tenantId={0}", tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(tripShipmentAssociationResponse), Headers.getLmsHeaderXML());
            response = (TripShipmentAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());
        } catch (Exception e) {
            e.printStackTrace();

        }
        return response;
    }

    /*
     * getAllUnattemptedShipments
     * Http method:- GET
     * */

    public List<String> getListOfForwardUnattemptedOrders(Long storeDcId, ShipmentType shipmentType) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Get_Unattempted_Shipments;
            String storeDcIds=Long.toString(storeDcId);
            String pathParm = MessageFormat.format("{0}?start={1}&size={2}&sortBy={3}&sortDirection={4}", storeDcIds, 0, 100, "id", "DESC");

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lasmtile client");
        }
        List<String> trackingNo = new ArrayList<>();
        for (TripOrderAssignementEntry entry : tripOrderAssignmentResponse.getData()) {
            if (entry.getOrderEntry().getShipmentType().name().equals(shipmentType.name()))
                trackingNo.add(entry.getOrderEntry().getTrackingNumber());
        }

        return trackingNo;
    }

    public List<String> getListOfExchangeUnattemptedOrders(Long storeDcId, ShipmentType shipmentType) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Get_Unattempted_Shipments;
            String pathParm = MessageFormat.format("/{0}?start={1}&size={2}&sortBy={3}&sortDirection={4}", storeDcId, 0, 100, "id", "DESC");
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());


        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lasmtile client");
        }
/*
        tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getTrackingNumber()
-tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getExchangeOrderId()*/
        List<String> trackingNo = new ArrayList<>();
        for (TripOrderAssignementEntry entry : tripOrderAssignmentResponse.getData()) {
            if (entry.getOrderEntry().getShipmentType().name().equals(shipmentType.name()))
                trackingNo.add(entry.getOrderEntry().getTrackingNumber());
        }

        return trackingNo;
    }

    public TripOrderAssignmentResponse getUnattemptedShipmets(Long storeDcId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Get_Unattempted_Shipments;
            String pathParm = MessageFormat.format("{0}?start={1}&size={2}&sortBy={3}&sortDirection={4}", storeDcId, 0, 100, "id", "DESC");
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lasmtile client");
        }
        return tripOrderAssignmentResponse;
    }

    public String base64Encoder(String imagePath) {
        String base64Image = "";
        File file = new File(imagePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a Image file from file system
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
            base64Image = Base64.getEncoder().encodeToString(imageData);
        } catch (FileNotFoundException fnf) {
            System.out.println("Image File not found" + fnf);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
        }
        return base64Image;
    }

    public TripOrderAssignmentResponse deliverStoreOrders(Long storeTripId, String storeMLId, Long
            tripOrderAssignmentId, String storeTenantId, String payment, String tenantId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        TripOrderAssignmentResponse request = new TripOrderAssignmentResponse();
        List<TripOrderAssignementEntry> entryList = new ArrayList<>();
        for (String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripOrderAssignmentId(tripOrderAssignmentId)) {
            TripOrderAssignementEntry entry = new TripOrderAssignementEntry();
            entry.setId(tripOrderAssignmentId);
            entry.setTripId(storeTripId);
            entry.setTripAction(TripAction.UPDATE);
            entry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
            entry.setOrderId(storeMLId);
            entry.setTenantId(storeTenantId);
            entry.setPaymentType(payment);
            entry.setReceivedAt("HOME");
            entry.setReceivedBy("SELF");
            entry.setTrackingNumber(trackingNumber);
            // entry.setAmountCollected(200f);
            entry.setAttemptReasonCode(AttemptReasonCode.DELIVERED);
            String signature = base64Encoder("./Data/lms/signature_request.jpeg");
            entry.setCustomerSignature(signature);
            entryList.add(entry);
        }
        request.setTripOrderAssignmentEntries(entryList);

        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_UPDATEVersion2;
            String pathParm = MessageFormat.format("?tenantId={0}", tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(request), Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (tripOrderAssignmentResponse.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.fail("Delivering of shipment to customer failed : " + tripOrderAssignmentResponse.getStatus().getStatusMessage());
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse failedDeliverStoreOrders(Long storeTripId, String storeMLId, Long tripOrderAssignmentId, String storeTenantId, String tenantId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        TripOrderAssignmentResponse request = new TripOrderAssignmentResponse();
        List<TripOrderAssignementEntry> entryList = new ArrayList<>();
        for (String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripOrderAssignmentId(tripOrderAssignmentId)) {
            TripOrderAssignementEntry entry = new TripOrderAssignementEntry();
            entry.setId(tripOrderAssignmentId);
            entry.setTripId(storeTripId);
            entry.setTripAction(TripAction.UPDATE);
            entry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
            entry.setOrderId(storeMLId);
            entry.setTenantId(storeTenantId);
            entry.setAttemptReasonCode(AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
            entry.setCustomerSignature(storeTenantId + "_Gloria");
            entry.setOrderEntry(null);
            entry.setTrackingNumber(trackingNumber);
            entryList.add(entry);
        }
        request.setTripOrderAssignmentEntries(entryList);

        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_UPDATEVersion2;
            String pathParm = MessageFormat.format("?tenantId={0}", tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(request), Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (tripOrderAssignmentResponse.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.fail("Marking FD of shipment failed : " + tripOrderAssignmentResponse.getStatus().getStatusMessage());
        }
        return tripOrderAssignmentResponse;

    }

    public TripOrderAssignmentResponse getActiveTripOrdersByDeliveryStaffMobile(String deliveryStaffMobile, String tenantId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;

        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.GetActiveTripOrders_By_DeliveryStaffMobile;
            String pathParm = MessageFormat.format("{0}?tenantId={1}", deliveryStaffMobile, tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse closeStoreTrip(Long storeTripId, HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap, String storeTenantId) {

        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        TripOrderAssignmentResponse request = new TripOrderAssignmentResponse();

        Iterator it = tripOrderIdAttemptReasonMap.entrySet().iterator();
        List<TripOrderAssignementEntry> entryList = new ArrayList<>();
        for (Map.Entry<Long, AttemptReasonCode> mapEntry : tripOrderIdAttemptReasonMap.entrySet()) {

            for (String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripId(storeTripId)) {
                TripOrderAssignementEntry entry = new TripOrderAssignementEntry();
                entry.setId(mapEntry.getKey());
                entry.setTripId(storeTripId);
                entry.setTripAction(TripAction.TRIP_COMPLETE);
                entry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
                entry.setTenantId(storeTenantId);
                entry.setAttemptReasonCode(mapEntry.getValue());
                entry.setCustomerSignature(storeTenantId + "_Gloria");
                entry.setOrderEntry(null);
                entry.setTrackingNumber(trackingNumber);
                entryList.add(entry);
            }

        }
        request.setTripOrderAssignmentEntries(entryList);

        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_UPDATEVersion2;
            String pathParm = MessageFormat.format("?tenantId={0}", storeTenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(request), Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tripOrderAssignmentResponse;

    }

    public TripShipmentAssociationResponse getShipmentIdUsingTripId(Long tripId, String tenantId) throws UnsupportedEncodingException, JAXBException, JAXBException {
        String FGPathparam = "?tenantId=" + tenantId;
        Svc service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.FIND_SHIPMENTS_BY_TRIP_ID + "/" + tripId, new String[]{FGPathparam}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        TripShipmentAssociationResponse tripShipmentAssociationResponse = (TripShipmentAssociationResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());
        return tripShipmentAssociationResponse;
    }


    public TripOrderAssignmentResponse closeMyntraTripWithStoreReverseBag(String myntraTenantId, Long myntraTripId) throws IOException, JAXBException, InterruptedException, XMLStreamException, ManagerException, JSONException, WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        TripOrderAssignmentResponse request = new TripOrderAssignmentResponse();
        TripResponse response = (TripResponse) lmsServiceHelper.getTrip.apply(myntraTripId.toString());// cannot use client
        String tripNumber = response.getTrips().get(0).getTripNumber();
        TripShipmentAssociationResponse tripShipmentAssociationResponse = findShipmentByTripNumber(tripNumber, ShipmentType.REVERSE_BAG, myntraTenantId);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");

        String shipmentId = tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId();
        ShipmentResponse shipmentOrderMapResponse = masterBagClient_qa.getShipmentOrderMap(Long.valueOf(shipmentId));
        String trackingNumber = shipmentOrderMapResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getTrackingNumber();
        List<TripOrderAssignementEntry> entryList = new ArrayList<>();
        TripOrderAssignementEntry entry = new TripOrderAssignementEntry();
        entry.setId(myntraTripId);
        entry.setTripId(myntraTripId);
        entry.setTripAction(TripAction.TRIP_COMPLETE);
        entry.setShipmentType(ShipmentType.TRIP);
        entryList.add(entry);

        request.setTripOrderAssignmentEntries(entryList);
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_UPDATEVersion2;
            String pathParm = MessageFormat.format("?tenantId={0}", myntraTenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (tripOrderAssignmentResponse.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.fail("Marking FD of shipment failed : " + tripOrderAssignmentResponse.getStatus().getStatusMessage());
        }
        return tripOrderAssignmentResponse;

    }


    public TripOrderAssignmentResponse closeMyntraTripWithStoreBag(String myntraTenantId, Long myntraTripId) throws UnsupportedEncodingException, JAXBException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        TripOrderAssignmentResponse request = new TripOrderAssignmentResponse();
        TripShipmentAssociationResponse tripShipmentAssociationResponse = getShipmentIdUsingTripId(myntraTripId, myntraTenantId);
        String shipmentId = tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId();
        ShipmentResponse shipmentOrderMapResponse = masterBagClient_qa.getShipmentOrderMap(Long.valueOf(shipmentId));
        String trackingNumber = shipmentOrderMapResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getTrackingNumber();
        List<TripOrderAssignementEntry> entryList = new ArrayList<>();
        TripOrderAssignementEntry entry = new TripOrderAssignementEntry();
        entry.setId(myntraTripId);
        entry.setTripId(myntraTripId);
        entry.setTripAction(TripAction.TRIP_COMPLETE);
        entry.setShipmentType(ShipmentType.TRIP);
        entry.setTrackingNumber(trackingNumber);
        entryList.add(entry);
        request.setTripOrderAssignmentEntries(entryList);

        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_UPDATEVersion2;
            String pathParm = MessageFormat.format("?tenantId={0}", myntraTenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(request), Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (tripOrderAssignmentResponse.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.fail("Marking FD of shipment failed : " + tripOrderAssignmentResponse.getStatus().getStatusMessage());
        }
        return tripOrderAssignmentResponse;

    }

    public String assignReverseBagToTrip(Long reverseTripId, Long storeHlPId, String tenantId) {
        TripShipmentAssociationResponse request = new TripShipmentAssociationResponse();

        TripShipmentAssociationResponse response = null;
        TripShipmentAssociationEntry entry = new TripShipmentAssociationEntry();
        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        entry.setTripId(String.valueOf(reverseTripId));
        entry.setStoreDCId(storeHlPId);
        entry.setShipmentType(ShipmentType.REVERSE_BAG);
        tripShipmentAssociationEntryList.add(entry);
        request.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Assign_ReverseBag_ToTrip;
            String pathParm = MessageFormat.format("?tenantId={0}", tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(request), Headers.getLmsHeaderXML());
            response = (TripShipmentAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return response.getTripShipmentAssociationEntryList().get(0).getShipmentId();
    }

    public TripShipmentAssociationResponse getReverseShipmentBagDetails(String tripNumber, ShipmentType
            reverseBag, String tenantId) {
        TripShipmentAssociationResponse response = null;

        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.findShipment_By_TripNumber;
            String pathParm = MessageFormat.format("{0}?shipmentType={1}&tenantId={2}", tripNumber, reverseBag, tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            response = (TripShipmentAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());


        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    public TripResponse findAllActiveTripWithStore(Long originPremiseId) {
        TripResponse response = null;

        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.find_AllActiveTrip_WithStore;
            String pathParm = Long.toString(originPremiseId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            response = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }


    public TripOrderAssignmentResponse updateTripOrderAssignment(TripOrderAssignmentResponse response, final String tenantId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_UPDATEVersion2;
            String pathParm = MessageFormat.format("?tenantId={0}", tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST,  APIUtilities.convertXMLObjectToString(response), Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }


    public TripOrderAssignmentResponse getTripsDetail(final Long dcId, final String startDate) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Get_Trips_Detail;
            String dcIds=Long.toString(dcId);
            String pathParm = MessageFormat.format("{0}/{1}", dcIds, startDate);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse getTripDetails(final String trackingNumber, final String tenantId) throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Get_Trip_Detail;
            String pathParm = MessageFormat.format("{0}?tenantId={1}", trackingNumber, tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse findOrdersByTrip(final String tripNumber, final ShipmentType shipment_type, final String tenantId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_BY_TRIP_NUMBERVersion2;
            String pathParm = MessageFormat.format("{0}/{1}?tenantId={2}", tripNumber, shipment_type, tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse autoAssignmentOfOrderToTrip(final String orderIds, final String tenantId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.REQUEUE_ORDERVersion2;

            String pathParm = MessageFormat.format("{0}?tenantId={1}",orderIds,tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripResponse update(TripEntry tripEntry, final Long id) throws Exception {
        TripResponse tripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Update_Trip;
            String pathParm = Long.toString(id);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.PUT, APIUtilities.convertXMLObjectToString(tripEntry), Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripResponse;
    }


    //TODO : pass object response once mapping is done
    public TripUpdateDashboardInfoResponse getTripUpdateDashboardInfoByTripNumber(final String tripNumber) {
        TripUpdateDashboardInfoResponse tripUpdateDashboardInfoResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.GetTrip_UpdateDashboardInfo_ByTripNumber;
            String pathParm = MessageFormat.format("{0}", tripNumber);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripUpdateDashboardInfoResponse = (TripUpdateDashboardInfoResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripUpdateDashboardInfoResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripUpdateDashboardInfoResponse;
    }

    //TODO : pass object response once mapping is done
    public TripUpdateDashboardInfoResponse getTripUpdateDashboardInfoV2(final Long dcId, final String startDate) {
        TripUpdateDashboardInfoResponse tripUpdateDashboardInfoResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.GetTrip_UpdateDashboardInfo_ByTripNumber;
            String pathParm = MessageFormat.format("{0}/{1}", dcId, startDate);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripUpdateDashboardInfoResponse = (TripUpdateDashboardInfoResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripUpdateDashboardInfoResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripUpdateDashboardInfoResponse;
    }

    //TODO : pass object response once mapping is done
    public TripUpdateDashboardInfoResponse getTripUpdateDashboardInfo(final Long tripId) {
        TripUpdateDashboardInfoResponse tripUpdateDashboardInfoResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.GetTripUpdate_Dashboard_Info;
            String pathParm = Long.toString(tripId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripUpdateDashboardInfoResponse = (TripUpdateDashboardInfoResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripUpdateDashboardInfoResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripUpdateDashboardInfoResponse;
    }


    public TripResponse getAllAvailableTripsForDC(final Long deliveryCenterId) {
        TripResponse tripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.getAllAvailable_TripsForDC;
            String pathParm = Long.toString(deliveryCenterId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public StoreTripResponse getStoreTripDetails(final Long tripId) {
        StoreTripResponse storeTripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.getStore_TripDetails;
            String pathParm = Long.toString(tripId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            storeTripResponse = (StoreTripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new StoreTripResponse());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return storeTripResponse;
    }

    public FinanceReportResponse getFinanceReport(final String startDate, final String endDate, final String tenantId) {
        FinanceReportResponse financeReportResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.GetFinance_Report;
            String pathParm = MessageFormat.format("{0}/{1}?tenantId={2}", startDate, endDate, tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            financeReportResponse = (FinanceReportResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new FinanceReportResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return financeReportResponse;
    }

    public OrderResponse getAllFailedExchanges(final Long deliveryCenterId, final Integer start, final Integer limit, final String sortBy, final String dir, final String tenantId) {
        OrderResponse orderResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.getAllFailedExchanges;
            String pathParm = MessageFormat.format("{0}/{1}/{2}/{3}/{4}?tenantId={5}", deliveryCenterId, start, limit, sortBy, dir, tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            orderResponse = (OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderResponse;
    }

    public OrderResponse getAllFailedOrdersForDCBaedOnTripDate(TripEntry entry) {
        OrderResponse orderResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.getAll_FailedOrdersForDC_BaedOnTripDate;

            Svc service = HttpExecutorService.executeHttpService(pathURI, null, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(entry), Headers.getLmsHeaderXML());
            orderResponse = (OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderResponse;
    }

    public OrderResponse getAllOnHoldOrdersForDCBasedOnFilter(TripEntry entry) {
        OrderResponse orderResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.getAll_OnHoldOrdersFor_DCBasedOnFilter;

            Svc service = HttpExecutorService.executeHttpService(pathURI, null, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(entry), Headers.getLmsHeaderXML());
            orderResponse = (OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderResponse;
    }

    public TripResponse isAnyTripOpenedForToday(final Long tripId) {
        TripResponse tripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.IsAnyTripOpened_ForToday;
            String pathParm = Long.toString(tripId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public TripResponse getOpenedTripsOnStaff(final Long tripId) {
        TripResponse tripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.GetOpened_TripsOnStaff;
            String pathParm = Long.toString(tripId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public TripResponse autoCardEnabled(final Long dcId, final Long dfId) {
        TripResponse tripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.AutoCard_Enabled;
            String pathParm = MessageFormat.format("?dcId={0}&dfId={1}", dcId, dfId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public TripResponse getActiveTripForOrder(final String orderId, final String tenantId) {
        TripResponse tripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.get_ActiveTrip_ForOrder;
            String pathParm = MessageFormat.format("{0}?tenantId={1}",orderId,tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public TripOrderAssignmentResponse assignOrderToTrip(TripOrderAssignmentResponse d, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_ASSIGN_ORDERVersion2;
            String pathParm = MessageFormat.format("?tenantId={0}",tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripResponse updateOdometerReading(final Long tripId, final Long startOdometerReading, final Long endOdometerReading, final String createdBy) throws WebClientException {
        TripResponse tripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Update_Odometer_Reading;
            String pathParm = MessageFormat.format("{0}/{1}/{2}/{3}",tripId,startOdometerReading,endOdometerReading,createdBy);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public TripOrderAssignmentResponse unassignOrderFromTrip(final String tripOrderId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.unassign_OrderFrom_Trip;

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{tripOrderId}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse unassignOrderFromTripThroughTripId(final String orderId, final String trackingNumber, final Long tripId, final ShipmentType shipmentType) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.unassign_Order_FromTrip;
            String tripIds=Long.toString(tripId);
            String pathParm = MessageFormat.format("{0}/{1}/{2}/{3}",orderId,trackingNumber,tripIds,shipmentType);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse addAndOutscanNewOrderToTrip(final String trackingNo, final Long tripId, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.AddAndOut_scanNewOrder_ToTrip;
            String tripIds=Long.toString(tripId);
            String pathParm = MessageFormat.format("{0}/{1}?tenantId={2}",trackingNo,tripIds,tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse findExchangesByTrip(final String tripNumber, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.findExchangesByTrip;
            String pathParm = MessageFormat.format("{0}?tenantId={1}",tripNumber,tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse findExchangesByTrip(final Long tripId, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.find_ExchangesBy_Trip;
            String tripIds=Long.toString(tripId);
            String pathParm = MessageFormat.format("{0}?tenantId={1}",tripIds,tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse unassignExchangeFromTrip(final String orderId, final String returnId, final Long tripId, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.unassignExchangeFromTrip;
            String tripIds=Long.toString(tripId);
            String pathParm = MessageFormat.format("{0}/{1}/{2}?tenantId={3}",orderId,returnId,tripIds,tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }
    public TripResponse getAllAvailableTripsForDCByStaffType(final Long deliveryCenterId, final DeliveryStaffType staffType) throws WebClientException {
        TripResponse tripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.GetAllAvailableTripsForDCByStaffType;
            String pathParm = MessageFormat.format("{0}/{1}",deliveryCenterId,staffType);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripResponse;
    }
    //    public OrderResponse getAllIncompleteOrdersForDC(final Long deliveryCenterId, final ShipmentType shipment_type, final int start, final int limit, final String sortBy, final String dir, final String tenantId) throws WebClientException {
//        OrderResponse orderResponse = null;
//        try {
//
//            orderResponse = tripServiceV2Client.getAllIncompleteOrdersForDC(deliveryCenterId, shipment_type, start, limit, sortBy, dir, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
//        } catch (WebClientException e) {
//            e.printStackTrace();
//        }
//        return orderResponse;
//    }
//
//    public OrderResponse getAllIncompleteExchangesForDC(final Long deliveryCenterId, final int start, final int limit, final String sortBy, final String dir, final String tenantId) throws WebClientException {
//        OrderResponse orderResponse = null;
//        try {
//
//            orderResponse = tripServiceV2Client.getAllIncompleteExchangesForDC(deliveryCenterId, start, limit, sortBy, dir, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
//        } catch (WebClientException e) {
//            e.printStackTrace();
//        }
//        return orderResponse;
//    }

//    public OrderResponse getAllIncompleteScheduledShipmentsForDC(final Long deliveryCenterId, final ShipmentType shipment_type, final int start, final int limit, final String sortBy, final String dir, final String tenantId) throws WebClientException {
//        OrderResponse orderResponse = null;
//        try {
//
//            orderResponse = tripServiceV2Client.getAllIncompleteScheduledShipmentsForDC(deliveryCenterId, shipment_type, start, limit, sortBy, dir, tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
//        } catch (WebClientException e) {
//            e.printStackTrace();
//        }
//        return orderResponse;
//    }

    public TripShipmentAssociationResponse findShipmentByTripNumber(final String tripNumber, final ShipmentType shipmentType, final String tenantId) throws WebClientException {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.findShipment_By_TripNumber;
            String pathParm = MessageFormat.format("{0}?tenantId={1}&shipmentType={2}", tripNumber,tenantId,shipmentType);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripShipmentAssociationResponse = (TripShipmentAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());



        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripShipmentAssociationResponse;
    }

    public TripResponse findById(final Long id) throws WebClientException {
        TripResponse tripResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.TRIP_Find_By_Id;
            String pathParm = Long.toString(id);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripResponse;
    }

    public String receiveShipmentFromAdmin(String trackingNumber) {
        String pathParam = "?operationalTrackingId=" + trackingNumber + "&tenantId=" + LMS_CONSTANTS.TENANTID + "&adminReceive=true";
        String response="";
        try {
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.RECEIVE_SHIPMENT_BEFORE_TRIP_COMPLETE, new String[]{pathParam}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, null, Headers.getLmsHeaderJSON());
            response = APIUtilities.getElement(service.getResponseBody(), "tripOrderResponse.status.statusType", "json");
        }catch (Exception e){
            e.printStackTrace();
        }
//        TripOrderResponse tripOrderResponse=(TripOrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderResponse());
        return response;
    }

    public Integer generateOTP(Long tripID) {
        String pathParam = String.valueOf(tripID);
        Svc service= null;
        try {
             service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.GENERATE_OTP, new String[]{pathParam}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, null, Headers.getLmsHeaderJSON());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return Integer.valueOf(service.getResponseBody().trim());

    }

    /**
     * Start Trip API has been changed after introducing mobile number masking.
     *
     * @param tripId
     * @return TripOrderAssignmentResponse
     * Assertion not required. Since this API behaviour is quite different as status message changes time to time (initial=PROCESSING, after 2mins=OFD)
     */
    public TripOrderAssignmentResponse initiateStartTrip(String tripId) {
        LastmileServiceHelper lastmileServiceHelper = new LastmileServiceHelper();
        // generate otp
        int otp= generateOTP(Long.valueOf(tripId));
        TripOrderAssignmentResponse tripOrderResponse = null;
        try {
            TripOrderAssignmentResponse tripOrderAssignmentResponse1=lmsServiceHelper.getTripOrderDetails(Long.valueOf(tripId));
            if(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase("Success")){
                List<TripOrderAssignementEntry> tripOrderAssignementEntries=tripOrderAssignmentResponse1.getTripOrders();
                for(int i=0;i<=tripOrderAssignementEntries.size()-1;i++){
                    MobileNumberMaskingHelper mobileNumberMaskingHelper=new MobileNumberMaskingHelper();
                    mobileNumberMaskingHelper.generateVirtualNumberByTrackingId(tripOrderAssignementEntries.get(i).getTrackingNumber());
                }
            }
            tripOrderResponse = lastmileServiceHelper.initiateStartTrip(tripId, String.valueOf(otp));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lastmile client and start the trip");
        }
        return tripOrderResponse;
    }


    /**
     * Same as initiate Start Trip, but we dont generate VN explicitly here.
     */
    public TripOrderAssignmentResponse initiateStartTripForMNM(Long tripId) {
        LastmileServiceHelper lastmileServiceHelper = new LastmileServiceHelper();
        // generate otp
        int otp= generateOTP(Long.valueOf(tripId));
        TripOrderAssignmentResponse tripOrderResponse = null;
        try {
            tripOrderResponse = lastmileServiceHelper.initiateStartTrip(String.valueOf(tripId), String.valueOf(otp));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lastmile client and start the trip");
        }
        return tripOrderResponse;
    }


    public TripOrderAssignmentResponse startTrip(Long tripId){
        TripOrderAssignmentResponse tripOrderAssignmentResponse;
        if(LASTMILE_CONSTANTS.USE_INITIATE_START_TRIP){
            tripOrderAssignmentResponse = initiateStartTrip(String.valueOf(tripId));
        }else{
            tripOrderAssignmentResponse= startTripOld(tripId);
        }
        return tripOrderAssignmentResponse;
    }

    @SneakyThrows
    public TripResponse searchTripDetails(long deliveryCenterId, Date tripDateStart, Date tripDateEnd, TripStatus tripStatus, long deliveryStaffId, String tenantId) {
        TripEntry tripEntry = new TripEntry();
        tripEntry.setDeliveryCenterId(deliveryCenterId);
        tripEntry.setTripDateStart(tripDateStart);
        tripEntry.setTripDateEnd(tripDateEnd);
        tripEntry.setTripStatus(tripStatus);
        tripEntry.setDeliveryStaffId(deliveryStaffId);
        tripEntry.setIsInbound(false);
        tripEntry.setTenantId(tenantId);

        Svc service = null;
        TripResponse tripResponse = null;
        try {
            service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.SEARCH_TRIP_DETAILS, null, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(tripEntry), Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripResponse;
    }




}