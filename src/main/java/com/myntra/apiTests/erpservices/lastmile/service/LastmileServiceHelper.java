package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.groot.domain.entry.numberMask.PhoneNumberMaskEntry;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.VirtualNumberResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.testng.Assert;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bharath.MC
 * @since Feb-2019
 */
public class LastmileServiceHelper {

    private void ValidateBasicHTTPResponse(Svc service) {
        int httpStatusCode = service.getResponseStatus();
        String httpErrorMessage = service.getResponseStatus() + ":" + Response.Status.fromStatusCode(httpStatusCode).getReasonPhrase();
        Assert.assertEquals(service.getResponseStatus(), 200, "Invoking API failed! HTTP Status Code " + httpErrorMessage);
    }

    public VirtualNumberResponse getVirualNumberByTrackingNumber(String tenantId, String trackingNumber) throws Exception {
        List<String> virtualNumbers = new ArrayList<>();
        String serviceType = SERVICE_TYPE.Last_mile.toString();
        String pathURI = Constants.LASTMILE_PATH.GET_VIRTUAL_NUMBER;
        String pathParams = MessageFormat.format("tenant/{0}/trackingNumber/{1}", tenantId, trackingNumber);
        String responsePayload;
        Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        responsePayload = service.getResponseBody();
        //doing this replacement, since response class field mapping with API response is not matching
        //responsePayload = responsePayload.replace("partyAPin" , "virtalNumberCustomerPin").replace("partyBPin" , "virtualNumberPartyBPin");
        VirtualNumberResponse virtualNumberResponse =(VirtualNumberResponse) APIUtilities.convertXMLStringToObject(responsePayload, new VirtualNumberResponse());
        ValidateBasicHTTPResponse(service);
        return virtualNumberResponse;
    }

    public VirtualNumberResponse generateVirtualNumberDCManager(String tenantId, String deliveryCenterId, String trackingNumber) throws Exception {
        List<String> virtualNumbers = new ArrayList<>();
        String serviceType = SERVICE_TYPE.Last_mile.toString();
        String pathURI = Constants.LASTMILE_PATH.GENERATE_VIRTUAL_NUMBER;
        String pathParams = MessageFormat.format("tenant/{0}/deliveryCenter/{1}/trackingNumber/{2}", tenantId, deliveryCenterId, trackingNumber);
        String payload = "";
        Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                HTTPMethods.POST, null, Headers.getLmsHeaderXML());
        VirtualNumberResponse virtualNumberResponse =(VirtualNumberResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new VirtualNumberResponse());
        ValidateBasicHTTPResponse(service);
        return virtualNumberResponse;
    }

    //TODO map the request class to PhoneNumberMaskEntry
    public VirtualNumberResponse genrateVirtualNumbersAndPin(String event ,String clientReferenceId ,String connectionId ,String virtualNumber ,String partyOnePins ,String partyTwoPins) throws Exception {
        List<String> virtualNumbers = new ArrayList<>();
        String serviceType = SERVICE_TYPE.Last_mile.toString();
        String pathURI = Constants.LASTMILE_PATH.BULK_UPDATE_VIRTUAL_NUMBER;
        String payloadBody = "['{'\"event\": \"{0}\",\"clientReferenceId\": \"{1}\",\"connectionId\": \"{2}\",\"allocationDetails\": '{'\"virtualNumber\": {3}'}',\"partyOnePins\": [{4}],\"partyTwoPins\": [{5}],\"status\": \"ALLOCATED\"'}']";
        String payload = MessageFormat.format(payloadBody , event ,clientReferenceId ,connectionId , virtualNumber , partyOnePins , partyTwoPins);
        Svc service = HttpExecutorService.executeHttpService(pathURI, null, serviceType,
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        ValidateBasicHTTPResponse(service);
        VirtualNumberResponse virtualNumberResponse =(VirtualNumberResponse) APIUtilities.jsonToObject(service.getResponseBody(), VirtualNumberResponse.class);
        return virtualNumberResponse;
    }

    public VirtualNumberResponse generateVirtualNumbersAndPin(List<PhoneNumberMaskEntry> phoneNumberMaskEntries) throws Exception {
        List<String> virtualNumbers = new ArrayList<>();
        String serviceType = SERVICE_TYPE.Last_mile.toString();
        String pathURI = Constants.LASTMILE_PATH.BULK_UPDATE_VIRTUAL_NUMBER;
        Svc service = HttpExecutorService.executeHttpService(pathURI, null, serviceType,
                HTTPMethods.POST, phoneNumberMaskEntries.toString(), Headers.getLmsHeaderJSON());
        ValidateBasicHTTPResponse(service);
        VirtualNumberResponse virtualNumberResponse =(VirtualNumberResponse) APIUtilities.jsonToObject(service.getResponseBody(), VirtualNumberResponse.class);
        return virtualNumberResponse;
    }

    public TripOrderAssignmentResponse initiateStartTrip(String tripId, String otp) throws UnsupportedEncodingException, JAXBException {
        String serviceType = SERVICE_TYPE.Last_mile.toString();
        String pathURI = Constants.LASTMILE_PATH.INITIATE_TRIP;
        //generating otp here

        String pathParams = MessageFormat.format("{0}/", tripId) +"?otp="+otp;
        Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                HTTPMethods.PUT, null, Headers.getLmsHeaderXML());
        ValidateBasicHTTPResponse(service);
        TripOrderAssignmentResponse tripOrderResponse =(TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        return tripOrderResponse;
    }

    public TripResponse refreshTrips(String deliveryCenterId) throws UnsupportedEncodingException, JAXBException {
        String serviceType = SERVICE_TYPE.Last_mile.toString();
        String pathURI = Constants.LASTMILE_PATH.INITIATE_TRIP;
        String pathParams = MessageFormat.format("deliveryCenter/{0}", deliveryCenterId);
        Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                HTTPMethods.PUT, null, Headers.getLmsHeaderXML());
        ValidateBasicHTTPResponse(service);
        TripResponse tripResponse =(TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripResponse());
        return tripResponse;
    }

    /**
     * Refresh API in Trip Planning Page
     * Type: FORWARD Orders: Pickups, Deliveries, Try and Buys
     * HTTP Method: GET
     */
    public OrderResponse getAllIncompleteOrdersForDC(Long deliveryCenterId, ShipmentType shipment_type, int start, int limit, String sortBy, String dir, String tenantId, String startDate, String endDate) throws Exception {
        List<String> virtualNumbers = new ArrayList<>();
        String serviceType = SERVICE_TYPE.Last_mile.toString();
        String pathURI = Constants.LASTMILE_PATH.TRIP_FORWARD_REFRESH;
        String urIpath = "{0}/{1}/{2}/{3}?sortBy={4}&dir={5}&tenantId={6}&startDate={7}&endDate={8}";
        String pathParams = MessageFormat.format(urIpath, deliveryCenterId, shipment_type, start, limit, sortBy, dir, tenantId, startDate, endDate);
        Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        ValidateBasicHTTPResponse(service);
        OrderResponse tripOrderResponse =(OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderResponse());
        return tripOrderResponse;
    }


    /**
     * Refresh API in Trip Planning Page
     * Type: Scheduled Pickups
     * HTTP Method: GET
     */
    public OrderResponse getAllIncompleteScheduledShipmentsForDC(Long deliveryCenterId, ShipmentType shipment_type, int start, int limit, String sortBy, String dir, String tenantId, String startDate, String endDate) throws Exception {
        List<String> virtualNumbers = new ArrayList<>();
        String serviceType = SERVICE_TYPE.Last_mile.toString();
        String pathURI = Constants.LASTMILE_PATH.TRIP_SCHEDULED_REFRESH;
        String urIpath = "{0}/{1}/{2}/{3}?sortBy={4}&dir={5}&tenantId={6}&startDate={7}&endDate={8}";
        String pathParams = MessageFormat.format(urIpath, deliveryCenterId, shipment_type, start, limit, sortBy, dir, tenantId, startDate, endDate);
        Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        ValidateBasicHTTPResponse(service);
        OrderResponse tripOrderResponse =(OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderResponse());
        return tripOrderResponse;
    }

    /**
     * Refresh API in Trip Planning Page
     * Type: Exchanges
     * HTTP Method: GET
     */
    public OrderResponse getAllIncompleteExchangesForDC(Long deliveryCenterId, int start, int limit, String sortBy, String dir, String tenantId, String startDate, String endDate) throws Exception {
        List<String> virtualNumbers = new ArrayList<>();
        String serviceType = SERVICE_TYPE.Last_mile.toString();
        String pathURI = Constants.LASTMILE_PATH.TRIP_EXCHANGE_REFRESH;
        String urIpath = "{0}/{1}/{2}?sortBy={3}&dir={4}&tenantId={5}&startDate={6}&endDate={7}";
        String pathParams = MessageFormat.format(urIpath, deliveryCenterId, start, limit, sortBy, dir, tenantId, startDate, endDate);
        Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        ValidateBasicHTTPResponse(service);
        OrderResponse tripOrderResponse =(OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderResponse());
        return tripOrderResponse;
    }

    public static void main(String[] args) {
        System.out.println(Response.Status.fromStatusCode(416).getReasonPhrase());
    }
}
