package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lms.client.response.HubWareHouseConfigResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.junit.Assert;
import org.springframework.http.MediaType;

import java.text.MessageFormat;

public class HubWarehouseClient_QA {
    public HubWareHouseConfigResponse getHubWarehouseConfig(Long warehouseId, ShipmentType shipmentType, String tenantId,String clientId) {
        HubWareHouseConfigResponse hubWareHouseConfigResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH. HUB_WH_CONFIGURATION;
            String pathParm= MessageFormat.format("?warehouseIds={0}&shipmentType={1}&tenantId={2}&clientId={3}",warehouseId,shipmentType,tenantId,clientId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            hubWareHouseConfigResponse = (HubWareHouseConfigResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new HubWareHouseConfigResponse());

        } catch (Exception e) {
            Assert.fail("Unable to hit api to fetch hubWarehouseConfig");
        }
        return hubWareHouseConfigResponse;
    }
}
