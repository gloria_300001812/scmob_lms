package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLShipmentUpdate;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.springframework.http.MediaType;
import org.testng.Assert;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;

public class MLShipmentClientV2_QA
{
    public MLShipmentResponse getMLShipmentDetails(String trackingumber, String tenantId) {
        MLShipmentResponse mlShipmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.findByTrackingNumber;
            String pathParm = MessageFormat.format("{0}?tenantId={1}",trackingumber,tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            mlShipmentResponse = (MLShipmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new MLShipmentResponse());

        } catch (Exception e) {
            Assert.fail("Unable to call lastmile api - to fetch MLShipment Details by tracking number");
        }
        return mlShipmentResponse;
    }

    public static HashMap<String, String> getLmsHeaderXML() {
        HashMap<String, String> createOrderHeaders = new HashMap<String, String>();
        createOrderHeaders.put("Authorization", "Basic bG1zYWRtaW5+bG1zYWRtaW46dGVzdA==");
        createOrderHeaders.put("Content-Type", "Application/json");
        createOrderHeaders.put("Accept", "application/json");
        return createOrderHeaders;
    }

    public com.myntra.lastmile.client.response.TripResponse createStoreTrip(List<String> trackingNo, long deliveryStaffId) {
        com.myntra.lastmile.client.response.TripResponse tripResponse=null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.createTripForBulkShipments;
            String pathParm = Long.toString(deliveryStaffId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST,APIUtilities.getObjectToJSON(trackingNo), getLmsHeaderXML());
            tripResponse = (com.myntra.lastmile.client.response.TripResponse) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(), com.myntra.lastmile.client.response.TripResponse.class,true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to create store trip and connect to lastmile client");
        }
        return tripResponse;
    }

    public MLShipmentResponse updateStatus(MLShipmentUpdate mlShipmentUpdate) throws WebClientException {
        MLShipmentResponse mlShipmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.ML_SHIPMENT_UPDATEVersion2;

            Svc service = HttpExecutorService.executeHttpService(pathURI, null, serviceType,
                    HTTPMethods.POST,APIUtilities.convertXMLObjectToString(mlShipmentUpdate), Headers.getLmsHeaderXML());

            mlShipmentResponse = (MLShipmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new MLShipmentResponse());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return mlShipmentResponse;
    }


    public OrderResponse getTryAndBuyItems(final String tenantId, final String trackingNumber) throws WebClientException {
        OrderResponse orderResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.getTryAndBuyItemsml;
            String pathParm=MessageFormat.format("{0}/items?tenantId={1}",trackingNumber,tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            orderResponse = (OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderResponse;
    }

}
