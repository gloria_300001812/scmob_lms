package com.myntra.apiTests.erpservices.lastmile.model;

import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import lombok.Data;
import org.joda.time.DateTime;

@Data
public class SelfShipReturnQCUpdate {
    private String clientId;

    private String shipmentId;

    private String eventLocation;

    private String[] returnShipmentUpdates;

    private String tenantId;

    private ShipmentUpdateActivityTypeSource shipmentUpdateMode;

    private DateTime eventTime;

    private EventLocationPremise eventLocationPremise;

    private ShipmentUpdateEvent event;

    private String remarks;

    private ShipmentType shipmentType;



}
