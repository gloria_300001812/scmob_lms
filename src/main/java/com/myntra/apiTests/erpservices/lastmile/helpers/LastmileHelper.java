package com.myntra.apiTests.erpservices.lastmile.helpers;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.common.Constants.LambdaInterfaces;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.client.DeliveryCenterClient;
import com.myntra.apiTests.erpservices.lastmile.client.DeliveryStaffClient;
import com.myntra.apiTests.erpservices.lastmile.client.StoreClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.lastmile.client.code.utils.*;
import com.myntra.lastmile.client.code.utils.DeliveryStaffCommute;
import com.myntra.lastmile.client.code.utils.DeliveryStaffType;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;

import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.DeliveryCenterRegion;
import com.myntra.lastmile.client.code.utils.DeliveryCenterType;
import com.myntra.lastmile.client.code.utils.DeliveryStaffCommute;
import com.myntra.lastmile.client.code.utils.*;
import com.myntra.lastmile.client.entry.*;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.response.OrderShipmentAssociationEntry;
import com.myntra.lms.client.response.ShipmentEntry;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.*;

import com.myntra.logistics.masterbag.core.MasterbagType;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.lordoftherings.redis.RedisUtil;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Random;


import static com.myntra.lms.client.status.DeliveryStaffCommute.*;
import static org.assertj.core.util.Dates.yesterday;


public class LastmileHelper {
    /**
     * flushRedisForServieabilityKeys
     */
    @SuppressWarnings("unchecked")
    public LambdaInterfaces.SimpleCall flushRedisForDC = () -> {
        List<String> keys = new ArrayList<>();
        keys.add("deliverCenterSearchCacheV1*");

        RedisUtil.getRedisTemplate("erp").getConnectionFactory().getConnection().select(LASTMILE_CONSTANTS.REDIS_DB_INDEX);
        RedisUtil.getRedisTemplate("erpredis_6379").delete(keys);
    };
    DeliveryCenterClient deliveryCenterClient;
    DeliveryStaffClient deliveryStaffClient;
    StoreClient storeClient;
    MasterBagClient masterBagClient;
    MLShipmentClientV2 mlShipmentServiceV2Client;
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa=new DeliveryStaffClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();

    public LastmileHelper(String env, String clientId, String tenantId) {
        this.deliveryCenterClient = new DeliveryCenterClient(env, clientId, tenantId);
        this.storeClient = new StoreClient(env, clientId, tenantId);
        this.masterBagClient = new MasterBagClient(env, clientId, tenantId);
        mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        this.deliveryStaffClient = new DeliveryStaffClient(env,LMS_CONSTANTS.CLIENTID , LMS_CONSTANTS.TENANTID);
    }

    public void deleteDC(String pincode) {
        DBUtilities.exUpdateQuery("delete from delivery_center where pincode= '" + pincode + "'", "lms");

    }

    public void deleteDCStaffForPincode(String pincode) {

        DBUtilities.exUpdateQuery("delete from delivery_staff where delivery_center_id in (select id from delivery_center where pincode= '" + pincode + "')", "lms");

    }

    public void deleteTripsForDC(String pincode) {

        DBUtilities.exUpdateQuery("delete from trip where delivery_center_id in (select id from delivery_center where pincode= '" + pincode + "')", "lms");

    }

    public StoreResponse createStore(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                     String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) {
        StoreEntry storeEntry = new StoreEntry();
        storeEntry.setName(name);
        storeEntry.setOwnerFirstName(ownerFirstName);
        storeEntry.setOwnerLastName(ownerLastName);
        storeEntry.setLatLong(latLong);
        storeEntry.setEmailId(emailId);
        storeEntry.setContactNumber(mobileNumber);
        storeEntry.setAddress(address);
        storeEntry.setPincode(pincode);
        storeEntry.setCity(city);
        storeEntry.setState(state);
        storeEntry.setMappedDcCode(mappedDcCode);
        storeEntry.setGstin(gstin);
        storeEntry.setTenantId(tenantId);
        storeEntry.setIsAvailable(isAvailable);
        storeEntry.setIsDeleted(isDeleted);
        storeEntry.setIsCardEnabled(isCardEnabled);
        storeEntry.setRating(rating);
        storeEntry.setStoreType(storeType);
        storeEntry.setReconType(hlpReconType);
        storeEntry.setCapacity(capacity);
        storeEntry.setCode(code);

        StoreResponse storeResponse = storeClient_qa.createStore(storeEntry);
        if (storeResponse.getStatus().getStatusType().name().equals( StatusResponse.Type.ERROR.name())) {
            Assert.fail("Store creation has failed "+storeResponse.getStatus().getStatusMessage());
        }
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getCode().equals(code));
        Assert.assertNotNull(storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId());
        String searchParams = "code.like:" + code;
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Assert.assertTrue(storeResponse2.getStoreEntries().get(0).getContactNumber().equals(mobileNumber));
        StoreResponse storeResponse3 = storeClient_qa.findStoreById(storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId());
        Assert.assertTrue(storeResponse3.getStoreEntries().get(0).getCode().equals(code));
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId());
        DBUtilities.exUpdateQuery("update delivery_staff set `is_mobile_verified` = 1 where id = " + storeDeliveryStaffId + " ; ","lms");
        return storeResponse;
    }


    public StoreResponse createStore(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                     String gstin, String tenantId, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code,boolean allowsAlteration,boolean isActive) {
        StoreEntry storeEntry = new StoreEntry();
        storeEntry.setName(name);
        storeEntry.setOwnerFirstName(ownerFirstName);
        storeEntry.setOwnerLastName(ownerLastName);
        storeEntry.setLatLong(latLong);
        storeEntry.setEmailId(emailId);
        storeEntry.setContactNumber(mobileNumber);
        storeEntry.setAddress(address);
        storeEntry.setPincode(pincode);
        storeEntry.setCity(city);
        storeEntry.setState(state);
        storeEntry.setMappedDcCode(mappedDcCode);
        storeEntry.setGstin(gstin);
        storeEntry.setTenantId(tenantId);
        storeEntry.setIsDeleted(isDeleted);
        storeEntry.setIsCardEnabled(isCardEnabled);
        storeEntry.setRating(rating);
        storeEntry.setStoreType(storeType);
        storeEntry.setReconType(hlpReconType);
        storeEntry.setCapacity(capacity);
        storeEntry.setCode(code);
        storeEntry.setAllowsAlteration(allowsAlteration);
        storeEntry.setActive(isActive);

        StoreResponse storeResponse = storeClient_qa.createStore(storeEntry);
        if (storeResponse.getStatus().getStatusType().name().equals( StatusResponse.Type.ERROR.name())) {
            Assert.fail("Store creation has failed "+storeResponse.getStatus().getStatusMessage());
        }
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getCode().equals(code));
        Assert.assertNotNull(storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId());
        String searchParams = "code.like:" + code;
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Assert.assertTrue(storeResponse2.getStoreEntries().get(0).getContactNumber().equals(mobileNumber));
        StoreResponse storeResponse3 = storeClient_qa.findStoreById(storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId());
        Assert.assertTrue(storeResponse3.getStoreEntries().get(0).getCode().equals(code));
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId());
        DBUtilities.exUpdateQuery("update delivery_staff set `is_mobile_verified` = 1 where id = " + storeDeliveryStaffId + " ; ","lms");
        return storeResponse;
    }

    public ShipmentResponse createMasterBag(long originPremisesId, PremisesType originPremisesType,
                                            long destinationPremisesId, PremisesType destinationPremisesType, ShippingMethod shippingMethod, String tenantId, String originCity, String destinationCity)
            throws IOException, JAXBException {


        ShipmentEntry shipment = new ShipmentEntry();
        shipment.setTenantId(tenantId);
        shipment.setOriginPremisesId(originPremisesId);
        shipment.setOriginPremisesType(originPremisesType);
        shipment.setOriginCity(originCity);
        shipment.setDestinationPremisesId(destinationPremisesId);
        shipment.setDestinationPremisesType(destinationPremisesType);
        shipment.setDestinationCity(destinationCity);
        shipment.setCapacity(200);
        shipment.setStatus(ShipmentStatus.NEW);
        shipment.setShippingMethod(shippingMethod);

        //TODO : Use client
        String payload = APIUtilities.convertXMLObjectToString(shipment);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.MASTER_BAG, null, SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        ShipmentResponse shipmentResponse = (ShipmentResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new ShipmentResponse());
        if (shipmentResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("MasterBag creation failed");
        }
        return shipmentResponse;
    }


    public DeliveryCenterResponse createDC(DeliveryCenterType type, String code, String name, String courier, Long store, String manager, String contactNumber, String address, String city,
                                           String cityCode, String state, String pincode, Integer masterbagCapacity, String transportHub, String registeredAddressLine1, String registeredAddressLine2,
                                           String registeredCity, String registeredPincode, String registeredStateName, DeliveryCenterRegion deliveryCenterRegion, String registeredCompanyName, String companyIdentificationNumber,
                                           Boolean selfShipSupported, Boolean active, Boolean bulkInvoiceEnabled, Boolean isCardEnabled, Boolean isStrictServiceable, ReconType hlpReconType) {

        String searchParams = "pincode.like:" + pincode;

        DeliveryCenterEntry deliveryCenterEntry = new DeliveryCenterEntry();
        deliveryCenterEntry.setType(type);
        deliveryCenterEntry.setCode(code);
        deliveryCenterEntry.setName(name);
        deliveryCenterEntry.setCourierCode(courier);
        deliveryCenterEntry.setStoreId(store);
        deliveryCenterEntry.setManager(manager);
        deliveryCenterEntry.setContactNumber(contactNumber);
        deliveryCenterEntry.setAddress(address);
        deliveryCenterEntry.setCity(city);
        deliveryCenterEntry.setCityCode(cityCode);
        deliveryCenterEntry.setState(state);
        deliveryCenterEntry.setPincode(pincode);
        deliveryCenterEntry.setMasterbagCapacity(masterbagCapacity);
        deliveryCenterEntry.setTmsHubCode(transportHub);
        deliveryCenterEntry.setRegisteredAddressLine1(registeredAddressLine1);
        deliveryCenterEntry.setRegisteredAddressLine2(registeredAddressLine2);
        deliveryCenterEntry.setRegisteredCity(registeredCity);
        deliveryCenterEntry.setRegisteredPincode(registeredPincode);
        deliveryCenterEntry.setRegisteredStateName(registeredStateName);
        deliveryCenterEntry.setDcRegion(deliveryCenterRegion);
        deliveryCenterEntry.setRegisteredCompanyName(registeredCompanyName);
        deliveryCenterEntry.setCompanyIdentificationNumber(companyIdentificationNumber);
        deliveryCenterEntry.setSelfShipSupported(selfShipSupported);
        deliveryCenterEntry.setActive(active);
        deliveryCenterEntry.setBulkInvoiceEnabled(bulkInvoiceEnabled);
        deliveryCenterEntry.setIsCardEnabled(isCardEnabled);
        deliveryCenterEntry.setIsStrictServiceable(isStrictServiceable);
        deliveryCenterEntry.setReconType(hlpReconType);
        //TODO : If this is complusory, how it creates without providing
        // deliveryCenterEntry.setHlpReconTypeLastChangedOn(hlpReconTypeLastChangedOn);
        deliveryCenterClient_qa.createDeliveryCenter(deliveryCenterEntry);
//TODO :FLush redis

        //Check if it is added

        DeliveryCenterResponse dcResponseAfterAdding = deliveryCenterClient_qa.searchDCByParam(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        //without client:
        // DeliveryCenterResponse dcResponse = deliveryCenterClient.searchWithoutClient(pincode);
        String dcCode = null;
        try {
            dcCode = dcResponseAfterAdding.getDeliveryCenters().get(0).getCode();
        } catch (NullPointerException e) {
            Assert.fail("DC creation failed for the pincode " + pincode);
        } catch (IndexOutOfBoundsException e) {
            Assert.fail("DC creation failed for the pincode " + pincode);
        }

        System.out.println("The DC " + dcCode + " has been created for pincode " + pincode);
        Assert.assertTrue(dcResponseAfterAdding.getDeliveryCenters().get(0).getReconType().name().equals(hlpReconType.name()), "The RECON_TYPE is not matching");

        return dcResponseAfterAdding;
    }

    //TODO : To add this in validator
    public OrderShipmentAssociationStatus validateOrderAddedInStoreBag(Long masterBagId, String trackingNumber) {
        ShipmentResponse shipmentResponse = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getStatus().name().equals(ShipmentStatus.NEW.name()), "The masterBag status is NOT " + ShipmentStatus.ADDED_TO_TRIP.name());
        OrderShipmentAssociationStatus status = null;
        List<OrderShipmentAssociationEntry> packetIdList = shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries();
        for (OrderShipmentAssociationEntry entry : packetIdList) {
            if (entry.getTrackingNumber().equals(trackingNumber)) {
                status = entry.getStatus();
            }
        }
        return status;
    }

    public void validateStoreBagAddedToTrip(Long masterBagId, int noOfOrders, String code, Long originPremiseId) {
        ShipmentResponse shipmentResponse = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getStatus().name().equals(ShipmentStatus.ADDED_TO_TRIP.name()), "The masterBag status is NOT " + ShipmentStatus.ADDED_TO_TRIP.name());
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().size() == noOfOrders, "NO. of orders in MB is not matching ");
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getOrderCount() == noOfOrders, "NO. of orders in MB is not matching ");
        //Assert.assertTrue(shipmentResponse.getEntries().get(0).getDestinationPremisesCode().equals(code), "The store code and destination premise code of MB not matching");
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getCourier().equals(code), "The courier code and MB courierCode not matching");
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getDestinationPremisesType().name().equals(PremisesType.DC.name()), "The store premiseType and destination premise type of MB not matching");
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getOriginPremisesId().equals(originPremiseId), "The origin premise id is not matching");

    }

    public void validateMasterBagCreation(Long masterBagId, String courierCode) {
        ShipmentResponse shipmentResponse = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getType().name().equals(MasterbagType.DC_TO_HLP.name()), "The masterBag type is not DC_TP_HLP");
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID));
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getCourier().equals(courierCode));
    }

    public void validateTripStartedForMB(Long masterBagId) {

        //Validate shipment status is IN_TRANIT and shipment_order_map status of each order in the bag is IN_TRANSIT
        ShipmentResponse shipmentResponse = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getStatus().name().equals(ShipmentStatus.IN_TRANSIT.name()), "The masterBag status is NOT " + ShipmentStatus.IN_TRANSIT.name());
        OrderShipmentAssociationStatus status = null;
        List<OrderShipmentAssociationEntry> packetIdList = shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries();
        for (OrderShipmentAssociationEntry entry : packetIdList) {
            Assert.assertTrue(entry.getStatus().name().equals(OrderShipmentAssociationStatus.IN_TRANSIT.name()), "The order with trackingNumber : " + entry.getTrackingNumber() + " is not IN_TRANSIT");
        }
    }

    public void validateForReverseMbEntriesAfterPickup(Long masterBagId,List<String> failedDeliveryList) {

        ShipmentResponse shipmentResponse = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getStatus().name().equals(ShipmentStatus.IN_TRANSIT.name()), "The masterBag status is NOT " + ShipmentStatus.IN_TRANSIT.name());
        Assert.assertFalse(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().isEmpty());
        List<OrderShipmentAssociationEntry> packetIdList = shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries();
        for (OrderShipmentAssociationEntry entry : packetIdList) {
            Assert.assertTrue(failedDeliveryList.contains(entry.getTrackingNumber()));
            Assert.assertTrue(entry.getStatus().name().equals(OrderShipmentAssociationStatus.IN_TRANSIT.name()), "The order with trackingNumber : " + entry.getTrackingNumber() + " is not IN_TRANSIT");
        }
    }


    public void validateEmptyReverseBag(Long masterBagId,List<String> failedDeliveryList) {

        ShipmentResponse shipmentResponse = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getStatus().name().equals(ShipmentStatus.IN_TRANSIT.name()), "The masterBag status is NOT " + ShipmentStatus.IN_TRANSIT.name());
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().isEmpty());

    }

    public void validateForReverseMbAddedToTrip(Long masterBagId) {

        ShipmentResponse shipmentResponse = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getStatus().name().equals(ShipmentStatus.ADDED_TO_TRIP.name()), "The masterBag status is NOT " + ShipmentStatus.IN_TRANSIT.name());
        Assert.assertTrue(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().isEmpty());

    }
    public OrderShipmentAssociationStatus getShipmentOrderMapStatus(String trackingNo, long masterBagId) {
        ShipmentResponse shipmentResponse = masterBagClient.getShipmentOrderMap(masterBagId);
        return shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().stream().filter(entry -> entry.getTrackingNumber().equals(trackingNo)).findFirst().get().getStatus();

    }

    public void validateStoreSummaryBeforeDelivering(Long originPremiseId, Long storeHlPId, List<String> forwardTrackingNumbersList, int noOfOrders) {
        StoreSummaryResponse storeSummaryResponse = storeClient_qa.storeSummaryResponse(originPremiseId, storeHlPId);
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getTotalAmountCollected() == null);
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getTotalAmountToBeCollected().equals(0.0f));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getForwardShipmentsSummary().getTotalAttempted() == 0);
        for (MLShipmentEntry entry : storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getForwardShipmentsSummary().getMlShipmentEntries()) {
            Assert.assertTrue(forwardTrackingNumbersList.contains(entry.getTrackingNumber()));
            Assert.assertTrue(entry.getShipmentStatus().equals(com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY.name()));
        }
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getForwardShipmentsSummary().getTotalCount().equals(noOfOrders));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getForwardShipmentsSummary().getTotalAttempted().equals(0));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getForwardShipmentsSummary().getTotalSuccess().equals(0));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getForwardShipmentsSummary().getTotalUnAttempted().equals(noOfOrders));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getTotalAmountToBeCollected() == 0.0);

    }

    public void validateStoreSummaryBeforeDeliveringExchange(Long originPremiseId, Long storeHlPId, List<String> forwardTrackingNumbersList, int noOfOrders) {
        StoreSummaryResponse storeSummaryResponse = storeClient_qa.storeSummaryResponse(originPremiseId, storeHlPId);
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getTotalAmountCollected() == null);
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getTotalAmountToBeCollected().equals(0.0f));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getExchangeShipmentsSummary().getTotalAttempted() == 0);
        for (MLShipmentEntry entry : storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getExchangeShipmentsSummary().getMlShipmentEntries()) {
            Assert.assertTrue(forwardTrackingNumbersList.contains(entry.getTrackingNumber()));
            Assert.assertTrue(entry.getShipmentStatus().equals(com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY.name()));
        }
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getExchangeShipmentsSummary().getTotalCount().equals(noOfOrders));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getExchangeShipmentsSummary().getTotalAttempted().equals(0));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getExchangeShipmentsSummary().getTotalSuccess().equals(0));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getExchangeShipmentsSummary().getTotalUnAttempted().equals(noOfOrders));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getTotalAmountToBeCollected() == 0.0);

    }
    public void validateStoreSummaryAfterReconcilation(Long originPremiseId, Long storeHlPId) {
        StoreSummaryResponse storeSummaryResponse = storeClient_qa.storeSummaryResponse(originPremiseId, storeHlPId);
        Assert.assertNull(storeSummaryResponse.getStoreSummaries());
        Assert.assertTrue(storeSummaryResponse.getStatus().getStatusMessage().toLowerCase().equals("success"));

    }
    public void validateUnattemptedAfterReconcilation(Long storeDcId) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.getUnattemptedShipmets(storeDcId);
        Assert.assertTrue(tripOrderAssignmentResponse.getTripOrders().size()==0);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().toLowerCase().equals("success"));
    }

    public void validateUnattemptedAfterReconcilation(Long storeDcId,int noOfUnAttempted) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.getUnattemptedShipmets(storeDcId);
        Assert.assertTrue(tripOrderAssignmentResponse.getTripOrders().size()==noOfUnAttempted);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().toLowerCase().equals("success"));
    }

    public void validStoreSummaryCount(Long originPremiseId, Long storeHlPId, int totalAttempted, int totalCount, int totalSuccess, int totalUnattempted) {
        StoreSummaryResponse storeSummaryResponse = storeClient_qa.storeSummaryResponse(originPremiseId, storeHlPId);

        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getForwardShipmentsSummary().getTotalCount().equals(totalCount));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getForwardShipmentsSummary().getTotalAttempted() == totalAttempted);
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getForwardShipmentsSummary().getTotalSuccess().equals(totalSuccess));
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getForwardShipmentsSummary().getTotalUnAttempted().equals(totalUnattempted));
    }

    public void validStoreSummaryAmtToBeCollected(Long originPremiseId, Long storeHlPId, float amount) {
        StoreSummaryResponse storeSummaryResponse = storeClient_qa.storeSummaryResponse(originPremiseId, storeHlPId);
        Assert.assertTrue(storeSummaryResponse.getStoreSummaries().get(0).getStoreSummaryEntry().getTotalAmountToBeCollected().equals(amount));
    }

    public void validateStoreMLEntryDelivered(Long originPremiseId, Long storeHlPId, String storeTenantId, String trackingNumber) {
        StoreSummaryResponse storeSummaryResponse = storeClient_qa.storeSummaryResponse(originPremiseId, storeHlPId);
        MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
        //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

        //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
        //Validate the client id is 4019 and tenantId is storeTenant id
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));


        //Validate the status for the Store Tenant ML entry as DELIVERED
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));
        //Validate DC id is the store Id
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));

        //TODO : Check with LA
        //   Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getLastAttemptDate());

        //Validate lastAttempt reason code
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getLastAttemptReasonCode().name().equals(AttemptReasonCode.DELIVERED.name()));
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getReceivedAt().equals("HOME"));
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getReceivedBy().equals("SELF"));
        //TODO : Check with Azeem
        //  Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getSignatureUrl().contains("Gloria"));
        Assert.assertNull(mlShipmentResponse.getMlShipmentEntries().get(0).getFailedAttemptCount());


    }

    public void validateStoreMLEntryFailedDeliveredb4CloseTrip(Long originPremiseId, Long storeHlPId, String storeTenantId, String trackingNumber) {
        StoreSummaryResponse storeSummaryResponse = storeClient_qa.storeSummaryResponse(originPremiseId, storeHlPId);
        MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
        //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

        //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
        //Validate the client id is 4019 and tenantId is storeTenant id
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));


        //Validate the status for the Store Tenant ML entry as FAILED_DELIVERY
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));
        //Validate DC id is the store Id
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));

        //TODO : Check with Azeem
        //   Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getLastAttemptDate());

        //Validate lastAttempt reason code
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getLastAttemptReasonCode().name().equals(AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS.name()));
        Assert.assertNull(mlShipmentResponse.getMlShipmentEntries().get(0).getReceivedAt());
        Assert.assertNull(mlShipmentResponse.getMlShipmentEntries().get(0).getReceivedBy());
        Assert.assertNull(mlShipmentResponse.getMlShipmentEntries().get(0).getSignatureUrl());


    }

    public void validateReverseBagShipmentItemCnt(String tripNumber, String clientid, int shipmentCount) {
        TripShipmentAssociationResponse reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(tripNumber, ShipmentType.REVERSE_BAG, clientid);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getShipmentItemCount().equals(String.valueOf(shipmentCount)));

    }

    public void validateReverseBagAmount(String tripNumber, String clientid, float amountCollected, float amountToBeCollected) {
        TripShipmentAssociationResponse reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(tripNumber, ShipmentType.REVERSE_BAG, clientid);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getAmountCollected() == amountCollected);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getAmountToBeCollected() == amountToBeCollected);

    }

    public void validateReverseFailedDeliveries(String tripNumber, String clientid, int totalFailedDeliveriesPicked, int totalFailedDeliveriesToPick) {
        TripShipmentAssociationResponse reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(tripNumber, ShipmentType.REVERSE_BAG, clientid);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getTotalFailedDeliveriesPicked() == totalFailedDeliveriesPicked);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getTotalFailedDeliveriesToPick() == totalFailedDeliveriesToPick);

    }

    public void validateReverseSuccess(String tripNumber, String clientid, int totalSuccess, int totalSuccessfulReturnsPicked, int totalSuccessfulReturnsToPick) {
        TripShipmentAssociationResponse reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(tripNumber, ShipmentType.REVERSE_BAG, clientid);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getTotalSuccess()==totalSuccess);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getTotalSuccessfulReturnsPicked()== totalSuccessfulReturnsPicked);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getTotalSuccessfulReturnsToPick()== totalSuccessfulReturnsToPick);

    }

    public void validateReverseUnAttempted(String tripNumber, String clientid, int totalUnAttempted) {
        TripShipmentAssociationResponse reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(tripNumber, ShipmentType.REVERSE_BAG, clientid);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getTotalUnAttempted()== totalUnAttempted);

    }

    public void validateStoreMLEntryFailedDeliveredAfterCloseTrip(String storeTenantId, String trackingNumber) {
        MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(),(MLDeliveryShipmentStatus.RTO_CONFIRMED.name()),"ML status not matching");

    }

    public void validateMLStatus(String storeTenantId, String trackingNumber,MLDeliveryShipmentStatus status) {
        MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(),(status.name()),"ML status not matching");

    }

    public StoreResponse updateStore (String name,  String address, String pincode, String city, String state, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled,  String code, ReconType updatehlpReconType , long HlpDCId)
    {
        StoreEntry storeEntry = new StoreEntry();
        storeEntry.setName(name);
        storeEntry.setAddress(address);
        storeEntry.setPincode(pincode);
        storeEntry.setCity(city);
        storeEntry.setState(state);
        storeEntry.setIsAvailable(isAvailable);
        storeEntry.setIsDeleted(isDeleted);
        storeEntry.setIsCardEnabled(isCardEnabled);
        storeEntry.setReconType(updatehlpReconType);
        storeEntry.setCode(code);
        StoreResponse storeResponse = storeClient_qa.updateStore(storeEntry, HlpDCId);
        // Assert.assertTrue(storeResponse.getStoreEntries().get(0).getCode().equals(code));
        return storeResponse;
    }

    public StoreResponse updateStore (String name,String ownerFirstName,String ownerLastName,String address, String pinCode,
                                      String city, boolean isActive,boolean isCardEnabled,boolean isAllowsAlteration,
                                      String contactNumber, StoreType storeType, String mappedDcCode,String code, long hlpDCId)
    {
        StoreEntry storeEntry = new StoreEntry();
        storeEntry.setId(hlpDCId);
        storeEntry.setCode(code);
        storeEntry.setName(name);
        storeEntry.setOwnerFirstName(ownerFirstName);
        storeEntry.setOwnerLastName(ownerLastName);
        storeEntry.setAddress(address);
        storeEntry.setCity(city);
        storeEntry.setPincode(pinCode);
        storeEntry.setActive(isActive);
        storeEntry.setIsCardEnabled(isCardEnabled);
        storeEntry.setAllowsAlteration(isAllowsAlteration);
        storeEntry.setContactNumber(contactNumber);
        storeEntry.setStoreType(storeType);
        storeEntry.setMappedDcCode(mappedDcCode);


        StoreResponse storeResponse = storeClient_qa.updateStore(storeEntry, hlpDCId);
        return storeResponse;
    }

    public DeliveryStaffResponse createStoreDeliveryStaff (Long DcId , String tenantId ,int contactNumber , DeliveryStaffType deliveryStaffType , DeliveryStaffCommute deliveryStaffCommute) throws Exception {

        DeliveryStaffEntry deliveryStaffEntry = new DeliveryStaffEntry();
        deliveryStaffEntry.setCode("DS" + String.valueOf(contactNumber).substring(5, 9));
        deliveryStaffEntry.setFirstName("D"+ String.valueOf(contactNumber).substring(5, 9));
        deliveryStaffEntry.setLastName("S"+ String.valueOf(contactNumber).substring(5, 9));
        deliveryStaffEntry.setDeliveryCenterId(DcId);
        deliveryStaffEntry.setEmpCode("" + String.valueOf(contactNumber).substring(5, 9));
        deliveryStaffEntry.setMobile("" + contactNumber);
        deliveryStaffEntry.setModeOfCommute(deliveryStaffCommute);
        deliveryStaffEntry.setTenantId(tenantId);
        deliveryStaffEntry.setAvailable(true);
        deliveryStaffEntry.setDeleted(false);
        deliveryStaffEntry.setIsCardEnabled(true);
        deliveryStaffEntry.setDeliveryStaffType(deliveryStaffType);
        DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient_qa.create(deliveryStaffEntry);
        return deliveryStaffResponse;
    }



    public void validateDeliveryStaffCreation (Long deliveryStaffId , Long deliveryCenterId , String tenantId , String deliveryStaffType) throws Exception {
        DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient_qa.findById(deliveryStaffId);
        Assert.assertEquals(deliveryStaffResponse.getDeliveryStaffs().get(0).getDeliveryCenterId(),deliveryCenterId,"delivery center not macthing");
        Assert.assertEquals(deliveryStaffResponse.getDeliveryStaffs().get(0).getTenantId(),tenantId,"tenantId not macthing");
        List resultSet = DBUtilities.exSelectQuery("select `delivery_staff_role` from `delivery_staff` where id = " + deliveryStaffId, "myntra_lms");
        HashMap<String, Object>row = (HashMap<String, Object>) resultSet.get(0);
        Assert.assertEquals(row.get("delivery_staff_role"),deliveryStaffType,"deliveryStaffType not macthing");
    }



    //
//    public DeliveryStaffResponse UpdateStoreDeliveryStaff1 ( Long SDAId ,Long DcId , String tenantId ,int contactNumber , boolean isAvailable , boolean isDeleted , boolean isCardEnabled , DeliveryStaffRole deliveryStaffRole) throws WebClientException, JAXBException, UnsupportedEncodingException {
//
//        DeliveryStaffEntry1 deliveryStaffEntry = new DeliveryStaffEntry1();
//        deliveryStaffEntry.setDeliveryCenterId(DcId);
//        deliveryStaffEntry.setMobile("" + contactNumber);
//        deliveryStaffEntry.setModeOfCommute(DeliveryStaffCommute.BIKER);
//        deliveryStaffEntry.setCode("DS" + String.valueOf(contactNumber).substring(5, 9));
//        deliveryStaffEntry.setFirstName("D"+ String.valueOf(contactNumber).substring(5, 9));
//        deliveryStaffEntry.setLastName("S"+ String.valueOf(contactNumber).substring(5, 9));
//        deliveryStaffEntry.setEmpCode("" + String.valueOf(contactNumber).substring(5, 9));
//        deliveryStaffEntry.setDeliveryStaffType(DeliveryStaffType.STORE);
//        deliveryStaffEntry.setTenantId(tenantId);
//        deliveryStaffEntry.setAvailable(isAvailable);
//        deliveryStaffEntry.setDeleted(isDeleted);
//        deliveryStaffEntry.setIsCardEnabled(isCardEnabled);
//        deliveryStaffEntry.setDeliveryStaffRole(deliveryStaffRole);
//
//        List<String> SDAIDs = new ArrayList<>();
//        SDAIDs.add(SDAId.toString());
//
//        String payload = APIUtilities.convertXMLObjectToString(deliveryStaffEntry);
//        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_DELIVERYSTAFF,  SDAIDs, SERVICE_TYPE.Last_mile.toString(),
//                HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
//        DeliveryStaffResponse deliveryStaffResponse = (DeliveryStaffResponse) APIUtilities
//                .convertXMLStringToObject(service.getResponseBody(), new DeliveryStaffResponse());
//        if (DeliveryStaffResponse.getStatus().getStatusMessage().contains("Error")) {
//            Assert.fail("SDA update failed");
//        }
//        return deliveryStaffResponse;
//    }
//

    public DeliveryStaffResponse UpdateStoreDeliveryStaff ( Long SDAId ,Long DcId , String tenantId ,int contactNumber , boolean isAvailable , boolean isDeleted , boolean isCardEnabled ) throws Exception {

        DeliveryStaffEntry deliveryStaffEntry = new DeliveryStaffEntry();
        deliveryStaffEntry.setDeliveryCenterId(DcId);
        deliveryStaffEntry.setMobile("" + contactNumber);
        deliveryStaffEntry.setModeOfCommute(DeliveryStaffCommute.BIKER);
        deliveryStaffEntry.setCode("DS" + String.valueOf(contactNumber).substring(5, 9));
        deliveryStaffEntry.setFirstName("D"+ String.valueOf(contactNumber).substring(5, 9));
        deliveryStaffEntry.setLastName("S"+ String.valueOf(contactNumber).substring(5, 9));
        deliveryStaffEntry.setEmpCode("" + String.valueOf(contactNumber).substring(5, 9));
        deliveryStaffEntry.setDeliveryStaffType(DeliveryStaffType.MYNTRA_PAYROLL);
        deliveryStaffEntry.setTenantId(tenantId);
        deliveryStaffEntry.setAvailable(isAvailable);
        deliveryStaffEntry.setDeleted(isDeleted);
        deliveryStaffEntry.setIsCardEnabled(isCardEnabled);
       // deliveryStaffEntry.setDeliveryStaffRole();
        DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient_qa.update(deliveryStaffEntry,SDAId);
        return deliveryStaffResponse;
    }

    public StoreResponse UpdateStoreInActive (String name,  String address, String pincode, String city, String state, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled,Boolean isActive ,  String code, ReconType updatehlpReconType , long HlpDCId) {
        StoreEntry storeEntry = new StoreEntry();
        storeEntry.setName(name);
        storeEntry.setAddress(address);
        storeEntry.setPincode(pincode);
        storeEntry.setCity(city);
        storeEntry.setState(state);
        storeEntry.setIsAvailable(isAvailable);
        storeEntry.setIsDeleted(isDeleted);
        storeEntry.setActive(isActive);
        storeEntry.setTenantId(tenantId);
        storeEntry.setIsCardEnabled(isCardEnabled);
        storeEntry.setReconType(updatehlpReconType);
        storeEntry.setCode(code);
        StoreResponse storeResponse = storeClient_qa.updateStore(storeEntry, HlpDCId);
        // Assert.assertTrue(storeResponse.getStoreEntries().get(0).getCode().equals(code));
        return storeResponse;
    }

    public void validateMLShipmentDetails (String trackingNo , MLDeliveryShipmentStatus mlDeliveryShipmentStatus ,Long originPremiseId , String tenantId , String clientId) {
        MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        //Validate tenant is 4019 and client is 2297
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(clientId));
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(tenantId));
        //Validate the source reference id is the orderId
        //  Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId().equals(trackingNoOrderIdMap.get(trackingNo)));
        //Validate the status for the Store Tenant ML entry as HANDED_OVER_TO_LASTMILE_PARTNER
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(mlDeliveryShipmentStatus.name()));
        //Validate the DC id is 5
        Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(originPremiseId));
    }

    public void modifiedMLLastmilePartnerShipmentDate(String courierCode){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String updateOrderTracking = "update ml_last_mile_partner_shipment_assignment set last_modified_on = '" +dateFormat.format(yesterday())+ "' where courier_code = '"+courierCode+"';" ;
        try{
            DBUtilities.exUpdateQuery(updateOrderTracking, "myntra_lms");
        }catch(Exception e) {
            Assert.fail("ML lastmile partner shipment table last modified date is not successfully changed");
        }

    }


    @NotNull
    public static String GetStartDate() {
        return LocalDate.now().minusDays(30).toString() + "%2000%3A00%3A00";
    }

    @NotNull
    public static String GetEndDate() {
        return LocalDate.now().toString() + "%2023%3A59%3A59";
    }

}
