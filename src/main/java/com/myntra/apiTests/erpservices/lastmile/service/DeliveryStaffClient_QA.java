package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.springframework.http.MediaType;
import org.testng.Assert;

import java.text.MessageFormat;

public class DeliveryStaffClient_QA
{
    /*
     * searchDeliveryStaff
     * Http method:- GET
     * */
    public DeliveryStaffResponse findDeliveryStaffByMobNo(String mobile) {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.DELIVERY_Staff_Search;
            String urIpath = "?q=available.eq:true___mobile.eq:{0}";
            String pathParams = MessageFormat.format(urIpath, mobile);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());

            deliveryStaffResponse = (DeliveryStaffResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryStaffResponse());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lastmile client");
        }
        if (deliveryStaffResponse.getStatus().getStatusMessage().toLowerCase().contains("ERROR".toLowerCase())) {
            Assert.fail("Error while fetchin Delivery staff details");
        }
        try {
            Assert.assertEquals(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile(),mobile,"The delivery staff is not matching with the mobile number");
        } catch (Exception e) {
            Assert.fail("No delivery staff found for the mobile number");
        }
        return deliveryStaffResponse;
    }

    /*
     * CreateDeliveryStaff
     * Http method:- POST
     * */
    public DeliveryStaffResponse create(DeliveryStaffEntry deliveryStaffEntry) throws Exception
    {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Create_Delivery_Staff;

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(deliveryStaffEntry), Headers.getLmsHeaderXML());

            deliveryStaffResponse = (DeliveryStaffResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryStaffResponse());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to create deliveryStaff / check lastmile is up");
        }
        return deliveryStaffResponse;
    }

    /*
     * UpdateDeliveryStaff
     * Http method:- PUT
     * */
    public DeliveryStaffResponse update(DeliveryStaffEntry deliveryStaffEntry , long id) throws Exception {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Create_Delivery_Staff;
            String pathParm = Long.toString(id);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.PUT, APIUtilities.convertXMLObjectToString(deliveryStaffEntry), Headers.getLmsHeaderXML());

            deliveryStaffResponse = (DeliveryStaffResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryStaffResponse());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to update deliveryStaff / check lastmile is up");
        }
        return deliveryStaffResponse;
    }
    /*
     * FinfByDeliveryStaff throufg ID
     * Http method:- GET
     * */

    public DeliveryStaffResponse findById(final Long id) throws Exception {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Create_Delivery_Staff;
            String pathParm = Long.toString(id);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            deliveryStaffResponse = (DeliveryStaffResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryStaffResponse());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to find deliveryStaff by deliveryStaff Id / check lastmile is up");
        }
        return deliveryStaffResponse;
    }

    /*
     * searchDeliveryStaffByDeliveryCenterId
     * Http method:- GET
     * */

    public Long searchDeliveryStaffByDeliveryCenterId(long deliveryCenterId)
    {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH. DELIVERY_Staff_Search;
            String urIpath = "?q=deliveryCenter.id.eq:{0}___available.eq:true";
            String dcID=Long.toString(deliveryCenterId);
            String pathParams = MessageFormat.format(urIpath,dcID);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            deliveryStaffResponse = (DeliveryStaffResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryStaffResponse());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lastmile client");
        }
        return  deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
    }
    /*
     * findDeliveryStaffByMobNo
     * Http method:- GET
     * */

    public Long findDeliveryStaffByMobNo2(String mobile) {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH. DELIVERY_Staff_Search;
            String urIpath = "?q=available.eq:true___mobile.eq:{0}";
            String pathParams = MessageFormat.format(urIpath,mobile);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            deliveryStaffResponse = (DeliveryStaffResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryStaffResponse());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lastmile client");
        }
        if (deliveryStaffResponse.getStatus().getStatusMessage().toLowerCase().contains("ERROR".toLowerCase())) {
            Assert.fail("Error while fetchin Delivery staff details");
        }
        try {
            Assert.assertTrue(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile().equals(mobile), "The delivery staff is not matching with the mobile number");
        } catch (NullPointerException e) {
            Assert.fail("No delivery staff found for the mobile number");
        }
        return deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
    }


    public  DeliveryStaffResponse searchDeliveryStaffbyDeliveryStaffId(long deliveryStaffId){
        DeliveryStaffResponse deliveryStaffResponse = null;
        try{
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH. DELIVERY_STAFF;
            String pathParm = Long.toString(deliveryStaffId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            deliveryStaffResponse = (DeliveryStaffResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryStaffResponse());
        }catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to Connect to Lastmile client");
        }
        return  deliveryStaffResponse;

    }

    /*
     * searchDeliveryStaffByDeliveryCenterId
     * Http method:- GET
     * */

    public DeliveryStaffResponse searchDeliveryStaffByDeliveryCenterId2(long deliveryCenterId) {
        DeliveryStaffResponse deliveryStaffResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH. DELIVERY_Staff_Search;
            String urIpath = "?q=deliveryCenter.id.eq:{0}___available.eq:true";
            String dcId=Long.toString(deliveryCenterId);
            String pathParams = MessageFormat.format(urIpath,dcId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            deliveryStaffResponse = (DeliveryStaffResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryStaffResponse());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to connect to lastmile client");
        }
        return  deliveryStaffResponse;
    }
}
