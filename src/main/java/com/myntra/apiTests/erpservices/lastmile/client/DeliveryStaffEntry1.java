package com.myntra.apiTests.erpservices.lastmile.client;

import com.myntra.lastmile.client.code.utils.DeliveryStaffCommute;
import com.myntra.lastmile.client.code.utils.DeliveryStaffRole;
import com.myntra.lastmile.client.code.utils.DeliveryStaffType;

import java.util.Date;

public class DeliveryStaffEntry1  {
    private String firstName;
    private String lastName;
    private String mobile;
    private Long deliveryCenterId;
    private String deliveryCenterName;
    private String deliveryCenterCode;
    private String rootDeliveryCenterCode;
    private String rootDeliveryCenterId;
    private String deliveryCenterCity;
    private Boolean available = true;
    private Boolean deleted = false;
    private String code;
    private String empCode;
    private DeliveryStaffCommute modeOfCommute;
    private Boolean isCardEnabled = false;
    private Boolean isMobileVerified;
    private String mobileType;
    private String appVersion;
    private String deviceModel;
    private String deviceOsVersion;
    private String tshirtSize;
    private String trouserSize;
    private String shoeSize;
    private Date joiningDate;
    private String noticePeriod;
    private String shirtType;
    private DeliveryStaffType deliveryStaffType;
    private String storeAddress;
    private String deliveryCenterRegisteredCompany;
    private String tenantId;
    private DeliveryStaffRole deliveryStaffRole;

    public DeliveryStaffEntry1() {
    }

    public DeliveryStaffRole getdeliveryStaffRole() {
        return this.deliveryStaffRole;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getMobile() {
        return this.mobile;
    }

    public Long getDeliveryCenterId() {
        return this.deliveryCenterId;
    }

    public String getDeliveryCenterName() {
        return this.deliveryCenterName;
    }

    public String getDeliveryCenterCode() {
        return this.deliveryCenterCode;
    }

    public String getRootDeliveryCenterCode() {
        return this.rootDeliveryCenterCode;
    }

    public String getRootDeliveryCenterId() {
        return this.rootDeliveryCenterId;
    }

    public String getDeliveryCenterCity() {
        return this.deliveryCenterCity;
    }

    public Boolean getAvailable() {
        return this.available;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public String getCode() {
        return this.code;
    }

    public String getEmpCode() {
        return this.empCode;
    }

    public DeliveryStaffCommute getModeOfCommute() {
        return this.modeOfCommute;
    }

    public Boolean getIsCardEnabled() {
        return this.isCardEnabled;
    }

    public Boolean getIsMobileVerified() {
        return this.isMobileVerified;
    }

    public String getMobileType() {
        return this.mobileType;
    }

    public String getAppVersion() {
        return this.appVersion;
    }

    public String getDeviceModel() {
        return this.deviceModel;
    }

    public String getDeviceOsVersion() {
        return this.deviceOsVersion;
    }

    public String getTshirtSize() {
        return this.tshirtSize;
    }

    public String getTrouserSize() {
        return this.trouserSize;
    }

    public String getShoeSize() {
        return this.shoeSize;
    }

    public Date getJoiningDate() {
        return this.joiningDate;
    }

    public String getNoticePeriod() {
        return this.noticePeriod;
    }

    public String getShirtType() {
        return this.shirtType;
    }

    public DeliveryStaffType getDeliveryStaffType() {
        return this.deliveryStaffType;
    }

    public String getStoreAddress() {
        return this.storeAddress;
    }

    public String getDeliveryCenterRegisteredCompany() {
        return this.deliveryCenterRegisteredCompany;
    }

    public String getTenantId() {
        return this.tenantId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setDeliveryCenterId(Long deliveryCenterId) {
        this.deliveryCenterId = deliveryCenterId;
    }

    public void setDeliveryCenterName(String deliveryCenterName) {
        this.deliveryCenterName = deliveryCenterName;
    }

    public void setDeliveryCenterCode(String deliveryCenterCode) {
        this.deliveryCenterCode = deliveryCenterCode;
    }

    public void setRootDeliveryCenterCode(String rootDeliveryCenterCode) {
        this.rootDeliveryCenterCode = rootDeliveryCenterCode;
    }

    public void setRootDeliveryCenterId(String rootDeliveryCenterId) {
        this.rootDeliveryCenterId = rootDeliveryCenterId;
    }

    public void setDeliveryCenterCity(String deliveryCenterCity) {
        this.deliveryCenterCity = deliveryCenterCity;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public void setModeOfCommute(DeliveryStaffCommute modeOfCommute) {
        this.modeOfCommute = modeOfCommute;
    }

    public void setIsCardEnabled(Boolean isCardEnabled) {
        this.isCardEnabled = isCardEnabled;
    }

    public void setIsMobileVerified(Boolean isMobileVerified) {
        this.isMobileVerified = isMobileVerified;
    }

    public void setMobileType(String mobileType) {
        this.mobileType = mobileType;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    public void setTshirtSize(String tshirtSize) {
        this.tshirtSize = tshirtSize;
    }

    public void setTrouserSize(String trouserSize) {
        this.trouserSize = trouserSize;
    }

    public void setShoeSize(String shoeSize) {
        this.shoeSize = shoeSize;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public void setNoticePeriod(String noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    public void setShirtType(String shirtType) {
        this.shirtType = shirtType;
    }

    public void setDeliveryStaffRole(DeliveryStaffRole deliveryStaffRole){this.deliveryStaffRole = deliveryStaffRole;}

    public void setDeliveryStaffType(DeliveryStaffType deliveryStaffType) {
        this.deliveryStaffType = deliveryStaffType;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public void setDeliveryCenterRegisteredCompany(String deliveryCenterRegisteredCompany) {
        this.deliveryCenterRegisteredCompany = deliveryCenterRegisteredCompany;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

}

