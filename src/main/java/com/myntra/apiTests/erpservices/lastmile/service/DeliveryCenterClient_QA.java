package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.client.DeliveryCenterClient;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.returnComplete.client.BaseClient;
import com.myntra.lastmile.client.entry.DeliveryCenterEntry;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.testng.Assert;
import org.testng.log4testng.Logger;

import java.text.MessageFormat;

public class DeliveryCenterClient_QA
{
    static Logger log = Logger.getLogger(DeliveryCenterClient.class);

    /*
     *Create Delivery Center
     * HTTP Method: post
     */
    public DeliveryCenterResponse createDeliveryCenter(DeliveryCenterEntry deliveryCenterEntry) {
        DeliveryCenterResponse deliveryCenterResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.CREATE_DELIVERY_CENTER;

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(deliveryCenterEntry), Headers.getLmsHeaderXML());
            deliveryCenterResponse = (DeliveryCenterResponse)  APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryCenterResponse());
        } catch (Exception e) {
            Assert.fail("Unable to create Delivery Center, unable to call lastmile client");
        }
        Assert.assertNotNull(deliveryCenterResponse);
        return deliveryCenterResponse;
    }

    /*
     *Update Delivery Center
     *HTTP Method: put
     */
    public DeliveryCenterResponse updateDeliveryCenter(DeliveryCenterEntry deliveryCenterEntry, Long DCId) {
        DeliveryCenterResponse deliveryCenterResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.UPDATE_DELIVERY_CENTER;
            String pathParm = Long.toString(DCId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.PUT, APIUtilities.convertXMLObjectToString(deliveryCenterEntry), Headers.getLmsHeaderXML());

            deliveryCenterResponse = (DeliveryCenterResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryCenterResponse());
        } catch (Exception e) {
            Assert.fail("Unable to update Delivery Center, unable to call lastmile client");
        }
        Assert.assertNotNull(deliveryCenterResponse);
        return deliveryCenterResponse;
    }

    /*
     *Update Delivery Center
     *HTTP Method: get
     */
    public DeliveryCenterResponse searchDCByParam(int start, int fetchSize, String sortBy, String sortOrder, Boolean distinct, String q, String f) {
        DeliveryCenterResponse deliveryCenterResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.DELIVERY_CENTER_SEARCH;
            String urIpath ="?q={5}&f={6}&fetchSize={1}&sortOrder={3}&start={0}&distinct={4}&sortBy={2}";
            String pathParams = MessageFormat.format(urIpath, start, fetchSize, sortBy, sortOrder, distinct, q, f);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());

            deliveryCenterResponse = (DeliveryCenterResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryCenterResponse());

        } catch (Exception e) {
            Assert.fail("Unable to search Delivery Center, unable to call lastmile client");
        }
        Assert.assertNotNull(deliveryCenterResponse);
        return deliveryCenterResponse;
    }

    public long getOriginPremiseIdOfDC(String pincode, String tenantId) {
        String searchParams = "pincode.like:" + pincode + "___tenantId.eq:" + LMS_CONSTANTS.TENANTID;
        DeliveryCenterResponse dcResponse = searchDCByParam(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Long originPremiseId = dcResponse.getDeliveryCenters().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        return originPremiseId;
    }

    public String getCityOfDC(String pincode, String tenantId) {
        String searchParams = "pincode.like:" + pincode + "___tenantId.eq:" + LMS_CONSTANTS.TENANTID;
        DeliveryCenterResponse dcResponse = searchDCByParam(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        log.info("The city  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + dcResponse.getDeliveryCenters().get(0).getCity());
        System.out.println("The city  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + dcResponse.getDeliveryCenters().get(0).getCity());

        return dcResponse.getDeliveryCenters().get(0).getCity();
    }

}
