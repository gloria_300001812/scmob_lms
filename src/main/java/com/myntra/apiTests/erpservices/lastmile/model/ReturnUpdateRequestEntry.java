package com.myntra.apiTests.erpservices.lastmile.model;

import com.myntra.returns.common.enums.CancellationType;
import com.myntra.returns.common.enums.code.ReturnActionCode;
import com.myntra.returns.entry.ReturnLineUpdateRequestEntry;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ReturnUpdateRequestEntry {

    private ReturnActionCode returnActionCode;

    private List<ReturnLineUpdateRequestEntryList> returnLineUpdateRequestEntryList;

    private String userComment;

    private String createdBy;

    private String courierService;

    private Long returnId;

    private CancellationType cancellationType;

    private Date createdOn;

    private String trackingNumber;



    @Override
    public String toString() {
        return "ClassPojo [returnActionCode = "+returnActionCode+", returnLineUpdateRequestEntryList = "+returnLineUpdateRequestEntryList+", userComment = "+userComment+", createdBy = "+createdBy+", courierService = "+courierService+", returnId = "+returnId+", cancellationType = "+cancellationType+", createdOn = "+createdOn+", trackingNumber = "+trackingNumber+"]";
    }

}
