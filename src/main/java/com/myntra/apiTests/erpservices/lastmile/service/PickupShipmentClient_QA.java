package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lms.client.domain.response.PickupResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ReturnType;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import io.qameta.allure.Step;
import org.springframework.http.MediaType;
import org.testng.Assert;

import java.text.MessageFormat;

public class PickupShipmentClient_QA {
    @Step("Manifesting the returns")
    public PickupResponse manifestReturn(ReturnType returnType, String courierCode, String startDate, String endDate,String tenantId,String sourceId) {
        PickupResponse pickupResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH.manifest;
            String pathParm= MessageFormat.format("/returnType={0}/courierCode={1}/startDate={2}/endDate{3}?tenantId={4}&sourceId={5}",returnType,courierCode,startDate,endDate,tenantId,sourceId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());
            pickupResponse = (PickupResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new PickupResponse());


        } catch (Exception e) {
            Assert.fail("Unable to call manifest api");
        }
        return pickupResponse;
    }


    //TODO : get the pickup id from here, verify tracking number from return details is same here,
    public PickupResponse findPickupFromSourceReturnId(String returnId,String tenantId,String sourceId) {
        PickupResponse pickupResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH.PICKUP_SHIPMENT_STATUS_LMS;
            String pathParm= MessageFormat.format("?sourceReturnId={0}tenantId={1}&sourceId={2}",returnId,tenantId,sourceId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            pickupResponse = (PickupResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new PickupResponse());

        } catch (Exception e) {
            Assert.fail("Unable to call  api-Find Pickup by returnId Id");
        }
        return pickupResponse;
    }

    public PickupResponse findPickupReturnId(String returnId,String tenantId,String sourceId) {
        PickupResponse pickupResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH.findBySourceReturnId;
            String pathParm= MessageFormat.format("returnId={0}?tenantId={1}&sourceId={2}",returnId,tenantId,sourceId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            pickupResponse = (PickupResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new PickupResponse());

        } catch (Exception e) {
            Assert.fail("Unable to call  api-Find Pickup by returnId Id");
        }
        return pickupResponse;
    }

    public PickupResponse findPickupByTrackingNumber(String trackingNumber,String tenantId) {
        PickupResponse pickupResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH.findByTrackingNumber;
            String pathParm= MessageFormat.format("?trackingNumber={0}&tenantId={1}",trackingNumber,tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getBasicHeaderJSON());
            pickupResponse = (PickupResponse) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(), PickupResponse.class, false);

        } catch (Exception e) {
            Assert.fail("Unable to call  api-Find Pickup by tracking number");
        }
        return pickupResponse;

    }
    public PickupResponse getTryAndBuyPickupDetails(String trackingNumber, Long itemIdentifier,String tenantId) {
        PickupResponse pickupResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH.getTryAndBuyPickupDetails;
            String pathParm= MessageFormat.format("/{0}/{1}?tenantId={2}",trackingNumber,itemIdentifier,tenantId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            pickupResponse = (PickupResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new PickupResponse());


        } catch (Exception e) {
            Assert.fail("Unable to call  api-Find Try and Buy Pickup by tracking number");
        }
        return pickupResponse;

    }
}
