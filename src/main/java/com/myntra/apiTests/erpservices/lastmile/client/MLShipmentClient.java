package com.myntra.apiTests.erpservices.lastmile.client;

import com.myntra.apiTests.erpservices.returnComplete.client.BaseClient;
import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.client.MLShipmentServiceV2Client;
import com.myntra.lastmile.client.client.TripServiceV2Client;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.TripUpdateDashboardInfoResponse;
import com.myntra.scm.utils.EnvironmentUtil;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class MLShipmentClient implements BaseClient {
    MLShipmentServiceV2Client mlShipmentServiceV2Client;
    String tenantId;
    String clientId;
    String serviceURL;

    public MLShipmentClient(String env, String clientId, String tenantId) {
        EnvironmentUtil environmentUtil = null;
        try {
            environmentUtil = new EnvironmentUtil();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        try {
            this.serviceURL = environmentUtil.formUrl(env, "lastmile") + "lastmile-service";

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.tenantId = tenantId;
        this.clientId = clientId;

        mlShipmentServiceV2Client = new MLShipmentServiceV2Client(this.serviceURL, 5, 10);
    }

    public MLShipmentResponse assignPickupToDC(final Long originDCID, final Long destDCId, final String tenantId, List<String> trackingNumbers) throws WebClientException {
        MLShipmentResponse  mlShipmentResponse = null;
        try {

            mlShipmentResponse = mlShipmentServiceV2Client.assignPickupToDC(originDCID,destDCId,tenantId,trackingNumbers, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return mlShipmentResponse;
    }

}
