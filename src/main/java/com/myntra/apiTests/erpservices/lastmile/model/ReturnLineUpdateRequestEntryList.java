package com.myntra.apiTests.erpservices.lastmile.model;


import lombok.Data;

@Data
public class ReturnLineUpdateRequestEntryList {

    private Integer quantity;

    private Long skuId;

}
