package com.myntra.apiTests.erpservices.lastmile.helpers;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.lastmile.client.client.TripServiceV2Client;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.scm.utils.EnvironmentUtil;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class LastmileTripPlanningSearchHelper {

    TripServiceV2Client tripServiceV2Client;
    String tenantId;
    String clientId;
    String serviceURL;
    int sizeOfRecords=20;

    public LastmileTripPlanningSearchHelper(String env, String clientId, String tenantId) {
        EnvironmentUtil environmentUtil = null;
        try {
            environmentUtil = new EnvironmentUtil();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        try {
            this.serviceURL = environmentUtil.formUrl(env, "lastmile") + "lastmile-service";

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.tenantId = tenantId;
        this.clientId = clientId;

        tripServiceV2Client = new TripServiceV2Client(this.serviceURL, 5, 10);
    }

    /**
     * this method gives the trip response for given combination of query params
     *
     * @param
     * @return
     */

    public TripResponse tripPlanningSerchBasedOnFilterSearch(String queryparam) throws UnsupportedEncodingException, JAXBException {
        Svc service = HttpExecutorService.executeHttpService(LASTMILE_CONSTANTS.Trip_Planning_ServiceURL.FILTER_SEARCH_LASTMILE, new String[]{queryparam}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        TripResponse tripResponse = (TripResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new TripResponse());
        return tripResponse;
    }

    /**
     * this method for retrive complete data for given query from database
     *
     * @param
     * @return
     */
    public static List<Map<String, Object>> getDbData(String query) {
        List<Map<String, Object>> tripIdCompleteQuery = DBUtilities.exSelectQuery(query, "lms");
        return tripIdCompleteQuery;
    }

    /**
     * this method compares the api response data and db data of given db query and given response
     *
     * @param
     * @return
     */
    public void compareAPIDBValues(TripResponse tripResponse, List<Map<String, Object>> dbValues) {
        List<TripEntry> tripEntries = tripResponse.getTrips();
        Assert.assertEquals(tripResponse.getStatus().getTotalCount(),dbValues.size(),"No of records are not matching");
        if(tripEntries.size()<sizeOfRecords){
            sizeOfRecords=tripEntries.size();
        }
        int j=0;
        while(j<sizeOfRecords){
            for (int i = 0; i < sizeOfRecords; i++) {
                if(tripEntries.get(j).getId().toString().equalsIgnoreCase(dbValues.get(i).get("id").toString())){
                    Assert.assertEquals(tripEntries.get(j).getTripNumber(), dbValues.get(i).get("trip_number").toString(), "trip number is not matching ");
                    Assert.assertEquals(tripEntries.get(j).getDeliveryStaffId().toString(), dbValues.get(i).get("delivery_staff_id").toString(), "delivery staff id is not matching ");
                    Assert.assertEquals(tripEntries.get(j).getTripStatus().toString(), dbValues.get(i).get("trip_status").toString(), "trip status is not matching ");
                    break;
                }else{
                    if(i==sizeOfRecords-1){
                        Assert.fail("after checking 20 records the api record - "+tripEntries.get(j).getId().toString()+" is not there is DB");
                    }
                }
            }
            j++;
        }
    }
    /**
     * this method compares the api response data and db data of given db query and given response
     *
     * @param
     * @return
     */
    public void compareAPIDBValues_Order_Response(OrderResponse orderResponse, List<Map<String, Object>> dbValues) {

        List<OrderEntry> orderEntries = orderResponse.getOrders();
        if(orderEntries.size()<sizeOfRecords){
            sizeOfRecords=orderEntries.size();
        }
        if (!(orderEntries.get(0).getTrackingNumber().equalsIgnoreCase((String) dbValues.get(0).get("tracking_number")))) {
            System.out.println("first record is not matching from API data and DB Data, From API Dtat is - " + orderEntries.get(0).getTrackingNumber()
                    + " and from DB first records is - " + dbValues.get(0).get("tracking_number"));
        }
        int j = 1;
        //TODO check when the records less then 20
        while (j < sizeOfRecords) {
            for (int i = 1; i < orderResponse.getOrders().size(); i++) {
                if (orderEntries.get(i).getTrackingNumber().equalsIgnoreCase(dbValues.get(i).get("tracking_number").toString())) {
                    Assert.assertEquals(orderEntries.get(i).getShipmentType().toString(), dbValues.get(i).get("shipment_type").toString(), "shipment type is not matching ");
                    Assert.assertEquals(orderEntries.get(i).getTenantId(), dbValues.get(i).get("tenant_id"), "tenant id is not matching ");
                    Assert.assertEquals(orderResponse.getStatus().getTotalCount(), dbValues.size(), "No of records are not matching");
                    Assert.assertEquals(orderEntries.get(i).getDeliveryCenterId(), dbValues.get(i).get("delivery_center_id"), "delivery center id not matching");
                    Assert.assertEquals(orderEntries.get(i).getMobile(), dbValues.get(i).get("recipient_contact_number"));
                    break;
                } else {
                    if (i == sizeOfRecords - 1) {
                        Assert.fail("after checking 20 records the api record - " + orderEntries.get(i).getTrackingNumber() + " is not there is DB");
                    }
                }
            }
            j++;
        }
    }

    /**
     * To verify refresh api response from trip planning page and respective DB data
     * @param orderResponse
     * @param dbValues
     */
    public void validateRefreshAPIResponseAndDBValues(OrderResponse orderResponse, List<Map<String, Object>> dbValues){

        List<OrderEntry> tripEntries = orderResponse.getOrders();
        if(tripEntries.size()<sizeOfRecords){
            sizeOfRecords=tripEntries.size();
        }
        int j = 0;
        while (j < sizeOfRecords) {
            for (int i = 0; i < tripEntries.size(); i++) {
                if(tripEntries.get(i).getTrackingNumber().equalsIgnoreCase(dbValues.get(i).get("tracking_number").toString())){
                    System.out.println("Iteration : "+i);
                    if(tripEntries.get(i).getShipmentType().toString().equalsIgnoreCase(ShipmentType.EXCHANGE.toString())){
                        System.out.println("Exchnage orderId From API is -"+tripEntries.get(i).getExchangeOrderId()+" and DB is - "+dbValues.get(i).get("source_reference_id").toString());
                        Assert.assertEquals(tripEntries.get(i).getExchangeOrderId(), dbValues.get(i).get("source_reference_id").toString(), "order_Id is not matching ");
                        Assert.assertEquals(tripEntries.get(i).getTrackingNumber(), dbValues.get(i).get("tracking_number").toString(), "tracking_number is not matching ");
                        Assert.assertEquals(tripEntries.get(i).getShipmentType().toString(), dbValues.get(i).get("shipment_type").toString(), "shipmentType not matching");
                        Assert.assertEquals(tripEntries.get(i).getTenantId(), dbValues.get(i).get("tenant_id").toString(), "tenant_Id is not matching");
                        break;

                    }else if (tripEntries.get(i).getShipmentType().toString().equalsIgnoreCase(ShipmentType.PU.toString())){
                        System.out.println("SourcereturnId API is -"+tripEntries.get(i).getSourceReturnId()+" and DB is - "+dbValues.get(i).get("source_return_id").toString());
                        Assert.assertEquals(tripEntries.get(i).getSourceReturnId(), dbValues.get(i).get("source_return_id").toString(), "source_return_id is not matching ");
                        Assert.assertEquals(tripEntries.get(i).getTrackingNumber(), dbValues.get(i).get("tracking_number").toString(), "tracking_number is not matching ");
                        Assert.assertEquals(tripEntries.get(i).getStatus().toString(), dbValues.get(i).get("status").toString(), "status is not matching ");
                        Assert.assertEquals(tripEntries.get(i).getShipmentType().toString(), dbValues.get(i).get("shipment_type").toString(), "shipmentType not matching");
                        Assert.assertEquals(tripEntries.get(i).getTenantId(), dbValues.get(i).get("tenant_id").toString(), "tenant_Id is not matching");
                        Assert.assertEquals(tripEntries.get(i).getClientId(), dbValues.get(i).get("client_id").toString(), "client_Id is not matching");
                        break;
                    }else {
                        System.out.println("orderIdFrom API is -"+tripEntries.get(i).getOrderId()+" and DB is - "+dbValues.get(i).get("order_id").toString());
                        Assert.assertEquals(tripEntries.get(i).getOrderId(), dbValues.get(i).get("order_id").toString(), "order_Id is not matching ");
                        Assert.assertEquals(tripEntries.get(i).getTrackingNumber(), dbValues.get(i).get("tracking_number").toString(), "tracking_number is not matching ");
                        Assert.assertEquals(tripEntries.get(i).getStatus().toString(), dbValues.get(i).get("status").toString(), "status is not matching ");
                        Assert.assertEquals(tripEntries.get(i).getShipmentType().toString(), dbValues.get(i).get("shipment_type").toString(), "shipmentType not matching");
                        Assert.assertEquals(tripEntries.get(i).getRtoHubCode(), dbValues.get(i).get("rto_hub_code").toString(), "rto hub code is not matching");
                        Assert.assertEquals(tripEntries.get(i).getTenantId(), dbValues.get(i).get("tenant_id").toString(), "tenant_Id is not matching");
                        Assert.assertEquals(tripEntries.get(i).getClientId(), dbValues.get(i).get("store_partner_id").toString(), "client_Id is not matching");
                        break;
                    }
                }else {
                    if (i == sizeOfRecords - 1) {
                        Assert.fail("after checking 20 records the api record - " + tripEntries.get(i).getShipmentType().toString()+ " is not there is DB");
                    }
                }

            }
            j++;
        }

    }



}
