/**
 * Created by Abhishek Jaiswal on 15/11/18.
 */

package com.myntra.apiTests.erpservices.lastmile.validator;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.lastmile.client.response.TripResponse;
import org.testng.Assert;

public class TripResponseValidator {
    TripResponse lmsTripResponse;

    public TripResponseValidator(TripResponse lmsTripResponse) {
        this.lmsTripResponse = lmsTripResponse;
        Assert.assertEquals(lmsTripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        if (lmsTripResponse.getTrips() == null || lmsTripResponse.getTrips().isEmpty())
            Assert.fail("Could not find trip in LMS");
    }

    /*
     * BASE MODULAR FUNCTIONS START For Trip Response*/


    public void validateDeliveryCenterId(int id) {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getDeliveryCenterId().intValue(), id, "Wrong delivery center id selected");
    }

    public void validateDeliveryStaffType(String DeliveryStaffType, int index)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(index).getDeliveryStaffEntry().getDeliveryStaffType().toString(), DeliveryStaffType, "Wrong Delivery Staff Returned");
    }

    public void validateDeliveryStaffType(String DeliveryStaffType)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getDeliveryStaffEntry().getDeliveryStaffType().toString(), DeliveryStaffType, "Wrong Delivery Staff Returned");
    }

    public void validateTripNumber(String tripNumber, int index)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(index).getTripNumber(), tripNumber,"TripNumber Mismatch");
    }

    public void validateTripNumber(String tripNumber)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getTripNumber(), tripNumber,"TripNumber Mismatch");
    }

    public void validateCODAmount(String amount)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getCodAmountCollected().toString(), amount, "Amount collected mismatch");
    }


    public void validateId(String Id , int index)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(index).getId().toString(),Id, "Invalid Id");
    }

    public void validateId(String Id)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getId().toString(),Id, "Invalid Id");
    }

    public void validateDeliveryCenterId(int id, int index) {
        Assert.assertEquals(lmsTripResponse.getTrips().get(index).getDeliveryCenterId().intValue(), id, "Wrong delivery center id selected");
    }

    public void validateCardEnabled(boolean isEnabled) {
        boolean isCardEnabled = lmsTripResponse.getTrips().get(0).getIsCardEnabled().booleanValue();
        Assert.assertTrue(isCardEnabled == isEnabled, "Expected value of card doesn't match");
    }

    public void validateCardEnabled(boolean isEnabled, int index) {
        boolean isCardEnabled = lmsTripResponse.getTrips().get(index).getIsCardEnabled().booleanValue();
        Assert.assertTrue(isCardEnabled == isEnabled, "Expected value of card doesn't match");
    }

    public void validateTripStatus(String status) {
        String tripStatus = lmsTripResponse.getTrips().get(0).getTripStatus().toString();
        Assert.assertEquals(tripStatus, status, "Trip status doesn't match");
    }

    public void validateTripStatus(String status, int index) {
        String tripStatus = lmsTripResponse.getTrips().get(index).getTripStatus().toString();
        Assert.assertEquals(tripStatus, status, "Trip status doesn't match");
    }

    public void validateDeliveryStaffId(String staffId)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getDeliveryStaffId().toString(), staffId, "Staff Id doesn't match");
    }

    public void validateDeliveryStaffId(String staffId, int index)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(index).getDeliveryStaffId().toString(), staffId, "Staff Id doesn't match");
    }

    public void validateCreatedBy(String name, int index)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(index).getDeliveryStaffEntry().getCreatedBy(), name, "Invalid data in created by");
    }

    public void validateCreatedBy(String name)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getDeliveryStaffEntry().getCreatedBy(), name, "Invalid data in created by");
    }

    public void validateMobileNum(String phoneNum, int index)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(index).getDeliveryStaffEntry().getMobile(), phoneNum, "PhoneNumber Is wrong");
    }

    public void validateMobileNum(String phoneNum)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile(), phoneNum, "PhoneNumber Is wrong");
    }

    public void validateIsComplete(boolean value)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getIsComplete().booleanValue(), value, "Trip was expected to be incomplete");
    }

    public void validateShipmentStatus(String shipmentStatus)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getShipmentType().toString(), shipmentStatus, "Trip was expected to be incomplete");
    }

    public void validateShipmentStatus(String shipmentStatus, int index)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(index).getShipmentType().toString(), shipmentStatus, "Trip was expected to be incomplete");
    }

    public void validateTotalCount(long count)
    {
        Assert.assertEquals(lmsTripResponse.getStatus().getTotalCount(), count, "total count expected to be 1");
    }

    public void validateTenantId(String tenantId,int index)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(index).getTenantId(),tenantId, "Tenant Id is incorrect at"+ index);
    }

    public void validateStatusCode(int statusCode)
    {
        Assert.assertEquals(lmsTripResponse.getStatus().getStatusCode(), statusCode, "Status Code differs");
    }

    public void validateStatusMessage(String statusMessage)
    {
        Assert.assertEquals(lmsTripResponse.getStatus().getStatusMessage(), statusMessage, "Unexpected status message");
    }

    public void validateFirstName(String name, int index)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(index).getDeliveryStaffEntry().getFirstName(), name, "First Name Invalid");
    }

    public void validateFirstName(String firstName)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName(), firstName, "First Name Invalid");
    }

    public void validateLastName(String lastName, int index)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(index).getDeliveryStaffEntry().getLastName(), lastName, "Last Name Invalid");
    }

    public void validateLastName(String lastName)
    {
        Assert.assertEquals(lmsTripResponse.getTrips().get(0).getDeliveryStaffEntry().getLastName(), lastName, "Last Name Invalid");
    }

    // NOT NULL
    public void isNotNullTripNumber() {
        Assert.assertNotNull(lmsTripResponse.getTrips().get(0).getTripNumber(), "TripNumber is empty");
    }

    public void isNotNullDeliveryStaffId() {
        Assert.assertNotNull(lmsTripResponse.getTrips().get(0).getDeliveryStaffId());
    }

    public void isNotNullCreatedOn() {
        Assert.assertNotNull(lmsTripResponse.getTrips().get(0).getCreatedOn());
    }

    public void isNotNullLastModifiedOn() {
        Assert.assertNotNull(lmsTripResponse.getTrips().get(0).getLastModifiedOn());
    }

    public void isNotNullDeliveryCenterId() {
        Assert.assertNotNull(lmsTripResponse.getTrips().get(0).getDeliveryCenterId());
    }

    public void isNotNullTripStatus() {
        Assert.assertNotNull(lmsTripResponse.getTrips().get(0).getTripStatus());
    }

    public void isNotNullDeliveryStaffType() {
        Assert.assertNotNull(lmsTripResponse.getTrips().get(0).getDeliveryStaffEntry().getDeliveryStaffType());
    }

    public void isNotNullFirstName() {
        Assert.assertNotNull(lmsTripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName());
    }

    public void isNotNullLastName() {
        Assert.assertNotNull(lmsTripResponse.getTrips().get(0).getDeliveryStaffEntry().getLastName());
    }

    public void isNotNullDeliveryStaffAvailable() {
        Assert.assertNotNull(lmsTripResponse.getTrips().get(0).getDeliveryStaffEntry().getAvailable());
    }


    //NOT NULL ENDS

    /*BASE MODULAR FUNCTIONS END*/

    public void validateStatus(int statusCode, String statusMessage)
    {
        validateStatusCode(statusCode);
        validateStatusMessage(statusMessage);
    }
    
    public void validateName(String firstName, String lastName, int index)
    {
        validateFirstName(firstName, index);
        validateLastName(lastName,index);
    }

    public void validateNameAndMobileNum(String firstName, String lastName, String mobileNum,  int index)
    {
        validateFirstName(firstName, index);
        validateLastName(lastName,index);
        validateMobileNum(mobileNum, index);
    }

    public void validateNameAndMobileNum(String firstName, String lastName, String mobileNum)
    {
        validateFirstName(firstName);
        validateLastName(lastName);
        validateMobileNum(mobileNum);
    }
    

    
    public void validateGetTripWithParamForCardEnabled(boolean isEnabled) {
        validateCardEnabled(isEnabled);
    }

    public void validateGetTripWithParamForTripStatus(String status) {
        validateTripStatus(status);
    }

    public void validateGetTripWithParamForCheckTripNumberNotNull() {
        isNotNullTripNumber();
    }

    public void validateGetTripByTripNumber(int dcId)
    {
        isNotNullTripNumber();
        isNotNullCreatedOn();
        isNotNullLastModifiedOn();
        validateDeliveryCenterId(dcId);
        isNotNullDeliveryStaffId();
        isNotNullTripStatus();
        isNotNullDeliveryStaffType();
        isNotNullFirstName();
        isNotNullLastName();
        isNotNullDeliveryStaffAvailable();
    }

    public void validateSearchTripPlanningByTripId(int dcId)
    {
        validateDeliveryCenterId(dcId);
    }

    public void validateCreateTrip(String staffId, String status)
    {
        validateDeliveryStaffId(staffId);
        validateTripStatus(status);
    }

    public void validateStartTripWithValidTripId(boolean isComplete, String tripStatus)
    {
        isNotNullDeliveryStaffId();
        validateIsComplete(isComplete);
        validateTripStatus(tripStatus);
    }

    public void validateGetOpenTripForStaff(long count, String status)
    {
        validateTripStatus(status);
        validateTotalCount(count);
    }
}
