package com.myntra.apiTests.erpservices.lastmile.client;

import com.myntra.apiTests.erpservices.returnComplete.client.BaseClient;
import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.client.TripOrderAssignmentServiceClient;
import com.myntra.lastmile.client.client.TripServiceV2Client;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.scm.utils.EnvironmentUtil;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class TripOrderAssignmentClient implements BaseClient {
    TripOrderAssignmentServiceClient tripOrderAssignmentServiceClient;
    String tenantId;
    String clientId;
    String serviceURL;


    public TripOrderAssignmentClient(String env, String clientId, String tenantId) {
        EnvironmentUtil environmentUtil = null;
        try {
            environmentUtil = new EnvironmentUtil();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        try {
            this.serviceURL = environmentUtil.formUrl(env, "lastmile") + "lastmile-service";

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.tenantId = tenantId;
        this.clientId = clientId;

        tripOrderAssignmentServiceClient = new TripOrderAssignmentServiceClient(this.serviceURL, 5, 10);
    }


    public TripOrderAssignmentResponse receiveTripOrder(final String operationalTrackingId, final String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            tripOrderAssignmentResponse = tripOrderAssignmentServiceClient.receiveTripOrder( operationalTrackingId,tenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 25000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }


    public TripOrderAssignmentResponse receiveTripShipments(String trackingNumber, String tenantid) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            tripOrderAssignmentResponse = tripOrderAssignmentServiceClient.receiveTripOrder(trackingNumber, tenantid, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
        }
        return tripOrderAssignmentResponse;
    }
}
