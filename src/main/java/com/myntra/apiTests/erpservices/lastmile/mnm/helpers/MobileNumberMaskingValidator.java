package com.myntra.apiTests.erpservices.lastmile.mnm.helpers;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.service.LastmileServiceHelper;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClientV2_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.TripOrderAssignmentResponseValidator;
import com.myntra.apiTests.erpservices.lastmile.validator.TripResponseValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.DB.LastMileDAO;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.validators.CreateTripValidator;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.code.utils.VirtualNumberAllocationStatus;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.VirtualNumberEntry;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.VirtualNumberResponse;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShipmentType;
import org.joda.time.DateTime;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;


/**
 * @author Bharath.MC
 * @since Feb-2019
 */
public class MobileNumberMaskingValidator {

    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    final String maskedMobileNumberPattern = "[X]+\\d{4}\\b";
    LastMileDAO lastMileDAO = new LastMileDAO();
    LastmileServiceHelper lastmileServiceHelper = new LastmileServiceHelper();
    static String env = getEnvironment();
    TripClient_QA tripClient_qa = new TripClient_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_QA = new MLShipmentClientV2_QA();

    public MobileNumberMaskingValidator(){}

    public void validateTripStatus(String tripId, String expectedTripStatus) throws IOException, JAXBException {
        CreateTripValidator createTripValidator = null;
        TripResponse tripOrder = tripClient_qa.searchByTripId(Long.valueOf(tripId));
        createTripValidator = new CreateTripValidator(tripOrder);
        Assert.assertEquals(tripOrder.getTrips().get(0).getTripStatus().toString(), expectedTripStatus , "Trip is in wrong Status");
        createTripValidator.validateCreateTripStatus(1003, "Trip(s) retrieved successfully", EnumSCM.SUCCESS);
    }

    public void validateTripStatus(TripResponse tripResponse, String expectedStatus) {
        TripResponseValidator lmsTripValidator = new TripResponseValidator(tripResponse);
        lmsTripValidator.validateTripStatus(expectedStatus);
    }

    public void validateTripStatus(TripOrderAssignmentResponse tripOrderAssignmentResponse, String expectedStatus) {
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateCorrespondingTripStatus(expectedStatus);
    }

    // validate before getting the CTI response trip status should be Processing Status
    public void validateTripStatusProcessing(TripResponse tripResponse) throws IOException, JAXBException {
        CreateTripValidator createTripValidator = null;
        long tripId = tripResponse.getTrips().get(0).getId();
        TripResponse tripOrder = tripClient_qa.searchByTripId(Long.valueOf(tripId));
        createTripValidator = new CreateTripValidator(tripOrder);
        Assert.assertEquals(tripOrder.getTrips().get(0).getTripStatus().toString(), TripStatus.PROCESSING.toString(), "Trip is in wrong Status");
        createTripValidator.validateCreateTripStatus(1003, "Trip(s) retrieved successfully", EnumSCM.SUCCESS);
    }


    // validate before getting the CTI response VN table status should be Awaitting status
    // validate After getting the CTI response VN table status should be status Received
    //Validate After Trip Completion, the VN status should be DEALLOCAED
    public void validateVirtualNumberAllocationStatus(String tripId , VirtualNumberAllocationStatus expectedStatus){

    }

    public void validateVirtualNumberAllocationStatus(TripResponse tripResponse , VirtualNumberAllocationStatus expectedStatus){
        long tripId = tripResponse.getTrips().get(0).getId();
        validateVirtualNumberAllocationStatus(String.valueOf(tripId) , expectedStatus);
    }

    // Validate mobileNumber Masking Scenario:- validate Generated the Virtual Number, PIn A and Pin B
    public void validateVirtualNumberEntriesDB(String trackingNumber, Map<String, String> postedVNentries){
        Map<String, Object> virtualNumberEntriesDB = LastMileDAO.GetVirtualNumberEntriesByTrackingNumberDB(trackingNumber);
        Assert.assertEquals(postedVNentries.get("clientReferenceId"), virtualNumberEntriesDB.get("virtual_number_request_id"), "Invalid client refrence ID");
        //Assert.assertEquals(postedVNentries.get("connectionId"), virtualNumberEntriesDB.get("call_bridge_connection_id"), "Invalid connection ID in DB");
        Assert.assertEquals(postedVNentries.get("virtualNumber"), virtualNumberEntriesDB.get("virtual_number"), "Invalid virtual Number in DB");
        Assert.assertEquals(postedVNentries.get("partyOnePins"), virtualNumberEntriesDB.get("party_A_pin"), "Invalid partyA pin");
        Assert.assertEquals(postedVNentries.get("partyTwoPins"), virtualNumberEntriesDB.get("party_B_pin"), "Invalid PartyB pin");

        Assert.assertNotNull(virtualNumberEntriesDB.get("call_bridge_connection_id"), "call_bridge_connection_id is Null");
        Assert.assertNotNull(virtualNumberEntriesDB.get("virtual_number") , "virtual_number is Null");
        Assert.assertNotNull(virtualNumberEntriesDB.get("party_A_pin"), "party_A_pin is Null");
        Assert.assertNotNull(virtualNumberEntriesDB.get("party_B_pin"), "party_B_pin is Null");
    }

    // validate After start trip status should be OUT_FOR_DELIVERY status
    public void validateStartTripStatus(String tripID){}

    //Negative Scenario: process seccond order,create trip(validate trip status), Assign the trip(validate it should be awaiing status),Start the trip,complete the trip(It should be NOT_RECEIVED status in VN table)
    public void validate6(String tripID){}

    // process two Orders with same mobile Number till SH, Validate two row with single VN will be created
    public void validate7(String tripID){}

    // Process two Orders with two Mobile Numbers till SH, Validate two row with diffarent VN should be displayed
    public void validate8(String tripID){}

    public void validateMaskedNumber(String mobileNumber){
        System.out.println("Mobile Number = "+mobileNumber);
        Assert.assertEquals(Pattern.compile(maskedMobileNumberPattern).matcher(mobileNumber).find(), true, "Mobile Number has not been masked!");
    }

    public void validateMaskedNumber(OrderEntry orderEnty){
        validateMaskedNumber(orderEnty.getMobile());
    }


    public void validateVNAllocationStatus(String trackingNumber, VirtualNumberAllocationStatus vnAllocationStatus) {
        waitForDuration(1000);
        Assert.assertEquals(LastMileDAO.GetTripAndVNStatusByTrackingNumberDB(trackingNumber).get("virtual_number_allocation_status"), vnAllocationStatus.toString() , "Invalid Virtual Number Allocation Status!");
    }

    public void validateVNAllocationStatus(List<String> trackingNumbers, VirtualNumberAllocationStatus vnAllocationStatus) {
        for(String trackingNumber : trackingNumbers){
            validateVNAllocationStatus(trackingNumber, vnAllocationStatus);
        }
    }

    /**
     * Validate trip and VN allocation status when VN is not generated
     */
    public void validateVNandTripStatusInitialStart(String trackingNumber, TripResponse tripResponse) throws IOException, JAXBException {
        validateTripStatus(tripResponse , TripStatus.PROCESSING.toString());
        validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.AWAITING);
    }

    public void validateVNandTripStatusInitialStart(List<String> trackingNumbers, List<TripOrderAssignmentResponse> tripOrderAssignmentResponses) throws IOException, JAXBException {
        for(TripOrderAssignmentResponse tripOrderAssignmentResponse : tripOrderAssignmentResponses) {
            validateTripStatus(tripOrderAssignmentResponse, TripStatus.PROCESSING.toString());
        }
        for(String trackingNumber : trackingNumbers) {
            validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.AWAITING);
        }
    }

    public void validateVNandTripStatusReInitiateStart(String trackingNumber, TripResponse tripResponse) throws IOException, JAXBException {
        validateTripStatus(tripResponse , TripStatus.PROCESSING.toString());
        validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.AWAITING);
    }

    public void validateVNandTripStatusWhenVNGenerated(String trackingNumber, TripResponse tripResponse) throws IOException, JAXBException {
        validateTripStatus(tripResponse , EnumSCM.OUT_FOR_DELIVERY);
        validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
    }


    public void validateVNandTripStatusWhenVNGenerated(List<String> trackingNumbers, List<TripOrderAssignmentResponse> tripOrderAssignmentResponses) throws IOException, JAXBException {
        for(String trackingNumber : trackingNumbers)
            validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
        for(TripOrderAssignmentResponse tripOrderAssignmentResponse : tripOrderAssignmentResponses)
            validateTripStatus(tripOrderAssignmentResponse , EnumSCM.OUT_FOR_DELIVERY);

    }

    public void validateVNStatusWhenVNGenerated(String trackingNumber) throws IOException, JAXBException {
        validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
    }

    public void validateVNandTripStatusAfterDelayWhenNoVN(String trackingNumber, String tripId) throws IOException, JAXBException {
        validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.NOT_RECEIVED);
        validateTripStatus(tripId , EnumSCM.OUT_FOR_DELIVERY);
    }

    public void validateVNandTripStatusAfterDelayWhenNoVN(List<String> trackingNumbers, String tripId) throws IOException, JAXBException {
        for(String trackingNumber : trackingNumbers){
            validateVNandTripStatusAfterDelayWhenNoVN(trackingNumber, tripId);
        }
    }

    public void validateMaskedNumber(OrderResponse orderResponse){
        for(OrderEntry orderEnty : orderResponse.getOrders()){
            validateMaskedNumber(orderEnty.getMobile());
            if(orderEnty.getCustMobile()!=null) validateMaskedNumber(orderEnty.getCustMobile());
            if(orderEnty.getDeliveryStaffMobile()!=null) validateMaskedNumber(orderEnty.getDeliveryStaffMobile());
        }
    }

    /**
     * After Validating the number has been masked, verify that last 4 digits are from actual mobile Number
     */
    public void validateMaskedNumberWithActualNumber(OrderEntry orderEnty){

    }

    /**
     * Sort by mobile number and compare it with DB
     * @param shipmentType
     * @param tripOrderResponse
     * @param sortOrder
     */
    public void validateMobileNumberSorting(ShipmentType shipmentType, OrderResponse tripOrderResponse, String sortOrder) throws Exception {
        List<Map<String, String>> mlShipmentRecords = null;
        int index =0;
        String deliveryCenterID = String.valueOf(tripOrderResponse.getOrders().get(0).getDeliveryCenterId());

        switch(shipmentType){
            case DL:
                mlShipmentRecords =  LastMileDAO.getDeliveriesOrdersMLShipmentDB(deliveryCenterID, sortOrder);
                break;
            case TRY_AND_BUY:
                mlShipmentRecords =  LastMileDAO.getTryAndBuyOrdersMLShipmentDB(deliveryCenterID, sortOrder);
                break;
            case PU:
                mlShipmentRecords =  LastMileDAO.getPickupOrdersMLShipmentDB(deliveryCenterID, sortOrder);
                break;
            case PO: //Since PU is used for pickup & Scheduled Pick-ups, but API changes in both case. so using PO as Scheduled Pick-ups.
                mlShipmentRecords =  LastMileDAO.getDeliveriesOrdersMLShipmentDB(deliveryCenterID, sortOrder);
                break;
            case EXCHANGE:
                mlShipmentRecords =  LastMileDAO.getExchangeOrdersMLShipmentDB(deliveryCenterID, sortOrder);
                break;
            default:
                throw new Exception("Unsupported Shipment Type");
        }

        String responseMobile , dbMobile;
        for(OrderEntry orderEnty : tripOrderResponse.getOrders()){
            responseMobile = orderEnty.getMobile();
            dbMobile = mlShipmentRecords.get(index).get("recipient_contact_number");
            System.out.println(orderEnty.getTrackingNumber() +" : "+  mlShipmentRecords.get(index).get("tracking_number"));
            System.out.println(responseMobile.substring(responseMobile.length()-4) +" : "+ dbMobile.substring(dbMobile.length()-4));
            System.out.println(orderEnty.getOrderId() +" : "+ mlShipmentRecords.get(index).get("order_id"));
            Assert.assertEquals(orderEnty.getTrackingNumber(), mlShipmentRecords.get(index).get("tracking_number"), "Sort by mobile number failed - invalid tracking number returned in API");
            Assert.assertEquals(responseMobile.substring(responseMobile.length()-4) , dbMobile.substring(dbMobile.length()-4), "Sort by mobile number failed - invalid mobile number returned in API");
            Assert.assertEquals(orderEnty.getOrderId(), mlShipmentRecords.get(index).get("order_id") ,"Sort by mobile number failed - invalid mobile number returned in API");
            ++index;
        }
    }

    public void waitForDuration(long milliseconds){
        try {
            System.out.println("Sleeping for "+milliseconds+" milliseconds "+ DateTime.now());
            Thread.sleep(milliseconds);
            System.out.println("waking after sleeping "+milliseconds+" milliseconds "+ DateTime.now());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * Validating virtual Number before and checking the newly created record in DB is null.
     * To avoid wrong customer virtual number in newly created VN record.
     * In case if VN is not generated, this API should return SDA Number in place of VN.
     */
    public void validateGetVirtualNumberByTrackingNumberBefore(VirtualNumberResponse virtualNumberResponse , String trackingNumber) throws Exception {
        List<VirtualNumberEntry> virtualNumberEntries = virtualNumberResponse.getVirtualNumberEntries();
        Map<String, String> dbVirtualNumberEntries = LastMileDAO.getVirtualNumberEntriesByTrackingNumberDB(trackingNumber);
        String deliveryStaffID = LastMileDAO.getDeliveryStaffIdByTrackingNumberDB(trackingNumber);
        String deliveryStaffMobileNumber = MobileNumberMaskingHelper.getDeliveryStaffMobileNumber(deliveryStaffID);
        for(VirtualNumberEntry virtualNumberEntry : virtualNumberEntries){
            System.out.println(virtualNumberEntry);
            Assert.assertEquals(virtualNumberEntry.getVirtualNumber(), deliveryStaffMobileNumber, "Invalid virtual Number returned by API! :Comparision(DB:API)");
            Assert.assertEquals(virtualNumberEntry.getPartyAPin() , null, "Invalid party_A_Pin returned by API! :Comparision(DB:API)");
            Assert.assertEquals(virtualNumberEntry.getPartyBPin() , null, "Invalid party_B_Pin returned by API! :Comparision(DB:API)");
        }
    }

    /**
     * Validating virtual Number before and checking the newly created record in DB is null.
     * To avoid wrong customer virtual number in newly created VN record.
     */
    public void validateGetVirtualNumberByTrackingNumberAfter(VirtualNumberResponse virtualNumberResponse , String trackingNumber) {
        List<VirtualNumberEntry> virtualNumberEntries = virtualNumberResponse.getVirtualNumberEntries();
        Map<String, String> dbVirtualNumberEntries = LastMileDAO.getVirtualNumberEntriesByTrackingNumberDB(trackingNumber);
        for(VirtualNumberEntry virtualNumberEntry : virtualNumberEntries){
            System.out.println(virtualNumberEntry);
            Assert.assertEquals(virtualNumberEntry.getVirtualNumber(), dbVirtualNumberEntries.get("virtual_Number"), "Invalid virtual Number returned by API! :Comparision(DB:API)");
            Assert.assertEquals(virtualNumberEntry.getPartyAPin() , dbVirtualNumberEntries.get("party_A_Pin"), "Invalid party_A_Pin returned by API! :Comparision(DB:API)");
            Assert.assertEquals(virtualNumberEntry.getPartyBPin() , dbVirtualNumberEntries.get("party_B_Pin"), "Invalid party_B_Pin returned by API! :Comparision(DB:API)");
            Assert.assertEquals(virtualNumberEntry.getTenantId() , dbVirtualNumberEntries.get("tenant_Id"), "Invalid tenant_Id returned by API! :Comparision(DB:API)");
        }
    }

    /**
     * Validates Get Virtual Number API after shipment assigned to trip.
     * Ideally at this stage, only VN entry will be created for Shipment in VN table and VN allocation status will be AWAITING.
     */
    public void validateGetVNafterTripAssignment(String trackingNumber) throws Exception {
        validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.AWAITING);
        VirtualNumberResponse virtualNumberResponse = lastmileServiceHelper.getVirualNumberByTrackingNumber(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        validateVirtualNumberResponseBasic(virtualNumberResponse);
        validateGetVirtualNumberByTrackingNumberBefore(virtualNumberResponse, trackingNumber);
    }

    /**
     * Validates list of tracking numbers using GET VN API.
     */
    public void validateGetVNafterTripAssignment(List<String> trackingNumbers) throws Exception {
        for(String trackingNumber : trackingNumbers){
            validateGetVNafterTripAssignment(trackingNumber);
        }
    }

    /**
     * Validate Generated VN's are not null and exactly same as generated earlier.
     */
    public void validateGeneratedVNbyGetVNAPI(String trackingNumber) throws Exception {
        VirtualNumberResponse virtualNumberResponse;
        virtualNumberResponse = lastmileServiceHelper.getVirualNumberByTrackingNumber(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        System.out.println(virtualNumberResponse);
        validateVirtualNumberResponseBasic(virtualNumberResponse);
        validateGetVirtualNumberByTrackingNumberAfter(virtualNumberResponse, trackingNumber);
    }

    public void validateGeneratedVNbyGetVNAPI(List<String> trackingNumbers) throws Exception {
        for(String trackingNumber : trackingNumbers){
            validateGeneratedVNbyGetVNAPI(trackingNumber);
        }
    }

    public void validateExchangesByTrip(String tripNumber, String tripId, List<String> trackingNumbers, List<String> packetIds) throws WebClientException {
        int trackingNumberIndex =0 , packetIdIndex =0;
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripNumber, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateTripOrderStatus(EnumSCM.OFD);
        tripOrderAssignmentResponseValidator.validateTripId(Long.valueOf(tripId));
        for(String trackingNumber : trackingNumbers) {
            Assert.assertTrue(trackingNumbers.contains(tripOrderAssignmentResponse.getTripOrders().get(trackingNumberIndex).getTrackingNumber()) , "Tracking number not found in exchange order response");
            trackingNumberIndex++;
        }
        for(String packetId : packetIds) { //packetID = OrderId
            Assert.assertTrue(packetIds.contains(tripOrderAssignmentResponse.getTripOrders().get(packetIdIndex).getExchangeOrderId()), "Exchange OrderId not found in exchange order response");
            packetIdIndex++;
        }
        Assert.assertEquals(tripOrderAssignmentResponse.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant Id in response");
    }


    public void validateVirtualNumberResponseBasic(VirtualNumberResponse virtualNumberResponse) {
        String statusType = virtualNumberResponse.getStatus().getStatusType().toString();
        String statusMessage = virtualNumberResponse.getStatus().getStatusMessage().toString();
        System.out.println("statusType=" + statusType);
        Assert.assertEquals(statusType, "SUCCESS", statusType + ":" + statusMessage);
    }

    public void validateOrderStatusInLMSAndML(List<String> packetIds) {
        for(String packetId : packetIds) {
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS(packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        }
    }

    /**
     * DCManager API validation
     */
    public void validateVNGeneratedByDCManagerWithNoVN(VirtualNumberResponse virtualNumberResponse, String trackingNumber) {
        MLShipmentResponse mLShipmentResponse = mlShipmentClientV2_QA.getMLShipmentDetails(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        String recipientContactNumber = mLShipmentResponse.getMlShipmentEntries().get(0).getRecipientContactNumber(); //customer primary number
        Assert.assertEquals(virtualNumberResponse.getVirtualNumberEntries().get(0).getVirtualNumber() , recipientContactNumber , "Invalid mobile number returned by DC Manger API");
        Assert.assertEquals(virtualNumberResponse.getVirtualNumberEntries().get(0).getPartyAPin() , null, "Invalid party_A_Pin returned by API! :Comparision(DB:API)");
        Assert.assertEquals(virtualNumberResponse.getVirtualNumberEntries().get(0).getPartyBPin() , null, "Invalid party_B_Pin returned by API! :Comparision(DB:API)");
        Assert.assertEquals(virtualNumberResponse.getVirtualNumberEntries().get(0).getTrackingNumber() , trackingNumber, "Invalid trackingNumber returned by API! :Comparision(DB:API)");
    }

    public void validateVNGeneratedByDCManagerWithVN(VirtualNumberResponse virtualNumberResponse, String trackingNumber) {
        List<VirtualNumberEntry> virtualNumberEntries = virtualNumberResponse.getVirtualNumberEntries();
        Map<String, String> dbVirtualNumberEntries = LastMileDAO.getVirtualNumberEntriesByTrackingNumberDB(trackingNumber);
        for(VirtualNumberEntry virtualNumberEntry : virtualNumberEntries){
            System.out.println(virtualNumberEntry);
            //Need to verify with CTI response - after integration
            //Assert.assertEquals(virtualNumberEntry.getVirtualNumber(), dbVirtualNumberEntries.get("virtual_Number"), "Invalid virtual Number returned by API! :Comparision(DB:API)");
            //Assert.assertEquals(virtualNumberEntry.getPartyAPin() , dbVirtualNumberEntries.get("party_A_Pin"), "Invalid party_A_Pin returned by API! :Comparision(DB:API)");
            //Assert.assertEquals(virtualNumberEntry.getPartyBPin() , dbVirtualNumberEntries.get("party_B_Pin"), "Invalid party_B_Pin returned by API! :Comparision(DB:API)");
            Assert.assertEquals(virtualNumberEntry.getTenantId() , dbVirtualNumberEntries.get("tenant_Id"), "Invalid tenant_Id returned by API! :Comparision(DB:API)");
        }
    }
}