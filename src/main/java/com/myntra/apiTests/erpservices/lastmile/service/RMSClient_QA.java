package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.returns.response.ReturnResponse;
import org.testng.Assert;

import java.io.IOException;

public class RMSClient_QA
{
    RMSServiceHelper rmsServiceHelper=new RMSServiceHelper();


    public ReturnResponse getRMSReturnDetails(String returnId) {
        ReturnResponse rmsReturnResponse = null;
        try {
            rmsReturnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(Long.parseLong(returnId)));
        } catch (IOException e) {
            Assert.fail("Unable to fetch return details from RMS ");
        }
        return rmsReturnResponse;
    }
}
