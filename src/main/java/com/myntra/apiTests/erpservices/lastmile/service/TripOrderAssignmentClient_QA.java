package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.springframework.http.MediaType;

import java.text.MessageFormat;

public class TripOrderAssignmentClient_QA
{
    /*
     * CreateDeliveryStaff
     * Http method:- POST
     * */

    public TripOrderAssignmentResponse receiveTripOrder(final String operationalTrackingId, final String tenantId) throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.RECEIVE_SHIPMENT_BEFORE_TRIP_COMPLETE;
            String pathParm= MessageFormat.format("?operationalTrackingId={0}&tenantId={1}",operationalTrackingId,tenantId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());

            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse receiveTripShipments(String trackingNumber, String tenantid) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.RECEIVE_SHIPMENT_BEFORE_TRIP_COMPLETE;
            String pathParm= MessageFormat.format("?operationalTrackingId={0}&tenantId={1}",trackingNumber,tenantid);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());

            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());


        } catch (Exception e) {
        }
        return tripOrderAssignmentResponse;
    }
}
