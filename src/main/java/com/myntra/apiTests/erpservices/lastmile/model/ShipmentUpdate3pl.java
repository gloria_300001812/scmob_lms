package com.myntra.apiTests.erpservices.lastmile.model;

import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.scm.enums.ShipmentType;
import lombok.Data;
import org.joda.time.DateTime;

@Data
public class ShipmentUpdate3pl {
    private String eventLocation;

    private DateTime eventTime;

    private ShipmentUpdateActivityTypeSource shipmentUpdateMode;

    private String event;

    private String trackingNumber;

    private String remarks;

    private ShipmentType shipmentType;


}