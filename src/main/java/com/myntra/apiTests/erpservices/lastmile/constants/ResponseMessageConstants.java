package com.myntra.apiTests.erpservices.lastmile.constants;

public class ResponseMessageConstants {
    public static final String storeCreated="Store created";
    public static final String createShipment="Shipment has been created successfully";
    public static final String retrievedShipment="Shipment has been retrieved successfully";
    public static final String retrievedOrder="ORDER(s) retrieved successfully";
    public static final String tripAdded="Trip added successfully";
    public static final String shipmentAdded="Shipments Successfully added to trip";
    public static final String errorStatus="error";
    public static final String tripUpdate="Trip updated successfully";
    public static final String success="Success";
    public static final String success1="SUCCESS";
    public static final String updateOrder="ORDER updated successfully";
    public static final String completeTrip="Trip completed in just ";
    public static final String closeShipment="Shipment Closed";
    public static final String addContainer="Container Added Successfully";
    public static final String receiveMasterBag="Masterbag Received Successfully";
    public static final String retrievedDeliveryStaff="Delivery Staff(s) retrieved successfully";
    public static final String qcUpdate="Shipment Updation Successful";
    public static final String shipmentReconcile="has shipments that are not reconciled";
    public static final String assignOrderToStoreBag="Orders have been assigned with shipment successfully";
    public static final String returnReleseRetrieved="Return Release retrieved successfully";
    public static final String reshipToCustomer="1/1 updated successfully";
    public static final String pickupShipmentRetrieved="Pickup Shipment retrieved successfully.";
    public static final String updateReturnRelease="Return Release updated successfully";
    public static final String createreturnAfterGreenchannelApproved="Refund Already processed for Return";
    public static final String returnRejectedGreenChannelApprove="TRANSITION_NOT_CONFIGURED from REJECTED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER -> APPROVE_GREEN_CHANNEL";
    public static final String masterBagUpdate="Shipment has been updated successfully";
    public static final String startTrip="Trip Started Successfully";
}
