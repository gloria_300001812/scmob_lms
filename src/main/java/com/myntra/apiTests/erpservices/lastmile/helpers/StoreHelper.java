package com.myntra.apiTests.erpservices.lastmile.helpers;

import com.aerospike.client.lua.LuaAerospikeLib;
import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.common.Constants.LambdaInterfaces;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.client.TripClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.ShippingLabel.lms.LabelParsers;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.model.EventLocationPremise;
import com.myntra.apiTests.erpservices.lastmile.model.ReturnLineUpdateRequestEntryList;
import com.myntra.apiTests.erpservices.lastmile.model.ReturnUpdateRequestEntry;
import com.myntra.apiTests.erpservices.lastmile.model.SelfShipReturnQCUpdate;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClientV2_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.DB.IQueries;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Helper.HubToTransportHubConfigResponse;
import com.myntra.apiTests.erpservices.lms.Helper.LMSUtils;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.MLShipmentUpdateEntry;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.BaseClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.cms.pim.dto.StatusType;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.response.EmptyResponse;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.client.TripServiceV2Client;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.TripAction;
import com.myntra.lastmile.client.code.utils.UpdatedVia;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.TripOrderAssignementEntry;
import com.myntra.lastmile.client.entry.TripShipmentAssociationEntry;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.domain.response.ReturnResponse;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.*;
import com.myntra.lms.client.status.ShipmentStatus;
import com.myntra.logistics.masterbag.core.MasterbagShipment;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.*;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;

import com.myntra.returns.common.enums.CancellationType;
import com.myntra.returns.common.enums.code.ReturnActionCode;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.entry.*;
import com.myntra.scm.utils.EnvironmentUtil;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import com.myntra.tms.container.ContainerEntry;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagEntry;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import lombok.SneakyThrows;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.codehaus.jettison.json.JSONException;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.springframework.http.MediaType;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.myntra.apiTests.common.Constants.Headers.getRMSHeader;
import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS.CLIENTID;
import static com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS.TENANTID;

public class StoreHelper implements BaseClient {
    String FGPathparam=null;
    String FGTRIP_UPDATE= Constants.LMS_PATH.TRIP_UPDATEVersion2;
    private LmsServiceHelper lmsServiceHelper=new LmsServiceHelper();
    private LMS_ReturnHelper lms_returnHelper=new LMS_ReturnHelper();
    TripClient_QA tripClient_qa=new TripClient_QA();
    LabelParsers labelParsers=new LabelParsers();
    TripServiceV2Client tripServiceV2Client;
    String tenantId;
    String clientId;
    String serviceURL;

    public StoreHelper(String env, String clientId, String tenantId) {
        EnvironmentUtil environmentUtil = null;
        try {
            environmentUtil = new EnvironmentUtil();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        try {
            this.serviceURL = environmentUtil.formUrl(env, "lastmile") + "lastmile-service";

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.tenantId = tenantId;
        this.clientId = clientId;

        tripServiceV2Client = new TripServiceV2Client(this.serviceURL, 5, 10);
    }

    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }


   // @Step("Update try and buy order using different item status")
    public TripOrderAssignmentResponse updateTryAndBuyOrders(Long tripOrderAssignmentId, AttemptReasonCode attemptReasonCode, TripAction tripAction, String packetId, Long dcId, Double amount,
                                                             Long itemEntriesId,String storeTenantId,TryAndBuyItemStatus tryAndBuyItemStatus,Long storeTripId) throws JAXBException, UnsupportedEncodingException {

        TripOrderAssignmentResponse tripOrderAssignmentResponse = new TripOrderAssignmentResponse();
        List<TripOrderAssignementEntry> tripOrderAssignementEntries = new ArrayList<>();
        for(String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripId(storeTripId)) {
            TripOrderAssignementEntry tripOrderAssignementEntry = new TripOrderAssignementEntry();
            tripOrderAssignementEntry.setId(tripOrderAssignmentId);// assignOrderToTripBulk
            tripOrderAssignementEntry.setRemark("test");
            tripOrderAssignementEntry.setAttemptReasonCode(attemptReasonCode);
            tripOrderAssignementEntry.setTripAction(tripAction);
            tripOrderAssignementEntry.setUpdatedVia(UpdatedVia.WEB);
            tripOrderAssignementEntry.setPaymentType("CASH");
            tripOrderAssignementEntry.setTripId(storeTripId);
            tripOrderAssignementEntry.setIsOutScanned(true);
            tripOrderAssignementEntry.setTrackingNumber(trackingNumber);
            OrderEntry orderEntry = new OrderEntry();
            ItemEntry itemEntry = new ItemEntry();
            orderEntry.setOrderId(packetId);
            orderEntry.setDeliveryCenterId(dcId);
            orderEntry.setCodAmount(amount);
            itemEntry.setId(itemEntriesId);
            itemEntry.setStatus(tryAndBuyItemStatus);
            itemEntry.setRemarks("Bought");
            if (tryAndBuyItemStatus.equals(TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT)) {
                itemEntry.setRemarks("treid and NB");
                itemEntry.setQcStatus(ItemQCStatus.PASSED);
                itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
            }
            List<ItemEntry> entries = new ArrayList<>();
            entries.add(itemEntry);
            orderEntry.setItemEntries(entries);
            if (orderEntry != null) {
                tripOrderAssignementEntry.setOrderEntry(orderEntry);
            }
            tripOrderAssignementEntries.add(tripOrderAssignementEntry);
        }
        tripOrderAssignmentResponse.setTripOrderAssignmentEntries(tripOrderAssignementEntries);
        String payload = APIUtilities.convertXMLObjectToString(tripOrderAssignmentResponse);
        FGPathparam= "?tenantId="+storeTenantId;
        Svc service = HttpExecutorService.executeHttpService(FGTRIP_UPDATE, new String[]{FGPathparam} , SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = (TripOrderAssignmentResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        return tripOrderAssignmentResponse1;

    }

    public DeliveryStaffResponse getDeliverySDABasedOnDC(Long DCId,String tenantId,int start,int fetchSize,String sortBy,String sortOrder) throws UnsupportedEncodingException, JAXBException {
        FGPathparam= "?q=deliveryCenter.id.eq:"+DCId+"___deleted.eq:false___tenantId.eq:"+tenantId+"&start="+start+"&fetchSize="+fetchSize+"&sortBy="+sortBy+"&sortOrder="+sortOrder;
        Svc service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.FILTER_SEARCH, new String[]{FGPathparam} , SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        DeliveryStaffResponse deliveryStaffResponse = (DeliveryStaffResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new DeliveryStaffResponse());
        return deliveryStaffResponse;
    }

   // @Step("Pickup the reverse master bag from store")
    public TripShipmentAssociationResponse pickupReverseBagFromStore(String masterBagId, List<String> trackingNumberList, String tripId, AttemptReasonCode attemptReasonCode,
                                                                      int totalFailedDeliveriesPicked, int totalSuccessfulReturnsPicked, float amountCollected,String tenantId,
                                                                     TripOrderStatus tripOrderStatus,ShipmentType shipmentType) throws UnsupportedEncodingException, JAXBException {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();
        for (String trackingNumber : trackingNumberList) {
            MasterbagShipment shipmentItem = new MasterbagShipment();
            shipmentItem.setShipmentType(shipmentType);
            shipmentItem.setTrackingNumber(trackingNumber);
            shipmentItemList.add(shipmentItem);
        }
        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.REVERSE_BAG);
        tripShipmentAssociationEntry.setStatus(tripOrderStatus);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);

        //Specific to reverseBag
        tripShipmentAssociationEntry.setTotalFailedDeliveriesPicked(totalFailedDeliveriesPicked);
        tripShipmentAssociationEntry.setTotalSuccessfulReturnsPicked(totalSuccessfulReturnsPicked);
        tripShipmentAssociationEntry.setAmountCollected(amountCollected);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        String payload = APIUtilities.convertXMLObjectToString(tripShipmentAssociationResponse);
        FGPathparam= "?tenantId="+tenantId;
        Svc service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.UPDATE_TRIP_SHIPMENTS, new String[]{FGPathparam} , SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = (TripShipmentAssociationResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());

        return tripShipmentAssociationResponse1;
    }

   // @Step("mark the order as QC pass, order which is received from store")
    public String  mlOpenBoxQCPass(String returnId, ShipmentUpdateEvent event, String trackingNo) throws IOException, JAXBException {
        ShipmentUpdate.ShipmentUpdateBuilder shipmentUpdateBuilder = new ShipmentUpdate.ShipmentUpdateBuilder(
                returnId, event);
        shipmentUpdateBuilder.courierCodeAndTrackingNumber("ML",trackingNo);
        shipmentUpdateBuilder.tenantId(LMS_CONSTANTS.TENANTID);
        shipmentUpdateBuilder.clientId(LMS_CONSTANTS.CLIENTID);
        shipmentUpdateBuilder.eventLocation("location");
        shipmentUpdateBuilder.eventTime(new DateTime());
        shipmentUpdateBuilder.shipmentType(ShipmentType.PU);
        shipmentUpdateBuilder.shipmentUpdateActivitySource(ShipmentUpdateActivityTypeSource.LogisticsPortal);
        shipmentUpdateBuilder.userName("User");
        shipmentUpdateBuilder.remarks("remarks");
        shipmentUpdateBuilder.eventLocationPremise(new Premise("5", Premise.PremiseType.DELIVERY_CENTER) );
        String payload = APIUtilities.getObjectToJSON(shipmentUpdateBuilder.build() );

        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_OPENBOX_QC_STATUS, null,
                SERVICE_TYPE.Last_mile.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());

        return service.getResponseBody();
    }

   // @Step("Close the master bag")
    public ShipmentResponse closeShipment(Long masterBagId) throws IOException, JAXBException {

        ShipmentEntry shipmentEntry = new ShipmentEntry();
        shipmentEntry.setId(masterBagId);
        ShipmentResponse shipmentResponsePayload = new ShipmentResponse(shipmentEntry);
        String payload = APIUtilities.convertXMLObjectToString(shipmentResponsePayload);

        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.CLOSE_MB, null, SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        ShipmentResponse shipmentResponse = (ShipmentResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new ShipmentResponse());
        return shipmentResponse;
    }

    //receive container in transport hub
    public LambdaInterfaces.BiFunction receiveContainerInTransportHub = (containerId, hubId)-> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_CONTAINER, new String[]{containerId.toString(),"dropOffHubCode",hubId.toString(),"tenant",LMS_CONSTANTS.TENANTID},
            SERVICE_TYPE.TMS_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new ContainerResponse());

   // @Step("master bag inscan in warehouse")
    public String masterBagInscanProcess(String trackingNumber, long masterBagId, ShipmentType shipmentType, OrderShipmentAssociationStatus orderShipmentAssociationStatus,
                                                   ShipmentStatus shipmentStatus, PremisesType premisesType, Long lastScannedPremisesId) throws NumberFormatException, JAXBException, UnsupportedEncodingException {
        // Order Scan After Shipment At DC

            OrderShipmentAssociationEntry orderShipmentAssociationEntries = new OrderShipmentAssociationEntry();
            orderShipmentAssociationEntries.setTrackingNumber(trackingNumber);
            orderShipmentAssociationEntries.setShipmentType(shipmentType);
            orderShipmentAssociationEntries.setStatus(orderShipmentAssociationStatus);

            List<OrderShipmentAssociationEntry> orderShipmentAssociation = new ArrayList<>();
            orderShipmentAssociation.add(orderShipmentAssociationEntries);

            ShipmentEntry shipmentEntryPayload = new ShipmentEntry();
            shipmentEntryPayload.setId(masterBagId);
            shipmentEntryPayload.setStatus(shipmentStatus);
            shipmentEntryPayload.setLastScannedCity("Bangalore returns hub-HUB");
            shipmentEntryPayload.setLastScannedPremisesId(lastScannedPremisesId);
            shipmentEntryPayload.setLastScannedPremisesType(premisesType);
            shipmentEntryPayload.setArrivedOn(new Date());
            shipmentEntryPayload.setLastScannedOn(new Date());
            shipmentEntryPayload.setOrderShipmentAssociationEntries(orderShipmentAssociation);

            String payload = APIUtilities.convertXMLObjectToString(shipmentEntryPayload);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.RECEIVE_SHIPMENT_IN_DC, new String[]{String.valueOf(masterBagId)} , SERVICE_TYPE.LMS_SVC.toString(),
                    HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        return service.getResponseBody();
    }

   // @Step(" update pickup order status")
    public TripOrderAssignmentResponse updatePickupInTrip(long tripOrderAssignmentId, String status, String tripAction,String tenantId){
        String param="?tenantId="+tenantId;
        AttemptReasonCode reasonCode = AttemptReasonCode.PICKED_UP_SUCCESSFULLY;
        if (status.equalsIgnoreCase(EnumSCM.NOT_ABLE_TO_PICKUP)) {
            reasonCode = AttemptReasonCode.CANNOT_PICKUP;
        } else if (status.equalsIgnoreCase(EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING)) {
            reasonCode = AttemptReasonCode.PICKUP_SUCCESSFUL_QC_PENDING;
        } else if (status.equalsIgnoreCase(EnumSCM.RETURNS_CANCELLATION)) {
            reasonCode = AttemptReasonCode.OTHERS;
        } else if (status.equalsIgnoreCase(EnumSCM.RETURN_REJECTED)) {
            reasonCode = AttemptReasonCode.RETURN_QC_FAIL;
        } else if (status.equalsIgnoreCase(EnumSCM.ON_HOLD_DAMAGED_PRODUCT)) {
            reasonCode = AttemptReasonCode.PICKUP_ON_HOLD_DAMAGED_PRODUCT;
        } else if (status.equalsIgnoreCase(EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE)) {
            reasonCode = AttemptReasonCode.REQUESTED_RE_SCHEDULE;
        } else if (status.equals(EnumSCM.LOST)) {
            reasonCode = AttemptReasonCode.OTHERS;
        } else if (status.equals(EnumSCM.FAILED)) {
            reasonCode = AttemptReasonCode.REQUESTED_RE_SCHEDULE;
        }else if (status.equals(EnumSCM.HAPPY_WITH_PRODUCT)) {
            reasonCode = AttemptReasonCode.HAPPY_WITH_PRODUCT;
        }
        TripAction ta = TripAction.TRIP_COMPLETE;
        if (tripAction.equalsIgnoreCase(EnumSCM.UPDATE)) {
            ta = TripAction.UPDATE;
        } else if (tripAction.equalsIgnoreCase("TRIPSTART")) {
            ta = TripAction.TRIP_START;
        } else if (tripAction.equalsIgnoreCase("MARKRETURNSCAN")) {
            ta = TripAction.MARK_RETURNSCAN;
        } else if (tripAction.equalsIgnoreCase("MARKOUTSCAN")) {
            ta = TripAction.MARK_OUTSCAN;
        }
        TripOrderAssignmentResponse tripOrderAssignmentResponse = new TripOrderAssignmentResponse();
        List<TripOrderAssignementEntry> tripOrderAssignementEntries = new ArrayList<>();
        for(String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripOrderAssignmentId(tripOrderAssignmentId)) {
            TripOrderAssignementEntry tripOrderAssignementEntry = new TripOrderAssignementEntry();
            tripOrderAssignementEntry.setId(tripOrderAssignmentId);
            tripOrderAssignementEntry.setRemark("test");
            tripOrderAssignementEntry.setAttemptReasonCode(reasonCode);
            tripOrderAssignementEntry.setTripAction(ta);
            tripOrderAssignementEntry.setUpdatedVia(UpdatedVia.WEB);
            tripOrderAssignementEntry.setPaymentType("CASH");
            tripOrderAssignementEntry.setIsOutScanned(true);
            tripOrderAssignementEntry.setTrackingNumber(trackingNumber);
            tripOrderAssignementEntries.add(tripOrderAssignementEntry);
        }
        tripOrderAssignmentResponse.setTripOrderAssignmentEntries(tripOrderAssignementEntries);
        TripOrderAssignmentResponse shipmentResponse=null;
        try {
            String payload = APIUtilities.convertXMLObjectToString(tripOrderAssignmentResponse);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.TRIP_UPDATEVersion2, new String[]{param}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
            shipmentResponse = (TripOrderAssignmentResponse) APIUtilities
                    .convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        }catch (JAXBException | UnsupportedEncodingException e){
            Assert.fail("FAILED:: While Updating the trip with triporder assignment id "+tripOrderAssignmentId+" to status "+status+" "+e.getMessage());
        }
        return shipmentResponse;
    }

   // @Step("delivered store bag to store")
    public TripShipmentAssociationResponse deliverStoreBagToStore(String masterBagId, List<String> trackingNumberList, String tripId, AttemptReasonCode attemptReasonCode,
                                                                  TripOrderStatus tripOrderStatus,ShipmentType shipmentType,String tenantId) {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = new TripShipmentAssociationResponse();
        List<MasterbagShipment> shipmentItemList = new ArrayList<>();
        for (String trackingNumber : trackingNumberList) {
            MasterbagShipment shipmentItem = new MasterbagShipment();
            shipmentItem.setShipmentType(shipmentType);
            shipmentItem.setTrackingNumber(trackingNumber);
            shipmentItemList.add(shipmentItem);

        }
        List<TripShipmentAssociationEntry> tripShipmentAssociationEntryList = new ArrayList<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntry = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntry.setShipmentItems(shipmentItemList);
        tripShipmentAssociationEntry.setAttemptReasonCode(attemptReasonCode);
        tripShipmentAssociationEntry.setCustomerSignature("Gloria");
        tripShipmentAssociationEntry.setShipmentId(masterBagId);
        tripShipmentAssociationEntry.setShipmentType(ShipmentType.FORWARD_BAG);
        tripShipmentAssociationEntry.setStatus(tripOrderStatus);
        tripShipmentAssociationEntry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
        tripShipmentAssociationEntryList.add(tripShipmentAssociationEntry);
        tripShipmentAssociationResponse.setTripShipmentAssociationEntryList(tripShipmentAssociationEntryList);
        tripShipmentAssociationResponse.setTripId(tripId);
        TripShipmentAssociationResponse response = null;
        try {
            String param="?tenantId="+tenantId;
            String payload = APIUtilities.convertXMLObjectToString(tripShipmentAssociationResponse);
            Svc service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.UPDATE_TRIP_SHIPMENTS, new String[]{param}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
            response = (TripShipmentAssociationResponse) APIUtilities
                    .convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());
        }catch (JAXBException | UnsupportedEncodingException e){
            Assert.fail("FAILED:: While delivering the store bag"+e.getMessage());
        }
        return response;
    }

   // @Step("get return status in LMS")
    public ReturnResponse getReturnStatusInLMS(String returnId, String tenantId, Long sourceId) throws IOException, JAXBException, InterruptedException {
        String param=returnId+"?sourceId="+sourceId+"&tenantId="+tenantId;
        Thread.sleep(Lastmile_ReturnManagement_constants.wait_Time);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.RETURN_STATUS_LMS, new String[]{param}, SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        ReturnResponse returnResponse = (ReturnResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new ReturnResponse());
        return returnResponse;
    }

    // @Step("get return status in LMS")
    public ReturnResponse getMLShipmentDetails(String trackingNUmber, String tenantId) throws IOException, JAXBException, InterruptedException {
        String param="?tenantId="+tenantId;
        Thread.sleep(Lastmile_ReturnManagement_constants.wait_Time);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.RETURN_STATUS_LMS, new String[]{param}, SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        ReturnResponse returnResponse = (ReturnResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new ReturnResponse());
        return returnResponse;
    }

   // @Step("get return status in LMS")
    public String getReturnStatusInLMSForSchedulePickup(String returnId, String tenantId, Long sourceId) throws IOException, JAXBException {
        String param=returnId+"?sourceId="+sourceId+"&tenantId="+tenantId;

        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.RETURN_STATUS_LMS, new String[]{param}, SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        String returnResponse = service.getResponseBody();
        Assert.assertEquals(APIUtilities.getElement(returnResponse, "status.statusType", "json"), StatusType.SUCCESS.toString(), "Get Return status not retrieved");
        return APIUtilities.getElement(returnResponse, "data[0].shipmentStatus", "json");
    }

   // @Step("find the shipment status using tripId")
   public TripShipmentAssociationResponse findShipmentByTrip(Long tripId, String tenantId) throws UnsupportedEncodingException, JAXBException, XMLStreamException, JSONException {
       String FGPathparam = tripId + "?tenantId=" + tenantId;
       Svc service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.FIND_SHIPMENTS_BY_TRIP_ID, new String[]{FGPathparam},
               SERVICE_TYPE.Last_mile.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
       TripShipmentAssociationResponse response = (TripShipmentAssociationResponse) APIUtilities.jsonToObject(service.getResponseBody(),
               TripShipmentAssociationResponse.class);
       return response;

   }

   // @Step("update RTO order or Reqque FP orders")
    public String createRTO(String trackingNumber, Long dcId,MLShipmentUpdateEvent mlShipmentUpdateEvent,
                            ShipmentType shipmentType,ShipmentUpdateActivityTypeSource shipmentUpdateActivityTypeSource,String tenantId) throws JAXBException, UnsupportedEncodingException {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(dcId);
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("initiating RTO for shipment");
        mlShipmentUpdateEntryForRTO.setEvent(mlShipmentUpdateEvent);
        mlShipmentUpdateEntryForRTO.setShipmentType(shipmentType);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(shipmentUpdateActivityTypeSource);
        mlShipmentUpdateEntryForRTO.setTenantId(tenantId);

        String payload = APIUtilities.convertJavaObjectToJsonUsingGson(mlShipmentUpdateEntryForRTO);

        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.ML_SHIPMENT_UPDATEVersion2, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, payload, Headers.getCTSHeaderJSON());
        return service.getResponseBody();
    }

   // @Step("Delivered store bag orders to customer")
    public TripOrderAssignmentResponse deliveredStoreOrders(Long tripOrderId,AttemptReasonCode attemptReasonCode,TripAction tripAction,String paymentType,
                                     String tenantId) throws WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentPayload= new TripOrderAssignmentResponse();
        List<TripOrderAssignementEntry> tripOrderPayload = new ArrayList<>();
        for(String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripOrderAssignmentId(tripOrderId)) {
            TripOrderAssignementEntry tripOrderAssignementEntry = new TripOrderAssignementEntry();
            tripOrderAssignementEntry.setId(tripOrderId);
            tripOrderAssignementEntry.setRemark("delivered order to customed");
            tripOrderAssignementEntry.setAttemptReasonCode(attemptReasonCode);
            tripOrderAssignementEntry.setDeliveryTime(null);
            tripOrderAssignementEntry.setTripAction(tripAction);
            tripOrderAssignementEntry.setUpdatedVia(UpdatedVia.WEB);
            tripOrderAssignementEntry.setPaymentType(paymentType);
            tripOrderAssignementEntry.setShipmentType(null);
            tripOrderAssignementEntry.setIsOutScanned(false);
            tripOrderAssignementEntry.setTrackingNumber(trackingNumber);
            tripOrderPayload.add(tripOrderAssignementEntry);
        }
        tripOrderAssignmentPayload.setTripOrderAssignmentEntries(tripOrderPayload);

        TripOrderAssignmentResponse tripOrderAssignmentResponse=tripClient_qa.updateTripOrderAssignment(tripOrderAssignmentPayload,tenantId);
        return tripOrderAssignmentResponse;
    }

   // @Step("Make thr order status as Failed Delivered in store")
    public TripOrderAssignmentResponse failedDeliverStoreOrders(Long storeTripId, String storeMLId, Long tripOrderAssignmentId,
                                                                String storeTenantId,TripAction tripAction,AttemptReasonCode attemptReasonCode) {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        TripOrderAssignmentResponse request = new TripOrderAssignmentResponse();
        List<TripOrderAssignementEntry> entryList = new ArrayList<>();
        for(String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripId(storeTripId)) {
            TripOrderAssignementEntry entry = new TripOrderAssignementEntry();
            entry.setId(tripOrderAssignmentId);
            entry.setTripId(storeTripId);
            entry.setTripAction(tripAction);
            entry.setUpdatedVia(UpdatedVia.DELIVERY_APP_V2);
            entry.setOrderId(storeMLId);
            entry.setTenantId(storeTenantId);
            entry.setAttemptReasonCode(attemptReasonCode);
            entry.setCustomerSignature(storeTenantId + "_Gloria");
            entry.setOrderEntry(null);
            entry.setTrackingNumber(trackingNumber);
            entryList.add(entry);
        }
        request.setTripOrderAssignmentEntries(entryList);

        try {
            tripOrderAssignmentResponse = tripServiceV2Client.updateTripOrderAssignment(request, storeTenantId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 10000, 10000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
        }

        if (tripOrderAssignmentResponse.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.fail("Marking FD of shipment failed : " + tripOrderAssignmentResponse.getStatus().getStatusMessage());
        }
        return tripOrderAssignmentResponse;

    }

   // @Step("Check NDR status using query")
    public Map<String, Object> getNDRStatus(String attemptReasonCode, String serviceType, String clientId, String tenantId){
        if(attemptReasonCode.equalsIgnoreCase(AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS.toString())){
            attemptReasonCode="INCOMPLETE_ADDRESS";
        }else if(attemptReasonCode.equalsIgnoreCase(AttemptReasonCode.CASH_CARD_NOT_READY.toString())){
            attemptReasonCode="COD_NOT_READY";
        }
        String query="select  is_enabled, is_rto_blocked from ndr_config where attempt_reason_code=\""+attemptReasonCode+"\"and `service_type`=\""+serviceType+"\" and client_id = "+clientId+" and tenant_id="+tenantId+";";
        Map<String, Object> input= DBUtilities.exSelectQueryForSingleRecord(query,"myntra_lms");
        if(input.isEmpty()){
            Assert.fail("The entered query doesn't returned any value");
        }
        return input;
    }

   // @Step("Modify the store createion date as yesterday")
    public void modifiedMLLastmilePartnerShipmentDate(String courierCode){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String updateOrderTracking = "update ml_last_mile_partner_shipment_assignment set last_modified_on = '" +dateFormat.format(yesterday())+ "' where courier_code = '"+courierCode+"';" ;
        try{
            DBUtilities.exUpdateQuery(updateOrderTracking, "myntra_lms");
        }catch(Exception e) {
            Assert.fail("ML lastmile partner shipment table last modified date is not successfully changed");
        }

    }
    // @Step("Modify the store createion date as yesterday")
    public void modifiedMLLastmilePartnerShipmentDateAsCurrentDate(String courierCode){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String updateOrderTracking = "update ml_last_mile_partner_shipment_assignment set last_modified_on = '" +dateFormat.format(new Date())+ "' where courier_code = '"+courierCode+"';" ;
        try{
            DBUtilities.exUpdateQuery(updateOrderTracking, "myntra_lms");
        }catch(Exception e) {
            Assert.fail("ML lastmile partner shipment table last modified date is not successfully changed");
        }

    }


   // @Step("Update exchange order status")
    public TripOrderAssignmentResponse updateOrderInTrip(long tripOrderAssignmentId, String status, String tripAction, String exchangeOrderId, long tripId, OrderEntry orderEntry,String tenantId,Long clientId)
            throws  IOException, JAXBException {
        FGPathparam= "?tenantId="+ tenantId;
        AttemptReasonCode s = AttemptReasonCode.DELIVERED;
        if (status.equals(EnumSCM.LOST)) {
            s = AttemptReasonCode.OTHERS;
        } else if (status.equals(EnumSCM.FAILED)) {
            s = AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE;
        }
        TripAction ta = TripAction.TRIP_COMPLETE;
        if (tripAction.equalsIgnoreCase(EnumSCM.UPDATE)) {
            ta = TripAction.UPDATE;
        } else if (tripAction.equalsIgnoreCase("TRIPSTART")) {
            ta = TripAction.TRIP_START;
        } else if (tripAction.equalsIgnoreCase("MARKRETURNSCAN")) {
            ta = TripAction.MARK_RETURNSCAN;
        } else if (tripAction.equalsIgnoreCase("MARKOUTSCAN")) {
            ta = TripAction.MARK_OUTSCAN;
        }

        TripOrderAssignmentResponse tripOrderAssignmentResponse = new TripOrderAssignmentResponse();
        List<TripOrderAssignementEntry> tripOrderAssignementEntries = new ArrayList<>();
        for(String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripId(tripId)) {
            TripOrderAssignementEntry tripOrderAssignementEntry = new TripOrderAssignementEntry();
            tripOrderAssignementEntry.setId(tripOrderAssignmentId);// assignOrderToTripBulk
            tripOrderAssignementEntry.setRemark("test");
            tripOrderAssignementEntry.setAttemptReasonCode(s);
            tripOrderAssignementEntry.setTripAction(ta);
            tripOrderAssignementEntry.setUpdatedVia(UpdatedVia.WEB);
            tripOrderAssignementEntry.setPaymentType("CASH");
            tripOrderAssignementEntry.setTripId(tripId);
            tripOrderAssignementEntry.setIsOutScanned(true);
            tripOrderAssignementEntry.setTrackingNumber(trackingNumber);
            tripOrderAssignementEntry.setExchangeOrderId(exchangeOrderId);
            if (orderEntry != null) {
                tripOrderAssignementEntry.setOrderEntry(orderEntry);
            }
            tripOrderAssignementEntries.add(tripOrderAssignementEntry);
        }
        tripOrderAssignmentResponse.setTripOrderAssignmentEntries(tripOrderAssignementEntries);
        String payload = APIUtilities.convertXMLObjectToString(tripOrderAssignmentResponse);
        Svc service = HttpExecutorService.executeHttpService(FGTRIP_UPDATE, new String[]{FGPathparam} ,SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        TripOrderAssignmentResponse shipmentResponse = (TripOrderAssignmentResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        return shipmentResponse;
    }

   // @Step("get mapped transport hub details for Dc or WH")
    public HubToTransportHubConfigResponse getMappedTransportHubDetails(String tenantId, String dcOrWHHubCOde) throws UnsupportedEncodingException, JAXBException {

        FGPathparam=tenantId+"/"+dcOrWHHubCOde;

        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_LOCATION_HUB_CONFIG_FW, new String[]{FGPathparam} , SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new HubToTransportHubConfigResponse());
        return hubToTransportHubConfigResponse;
    }

   // @Step("add order tostore bag")
    public String addShipmentToStoreBag(Long masterbagId, ShipmentUpdateInfo shipmentUpdateInfo)
            throws IOException {

        String endPoint = "addShipmentByTrackingNumber?tenantId=" + LMS_CONSTANTS.TENANTID;
        String responseFormat = "status.statusMessage";

        String payload = APIUtilities.getObjectToJSON(shipmentUpdateInfo);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.MASTERBAG_NEW, new String[]{String.valueOf(masterbagId), endPoint},
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        String response = APIUtilities.getElement(service.getResponseBody(), responseFormat, "json");
        return response;
    }

   // @Step("QC Pass for self ship return")
    public String selfShipPickupQCUpdates(String returnId,String tenantId,Long clientId,ShipmentUpdateEvent shipmentUpdateEvent,
                                          ShipmentType shipmentType,ShipmentUpdateActivityTypeSource shipmentUpdateActivityTypeSource,
                                          String premiseId,Premise.PremiseType premiseType) throws Exception{

        SelfShipReturnQCUpdate selfShipReturnQCUpdate=new SelfShipReturnQCUpdate();
        selfShipReturnQCUpdate.setShipmentId(returnId);
        selfShipReturnQCUpdate.setEvent(shipmentUpdateEvent);
        selfShipReturnQCUpdate.setTenantId(tenantId);
        selfShipReturnQCUpdate.setClientId(String.valueOf(clientId));
        selfShipReturnQCUpdate.setEventLocation("Electronics City (ELC)-DC");
        selfShipReturnQCUpdate.setRemarks("Pickup QC check complete");
        selfShipReturnQCUpdate.setShipmentType(shipmentType);
        selfShipReturnQCUpdate.setShipmentUpdateMode(shipmentUpdateActivityTypeSource);
        selfShipReturnQCUpdate.setEventTime(new DateTime());

        EventLocationPremise eventLocationPremise=new EventLocationPremise();
        eventLocationPremise.setPremiseId(premiseId);
        eventLocationPremise.setPremisesType(premiseType);
        selfShipReturnQCUpdate.setEventLocationPremise(eventLocationPremise);

        String payload = APIUtilities.getObjectToJSON(selfShipReturnQCUpdate);

        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.SELF_SHIP_PICKUP_QC_UPDATE, null,
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());

        String response = APIUtilities.getElement(service.getResponseBody(), "status.statusMessage", "json");
    	return response;
    }

    /**
     * @author Shanmugam Yuvaraj
     * @param trackingNumber
     * @param tenantId
     * @return
     * @throws InterruptedException 
     */

   // @Step("receive Shipments in DC by using tenantId and trackingNumber")
    public String receiveShipmentInDC(String trackingNumber,String tenantId) throws InterruptedException {
    	Thread.sleep(Lastmile_ReturnManagement_constants.wait_Time);
        String pathParam = "?operationalTrackingId=" + trackingNumber + "&tenantId=" + tenantId+ "";
        String response="";
        try {
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.RECEIVE_SHIPMENT_BEFORE_TRIP_COMPLETE, new String[]{pathParam}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, null, Headers.getLmsHeaderJSON());
            response = APIUtilities.getElement(service.getResponseBody(), "tripOrderResponse.status.statusType", "json");
        }catch (Exception e){
            e.printStackTrace();
        }
//        TripOrderResponse tripOrderResponse=(TripOrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderResponse());
        return response;
    }




   // @Step("self Ship PickUp Approve By CC")
    public String selfShipPickUpApproveByCC(String returnId,ShipmentUpdateEvent toEvnet)throws UnsupportedEncodingException,IOException{


        Map<String, Object> return_shipment = DBUtilities.exSelectQueryForSingleRecord("select * from return_shipment where source_return_id = " + returnId, "lms");
        String trackingNumber=return_shipment.get("tracking_number").toString();
        String courierCode=return_shipment.get("courier_code").toString();

        ShipmentUpdate.ShipmentUpdateBuilder pickupBuilder = new ShipmentUpdate.ShipmentUpdateBuilder(courierCode, trackingNumber,
                toEvnet);
        pickupBuilder.eventLocation("Return Processing Center");
        pickupBuilder.eventTime(new DateTime());
        pickupBuilder.shipmentId(returnId);
        pickupBuilder.shipmentType(ShipmentType.RETURN);
        pickupBuilder.tenantId(TENANTID);
        pickupBuilder.clientId(CLIENTID);
        pickupBuilder.shipmentUpdateActivitySource(ShipmentUpdateActivityTypeSource.LogisticsPlatform);
        pickupBuilder.userName(Context.getContextInfo().getLoginId());
        pickupBuilder.remarks("Acknowledge approve onHold pickup with  RPC");
        pickupBuilder.eventAdditionalInfo(null);
        ShipmentUpdate updatePickupStatus = pickupBuilder.build();
        String payload = APIUtilities.getObjectToJSON(updatePickupStatus);
        String pathParam="?tenantId="+TENANTID+"&clientId="+CLIENTID+"";
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_RETURN_STATUS_EVENT, new String[]{pathParam},
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.PUT, "["+payload+"]", Headers.getLmsHeaderJSON());

        String response = APIUtilities.getElement(service.getResponseBody(), "status.statusMessage", "json");
        return response;
    }

   // @Step("create csv file ")
    public String createCSVFile(Long returnId,String courier_code,String tracking_number) throws IOException {
        java.sql.Date currentTimestamp = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
        String uploadFileHeader = "\"Return Id\",\"Reship Courier Code\",\"Reship Tracking Number\"";

        File file = new File("./Data/lms/csv/ReshipToCustomer.csv");
        if (file.exists()) {
            file.delete();
             file = new File("./Data/lms/csv/ReshipToCustomer.csv");
        }
        FileWriter writer = new FileWriter(file, true);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(uploadFileHeader);
        bufferedWriter.newLine();
        bufferedWriter.write(returnId+ "," + courier_code + "," + tracking_number );
        bufferedWriter.newLine();
        bufferedWriter.close();
        return file.getPath();
    }

   // @Step("create token for file upload")
    public static HashMap<String, String> getTokenForFileUpload() {
        HashMap<String, String> Headers = new HashMap<>();
        Headers.put("Authorization", "Basic dXNlcjE6dXNlcjFQYXNz,Basic Mzoz");
        Headers.put("Accept","application/json");
        return Headers;
    }

   // @Step("complete reship to customer")
    public String reshipToCustomer(String filePath) throws UnsupportedEncodingException, JAXBException {
        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
        multipartEntityBuilder.addPart("file",new FileBody(new File(filePath)));
        multipartEntityBuilder.setContentType(ContentType.MULTIPART_FORM_DATA);
        Svc service = HttpExecutorService.executeHttpServiceForUpload(Constants.LMS_PATH.RESHIP_TO_CUSTOMER,null,SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.POST,multipartEntityBuilder,getTokenForFileUpload());
        String response=service.getResponseBody();
        return response;
    }

    // @Step("Find orders by trip id")
    public TripOrderAssignmentResponse findOrdersByTripId(final String tripId, final ShipmentType shipment_type, final String tenantId) throws WebClientException, UnsupportedEncodingException, JAXBException, XMLStreamException, JSONException, JSONException, XMLStreamException {
        String FGPathparam = tripId + "/" + shipment_type + "?tenantId=" + tenantId;
        Svc service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.GETTRIPORDERDETAILS, new String[]{FGPathparam},
                SERVICE_TYPE.Last_mile.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        TripOrderAssignmentResponse response = (TripOrderAssignmentResponse) APIUtilities.jsonToObject(service.getResponseBody(),
                TripOrderAssignmentResponse.class);
        return response;
    }


    /**
     * assignPickupToHLP it will return status type
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.TriFunction assignPickupToHLPWithCourierCode = (trackingNumber, hlpCourierCode, tenantId) -> {
        String  FGASSIGN_SHIPMENT_TO_HLP=Constants.LMS_PATH.ASSIGN_SHIPMENT_TO_HLPVersion2;
        String payload = "[\n" +
                "  {\n" +
                "    \"trackingNumber\": \""+trackingNumber+"\",\n" +
                "    \"eventTime\": \"2019-05-07 22:44:52\",\n" +
                "    \"eventAdditionalInfo\": null,\n" +
                "    \"deliveryCenterId\": \"5\",\n" +
                "    \"eventLocation\": \"DC-5\",\n" +
                "    \"remarks\": \"ASSIGN_TO_LAST_MILE_PARTNER\",\n" +
                "    \"userName\": \"null\",\n" +
                "    \"event\": \"ASSIGN_TO_LAST_MILE_PARTNER\",\n" +
                "    \"shipmentType\": \"PU\",\n" +
                "    \"shipmentUpdateMode\": \"MyntraLogistics\",\n" +
                "    \"hlpCourierCode\": \""+hlpCourierCode+"\",\n" +
                "    \"tenantId\": \""+tenantId+"\",\n" +
                "    \"hlpTrackingNumber\": \""+trackingNumber+"\"\n" +
                "  }\n" +
                "]";
        return APIUtilities.getElement(HttpExecutorService.executeHttpService(FGASSIGN_SHIPMENT_TO_HLP, null, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON()).getResponseBody(),"mlShipmentResponse.status.statusType","json");};

    /**
     * assignPickupToHLP,will Return status message
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.TriFunction assignPickupToHLP = (trackingNumber, hlpCourierCode, tenantId) -> {
        String  FGASSIGN_SHIPMENT_TO_HLP=Constants.LMS_PATH.ASSIGN_SHIPMENT_TO_HLPVersion2;
        String payload = "[\n" +
                "  {\n" +
                "    \"trackingNumber\": \""+trackingNumber+"\",\n" +
                "    \"eventTime\": \"2019-05-07 22:44:52\",\n" +
                "    \"eventAdditionalInfo\": null,\n" +
                "    \"deliveryCenterId\": \"5\",\n" +
                "    \"eventLocation\": \"DC-5\",\n" +
                "    \"remarks\": \"ASSIGN_TO_LAST_MILE_PARTNER\",\n" +
                "    \"userName\": \"null\",\n" +
                "    \"event\": \"ASSIGN_TO_LAST_MILE_PARTNER\",\n" +
                "    \"shipmentType\": \"PU\",\n" +
                "    \"shipmentUpdateMode\": \"MyntraLogistics\",\n" +
                "    \"hlpCourierCode\": \""+hlpCourierCode+"\",\n" +
                "    \"tenantId\": \""+tenantId+"\",\n" +
                "    \"hlpTrackingNumber\": \""+trackingNumber+"\"\n" +
                "  }\n" +
                "]";
        return APIUtilities.getElement(HttpExecutorService.executeHttpService(FGASSIGN_SHIPMENT_TO_HLP, null, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON()).getResponseBody(),"mlShipmentResponse.status.statusMessage","json");};

    public TripShipmentAssociationResponse getShipmentIdUsingTripId(Long tripId,String tenantId) throws UnsupportedEncodingException, JAXBException {
        FGPathparam= "?tenantId="+tenantId;
        Svc service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.FIND_SHIPMENTS_BY_TRIP_ID+"/"+tripId, new String[]{FGPathparam} , SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        TripShipmentAssociationResponse tripShipmentAssociationResponse = (TripShipmentAssociationResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new TripShipmentAssociationResponse());
        return tripShipmentAssociationResponse;
    }

    public TripOrderAssignmentResponse closeMyntraTripWithStoreBag(String myntraTenantId, Long myntraTripId) throws UnsupportedEncodingException, JAXBException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;

        FGPathparam = "?tenantId=" + myntraTenantId;
        Svc service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.COMPLETE_TRIP + "/" + myntraTripId, new String[]{FGPathparam}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, null, Headers.getLmsHeaderXML());
        TripOrderAssignmentResponse tripShipmentAssociationResponse = (TripOrderAssignmentResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        return tripShipmentAssociationResponse;

    }
   
   // @Step("Create a return with schedule pickup slot")
    public com.myntra.returns.response.ReturnResponse createReturnWithSchedulePickup(Long lineID, ReturnType returnType, ReturnMode returnMode, int quantity,
                                                                   Long returnReasonID, RefundMode refundMode, String refundAccountId,
                                                                   String address, String addressId, String pincode, String city,
                                                                   String state, String country, String mobileNumber,String ppsId,Long scheduleStart,Long scheduleEnd){

        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        OrderLineEntry orderLineEntry = omsServiceHelper.getOrderLineEntry(""+lineID);
        com.myntra.oms.client.entry.OrderEntry orderEntry = omsServiceHelper.getOrderEntry(orderLineEntry.getOrderId().toString());
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderEntry.getId().toString()));

        ReturnEntry returnEntry = new ReturnEntry();
        returnEntry.setOrderId(orderLineEntry.getOrderId());
        returnEntry.setReturnType(returnType);
        returnEntry.setReturnMode(returnMode);
        returnEntry.setStoreId(1);
        returnEntry.setLogin(orderEntry.getLogin());
        returnEntry.setCustomerName(orderReleaseEntry.getReceiverName());
        returnEntry.setComment("Return Creation Entry For Line ID: "+ lineID + " Quantity : " + quantity);
        returnEntry.setEmail(orderReleaseEntry.getEmail());
        returnEntry.setMobile(mobileNumber);

        //Set ReturnAddressDetailsEntry
        ReturnAddressDetailsEntry returnAddressDetailsEntry = new ReturnAddressDetailsEntry();
        returnAddressDetailsEntry.setAddress(address);
        returnAddressDetailsEntry.setAddressId(Long.valueOf(addressId));
        returnAddressDetailsEntry.setCity(city);
        returnAddressDetailsEntry.setCountry(country);
        returnAddressDetailsEntry.setState(state);
        returnAddressDetailsEntry.setZipcode(pincode);
        returnEntry.setReturnAddressDetailsEntry(returnAddressDetailsEntry);

        //Set Return Line Entries
        ReturnLineEntry returnLineEntry = new ReturnLineEntry();
        returnLineEntry.setComment("Line Entry Comment For Line ID "+lineID);
        returnLineEntry.setOrderId(orderLineEntry.getOrderId());
        returnLineEntry.setOrderLineId(lineID);
        returnLineEntry.setOrderReleaseId(orderLineEntry.getOrderReleaseId());
        returnLineEntry.setQuantity(quantity);
        returnLineEntry.setSupplyType(orderLineEntry.getSupplyType());
        returnLineEntry.setReturnReasonId(returnReasonID);
        returnLineEntry.setSkuId(orderLineEntry.getSkuId());
        returnLineEntry.setOptionId(orderLineEntry.getOptionId());
        returnLineEntry.setStyleId(orderLineEntry.getStyleId());
        List<ReturnLineEntry> returnlineEntries = new ArrayList<>();
        returnlineEntries.add(returnLineEntry);
        returnEntry.setReturnLineEntries(returnlineEntries);

        ReturnRefundDetailsEntry returnRefundDetailsEntry = new ReturnRefundDetailsEntry();
        returnRefundDetailsEntry.setRefundMode(refundMode);
        returnRefundDetailsEntry.setRefundAccountId(refundAccountId);
        returnEntry.setReturnRefundDetailsEntry(returnRefundDetailsEntry);

        ReturnAdditionalDetailsEntry returnAdditionalDetailsEntry=new ReturnAdditionalDetailsEntry();
        returnAdditionalDetailsEntry.setPaymentPpsId(ppsId);
        returnAdditionalDetailsEntry.setRouteId("RMS");
        returnAdditionalDetailsEntry.setAnySlot(false);
        returnAdditionalDetailsEntry.setPickupSlotEntry("{\"scheduleType\":\"SLOT\",\"scheduleStart\":"+scheduleStart+",\"scheduleEnd\":"+scheduleEnd+",\"isActive\":true}");
        returnEntry.setReturnAdditionalDetailsEntry(returnAdditionalDetailsEntry);
       
        String payLoad = null;
        try {
            payLoad = APIUtilities.getObjectToJSON(returnEntry);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Svc service = null;
        try {
            service = HttpExecutorService.executeHttpService(Constants.RMS_PATH.CREATE_RETURN, null, SERVICE_TYPE.RMS_SVC.toString(), HTTPMethods.POST, payLoad, getRMSHeader());
        } catch (UnsupportedEncodingException e) {
            Assert.fail("Test fail : Return creation failed due to UnsupportedEncodingException");
        }
        com.myntra.returns.response.ReturnResponse returnResponse = null;
        try {
            returnResponse = (com.myntra.returns.response.ReturnResponse) APIUtilities.getJsontoObject(service.getResponseBody(), new com.myntra.returns.response.ReturnResponse());
        } catch (IOException e) {
            Assert.fail("Test fail : Return creation failed due to IOException");
        }
        return returnResponse;
    }

   // @Step("Create epoch time ")
    public Long convertTimetoEpoch(String timeWithTZ){
        // Sample input dormat : 2019-06-07T02:00:00+05:30
        ZonedDateTime result = ZonedDateTime.parse(timeWithTZ, DateTimeFormatter.ISO_DATE_TIME);
        Date date = Date.from(result.toInstant());
        return date.toInstant().toEpochMilli();
    }

    public String createCSVFileForGreenChannel(List<String> lstTrackingNumber,String tenantId) throws IOException {
        String uploadFileHeader = "\"Tracking Number\",\"Tenant Id\"";

        File file = new File("./Data/lms/csv/GreenChannel.csv");
        if (file.exists()) {
            file.delete();
            file = new File("./Data/lms/csv/GreenChannel.csv");
        }
        FileWriter writer = new FileWriter(file, true);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(uploadFileHeader);
        bufferedWriter.newLine();
        for(String trackingNumber:lstTrackingNumber){
            bufferedWriter.write("\""+trackingNumber+"\",\""+tenantId+"\"");
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
        return file.getPath();
    }

   // @Step("upload green channel ")
    public String uploadGreenChannel(String filePath) throws UnsupportedEncodingException {

        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
        multipartEntityBuilder.addPart("file",new FileBody(new File(filePath)));
        multipartEntityBuilder.setContentType(ContentType.MULTIPART_FORM_DATA);
        Svc service = HttpExecutorService.executeHttpServiceForUpload(Constants.LMS_PATH.GREEN_CHANNEL,null,SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.POST,multipartEntityBuilder,getTokenForFileUpload());
        String response=service.getResponseBody();

        return response;

    }

   // @Step("get pickup shipment details")
    public String findPickupFromSourceReturnId(String returnId,String tenantId,Long sourceId) throws UnsupportedEncodingException {
        FGPathparam= "?sourceReturnId="+returnId+"&tenantId="+tenantId+"&sourceId="+sourceId;
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.PICKUP_SHIPMENT_STATUS_LMS, new String[]{FGPathparam} , SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        Assert.assertEquals(APIUtilities.getElement(service.getResponseBody(), "status.statusMessage", "json"), ResponseMessageConstants.pickupShipmentRetrieved,"pickup Shipment is not Retrieved successfully ");

        return APIUtilities.getElement(service.getResponseBody(), "data.shipmentStatus", "json");
    }

   // @Step("Decline return after creation")
    public com.myntra.returns.response.ReturnResponse declineReturn(Long returnId, ReturnActionCode returnActionCode,String courierService,
                                                                    String trackingNumber,Long sku) throws IOException {
        com.myntra.apiTests.erpservices.lastmile.model.ReturnUpdateRequestEntry returnUpdate= new ReturnUpdateRequestEntry();
        returnUpdate.setReturnId(returnId);
        returnUpdate.setReturnActionCode(returnActionCode);
        returnUpdate.setCancellationType(CancellationType.LOGISTICS_CANCELLATION);
        returnUpdate.setUserComment("Logistics Processing Initiated");
        returnUpdate.setCreatedOn(new Date());
        returnUpdate.setCreatedBy("RMS-SYSTEM");
        returnUpdate.setCourierService(courierService);
        returnUpdate.setTrackingNumber(trackingNumber);
        ReturnLineUpdateRequestEntryList returnLineUpdateRequestEntry=new ReturnLineUpdateRequestEntryList();
        returnLineUpdateRequestEntry.setSkuId(sku);
        returnLineUpdateRequestEntry.setQuantity(1);

        List<ReturnLineUpdateRequestEntryList> lstReturnLineUpdateRequestEntry=new ArrayList<>();
        lstReturnLineUpdateRequestEntry.add(returnLineUpdateRequestEntry);
        returnUpdate.setReturnLineUpdateRequestEntryList(lstReturnLineUpdateRequestEntry);

        String payLoad = APIUtilities.getObjectToJSON(returnUpdate);
        Svc service = HttpExecutorService.executeHttpService(Constants.RMS_PATH.RETURN_STATUS_PROCESS+"/"+returnId, null, SERVICE_TYPE.RMS_SVC.toString(), HTTPMethods.PUT, payLoad, getRMSHeader());
        com.myntra.returns.response.ReturnResponse returnResponse =(com.myntra.returns.response.ReturnResponse) APIUtilities.getJsontoObject(service.getResponseBody(), new com.myntra.returns.response.ReturnResponse());
        return returnResponse;
    }

   // @Step("Mark close box return as QC fail ON_HOLD or SHORTAGE")
    public String performClosedBoxReturnQC(String returnId, ItemQCStatus status, String courier_code) throws IOException {
        //Find the pickup id
        String pickupId = lms_returnHelper.getPickupIdOfReturn(returnId);
        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId);

        List<String> returnList = lms_returnHelper.getReturnsInPickup(pickupId);
        //We have to process all the returns in the pickup, the one passed as a parameter will be processed for the status which is passed, rest of the returns will be randomly processed
        returnList.remove(returnId);

        Map<String, ItemQCStatus> returnStatusMap = new HashMap<>();
        for (String remainingReturn : returnList) {
            if (status.toString().equalsIgnoreCase(ItemQCStatus.ON_HOLD.toString())) {
                returnStatusMap.put(remainingReturn, ItemQCStatus.ON_HOLD);

            } else if(status.toString().equalsIgnoreCase(ItemQCStatus.SHORTAGE.toString())){
                returnStatusMap.put(remainingReturn, ItemQCStatus.SHORTAGE);

            }
        }
        ArrayList<ReturnShipmentUpdate> returnShipmentUpdates = new ArrayList<>();

        ReturnShipmentUpdate returnShipmentUpdate1 = new ReturnShipmentUpdate();
        returnShipmentUpdate1.setSourceReturnId(returnId);
        returnShipmentUpdate1.setRemark("Remark");
        returnShipmentUpdate1.setItemQCStatus(status);
        returnShipmentUpdates.add(returnShipmentUpdate1);

        for (Map.Entry<String, ItemQCStatus> entry : returnStatusMap.entrySet()) {
            ReturnShipmentUpdate returnShipmentUpdate = new ReturnShipmentUpdate();
            returnShipmentUpdate.setSourceReturnId(entry.getKey());
            returnShipmentUpdate.setRemark("Remark");
            returnShipmentUpdate.setItemQCStatus(entry.getValue());
            returnShipmentUpdates.add(returnShipmentUpdate);
        }


        ClosedBoxPickupQCCompleteUpdate closedBoxPickupQCCompleteUpdate;
        closedBoxPickupQCCompleteUpdate = new ClosedBoxPickupQCCompleteUpdate(returnId, TENANTID, CLIENTID,
                courier_code, tracking_number, ShipmentUpdateEvent.QUALITY_CHECK_COMPLETE, null, 5L,
                new Premise("5", Premise.PremiseType.DELIVERY_CENTER), "Location", new DateTime(), "remarks", ShipmentType.PU, ShipmentUpdateActivityTypeSource.LogisticsPortal, "Gloria", null, returnShipmentUpdates, null, null, null, null, null, null, null);
        String payload = APIUtilities.getObjectToJSON(closedBoxPickupQCCompleteUpdate);

        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_CLOSEDBOX_QC_STATUS, null,
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        String response = APIUtilities.getElement(service.getResponseBody(), "status.statusMessage", "json");
        return response;
    }

   // @Step("Receive close box return in WH")
    public String  receiveReturn(String returnId, String trackingNo,String tenantId,Long sourceId) throws IOException, JAXBException {

        FGPathparam= "?trackingNumber="+trackingNo+"&sourceReturnId="+returnId+"&sourceId="+sourceId+"&tenantId="+tenantId;

        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.RECEIVERETURNINWH, new String[]{FGPathparam},
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.PUT, null, Headers.getLmsHeaderJSON());
        String response = APIUtilities.getElement(service.getResponseBody(), "status.statusMessage", "json");

        return response;
    }

    public void manifest(String returnType,String courierCode,String tenantId,String clientId) throws UnsupportedEncodingException{
        String start_date = LocalDateTime.now().toLocalDate().toString().replaceAll("-", "").concat("000000");
        String end_date = LocalDateTime.now().plusDays(1).toLocalDate().toString().replaceAll("-","").concat("000000");

        //String[] pathParam={start_date+"/"+end_date+"?tenantId="+tenantId+"&sourceId="+clientId+""};
        String[] pathParam={MessageFormat.format("{0}/{1}/{2}/{3}?tenantId={4}&sourceId={5}",returnType,courierCode,start_date,end_date,tenantId,clientId)};
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.MANIFEST, pathParam, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, null, Headers.getLmsHeaderJSON());

        String response = APIUtilities.getElement(service.getResponseBody(), "status.statusType", "json");
        response.equals("SUCCESS");
    }

   // @Step("get pickup shipment details")
    public String checkManifestDone(String returnId,String tenantId,Long sourceId) throws UnsupportedEncodingException {
        FGPathparam= "?sourceReturnId="+returnId+"&tenantId="+tenantId+"&sourceId="+sourceId;
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.PICKUP_SHIPMENT_STATUS_LMS, new String[]{FGPathparam} , SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        Assert.assertEquals(APIUtilities.getElement(service.getResponseBody(), "status.statusMessage", "json"), ResponseMessageConstants.pickupShipmentRetrieved,"pickup Shipment is not Retrieved successfully ");

        return APIUtilities.getElement(service.getResponseBody(), "data[0].manifested", "json");
    }

   // @Step("get nearest warehouse for the perticular pincode")
    public String getNearestWHId(String source_warehouse_id,String courier_code,String zipcode_prefix){
       
        String query="select rto_destination_warehouse_id from return_warehouse_config where source_warehouse_id="+source_warehouse_id+" and courier_code="+"'"+courier_code+"'"+" and zipcode_prefix="+zipcode_prefix+";";
    	
        Map<String, Object> input= DBUtilities.exSelectQueryForSingleRecord(query,"myntra_lms");
        if(input.isEmpty()){
            Assert.fail("The entered query doesn't returned any value");
        }
        String trackingNumber=input.get("rto_destination_warehouse_id").toString();
        return trackingNumber;
    }

    //remove store shipment from trip
    public EmptyResponse removeStoreShipmentFromTrip(Long shipmentId, Long tripId) throws UnsupportedEncodingException, JAXBException {
        String path = MessageFormat.format( "{0}/removeShipment/{1}/TRIP/FromTrip/{2}",Constants.LASTMILE_PATH.Update_Trip,shipmentId.toString(),tripId.toString());
        Svc service = HttpExecutorService.executeHttpService(path, null , SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, null, Headers.getLmsHeaderXML());
        EmptyResponse emptyResponse = (EmptyResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new EmptyResponse());
        return emptyResponse;
    }

    /**
     * get mapped DC id for the store dc
     * @param hlpDCId
     * @return
     */
    public Map<String, Object> getMappedDCDetails(String hlpDCId){
        return DBUtilities.exSelectQueryForSingleRecord(MessageFormat.format("SELECT * FROM dc_hlp_config WHERE hlp_dc_id={0};",hlpDCId),"myntra_lms");
    }

    /**
     * Used to validate cash in OMS and LMS db's
     * @param iscod
     * @param codAmount
     * @param packetId
     */
    @SneakyThrows
    public void cashValidation(String iscod, double codAmount, String packetId,String tenantId,String clientId) {
        Map<String, Object> packetDetails = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.CashValidationQuery.orderLineTableData), packetId), "myntra_oms");
        String unitPrice = String.valueOf(packetDetails.get("unit_price"));
        switch (iscod) {
            case "true":
                Assert.assertTrue(codAmount != 0);
                Assert.assertTrue(codAmount == Double.parseDouble(unitPrice), "cod amount is not matching from OMS and LMS system");

                //validation cod amount from OMS and OrderToShip table in LMS
                OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
                Assert.assertEquals(orderResponse.getStatus().getStatusMessage(), "ORDER(s) retrieved successfully", "Order_to_ship details is not retrieved");
                Assert.assertTrue(Double.parseDouble(orderResponse.getOrders().get(0).getCodAmount().toString()) == Double.parseDouble(unitPrice), "Shipment value in lms-MLShipment is not matching with unit price OMS");

                //validate Shipment Item validation
                Map<String, Object> shipmentItemDetailsForCash = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.CashValidationQuery.shipmentItemTableData), packetId), "myntra_lms");

                Assert.assertTrue(Double.parseDouble(shipmentItemDetailsForCash.get("item_value").toString()) == Double.parseDouble(unitPrice),
                        "In Shipment item table item value is not expected when payment type as cod,Found - " + Double.parseDouble(shipmentItemDetailsForCash.get("item_value").toString()));
                Assert.assertTrue(Double.parseDouble(shipmentItemDetailsForCash.get("item_mrp").toString()) == Double.parseDouble(unitPrice),
                        "In Shipment item table item_mrp value is not expected when payment type as cod,Found - " + Double.parseDouble(shipmentItemDetailsForCash.get("item_mrp").toString()));
                Assert.assertTrue(Double.parseDouble(shipmentItemDetailsForCash.get("cod_amount").toString()) == Double.parseDouble(unitPrice),
                        "In Shipment item table cod_amount value is not expected when payment type as cod,Found - " + Double.parseDouble(shipmentItemDetailsForCash.get("cod_amount").toString()));

                //ml delivery shipment validation
                Map<String, Object> mlDeliveryShipmentDetailsForCash = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.CashValidationQuery.mlDeliveryShipmentTableData), packetId), "myntra_lms");
                Assert.assertTrue(Double.parseDouble(mlDeliveryShipmentDetailsForCash.get("cod_amount").toString()) == Double.parseDouble(unitPrice),
                        "In ml_Delivery_Shipment table item value is not expected when payment type as cod,Found - " + Double.parseDouble(mlDeliveryShipmentDetailsForCash.get("cod_amount").toString()));

                //order_additional_info
                Map<String, Object> orderAdditionalInfoDetailsForCash = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.CashValidationQuery.orderAdditionalInfoTableData), packetId), "myntra_lms");
                Assert.assertTrue(Double.parseDouble(orderAdditionalInfoDetailsForCash.get("total").toString()) == Double.parseDouble(unitPrice),
                        "In order_additional_info table total value is not expected when payment type as cod,Found - " + Double.parseDouble(orderAdditionalInfoDetailsForCash.get("total").toString()));
                Assert.assertTrue(Double.parseDouble(orderAdditionalInfoDetailsForCash.get("subtotal").toString()) == Double.parseDouble(unitPrice),
                        "In order_additional_info table subtotal value is not expected when payment type as cod,Found - " + Double.parseDouble(orderAdditionalInfoDetailsForCash.get("subtotal").toString()));
                //shipping label validation
                String shippingLabelV2 = labelParsers.parseShippingLabelV2(packetId, tenantId, clientId);
                Assert.assertEquals(shippingLabelV2.contains("NORMAL"),true,"shipping method is not matching as NORMAL");
                Assert.assertEquals(shippingLabelV2.contains("COD"),true,"Payment type is not matching as COD");
                Assert.assertEquals(shippingLabelV2.contains("Rs.1099.0"),true,"COD amount is not matching");

                break;
            case "false":
                Assert.assertTrue(codAmount==0.0, "non COD orders having cod value");
                //validate Shipment Item validation
                Map<String, Object> shipmentItemDetails = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.CashValidationQuery.shipmentItemTableData), packetId), "myntra_lms");

                Assert.assertTrue(Double.parseDouble(shipmentItemDetails.get("item_value").toString()) != 0,
                        "In Shipment item table item value is not expected when payment type as non cod,Found - " + Double.parseDouble(shipmentItemDetails.get("item_value").toString()));
                Assert.assertTrue(Double.parseDouble(shipmentItemDetails.get("item_mrp").toString()) != 0,
                        "In Shipment item table item_mrp value is not expected when payment type as non cod,Found - " + Double.parseDouble(shipmentItemDetails.get("item_mrp").toString()));
                Assert.assertTrue(Double.parseDouble(shipmentItemDetails.get("cod_amount").toString()) == 0,
                        "In Shipment item table cod_amount value is not expected when payment type as non cod,Found - " + Double.parseDouble(shipmentItemDetails.get("cod_amount").toString()));

                //validation cod amount from OMS and OrderToShip table in LMS
                OrderResponse orderResponseForOnline = lmsServiceHelper.getOrderDetails(packetId);
                Assert.assertEquals(orderResponseForOnline.getStatus().getStatusMessage(), "ORDER(s) retrieved successfully", "Order_to_ship details is not retrieved");
                Assert.assertTrue(Double.parseDouble(orderResponseForOnline.getOrders().get(0).getCodAmount().toString()) == Double.parseDouble("0.0"), "Shipment value in lms-MLShipment is not matching with unit price OMS");

                //order_additional_info
                Map<String, Object> orderAdditionalInfoDetailsForOnline = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.CashValidationQuery.orderAdditionalInfoTableData), packetId), "myntra_lms");
                Assert.assertTrue(Double.parseDouble(orderAdditionalInfoDetailsForOnline.get("total").toString()) == Double.parseDouble(unitPrice),
                        "In order_additional_info table total value is not expected when payment type as cod,Found - " + Double.parseDouble(orderAdditionalInfoDetailsForOnline.get("total").toString()));
                Assert.assertTrue(Double.parseDouble(orderAdditionalInfoDetailsForOnline.get("subtotal").toString()) == Double.parseDouble(unitPrice),
                        "In order_additional_info table subtotal value is not expected when payment type as cod,Found - " + Double.parseDouble(orderAdditionalInfoDetailsForOnline.get("subtotal").toString()));

                //ml delivery shipment validation
                Map<String, Object> mlDeliveryShipmentDetails = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.CashValidationQuery.mlDeliveryShipmentTableData), packetId), "myntra_lms");
                Assert.assertTrue(Double.parseDouble(mlDeliveryShipmentDetails.get("cod_amount").toString()) == 0,
                        "In ml_Delivery_Shipment table item value is not expected when payment type as non cod,Found - " + Double.parseDouble(mlDeliveryShipmentDetails.get("cod_amount").toString()));
                try{
                    Assert.assertNull(mlDeliveryShipmentDetails.get("payment_method").toString(), "payment method is not null when payment type is not cod");
                }catch (NullPointerException n){
                    n.getMessage();
                    System.out.println("Null pointer exception accrued");
                }
                //shipping label validation
                String shippingLabelValidation = labelParsers.parseShippingLabelV2(packetId, tenantId, clientId);
                Assert.assertEquals(shippingLabelValidation.contains("NORMAL"),true,"shipping method is not matching as NORMAL");
                Assert.assertEquals(shippingLabelValidation.contains("ON"),true,"Payment type is not matching as ON");
                Assert.assertEquals(shippingLabelValidation.contains("Rs.0.0"),true,"Amount is not matching for online orders");
                break;
        }
    }
}
