package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lms.client.response.HubWareHouseConfigResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.springframework.http.MediaType;
import org.testng.Assert;

import java.text.MessageFormat;

public class LMSOrderDetailsClient_QA {
    //TODO : findByOrderId is not taking client id in request, hence this api is saying "No orders found" to check with dev about this
    public OrderResponse getLMSOrderDetails(String orderId,String tenantId,String clientId) {
        OrderResponse orderResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH. findByOrderId;
            String pathParm= MessageFormat.format("{0}?tenantId={1}&clientId={2}",orderId,tenantId,clientId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            orderResponse = (OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderResponse());


        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Could not find the order details in LMS");
        }
        return orderResponse;
    }
}
