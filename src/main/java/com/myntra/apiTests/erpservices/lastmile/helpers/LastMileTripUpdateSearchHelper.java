package com.myntra.apiTests.erpservices.lastmile.helpers;

import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lms.client.status.ShipmentType;
import org.testng.Assert;
import java.util.Map;

public class LastMileTripUpdateSearchHelper {

    /**
     * used to validate api and DB data
     * @param tripUpdateFilterSearch
     * @param getTripDetailsUsingTripId
     */
    public void validateTripUpdatePageData(TripOrderAssignmentResponse tripUpdateFilterSearch, Map<String, Object> getTripDetailsUsingTripId){

        try{
            //validation for api data and DB data
            Assert.assertEquals(tripUpdateFilterSearch.getTripOrders().get(0).getTripId().toString(),getTripDetailsUsingTripId.get("trip_id").toString(),"Trip_id is not matching for the details which has taken from api and db  ");
            Assert.assertEquals(tripUpdateFilterSearch.getTripOrders().get(0).getDeliveryStaffId().toString(),getTripDetailsUsingTripId.get("delivery_staff_id").toString(),"delivery_staff_id is not matching for the details which has taken from api and db  ");
            Assert.assertEquals(tripUpdateFilterSearch.getTripOrders().get(0).getTripNumber(),getTripDetailsUsingTripId.get("trip_number").toString(),"trip_number is not matching for the details which has taken from api and db  ");
            Assert.assertEquals(tripUpdateFilterSearch.getTripOrders().get(0).getTrackingNumber(),getTripDetailsUsingTripId.get("tracking_no").toString(),"tracking_no is not matching for the details which has taken from api and db  ");
            if(!(tripUpdateFilterSearch.getTripOrders().get(0).getShipmentType().toString().equalsIgnoreCase(ShipmentType.PU.toString()))) {
                Assert.assertEquals(tripUpdateFilterSearch.getTripOrders().get(0).getOrderId(), getTripDetailsUsingTripId.get("order_id").toString(), "order_id is not matching for the details which has taken from api and db  ");
                Assert.assertEquals(tripUpdateFilterSearch.getTripOrders().get(0).getTripOrderStatus().toString(),getTripDetailsUsingTripId.get("trip_order_status").toString(),"trip_order_status is not matching for the details which has taken from api and db  ");
            }
            Assert.assertEquals(tripUpdateFilterSearch.getTripOrders().get(0).getCorrespondingTripStatus().toString(),getTripDetailsUsingTripId.get("trip_status").toString(),"trip_status is not matching for the details which has taken from api and db  ");
        }catch(NullPointerException n){
            n.printStackTrace();
            Assert.fail("getting null pointer exception");
        }

    }

    /**
     * used to validate api and DB data for store shipments
     * @param tripShipmentAssociationResponse
     * @param getTripShipmentDetailsUsingTripId
     */
    public void validateTripUpdatePageStoreShipmentsData(TripShipmentAssociationResponse tripShipmentAssociationResponse, Map<String, Object> getTripShipmentDetailsUsingTripId) {

        try {
            //validation for api data and DB data
            Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId(), getTripShipmentDetailsUsingTripId.get("trip_id").toString(), "Trip_id is not matching for the details which has taken from api and db  ");
            Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getMyntraSDATripNumber(), getTripShipmentDetailsUsingTripId.get("trip_number").toString(), "trip_number is not matching for the details which has taken from api and db  ");
            Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId(), getTripShipmentDetailsUsingTripId.get("shipment_id").toString(), "shipment_id is not matching for the details which has taken from api and db  ");
            Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), getTripShipmentDetailsUsingTripId.get("shipment_type").toString(), "shipment_type is not matching for the details which has taken from api and db  ");
            Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), getTripShipmentDetailsUsingTripId.get("status").toString(), "status is not matching for the details which has taken from api and db  ");

        } catch (NullPointerException n) {
            n.printStackTrace();
            Assert.fail("getting null pointer exception");
        }
    }
}
