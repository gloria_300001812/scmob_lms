package com.myntra.apiTests.erpservices.lastmile.client;

import com.myntra.apiTests.erpservices.returnComplete.client.BaseClient;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.client.StoreServiceClient;
import com.myntra.lastmile.client.entry.StoreEntry;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.StoreSummaryResponse;
import com.myntra.scm.utils.EnvironmentUtil;
import org.springframework.http.MediaType;
import org.testng.Assert;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class StoreClient implements BaseClient {
    StoreServiceClient storeServiceClient;
    String tenantId;
    String clientId;
    String serviceURL;

    public StoreClient(String env, String clientId, String tenantId) {
        EnvironmentUtil environmentUtil = null;
        try {
            environmentUtil = new EnvironmentUtil();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        try {
            this.serviceURL = environmentUtil.formUrl(env, "lastmile") + "lastmile-service";

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.tenantId = tenantId;
        this.clientId = clientId;

        storeServiceClient = new StoreServiceClient(this.serviceURL, 5, 10);
    }

    public StoreResponse createStore(StoreEntry storeEntry) {
        StoreResponse storeResponse = null;
        try {
            storeResponse = storeServiceClient.create(storeEntry, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            Assert.fail("Unable to create Store, unable to call lastmile client");
        }
        if (storeResponse.getStatus().getStatusMessage().toLowerCase().contains("failed")) {
            Assert.fail("Unable to create store , reason : " + storeResponse.getStatus().getStatusMessage());
        }

        return storeResponse;
    }


    public StoreResponse searchStoreByCode(int start, int fetchSize, String sortBy, String sortOrder, Boolean distinct, String q, String f) {
        StoreResponse storeResponse = null;
        try {
            storeResponse = storeServiceClient.filteredSearch(start, fetchSize, sortBy, sortOrder, distinct, q, f, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            Assert.fail("Unable to search Delivery Center, unable to call lastmile client");
        }
        return storeResponse;
    }

    public StoreResponse updateStore(StoreEntry storeEntry, Long hlpDCID) {
        StoreResponse storeResponse = null;
        try {
            storeResponse = storeServiceClient.update(storeEntry, hlpDCID, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            Assert.fail("Unable to update Store, unable to call lastmile client");
        }
        return storeResponse;

    }

    public StoreResponse findStoreById(long hlpId) {
        StoreResponse storeResponse = null;
        try {
            storeResponse = storeServiceClient.findById(hlpId, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            Assert.fail("Unable to search Delivery Center, unable to call lastmile client");
        }
        return storeResponse;
    }

    public StoreSummaryResponse storeSummaryResponse(Long deliveryCenterId, Long storeDcId) {
        StoreSummaryResponse storeSummaryResponse = null;
        try {
            storeSummaryResponse = storeServiceClient.summary(deliveryCenterId, storeDcId, false, MediaType.APPLICATION_XML.toString(), MediaType.APPLICATION_XML.toString(), 5000, 5000, 1, Context.getContextInfo());
        } catch (WebClientException e) {
            e.printStackTrace();
            Assert.fail("Unable to get Store summary, unable to call lastmile client");

        }
        return storeSummaryResponse;
    }
}
