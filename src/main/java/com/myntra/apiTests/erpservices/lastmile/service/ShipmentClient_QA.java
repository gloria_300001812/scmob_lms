package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lms.client.domain.response.ReturnResponse;
import com.myntra.lms.client.response.ShipmentActivityResponse;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.springframework.http.MediaType;
import org.testng.Assert;

import java.text.MessageFormat;

public class ShipmentClient_QA
{
    public ShipmentActivityResponse getShipmentActivityDetails(String shipmentId, ShipmentType shipmentType, OrderTrackingDetailLevel level,String tenantId,String sourceId) {
        ShipmentActivityResponse shipmentActivityResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH.findBy_Source_ReturnId;
            String pathParm= MessageFormat.format("?shipmentId={0}&shipmentType={1}&level={2}&tenantId={0}&sourceId={1}",shipmentId,shipmentType,level,tenantId,sourceId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            shipmentActivityResponse = (ShipmentActivityResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new ShipmentActivityResponse());

        } catch (Exception e) {
            Assert.fail("Unable to call api to fetch Shipment Activity Details");
        }
        return shipmentActivityResponse;
    }
}
