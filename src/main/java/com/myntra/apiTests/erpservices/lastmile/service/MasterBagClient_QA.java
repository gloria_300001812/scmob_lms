package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import com.myntra.logistics.platform.response.MasterbagResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.springframework.http.MediaType;
import org.testng.Assert;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;

public class MasterBagClient_QA {

    public String addShipmentToStoreBag(Long masterbagId, ShipmentUpdateInfo shipmentUpdateInfo)
            throws IOException {
        String endPoint = "addShipmentByTrackingNumber?tenantId=" + LMS_CONSTANTS.TENANTID;
        String responseFormat = "status.statusType";
        String payload = APIUtilities.getObjectToJSON(shipmentUpdateInfo);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.MASTERBAG_NEW, new String[]{String.valueOf(masterbagId), endPoint},
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        String response = APIUtilities.getElement(service.getResponseBody(), responseFormat, "json");
        return response;
    }

    public ShipmentResponse getShipmentOrderMap(Long masterBagId) {
        Svc service = null;
        try {
            service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.MASTER_BAG, new String[]{String.valueOf(masterBagId) + "?um=true"},
                    SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ShipmentResponse shipmentResponse = null;
        try {
            shipmentResponse = (ShipmentResponse) APIUtilities.getJsontoObject(service.getResponseBody(), new ShipmentResponse());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return shipmentResponse;
    }

}
