package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lms.client.domain.response.PickupResponse;
import com.myntra.lms.client.domain.response.ReturnResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.junit.Assert;
import org.springframework.http.MediaType;

import java.text.MessageFormat;

public class ReturnShipmentClient_QA {
    public ReturnResponse getReturnShipmentDetailsLMS(String returnId,String tenantId,String sourceId) {

        ReturnResponse returnShipmentResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH.findBy_Source_ReturnId;
            String pathParm= MessageFormat.format("{0}?tenantId={0}&sourceId={1}",returnId,tenantId,sourceId);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            returnShipmentResponse = (ReturnResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new ReturnResponse());



        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to fetch return details");
        }

        return returnShipmentResponse;
    }
}
