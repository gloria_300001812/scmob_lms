package com.myntra.apiTests.erpservices.lastmile.otp;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import org.apache.commons.lang.StringUtils;

import java.text.MessageFormat;
import java.util.Objects;

/**
 * @author Bharath.MC
 * @since Aug-2019
 */
public class OTPManager {
    private static final String START_TRIP_OTP_SECRET_KEY = "startTripOtpSecretKey";
    private static GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator();

    public static Integer generateOTP(String tripId) {
        String key = START_TRIP_OTP_SECRET_KEY + tripId;
        Integer totpPassword = googleAuthenticator.getTotpPassword(key);
        System.out.println(MessageFormat.format("generated OTP for entityId/tripId {0} - {1}", tripId, String.valueOf(totpPassword)));
        return totpPassword;
    }

    public static boolean validateOTP(String tripId, Integer otp) throws Exception {
        if (StringUtils.isBlank(tripId) || Objects.isNull(otp)) {
            throw new Exception(String.format("tripId[%s] OR OTP[%s] is empty or null " , tripId, otp));
        }
        String key = START_TRIP_OTP_SECRET_KEY + tripId;
        return googleAuthenticator.authorize(key, otp);
    }
}
