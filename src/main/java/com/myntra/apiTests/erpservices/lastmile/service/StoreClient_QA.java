package com.myntra.apiTests.erpservices.lastmile.service;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.entry.StoreEntry;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.StoreSummaryResponse;
import com.myntra.lms.client.response.CourierResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.springframework.http.MediaType;
import org.testng.Assert;

import java.text.MessageFormat;

public class StoreClient_QA {
    /*
     * CreateStore
     * Http method:- POST
     * */
    public StoreResponse createStore(StoreEntry storeEntry) {
        StoreResponse storeResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.CREATE_STORE;

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{}, serviceType,
                    HTTPMethods.POST, APIUtilities.convertXMLObjectToString(storeEntry), Headers.getLmsHeaderXML());
            storeResponse = (StoreResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new StoreResponse());
        } catch (Exception e) {
            Assert.fail("Unable to create Store, unable to call lastmile client");
        }
        if (storeResponse.getStatus().getStatusMessage().toLowerCase().contains("failed")) {
            Assert.fail("Unable to create store , reason : " + storeResponse.getStatus().getStatusMessage());
        }
        return storeResponse;
    }
    /*
     * filterSearch
     * Http method:- GET
     * */

    public StoreResponse searchStoreByCode(int start, int fetchSize, String sortBy, String sortOrder, Boolean distinct, String q, String f) {
        StoreResponse storeResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.filteredSearch;
            String urIpath ="?q={5}&f={6}&fetchSize={1}&sortOrder={3}&start={0}&distinct={4}&sortBy={2}";
            String pathParams = MessageFormat.format(urIpath, start, fetchSize, sortBy, sortOrder, distinct, q, f);

            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParams}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            storeResponse = (StoreResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new StoreResponse());
        } catch (Exception e) {
            Assert.fail("Unable to search Delivery Center, unable to call lastmile client");
        }
        return storeResponse;
    }
    /*
     * Update
     * Http method:- PUT
     * */
    public StoreResponse updateStore(StoreEntry storeEntry, Long hlpDCID) {
        StoreResponse storeResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Update_Store;
            String pathParm = Long.toString(hlpDCID);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.PUT, APIUtilities.convertXMLObjectToString(storeEntry), Headers.getLmsHeaderXML());
            storeResponse = (StoreResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new StoreResponse());
        } catch (Exception e) {
            Assert.fail("Unable to update Store, unable to call lastmile client");
        }
        return storeResponse;

    }

    /*
     * findStoreById
     * Http method:- GET
     * */
    public StoreResponse findStoreById(long hlpId) {
        StoreResponse storeResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Update_Store;
            String pathParm = Long.toString(hlpId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            storeResponse = (StoreResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new StoreResponse());
        } catch (Exception e) {
            Assert.fail("Unable to search Delivery Center, unable to call lastmile client");
        }
        return storeResponse;
    }

    /*
     * storeSummaryResponse
     * Http method:- GET
     * */
    public StoreSummaryResponse storeSummaryResponse(Long deliveryCenterId, Long storeDcId) {
        StoreSummaryResponse storeSummaryResponse = null;
        try {
            String serviceType = SERVICE_TYPE.Last_mile.toString();
            String pathURI = Constants.LASTMILE_PATH.Store_Summary;
            String pathParm = Long.toString(storeDcId);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.PUT, APIUtilities.convertXMLObjectToString(deliveryCenterId), Headers.getLmsHeaderXML());
            storeSummaryResponse = (StoreSummaryResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new DeliveryStaffResponse());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to get Store summary, unable to call lastmile client");

        }
        return storeSummaryResponse;
    }

    public CourierResponse searchCourierDataForStore(String code,String courierType) {
        CourierResponse courierResponse = null;
        try {
            String serviceType = SERVICE_TYPE.LMS_SVC.toString();
            String pathURI = Constants.LMS_PATH.COURIER_SEARCH;
            String pathParm = MessageFormat.format("?q=code.like:{0}___courierType.in:{1}&start=0&fetchSize=20&sortBy=code&sortOrder=ASC",code,courierType);
            Svc service = HttpExecutorService.executeHttpService(pathURI, new String[]{pathParm}, serviceType,
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            courierResponse = (CourierResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new CourierResponse());
        } catch (Exception e) {
            Assert.fail("Unable to search Delivery Center, unable to call lastmile client");
        }
        return courierResponse;
    }
}
