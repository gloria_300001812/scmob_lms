package com.myntra.apiTests.erpservices.serviceability;

public class CONSTANTS {
	public static int CAPACITY = 5;
	
	public static long PINCODE = 100050;
	public static long PINCODEJ = 100051;
	
	public static String REGIONCODE = "TAREC";
	public static String REGIONCODEJ = "TAREJ";
	
	public static int CODLIMIT = 1000;
	public static int MRPLIMIT = 2000;
	
	public static String COURIERCODE = "TACOU";
	static String COURIERTYPE = "NORMAL";
	public static String COURIERCODEJ = "TACOJ";
	
	public static String DCCODE = "TADCC";
	static String DCTYPE = "ML";
	public static String DCCODEJ = "TADCJ";
	
	public static long WAREHOUSEID = 20;
	static String SERVICETYPESTAT = "NORMAL:EXPRESS:SDD";
	
	/*
	String regionCode = "TAREC", regionCodeJ="TAREJ";
	long pincode = 100050, pincodeJ = 100051;	
	int codLimit = 1000, mrpLimit = 2000;
	
	//Courier
	String courierCode = "TACOU", courierType = "NORMAL", courierCodeJ = "TACOJ";
	//Data center
	String dcCode = "TADCC", dcType = "ML", dcCodeJ = "TADCJ";

	//For Upload TAT
	long warehouseId = 20; String serviceTypesTat = "NORMAL:EXPRESS:SDD";	
	//Set Courier capacity	
	boolean addRegionResult, addPincodeResult, addCourierResult, addDCResult, addCourierSerResult, addTatResult, addCapacityConfigResult, addCourierPreferResult = true;
	static boolean alreadyExecuted = false;
	int capacity = 5;
	*/


}
