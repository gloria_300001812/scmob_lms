package com.myntra.apiTests.erpservices.khata.helpers;
import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.khata.constants.khata_constants;
import com.myntra.khata.domain.*;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.*;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.util.*;


public class KhataServiceHelper {
    private static Logger log = Logger.getLogger(KhataServiceHelper.class);

   public static AccountResponse createAccount(Boolean isActive, AccountType accountType, String mobileNumber, String ownerReferenceId, Long parentAccountId, AccountType parentAccountType, Long reportingParentId, String reportingParentReferenceId) throws JAXBException , UnsupportedEncodingException {

        AccountResponse response = null;
        AccountEntry accountEntry = new AccountEntry();
        accountEntry.setFirstName("Automation");
        accountEntry.setLastName("Test");
        accountEntry.setIsActive(isActive);
        accountEntry.setType(accountType);
        accountEntry.setMobile(mobileNumber);
        accountEntry.setOwnerReferenceId(ownerReferenceId);
        accountEntry.setParentAccountId(parentAccountId);
        accountEntry.setParentType(parentAccountType);
        accountEntry.setReportingParentId(reportingParentId);
        accountEntry.setReportingParentReferenceId(reportingParentReferenceId);
        accountEntry.setCreatedBy("LMSAutomation");
        String payload = APIUtilities.convertXMLObjectToString(accountEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.CREATE_ACCOUNT,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        response = (AccountResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountResponse());
       if (response.getStatus().getStatusMessage().contains("Error")) {
           Assert.fail("Account creation failed , Check if Khata service is UP");
       }
        return  response;
    }

    public static AccountResponse getAccountbyId(Long accountId) throws  UnsupportedEncodingException , JAXBException{
        AccountResponse response = null;
        String pathParam = accountId.toString();
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_BY_ID,  new String[]{pathParam}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (AccountResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Get Account failed , Check if Khata service is UP");
        }
        return  response;
    }

    public static AccountResponse getAccountbyType(AccountType accountType)throws  UnsupportedEncodingException , JAXBException{
        AccountResponse response = null;
        String pathParam = accountType.toString();
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_BY_TYPE,  new String[]{pathParam}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (AccountResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Get Account failed , Check if Khata service is UP");
        }
        return  response;
    }

    public static AccountResponse updateAccountbyId (Long accountId , Boolean isActive, AccountType accountType, String mobileNumber, String ownerReferenceId, Long parentAccountId, AccountType parentAccountType, Long reportingParentId, String reportingParentReferenceId ) throws JAXBException , UnsupportedEncodingException{
        AccountResponse response = null;
        AccountEntry accountEntry = new AccountEntry();
        accountEntry.setFirstName("Automation");
        accountEntry.setLastName("Test");
        accountEntry.setIsActive(isActive);
        accountEntry.setType(accountType);
        accountEntry.setMobile(mobileNumber);
        accountEntry.setOwnerReferenceId(ownerReferenceId);
        accountEntry.setParentAccountId(parentAccountId);
        accountEntry.setParentType(parentAccountType);
        accountEntry.setReportingParentId(reportingParentId);
        accountEntry.setReportingParentReferenceId(reportingParentReferenceId);
        accountEntry.setCreatedBy("LMSAutomation");
        String payload = APIUtilities.convertXMLObjectToString(accountEntry);
        String pathParam = accountId.toString();
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_BY_ID,  new String[]{pathParam}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        response = (AccountResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Account updation failed , Check if Khata service is UP");
        }
        return  response;
    }

    public static AccountResponse getAccountbyownerRefernceandType (long ownerRefernceId , AccountType accountType)throws  UnsupportedEncodingException , JAXBException{
        AccountResponse response = null;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_BY_OWNER_REFERNCE_ID+"/"+ownerRefernceId+"/type/"+accountType,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (AccountResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Get Account failed  , Check if Khata service is UP");
        }
        return  response;
    }

    public static AccountResponse updateAccount (Boolean isActive, AccountType accountType, String mobileNumber, String ownerReferenceId, Long parentAccountId, AccountType parentAccountType, Long reportingParentId, String reportingParentReferenceId ) throws JAXBException , UnsupportedEncodingException{
        AccountResponse response = null;
        AccountEntry accountEntry = new AccountEntry();
        accountEntry.setFirstName("Automation");
        accountEntry.setLastName("Test");
        accountEntry.setIsActive(isActive);
        accountEntry.setType(accountType);
        accountEntry.setMobile(mobileNumber);
        accountEntry.setOwnerReferenceId(ownerReferenceId);
        accountEntry.setParentAccountId(parentAccountId);
        accountEntry.setParentType(parentAccountType);
        accountEntry.setReportingParentId(reportingParentId);
        accountEntry.setReportingParentReferenceId(reportingParentReferenceId);
        accountEntry.setCreatedBy("LMSAutomation");
        String payload = APIUtilities.convertXMLObjectToString(accountEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.UPDATE_ACCOUNT,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        response = (AccountResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Account updation failed , Check if Khata service is UP");
        }
        return  response;
    }

    public static AccountResponse searchAccountByOwnerRefId (Long ownerRefId)throws  UnsupportedEncodingException , JAXBException{
        AccountResponse response = null;
        String searchQuery = "dir=DESC&ownerReferenceId="+ownerRefId;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.SEARCH_ACCOUNT+"?"+searchQuery,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (AccountResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Get Account failed  , Check if Khata service is UP");
        }
        return  response;
    }

    public static AccountAssociationResponse getAccountAssociationById (Long accountAssociationId)throws  UnsupportedEncodingException , JAXBException {
       AccountAssociationResponse response = null;
       String pathParam = accountAssociationId.toString();
       Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_ASSOCIATION_BY_ID,  new String[]{pathParam}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
       response = (AccountAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountAssociationResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Get Account failed , Check if Khata service is UP");
        }
       return  response;
    }

    public static AccountAssociationResponse updateAccountAssociationById (Long accountAssociationId ,Long associatedAccountId, Long parentAccountId)throws  UnsupportedEncodingException , JAXBException {
        AccountAssociationResponse response = null;
        AccountAssociationEntry accountAssociationEntry = new AccountAssociationEntry();
        accountAssociationEntry.setAssociateAccountId(associatedAccountId);
        accountAssociationEntry.setParentAccountId(parentAccountId);
        String payload = APIUtilities.convertXMLObjectToString(accountAssociationEntry);
        String pathParam = accountAssociationId.toString();
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_ASSOCIATION_BY_ID,  new String[]{pathParam}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        response = (AccountAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountAssociationResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail(" update AccountAssociation failed , Check if Khata service is UP");
        }
        return  response;
    }

    public static AccountAssociationResponse createAccountAssociation (Long associatedAccountId, Long parentAccountId)throws  UnsupportedEncodingException , JAXBException {
        AccountAssociationResponse response = null;
        AccountAssociationEntry accountAssociationEntry = new AccountAssociationEntry();
        accountAssociationEntry.setAssociateAccountId(associatedAccountId);
        accountAssociationEntry.setParentAccountId(parentAccountId);
        accountAssociationEntry.setCreatedBy("LMSAutomation");
        String payload = APIUtilities.convertXMLObjectToString(accountAssociationEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.CREATE_ACCOUNT_ASSOCIATION,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        response = (AccountAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountAssociationResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail(" create AccountAssociation failed, Check if Khata service is UP");
        }
        return  response;
    }

    public static AccountAssociationResponse getAccountAssociationbyparentAccount (Long parentAccount) throws UnsupportedEncodingException , JAXBException {
        AccountAssociationResponse response = null;
        String pathParam = parentAccount.toString();
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_ASSOCIATION_BY_PARENT_ID,  new String[]{pathParam}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (AccountAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountAssociationResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("getAccountAssociation failed, Check if Khata service is UP");
        }
        return  response;
    }

    public static AccountAssociationResponse searchAccountAssociation (String limit , Long parentAccount , String sortBy ) throws UnsupportedEncodingException , JAXBException {
        AccountAssociationResponse response = null;
        String searchQuery = "dir=DESC&limit="+limit+"&parentAccountId="+parentAccount+"&sortBy="+sortBy+"&start=0";
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.SEARCH_ACCOUNT_ASSOCIATION+"?"+searchQuery,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (AccountAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountAssociationResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("getAccountAssociation failed, Check if Khata service is UP");
        }
        return  response;

    }

    public static AccountAssociationResponse searchAccountAssociationByAssociatedAccountId ( Long associatedaccount  ) throws UnsupportedEncodingException , JAXBException {
        AccountAssociationResponse response = null;
        String searchQuery = "associateAccountId="+associatedaccount;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.SEARCH_ACCOUNT_ASSOCIATION+"?"+searchQuery,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (AccountAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountAssociationResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("getAccountAssociation failed , Check if Khata service is UP");
        }
        return  response;
    }
    
    public static AccountPendencyResponse getAccountPendencyById (Long accountPendencyId) throws UnsupportedEncodingException , JAXBException {
         AccountPendencyResponse response = null;
         String pathParam = accountPendencyId.toString();
         Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_PENDENCY_BY_ID,  new String[]{pathParam}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
         response = (AccountPendencyResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountPendencyResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("getAccountPendencyfailed , Check if Khata service is UP");
        }
        return response;
    }

    public static AccountPendencyResponse updateAccountPendencyById (Long accountPendencyId , Long accountId , Long to_accountId , Float cash , Float goods ) throws UnsupportedEncodingException , JAXBException {
        AccountPendencyResponse response = null;
        AccountPendencyEntry accountPendencyEntry = new AccountPendencyEntry();
        accountPendencyEntry.setId(accountPendencyId);
        accountPendencyEntry.setAccountId(accountId);
        accountPendencyEntry.setToAccountId(to_accountId);
        accountPendencyEntry.setCash(cash);
        accountPendencyEntry.setGoods(goods);
        String payload = APIUtilities.convertXMLObjectToString(accountPendencyEntry);
        String pathParam = accountPendencyId.toString();
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_PENDENCY_BY_ID,  new String[]{pathParam}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        response = (AccountPendencyResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountPendencyResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Account Pendency update failed , Check if Khata service is UP");
        }
        return response;
    }

    public static AccountPendencyResponse createAccountPendency( Long accountId , Long to_accountId , Float cash , Float goods ) throws UnsupportedEncodingException , JAXBException {
        AccountPendencyResponse response = null;
        AccountPendencyEntry accountPendencyEntry = new AccountPendencyEntry();
        accountPendencyEntry.setAccountId(accountId);
        accountPendencyEntry.setToAccountId(to_accountId);
        accountPendencyEntry.setCash(cash);
        accountPendencyEntry.setGoods(goods);
        String payload = APIUtilities.convertXMLObjectToString(accountPendencyEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.CREATE_ACCOUNT_PENDENCY,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        response = (AccountPendencyResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountPendencyResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Account Pendency creation failed , Check if Khata service is UP");
        }
        return response;
    }

    public static AccountPendencyResponse getAccountPendencyByaccount (Long accountId) throws UnsupportedEncodingException , JAXBException {
        AccountPendencyResponse response = null;
        String pathParam = accountId.toString();
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_PENDENCY_BY_ACCOUNT_ID,  new String[]{pathParam}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (AccountPendencyResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountPendencyResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("GetAccountPendency , Check if Khata service is UP");
        }
        return response;
    }

    public static AccountPendencyResponse getAccountPendencyByaccount (Long accountId,Long toaccountId) throws UnsupportedEncodingException , JAXBException {
        AccountPendencyResponse response = null;

        String pathParam = accountId.toString() + "?toAccountId="+ toaccountId;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_PENDENCY_BY_ACCOUNT_ID,  new String[]{pathParam}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (AccountPendencyResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountPendencyResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("GetAccountPendency By account id failed , Check if Khata service is UP");
        }
        return response;
    }

    public static AccountPendencyResponse getAccountPendencyByOwnerRefIdandType (Long ownerRefernceId , AccountType accountType ,Long toOwnerRefId , AccountType toAccountType) throws UnsupportedEncodingException , JAXBException  {
        AccountPendencyResponse response = null;
        String pathParam = accountType.toString();
        String queryParams = "?toOwnerRef="+toOwnerRefId+"&toAccountType="+toAccountType;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_ACCOUNT_PENDENCY_BY_OWNER_REF_ID_AND_TYPE +"/"+ ownerRefernceId +"/accountType/"+pathParam+"?toOwnerRef="+toOwnerRefId+"&toAccountType="+toAccountType, null, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (AccountPendencyResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new AccountPendencyResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("GetAccountPendency , Check if Khata service is UP");
        }
        return response;
    }

    public LedgerResponse createLedger(Long accountId, Long fromAccountId, Float amount, String eventId, String sourceId, LedgerType ledgerType) throws JAXBException , UnsupportedEncodingException {
        LedgerResponse ledgerResponse = new LedgerResponse();
        LedgerEntry ledgerEntry = new LedgerEntry();
        ledgerEntry.setAccountId(accountId);
        ledgerEntry.setFromAccountId(fromAccountId);
        ledgerEntry.setAmount(amount);
        ledgerEntry.setEventId(eventId);
        ledgerEntry.setSourceId(sourceId);
        ledgerEntry.setType(ledgerType);
        String payload = APIUtilities.convertXMLObjectToString(ledgerEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.CREATE,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        ledgerResponse = (LedgerResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new LedgerResponse());
        if (ledgerResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Ledger search didn't result , Check if Khata service is UP");
        }
        return  ledgerResponse;
    }


    public LedgerResponse getBulkLedger (String accountId, String sourceId,String type,String limit, String start)throws  UnsupportedEncodingException , JAXBException{
        LedgerResponse response = null;
        String searchquery = "/account/"+accountId+"/source/"+sourceId+"/type/"+type+"?limit="+limit+"&start="+start;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.LEDGER+searchquery,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (LedgerResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new LedgerResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("creating bulk ledger failed , Check if Khata service is UP");
        }

        return  response;
    }


    public LedgerResponse ledgerSearchByParam(String searchquery)throws  UnsupportedEncodingException , JAXBException{
        LedgerResponse response = null;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.LEDGER+"/"+searchquery,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (LedgerResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new LedgerResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Ledger search failed , Check if Khata service is UP or clean redis");
        }

        return  response;
    }


    public LedgerResponse getLedgerDetailsByLedgerId(String id)throws  UnsupportedEncodingException , JAXBException{
        LedgerResponse response = null;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.LEDGER+"/"+id,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        response = (LedgerResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new LedgerResponse());
        if (response.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Ledger details by ID , Check if Khata service is UP or clean redis");
        }

        return  response;
    }

    public  LedgerResponse ledgerUpdate(Long accountId, Float amount, String createdBy, Date createdOn, String eventId, Long fromAccountId, Long Id, Long paymentId, Date lastModifiedOn, String remark, String sourceId, LedgerType ledgerType) throws JAXBException , UnsupportedEncodingException{
        LedgerResponse ledgerResponse = new LedgerResponse();
        LedgerEntry ledgerEntry = new LedgerEntry();
        ledgerEntry.setAccountId(accountId);
        ledgerEntry.setAmount(amount);
        ledgerEntry.setCreatedBy(createdBy);
        ledgerEntry.setCreatedOn(createdOn);
        ledgerEntry.setEventId(eventId);
        ledgerEntry.setFromAccountId(fromAccountId);
        ledgerEntry.setId(Id);
        ledgerEntry.setPaymentId(paymentId);
        ledgerEntry.setLastModifiedOn(lastModifiedOn);
        ledgerEntry.setRemark(remark);
        ledgerEntry.setSourceId(sourceId);
        ledgerEntry.setType(ledgerType);

        String payload = APIUtilities.convertXMLObjectToString(ledgerEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.LEDGER+"/"+Id,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        ledgerResponse = (LedgerResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new LedgerResponse());
        if (ledgerResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Update Ledger failed , Check if DB is populated");
        }

        return  ledgerResponse;
    }

    public LedgerResponse  bulkCreateLedger(Long accountId, Long fromAccountId, Float amount, String eventId, String sourceId, LedgerType ledgerType, int totalLedgersToCreate) throws JAXBException , UnsupportedEncodingException {
        LedgerResponse ledgerResponse = new LedgerResponse();
        List<LedgerEntry> ledgerEntryList = new ArrayList<>();

        for (int i=0 ;i< totalLedgersToCreate ; i++)
        {
            LedgerEntry ledgerEntry = new LedgerEntry();
            ledgerEntry.setAccountId(accountId);
            ledgerEntry.setFromAccountId(fromAccountId);
            ledgerEntry.setAmount(amount);
            ledgerEntry.setEventId(eventId);
            ledgerEntry.setSourceId(sourceId);
            ledgerEntry.setType(ledgerType);

            ledgerEntryList.add(ledgerEntry);
        }
        String payload = APIUtilities.convertJavaObjectToJsonUsingGson(ledgerEntryList);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.BULK_CREATE,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        ledgerResponse = (LedgerResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new LedgerResponse());
        if (ledgerResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Bulk creation Ledger failed , Check if DB is populated");
        }

        return  ledgerResponse;
    }

    //payment
    //TODO: create payment
    public static PaymentResponse createPayment(Long accountId, Long fromAccountId, Float amount, PaymentType paymentType , String paymentReferenceId) throws JAXBException , UnsupportedEncodingException {
        PaymentResponse paymentResponse = new PaymentResponse();

        AccountBasicEntry accountBasicEntry = new AccountBasicEntry();
        AccountBasicEntry accountBasicEntry1 = new AccountBasicEntry();

        accountBasicEntry.setAccountId(accountId);
        PaymentEntry paymentEntry = new PaymentEntry();
        paymentEntry.setToAccount(accountBasicEntry);
        accountBasicEntry1.setAccountId(fromAccountId);
        paymentEntry.setFromAccount(accountBasicEntry1);
        paymentEntry.setAmount(amount);
        paymentEntry.setType(paymentType);
        paymentEntry.setPaymentReferenceId(paymentReferenceId);
        paymentEntry.setRemark("DC to CMS");
        String payload = APIUtilities.convertXMLObjectToString(paymentEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.PAYMENT_CREATE,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        paymentResponse = (PaymentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new PaymentResponse());
        if (paymentResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("payment creation Ledger failed , Check service khata");
        }
        return  paymentResponse;
    }

    public static PaymentResponse getPaymentById(String paymentId) throws JAXBException , UnsupportedEncodingException {
        PaymentResponse paymentResponse = new PaymentResponse();

        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.PAYMENT,  new String[]{paymentId}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        paymentResponse = (PaymentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new PaymentResponse());
        if (paymentResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("getting payment by Id , Check if DB is populated");
        }
        return  paymentResponse;
    }

    public static PaymentResponse paymentUpdate(Long accountId, Long fromAccountId, Float amount, PaymentType paymentType, Long PaymentId, PaymentStatus paymentStatus) throws JAXBException , UnsupportedEncodingException {
        PaymentResponse paymentResponse = new PaymentResponse();

        AccountBasicEntry accountBasicEntry = new AccountBasicEntry();
        AccountBasicEntry accountBasicEntry1 = new AccountBasicEntry();

        accountBasicEntry.setAccountId(accountId);


        PaymentEntry paymentEntry = new PaymentEntry();
        paymentEntry.setAmount(amount);

        paymentEntry.setFromAccount(accountBasicEntry);

        accountBasicEntry1.setAccountId(fromAccountId);


        paymentEntry.setToAccount(accountBasicEntry1);
        paymentEntry.setId(PaymentId);
        paymentEntry.setStatus(paymentStatus);
        paymentEntry.setType(paymentType);

        String payload = APIUtilities.convertXMLObjectToString(paymentEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.PAYMENT,  new String[]{String.valueOf(PaymentId)}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        paymentResponse = (PaymentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new PaymentResponse());
        if (paymentResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("payment creation Ledger failed , Check if DB is populated and khata service is up");
        }
        return  paymentResponse;
    }

    public static PaymentResponse getPaymentDetailsFromAccountToAccount(String fromAccount, String toAccount) throws JAXBException , UnsupportedEncodingException {
        PaymentResponse paymentResponse = new PaymentResponse();
        String param = "fromAccount/"+fromAccount+"/toAccount/"+toAccount;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.PAYMENT,  new String[]{param}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        paymentResponse = (PaymentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new PaymentResponse());
        if (paymentResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("payment creation Ledger failed , Check if payment entry exists from account and khata is up");
        }
        return  paymentResponse;
    }

    public PaymentResponse paymentSearch(String param) throws JAXBException , UnsupportedEncodingException {
        PaymentResponse paymentResponse = new PaymentResponse();
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.PAYMENT,  new String[]{param}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        paymentResponse = (PaymentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new PaymentResponse());
        if (paymentResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("payment search failed , Check if DB is populated and khata service is up");
        }
        return  paymentResponse;
    }

    public static PaymentResponse paymentSearchByPaymentReferenceId(String paymentReferenceId) throws JAXBException , UnsupportedEncodingException {
        PaymentResponse paymentResponse = new PaymentResponse();
        String param = "?paymentReferenceId="+paymentReferenceId;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.PAYMENT_SEARCH,  new String[]{param}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        paymentResponse = (PaymentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new PaymentResponse());
        if (paymentResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("payment search failed , Check if DB is populated and khata service is up");
        }
        return  paymentResponse;
    }


    public static HashMap<String, String> getTokenForFileUpload() {
        HashMap<String, String> Headers = new HashMap<String, String>();
        Headers.put("Authorization", "Basic dXNlcjE6dXNlcjFQYXNz,Basic Mzoz");
        Headers.put("Accept","application/json");
        return Headers;
    }

    public static void uploadMISReport(String filePath) throws UnsupportedEncodingException {
        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
       multipartEntityBuilder.addPart("file",new FileBody(new File(filePath)));
        multipartEntityBuilder.setContentType(ContentType.MULTIPART_FORM_DATA);
        Svc service = HttpExecutorService.executeHttpServiceForUpload(khata_constants.KHATA_PATH.MIS_UPLOAD,null,SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.POST,multipartEntityBuilder,getTokenForFileUpload());
    }


    public static void upload3pltoCMS(String filePath) throws UnsupportedEncodingException {
        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
        multipartEntityBuilder.addPart("file",new FileBody(new File(filePath)));
        multipartEntityBuilder.setContentType(ContentType.MULTIPART_FORM_DATA);
        Svc service = HttpExecutorService.executeHttpServiceForUpload(khata_constants.KHATA_PATH.COURIER_CMS_UPLOAD,null,SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.POST,multipartEntityBuilder,getTokenForFileUpload());
    }


    public static String generateMISReport(String DeliveryCenterName, String DepositeReferenceNo, String DepositedAmount) throws IOException {

        java.sql.Date currentTimestamp = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
        String uploadFileHeader = "Delivery Center/Courier Name,Deposit Slip No/Transaction Reference No,Deposit Amount";

        File file = new File("./Data/lms/MISUpload" + currentTimestamp + Math.random() + ".csv");
        if (file.exists()) {
            file.delete();
        }
        FileWriter writer = new FileWriter(file, true);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(uploadFileHeader);
        bufferedWriter.newLine();
        bufferedWriter.write(DeliveryCenterName + "," + DepositeReferenceNo + "," + DepositedAmount );
        bufferedWriter.newLine();
        bufferedWriter.close();
        log.info("MIS upload file (DC to CMS)- " + file.getName() + " is created in Folder :" + file.getPath());
        return file.getPath();

    }

    public static String generateMISReport3Pl(String courierCode, String trackingNumber, Float amountCollected) throws IOException {

        java.sql.Date currentTimestamp = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
        String uploadFileHeader = "Courier Code,Tracking Number,Transaction Reference No,Amount";

        File file = new File("./Data/lms/3plUpload" + currentTimestamp + Math.random() + ".csv");
        if (file.exists()) {
            file.delete();
        }
        FileWriter writer = new FileWriter(file, true);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(uploadFileHeader);
        bufferedWriter.newLine();
        bufferedWriter.write(courierCode + "," + trackingNumber + "," + amountCollected );
        bufferedWriter.newLine();
        bufferedWriter.close();
        log.info("CMS upload file (3PL to CMS)- " + file.getName() + " is created in Folder :" + file.getPath());
        return file.getPath();

    }

    //To pay for SDA
    public PaymentResponse createAndApprove(Long accountId, Long fromAccountId, Float amount, PaymentType paymentType) throws JAXBException , UnsupportedEncodingException {
        PaymentResponse paymentResponse = new PaymentResponse();

        AccountBasicEntry accountBasicEntry = new AccountBasicEntry();
        AccountBasicEntry accountBasicEntry1 = new AccountBasicEntry();

        accountBasicEntry.setAccountId(accountId);
        PaymentEntry paymentEntry = new PaymentEntry();
        paymentEntry.setToAccount(accountBasicEntry);
        accountBasicEntry1.setAccountId(fromAccountId);
        paymentEntry.setFromAccount(accountBasicEntry1);
        paymentEntry.setAmount(amount);
        paymentEntry.setType(paymentType);
        String payload = APIUtilities.convertXMLObjectToString(paymentEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.PAYMENT_CREATE_AND_APPROVE,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        paymentResponse = (PaymentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new PaymentResponse());
        if (paymentResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("payment creation Ledger failed , Check service khata");
        }
        return  paymentResponse;
    }

    public PaymentResponse createAndApproveForNegScenarios(String accountId, String fromAccountId, Float amount, PaymentType paymentType) throws JAXBException , UnsupportedEncodingException {
        PaymentResponse paymentResponse = new PaymentResponse();

        AccountBasicEntry accountBasicEntry = new AccountBasicEntry();
        AccountBasicEntry accountBasicEntry1 = new AccountBasicEntry();
        PaymentEntry paymentEntry = new PaymentEntry();

        if (accountId != "") {
            accountBasicEntry.setAccountId(Long.valueOf(accountId));
            paymentEntry.setToAccount(accountBasicEntry);

        }
        paymentEntry.setToAccount(accountBasicEntry);
        if ( fromAccountId != "") {
            accountBasicEntry1.setAccountId(Long.valueOf(fromAccountId));
            paymentEntry.setFromAccount(accountBasicEntry1);

        }
        paymentEntry.setAmount(amount);

        paymentEntry.setType(paymentType);
        String payload = APIUtilities.convertXMLObjectToString(paymentEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.PAYMENT_CREATE_AND_APPROVE,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        paymentResponse = (PaymentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new PaymentResponse());
        if (paymentResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("payment creation Ledger failed , Check service khata");
        }
        return  paymentResponse;
    }

    public ExcessResponse createExcess(Long accountId, Long fromAccountId, Float amount, PaymentType paymentType , Long paymentId, AccountType fromAccountType , String fromAccountOwnerRefId, AccountType AccountType , String AccountOwnerRefId) throws JAXBException , UnsupportedEncodingException {
        ExcessResponse excessResponse = new ExcessResponse();
        ExcessEntry excessEntry = new ExcessEntry();
        AccountBasicEntry accountBasicEntry = new AccountBasicEntry();
        accountBasicEntry.setAccountId(fromAccountId);
        accountBasicEntry.setType(fromAccountType);
        accountBasicEntry.setOwnerReferenceId(fromAccountOwnerRefId);
        excessEntry.setToAccount(accountBasicEntry);
        accountBasicEntry.setAccountId(accountId);
        accountBasicEntry.setType(AccountType);
        accountBasicEntry.setOwnerReferenceId(AccountOwnerRefId);
        excessEntry.setFromAccount(accountBasicEntry);
        excessEntry.setType(paymentType);
        excessEntry.setStatus(PaymentStatus.CREATED);
        excessEntry.setPaymentId(paymentId);
        String payload = APIUtilities.convertXMLObjectToString(excessEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.CREATE_EXCESS,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        excessResponse = (ExcessResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new ExcessResponse());
        if (excessResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Excess creation is failed , Check khata service is up");
        }
        return  excessResponse;
    }

    public ExcessResponse updateExcess(Long accountId, Long fromAccountId, Float amount, PaymentType paymentType , Long paymentId, AccountType fromAccountType , String fromAccountOwnerRefId, AccountType AccountType , String AccountOwnerRefId) throws JAXBException , UnsupportedEncodingException {
        ExcessResponse excessResponse = new ExcessResponse();
        ExcessEntry excessEntry = new ExcessEntry();
        AccountBasicEntry accountBasicEntry = new AccountBasicEntry();
        accountBasicEntry.setAccountId(fromAccountId);
        accountBasicEntry.setType(fromAccountType);
        accountBasicEntry.setOwnerReferenceId(fromAccountOwnerRefId);
        excessEntry.setToAccount(accountBasicEntry);
        accountBasicEntry.setAccountId(accountId);
        accountBasicEntry.setType(AccountType);
        accountBasicEntry.setOwnerReferenceId(AccountOwnerRefId);
        excessEntry.setFromAccount(accountBasicEntry);
        excessEntry.setType(paymentType);
        excessEntry.setStatus(PaymentStatus.PROCESSING);
        excessEntry.setPaymentId(paymentId);
        String payload = APIUtilities.convertXMLObjectToString(excessEntry);
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.UPDATE_EXCESS,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        excessResponse = (ExcessResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new ExcessResponse());
        if (excessResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Excess updation is  failed , Check khata service is up");
        }
        return  excessResponse;
    }

    public ExcessResponse pushExcess(Long fromAccountId, Long accountId ) throws JAXBException , UnsupportedEncodingException {
        String param = "from/"+fromAccountId+"/to/"+accountId;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.PUSH_EXCESS,  new String[]{param}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.PUT, null, Headers.getLmsHeaderXML());
        ExcessResponse excessResponse = (ExcessResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new ExcessResponse());
        if (excessResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Push Excess is  failed , Check khata service is up");
        }
        return  excessResponse;
    }

    public ExcessResponse getExcessById(Long Id) throws JAXBException , UnsupportedEncodingException {
        String param = ""+Id;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.GET_EXCESS,  new String[]{param}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        ExcessResponse excessResponse = (ExcessResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new ExcessResponse());
        if (excessResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Get Excess by ID is failing , Check khata service is up");
        }
        return  excessResponse;
    }

    public ExcessResponse searchExcess(Long Id) throws JAXBException , UnsupportedEncodingException {
        String param = ""+Id;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.SEARCH_EXCESS,  new String[]{param}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        ExcessResponse excessResponse = (ExcessResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new ExcessResponse());
        if (excessResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Search API is failing , Check khata service is up");
        }
        return  excessResponse;
    }

    public static ExcessResponse searchExcessByPaymentId(Long paymentId) throws JAXBException , UnsupportedEncodingException {
        String param = "?paymentId="+paymentId;
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.SEARCH_EXCESS,  new String[]{param}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        ExcessResponse excessResponse = (ExcessResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new ExcessResponse());
        if (excessResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Search API by payment Id is failing , Check khata service is up");
        }
        return  excessResponse;
    }

    public static ExcessResponse searchExcessByfromtoaccount(Long fromAccountId , Long toAccountId) throws JAXBException , UnsupportedEncodingException {
        String param = "?fromAccountId="+fromAccountId+"&toAccountId="+toAccountId+"&dir=DESC&limit=1&sortBy=createdOn";
        Svc service = HttpExecutorService.executeHttpService(khata_constants.KHATA_PATH.SEARCH_EXCESS,  new String[]{param}, SERVICE_TYPE.KHATA_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        ExcessResponse excessResponse = (ExcessResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new ExcessResponse());
        if (excessResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Search API by payment Id is failing , Check khata service is up");
        }
        return  excessResponse;
    }

}
