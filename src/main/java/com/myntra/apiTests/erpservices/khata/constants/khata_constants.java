package com.myntra.apiTests.erpservices.khata.constants;

public class khata_constants {
    public static class KHATA_PATH {
        public static final String CREATE_ACCOUNT = "account/create";
        public static final String GET_ACCOUNT_BY_ID = "account";
        public static final String GET_ACCOUNT_BY_TYPE = "account/type";
        public static final String GET_ACCOUNT_BY_OWNER_REFERNCE_ID = "account/ownerReference";
        public static final String UPDATE_ACCOUNT = "account/update";
        public static final String SEARCH_ACCOUNT = "account/search";
        public static final String GET_ACCOUNT_ASSOCIATION_BY_ID = "accountAssociation";
        public static final String CREATE_ACCOUNT_ASSOCIATION = "accountAssociation/create";
        public static final String GET_ACCOUNT_ASSOCIATION_BY_PARENT_ID = "accountAssociation/parentAccount";
        public static final String SEARCH_ACCOUNT_ASSOCIATION = "accountAssociation/search";
        public static final String CREATE_ACCOUNT_PENDENCY = "accountPendency/create";
        public static final String GET_ACCOUNT_PENDENCY_BY_ID = "accountPendency";
        public static final String GET_ACCOUNT_PENDENCY_BY_ACCOUNT_ID = "accountPendency/accountId";
        public static final String GET_ACCOUNT_PENDENCY_BY_OWNER_REF_ID_AND_TYPE = "accountPendency/ownerRef";

        public static final String CREATE = "ledger/create";
        public static final String BULK_CREATE = "ledger/bulkCreate";
        public static final String LEDGER = "ledger";
        public static final String PAYMENT_CREATE = "payment/create";
        public static final String PAYMENT = "payment";
        public static final String PAYMENT_SEARCH = "payment/search";
        public static final String MIS_UPLOAD = "payment/uploadMISReport";
        public static final String COURIER_CMS_UPLOAD = "payment/uploadMISReport";
        public static final String PAYMENT_CREATE_AND_APPROVE = "payment/createAndApprove";
        public static final String CREATE_EXCESS = "excess/create";
        public static final String UPDATE_EXCESS = "excess/update";
        public static final String GET_EXCESS = "excess";
        public static final String SEARCH_EXCESS = "excess/search";
        public static final String PUSH_EXCESS = "excess/push";



    }
}
