package com.myntra.apiTests.erpservices.khata.validator;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.khata.response.LedgerResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.testng.Assert;
import org.testng.log4testng.Logger;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public class LedgerValidator {
    static Logger log = Logger.getLogger(LedgerValidator.class);
    KhataServiceHelper khataServiceHelper = new KhataServiceHelper();

    public void validateLedger(String from_account_id, String account_id, String amount, String type, String source_id) throws UnsupportedEncodingException, JAXBException {
        String searchquery = "search?accountId="+account_id+"&fromAccount="+from_account_id+"&sourceId=" + source_id + "&type="+ type+"&sortBy=id";//"&dir=ASC"
        LedgerResponse ledgerResponse = khataServiceHelper.ledgerSearchByParam(searchquery);
        Assert.assertEquals(ledgerResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "No ledger data found for query: "+ searchquery);
        Assert.assertEquals(ledgerResponse.getLedgers().get(0).getAccountId().toString(), account_id, "In ledger Account id mismatch for " +searchquery);
        Assert.assertEquals(ledgerResponse.getLedgers().get(0).getFromAccountId().toString(), from_account_id, "In ledger From Account Id  for " +searchquery);
        Assert.assertEquals(ledgerResponse.getLedgers().get(0).getAmount().toString(), amount, "In ledger Amount mismatch for " +searchquery);
        Assert.assertEquals(ledgerResponse.getLedgers().get(0).getType().toString(), type, "In ledger Type mismatch for " +searchquery);
        Assert.assertEquals(ledgerResponse.getLedgers().get(0).getSourceId().toString(), source_id,"In ledger Source Id mismatch for " +searchquery);
    }


    public static void validateLedgerSettlement(String fromAccountId, String AccountId, String type, Float expectedAmount) {
        Map<String, Object> shipmentPendency = null;
        try {

            shipmentPendency = DBUtilities.exSelectQueryForSingleRecord("select sum(amount) from ledger where `account_id` = " + AccountId + " and `from_account_id` = " + fromAccountId + " and `type` = '" + type + "'", "myntra_lms_khata");
            if (shipmentPendency == null)
                throw new NullPointerException();

        } catch (Exception e) {
            System.out.println("No Ledger entry there from " + fromAccountId + " to " +AccountId);
        }
        Assert.assertEquals(shipmentPendency.get("sum(amount)").toString(),expectedAmount , "Ledger Amount is changing for the Payment from " + fromAccountId + "and account ID" + AccountId + "for type" + type);
    }

}
