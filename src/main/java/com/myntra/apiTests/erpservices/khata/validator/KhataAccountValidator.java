package com.myntra.apiTests.erpservices.khata.validator;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.response.AccountAssociationResponse;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;

import org.testng.Assert;
import org.testng.log4testng.Logger;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;

public class KhataAccountValidator {
    static Logger log = Logger.getLogger(KhataAccountValidator.class);
    KhataServiceHelper khataServiceHelper = new KhataServiceHelper();

    public static void validateAccountCreation(Long ownerRefId, String firstName, String lastNAme, String mobileNumber, Long parentAccountId, AccountType accountType, Boolean isActive) throws UnsupportedEncodingException, JAXBException {
        AccountResponse accountResponse = KhataServiceHelper.searchAccountByOwnerRefId(ownerRefId);
        Assert.assertEquals(accountResponse.getAccounts().get(0).getMobile(),mobileNumber, "Mobile number not matching");
        Assert.assertEquals(accountResponse.getAccounts().get(0).getIsActive(),isActive , "Unmatch of isActive");
        Assert.assertEquals(accountResponse.getAccounts().get(0).getOwnerReferenceId(),ownerRefId.toString(), "Unmatch of owner reference Is");
        Assert.assertEquals(accountResponse.getAccounts().get(0).getParentAccountId(),parentAccountId , "Unmatch of Parent Account Id");
        Assert.assertEquals(accountResponse.getAccounts().get(0).getType(),accountType , "Unmatch of Account Type");
        Assert.assertEquals(accountResponse.getAccounts().get(0).getFirstName() ,firstName , "UnMatch of firstname");
        Assert.assertEquals(accountResponse.getAccounts().get(0).getLastName() ,lastNAme , "UnMatch of lastNAme");
    }

    public static void validateAccountAssociationCreation(Long associateAccount, Long parentAccountId) throws UnsupportedEncodingException, JAXBException {
        AccountAssociationResponse accountAssociationResponse = KhataServiceHelper.searchAccountAssociationByAssociatedAccountId(associateAccount);
        Assert.assertEquals(accountAssociationResponse.getAccountAssociation().get(0).getParentAccountId(),parentAccountId, "Unmatch of parent account Id");
        Assert.assertEquals(accountAssociationResponse.getAccountAssociation().get(0).getAssociateAccountId(),associateAccount, "Unmatch of associate account Id");
    }

    public void validateAccountPendency(String accountId, String toAccountId, String cash, String goods) throws UnsupportedEncodingException, JAXBException {
        AccountPendencyResponse accountPendencyResponse = khataServiceHelper.getAccountPendencyByaccount(Long.valueOf(accountId), Long.valueOf(toAccountId));
        Assert.assertEquals(accountPendencyResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Account pendency not found");
        Assert.assertEquals(accountPendencyResponse.getAccountPendency().get(0).getAccountId().toString(), accountId, "In Account Pendency Wrong Account Id");
        Assert.assertEquals(accountPendencyResponse.getAccountPendency().get(0).getToAccountId().toString(), toAccountId, "In Account Pendency Wrong To account Id");
        Assert.assertEquals(accountPendencyResponse.getAccountPendency().get(0).getCash().toString(), cash, "In Account Pendency Cash amount mismatch");
        Assert.assertEquals(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString(), goods, "In Account Pendency Goods amount mismatch");
    }

}
