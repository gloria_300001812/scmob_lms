package com.myntra.apiTests.erpservices.khata.validator;

import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.PaymentResponse;
import org.testng.Assert;
import org.testng.log4testng.Logger;
import com.myntra.khata.response.PaymentResponse;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;

public class PaymentValidator {
    KhataServiceHelper khataServiceHelper = new KhataServiceHelper();

    public void paymentValidation(String from_account, String to_account, String cash, String status) throws JAXBException, UnsupportedEncodingException {

        PaymentResponse paymentResponse =  khataServiceHelper.getPaymentDetailsFromAccountToAccount(from_account,to_account);
        int size = paymentResponse.getPayments().size()-1;
        //will be taking the latest payment entry for vaidation
        String actual_status = paymentResponse.getPayments().get(size).getStatus().toString();
        String actual_cash = paymentResponse.getPayments().get(size).getAmount().toString();
        String actual_from_account_id = paymentResponse.getPayments().get(size).getFromAccount().getAccountId().toString();
        String  actual_to_account_id = paymentResponse.getPayments().get(size).getToAccount().getAccountId().toString();

        Assert.assertEquals(actual_from_account_id, from_account,"For Payment validation , from "+ from_account + "and to account" +to_account +"  Account mismatch");
        Assert.assertEquals(actual_to_account_id, to_account, "For Payment validation , from "+ from_account + "and to account" +to_account + "to_account_id mismatch");
        Assert.assertEquals(actual_cash, cash, "For Payment validation , from "+ from_account + "and to account" +to_account + "Amount mismatch");
        Assert.assertEquals(actual_status, status, "For Payment validation , from "+ from_account + "and to account" +to_account +"Status mismatch");
    }


    public static void validatePaymentCreation(Long paymentId ,Long fromaccountId, Long toaccountId, String Status, PaymentType paymentType , String paymentRefernceId, float amount, Boolean isAcknowledged ) throws UnsupportedEncodingException, JAXBException {
        PaymentResponse paymentResponse = KhataServiceHelper.getPaymentById(paymentId.toString());
        Assert.assertEquals(paymentResponse.getPayments().get(0).getFromAccount().getAccountId(),fromaccountId);
        Assert.assertEquals(paymentResponse.getPayments().get(0).getToAccount().getAccountId(),toaccountId);
        Assert.assertEquals(paymentResponse.getPayments().get(0).getAmount(),amount, "For" + paymentId + ", Amount mismatch");
        //Assert.assertEquals(paymentResponse.getPayments().get(0).getIsAcknowledged(),isAcknowledged , "For" + paymentId + ", isAcknowledged mismatch");
        Assert.assertEquals(paymentResponse.getPayments().get(0).getPaymentReferenceId(),paymentRefernceId, "For" + paymentId + ", paymentRefernceId mismatch");
        Assert.assertEquals(paymentResponse.getPayments().get(0).getStatus().toString(),Status , "For" + paymentId + ", Payment Status mismatch");
        Assert.assertEquals(paymentResponse.getPayments().get(0).getType(),paymentType , "For" + paymentId + ", paymentType mismatch");
    }

    public static void validatePaymentCreationByMIS(Long paymentId ,Long fromaccountId, Long toaccountId, String Status, PaymentType paymentType , String paymentRefernceId, float amount, Boolean isAcknowledged ) throws UnsupportedEncodingException, JAXBException {
        PaymentResponse paymentResponse = KhataServiceHelper.getPaymentById(paymentId.toString());
        Assert.assertEquals(paymentResponse.getPayments().get(0).getFromAccount().getAccountId(),fromaccountId);
        Assert.assertEquals(paymentResponse.getPayments().get(0).getToAccount().getAccountId(),toaccountId);
        Assert.assertEquals(paymentResponse.getPayments().get(0).getApprovedAmount(),amount, "For" + paymentId + ", Amount mismatch");
        //Assert.assertEquals(paymentResponse.getPayments().get(0).getIsAcknowledged(),isAcknowledged , "For" + paymentId + ", isAcknowledged mismatch");
        Assert.assertEquals(paymentResponse.getPayments().get(0).getPaymentReferenceId(),paymentRefernceId, "For" + paymentId + ", paymentRefernceId mismatch");
        Assert.assertEquals(paymentResponse.getPayments().get(0).getStatus().toString(),Status , "For" + paymentId + ", Payment Status mismatch");
        Assert.assertEquals(paymentResponse.getPayments().get(0).getType(),paymentType , "For" + paymentId + ", paymentType mismatch");
    }

}
