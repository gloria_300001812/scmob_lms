package com.myntra.apiTests.erpservices.LMSCancellation.validators;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.erpservices.lastmile.service.LMSOrderDetailsClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.OrderTrackingClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.client.MLShipmentClient;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.OrderTrackingClient;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lms.client.domain.response.PickupResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.OrderTrackingDetailEntryV2;
import com.myntra.lms.client.response.OrderTrackingResponseV2;
import com.myntra.lms.client.status.OrderStatus;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponseCode;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.oms.client.response.PacketResponse;
import lombok.SneakyThrows;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class OrderValidator {
    String env = getEnvironment();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    MLShipmentClient_QA mlShipmentClient_qa = new MLShipmentClient_QA();
    LMSOrderDetailsClient lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private OrderTrackingClient orderTrackingClient = new OrderTrackingClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    OrderTrackingClient_QA orderTrackingClient_qa=new OrderTrackingClient_QA();

    public void validateShipmentOrderStatus(String orderId, ShipmentStatus expectedStatus) throws IOException, JAXBException {
        OrderResponse orderResponse = lmsServiceHelper.getOrderByOrderId(orderId);
        Assert.assertEquals(orderResponse.getOrders().get(0).getPlatformShipmentStatus().toString(), expectedStatus.toString(), "Invalid shipment status in Order_to_Ship");
        switch (orderResponse.getOrders().get(0).getPlatformShipmentStatus()) {
            case PACKED:
                Assert.assertEquals(orderResponse.getOrders().get(0).getStatus().toString(), OrderStatus.PK.toString(), "Mismatch in shipment status & Order status in Order_to_Ship");
                break;
            case INSCANNED:
                Assert.assertEquals(orderResponse.getOrders().get(0).getStatus().toString(), OrderStatus.IS.toString(), "Mismatch in shipment status & Order status in Order_to_Ship");
                break;
            case ADDED_TO_MB:
                Assert.assertEquals(orderResponse.getOrders().get(0).getStatus().toString(), OrderStatus.IS.toString(), "Mismatch in shipment status & Order status in Order_to_Ship");
                break;
            case SHIPPED:
                Assert.assertEquals(orderResponse.getOrders().get(0).getStatus().toString(), OrderStatus.SH.toString(), "Mismatch in shipment status & Order status in Order_to_Ship");
                break;
            case OUT_FOR_DELIVERY:
                Assert.assertEquals(orderResponse.getOrders().get(0).getStatus().toString(), OrderStatus.SH.toString(), "Mismatch in shipment status & Order status in Order_to_Ship");
                break;
            case CANCELLED:
                Assert.assertEquals(orderResponse.getOrders().get(0).getStatus().toString(), OrderStatus.CANCELLED.toString(), "Mismatch in shipment status & Order status in Order_to_Ship");
                break;
            case DELIVERED:
                Assert.assertEquals(orderResponse.getOrders().get(0).getStatus().toString(), OrderStatus.DL.toString(), "Mismatch in shipment status & Order status in Order_to_Ship");
                break;
            case FAILED_DELIVERY:
                Assert.assertEquals(orderResponse.getOrders().get(0).getStatus().toString(), OrderStatus.FD.toString(), "Mismatch in shipment status & Order status in Order_to_Ship");
                break;
            case CANCELLED_LOST:
                Assert.assertEquals(orderResponse.getOrders().get(0).getStatus().toString(), OrderStatus.CANCELLED_LOST.toString(), "Mismatch in shipment status & Order status in Order_to_Ship");
                break;
            case RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER:
                Assert.assertEquals(orderResponse.getOrders().get(0).getStatus().toString(), OrderStatus.SH.toString(), "Mismatch in shipment status & Order status in Order_to_Ship");
                break;
            default:
                Assert.fail("Invalid shipment status or update the changes as per receent changes. " + orderResponse.getOrders().get(0).getPlatformShipmentStatus());
                break;
        }
    }

    public void validateShipmentOrderStatus(List<String> orderIds, ShipmentStatus expectedStatus) throws IOException, JAXBException {
        for (String orderId : orderIds) {
            validateShipmentOrderStatus(orderId, expectedStatus);
        }
    }

    public void validateShipmentOrderDestinationHub(String orderId, String expectedDestinationHub) throws IOException, JAXBException {
        OrderResponse orderResponse = lmsServiceHelper.getOrderByOrderId(orderId);
        Assert.assertEquals(orderResponse.getOrders().get(0).getDeliveryCenterCode(), expectedDestinationHub, "Invalid Destination Hub");
    }

    public void validateOrderTrackingDetail(String trackingNumber, ShipmentStatus shipmentStatus, ShipmentUpdateEvent shipmentUpdateEvent, String courierCode,OrderTrackingDetailLevel level,String tenantId, String clientId) throws InterruptedException {
        Thread.sleep(15000);
        Boolean isValidEntries = false;
        OrderTrackingResponseV2 orderTrackingResponseV2 = orderTrackingClient_qa.getOrderTrackingDetailV2(trackingNumber,courierCode,level,tenantId,clientId);
        Assert.assertEquals(orderTrackingResponseV2.getOrderTrackingEntry().getShipmentStatus(), shipmentStatus);
        StringBuilder errorMessage = new StringBuilder();
        List<OrderTrackingDetailEntryV2> orderTrackingDetailEntryV2List = orderTrackingResponseV2.getOrderTrackingEntry().getOrderTrackingDetails();
        //since it is list and order of order tacking detail is not in consistent state, we need to check all the entries
        for (OrderTrackingDetailEntryV2 orderTrackingDetailEntryV2 : orderTrackingDetailEntryV2List) {
            if (!(orderTrackingDetailEntryV2.getActivityResult().equalsIgnoreCase(ShipmentUpdateResponseCode.TRANSITION_NOT_CONFIGURED.toString()) ||
                    orderTrackingDetailEntryV2.getActivityResult().equalsIgnoreCase(ShipmentUpdateResponseCode.ERROR_DURING_TRANSITION.toString()) ||
                    orderTrackingDetailEntryV2.getActivityResult().equalsIgnoreCase(ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString()))
                    && orderTrackingDetailEntryV2.getActivityType().equalsIgnoreCase(ShipmentUpdateEvent.CANCEL.toString())) {
                //TO status will be null in TRANSITION_NOT_CONFIGURED, ERROR_DURING_TRANSITION, TRANSITION_NOT_ALLOWED cases. No Transition as it is Error.
                if (orderTrackingDetailEntryV2.getToStatus().equalsIgnoreCase(shipmentStatus.name()) &&
                        orderTrackingDetailEntryV2.getActivityResult().equalsIgnoreCase("SUCCESS")) {
                    isValidEntries = true;
                }
            }
            if (orderTrackingDetailEntryV2.getActivityType().equalsIgnoreCase(shipmentUpdateEvent.name())) {
                isValidEntries = true;
                break;
            } else
                isValidEntries = false;
        }
        Assert.assertTrue(isValidEntries, "Invalid order entry found in order tracking detail");
    }

    public void validateOrderTrackingDetail(List<String> trackingNumbers, ShipmentStatus shipmentStatus, ShipmentUpdateEvent shipmentUpdateEvent, String courierCode,OrderTrackingDetailLevel level,String tenantId,String clientId) throws InterruptedException {
        for (String trackingNumber : trackingNumbers) {
            validateOrderTrackingDetail(trackingNumber, shipmentStatus, shipmentUpdateEvent, courierCode,level,tenantId,clientId);
        }
    }

    public void validatePacketStatus(String packetId, String expectedStatus) throws Exception {
        /* // Enable on integration
        String ownerId = "2297";
        String buyerId = "2297";
        PacketResponse packetResponse = omsServiceHelper.getPacket(packetId, ownerId, buyerId);
        Assert.assertEquals(packetResponse.getData().get(0).getOrderLines().get(0).getStatus(), expectedStatus);
        if (expectedStatus.equalsIgnoreCase("IC")) {
            Assert.assertEquals(packetResponse.getData().get(0).getStatusDisplayName(), "Cancelled");
            Assert.assertEquals(packetResponse.getStatus(), "F");
        }else if(expectedStatus.equalsIgnoreCase("S")){
            Assert.assertEquals(packetResponse.getData().get(0).getStatusDisplayName(), "Shipped");
            Assert.assertEquals(packetResponse.getStatus(), "S");
        }
        Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(packetId, EnumSCM.F, 5));
        */
    }

    public void validateMLShipmentStatus(String trackingNumber, String expectedShipmentStatus) {
        MLShipmentResponse mlShipmentResponse = mlShipmentClient_qa.getMLShipmentDetailsByTrackingNumber(trackingNumber);
        Assert.assertEquals(mlShipmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), expectedShipmentStatus, "Unexpected shipment status");
    }

    public void validateMLShipmentStatus(List<String> trackingNumbers, String expectedStatus) {
        for (String trackingNumber : trackingNumbers) {
            validateMLShipmentStatus(trackingNumber, expectedStatus);
        }
    }

    /**
     * CANCELLED at DC = RTO_CONFIRMED
     */
    public void validateMLShipmentStatusOnCancel(String trackingNumber, ReleaseStatus releaseStatus) {
        MLShipmentResponse mlShipmentResponse = mlShipmentClient_qa.getMLShipmentDetailsByTrackingNumber(trackingNumber);
        Assert.assertEquals(mlShipmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        if (mlShipmentResponse.getMlShipmentEntries() == null || mlShipmentResponse.getMlShipmentEntries().isEmpty())
            Assert.fail("No shipment entries available in the response");
        switch (releaseStatus) {
            case SH:
                Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), ShipmentStatus.RTO_CONFIRMED.toString(), "Unexpected shipment status");
                break;
            case DL:
                Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), ShipmentStatus.DELIVERED.toString(), "Unexpected shipment status");
                break;
            case OFD:
                Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), ShipmentStatus.OUT_FOR_DELIVERY.toString(), "Unexpected shipment status");
                break;
            case CANCELLED_LOST:
                Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), ShipmentStatus.CANCELLED_LOST.toString(), "Unexpected shipment status");
                break;
            default:
                Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), ShipmentStatus.CANCELLED.toString(), "Unexpected shipment status");
                break;
        }
    }

    /**
     * Cancelation is not possible on OFD, DL state
     */
    public void validateCancelShipmentResponseML(String cancelShipmentResponse, ReleaseStatus releaseStatus) {
        String updateStatus = APIUtilities.getElement(cancelShipmentResponse, "shipmentUpdateResponseCode", "json");
        if (releaseStatus == ReleaseStatus.OFD || releaseStatus == ReleaseStatus.DL)
            Assert.assertEquals(updateStatus, ShipmentUpdateResponseCode.TRANSITION_NOT_CONFIGURED.toString());
        else
            Assert.assertEquals(updateStatus, EnumSCM.SUCCESS);
    }

    public void validateCancelShipmentResponse(String cancelShipmentResponse, ShipmentUpdateResponseCode expectedShipmentUpdateResponseCode) {
        String updateStatus = APIUtilities.getElement(cancelShipmentResponse, "shipmentUpdateResponseCode", "json");
        Assert.assertEquals(updateStatus, expectedShipmentUpdateResponseCode.toString());
    }

    /**
     * Cancelation is not possible After Add_to_MB
     */
    public void validateCancelShipmentResponse3PL(String cancelShipmentResponse, ReleaseStatus releaseStatus) {
        String updateStatus = APIUtilities.getElement(cancelShipmentResponse, "shipmentUpdateResponseCode", "json");
        boolean expectedStatus = false;
        if (releaseStatus == ReleaseStatus.ADDED_TO_MB || releaseStatus == ReleaseStatus.SHIPPED ||
                releaseStatus == ReleaseStatus.OFD || releaseStatus == ReleaseStatus.DL) {
            if(updateStatus.equalsIgnoreCase(ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString()) ||
                    updateStatus.equalsIgnoreCase(ShipmentUpdateResponseCode.TRANSITION_NOT_CONFIGURED.toString()))
                expectedStatus = true;
            Assert.assertTrue(expectedStatus, "Invalid cancel status "+updateStatus);
        }
        else
            Assert.assertEquals(updateStatus, EnumSCM.SUCCESS);
    }

    public void validateCancelShipmentResponse(String cancelShipmentResponse, ShipmentUpdateResponseCode expectedShipmentUpdateResponseCode, String errorMessage) {
        String updateStatus = APIUtilities.getElement(cancelShipmentResponse, "shipmentUpdateResponseCode", "json");
        Assert.assertEquals(updateStatus, expectedShipmentUpdateResponseCode.toString(), errorMessage);
    }

    public void validateIsCancelableFlagSetforOrder(String packetId, Boolean isCancelable) {
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        System.out.println(lmsOrderDetails);
        Assert.assertEquals(lmsOrderDetails.getOrders().get(0).getIsCancellable(), isCancelable, "isCancelable Flag is set to " + lmsOrderDetails.getOrders().get(0).getIsCancellable());
    }

    public void validateIsCancelableFlagSetforOrder(List<String> packetIds, Boolean isCancelable) {
        packetIds.forEach((packetId) -> {
            validateIsCancelableFlagSetforOrder(packetId, isCancelable);
        });
    }

    public void validateOrderIsCancellable(String packetId, ReleaseStatus releaseStatus) {
        if (releaseStatus == ReleaseStatus.DL || releaseStatus == ReleaseStatus.OFD)
            validateIsCancelableFlagSetforOrder(packetId, false);
        else
            validateIsCancelableFlagSetforOrder(packetId, true);
    }

    public void validateExcahngeOrdersOnCancelation(String trackingNumber) throws Exception {
        //Exchange associated return should be in REJECTED Status on cancelation - (order_to_ship.exchange_pickup_id) pickup_shipment table
        String mlShipmentResponse = MLShipmentClient.GetMLShipmentDetailsByTrackingNumber(trackingNumber);
        String exchangeAssociatedTrackingNumber = APIUtilities.getElement(mlShipmentResponse, "mlShipmentResponse.data.pickupTrackingNumber", "json");
        PickupResponse pickupResponse = lmsServiceHelper.findPickupByTrackingNumber(exchangeAssociatedTrackingNumber);
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus(), ShipmentStatus.PICKUP_REJECTED, "Exchange Pickup order is in Wrong status after Cancelation");
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentType(), ShipmentType.PU, "Exchange Pickup order is in Wrong status after Cancelation");
    }

    public void validateCancelledShipmentOrderInscan(String orderInScanResponse, ShipmentStatus expectedShipmentStatus, String currentHub, String expectedDestinationHubCode) throws IOException {
        String shipmentStatus = APIUtilities.getElement(orderInScanResponse, "shipment.shipmentStatus", "json");
        String destinationHubCode = APIUtilities.getElement(orderInScanResponse, "shipment.destinationHubCode", "json");
        String dispatchHubCode = APIUtilities.getElement(orderInScanResponse, "shipment.dispatchHubCode", "json");
        String rtoHubCode =  APIUtilities.getElement(orderInScanResponse, "shipment.rtoHubCode", "json");
        Assert.assertEquals(shipmentStatus, expectedShipmentStatus.toString(), "Invalid ShipmentStatus");
        Assert.assertEquals(destinationHubCode, expectedDestinationHubCode, "Invalid ShipmentStatus");
        Assert.assertEquals(dispatchHubCode, currentHub, "Invalid ShipmentStatus");
        Assert.assertEquals(rtoHubCode, expectedDestinationHubCode, "Invalid ShipmentStatus");
    }

    public void validateNextHubInOrderAdditionalInfo(String trackingNumber, String expecedNextHub){
        //No API for this
        String query = "SELECT next_expected_hub \n" +
                "FROM order_to_ship O2S \n" +
                "INNER JOIN order_additional_info info \n" +
                "ON O2S.order_additional_info_id = info.id \n" +
                "WHERE tracking_number = '%s'" +
                "LIMIT 1;";
        query = String.format(query, trackingNumber);
        Map<String, Object> resultSet = DBUtilities.exSelectQueryForSingleRecord(query, "myntra_lms");
        Assert.assertEquals(resultSet.get("next_expected_hub").toString(), expecedNextHub, "Invalid next hub in Order Additional Info Table");
    }

    @SneakyThrows
    public void validatePacketStatusInOMS(String packetId, String expectedStatus){
        // Enable on integration
        String ownerId = "2297";
        String buyerId = "2297";
        Thread.sleep(5000);
        PacketResponse packetResponse = omsServiceHelper.getPacket(packetId, ownerId, buyerId);
        Assert.assertEquals(packetResponse.getData().get(0).getOrderLines().get(0).getStatus(), expectedStatus);
        if (expectedStatus.equalsIgnoreCase("IC")) {
            Assert.assertEquals(packetResponse.getData().get(0).getStatusDisplayName(), "Cancelled");
            Assert.assertEquals(packetResponse.getData().get(0).getStatus().toString(), "F");
            Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(packetId, EnumSCM.F, 5));
        }else if(expectedStatus.equalsIgnoreCase("S")){
            Assert.assertEquals(packetResponse.getData().get(0).getStatusDisplayName().toString(), "Shipped");
            Assert.assertEquals(packetResponse.getStatus(), "S");
        }else if(expectedStatus.equalsIgnoreCase("D")){
            Assert.assertEquals(packetResponse.getData().get(0).getStatusDisplayName(), "Delivered");
            Assert.assertEquals(packetResponse.getData().get(0).getStatus().toString(), "D");
        }
    }

    public void validateOrderIsCancellable(String packetId, ReleaseStatus releaseStatus,String tenantId,String clientId) {
        if (releaseStatus == ReleaseStatus.DL || releaseStatus == ReleaseStatus.OFD|| releaseStatus == ReleaseStatus.ASSIGNED_TO_SDA) {
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, tenantId, clientId);
            System.out.println(lmsOrderDetails);
            Assert.assertEquals(lmsOrderDetails.getOrders().get(0).getIsCancellable().toString(), "false", "isCancelable Flag is set to " + lmsOrderDetails.getOrders().get(0).getIsCancellable());
        } else {
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, tenantId, clientId);
            System.out.println(lmsOrderDetails);
            Assert.assertEquals(lmsOrderDetails.getOrders().get(0).getIsCancellable().toString(), "true", "isCancelable Flag is set to " + lmsOrderDetails.getOrders().get(0).getIsCancellable());
        }
    }

    public void validateMLShipmentStatus(String trackingNumber, String expectedShipmentStatus,String tenantId) {
        MLShipmentResponse mlShipmentResponse = mlShipmentClient_qa.getMLShipmentDetailsByTrackingNumber(trackingNumber);
        Assert.assertEquals(mlShipmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), expectedShipmentStatus, "Unexpected shipment status");
    }
}
