package com.myntra.apiTests.erpservices.LMSCancellation;

import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.sortation.domain.SortLocationType;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.Iterator;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class CancelationHelper {
    static String env = getEnvironment();
    static LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    static SortationHelper sortationHelper = new SortationHelper();
    MasterBagValidator masterBagValidator = new MasterBagValidator();

    public void enableCancelationOnHub(String hubCode) throws Exception {
        HubResponse hubResponse = lmsServiceHelper.searchHubByCode(hubCode);
        Boolean isCancellationAllowed = hubResponse.getHub().get(0).getAllowsCancellation();
        if(!isCancellationAllowed){
            System.out.println("isCancellationAllowed:"+isCancellationAllowed);
            HubEntry hubEntry = new HubEntry();
            hubEntry.setAllowsCancellation(true);
            HubResponse editHubResponse = lmsServiceHelper.editHub(hubResponse.getHub().get(0).getId(), hubEntry);
            Assert.assertEquals(editHubResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString(), "hub Edit Failed");
            hubResponse = lmsServiceHelper.searchHubByCode(hubCode);
            isCancellationAllowed = hubResponse.getHub().get(0).getAllowsCancellation();
            Assert.assertTrue(isCancellationAllowed, "enableCancelationOnHub Failed - Edit hub Failed");
        }
    }

    public void disableCancelationOnHub(String hubCode) throws Exception {
        HubResponse hubResponse = lmsServiceHelper.searchHubByCode(hubCode);
        Boolean isCancellationAllowed = hubResponse.getHub().get(0).getAllowsCancellation();
        if(isCancellationAllowed){
            System.out.println("isCancellationAllowed:"+isCancellationAllowed);
            HubEntry hubEntry = new HubEntry();
            hubEntry.setAllowsCancellation(false);
            HubResponse editHubResponse = lmsServiceHelper.editHub(hubResponse.getHub().get(0).getId(), hubEntry);
            Assert.assertEquals(editHubResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString(), "hub Edit Failed");
            hubResponse = lmsServiceHelper.searchHubByCode(hubCode);
            isCancellationAllowed = hubResponse.getHub().get(0).getAllowsCancellation();
            Assert.assertFalse(isCancellationAllowed, "disableCancelationOnHub failed - Edit hub Failed");
        }
    }


    public void enableSorterOnHub(String hubCode) throws Exception {
        HubResponse hubResponse = lmsServiceHelper.searchHubByCode(hubCode);
        Boolean isSorterEnabled = hubResponse.getHub().get(0).getIsSorterEnabled();
        if(!isSorterEnabled){
            System.out.println("isCancellationAllowed:"+isSorterEnabled);
            System.out.println("Enabling sorter on "+hubCode);
            HubEntry hubEntry = new HubEntry();
            hubEntry.setIsSorterEnabled(true);
            HubResponse editHubResponse = lmsServiceHelper.editHub(hubResponse.getHub().get(0).getId(), hubEntry);
            Assert.assertEquals(editHubResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString(), "hub Edit Failed");
            hubResponse = lmsServiceHelper.searchHubByCode(hubCode);
            isSorterEnabled = hubResponse.getHub().get(0).getIsSorterEnabled();
            Assert.assertTrue(isSorterEnabled, "enableCancelationOnHub Failed - Edit hub Failed");
        }
    }

    public void disableSorterOnHub(String hubCode) throws Exception {
        HubResponse hubResponse = lmsServiceHelper.searchHubByCode(hubCode);
        Boolean isSorterEnabled = hubResponse.getHub().get(0).getIsSorterEnabled();
        if(isSorterEnabled){
            System.out.println("isCancellationAllowed:"+isSorterEnabled);
            System.out.println("Disabling sorter on "+hubCode);
            HubEntry hubEntry = new HubEntry();
            hubEntry.setIsSorterEnabled(false);
            HubResponse editHubResponse = lmsServiceHelper.editHub(hubResponse.getHub().get(0).getId(), hubEntry);
            Assert.assertEquals(editHubResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString(), "hub Edit Failed");
            hubResponse = lmsServiceHelper.searchHubByCode(hubCode);
            isSorterEnabled = hubResponse.getHub().get(0).getIsSorterEnabled();
            Assert.assertFalse(isSorterEnabled, "disableCancelationOnHub failed - Edit hub Failed");
        }
    }

    public static String MasterBagInscanV2OnCancelledOrder(Long masterBagId, String currentHub, String nextExpectedHub) throws JAXBException, IOException {
        System.out.println(String.format("MasterBag[%s] Inscan at %s", masterBagId, currentHub));
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, LMS_CONSTANTS.TENANTID);
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        ShipmentResponse shipmentResponse;
        String nextSortationLocation = null;
        String nextHub = null;
        while (shipmentEntryItr.hasNext()) {
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            for (OrderShipmentAssociationEntry orderShipmentAssociationEntry : shipmentEntry.getOrderShipmentAssociationEntries()) {
                String tempTrackingNumber = orderShipmentAssociationEntry.getTrackingNumber();
                ShipmentType shipmentType = orderShipmentAssociationEntry.getShipmentType();
                lmsServiceHelper.masterBagInScanV2Update(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
                shipmentResponse = lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), tempTrackingNumber,
                        shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId(),
                        shipmentType, shipmentEntry.getLastScannedPremisesType());
                Assert.assertTrue(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getShipmentCancelled(),
                        "Shipment should be cancelled state, UI error message 'This Shipment is cancelled. Please put it in cancellation bin' wont be displayed without this flag");
                lmsServiceHelper.masterBagInScanV2RecieveShipment(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
                nextSortationLocation = sortationHelper.getNextSortLocation(tempTrackingNumber, currentHub, SortLocationType.EXTERNAL).getSortationConfigList()
                        .get(0).getSortLocation().getSourceReferenceCode();
                Assert.assertEquals(nextSortationLocation, nextExpectedHub, "Invalid Sortloaction/Returns Hub");
            }
        }
        return nextSortationLocation;
    }

    public static void MasterBagInscanV2OnCancelledOrderAtRTO(Long masterBagId, String currentHub, String nextExpectedHub) throws JAXBException, IOException {
        System.out.println(String.format("MasterBag[%s] Inscan at %s", masterBagId, currentHub));
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, LMS_CONSTANTS.TENANTID);
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        ShipmentResponse shipmentResponse;
        String nextSortationLocation;
        String nextHub = null;
        while (shipmentEntryItr.hasNext()) {
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            for (OrderShipmentAssociationEntry orderShipmentAssociationEntry : shipmentEntry.getOrderShipmentAssociationEntries()) {
                String tempTrackingNumber = orderShipmentAssociationEntry.getTrackingNumber();
                ShipmentType shipmentType = orderShipmentAssociationEntry.getShipmentType();
                lmsServiceHelper.masterBagInScanV2Update(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
                shipmentResponse = lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), tempTrackingNumber,
                        shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId(),
                        shipmentType, shipmentEntry.getLastScannedPremisesType());
                Assert.assertNull(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getShipmentCancelled(),
                        "Cancellation message should not appear again in RTO");
                lmsServiceHelper.masterBagInScanV2RecieveShipment(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
                nextSortationLocation = sortationHelper.getNextSortLocation(tempTrackingNumber, currentHub, SortLocationType.EXTERNAL).getSortationConfigList()
                        .get(0).getSortLocation().getSourceReferenceCode();
                Assert.assertEquals(nextSortationLocation, nextExpectedHub, "Invalid Sortloaction/Returns Hub");
            }
        }
    }


    public static void SetCourierCreationStatusFor3PL(String packetId){
        DBUtilities
                .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                        + packetId + "'", "lms");
    }
}
