package com.myntra.apiTests.erpservices.lmsUploder;

import com.myntra.apiTests.erpservices.sortation.configuploader.SortationConfigUploader;
import com.myntra.apiTests.erpservices.sortation.configuploader.pojos.UploadMetadataResponse;
import org.testng.Assert;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class LMSUploadHelper {


    /**
     * @description : It will handle the SORT_LOCATION_UPLOAD,SORTATION_CONFIG_UPLOAD and BULK_ORDER_STATUS_UPDATE config upload
     * @param bulkJobType
     * @param filePath
     * @throws Exception
     */
    public void configurationUpload(String bulkJobType,String filePath) throws Exception {
        SortationConfigUploader sortationConfigUploader;
        UploadMetadataResponse uploadMetadataResponse;
        String awsFileUrl;
        Integer jobId;
        Boolean jobStatus;
        switch (bulkJobType) {
            case "SORT_LOCATION_UPLOAD":
                sortationConfigUploader = new SortationConfigUploader();
                uploadMetadataResponse = sortationConfigUploader.getMetadataForUpload();
                awsFileUrl = sortationConfigUploader.uploadFileToAWS(filePath, uploadMetadataResponse);
                jobId = sortationConfigUploader.submitS3Job(awsFileUrl, filePath, bulkJobType);
                jobStatus = sortationConfigUploader.getJobFinalStatus(jobId);
                Assert.assertTrue(jobStatus,"Upload is not successful");
                break;
            case "SORTATION_CONFIG_UPLOAD":
                sortationConfigUploader = new SortationConfigUploader();
                uploadMetadataResponse = sortationConfigUploader.getMetadataForUpload();
                awsFileUrl = sortationConfigUploader.uploadFileToAWS(filePath, uploadMetadataResponse);
                jobId = sortationConfigUploader.submitS3Job(awsFileUrl, filePath, bulkJobType);
                jobStatus = sortationConfigUploader.getJobFinalStatus(jobId);
                Assert.assertTrue(jobStatus,"Upload is not successful");
                break;
            case "BULK_ORDER_STATUS_UPDATE":
                sortationConfigUploader = new SortationConfigUploader();
                uploadMetadataResponse = sortationConfigUploader.getMetadataForUpload();
                awsFileUrl = sortationConfigUploader.uploadFileToAWS(filePath, uploadMetadataResponse);
                jobId = sortationConfigUploader.submitS3Job(awsFileUrl, filePath, bulkJobType);
                jobStatus = sortationConfigUploader.getJobFinalStatus(jobId);
                Assert.assertTrue(jobStatus,"Upload is not successful");
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + bulkJobType);
        }
    }
}
