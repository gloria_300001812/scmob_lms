package com.myntra.apiTests.erpservices.lmsUploader;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class LMSDownloadHelper {

    public static List<String> downloadFileFromAmazonurl(String urlToRead) throws Exception {
        List<String> lstValue=new ArrayList<>();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            lstValue.add(line);
        }
        bufferedReader.close();
        return lstValue;
    }
}
