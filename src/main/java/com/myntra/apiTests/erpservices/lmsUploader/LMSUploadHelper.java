package com.myntra.apiTests.erpservices.lmsUploader;

import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.sortation.configuploader.SortationConfigUploader;
import com.myntra.apiTests.erpservices.sortation.configuploader.pojos.UploadMetadataResponse;
import com.myntra.lms.client.status.BulkJobType;
import org.apache.log4j.Logger;

public class LMSUploadHelper {
    private static Logger log = Logger.getLogger(LmsServiceHelper.class);

    /**
     * @param bulkJobType
     * @param filePath
     * @throws Exception
     * @description : It will handle the SORT_LOCATION_UPLOAD,SORTATION_CONFIG_UPLOAD and BULK_ORDER_STATUS_UPDATE config upload
     */
    public void configurationUpload(BulkJobType bulkJobType, String filePath) throws Exception {
        SortationConfigUploader sortationConfigUploader;
        UploadMetadataResponse uploadMetadataResponse;
        String awsFileUrl;
        Integer jobId;
        Boolean jobStatus;
        int i = 0;
        int MAX_JOB_TIME= 60; //60 seconds
        switch (bulkJobType) {
            case SORT_LOCATION_UPLOAD:
                sortationConfigUploader = new SortationConfigUploader();
                uploadMetadataResponse = sortationConfigUploader.getMetadataForUpload();
                awsFileUrl = sortationConfigUploader.uploadFileToAWS(filePath, uploadMetadataResponse);
                jobId = sortationConfigUploader.submitS3Job(awsFileUrl, filePath, bulkJobType.toString());
                jobStatus = sortationConfigUploader.getJobFinalStatus(jobId);
                while (!(jobStatus)) {
                    log.info("Trying to upload SORT_LOCATION_UPLOAD, but current status is- "+jobStatus);
                    Thread.sleep(1000);
                    if(i++>=MAX_JOB_TIME){
                        break;
                    }
                    jobStatus = sortationConfigUploader.getJobFinalStatus(jobId);
                }
                break;
            case SORTATION_CONFIG_UPLOAD:
                sortationConfigUploader = new SortationConfigUploader();
                uploadMetadataResponse = sortationConfigUploader.getMetadataForUpload();
                awsFileUrl = sortationConfigUploader.uploadFileToAWS(filePath, uploadMetadataResponse);
                jobId = sortationConfigUploader.submitS3Job(awsFileUrl, filePath, bulkJobType.toString());
                jobStatus = sortationConfigUploader.getJobFinalStatus(jobId);
                while (!(jobStatus)) {
                    log.info("Trying to upload SORTATION_CONFIG_UPLOAD, but current status is- "+jobStatus);
                    Thread.sleep(1000);
                    if(i++>=MAX_JOB_TIME){
                        break;
                    }
                    jobStatus = sortationConfigUploader.getJobFinalStatus(jobId);
                }
                break;
            case BULK_ORDER_STATUS_UPDATE:
                sortationConfigUploader = new SortationConfigUploader();
                uploadMetadataResponse = sortationConfigUploader.getMetadataForUpload();
                awsFileUrl = sortationConfigUploader.uploadFileToAWS(filePath, uploadMetadataResponse);
                jobId = sortationConfigUploader.submitS3Job(awsFileUrl, filePath, bulkJobType.toString());
                jobStatus = sortationConfigUploader.getJobFinalStatus(jobId);
                while (!(jobStatus)) {
                    log.info("Trying to upload BULK_ORDER_STATUS_UPDATE, but current status is- "+jobStatus);
                    Thread.sleep(1000);
                    if(i++>=MAX_JOB_TIME){
                        break;
                    }
                    jobStatus = sortationConfigUploader.getJobFinalStatus(jobId);
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + bulkJobType);
        }
    }

}
