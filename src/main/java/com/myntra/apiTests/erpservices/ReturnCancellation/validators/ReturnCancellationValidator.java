package com.myntra.apiTests.erpservices.ReturnCancellation.validators;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public class ReturnCancellationValidator {
    private Map<String, Object> eventEntries = null;
    private Map<String, Object> returnShipment = null;
    private Map<String, Object> returnShipmentId = null;
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();

    public String getReturnShipmentIdByTrackingNumber(String trackingNo) throws InterruptedException {
        for(int times=0;times<7;times++) {
            returnShipmentId = DBUtilities.exSelectQueryForSingleRecord("select id  from return_shipment where tracking_number=\"" + trackingNo + "\"", "myntra_lms");
            if (returnShipmentId == null)
                Thread.sleep(10000);
            else break;
        }
        return returnShipmentId.get("id").toString();
    }

    public void checkReturnShipmentStatus(String returnShipmentId, String status) throws InterruptedException {
        for(int times=0;times<7;times++) {
            returnShipment = DBUtilities.exSelectQueryForSingleRecord("select shipment_status from return_shipment where id =" + returnShipmentId, "myntra_lms");
            if (returnShipment.get("shipment_status").toString() != status) {
                Thread.sleep(10000);
            } else break;
        }
        Assert.assertEquals(returnShipment.get("shipment_status").toString(),status,"Return SHipment table is not updated to canceled"+returnShipmentId);
    }


    public void checkReconStatus(String orderId, String tripOrderAssignmentId, String status, String isReceived) throws InterruptedException {
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(orderId, status, 10), "Wrong status in ml_shipment");

        for(int times=0;times<7;times++)
        {
            if(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId) == isReceived)
            Thread.sleep(10000);
            else break;
        }
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), isReceived, "Wrong status in trip_order_assignment");
    }

    public void validateAssociation(String pickup_shipment_id_fk, String return_shipment_id_fk, int pickup_is_active, int return_is_active, String returnId, String trackingNo) throws UnsupportedEncodingException, JAXBException, InterruptedException {
        Map<String, Object> pickupShipmentMap = null;Map<String, Object> returnShipmentMap = null;
        for(int times=0; times<7;times++) {
           pickupShipmentMap = DBUtilities.exSelectQueryForSingleRecord("select is_active from return_pickup_association where pickup_shipment_id_fk=" + pickup_shipment_id_fk, "myntra_lms");
            if (Integer.parseInt(pickupShipmentMap.get("is_active").toString()) != pickup_is_active)
                Thread.sleep(10000);
            else break;
        }
        Assert.assertTrue(pickupShipmentMap.get("is_active").equals(pickup_is_active));

        for(int times=0;times<7;times++)
        {
            returnShipmentMap = DBUtilities.exSelectQueryForSingleRecord("select is_active from return_pickup_association where return_shipment_id_fk=" + return_shipment_id_fk, "myntra_lms");
            if(Integer.parseInt(returnShipmentMap.get("is_active").toString()) == return_is_active)
                Thread.sleep(10000);
            else break;
        }
        Assert.assertTrue(returnShipmentMap.get("is_active").equals(return_is_active));

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(returnId, EnumSCM.CANCELLED, 10),"Cancelled event expected in ml_shipment table for return id : "+returnId);
        //Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS(packetId, EnumSCM.CANCELLED, 4),"Cancelled event expected in order_to_ship table for orderId : "+packetId);

        for (int times=0;times<7;times++)
        {
        if(lmsHelper.getShipmentStatusForOrderTracking(trackingNo) != EnumSCM.CANCELLED)
            Thread.sleep(10000);
        else break;}
        Assert.assertEquals(lmsHelper.getShipmentStatusForOrderTracking(trackingNo),EnumSCM.CANCELLED,"Expected CANCELLED sattus in order tracking");
    }

    public void validateIsCancellableFlag(String returnID,boolean val) throws InterruptedException {
        for (int times = 0;times<7;times++) {
            eventEntries = DBUtilities.exSelectQueryForSingleRecord("select is_cancellable from return_shipment where source_return_id="+returnID,"myntra_lms");
            if (Boolean.parseBoolean(eventEntries.get("is_cancellable").toString()) != val)
                Thread.sleep(10000);
            else break;
        }
        Assert.assertEquals(eventEntries.get("is_cancellable"),val,"is_cancellable flag is");
   }

}
