package com.myntra.apiTests.erpservices.shipmentPlatform.pojo;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.response.AbstractResponse;

import java.util.List;

@JsonRootName("PartnerResponse")
public final class PartnerResponse extends AbstractResponse {
    private List<PartnerEntry> partnerEntries;

    public PartnerResponse() {}

    public PartnerResponse(List<PartnerEntry> partnerEntries, StatusResponse response) {
        super(response);
        this.partnerEntries = partnerEntries;
    }

    public static PartnerResponseBuilder builder() {
        return new PartnerResponseBuilder();
    }

    public List<PartnerEntry> getPartnerEntries() {
        return this.partnerEntries;
    }


    protected boolean canEqual(Object other) {
        return other instanceof com.myntra.shipment.response.PartnerResponse;
    }


    public String toString() {
        return "PartnerResponse(partnerEntries=" + this.getPartnerEntries() + ")";
    }

    public static class PartnerResponseBuilder {
        private List<PartnerEntry> partnerEntries;
        private StatusResponse response;

        PartnerResponseBuilder() {
        }

        public PartnerResponse.PartnerResponseBuilder partnerEntries(List<PartnerEntry> partnerEntries) {
            this.partnerEntries = partnerEntries;
            return this;
        }

        public PartnerResponse.PartnerResponseBuilder response(StatusResponse response) {
            this.response = response;
            return this;
        }

        public PartnerResponse build() {
            return new PartnerResponse(this.partnerEntries, this.response);
        }

        public String toString() {
            return "PartnerResponse.PartnerResponseBuilder(partnerEntries=" + this.partnerEntries + ", response=" + this.response + ")";
        }
    }
}
