package com.myntra.apiTests.erpservices.shipmentPlatform.constants;

public class ShipmentPlatformconstants {
    public static final String CREATE_ALTERATION_SHIPMENT_IN_SHIPMENT_PLATFORM = "shipment/alteration";
    public static final String FIND_SHIPMENTS_BY_TRACKING_NUMBER_AND_TENANTID = "shipment/tenantId";
    public static final String ASSIGN_SHIPMENTS_PARTNER = "partner/assign";
    public static final String SEARCH_SHIPMENT="shipment/search";
    public static final String UPDATE_SHIPMENT_MEASUREMENT="shipment/measurement";
    public static final String GET_AVAILABLE_PARTNERS="partner/availablePartners";
    public static final String REFRESH_STATE_MACHINE="statemachine/refresh";
    public static final String UPDATE_DC_CODE = "shipment/deliveryCenterCode";
    public static final String COURIER_STATUS_UPDATE="shipment/courierStatus";
    public static final String STATUS_UPDATE="shipment/status";
    public static final String PUSH_ALTERATION_TO_LMS="owner";
}
