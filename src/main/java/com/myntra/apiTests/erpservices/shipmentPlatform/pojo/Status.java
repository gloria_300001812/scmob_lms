package com.myntra.apiTests.erpservices.shipmentPlatform.pojo;

import lombok.Data;

@Data
public class Status {

    private StatusCodes statusCodes;

    private String statusType;

    private String totalCount;

    private String statusMessage;

    private String statusCode;

}
