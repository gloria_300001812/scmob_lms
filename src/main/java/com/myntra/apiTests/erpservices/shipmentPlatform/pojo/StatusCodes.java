package com.myntra.apiTests.erpservices.shipmentPlatform.pojo;

import lombok.Data;

@Data
public class StatusCodes {
    private String statusMessage;

    private String statusCode;

    private String httpStatusCode;

}
