package com.myntra.apiTests.erpservices.shipmentPlatform.pojo;


public final class PartnerEntry {

    private String name;
    private String courierCode;
    private String tenantId;

    public PartnerEntry() {
    }

    PartnerEntry(String name, String courierCode, String tenantId) {
        this.name = name;
        this.courierCode = courierCode;
        this.tenantId = tenantId;
    }

    public static PartnerEntryBuilder builder() {
        return new PartnerEntryBuilder();
    }

    public String getName() {
        return this.name;
    }

    public String getCourierCode() {
        return this.courierCode;
    }

    public String getTenantId() {
        return this.tenantId;
    }

    public String toString() {
        return "PartnerEntry(name=" + this.getName() + ", courierCode=" + this.getCourierCode() + ", tenantId=" + this.getTenantId() + ")";
    }

    public static class PartnerEntryBuilder {
        private String name;
        private String courierCode;
        private String tenantId;

        PartnerEntryBuilder() {
        }

        public PartnerEntry.PartnerEntryBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PartnerEntry.PartnerEntryBuilder courierCode(String courierCode) {
            this.courierCode = courierCode;
            return this;
        }

        public PartnerEntry.PartnerEntryBuilder tenantId(String tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public PartnerEntry build() {
            return new PartnerEntry(this.name, this.courierCode, this.tenantId);
        }

        public String toString() {
            return "PartnerEntry.PartnerEntryBuilder(name=" + this.name + ", courierCode=" + this.courierCode + ", tenantId=" + this.tenantId + ")";
        }
    }
}
