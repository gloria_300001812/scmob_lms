package com.myntra.apiTests.erpservices.shipmentPlatform.pojo;

import lombok.Data;

@Data
public class AlterationShipmentRequest {

    private String pickupSlot;

    private String trackingNo;

    private String receiverName;

    private String isCancellable;

    private Long alterationId;

    private String actionCode;

    private String alterationMode;

    private String courierCode;
}
