package com.myntra.apiTests.erpservices.shipmentPlatform.pojo;

import com.myntra.shipment.types.ShipmentUpdateEvent;
import com.myntra.shipment.types.ShipmentUpdateMode;
import lombok.Data;

import java.util.Date;

@Data
public class CourierShipmentUpdateRequest {

    private String eventLocation;

    private String courierTrackingNumber;

    private String tenantId;

    private Date eventTime;

    private ShipmentUpdateMode shipmentUpdateMode;

    private ShipmentUpdateEvent event;

    private String remarks;
}
