package com.myntra.apiTests.erpservices.shipmentPlatform.helper;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.shipmentPlatform.constants.ShipmentPlatformconstants;
import com.myntra.apiTests.erpservices.shipmentPlatform.pojo.AlterationShipmentRequest;
import com.myntra.apiTests.erpservices.shipmentPlatform.pojo.CourierShipmentUpdateRequest;
import com.myntra.apiTests.erpservices.shipmentPlatform.pojo.PartnerResponse;
import com.myntra.apiTests.erpservices.shipmentPlatform.pojo.ShipmentCreationResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.shipment.entry.MeasurementDetailsEntry;
import com.myntra.shipment.entry.MeasurementEntry;
import com.myntra.shipment.entry.PartnerEntry;
import com.myntra.shipment.request.*;
import com.myntra.shipment.response.ShipmentDetailsResponse;
import com.myntra.shipment.response.ShipmentSearchResponse;
import com.myntra.shipment.response.ShipmentUpdateResponse;
import com.myntra.shipment.types.*;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import junit.framework.Assert;
import lombok.SneakyThrows;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ShipmentPlatformHelper {
    private static Logger log = Logger.getLogger(ShipmentPlatformHelper.class);


    public static <T> Object getDeseralizedDatesOnJsonToObjectUsingFasterXML(String json, Class<T> className) throws IOException {
        com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.disable(DeserializationFeature.UNWRAP_ROOT_VALUE);
        objectMapper.registerModule(new JodaModule());
        ;
        return objectMapper.readValue(json, className);
    }

    public static Object getJsontoObjectUsingFasterXML(String json, Object className) throws IOException {
        com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, false);
        return mapper.readValue(json, className.getClass());
    }

    public static com.myntra.apiTests.erpservices.shipmentPlatform.pojo.ShipmentCreationResponse createAlterationShipmentInSP(String sourceReferenceId, String deliveryCenterCode, String sourceItemReferenceId, String pincode, ShipmentType shipmentType, String tenantId, String clientId, ScheduleType scheduleType, Date scheduleStart, Date scheduleEnd, Mode mode, String skuId, String styleId, Double itemValue, Double shipmentValue) throws IOException {
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        CustomerInfoEntry customerInfoEntry = new CustomerInfoEntry(String.valueOf(random).substring(0, 6), "recipientName",
                String.valueOf(random).substring(0, 10), String.valueOf(random).substring(0, 10), "LMS Automation - Myntra",
                "560068", AddressType.OFFICE, "kudle Gate", "kudle Gate", "Bangalore", "Karnataka", pincode);
        ShipmentItemEntry shipmentItemEntry = new ShipmentItemEntry(sourceItemReferenceId, "Nice Cloth", skuId,
                styleId, itemValue, "shirt", "www.myntra.com/style/2335","S","12345",1);
        List<ShipmentItemEntry> shipmentItemEntries = new ArrayList();
        shipmentItemEntries.add(shipmentItemEntry);
        AlterationShipmentCreationRequest alterationShipmentCreationRequest = new AlterationShipmentCreationRequest(sourceReferenceId, deliveryCenterCode,"2297",
                shipmentType, tenantId, clientId, customerInfoEntry, scheduleType, scheduleStart, scheduleEnd,
                PaymentMode.ONLINE, shipmentValue, mode, SourcePath.MYNTRA, shipmentItemEntries);


        String payload = APIUtilities.getObjectToJSON(alterationShipmentCreationRequest);
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.CREATE_ALTERATION_SHIPMENT_IN_SHIPMENT_PLATFORM, new String[]{}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());

        com.myntra.apiTests.erpservices.shipmentPlatform.pojo.ShipmentCreationResponse shipmentCreationResponse = (com.myntra.apiTests.erpservices.shipmentPlatform.pojo.ShipmentCreationResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), com.myntra.apiTests.erpservices.shipmentPlatform.pojo.ShipmentCreationResponse.class);
        log.info("Created Alteration Shipment in Shipment Platform");
        if (shipmentCreationResponse.getStatus().getStatusType().toString().contains("ERROR")) {
            Assert.fail("Alteration Shipment creation in Shipment Platform - Service is failed , Check if  Shipment Platform service is UP");
        }
        return shipmentCreationResponse;
    }


    public static ShipmentDetailsResponse getMlShipmentDetailsByTrackingNumberAndTenantId(String trackingNumber, String tenantId) throws IOException {
        String path = MessageFormat.format("/{0}/trackingNumber/{1}", tenantId, trackingNumber);
        log.info("Get Shipment details from ml_lms_shipment ( Shipment-platform service API )");
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.FIND_SHIPMENTS_BY_TRACKING_NUMBER_AND_TENANTID + path, new String[]{}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        log.info("Fetched  Shipment details from ml_lms_shipment ( Shipment-platform service API )");
        ShipmentDetailsResponse shipmentDetailsResponse = (ShipmentDetailsResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), ShipmentDetailsResponse.class);
        return shipmentDetailsResponse;
    }

    public PartnerResponse assignShipmentsToPartners(String name, String courierCode, String trackingNumber, String tenantId) throws IOException {
        List<PartnerEntry> partnerEntries = new ArrayList<>();
        PartnerEntry.builder()
                .courierCode(courierCode)
                .tenantId(tenantId)
                .name(name)
                .build();
        partnerEntries.add(PartnerEntry.builder().build());
        String payload = APIUtilities.getObjectToJSON(partnerEntries);
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.ASSIGN_SHIPMENTS_PARTNER, new String[]{}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        PartnerResponse partnerResponse = (PartnerResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), PartnerResponse.class);
        return partnerResponse;
    }


    /**
     * Used to search the shipment using tracking number with Date inbetween 30 days and status
     *
     * @param trackingNumber
     * @param tenantId
     * @param shipmentStatus1
     * @param deliveryCenterCode
     * @param shipmentStatus2
     * @return
     */
    @SneakyThrows
    public static ShipmentSearchResponse searchShipments(String trackingNumber, String tenantId, ShipmentStatus shipmentStatus1, String deliveryCenterCode, ShipmentStatus shipmentStatus2) {

        String query = MessageFormat.format("?trackingNumber={0}&tenantId={1}&fromDate={2}&toDate={3}&shipmentStatus={4}&deliveryCenterCode={5}&shipmentStatus={6}",
                trackingNumber, tenantId, DateTimeHelper.generateDate("yyyy-MM-dd HH:mm:ss", -30).replace(" ", "%20"), DateTimeHelper.generateDate("yyyy-MM-dd HH:mm:ss", 0).replace(" ", "%20"),
                String.valueOf(shipmentStatus1), deliveryCenterCode, String.valueOf(shipmentStatus2));
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.SEARCH_SHIPMENT, new String[]{query}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        ShipmentSearchResponse shipmentSearchResponse = (ShipmentSearchResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), ShipmentSearchResponse.class);

        return shipmentSearchResponse;
    }

    /**
     * Used to Update the Measurement Values for existing Shipment
     *
     * @param trackingNumber
     * @param tenantId
     * @return
     */
    @SneakyThrows
    public static ShipmentUpdateResponse updateMeasurementsForShipment(String trackingNumber, String tenantId) {
        MeasurementEntry measurementEntry1 = new MeasurementEntry("length", "36");
        MeasurementEntry measurementEntry2 = new MeasurementEntry("breadth", "36");
        MeasurementEntry measurementEntry3 = new MeasurementEntry("width", "36");
        List<MeasurementEntry> measurementEntries = new ArrayList<>();
        measurementEntries.add(measurementEntry1);
        measurementEntries.add(measurementEntry2);
        measurementEntries.add(measurementEntry3);

        MeasurementDetailsEntry measurementDetailsEntry = new MeasurementDetailsEntry("inch", measurementEntries);

        UpdateMeasurementRequest updateMeasurementRequest = new UpdateMeasurementRequest(measurementDetailsEntry, trackingNumber, tenantId);

        String payload = APIUtilities.getObjectToJSON(updateMeasurementRequest);
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.UPDATE_SHIPMENT_MEASUREMENT, new String[]{}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        ShipmentUpdateResponse shipmentUpdateResponse = (ShipmentUpdateResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), ShipmentUpdateResponse.class);

        return shipmentUpdateResponse;
    }

    /**
     * Used to get all partner details based on tracking number and tenantId
     *
     * @param trackingNumber
     * @param tenantId
     * @return
     */
    @SneakyThrows
    public static PartnerResponse getAvailablePartners(String trackingNumber, String tenantId) {
        String query = MessageFormat.format("?trackingNumber={0}&tenantId={1}", trackingNumber, tenantId);
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.GET_AVAILABLE_PARTNERS, new String[]{query}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        PartnerResponse partnerResponse = (PartnerResponse) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(), PartnerResponse.class, false);
        return partnerResponse;
    }

    /**
     * Used to Refresh state machine
     *
     * @return
     */
    @SneakyThrows
    public static String refreshStateMachine() {
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.REFRESH_STATE_MACHINE, new String[]{}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        return service.getResponseBody();
    }

    /**
     * USed to update the DC code for an Shipment
     *
     * @param trackingNumber
     * @param tenantId
     * @param dcCode
     * @return
     */
    @SneakyThrows
    public static ShipmentUpdateResponse updateDCCode(String trackingNumber, String tenantId, String dcCode) {

        UpdateDeliveryCenterRequest updateDeliveryCenterRequest = new UpdateDeliveryCenterRequest(trackingNumber, tenantId, dcCode);
        String payload = APIUtilities.getObjectToJSON(updateDeliveryCenterRequest);
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.UPDATE_DC_CODE, new String[]{}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        ShipmentUpdateResponse shipmentUpdateResponse = (ShipmentUpdateResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), ShipmentUpdateResponse.class);
        return shipmentUpdateResponse;
    }

    /**
     * USed to assign the shipment to store partner
     *
     * @param courierCode
     * @param courierTenantId
     * @param trackingNumber
     * @param tenantId
     * @return
     */
    @SneakyThrows
    public static ShipmentCreationResponse assignShipmentToPartner(String courierCode, String courierTenantId, String trackingNumber, String tenantId) {

        PartnerAssignmentRequest partnerAssignmentRequest = new PartnerAssignmentRequest(courierCode, courierTenantId, trackingNumber, tenantId);
        String payload = APIUtilities.getObjectToJSON(partnerAssignmentRequest);
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.ASSIGN_SHIPMENTS_PARTNER, new String[]{}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        ShipmentCreationResponse partnerResponse = (ShipmentCreationResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), ShipmentCreationResponse.class);
        return partnerResponse;
    }

    /**
     * Used to update Courier status - sync
     *
     * @param courierTrackingNumber
     * @param event
     * @param tenantId
     * @param shipmentUpdateMode
     * @return
     */
    @SneakyThrows
    public static ShipmentCreationResponse courierStatusUpdateSync(String courierTrackingNumber, ShipmentUpdateEvent event, String tenantId, ShipmentUpdateMode shipmentUpdateMode) {
        com.myntra.apiTests.erpservices.shipmentPlatform.pojo.CourierShipmentUpdateRequest courierShipmentUpdateRequest = new CourierShipmentUpdateRequest();
        courierShipmentUpdateRequest.setCourierTrackingNumber(courierTrackingNumber);
        courierShipmentUpdateRequest.setEvent(event);
        courierShipmentUpdateRequest.setTenantId(tenantId);
        courierShipmentUpdateRequest.setEventLocation("Bangalore");
        courierShipmentUpdateRequest.setShipmentUpdateMode(shipmentUpdateMode);
        courierShipmentUpdateRequest.setRemarks("Alteration");
        courierShipmentUpdateRequest.setEventTime(new Date());

        String payload = APIUtilities.getObjectToJSON(courierShipmentUpdateRequest);
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.COURIER_STATUS_UPDATE + "/sync", new String[]{}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        ShipmentCreationResponse shipmentCreationResponse = (ShipmentCreationResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), ShipmentCreationResponse.class);

        return shipmentCreationResponse;
    }

    /**
     * Used to update Courier status - Async
     *
     * @param courierTrackingNumber
     * @param event
     * @param tenantId
     * @param shipmentUpdateMode
     * @return
     */
    @SneakyThrows
    public static ShipmentCreationResponse courierStatusUpdateAsync(String courierTrackingNumber, ShipmentUpdateEvent event, String tenantId, ShipmentUpdateMode shipmentUpdateMode) {
        com.myntra.apiTests.erpservices.shipmentPlatform.pojo.CourierShipmentUpdateRequest courierShipmentUpdateRequest = new CourierShipmentUpdateRequest();
        courierShipmentUpdateRequest.setCourierTrackingNumber(courierTrackingNumber);
        courierShipmentUpdateRequest.setEvent(event);
        courierShipmentUpdateRequest.setTenantId(tenantId);
        courierShipmentUpdateRequest.setEventLocation("Bangalore");
        courierShipmentUpdateRequest.setShipmentUpdateMode(shipmentUpdateMode);
        courierShipmentUpdateRequest.setRemarks("Alteration");
        courierShipmentUpdateRequest.setEventTime(new Date());

        String payload = APIUtilities.getObjectToJSON(courierShipmentUpdateRequest);
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.COURIER_STATUS_UPDATE + "/async", new String[]{}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        ShipmentCreationResponse shipmentCreationResponse = (ShipmentCreationResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), ShipmentCreationResponse.class);

        return shipmentCreationResponse;
    }

    /**
     * Used to Courier status - sync
     *
     * @param courierTrackingNumber
     * @param event
     * @param tenantId
     * @param shipmentUpdateMode
     * @return
     */
    @SneakyThrows
    public static ShipmentCreationResponse statusUpdateSync(String courierTrackingNumber, ShipmentUpdateEvent event, String tenantId, ShipmentUpdateMode shipmentUpdateMode) {
        com.myntra.apiTests.erpservices.shipmentPlatform.pojo.CourierShipmentUpdateRequest courierShipmentUpdateRequest = new CourierShipmentUpdateRequest();
        courierShipmentUpdateRequest.setCourierTrackingNumber(courierTrackingNumber);
        courierShipmentUpdateRequest.setEvent(event);
        courierShipmentUpdateRequest.setTenantId(tenantId);
        courierShipmentUpdateRequest.setEventLocation("Bangalore");
        courierShipmentUpdateRequest.setShipmentUpdateMode(shipmentUpdateMode);
        courierShipmentUpdateRequest.setRemarks("Alteration");
        courierShipmentUpdateRequest.setEventTime(new Date());

        String payload = APIUtilities.getObjectToJSON(courierShipmentUpdateRequest);
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.STATUS_UPDATE + "/sync", new String[]{}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        ShipmentCreationResponse shipmentCreationResponse = (ShipmentCreationResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), ShipmentCreationResponse.class);
        return shipmentCreationResponse;
    }

    /**
     * Used to Courier status - Async
     *
     * @param courierTrackingNumber
     * @param event
     * @param tenantId
     * @param shipmentUpdateMode
     * @return
     */
    @SneakyThrows
    public static ShipmentCreationResponse statusUpdateAsync(String courierTrackingNumber, ShipmentUpdateEvent event, String tenantId, ShipmentUpdateMode shipmentUpdateMode) {
        com.myntra.apiTests.erpservices.shipmentPlatform.pojo.CourierShipmentUpdateRequest courierShipmentUpdateRequest = new CourierShipmentUpdateRequest();
        courierShipmentUpdateRequest.setCourierTrackingNumber(courierTrackingNumber);
        courierShipmentUpdateRequest.setEvent(event);
        courierShipmentUpdateRequest.setTenantId(tenantId);
        courierShipmentUpdateRequest.setEventLocation("Bangalore");
        courierShipmentUpdateRequest.setShipmentUpdateMode(shipmentUpdateMode);
        courierShipmentUpdateRequest.setRemarks("Alteration");
        courierShipmentUpdateRequest.setEventTime(new Date());

        String payload = APIUtilities.getObjectToJSON(courierShipmentUpdateRequest);
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.STATUS_UPDATE + "/async", new String[]{}, SERVICE_TYPE.SHIPMENT_PLATFORM_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        ShipmentCreationResponse shipmentCreationResponse = (ShipmentCreationResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), ShipmentCreationResponse.class);
        return shipmentCreationResponse;
    }

    /**
     * Used to push the alteration shipment to LMS
     *
     * @param alterationId
     * @param actionCode
     * @param courierCode
     * @param trackingNumber
     * @param isCancellable
     * @param alterationMode
     * @param pickupSlot
     * @param clientId
     * @return
     */
    @SneakyThrows
    public static ShipmentCreationResponse pushAlterationToLMS(long alterationId, String actionCode, String courierCode, String trackingNumber, String isCancellable, String alterationMode, String pickupSlot, String clientId) {
        AlterationShipmentRequest alterationShipmentRequest = new AlterationShipmentRequest();
        alterationShipmentRequest.setAlterationId(alterationId);
        alterationShipmentRequest.setActionCode(actionCode);
        alterationShipmentRequest.setCourierCode(courierCode);
        alterationShipmentRequest.setTrackingNo(trackingNumber);
        alterationShipmentRequest.setReceiverName("test");
        alterationShipmentRequest.setIsCancellable(isCancellable);
        alterationShipmentRequest.setAlterationMode(alterationMode);
        alterationShipmentRequest.setPickupSlot(pickupSlot);

        String payload = APIUtilities.getObjectToJSON(alterationShipmentRequest);
        Svc service = HttpExecutorService.executeHttpService(ShipmentPlatformconstants.PUSH_ALTERATION_TO_LMS + "/" + clientId + "/update", new String[]{}, SERVICE_TYPE.AMS_SVC.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
        ShipmentCreationResponse shipmentCreationResponse = (ShipmentCreationResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), ShipmentCreationResponse.class);
        return shipmentCreationResponse;
    }


}

