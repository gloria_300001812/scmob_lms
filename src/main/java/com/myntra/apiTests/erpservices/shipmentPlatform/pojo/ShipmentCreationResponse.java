package com.myntra.apiTests.erpservices.shipmentPlatform.pojo;

import lombok.Data;

@Data
public class ShipmentCreationResponse {
    private String sourceReferenceId;

    private Status status;

}
