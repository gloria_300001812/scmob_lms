package com.myntra.apiTests.erpservices.Redis;

import lombok.Data;

/**
 * Created by Gloria on 4/12/17.
 */
public class Status {

    private Integer statusCode;
    private String statusType;
    private String statusMessage;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }



    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }



    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

}
