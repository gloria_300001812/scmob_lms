package com.myntra.apiTests.erpservices.Redis;

import lombok.Data;

/**
 * Created by Gloria R on 4/11/18.
 */
public class RedisResponse {

    private Status status;
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
