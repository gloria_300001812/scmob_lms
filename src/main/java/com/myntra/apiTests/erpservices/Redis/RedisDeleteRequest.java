package com.myntra.apiTests.erpservices.Redis;

import java.util.List;

public class RedisDeleteRequest {
    private String host;
    private Integer port;
    private List<String> keys;
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public List<String> getKeys() {
        return keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }


}
