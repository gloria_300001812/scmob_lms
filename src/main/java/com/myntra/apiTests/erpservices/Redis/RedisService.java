package com.myntra.apiTests.erpservices.Redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import java.util.Set;

/**
 * Created by Gloria R on 4/11/18.
 */
public class RedisService {

    static Logger log = LoggerFactory.getLogger(RedisService.class);

    public RedisResponse getService(String host, int port, String key){

        RedisResponse redisResponse = new RedisResponse();
        try{
            log.info("HOST: "+host+", PORT: "+port+", KEY: "+key);
            Jedis jedis = new Jedis(host,port);
            redisResponse.setValue(jedis.get(key));
            Status status = new Status();
            status.setStatusCode(1001);
            status.setStatusType("SUCCESS");
            status.setStatusMessage("Key Retrieved successfully");
            redisResponse.setStatus(status);
            log.info(redisResponse.toString());
            return redisResponse;
        } catch (Exception e) {
            log.info(""+e);
            Status status = new Status();
            status.setStatusCode(0);
            status.setStatusType("ERROR");
            status.setStatusMessage("Some Error occured. Please check the host, port and key values are correct and redis server is up");
            redisResponse.setStatus(status);
            log.info(redisResponse.toString());
            return redisResponse;
        }
    }

    public RedisResponse setService(String host , int port,String key, String value){
        RedisResponse redisResponse = new RedisResponse();
        try {
            log.info("HOST: "+host+", PORT: "+port+", KEY: "+key+", VALUE"+value);
            Jedis jedis = new Jedis(host,port);
            String result = jedis.get(key);
            jedis.set(key,value);
            redisResponse.setValue(jedis.get(key));
            Status status = new Status();
            status.setStatusCode(1001);
            status.setStatusType("SUCCESS");
            status.setStatusMessage("Key updated successfully");
            redisResponse.setStatus(status);
            log.info(redisResponse.toString());
            return redisResponse;
        } catch (Exception e) {
            log.info(""+e);
            Status status = new Status();
            status.setStatusCode(0);
            status.setStatusType("ERROR");
            status.setStatusMessage("Some Error occured. Please check the host, port and key values are correct and redis server is up");
            redisResponse.setStatus(status);
            log.info(redisResponse.toString());
            return redisResponse;
        }
    }

    public RedisResponse deleteRedisKey(String host, int port, String key){
        RedisResponse redisResponse = new RedisResponse();
        log.info("HOST: "+host+", PORT: "+port+", KEY: "+key);
        try {
            Jedis jedis = new Jedis(host,port);
            Set<String> keys = jedis.keys(key+"*");
            for(String k:keys){
                jedis.del(k);
            }
            redisResponse.setValue(jedis.get(key));
            Status status = new Status();
            status.setStatusCode(1001);
            status.setStatusType("SUCCESS");
            status.setStatusMessage("Key deleted successfully");
            redisResponse.setStatus(status);
            log.info(redisResponse.toString());
            return redisResponse;
        }catch (Exception e){
            log.info(""+e);
            Status status = new Status();
            status.setStatusCode(0);
            status.setStatusType("ERROR");
            status.setStatusMessage("Some Error occured. Please check the host, port and key values are correct and redis server is up");
            redisResponse.setStatus(status);
            log.info(redisResponse.toString());
            return redisResponse;
        }

    }

    public RedisResponse deleteRedisMutiKeys(RedisDeleteRequest request){
        log.info(request.toString());
        RedisResponse redisResponse = new RedisResponse();
        try{
            Jedis jedis = new Jedis(request.getHost(),request.getPort());
            request.getKeys().forEach(key->{Set<String> keys = jedis.keys(key+"*");
                for(String k:keys){
                    jedis.del(k);
                }});

            Status status = new Status();
            status.setStatusCode(1001);
            status.setStatusType("SUCCESS");
            status.setStatusMessage("Keys deleted successfully");
            redisResponse.setStatus(status);
            log.info(redisResponse.toString());
            return redisResponse;
        } catch (Exception e) {
            log.info(""+e);
            Status status = new Status();
            status.setStatusCode(0);
            status.setStatusType("ERROR");
            status.setStatusMessage("Some Error occured. Please check the host, port and key values are correct and redis server is up");
            redisResponse.setStatus(status);
            log.info(redisResponse.toString());
            return redisResponse;
        }
    }
}
