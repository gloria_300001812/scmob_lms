package com.myntra.apiTests.erpservices.tripService.helper;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMSUtils;
import com.myntra.apiTests.erpservices.tripService.constants.TripSerivceConstants;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.code.utils.AlterationMode;
import com.myntra.lastmile.client.entry.ShipmentItemEntry;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.tripservice.ShipmentPendencyCreationResponse;
import com.myntra.lastmile.client.status.*;
import com.myntra.lastmile.client.tripservice.*;
import com.myntra.lms.client.status.ShipmentSourceEnum;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.platform.domain.NDRStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class TripServiceHelper {
    private static Logger log = Logger.getLogger(TripServiceHelper.class);

    public static ShipmentPendencyCreationResponse createAlterationShipment(String sourceId, String sourceRefernceId, String trackingNumber, long deliveryCenterId, ShipmentType shipmentType, ShippingMethod shippingmethod,
                                                                            String pincode, Float shipmentValue, DateTime promiseDate, Boolean deleted, String tenantId, String clientId, ShipmentSourceEnum sourcePath, AlterationMode alterationMode, int noOfItem, String status) throws JAXBException, IOException, JSONException, XMLStreamException {
        ShipmentPendencyEntry shipmentPendencyEntry = new ShipmentPendencyEntry();
        shipmentPendencyEntry.setSourceId(sourceId);
        shipmentPendencyEntry.setSourceReferenceId(sourceRefernceId);
        shipmentPendencyEntry.setTrackingNumber(trackingNumber);
        shipmentPendencyEntry.setDeliveryCenterId(deliveryCenterId);
        shipmentPendencyEntry.setShipmentType(shipmentType);
        shipmentPendencyEntry.setShippingMethod(shippingmethod);
        shipmentPendencyEntry.setRecipientName("LMS automation");
        shipmentPendencyEntry.setAddress("AKR tech park");
        shipmentPendencyEntry.setCity("Bangalore");
        shipmentPendencyEntry.setPincode(pincode);
        shipmentPendencyEntry.setRecipientContactNumber("1234567890");
        shipmentPendencyEntry.setAlternateContactNumber("1234567890");
        shipmentPendencyEntry.setShipmentValue(shipmentValue);
        shipmentPendencyEntry.setContentsDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
        shipmentPendencyEntry.setPromiseDate(promiseDate);
        shipmentPendencyEntry.setDeleted(deleted);
        shipmentPendencyEntry.setRouteId("r15");
        shipmentPendencyEntry.setAddressRouteLocality("Kudlu gate");
        shipmentPendencyEntry.setTenantId(tenantId);
        shipmentPendencyEntry.setClientId(clientId);
        shipmentPendencyEntry.setSourcePath(sourcePath);
        shipmentPendencyEntry.setAlterationMode(alterationMode);
        List<LastMileShipmentItemEntry> shipmentItemEntries = new ArrayList();
        LastMileShipmentItemEntry shipmentItemEntry = new LastMileShipmentItemEntry();
        for (int i = 0; i < noOfItem; i++) {
            ItemDescriptionEntry itemDescriptionEntry=new ItemDescriptionEntry();
            itemDescriptionEntry.setDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
            shipmentItemEntry.setItemDescription(itemDescriptionEntry);
            shipmentItemEntry.setSourceItemReferenceId("322" + LMSUtils.randomGenn(9));
            shipmentItemEntries.add(shipmentItemEntry);
        }
        shipmentPendencyEntry.setShipmentItemEntries(shipmentItemEntries);
        log.info("Creating Alteration Shipment in Lastmile");
        String payload = APIUtilities.getObjectToJSON(shipmentPendencyEntry);
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.CREATE_ALTERATION_SHIPMENT, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = (ShipmentPendencyCreationResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), ShipmentPendencyCreationResponse.class);
        log.info("Created Alteration Shipment in Lastmile");
        if (shipmentPendencyCreationResponse.getStatus().getStatusType().toString().contains("ERROR")) {
            Assert.fail("Account creation failed , Check if Lastmile service is UP");
        }
        return shipmentPendencyCreationResponse;

    }


    public static <T> Object getDeseralizedDatesOnJsonToObjectUsingFasterXML(String json, Class<T> className) throws IOException {
        com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.disable(DeserializationFeature.UNWRAP_ROOT_VALUE);
        objectMapper.registerModule(new JodaModule());
        ;
        return objectMapper.readValue(json, className);
    }


    public static String createAlterationShipmentNegative(String sourceId, String sourceRefernceId, String trackingNumber, long deliveryCenterId, ShipmentType shipmentType, ShippingMethod shippingmethod,
                                                          String pincode, Float shipmentValue, DateTime promiseDate, Boolean deleted, String tenantId, String clientId, ShipmentSourceEnum sourcePath, AlterationMode alterationMode, int noOfItem, String status) throws JAXBException, IOException, JSONException, XMLStreamException {
        ShipmentPendencyEntry shipmentPendencyEntry = new ShipmentPendencyEntry();
        shipmentPendencyEntry.setSourceId(sourceId);
        shipmentPendencyEntry.setSourceReferenceId(sourceRefernceId);
        shipmentPendencyEntry.setTrackingNumber(trackingNumber);
        shipmentPendencyEntry.setDeliveryCenterId(deliveryCenterId);
        shipmentPendencyEntry.setShipmentType(shipmentType);
        shipmentPendencyEntry.setShippingMethod(shippingmethod);
        shipmentPendencyEntry.setRecipientName("LMS automation");
        shipmentPendencyEntry.setAddress("AKR tech park");
        shipmentPendencyEntry.setCity("Bangalore");
        shipmentPendencyEntry.setPincode(pincode);
        shipmentPendencyEntry.setRecipientContactNumber("1234567890");
        shipmentPendencyEntry.setAlternateContactNumber("1234567890");
        shipmentPendencyEntry.setShipmentValue(shipmentValue);
        shipmentPendencyEntry.setContentsDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
        shipmentPendencyEntry.setPromiseDate(promiseDate);
        shipmentPendencyEntry.setDeleted(deleted);
        shipmentPendencyEntry.setRouteId("r15");
        shipmentPendencyEntry.setAddressRouteLocality("Kudlu gate");
        shipmentPendencyEntry.setTenantId(tenantId);
        shipmentPendencyEntry.setClientId(clientId);
        shipmentPendencyEntry.setSourcePath(sourcePath);
        shipmentPendencyEntry.setAlterationMode(alterationMode);
        List<LastMileShipmentItemEntry> shipmentItemEntries = new ArrayList();
        LastMileShipmentItemEntry shipmentItemEntry = new LastMileShipmentItemEntry();
        for (int i = 0; i < noOfItem; i++) {
            ItemDescriptionEntry itemDescriptionEntry = new ItemDescriptionEntry();
            itemDescriptionEntry.setDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
            shipmentItemEntry.setItemDescription(itemDescriptionEntry);
            shipmentItemEntry.setSourceItemReferenceId("322" + LMSUtils.randomGenn(9));
            shipmentItemEntries.add(shipmentItemEntry);
        }
        shipmentPendencyEntry.setShipmentItemEntries(shipmentItemEntries);
        log.info("Creating Alteration Shipment in Lastmile");
        String payload = APIUtilities.getObjectToJSON(shipmentPendencyEntry);
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.CREATE_ALTERATION_SHIPMENT, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());

        String statusResponse = service.getResponseBody();
        return statusResponse;

    }

    public static ShipmentPendencyCreationResponse createAlterationShipment(String sourceId, String sourceRefernceId, String trackingNumber, long deliveryCenterId, ShipmentType shipmentType, ShippingMethod shippingmethod,
                                                                            String pincode, Float shipmentValue, String tenantId, String clientId, AlterationMode alterationMode, int noOfItem) throws JAXBException, IOException {
        ShipmentPendencyEntry shipmentPendencyEntry = new ShipmentPendencyEntry();
        shipmentPendencyEntry.setSourceId(sourceId);
        shipmentPendencyEntry.setSourceReferenceId(sourceRefernceId);
        shipmentPendencyEntry.setTrackingNumber(trackingNumber);
        shipmentPendencyEntry.setDeliveryCenterId(deliveryCenterId);
        shipmentPendencyEntry.setShipmentType(shipmentType);
        shipmentPendencyEntry.setShippingMethod(shippingmethod);
        shipmentPendencyEntry.setRecipientName("LMS automation");
        shipmentPendencyEntry.setAddress("AKR tech park");
        shipmentPendencyEntry.setCity("Bangalore");
        shipmentPendencyEntry.setPincode(pincode);
        shipmentPendencyEntry.setRecipientContactNumber("1234567890");
        shipmentPendencyEntry.setAlternateContactNumber("1234567890");
        shipmentPendencyEntry.setShipmentValue(shipmentValue);
        shipmentPendencyEntry.setContentsDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
        shipmentPendencyEntry.setPromiseDate(DateTimeHelper.convertTimetoEpoch(1));
        shipmentPendencyEntry.setDeleted(false);
        shipmentPendencyEntry.setRouteId("r15");
        shipmentPendencyEntry.setAddressRouteLocality("Kudlu gate");
        shipmentPendencyEntry.setTenantId(tenantId);
        shipmentPendencyEntry.setClientId(clientId);
        shipmentPendencyEntry.setSourcePath(ShipmentSourceEnum.MYNTRA);
        shipmentPendencyEntry.setAlterationMode(alterationMode);
        List<ShipmentItemEntry> shipmentItemEntries = new ArrayList();
        ShipmentItemEntry shipmentItemEntry = new ShipmentItemEntry();
        for (int i = 0; i < noOfItem; i++) {
            shipmentItemEntry.setItemDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
            shipmentItemEntry.setSourceItemReferenceId("322" + LMSUtils.randomGenn(9));
            shipmentItemEntries.add(shipmentItemEntry);
        }
        log.info("Creating Alteration Shipment in Lastmile");
        String payload = APIUtilities.getObjectToJSON(shipmentPendencyEntry);
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.CREATE_ALTERATION_SHIPMENT, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        ShipmentPendencyCreationResponse shipmentPendencyResponse = (ShipmentPendencyCreationResponse) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(),
                new ShipmentPendencyCreationResponse());
        log.info("Created Alteration Shipment in Lastmile");
        if (shipmentPendencyResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Alteration creation is failed , check if lastmile is down");
        }
        return shipmentPendencyResponse;
    }

    public static StatusResponse updateAlterationShipmentNegative(String sourceId, String sourceRefernceId, String trackingNumber, long deliveryCenterId, ShipmentType shipmentType, ShippingMethod shippingmethod,
                                                                  String pincode, Float shipmentValue, DateTime promiseDate, Boolean deleted, String tenantId, String clientId, ShipmentSourceEnum sourcePath, AlterationMode alterationMode, int noOfItem) throws JAXBException, IOException {
        ShipmentPendencyEntry shipmentPendencyEntry = new ShipmentPendencyEntry();
        shipmentPendencyEntry.setSourceId(sourceId);
        shipmentPendencyEntry.setSourceReferenceId(sourceRefernceId);
        shipmentPendencyEntry.setTrackingNumber(trackingNumber);
        shipmentPendencyEntry.setDeliveryCenterId(deliveryCenterId);
        shipmentPendencyEntry.setShipmentType(shipmentType);
        shipmentPendencyEntry.setShippingMethod(shippingmethod);
        shipmentPendencyEntry.setRecipientName("LMS automation");
        shipmentPendencyEntry.setAddress("AKR tech park");
        shipmentPendencyEntry.setCity("Bangalore");
        shipmentPendencyEntry.setPincode(pincode);
        shipmentPendencyEntry.setRecipientContactNumber("1234567890");
        shipmentPendencyEntry.setAlternateContactNumber("1234567890");
        shipmentPendencyEntry.setShipmentValue(shipmentValue);
        shipmentPendencyEntry.setContentsDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
        shipmentPendencyEntry.setPromiseDate(promiseDate);
        shipmentPendencyEntry.setDeleted(deleted);
        shipmentPendencyEntry.setRouteId("r15");
        shipmentPendencyEntry.setAddressRouteLocality("Kudlu gate");
        shipmentPendencyEntry.setTenantId(tenantId);
        shipmentPendencyEntry.setClientId(clientId);
        shipmentPendencyEntry.setSourcePath(sourcePath);
        shipmentPendencyEntry.setAlterationMode(alterationMode);
        List<LastMileShipmentItemEntry> shipmentItemEntries = new ArrayList();
        LastMileShipmentItemEntry shipmentItemEntry = new LastMileShipmentItemEntry();
        ItemDescriptionEntry itemDescriptionEntry=new ItemDescriptionEntry();
        itemDescriptionEntry.setDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
        shipmentItemEntry.setItemDescription(itemDescriptionEntry);
        shipmentItemEntry.setSourceItemReferenceId("322" + LMSUtils.randomGenn(9));
        shipmentItemEntries.add(shipmentItemEntry);
        shipmentPendencyEntry.setShipmentItemEntries(shipmentItemEntries);
        log.info("Updating Alteration Shipment in Lastmile");
        String payload = APIUtilities.getObjectToJSON(shipmentPendencyEntry);
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.CREATE_ALTERATION_SHIPMENT, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
        StatusResponse shipmentPendencyResponse = (StatusResponse) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(),
                new StatusResponse());
        log.info("Updated Alteration Shipment in Lastmile");
        return shipmentPendencyResponse;
    }


    public static ShipmentPendencyCreationResponse updateAlterationShipment(String sourceId, String sourceRefernceId, String trackingNumber, long deliveryCenterId, ShipmentType shipmentType, ShippingMethod shippingmethod,
                                                                            String pincode, Float shipmentValue, DateTime promiseDate, Boolean deleted, String tenantId, String clientId, ShipmentSourceEnum sourcePath, AlterationMode alterationMode, int noOfItem) throws JAXBException, IOException {
        ShipmentPendencyEntry shipmentPendencyEntry = new ShipmentPendencyEntry();
        shipmentPendencyEntry.setSourceId(sourceId);
        shipmentPendencyEntry.setSourceReferenceId(sourceRefernceId);
        shipmentPendencyEntry.setTrackingNumber(trackingNumber);
        shipmentPendencyEntry.setDeliveryCenterId(deliveryCenterId);
        shipmentPendencyEntry.setShipmentType(shipmentType);
        shipmentPendencyEntry.setShippingMethod(shippingmethod);
        shipmentPendencyEntry.setRecipientName("LMS automation");
        shipmentPendencyEntry.setAddress("AKR tech park");
        shipmentPendencyEntry.setCity("Bangalore");
        shipmentPendencyEntry.setPincode(pincode);
        shipmentPendencyEntry.setRecipientContactNumber("1234567890");
        shipmentPendencyEntry.setAlternateContactNumber("1234567890");
        shipmentPendencyEntry.setShipmentValue(shipmentValue);
        shipmentPendencyEntry.setContentsDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
        shipmentPendencyEntry.setPromiseDate(promiseDate);
        shipmentPendencyEntry.setDeleted(deleted);
        shipmentPendencyEntry.setRouteId("r15");
        shipmentPendencyEntry.setAddressRouteLocality("Kudlu gate");
        shipmentPendencyEntry.setTenantId(tenantId);
        shipmentPendencyEntry.setClientId(clientId);
        shipmentPendencyEntry.setSourcePath(sourcePath);
        shipmentPendencyEntry.setAlterationMode(alterationMode);
        List<LastMileShipmentItemEntry> shipmentItemEntries = new ArrayList();
        LastMileShipmentItemEntry shipmentItemEntry = new LastMileShipmentItemEntry();
        for (int i = 0; i < noOfItem; i++) {
            ItemDescriptionEntry itemDescriptionEntry=new ItemDescriptionEntry();
            itemDescriptionEntry.setDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
            shipmentItemEntry.setItemDescription(itemDescriptionEntry);
            shipmentItemEntry.setSourceItemReferenceId("322" + LMSUtils.randomGenn(9));
            shipmentItemEntries.add(shipmentItemEntry);
        }
        shipmentPendencyEntry.setShipmentItemEntries(shipmentItemEntries);
        log.info("Updating Alteration Shipment in Lastmile");
        String payload = APIUtilities.getObjectToJSON(shipmentPendencyEntry);
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.CREATE_ALTERATION_SHIPMENT, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
        ShipmentPendencyCreationResponse shipmentPendencyResponse = (ShipmentPendencyCreationResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(),
                ShipmentPendencyCreationResponse.class);
        log.info("Updated Alteration Shipment in Lastmile");
        if (shipmentPendencyResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Alteration updation is failed , check if lastmile is down");
        }
        return shipmentPendencyResponse;
    }


    public static ShipmentPendencyCreationResponse updateAlterationShipment(String sourceId, String sourceRefernceId, String trackingNumber, long deliveryCenterId, ShipmentType shipmentType, ShippingMethod shippingmethod,
                                                                            String pincode, Float shipmentValue, String tenantId, String clientId, AlterationMode alterationMode, int noOfItem) throws JAXBException, IOException {
        ShipmentPendencyEntry shipmentPendencyEntry = new ShipmentPendencyEntry();
        shipmentPendencyEntry.setSourceId(sourceId);
        shipmentPendencyEntry.setSourceReferenceId(sourceRefernceId);
        shipmentPendencyEntry.setTrackingNumber(trackingNumber);
        shipmentPendencyEntry.setDeliveryCenterId(deliveryCenterId);
        shipmentPendencyEntry.setShipmentType(shipmentType);
        shipmentPendencyEntry.setShippingMethod(shippingmethod);
        shipmentPendencyEntry.setRecipientName("LMS automation");
        shipmentPendencyEntry.setAddress("AKR tech park");
        shipmentPendencyEntry.setCity("Bangalore");
        shipmentPendencyEntry.setPincode(pincode);
        shipmentPendencyEntry.setRecipientContactNumber("1234567890");
        shipmentPendencyEntry.setAlternateContactNumber("1234567890");
        shipmentPendencyEntry.setShipmentValue(shipmentValue);
        shipmentPendencyEntry.setContentsDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
        shipmentPendencyEntry.setPromiseDate(DateTimeHelper.convertTimetoEpoch(1));
        shipmentPendencyEntry.setDeleted(false);
        shipmentPendencyEntry.setRouteId("r15");
        shipmentPendencyEntry.setAddressRouteLocality("Kudlu gate");
        shipmentPendencyEntry.setTenantId(tenantId);
        shipmentPendencyEntry.setClientId(clientId);
        shipmentPendencyEntry.setSourcePath(ShipmentSourceEnum.MYNTRA);
        shipmentPendencyEntry.setAlterationMode(alterationMode);
        List<ShipmentItemEntry> shipmentItemEntries = new ArrayList();
        ShipmentItemEntry shipmentItemEntry = new ShipmentItemEntry();
        for (int i = 0; i < noOfItem; i++) {
            shipmentItemEntry.setItemDescription("Moda Rapido Olive Green Printed Straight Kurta:Kurtas");
            shipmentItemEntry.setSourceItemReferenceId("322" + LMSUtils.randomGenn(9));
            shipmentItemEntries.add(shipmentItemEntry);
        }
        log.info("Updating Alteration Shipment in Lastmile");
        String payload = APIUtilities.getObjectToJSON(shipmentPendencyEntry);
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.CREATE_ALTERATION_SHIPMENT, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
        ShipmentPendencyCreationResponse shipmentPendencyResponse = (ShipmentPendencyCreationResponse) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(),
                ShipmentPendencyCreationResponse.class);
        log.info("Updated Alteration Shipment in Lastmile");
        if (shipmentPendencyResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Alteration Updating is failed , check if lastmile is down");
        }
        return shipmentPendencyResponse;
    }


    public static String getShipmentPendencyForDc(String tenantId, Long deliveryCenterId, ShipmentType shipmentType, List<ShipmentPendencyStatus> shipmentPendencyStatus) throws IOException, JAXBException {
        String startDate = DateTime.now().minusMinutes(5).toString("yyyy-MM-dd%20HH:mm:ss");
        String endDate = DateTime.now().toString("yyyy-MM-dd%20HH:mm:ss");
        String params = "?start=0&limit=20&shipmentType=" + shipmentType + "&tenantId=4019" + "&startDate=" + startDate + "&endDate=" + endDate;
        String pathParam = tenantId + "/deliveryCenter/" + deliveryCenterId;
        try {
            for (ShipmentPendencyStatus shipmentPendStatus : shipmentPendencyStatus) {
                params = params + "&shipmentPendencyStatus=" + shipmentPendStatus;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            params = params;
        }
        log.info("Fetching Shipment Pendency for DC");
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.GET_SHIPMENTPENDENCY_FOR_DC + pathParam, new String[]{params}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderJSON());

        log.info("Fetched Shipment Pendency for DC");
        return service.getResponseBody();
    }

    public static TripResponse updateTrip(Long tripId, TripUpdateEvent tripUpdateEvent, Long deliveryStaffId, Long deliveryCenterId, List<String> trackingNumber, String tenantId, String ClientId) throws UnsupportedEncodingException, JAXBException {

        TripEntry tripEntry = new TripEntry();
        tripEntry.setTenantId(tenantId);
        tripEntry.setId(tripId);

        String payload = APIUtilities.convertXMLObjectToString(tripEntry);
        log.info("Updating Trip ( trip service API )");
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.TRIP_UPDATE, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        TripResponse tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new TripResponse());
        log.info("Updated Trip ( trip service API )");
        if (tripResponse.getStatus().getStatusMessage().contains("Error")) {
            Assert.fail("Update Trip ( trip service API ) is failing , check if lastmile is down");
        }
        return tripResponse;
    }

    public static String updateTrip(Long tripId, TripUpdateEvent tripUpdateEvent, List<String> trackingNumber) throws Exception {
        TripResponse tripDetails = TripClient_QA.searchByTripId(tripId);
        Set<TripShipmentAssociationEntry> shipmentAssociationEntries = new HashSet<>();
        TripShipmentAssociationEntry tripShipmentAssociationEntries = new TripShipmentAssociationEntry();
        tripShipmentAssociationEntries.setDeliveryCenterId(tripDetails.getTrips().get(0).getDeliveryCenterId());
        tripShipmentAssociationEntries.setDeliveryStaffId(tripDetails.getTrips().get(0).getDeliveryStaffId());
        tripShipmentAssociationEntries.setTenantId(tripDetails.getTrips().get(0).getTenantId());
        tripShipmentAssociationEntries.setTripId(tripId);
        for (String tracking : trackingNumber) {
            tripShipmentAssociationEntries.setTrackingNumber(tracking);
        }
        shipmentAssociationEntries.add(tripShipmentAssociationEntries);
        TripUpdate tripUpdate = new TripUpdate(tripDetails.getTrips().get(0).getTenantId(), tripId, tripUpdateEvent, new DateTime(), shipmentAssociationEntries);
        String payload = APIUtilities.getObjectToJSON(tripUpdate);
        log.info("Updating Trip ( trip service API )");
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.TRIP_UPDATE, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
        log.info("Updated Trip ( trip service API )");
        return service.getResponseBody();
    }

    public static String updateShipmentPendency(long deliveryCenterId, String trackingNumber, ShipmentPendencyUpdateEvent shipmentPendencyUpdateEvent, ShipmentType shipmentType, String tenantId, String clientId, NDRStatus ndrStatus) throws IOException {
        ShipmentPendencyUpdate shipmentPendencyUpdate = new ShipmentPendencyUpdate(trackingNumber, null, deliveryCenterId, shipmentPendencyUpdateEvent, null, "Bangalore",
                DateTime.now().plusDays(1), "Update Shipment Pendency", shipmentType, ShipmentUpdateActivityTypeSource.MyntraLogistics, "userName", "rawStatusUpdate", tenantId,
                clientId, ndrStatus, "receiverName", "63478674764");
        String payload = APIUtilities.getObjectToJSON(shipmentPendencyUpdate);
        log.info("Updating Shipment Pendency ( trip service API )");
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.UPDATE_SHIPMENT_PENDENCY, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
        log.info("Updated Shipment Pendency ( trip service API )");
        return service.getResponseBody();
    }

    public static String getShipmentPendencyByShipmentTypeAndShipmentStatus(String tenantId, Long deliveryCenterId, ShipmentType shipmentType, ShipmentPendencyStatus shipmentPendencyStatus) throws UnsupportedEncodingException {
        String queryParam = "?start=0&limit=10&shipmentType=" + shipmentType + "&shipmentPendencyStatus=" + shipmentPendencyStatus;
        String path = "/" + tenantId + "/deliveryCenter/" + deliveryCenterId + queryParam;
        log.info("Get Shipment Pendency ( trip service API )");
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.GET_SHIPMENT_PENDENCY + path, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        log.info("Fetched Shipment Pendency details ( trip service API )");
        return service.getResponseBody();
    }

    //Dc to Dc Alteration
    public static TransferToDeliveryCenterShipmentUpdate reassignAlterationShipmenttoDC(String trackingNumber, Long deliveryCenterId, Long destDeliveryCenterId, String destinationLocationCode, String tenantId) throws IOException {
        TransferToDeliveryCenterShipmentUpdate transferToDeliveryCenterShipmentUpdate = new TransferToDeliveryCenterShipmentUpdate(trackingNumber, null, deliveryCenterId, MLShipmentUpdateEvent.ASSIGN_TO_OTHER_DC, null, "Bangalore",
                DateTime.now().plusDays(1), "remarks", ShipmentType.ALTERATION, ShipmentUpdateActivityTypeSource.MyntraLogistics, "user", "rawStatusUpdate", destDeliveryCenterId, tenantId, null, destinationLocationCode, "receiverName",
                "365463542564");
        String payload = APIUtilities.getObjectToJSON(transferToDeliveryCenterShipmentUpdate);
        Svc service = HttpExecutorService.executeHttpService(TripSerivceConstants.REASSIGN_SHIPMENTS_TO_DC, new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
        TransferToDeliveryCenterShipmentUpdate transferToDeliveryCenterShipmentUpdate1 = (TransferToDeliveryCenterShipmentUpdate) getDeseralizedDatesOnJsonToObjectUsingFasterXML(service.getResponseBody(), TransferToDeliveryCenterShipmentUpdate.class);
        log.info("Created Alteration Shipment in Lastmile");
//        if (transferToDeliveryCenterShipmentUpdate1.getStatus().getStatusType().toString().contains("ERROR")  ) {
//            Assert.fail("Account creation failed , Check if Lastmile service is UP");
//        }
        return transferToDeliveryCenterShipmentUpdate1;

    }

    @Test
    public void test() throws IOException {
        reassignAlterationShipmenttoDC("ML645366", 5l, 1l, "HSR", LMS_CONSTANTS.TENANTID
        );

    }

}
