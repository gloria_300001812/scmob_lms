package com.myntra.apiTests.erpservices.tripService.validator;

import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import org.testng.log4testng.Logger;

public class TripServiceValidator {
    static Logger log = Logger.getLogger(TripServiceValidator.class);
    private static StatusPollingValidator statusPollingValidator = new StatusPollingValidator();

    public static void validateAlterationShipmentcreation(String trackingNumber , String tenantId , String expectedStatus) throws InterruptedException {
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber, tenantId,expectedStatus);
        log.info("ML shipment Status is validated.");
    }

}
