package com.myntra.apiTests.erpservices.tripService.constants;

public class TripSerivceConstants {

        public static final String CREATE_ALTERATION_SHIPMENT = "tripService/shipmentPendency";
        public static final String GET_SHIPMENTPENDENCY_FOR_DC = "tripService/shipmentPendency/tenantId/";
        public static final String TRIP_UPDATE = "tripService/trip/tripShipment";
        public static final String UPDATE_SHIPMENT_PENDENCY = "tripService/shipmentPendency/updateShipmentPendency";
        public static final String GET_SHIPMENT_PENDENCY = "shipmentPendency/tenantId";
        public static final String REASSIGN_SHIPMENTS_TO_DC = "tripService/shipmentPendency/deliveryCenter";
}
