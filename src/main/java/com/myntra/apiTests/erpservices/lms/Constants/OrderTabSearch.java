package com.myntra.apiTests.erpservices.lms.Constants;

public enum OrderTabSearch {
    ORDER_ID("orderId.in:{0},{1}"),
    TRACKING_NUMBER("trackingNumber.in:{0},{1}"),
    MASTER_BAG_ID("masterbagIds={0},{1}"),
    PICKUP_ID("pickupId.in:{0},{1}"),
    TRACKING_NUMBER_WITHMB("trackingNumber={0},{1}"),
    ORDER_ID_WITHMB("orderId={0},{1}"),
    Pickup_ID_WITHMB("pickupId={0},{1}"),
    ML_TRACKING_NUMBER("trackingNumber.eq:{0}")
    ;


    private String fieldValue;

    OrderTabSearch(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getFieldValue() {
        return this.fieldValue;
    }
}
