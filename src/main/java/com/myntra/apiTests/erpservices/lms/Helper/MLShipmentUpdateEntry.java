package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateAdditionalInfo;
import org.joda.time.DateTime;

public class MLShipmentUpdateEntry {

    private String trackingNumber;
    private String eventTime;
    private Long deliveryCenterId;
    private String eventLocation;
    private String remarks;
    private MLShipmentUpdateEvent event;
    private ShipmentType shipmentType;
    private ShipmentUpdateActivityTypeSource shipmentUpdateMode;
    private String tenantId;

    private ShipmentUpdateAdditionalInfo eventAdditionalInfo;
    private  Long tripId;
    private  String userName;


    public void setTrackingNumber(String trackingNumber)
    {
        this.trackingNumber = trackingNumber;
    }

    public void setEventTime(String eventTime)
    {
        this.eventTime = eventTime;
    }

    public void setDeliveryCenterId(Long DcId)
    {
        this.deliveryCenterId = DcId;
    }

    public void setEventLocation(String eventLocation)
    {
        this.eventLocation = eventLocation;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public void setEvent(MLShipmentUpdateEvent event)
    {
        this.event = event;
    }

    public void setShipmentType(ShipmentType shipmentType)
    {
        this.shipmentType = shipmentType;
    }

    public void setTripId(Long tripId)
    {
        this.tripId = tripId;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public void setEventAdditionalInfo(ShipmentUpdateAdditionalInfo shipmentUpdateAdditionalInfo)
    {
        this.eventAdditionalInfo= shipmentUpdateAdditionalInfo;
    }

    public void setShipmentUpdateMode(ShipmentUpdateActivityTypeSource shipmentUpdateMode)
    {
        this.shipmentUpdateMode = shipmentUpdateMode;
    }

    public void setTenantId(String tenantId)
    {
        this.tenantId = tenantId;
    }
}


