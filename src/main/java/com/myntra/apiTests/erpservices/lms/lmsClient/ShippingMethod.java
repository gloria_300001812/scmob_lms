package com.myntra.apiTests.erpservices.lms.lmsClient;

/**
 * Created by Shubham Gupta on 7/21/17.
 */
public enum ShippingMethod {
	
	NORMAL("NORMAL"),
    EXPRESS("EXPRESS"),
    PRIORITY("PRIORITY"),
    SDD("SDD"),
    VALUE_SHIPPING("VALUE_SHIPPING"),
    ALL("ALL");

    private final String shippingMethod;

    private ShippingMethod(final String shippingMethod){
        this.shippingMethod = shippingMethod;
    }


    @Override
    public String toString() {
        return shippingMethod;
    }
    
}
