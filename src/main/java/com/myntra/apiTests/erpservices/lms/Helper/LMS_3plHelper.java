package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.lastmile.service.LMSOrderDetailsClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.HubResponse;
import com.myntra.lms.client.response.TransporterResponse;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.apache.log4j.Logger;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class LMS_3plHelper {
    static Logger log = Logger.getLogger(LMS_ReturnHelper.class);
    String env = getEnvironment();
    private LmsServiceHelper   lmsServiceHelper   = new LmsServiceHelper();
    private LMSHelper          lmsHelper          = new LMSHelper();
    LMSOrderDetailsClient lmsOrderDetailsClient =
            new LMSOrderDetailsClient(env, LMSConstants.DEFAULT_CLIENT_ID, LMSConstants.DEFAULT_TENANT_ID);
     LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    StatusPollingValidator statusPollingValidator=new StatusPollingValidator();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();

    public void masterBagOperationRHC(String packetId, String shippingMethod ,String source , String destination ,long originPremisesId , long destPremisesId, Long courierDC, String courier , ShipmentType shipmentType)
    throws Exception {

        TMSServiceHelper tmsServiceHelper=new TMSServiceHelper();
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        // Create a Masterbag from DH-BLR to BHU ( Myntra Regional Handover Bhubaneswar )

        Long masterBagId= masterBagServiceHelper.createMasterBag(originPremisesId , destPremisesId , ShippingMethod.valueOf(shippingMethod) , "ML" ,
                LMS_CONSTANTS.TENANTID).getId();

        // Add Shipment to Masterbag.

        lmsServiceHelper.addAndSaveMasterBag("" + packetId, "" + masterBagId, shipmentType);
        System.out.println("\nShipment is added to Masterbag :: " +masterBagId);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.ADDED_TO_MB),"Order is not moved to ADDED_TO_MB status  ");

        // Close MasterBag.

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");

        // Receive Masterbag at TH-BLR transport hub

        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(source, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");

        // Create Container from TH-BLR to BHU.

        long laneId = ((LaneResponse)tmsServiceHelper.getSupportedLanes.apply(source, destination)).getLaneEntries().get(1).getId();
        long transporterId = ((TransporterResponse)tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        ContainerResponse containerResponse = (ContainerResponse)tmsServiceHelper.createContainer.apply(source, destination, laneId, transporterId);
        long containerId = containerResponse.getContainerEntries().get(0).getId();

        // Add Masterbag to Container.

        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Error occurred during state transition");
        System.out.println("\nMasterbag "+ masterBagId+" is added to Container :: " +containerId);

        // Ship the Container.

        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "CONTAINER SHIP IS FAILING :: " +containerId);
        Thread.sleep(3000);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.SHIPPED),"Order is not moved to SHIPPED status  ");


        // Receive the Masterbag at BHU transport hub.

        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destination, masterBagId, containerId);

        // Masterbag Inscan at BHU DC.

        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.IN_TRANSIT, "Bangalore", destPremisesId, "DC");
        ExceptionHandler.handleEquals(lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2_RHD(masterBagId,trackingNum,"Bangalore",destPremisesId , shipmentType ,
                PremisesType.DC).
                getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");

        lmsServiceHelper.masterBagInScanUpdateNew(masterBagId, packetId, "Bangalore","Myntra BHU", destPremisesId, "DC",originPremisesId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT);

        ExceptionHandler.handleEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId, packetId, "Bangalore",destPremisesId
                , "DC", originPremisesId).
                getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive master bag in DC");
        ExceptionHandler.handleEquals(lmsHelper.getMasterBagStatus(masterBagId),EnumSCM.RECEIVED_AT_HANDOVER_CENTER,
                "Masterbag status is not update in DB to `RECEIVED`");


        // Create a MasterBag from BHU to Delhivery (DE) with courier DE.

        Long masterBagId1 = masterBagServiceHelper.createMasterBag(destPremisesId , courierDC, ShippingMethod.valueOf(shippingMethod), courier,LMS_CONSTANTS.TENANTID).getId();

        // Add Shipment to Masterbag.

        lmsServiceHelper.addAndSaveMasterBag("" + packetId, "" + masterBagId1, shipmentType);
        System.out.println("\nShipment is added to Masterbag :: " +masterBagId1);

        // Close Masterbag.

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId1).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag ::" + masterBagId1 );
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER),"Order is not moved to RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER status , Check whether FG is enabled . ( Should lms.enable_rhc_v2 = TRUE )");


        // Ship the Masterbag at Delhivery (DE)

        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId1).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Shipping masterbag is failed ::" +masterBagId1);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.SHIPPED),"Order is not moved to SHIPPED status , Check whether FG is enabled . ( Should lms.enable_rhc_v2 = TRUE )");
        System.out.println("Tracking Number With Courier DE :: " + trackingNum);

    }

    public void updateAndVerifyShipmentStatus3pl(String packetId,ShipmentUpdateEvent shipmentUpdateEvent ,ShipmentType shipmentType,String courierCode)
            throws IOException, JAXBException, InterruptedException {
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        lmsServiceHelper.updateShipmentfor3PL(trackingNum, shipmentUpdateEvent, courierCode, shipmentType);
        verifyShipmentStatus3pl(packetId,shipmentUpdateEvent);

    }

    public void verifyShipmentStatus3pl(String packetId, ShipmentUpdateEvent shipmentUpdateEvent) throws InterruptedException {
        LMSOrderDetailsClient lmsOrderDetailsClient=new LMSOrderDetailsClient(env , LMSConstants.DEFAULT_CLIENT_ID ,

                LMSConstants.DEFAULT_TENANT_ID);
        log.info("Validate shipment Status in LMS 3pl");
        String status_code=lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(
                0).getPlatformShipmentStatus().name();
        statusPollingValidator.validateOrderStatus(packetId, shipmentUpdateEvent.toString());
        Assert.assertEquals(status_code,shipmentUpdateEvent.name(),status_code+"is expected but shipmentUpdateEvent is not as expected"+shipmentUpdateEvent.name());
    }

    public String getHubCodeByPremiseId(Long premiseId) throws Exception {
        LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
        String premisesHubCode = null;
        HubResponse hubResponse = lmsServiceHelper.searchHubById(premiseId);
        if(hubResponse.getHub().size()>0){
            premisesHubCode = hubResponse.getHub().get(0).getCode();
        } else
        {
            DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCDetails(""+premiseId);
            premisesHubCode = deliveryCenterResponse.getDeliveryCenters().get(0).getDeliveryHubCode();
        }
        return premisesHubCode;
    }


}
