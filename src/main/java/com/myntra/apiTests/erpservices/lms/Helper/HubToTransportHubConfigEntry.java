package com.myntra.apiTests.erpservices.lms.Helper;

/**
 * @author Bharath.MC
 * @since Mar-2019
 */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import com.myntra.commons.entries.BaseEntry;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(
        name = "hubToTransportHubConfigEntry"
)
public class HubToTransportHubConfigEntry extends BaseEntry {
    private String hubCode;
    private String transportHubCode;
    private Boolean isColocated;
    private String tenantId;

    public HubToTransportHubConfigEntry() {
    }

    public String getHubCode() {
        return this.hubCode;
    }

    public String getTransportHubCode() {
        return this.transportHubCode;
    }

    public Boolean getIsColocated() {
        return this.isColocated;
    }

    public String getTenantId() {
        return this.tenantId;
    }

    public void setHubCode(String hubCode) {
        this.hubCode = hubCode;
    }

    public void setTransportHubCode(String transportHubCode) {
        this.transportHubCode = transportHubCode;
    }

    public void setIsColocated(Boolean isColocated) {
        this.isColocated = isColocated;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof HubToTransportHubConfigEntry)) {
            return false;
        } else {
            HubToTransportHubConfigEntry other = (HubToTransportHubConfigEntry)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label59: {
                    Object this$hubCode = this.getHubCode();
                    Object other$hubCode = other.getHubCode();
                    if (this$hubCode == null) {
                        if (other$hubCode == null) {
                            break label59;
                        }
                    } else if (this$hubCode.equals(other$hubCode)) {
                        break label59;
                    }

                    return false;
                }

                Object this$transportHubCode = this.getTransportHubCode();
                Object other$transportHubCode = other.getTransportHubCode();
                if (this$transportHubCode == null) {
                    if (other$transportHubCode != null) {
                        return false;
                    }
                } else if (!this$transportHubCode.equals(other$transportHubCode)) {
                    return false;
                }

                Object this$isColocated = this.getIsColocated();
                Object other$isColocated = other.getIsColocated();
                if (this$isColocated == null) {
                    if (other$isColocated != null) {
                        return false;
                    }
                } else if (!this$isColocated.equals(other$isColocated)) {
                    return false;
                }

                Object this$tenantId = this.getTenantId();
                Object other$tenantId = other.getTenantId();
                if (this$tenantId == null) {
                    if (other$tenantId != null) {
                        return false;
                    }
                } else if (!this$tenantId.equals(other$tenantId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof HubToTransportHubConfigEntry;
    }


    public String toString() {
        return "HubToTransportHubConfigEntry(hubCode=" + this.getHubCode() + ", transportHubCode=" + this.getTransportHubCode() + ", isColocated=" + this.getIsColocated() + ", tenantId=" + this.getTenantId() + ")";
    }
}