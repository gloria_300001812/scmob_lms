package com.myntra.apiTests.erpservices.lms.Flipkart;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Flipkart.Domain.ScanAndSortResponse;
import com.myntra.apiTests.erpservices.lms.Flipkart.Domain.ShipmentItems;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.platform.domain.Premise;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import lombok.SneakyThrows;
import org.testng.Assert;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FlipkartHelper {

    /**
     * create order for FLIPKART till packed state
     * @param courierCode
     * @param pincode
     * @param tenantId
     * @param warehouseId
     * @param shippingMethod
     * @return
     */
    @SneakyThrows
    public String createFlipKartOrder(String courierCode, String pincode, String tenantId,
                                      String warehouseId, ShippingMethod shippingMethod) {

        int sourceRefId = 100000000 + new Random().nextInt(900000000);

        try {
            com.myntra.apiTests.erpservices.lms.Flipkart.Domain.Shipment shipment = new com.myntra.apiTests.erpservices.lms.Flipkart.Domain.Shipment();
            shipment.setSourceReferenceId(String.valueOf(sourceRefId));
            shipment.setTrackingNumber("MLFF" + sourceRefId);
            shipment.setCourierCode(courierCode);
            shipment.setMrpTotal("138.00");
            shipment.setPromiseDate(DateTimeHelper.generateDate("yyyy-MM-dd", 0));
            shipment.setPackageWeight("0.3");
            shipment.setPackageBreadth("18");
            shipment.setPackageLength("5");
            shipment.setPackageHeight("2");
            shipment.setShipmentValue("138");
            shipment.setShipmentType(ShipmentType.DL.toString());
            shipment.setRtoWarehouseId("1122");
            shipment.setRecipientName("RahulJangid");
            shipment.setRecipientContactNumber("9466365603");
            shipment.setAlternateContactNumber("");
            shipment.setRecipientAddress("4/240 kala kuan housing board, alwarKala kuan housing board 301001 AlwarRajasthan");
            shipment.setCodAmount("0.0");
            shipment.setIsFragile("false");
            shipment.setIsHazmat("false");
            shipment.setIsLarge("false");
            shipment.setLandmark("");
            shipment.setEmail("anup.dutta.jrt@gmail.com");
            shipment.setPackedDate(DateTimeHelper.generateDate("yyyy-MM-dd HH:mm:ss", -5));
            shipment.setContentsDescription("electronic_hobby_kit, electronic_hobby_kit, electronic_hobby_kit, electronic_hobby_kit");
            shipment.setShippingMethod(String.valueOf(shippingMethod));
            shipment.setContainsFootwear("false");
            shipment.setContainsJewellery("false");
            shipment.setLocality("Jorhat");
            shipment.setWarehouseId(warehouseId);
            shipment.setTenantId(tenantId);
            shipment.setPincode(pincode);
            shipment.setMultiSellerShipment("false");

            ShipmentItems shipmentItem = new ShipmentItems();
            shipmentItem.setStateGoodsAndServiceTax("10.53");
            shipmentItem.setIntegratedGoodsAndServiceTax("0");
            shipmentItem.setCentralGoodsAndServiceTax("10.53");
            shipmentItem.setSourceItemReferenceId("11556451622932900");
            shipmentItem.setImageURL("");
            shipmentItem.setItemBarcode("");
            shipmentItem.setSellerName("The Ace Look");
            shipmentItem.setGoodsAndServiceTaxIdentificationNumber("08ALWPY5945R1ZF");
            shipmentItem.setSellerAddress("W7 Near Chokdi Ki DharamShala,Bajaj Road, SikarSIKARRAJASTHAN332001");
            shipmentItem.setItemDescription("MobiMonk Back Cover for Samsung Galaxy A10");
            shipmentItem.setSellerId(LMS_CONSTANTS.FLIPKART_SELLER_ID);

            List<com.myntra.apiTests.erpservices.lms.Flipkart.Domain.ShipmentItems> lstShipmentItem = new ArrayList<>();
            lstShipmentItem.add(shipmentItem);

            shipment.setShipmentItems(new ShipmentItems[]{shipmentItem});
            System.out.println(shipment);
            String payload = APIUtilities.getObjectToJSON(shipment);
            int retryCount = 3;
            Svc service = null;
            //TODO made this do-while loop for avoid failure, first time this api is giving 500 Internal Server Error
            do {
                service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.CREATESHIPMENT, null, SERVICE_TYPE.PARTNER_API.toString(),
                        HTTPMethods.POST, payload, Headers.createPartnerConnectServiceHeaders());
                if (retryCount-- <= 0)
                    break;
            } while (!(service.getResponseStatus() == 200));

            Assert.assertEquals(APIUtilities.getElement(service.getResponseBody(), "status.statusMessage", "json"), "Shipment Creation Successful", "Flipkart order is not created");
        } catch (IOException e) {
            Assert.fail("FAILURE:: Flip kart order creation failed", e.getCause());
        }

        return String.valueOf(sourceRefId);
    }


    /**
     * scan the packet in hub using scanandsort
     *
     * @param tenantId
     * @param trackingNumber
     * @param location
     * @param premiseType
     * @param forceInscan
     * @return
     */
    @SneakyThrows
    public ScanAndSortResponse scanAndSort(String tenantId, String trackingNumber, String location, Premise.PremiseType premiseType, boolean forceInscan) {
        String pathParm = MessageFormat.format("{0}/scanAndSort/{1}?location={2}&premiseType={3}&sortLevel=1&forceScan={4}",
                tenantId, trackingNumber, location, premiseType, forceInscan);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.SCAN_AND_SORT, new String[]{pathParm}, SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.PUT, null, Headers.getLmsHeaderJSON());
        ScanAndSortResponse sortationConfigResponse = (ScanAndSortResponse) APIUtilities.getJsontoObject(service.getResponseBody(), new ScanAndSortResponse());
        return sortationConfigResponse;
    }
}
