package com.myntra.apiTests.erpservices.lms.validators;

import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClient_QA;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.client.MLShipmentClient;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.NDRStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponse;
import com.myntra.apiTests.erpservices.Constants;
import org.testng.Assert;

public class NDRValidator {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NDRValidator.class);
    static LMSHelper lmsHelper = new LMSHelper();

    public static void ValidateMLShipmentResponse(MLShipmentResponse mlShipmentResponse, String statusType, String statusMessage, String statusCode) {
        Assert.assertEquals(mlShipmentResponse.getStatus().getStatusMessage().trim(), statusMessage, "Status massage not correct");
        Assert.assertEquals(mlShipmentResponse.getStatus().getStatusType().toString(), statusType, "Status type is not correct");
        if (statusCode != null) {
            Assert.assertEquals(mlShipmentResponse.getStatus().getStatusCode(), Integer.parseInt(statusCode), "Status Code mismatch");
            log.debug("NDR Status after failled Delivery for Packet:- " + mlShipmentResponse.getMlShipmentEntries().get(0).getNdrStatus());
        }
    }

    public static void ValidateShipmentUpdateResponse(ShipmentUpdateResponse shipmentUpdateResponse, String Event, String eventLocation, String TrackingNumber, NDRStatus NdrStatus, ShipmentType shipmentType, String courierCode, String ShipmentStatusPostEvent) {
        Assert.assertEquals(shipmentUpdateResponse.getShipmentUpdate().getEvent().toString(), Event, "Error In NDR Status");
        Assert.assertEquals(shipmentUpdateResponse.getShipmentUpdate().getEventLocation(), eventLocation, "Event Location Data Error");
        Assert.assertEquals(shipmentUpdateResponse.getShipmentUpdate().getTrackingNumber(), TrackingNumber, "Check Some issue is in tracking Number");
        Assert.assertEquals(shipmentUpdateResponse.getShipmentUpdate().getNdrStatus(), NdrStatus, "Error In NDR Status");
        Assert.assertEquals(shipmentUpdateResponse.getShipmentUpdateResponseCode().toString(), "SUCCESS", "Error in Response Code");
        Assert.assertEquals(shipmentUpdateResponse.getShipmentType(), shipmentType, "Error in ShipmentType");
        Assert.assertEquals(shipmentUpdateResponse.getCourierCode(), courierCode, "Error in Courier Code");
        Assert.assertEquals(shipmentUpdateResponse.getShipmentStatusPostEvent().toString(), ShipmentStatusPostEvent, "Error in Shipment Status");
    }

    public static void ValidateNDRStatusForShipment(String packetId, String errorMessage) throws InterruptedException {
        MLShipmentResponse mlShipmentResponse = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(lmsHelper.getTrackingNumber(packetId));
        ValidateMLShipmentResponse(mlShipmentResponse, "SUCCESS", "Success", "3");
        Assert.assertNull(mlShipmentResponse.getMlShipmentEntries().get(0).getNdrStatus(), errorMessage);
    }
    public static void ValidateNDRbaseShipmentResponse(ShipmentUpdateResponse shipmentUpdateResponse,String TrackingNumber, NDRStatus NdrStatus, ShipmentType shipmentType, String courierCode, String ShipmentStatusPostEvent,String shipmentUpdateMode) {
        Assert.assertEquals(shipmentUpdateResponse.getShipmentUpdate().getTrackingNumber(), TrackingNumber, "Check Some issue is in tracking Number");
        Assert.assertEquals(shipmentUpdateResponse.getShipmentUpdate().getNdrStatus(), NdrStatus, "Error In NDR Status");
        Assert.assertEquals(shipmentUpdateResponse.getShipmentUpdateResponseCode().toString(), "SUCCESS", "Error in Response Code");
        Assert.assertEquals(shipmentUpdateResponse.getShipmentType(), shipmentType, "Error in ShipmentType");
        Assert.assertEquals(shipmentUpdateResponse.getCourierCode(), courierCode, "Error in Courier Code");
        Assert.assertEquals(shipmentUpdateResponse.getShipmentStatusPostEvent().toString(), ShipmentStatusPostEvent, "Error in Shipment Status");
        Assert.assertEquals(shipmentUpdateResponse.getShipmentUpdateMode().toString(), shipmentUpdateMode, "Error in Shipment Update mode");
    }
    public static void ValidateShipmentThroughNDRConfig(String packetId, String errorMessage) throws InterruptedException {
        MLShipmentResponse mlShipmentResponse = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(lmsHelper.getTrackingNumber(packetId));
        ValidateMLShipmentResponse(mlShipmentResponse, "SUCCESS", "Success", "3");
        Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getNdrStatus(),NDRStatus.IN_PROGRESS,errorMessage);
    }

}
