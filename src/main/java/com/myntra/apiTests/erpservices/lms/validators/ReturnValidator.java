package com.myntra.apiTests.erpservices.lms.validators;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.ReturnHelper;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.client.MLShipmentClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.entries.ShipmentActivityDetailEntry;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.*;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

@Slf4j
public class ReturnValidator {
    String env=getEnvironment();
    String clientId=LMSConstants.DEFAULT_CLIENT_ID;
    String tenantId=LMSConstants.DEFAULT_TENANT_ID;
    private LmsServiceHelper lmsServiceHelper=new LmsServiceHelper();
    private RMSServiceHelper rmsServiceHelper=new RMSServiceHelper();
    StatusPollingValidator statusPollingValidator=new StatusPollingValidator();

    //    @Step("validate given variable is null or not")
    public void isNull(String variable) {
        if (variable == null) {
            Assert.fail("variable value is null");
        }
    }

    //    @Step("Validate ML shipment status for Dc and Store")
    public void validateMLShipmentStatus(String trackingNo,String expectedStatus) throws InterruptedException, JAXBException, IOException {
        MLShipmentClient mlShipmentClient=new MLShipmentClient();
        MLShipmentClient_QA mlShipmentClient_qa=new MLShipmentClient_QA();

        final MLShipmentResponse mlShipmentResponse=mlShipmentClient_qa.getMLShipmentDetailsByTrackingNumber(trackingNo);
        if (!(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus() == null)) {
            Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(),expectedStatus,
                    "Ml shipment status is not matching");
        } else {
            Assert.fail("MLShipment Response is null");
        }

    }

    public void validateMLShipmentStatusFail(String trackingNo,String expectedStatus) {
        MLShipmentClient mlShipmentClient=new MLShipmentClient();
        MLShipmentClient_QA mlShipmentClient_qa=new MLShipmentClient_QA();

        final MLShipmentResponse mlShipmentResponse=mlShipmentClient_qa.getMLShipmentDetailsByTrackingNumber(trackingNo);
        if (!(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus() == null)) {
            Assert.assertNotEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(),expectedStatus,
                    "Ml shipment status is not matching");
        } else {
            Assert.fail("MLShipment Response is null");
        }

    }

    //    @Step("Validate Pickup shipment status for Dc and Store")
    public void validatePickUpShipmentStatus(String returnId,com.myntra.logistics.platform.domain.ShipmentStatus pickupStatus) throws IOException, InterruptedException, JAXBException {
        String pickUpShipmentStatus=lmsServiceHelper.findPickupFromSourceReturnId(returnId,tenantId,
                Long.valueOf(clientId));
        Assert.assertEquals(pickUpShipmentStatus,pickupStatus.name(),
                "The pickup is not created in "+pickupStatus.name()+" status in LMS");
    }

    //    @Step("Validate Return shipment status for Dc and Store")
    public void validateReturnShipmentStatus(String returnId,OrderStatus orderStatus,com.myntra.logistics.platform.domain.ShipmentStatus shipmentStatus)
            throws UnsupportedEncodingException, JAXBException {
        final OrderResponse returnsInLMS=lmsServiceHelper.getReturnsInLMS(returnId);
        Assert.assertEquals(returnsInLMS.getOrders().get(0).getStatus().name(),orderStatus.name(),
                "The return is not in "+orderStatus.name()+" status in LMS");
//        Assert.assertNotNull(returnsInLMS.getOrders().get(0).getShipmentStatus(), "shipment status is null, some issue with API");
//        Assert.assertTrue(returnsInLMS.getOrders().get(0).getShipmentStatus().name().equals(shipmentStatus.name()), "The return is not in " + shipmentStatus.name() + " status in LMS");
//        APIUtilities.getElement(service.getResponseBody(), "data.shipmentStatus", "json");
    }

    //    @Step("Validate Return shipment status for Dc and Store")
    public void validateReturnShipmentStatus(String returnId,OrderStatus orderStatus)
            throws IOException, JAXBException, InterruptedException {
        Thread.sleep(3000);
        final OrderResponse returnsInLMS=lmsServiceHelper.getReturnsInLMS(returnId);
        Assert.assertEquals(returnsInLMS.getOrders().get(0).getStatus().name(),orderStatus.name(),
                "The return is not in "+orderStatus.name()+" it is in "+returnsInLMS.getOrders().get(
                        0).getStatus().name()+" status in LMS");
    }

    //@Step("Validate Return shipment status cc approval")
    public void validateReturnShipmentCCApproval(String returnId,PickupApprovalFlag approvalFlag)
            throws IOException, JAXBException, InterruptedException {
        final OrderResponse returnsInLMS=lmsServiceHelper.getReturnsInLMS(returnId);
        Assert.assertTrue(returnsInLMS.getOrders().get(0).getApprovalStatus().name().equals(approvalFlag.name()),
                "The return is not in "+approvalFlag.name()+" status in LMS");
    }

    //    @Step("validate order tracking")
    public void validateOrderTracking(String trackingNo, String clientId, String tenantId, CourierCode courierCode, DeliveryStatus deliveryStatus) throws IOException, JAXBException {
        ReturnHelper returnHelper=new ReturnHelper();
        OrderTrackingResponse orderTracking=returnHelper.getOrderTrackingDetail(courierCode,trackingNo,clientId,
                tenantId);
        Assert.assertEquals(orderTracking.getOrderTrackings().get(0).getDeliveryStatus().name(),deliveryStatus.name(),
                "The return is not in "+deliveryStatus.name()+" status in LMS");
    }

    //    @Step("Validate order tracking details status for Dc and Store")
    public void validateOrderTrackingDetailsStatus(String trackingNo,String activityType,String fromStatus,String toStatus)
            throws IOException, JAXBException {
        ReturnHelper returnHelper=new ReturnHelper();
        OrderTrackingResponseV3 orderTrackingResponseV3=returnHelper.getBulkOrderTrackingDetails(trackingNo);
        List <OrderTrackingDetailEntryV2> orderTrackingDetails=orderTrackingResponseV3.getOrderTrackingEntries().get(
                0).getOrderTrackingDetails();
        for (int i=0; i < orderTrackingDetails.size(); i++) {
            if (activityType.equalsIgnoreCase(orderTrackingDetails.get(i).getActivityType())) {
                Assert.assertEquals(orderTrackingDetails.get(i).getFromStatus(),fromStatus,"from status did not match");
                Assert.assertEquals(orderTrackingDetails.get(i).getToStatus(),toStatus,"from status did not match");
            }
        }
    }

    //    @Step("Validate trip order assignment status")
    public void validateTripOrderAssignment(Long tripId,ShipmentType shipmentType,String tripOrderStatus) throws UnsupportedEncodingException, JAXBException {
        LastmileClient lastmileClient=new LastmileClient();

        TripOrderAssignmentResponse ordersByTrip=lastmileClient.findOrdersByTrip(tripId,shipmentType);
        Assert.assertEquals(ordersByTrip.getTripOrders().get(0).getTripOrderStatus().toString(),(tripOrderStatus),
                "The return is not in "+tripOrderStatus+" status in LMS");
    }

    //@Step("Validate trip order assignment status")
    public void validateTripOrderAssignment(Long tripId,ShipmentType shipmentType,TripOrderStatus tripOrderStatus,String attemptReasonCode) throws UnsupportedEncodingException, JAXBException {
        String reasonCode=attemptReasonCode;
        if (attemptReasonCode.equalsIgnoreCase(EnumSCM.NOT_ABLE_TO_PICKUP)) {
            reasonCode=AttemptReasonCode.CANNOT_PICKUP.name();
        } else if (attemptReasonCode.equalsIgnoreCase(EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING)) {
            reasonCode=AttemptReasonCode.PICKUP_SUCCESSFUL_QC_PENDING.name();
        } else if (attemptReasonCode.equalsIgnoreCase(EnumSCM.RETURNS_CANCELLATION)) {
            reasonCode=AttemptReasonCode.OTHERS.name();
        } else if (attemptReasonCode.equalsIgnoreCase(EnumSCM.RETURN_REJECTED)) {
            reasonCode=AttemptReasonCode.RETURN_QC_FAIL.name();
        } else if (attemptReasonCode.equalsIgnoreCase(EnumSCM.ON_HOLD_DAMAGED_PRODUCT)) {
            reasonCode=AttemptReasonCode.PICKUP_ON_HOLD_DAMAGED_PRODUCT.name();
        } else if (attemptReasonCode.equalsIgnoreCase(EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE)) {
            reasonCode=AttemptReasonCode.REQUESTED_RE_SCHEDULE.name();
        } else if (attemptReasonCode.equals(EnumSCM.LOST)) {
            reasonCode=AttemptReasonCode.OTHERS.name();
        } else if (attemptReasonCode.equals(EnumSCM.FAILED)) {
            reasonCode=AttemptReasonCode.REQUESTED_RE_SCHEDULE.name();
        } else if (attemptReasonCode.equals(EnumSCM.HAPPY_WITH_PRODUCT)) {
            reasonCode=AttemptReasonCode.HAPPY_WITH_PRODUCT.name();
        }

        LastmileClient lastmileClient=new LastmileClient();
        TripOrderAssignmentResponse ordersByTrip=lastmileClient.findOrdersByTrip(tripId,shipmentType);
        if (ordersByTrip.getTripOrders() != null) {
            Assert.assertEquals(ordersByTrip.getTripOrders().get(0).getTripOrderStatus().toString(),
                    (tripOrderStatus.name()),"The return is not in "+tripOrderStatus.name()+" status in LMS");
            if (ordersByTrip.getTripOrders().get(0).getAttemptReasonCode() != null) {
                Assert.assertEquals(ordersByTrip.getTripOrders().get(0).getAttemptReasonCode().toString(),(reasonCode),
                        "The attempt reason code is not in "+attemptReasonCode+" status in LMS");
            }
        } else Assert.fail("TripOrder Assignment Entry Response is null");
    }

    //    @Step("Validate trip order assignment isRecieved status")
    public void validateTripOrderAssignmentIsReceived(Long tripId,ShipmentType shipmentType,boolean isReceived) throws UnsupportedEncodingException, JAXBException {
        LastmileClient lastmileClient=new LastmileClient();

        TripOrderAssignmentResponse ordersByTrip=lastmileClient.findOrdersByTrip(tripId,shipmentType);
        if (!isReceived) {
            Assert.assertNull(ordersByTrip.getTripOrders().get(0).getIsReceived(),"order is not received yet");
        } else
            Assert.assertEquals(ordersByTrip.getTripOrders().get(0).getIsReceived().toString(),"true",
                    "The return is not in received status in LMS");
    }

    //    @Step("validate shipment activity ")
    public void validateShipmentActivityDetails(String returnId,ShipmentType shipmentType,String activityType,String fromStatus,String toStatus) throws IOException, JAXBException, JSONException, XMLStreamException {
        ReturnHelper returnHelper=new ReturnHelper();
        ShipmentActivityResponse shipmentActivityDetails=returnHelper.getShipmentActivityDetails(returnId,shipmentType);
        ShipmentActivityDetailEntry shipmentActivityDetailEntry=shipmentActivityDetails.getShipmentActivityEntry().getShipmentActivityDetails().get(
                0);

        Assert.assertEquals(shipmentActivityDetailEntry.getActivityType(),activityType,
                "activity type does not match, actual is"+shipmentActivityDetailEntry.getActivityType());
        Assert.assertEquals(shipmentActivityDetailEntry.getFromStatus(),fromStatus,
                "from status does not match, actual is"+shipmentActivityDetailEntry.getFromStatus());
        Assert.assertEquals(shipmentActivityDetailEntry.getToStatus(),toStatus,
                "from status does not match, actual is"+shipmentActivityDetailEntry.getToStatus());

    }


    //    @Step("Validate Master Bag status")
    public void validateMasterBagStatus(ShipmentResponse shipmentResponse,ShipmentStatus expectedStatus) throws InterruptedException {
        if (!(shipmentResponse.getEntries().isEmpty())) {
            Assert.assertEquals(shipmentResponse.getEntries().get(0).getStatus(),expectedStatus,
                    "MasterBag status is not expected");
        } else {
            Assert.fail("Shipment entries was empty");
        }
    }


    //    @Step("validate trip status")
    public void validateTripStatus(Long tripId,TripStatus tripStatus) {
        LastmileClient lastmileClient=new LastmileClient();
        //validate trip status
        TripResponse tripResponse=lastmileClient.getTripDetailsByTripId(String.valueOf(tripId));

        if (!(tripResponse.getTrips().get(0).getTripStatus() == null)) {
            Assert.assertEquals(tripResponse.getTrips().get(0).getTripStatus().toString(),tripStatus.toString(),
                    "Trip status after trip order assignment not correct.");
        } else {
            Assert.fail("Trip response was null");
        }

    }

    //    @Step("Validate return order status in RMS")
    public void validateReturnOrderStatusInRMS(ReturnResponse returnResponse,ReturnStatus returnStatus) {
        if (!(returnResponse.getData().get(0).getStatusCode() == null)) {
            Assert.assertEquals(returnResponse.getData().get(0).getStatusCode().toString(),returnStatus.toString(),
                    "Return order status is not expected");
        } else {
            Assert.fail("Return response was null");
        }

    }

    //    @Step("Validate return order status in LMS")
    public void validateReturnOrderStatusInLMS(com.myntra.lms.client.domain.response.ReturnResponse returnResponse,com.myntra.lastmile.client.status.ShipmentStatus returnStatus) {
        if (!(returnResponse.getDomainReturnShipments().get(0).getShipmentStatus() == null)) {
            Assert.assertEquals(returnResponse.getDomainReturnShipments().get(0).getShipmentStatus().toString(),
                    returnStatus.toString(),"Return order status is not expected");
        } else {
            Assert.fail("Return response was null");
        }
    }


    //    @Step("validate PQCP")
    public void validatePQCP(String returnId,String trackingNo,Long tripId) throws IOException, JAXBException, InterruptedException {
        //validating return status in rms
        ReturnResponse returnResponse=rmsServiceHelper.getReturnDetailsNew(returnId);
        validateReturnOrderStatusInRMS(returnResponse,ReturnStatus.RL);
        // todo OpenBoxReturnForAlphaSeller_SDA_PQCP_BeforeTripComplete_DC_OnHold_CC_Reject_AfterTripComplete ----getting LPI state

        //validating tripOrderAssignment
        validateTripOrderAssignment(tripId,ShipmentType.PU,com.myntra.lastmile.client.status.TripOrderStatus.PS,
                AttemptReasonCode.PICKUP_SUCCESSFUL_QC_PENDING.name());

        //validating return shipment
        validateReturnShipmentStatus(returnId,OrderStatus.PQCP,
                com.myntra.logistics.platform.domain.ShipmentStatus.PICKUP_DONE);

        // validate pickup shipment
        validatePickUpShipmentStatus(returnId,com.myntra.logistics.platform.domain.ShipmentStatus.PICKUP_DONE);

        // validate order tracking details todo check the pojo
        validateOrderTrackingDetailsStatus(trackingNo,"PICKUP_DONE","OUT_FOR_PICKUP","PICKUP_DONE");

        // validate order tracking
        validateOrderTracking(trackingNo,clientId,tenantId,CourierCode.ML,DeliveryStatus.PQCP);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.PICKED_UP);

        //validating shipment activity details
//        validateShipmentActivityDetails(returnId,ShipmentType.RETURN,"PICKUP_DONE","RETURN_CREATED","PICKUP_DONE");

    }

    //    @Step("validate PQCP preApprove")
    public void validatePQCPpreApproved(String returnId,String trackingNo,Long tripId) throws IOException, JAXBException, InterruptedException {
        //validating return status in rms
        ReturnResponse returnResponse=rmsServiceHelper.getReturnDetailsNew(returnId);
        validateReturnOrderStatusInRMS(returnResponse,ReturnStatus.RL);

        //validating tripOrderAssignment
        validateTripOrderAssignment(tripId,ShipmentType.PU,com.myntra.lastmile.client.status.TripOrderStatus.PS,
                AttemptReasonCode.PICKUP_SUCCESSFUL_QC_PENDING.name());

        //validating return shipment
        validateReturnShipmentStatus(returnId,OrderStatus.RT);

        // validate pickup shipment
        validatePickUpShipmentStatus(returnId,com.myntra.logistics.platform.domain.ShipmentStatus.PICKUP_SUCCESSFUL);

        // validate order tracking details todo check the pojo
        validateOrderTrackingDetailsStatus(trackingNo,"PICKUP_SUCCESSFUL","OUT_FOR_PICKUP","PICKUP_SUCCESSFUL");

        // validate order tracking
//        validateOrderTracking(trackingNo,clientId,tenantId,CourierCode.ML,DeliveryStatus.RT);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.PICKED_UP);

        //validating shipment activity details
//        validateShipmentActivityDetails(returnId,ShipmentType.RETURN,"PICKUP_DONE","RETURN_CREATED","PICKUP_DONE");

    }


    //    @Step("validate Trip complete")
    public void validateTripComplete(String status,String trackingNo,Long tripId) throws IOException, JAXBException, InterruptedException {

        //validating tripOrderAssignment status and attempt reason code
        validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.PS,status);

        // validate order tracking
        validateOrderTracking(trackingNo,clientId,tenantId,CourierCode.ML,DeliveryStatus.RT);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.PICKUP_SUCCESSFUL);
    }

    public void validateTripCompleteCCReject(String status,String trackingNo,Long tripId) throws IOException, JAXBException, InterruptedException {

        //validating tripOrderAssignment status and attempt reason code
        validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.OHP,status);

        // validate order tracking
        validateOrderTracking(trackingNo,clientId,tenantId,CourierCode.ML,DeliveryStatus.REJ);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.RETURN_REJECTED);
    }

    public void validateTripCompleteFailedPickup(String status,String trackingNo,Long tripId) throws IOException, JAXBException, InterruptedException {

        //validating tripOrderAssignment status and attempt reason code
        validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.FP,status);

        // validate order tracking
        validateOrderTracking(trackingNo,clientId,tenantId,CourierCode.ML,DeliveryStatus.FP);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.ASSIGNED_TO_DC);
    }

    public void validateTripCompleteRejectOnHold(String status,String trackingNo,Long tripId) throws IOException, JAXBException, InterruptedException {

        //validating tripOrderAssignment status and attempt reason code
        validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.OHP,status);

        // validate order tracking
        validateOrderTracking(trackingNo,clientId,tenantId,CourierCode.ML,DeliveryStatus.REJ);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.REJECTED_ONHOLD_PICKUP_WITH_DC);
    }

    //    @Step("validate Trip complete")
    public void validateTripCompletePQCPRecevedInDC(String status,String trackingNo,Long tripId) throws IOException, JAXBException, InterruptedException {

        //validating tripOrderAssignment status and attempt reason code
        validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.PS,status);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.RECEIVED_IN_DC);
    }

    public void validateTripCompletePQCP(String status,String trackingNo,Long tripId) throws IOException, JAXBException, InterruptedException {

        //validating tripOrderAssignment status and attempt reason code
        validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.PS,status);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.PICKUP_SUCCESSFUL);
    }

    public void validateTripCompletePQCPCCApprove(String status,String trackingNo,Long tripId) throws IOException, JAXBException, InterruptedException {

        //validating tripOrderAssignment status and attempt reason code
        validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.PS,status);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.PICKUP_SUCCESSFUL);
    }

    //    @Step("validate Trip complete")
    public void validateTripCompletePickupFailed(String attemptReasonCode,String trackingNo,Long tripId) throws IOException, JAXBException, InterruptedException {

        //validating tripOrderAssignment status and attempt reason code
        validateTripOrderAssignment(tripId,ShipmentType.PU,com.myntra.lastmile.client.status.TripOrderStatus.FP,
                attemptReasonCode);

        // validate order tracking
        validateOrderTracking(trackingNo,clientId,tenantId,CourierCode.ML,DeliveryStatus.FP);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.ASSIGNED_TO_DC);
    }


    public void validateTripCompletePickupOnHold(String attemptReasonCode,String trackingNo,Long tripId) throws IOException, JAXBException, InterruptedException {

        //validating tripOrderAssignment status and attempt reason code
        validateTripOrderAssignment(tripId,ShipmentType.PU,com.myntra.lastmile.client.status.TripOrderStatus.OHP,
                attemptReasonCode);

        // validate order tracking
        validateOrderTracking(trackingNo,clientId,tenantId,CourierCode.ML,DeliveryStatus.OHP);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER);
    }


    //    @Step("validate OnHold PickUp With DC")
    public void validateOnHoldPickUpWithDC(String returnId,String trackingNo,Long tripId,String attemptReasonCode) throws IOException, JAXBException, InterruptedException {
        //validating return status in rms
        ReturnResponse returnResponse=rmsServiceHelper.getReturnDetailsNew(returnId);
        validateReturnOrderStatusInRMS(returnResponse,ReturnStatus.RL);

        //validating tripOrderAssignment
        validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.OHP,attemptReasonCode);

        //validating return shipment
        validateReturnShipmentStatus(returnId,OrderStatus.OHP);

        // validate pickup shipment
        validatePickUpShipmentStatus(returnId,
                com.myntra.logistics.platform.domain.ShipmentStatus.ONHOLD_PICKUP_WITH_COURIER);

        // validate order tracking details
        validateOrderTrackingDetailsStatus(trackingNo,"PICKUP_ON_HOLD","PICKUP_DONE","ONHOLD_PICKUP_WITH_COURIER");

        // validate order tracking
        validateOrderTracking(trackingNo,clientId,tenantId,CourierCode.ML,DeliveryStatus.OHP);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.ONHOLD_PICKUP_WITH_DC);

        //validating shipment activity details
//        validateShipmentActivityDetails(returnId,ShipmentType.RETURN,"PICKUP_DONE","RETURN_CREATED","PICKUP_DONE");
    }

    // @Step("validate OnHold PickUp With DC")
    public void validatePreApproveOnHoldPickUpWithDC(String returnId,String trackingNo,Long tripId) throws IOException, JAXBException, JSONException, XMLStreamException, InterruptedException {
        //validating return status in rms
        ReturnResponse returnResponse=rmsServiceHelper.getReturnDetailsNew(returnId);
        validateReturnOrderStatusInRMS(returnResponse,ReturnStatus.LPI);

        //validating tripOrderAssignment
        validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.OFD,AttemptReasonCode.OTHERS.name());

        //validating return shipment
        validateReturnShipmentStatus(returnId,OrderStatus.RIT);

        // validate pickup shipment
        validatePickUpShipmentStatus(returnId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_PICKUP);

        // validate order tracking details
        validateOrderTrackingDetailsStatus(trackingNo,"PICKUP_ON_HOLD","PICKUP_DONE","ONHOLD_PICKUP_WITH_COURIER");

        // validate order tracking
        validateOrderTracking(trackingNo,clientId,tenantId,CourierCode.ML,DeliveryStatus.RIT);

        //validating ml
        validateMLShipmentStatus(trackingNo,EnumSCM.OUT_FOR_PICKUP);

        //validating shipment activity details
//        validateShipmentActivityDetails(returnId,ShipmentType.RETURN,"PICKUP_DONE","RETURN_CREATED","PICKUP_DONE");
    }


}
