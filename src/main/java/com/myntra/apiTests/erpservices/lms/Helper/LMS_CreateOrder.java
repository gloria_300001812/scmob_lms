package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.lms.client.response.TrackingNumberResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.response.PacketResponse;
import org.apache.log4j.Logger;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LMS_CreateOrder {
    private static Logger log = Logger.getLogger(LMS_CreateOrder.class);
    private LMSHelper lmsHepler = new LMSHelper();


    public String createMockOrderExchange(String toStatus, String zipcode, String courierCode, String warehouseId, String
            shippingMethod, String paymentMode, boolean isTryAndBuy, boolean isMultiSeller, String exchangeReleaseId) throws Exception {
        String storeid = LMS_CONSTANTS.CLIENTID;
        String ownerid = LMS_CONSTANTS.CLIENTID;
        String partnerid = LMS_CONSTANTS.TENANTID;
        String email = LMS_CONSTANTS.email;
        String mobilenumber = LMS_CONSTANTS.mobilenumber;

        LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        String paymentM = getPaymentInsturmentMethodMap().get(paymentMode);
        int paymentInstrument = getPaymentInsturmentId().get(paymentMode);
        String status = EnumSCM.WP;
        if (toStatus.equals(EnumSCM.RFR)) status = EnumSCM.RFR;
        String storeOrderId = new StringBuilder("20").append(LMSUtils.randomGenn(17)).append("001").toString();
        String ppsId = "f" + LMSUtils.randomGenn(3) + "xy" + LMSUtils.randomGenn(2) + "-f" + LMSUtils.randomGenn(2) +
                "c-9z0r-y" + LMSUtils.randomGenn(2) + "e-" + LMSUtils.randomGenn(7) + "oi7xg";
        String orderId = insertOrdersExchange(storeOrderId, paymentM, ppsId, status, EnumSCM.WP, zipcode, courierCode,
                warehouseId, shippingMethod, isTryAndBuy, isMultiSeller, storeid, ownerid, email, mobilenumber, partnerid, exchangeReleaseId);
        insertPaymentPlan(ppsId, storeOrderId, paymentInstrument, isMultiSeller);
        log.info("----------------OrderId: " + orderId + "----------------");
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);
        int count = 0;
        String packetId = insertPacketData(orderReleaseId, paymentMode, "PK", zipcode, courierCode,
                warehouseId, shippingMethod, storeid, ownerid, email, mobilenumber);
        if (packetId == null && packetId.isEmpty()) {
            Assert.fail("Packet has not been created in LMS- Check is LMS /LMS3pl is up, check queue- omsLogisticsCreationQueue has consumers, this is new queue flow, based on Feature Gate - lms.couriers_enabled_for_new_3pl_module, (or check old queue - createOrUpdateForwardPacketQueueV2 should not be binded), check if  enrichment (PPS, Serviceability,Catalog) is failing, or check if fetch - instakartCreateShipmentURL is poititing to correct URL-http://d7lms.myntra.com/myntra-lms-service/platform/v2/shipment/create/v1. Also check WMS api : http://wms.scmqa.myntra.com/myntra-wms-service/wms/warehouses/36 ");

        }
        DBUtilities.exUpdateQuery("update order_line set status_code = 'PK' where order_id_fk = "
                + orderId, "oms");
        DBUtilities.exUpdateQuery("update order_release set status_code = 'PK' where order_id_fk =" +
                " " + orderId, "oms");
        insertWMSItem(orderReleaseId, warehouseId);
     /*   insertInWMSForTODOrder(isTryAndBuy, packetId, orderReleaseId, orderId, shippingMethod,
                warehouseId, zipcode, paymentMode, storeid);
*/
        List<Map> listLine = omsServiceHelper.getOrderLineDBEntryforRelease(orderReleaseId);
        Map<String, Object> hm = listLine.get(0);
        omsServiceHelper.insertInTaxationData(Long.parseLong(hm.get("id").toString()));
        Thread.sleep(2000);
        PacketResponse packetResponse = omsServiceHelper.pushPacketToLms(packetId);
        Assert.assertTrue(packetResponse.getStatus().getStatusType().toString().equals("SUCCESS"), "PushPacketToLMS called failed, check services OMS /LMS3pl/ LMS, check queue- omsLogisticsCreationQueue");
        Assert.assertTrue(packetResponse.getStatus().getStatusMessage().equals("Packet updated successfully"), "PushPacketToLMS called failed, check services OMS /LMS3pl/ LMS, check queue- omsLogisticsCreationQueue");

        if (packetResponse.getStatus().getStatusType().toString().equals("SUCCESS")) {
            Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(packetId, EnumSCM.PK, 10),
                    "Order status is not PK in OMS");
            boolean lmsStatus = lmsServiceHelper.validateOrderStatusInLMS(packetId, EnumSCM
                    .PACKED, 10);

            if (!lmsStatus) {
                Assert.fail("Packet has not been created in LMS- Check is LMS /LMS3pl is up, check queue- omsLogisticsCreationQueue has consumers, this is new queue flow, based on Feature Gate - lms.couriers_enabled_for_new_3pl_module, (or check old queue - createOrUpdateForwardPacketQueueV2 should not be binded), check if  enrichment (PPS, Serviceability,Catalog) is failing, or check if fetch - instakartCreateShipmentURL is poititing to correct URL-http://d7lms.myntra.com/myntra-lms-service/platform/v2/shipment/create/v1. Also check WMS api : http://wms.scmqa.myntra.com/myntra-wms-service/wms/warehouses/36 ");
            }
            lmsHepler.processOrderInSCM(toStatus, courierCode, orderReleaseId, packetId);
        }



        return orderId;
    }


    public Map<String, String> getPaymentInsturmentMethodMap() {

        Map<String, String> paymentInsturmentIdMethodMap = new HashMap<>();

        paymentInsturmentIdMethodMap.put("cod", "cod");
        paymentInsturmentIdMethodMap.put("COD", "cod");
        paymentInsturmentIdMethodMap.put("CC", "on");
        paymentInsturmentIdMethodMap.put("DC", "on");
        paymentInsturmentIdMethodMap.put("WALLET", "on");
        paymentInsturmentIdMethodMap.put("NETBANKING", "on");
        paymentInsturmentIdMethodMap.put("ON", "on");
        paymentInsturmentIdMethodMap.put("on", "on");

        return paymentInsturmentIdMethodMap;
    }

    public Map<String, Integer> getPaymentInsturmentId() {

        Map<String, Integer> paymentInsturmentIdMethodMap = new HashMap<>();

        paymentInsturmentIdMethodMap.put("cod", 5);
        paymentInsturmentIdMethodMap.put("COD", 5);
        paymentInsturmentIdMethodMap.put("CC", 1);
        paymentInsturmentIdMethodMap.put("DC", 2);
        paymentInsturmentIdMethodMap.put("WALLET", 10);
        paymentInsturmentIdMethodMap.put("NETBANKING", 4);
        paymentInsturmentIdMethodMap.put("ON", 1);
        paymentInsturmentIdMethodMap.put("on", 1);

        return paymentInsturmentIdMethodMap;
    }

    public String insertOrdersExchange(String storeOrderId, String paymentMode, String ppsId, String status, String
            lineStatus, String zipcode, String courierCode, String warehouseId, String shippingMethod, boolean isTryAndBuy,
                                       boolean isMultiSeller, String store_pid, String owner_pid, String email, String mobilenumber, String logistics_partnerid, String exchangeReleaseId) throws IOException, JAXBException {
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        String q = "INSERT INTO `orders` (`store_order_id`, `login`, `user_contact_no`, `customer_name`, " +
                "`payment_method`, `mrp_total`, `discount`, `cart_discount`, `coupon_discount`, `cash_redeemed`, " +
                "`pg_discount`, " +
                "`final_amount`, `shipping_charge`, `cod_charge`, `emi_charge`, `gift_charge`, `tax_amount`, " +
                "`cashback_offered`, `created_by`, `order_type`, `loyalty_points_used`,`store_pid`, `owner_pid`) " +
                "VALUES('" + storeOrderId + "', '2aa1d18c.86c9.4eff.88c0.a7ef8c677eb7TK8AVXQJJz', '1234567890', " +
                "'lmsmock', '" + paymentMode + "', 1099.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1099.00, 0.00, 0.00, 0.00, " +
                "0.00, 0.00, 0.00, " +
                "'2aa1d18c.86c9.4eff.88c0.a7ef8c677eb7TK8AVXQJJz', 'on', 0.00," + store_pid + "," + owner_pid + ")";

        DBUtilities.exUpdateQuery(q, "oms");
        String orderId = omsServiceHelper.getOrderId(storeOrderId);
        insertOrderAdditionalInfo(orderId, ppsId);
        String releaseId = insertOrderReleaseExchange(orderId, ppsId, storeOrderId, status, paymentMode, zipcode, courierCode,
                warehouseId, shippingMethod, store_pid, owner_pid, email, mobilenumber, exchangeReleaseId);
        Long exchangeOrderLine = omsServiceHelper.getOrderLineEntries(exchangeReleaseId).get(0).getId();
        insertOrderLineExchange(orderId, ppsId, releaseId, storeOrderId, lineStatus, isTryAndBuy, isMultiSeller, warehouseId,
                warehouseId, courierCode, store_pid, owner_pid, logistics_partnerid, exchangeOrderLine);
        return orderId;
    }
    public void insertOrderLineExchange(String orderId, String ppsId, String releaseId, String storeOrderId, String
            lineStatus, boolean isTryAndBuy, boolean isMultiSeller, String sourceWhId, String dispatchWhId, String
                                                courierCode,String store_pid,String owner_pid,String logistics_partnerid,Long exchangeOrderLine) {

        if (isMultiSeller) {
            String q = "INSERT INTO `order_line` (`order_id_fk`, `order_release_id_fk`, `store_order_id`, `style_id`, " +
                    "`option_id`, `sku_id`, `status_code`, `source_wh_id`, `dispatch_wh_id`, `courier_code`, `unit_price`, " +
                    "`quantity`, `discounted_quantity`, `discount`, " +
                    "`cart_discount`, `cash_redeemed`, `coupon_discount`, `pg_discount`, `final_amount`, " +
                    "`tax_amount`," +
                    " `tax_rate`, `cashback_offered`, `disocunt_rule_id`, `discount_rule_rev_id`, `is_discounted`, " +
                    "`is_returnable`, " +
                    "`created_by`, `loyalty_points_used`, `seller_id`, `supply_type`, `store_id`, `po_status`,`store_pid`, `owner_pid`, `logistics_pid`,`exchange_orderline_id`) " +
                    "VALUES" +
                    "(" + orderId + ", " + releaseId + ", '" + storeOrderId + "', 1543, 5298, 3879, '" + lineStatus +
                    "', " + sourceWhId + ", " + dispatchWhId + ", '" + courierCode + "', 1099.00, 1, 0, 0.00, 0.00, " +
                    "0.00, 0.00, 0.00, 1099.00, 0.00, 0.00, 0.00, 0, 0, 0, 1, " +
                    "'2aa1d18c.86c9.4eff.88c0.a7ef8c677eb7TK8AVXQJJz', 0.00, 21, 'ON_HAND', '1', 'UNUSED',"+store_pid+", "+owner_pid+", "+logistics_partnerid+","+exchangeOrderLine+")";

            DBUtilities.exUpdateQuery(q, "oms");

            Map<String, Object> ol1 = DBUtilities.exSelectQueryForSingleRecord("select id from order_line where sku_id" +
                    " = 3879 and order_release_id_fk = " + releaseId, "oms");
            inserOrderLineAdditionalInfo((long) ol1.get("id"), ppsId, isTryAndBuy);
        } else {
            String q = "INSERT INTO `order_line` (`order_id_fk`, `order_release_id_fk`, `store_order_id`, `style_id`, " +
                    "`option_id`, `sku_id`, `status_code`, `source_wh_id`, `dispatch_wh_id`, `courier_code`, `unit_price`, " +
                    "`quantity`, `discounted_quantity`, `discount`, " +
                    "`cart_discount`, `cash_redeemed`, `coupon_discount`, `pg_discount`, `final_amount`, `tax_amount`," +
                    " `tax_rate`, `cashback_offered`, `disocunt_rule_id`, `discount_rule_rev_id`, `is_discounted`, " +
                    "`is_returnable`, " +
                    "`created_by`, `loyalty_points_used`, `seller_id`, `supply_type`, `store_id`, `po_status`,`store_pid`, `owner_pid`, `logistics_pid`) " +
                    "VALUES" +
                    "(" + orderId + ", " + releaseId + ", '" + storeOrderId + "', 1543, 5298, 3879, '" + lineStatus +
                    "', " + sourceWhId + ", " + dispatchWhId + ", '" + courierCode + "', 1099.00, 2, 0, 0.00, 0.00, " +
                    "0.00, 0.00, 0.00, 1099.00, 0.00, 0.00, 0.00, 0, 0, 0, 1, " +
                    "'2aa1d18c.86c9.4eff.88c0.a7ef8c677eb7TK8AVXQJJz', 0.00, 25, 'ON_HAND', '5', 'UNUSED',"+store_pid+", "+owner_pid+", "+logistics_partnerid+","+exchangeOrderLine+")";

            DBUtilities.exUpdateQuery(q, "oms");
            Map<String, Object> ol1 = DBUtilities.exSelectQueryForSingleRecord("select id from order_line where sku_id" +
                    " = 3879 and order_release_id_fk = " + releaseId, "oms");
            inserOrderLineAdditionalInfo((long) ol1.get("id"), ppsId, isTryAndBuy);

        }
    }
    /**
     * @param lineId
     * @param ppsId
     * @param isTryAndBuy
     */
    public void inserOrderLineAdditionalInfo(long lineId, String ppsId, boolean isTryAndBuy) {

        String q = "INSERT INTO `order_line_additional_info` (`order_line_id_fk`, `key`, `value`, `created_by`) " +
                "VALUES" +
                "(" + lineId + ", 'FRAGILE', 'true', 'pps-admin'), " +
                "(" + lineId + ", 'HAZMAT', 'true', 'pps-admin'), " +
                "(" + lineId + ", 'JEWELLERY', 'false', 'pps-admin'), " +
                "(" + lineId + ", 'CUSTOMIZABLE', 'false', 'pps-admin'), " +
                "(" + lineId + ", 'PACKAGING_TYPE', 'NORMAL', 'pps-admin'), " +
                "(" + lineId + ", 'PACKAGING_STATUS', 'NOT_PACKAGED', 'pps-admin'), " +
                "(" + lineId + ", 'GIFT_CARD_AMOUNT', '0.0', 'pps-admin'), " +
                "(" + lineId + ", 'IS_EXCHANGEABLE', 'true', 'pps-admin'), " +
                "(" + lineId + ", 'PAYMENT_PPS_ID', '" + ppsId + "', 'pps-admin'), " +
                "(" + lineId + ", 'TRY_AND_BUY', '" + isTryAndBuy + "', 'pps-admin'), " +
                "(" + lineId + ", 'STORED_CREDIT_USAGE', '0.0', 'pps-admin'), " +


                "(" + lineId + ", 'CUSTOMER_PROMISE_TIME', '" + LMSUtils.addDate(5) + "', 'System'), " +
                "(" + lineId + ", 'EXPECTED_CUSTOMER_PROMISE_TIME', '" + LMSUtils.addDate(5) + "', 'System'), " +
                "(" + lineId + ", 'ACTUAL_CUSTOMER_PROMISE_TIME', '" + LMSUtils.addDate(5) + "', 'System'), " +
                "(" + lineId + ", 'PICK_ITEM_START_TIME', '" + LMSUtils.addDate(0) + "', 'System'), " +
                "(" + lineId + ", 'SELLER_PROCESSING_END_TIME', '" + LMSUtils.addDate(4) + "', 'System'), " +
                "(" + lineId + ", 'SELLER_PROCESSING_START_TIME', '" + LMSUtils.addDate(3) + "', 'System'), " +

                "(" + lineId + ", 'EARNED_CREDIT_USAGE', '0.0', 'pps-admin')";
        DBUtilities.exUpdateQuery(q, "oms");
    }

    /**
     * @param orderId
     * @param ppsId
     */
    public void insertOrderAdditionalInfo(String orderId, String ppsId) {

        String q = "INSERT INTO `order_additional_info` (`order_id_fk`, `key`, `value`, `created_by`) VALUES " +
                "(" + orderId + ", 'ORDER_PROCESSING_FLOW', 'OMS', 'pps-admin'), " +
                "(" + orderId + ", 'CHANNEL', 'web', 'pps-admin'), " +
                "(" + orderId + ", 'LOYALTY_CONVERSION_FACTOR', '0.5', 'pps-admin'), " +
                "(" + orderId + ", 'GIFT_CARD_AMOUNT', '0.0', 'pps-admin'), " +
                "(" + orderId + ", 'PAYMENT_PPS_ID', '" + ppsId + "', 'pps-admin'), " +
                "(" + orderId + ", 'ADDITIONAL_CHARGES_SELLER_ID', '21', 'pps-admin'), " +
                "(" + orderId + ", 'STORED_CREDIT_USAGE', '0.0', 'pps-admin'), " +
                "(" + orderId + ", 'EARNED_CREDIT_USAGE', '0.0', 'pps-admin')";
        DBUtilities.exUpdateQuery(q, "oms");
    }

    public String insertOrderReleaseExchange(String orderId, String ppsId, String storeOrderId, String status, String
            paymentMode, String zipcode, String courierCode, String warehouseId, String shippingMethod, String store_pid, String owner_pid, String email, String mobilenumber, String exchangeReleaseId) throws IOException,
            JAXBException {
        LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        String city = "Bangalore";

        if (zipcode.equals(LMS_PINCODE.MUMBAI_DE_RHD)) {
            city = "Mumbai";
        } else if (zipcode.equals(LMS_PINCODE.NORTH_DE)) {
            city = "Lalitpur";
        } else if (zipcode.equals(LMS_PINCODE.PUNE_EK)) {
            city = "Pune";
        }

        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        String trackingNumber = "";
        TrackingNumberResponse trackingNumberResponse = null;

        if (courierCode.equals("JB")) {
            trackingNumber = "JB" + System.currentTimeMillis();
        } else {
            trackingNumberResponse = lmsServiceHelper.getTrackingNumber(courierCode, warehouseId, "true", zipcode,
                    shippingMethod);
            if (trackingNumberResponse != null && trackingNumberResponse.getTrackingNumberEntry() != null) {
                trackingNumber = trackingNumberResponse.getTrackingNumberEntry().getTrackingNumber().toString();
            } else {
                Assert.fail("TrackingNumberResponse is null");
            }
        }

        String q = "INSERT INTO `order_release` (`order_id_fk`, `store_order_id`, `login`, `status_code`, " +
                "`payment_method`, `mrp_total`, `discount`, `cart_discount`, `coupon_discount`, `cash_redeemed`, " +
                "`pg_discount`, `final_amount`, `shipping_charge`, " +
                "`cod_charge`, `emi_charge`, `gift_charge`, `tax_amount`, `cashback_offered`, `receiver_name`, " +
                "`address`, `city`, `locality`, `state`, `country`, `zipcode`, `mobile`, `email`, `courier_code`, " +
                "`tracking_no`, " +
                "`warehouse_id`, `is_refunded`, `cod_pay_status`, `packed_on`, `created_by`, `user_contact_no`, " +
                "`shipping_method`, `loyalty_points_used`, `store_id`, `queued_on`,`store_pid`, `owner_pid`,`exchange_release_id`) " +
                "VALUES(" + orderId + "," + storeOrderId + ",'2aa1d18c.86c9.4eff.88c0.a7ef8c677eb7TK8AVXQJJz', '" +
                status + "', '" + paymentMode + "', 1099.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1099.00, 0.00, 0.00, 0.00, " +
                "0.00, 0.00, 0.00, 'lmsmock', 'Myntra test lms automation'," +
                " '" + city + "', 'Bommanahalli  &#x28;Bangalore&#x29;', 'KA', 'India', '" + zipcode + "', " +
                "'" + mobilenumber + "', '" + email + "', '" + courierCode + "', '" + trackingNumber + "', " +
                warehouseId + ", 0, 'pending', '" + LMSUtils.getCurrSqlDate() + "', " +
                "'2aa1d18c.86c9.4eff.88c0.a7ef8c677eb7TK8AVXQJJz', '1234567890', '" + shippingMethod + "', 0.00, 1, '"
                + date + "'," + store_pid + "," + owner_pid + "," + exchangeReleaseId + ")";
        DBUtilities.exUpdateQuery(q, "oms");
        String releaseId = omsServiceHelper.getReleaseId(orderId);
        inserOrderReleaseAdditionalInfo(releaseId, ppsId);
        return releaseId;
    }

    /**
     * @param releaseID
     * @param ppsId
     */
    public void inserOrderReleaseAdditionalInfo(String releaseID, String ppsId) {
        String q = "INSERT INTO `order_release_additional_info` (`order_release_id_fk`, `key`, `value`, `created_by`) " +
                "VALUES" +
                "(" + releaseID + ", 'GIFT_CARD_AMOUNT', '0.0', 'pps-admin'), " +
                "(" + releaseID + ", 'LOYALTY_CONVERSION_FACTOR', '0.5', 'pps-admin'), " +
                "(" + releaseID + ", 'ADDITIONAL_CHARGES_SELLER_ID', '21', 'pps-admin'), " +
                "(" + releaseID + ", 'ADDRESS_ID', '6135071', 'pps-admin'), " +
                "(" + releaseID + ", 'PAYMENT_PPS_ID', '" + ppsId + "', 'pps-admin'), " +
                "(" + releaseID + ", 'STORED_CREDIT_USAGE', '0.0', 'pps-admin'), " +
                "(" + releaseID + ", 'EARNED_CREDIT_USAGE', '0.0', 'pps-admin'), " +
                "(" + releaseID + ", 'CUSTOMER_PROMISE_TIME', '" + LMSUtils.addDate(5) + "', 'System'), " +
                "(" + releaseID + ", 'EXPECTED_CUTOFF_TIME', '" + LMSUtils.addDate(2) + "', 'System'), " +
                "(" + releaseID + ", 'EXPECTED_PICKING_TIME', '" + LMSUtils.addDate(1) + "', 'System'), " +
                "(" + releaseID + ", 'EXPECTED_PACKING_TIME', '" + LMSUtils.getCurrSqlDate() + "', 'System'), " +
                "(" + releaseID + ", 'EXPECTED_QC_TIME', '" + LMSUtils.getCurrSqlDate() + "', 'System'), " +
                "(" + releaseID + ", 'SELLER_PROCESSING_START_TIME', '" + LMSUtils.getCurrSqlDate() + "', " +
                "'erpMessageQueue'), " +
                "(" + releaseID + ", 'SELLER_PROCESSING_END_TIME', '" + LMSUtils.getCurrSqlDate() + "', " +
                "'erpMessageQueue')";
        DBUtilities.exUpdateQuery(q, "oms");
    }


    /**
     * @param ppsId
     * @param storeOrderId
     * @param paymentInstrument
     */
    public void insertPaymentPlan(String ppsId, String storeOrderId, int paymentInstrument, boolean isMulriSeller) {
        String q = "INSERT INTO `payment_plan` (`id`, `comments`, `updatedBy`, `updatedTimestamp`, `actionType`, " +
                "`login`, `orderId`, `ppsType`, `sourceId`, `state`, `sessionId`, `cartContext`, `totalAmount`, `clientIP`)" +
                " " +
                "VALUES('" + ppsId + "', 'PPS Plan created', 'SYSTEM', 1483946011537, 'SALE', " +
                "'2aa1d18c.86c9.4eff.88c0.a7ef8c677eb7TK8AVXQJJz', '" + storeOrderId + "', 'ORDER', " +
                "'e2c75adf-9179-48a8-92c9-94042a055c44', 'PPFSM Order Taking done', " +
                "'JJN1c52f20ed63b11e6b03422000a90a0271483946004G', 'DEFAULT', 109900, '1.1.1.1')";
        DBUtilities.exUpdateQuery(q, "pps");
        paymentPlanExecutionStatus(ppsId, paymentInstrument);
        insertPaymentPlanItem(ppsId, paymentInstrument, isMulriSeller);
    }

    /**
     * @param ppsId
     * @param paymentInstrument
     */
    public void paymentPlanExecutionStatus(String ppsId, int paymentInstrument) {

        String random = "9" + LMSUtils.randomGenn(9);
        String q = "INSERT INTO `payment_plan_execution_status` (`comments`, `updatedBy`, `updatedTimestamp`, " +
                "`actionType`, `instrumentTransactionId`, `invoker`, `invokerTransactionId`, `numOfRetriesDone`, " +
                "`ppsActionType`, `state`, `status`, `paymentPlanInstrumentDetailId`) VALUES	" +
                "('Payment Plan Execution Status created', 'SYSTEM', 1483946011494, 'DEBIT', " +
                "'COD:d35a502f-0785-4d4b-84ab-07b7d64beaec', 'pps', '5bdc8ac0-0b04-42b5-baad-ac41c8c2819b', 0, " +
                "'SALE', " +
                "'PIFSM Payment Successful', 0, " + random + ")";
        DBUtilities.exUpdateQuery(q, "pps");
        Map<String, Object> execution = DBUtilities.exSelectQueryForSingleRecord("select id from " +
                "payment_plan_execution_status where paymentPlanInstrumentDetailId = " + random, "pps");
        String instrumentId = insertPaymentInstrument(ppsId, paymentInstrument, execution.get("id").toString());
        DBUtilities.exUpdateQuery("update payment_plan_execution_status set paymentPlanInstrumentDetailId = " +
                instrumentId + " where id = " + execution.get("id").toString(), "pps");
    }

    /**
     * @param ppsId
     * @param paymentInstrument
     */
    public void insertPaymentPlanItem(String ppsId, int paymentInstrument, boolean isMultiSeller) {
        if (isMultiSeller) {
            String q = "INSERT INTO `payment_plan_item` (`comments`, `updatedBy`, `updatedTimestamp`, `itemType`, " +
                    "`pricePerUnit`, `quantity`, `sellerId`, `skuId`, `pps_Id`) VALUES " +
                    //"('Payment Plan Item created', 'SYSTEM', 1483946011431, 'SKU', 109900, 2, '19', '3879', '" +
                    // ppsId + "'), " +
                    "('Payment Plan Item created', 'SYSTEM', 1483946011433, 'SKU', 109900, 1, '25', '3879', '" + ppsId
                    + "')";
            DBUtilities.exUpdateQuery(q, "pps");
        } else {
            String q = "INSERT INTO `payment_plan_item` (`comments`, `updatedBy`, `updatedTimestamp`, `itemType`, " +
                    "`pricePerUnit`, `quantity`, `sellerId`, `skuId`, `pps_Id`) VALUES " +
                    //"('Payment Plan Item created', 'SYSTEM', 1483946011431, 'SKU', 109900, 2, '25', '3879', '" +
                    // ppsId + "'), " +
                    "('Payment Plan Item created', 'SYSTEM', 1483946011433, 'SKU', 109900, 1, '25', '3879', '" + ppsId
                    + "')";
            DBUtilities.exUpdateQuery(q, "pps");
        }
        Map<String, Object> ppi1 = DBUtilities.exSelectQueryForSingleRecord("select id from payment_plan_item where " +
                "skuId = '3879' and pps_Id = '" + ppsId + "'", "pps");

        insertPaymentPlanItemInstrument(ppi1.get("id").toString(), paymentInstrument, "109900");
    }

    /**
     * @param ppsId
     * @param paymentInstrument
     * @param executionId
     * @return
     */
    public String insertPaymentInstrument(String ppsId, int paymentInstrument, String executionId) {
        String q = "INSERT INTO `payment_plan_instrument_details` (`comments`, `updatedBy`, `updatedTimestamp`, " +
                "`paymentInstrumentType`, `totalPrice`, `pps_Id`, `paymentPlanExecutionStatus_id`, `actionType`) VALUES " +
                "('PPS Plan Instrument Details created', 'SYSTEM', 1483946011494, " + paymentInstrument + ", 109900, " +
                "'" + ppsId + "', " + executionId + ", 'DEBIT')";
        DBUtilities.exUpdateQuery(q, "pps");
        Map<String, Object> instrument = DBUtilities.exSelectQueryForSingleRecord("select id from " +
                "payment_plan_instrument_details where pps_Id = '" + ppsId + "'", "pps");
        return instrument.get("id").toString();
    }

    /**
     * @param ppsItemId
     * @param paymentInstrument
     * @param amount
     */
    public void insertPaymentPlanItemInstrument(String ppsItemId, int paymentInstrument, String amount) {
        String q = "INSERT INTO `payment_plan_item_instrument` (`comments`, `updatedBy`, `updatedTimestamp`, `amount`," +
                " `paymentInstrumentType`, `ppsItemId`) VALUES" +
                "('Payment Plan Item Instrument Detail created', 'SYSTEM', 1483946011432, " + amount + ", " +
                paymentInstrument + ", " + ppsItemId + ")";
        DBUtilities.exUpdateQuery(q, "pps");
    }

    public String insertPacketData(String releaseId, String paymentMode, String status, String zipcode, String
            courierCode, String warehouseId, String shippingMethod, String store_pid, String owner_pid, String email, String mobilenumber) throws IOException, JAXBException {

        LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
        String trackingNumber = "";

        if (courierCode.equals("JB")) {

            trackingNumber = "JB" + System.currentTimeMillis();
        } else {

            TrackingNumberResponse trackingNumberResponse = lmsServiceHelper.getTrackingNumber(courierCode,
                    warehouseId, "true", zipcode, shippingMethod);
            if (trackingNumberResponse != null && trackingNumberResponse.getTrackingNumberEntry() != null) {
                trackingNumber = trackingNumberResponse.getTrackingNumberEntry().getTrackingNumber().toString();
            } else {
                Assert.fail("TrackingNumberResponse is null");
            }
        }

        String createdBy = "lmsMock" + System.currentTimeMillis();
        String currentDate = LMSUtils.getCurrSqlDate();
        String q = "INSERT INTO `packet` (`store_id`, `login`, `status_code`, `payment_method`, `receiver_name`, " +
                "`address_id`, `address`, `city`, `locality`, "
                + "`state`, `country`, `zipcode`, `mobile`, `email`, `courier_code`, `tracking_no`, " +
                "`dispatch_warehouse_id`, `cod_pay_status`, "
                + "`shipped_on`, `delivered_on`, `completed_on`, `cancelled_on`, `cancellation_reason_id_fk`, " +
                "`user_contact_no`, `shipping_method`, "
                + "`recipient_email`, `seller_group_id`, `seller_packet_id`, `created_by`, `created_on`, " +
                "`last_modified_on`, `version`,`store_pid`, `owner_pid`)\n" +
                " VALUES (1, '2aa1d18c.86c9.4eff.88c0.a7ef8c677eb7TK8AVXQJJz', '" + status + "', '" + paymentMode +
                "', 'lmsmock', 6135071, "
                + "'Myntra test lms automation', 'Bangalore', 'Bommanahalli  &#x28;Bangalore&#x29;', 'KA', 'India', '"
                + zipcode + "', "
                + "'" + mobilenumber + "', '" + email + "', '" + courierCode + "', '" + trackingNumber + "', " +
                warehouseId + ", 'pending', NULL, NULL, NULL, NULL, NULL, '1234567890', "
                + "'" + shippingMethod + "', NULL, 1, NULL, '" + createdBy + "', '" + currentDate + "', '" +
                currentDate + "', 1," + store_pid + "," + owner_pid + ")";
        DBUtilities.exUpdateQuery(q, "oms");
        String packetIdQuery = "select id from packet where created_by='" + createdBy + "'";
        String packetId = (long) DBUtilities.exSelectQueryForSingleRecord(packetIdQuery, "oms").get("id") + "";
        String updateOrderLineQuery = "update order_line set packet_id_fk='" + packetId + "' where " +
                "order_release_id_fk='" + releaseId + "'";
        DBUtilities.exUpdateQuery(updateOrderLineQuery, "oms");
        return packetId;
    }

    /**
     * @param releaseId
     * @param warehouseId
     */
    @SuppressWarnings("unchecked")
    public void insertWMSItem(String releaseId, String warehouseId) {
        List<Map<String, Object>> orderLine = DBUtilities.exSelectQuery("select * from order_line where " +
                "order_release_id_fk = " + releaseId, "oms");
        for (Map<String, Object> line : orderLine) {
            int qty = (Integer) line.get("quantity");
            insertItemWithOrderId(line.get("sku_id").toString(), warehouseId, qty, releaseId);
        }
    }


    public long getMaxItemId() {
        Map<String, Object> getId = DBUtilities.exSelectQueryForSingleRecord("select max(id) from item", "wms");
        return (Long) getId.get("max(id)");
    }

    /**
     * @param skuId
     * @param warehosueId
     * @param qty
     * @param releaseId
     */

    public synchronized void insertItemWithOrderId(String skuId, String warehosueId, int qty, String releaseId) {

        Map<String, String> binEntry = new HashMap<>();
        binEntry.put("1", "403");
        binEntry.put("19", "151901");
        binEntry.put("28", "271521");
        binEntry.put("36", "577992");

        for (int i = 0; i < qty; i++) {

            Map<String, Object> getId = DBUtilities.exSelectQueryForSingleRecord("select max(id)+1 as ukey from item", "wms");
            long maxitemID = (Long) getId.get("ukey");

            String insertitemquery = "INSERT INTO `item` (`id`, `barcode`, `sku_id`, `quality`, `item_status`, `warehouse_id`, `enabled`, `po_id`, `po_barcode`, `po_sku_id`, `lot_id`, `lot_barcode`, `comments`, `order_id`, `bin_id`, `grn_sku_id`, `grn_barcode`, `inwarded_on`, `reject_reason_code`, `reject_reason_description`, `carton_barcode`, `item_type`, `created_on`, `created_by`, `last_modified_on`, `version`, `inward_request_id`, `inward_source_barcode`) VALUES ("+maxitemID+"," + maxitemID + " , " + skuId + ", 'Q1' , 'STORED', " + warehosueId + ", '1' , 313, 'OPST050911-09', 1, 1, 'OPST050911-09', 'Santhosh item', "+releaseId+", " + binEntry.get(warehosueId) + ", NULL, NULL, NULL, NULL, NULL, NULL, 'ON_HAND', NULL, NULL, NULL, 0, NULL, NULL);";

            String insertiteminfoquery = "INSERT INTO `item_info` (`id`, `item_id`, `item_action_status`, `task_id`, `order_id`, `order_barcode`, `created_on`, `created_by`, `last_modified_on`, `version`, `invoice_sku_id`, `agreement_type`, `source_owner_id`, `buyer_id`, `manufacturing_date`, `is_expirable`, `mrp`, `list_price`, `landed_price`) VALUES (null, " + maxitemID + ", 'NEW', NULL, NULL, NULL, '2019-04-08 19:05:26', 'erpMessageQueue', '2019-04-08 19:05:26', 0, NULL, 'OUTRIGHT', 3974, 3974, NULL, NULL, NULL, NULL, NULL);";
            DBUtilities.exUpdateQuery(insertitemquery, "myntra_wms");
            DBUtilities.exUpdateQuery(insertiteminfoquery, "myntra_wms");


        }
    }

    public synchronized void insertItemWithOrder(String skuId, String warehosueId, int qty, String releaseId) {

        Map<String, String> binEntry = new HashMap<>();
        binEntry.put("1", "403");
        binEntry.put("19", "151901");
        binEntry.put("28", "271521");
        binEntry.put("36", "577992");

        for (int i = 0; i < qty; i++) {

            long barcode = getMaxItemId() + 1;
            DBUtilities.exUpdateQuery("INSERT INTO `item` (`id`,`barcode`, `sku_id`, `quality`, `item_status`, " +
                    "`warehouse_id`, `enabled`, `po_id`, `po_barcode`, `po_sku_id`, `lot_id`, `lot_barcode`, " +
                    "`comments`, `order_id`, `bin_id`) VALUES ("+barcode+"," + barcode + ", " + skuId + ", 'Q1', 'SHIPPED', " +
                    warehosueId + ", 1, 313, 'OPST050911-09', 1, 1, 'LOTVHGA-01', 'Automation item', " + releaseId +
                    ", " +
                    "" + binEntry.get("" + warehosueId) + ")", "wms");

            DBUtilities.exUpdateQuery("INSERT INTO `item_info` ( `item_id`, `item_action_status`, `task_id`, " +
                    "`order_barcode`, `created_on`, `created_by`, `last_modified_on`, `version`, `order_id`, " +
                    "`invoice_sku_id`, `agreement_type`, `buyer_id`) VALUES (" + barcode + ", 'NEW', NULL, NULL, now()" +
                    ", 'erpMessageQueue', now(), 0, NULL, NULL, 'OUTRIGHT', 2297)", "wms");

            String query = "INSERT INTO `order_release_item` (`oms_release_id`, `worms_release_id`, `item_barcode`, " +
                    "`sku_id`, `shipped_on`, `return_id`, `return_type`, `return_received_on`, `return_restocked_on`, " +
                    "`status`, `created_by`, `created_on`, `last_modified_on`, `version`, `supply_type`, `warehouse_id`, " +
                    "`return_warehouse_id`, `quality`, `reject_reason`, `reject_reason_description`, " +
                    "`invoice_release_transaction_id`, `creditnote_release_transaction_id`)\n" +
                    "VALUES\n" +
                    " ('" + releaseId + "', NULL, '" + barcode + "', '" + skuId + "', '" + LMSUtils.getCurrSqlDate() +
                    "', NULL, NULL, NULL, NULL, 'SHIPPED', 'erpadmin', '" + LMSUtils.getCurrSqlDate() + "', '" +
                    LMSUtils.getCurrSqlDate() + "', 0, 'ON_HAND', " + warehosueId + ", NULL, NULL, NULL, NULL, 6051, " +
                    "NULL)";
            DBUtilities.exUpdateQuery(query, "myntra_worms");
        }
    }

}
