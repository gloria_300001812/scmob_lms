package com.myntra.apiTests.erpservices.lms.Constants;

public enum CourierCode {
	ML,
	TC,
	DE,
	BD,
	EE,
	IP,
	EK,
	EKR,
	NE,
	WW,
	EER,
	GIH,
	AR,
	GA,
	DD,
	DP;

	private CourierCode() {
	}
}
