package com.myntra.apiTests.erpservices.lms.Helper;

import com.fasterxml.jackson.core.JsonParseException;
import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.common.Constants.LambdaInterfaces;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.validators.ReturnValidator;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.response.EmptyResponse;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lms.client.domain.response.ReturnResponse;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import com.myntra.tms.container.ContainerEntry;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagEntry;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS.CLIENTID;
import static com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS.TENANTID;

public class ReturnHelper implements StatusPoller {
    TripClient_QA tripClient_qa=new TripClient_QA();
    StoreValidator storeValidator=new StoreValidator();
    StatusPollingValidator statusPollingValidator=new StatusPollingValidator();
    StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);

    //Receive master bag in TMS
    public LambdaInterfaces.TriFunction tmsReceiveMasterBag=(hubCode,masterBagId,tenantId) -> {

        TMSMasterbagEntry tmsMasterbagEntry=new TMSMasterbagEntry();
        tmsMasterbagEntry.setMasterbagId(masterBagId.toString());
        tmsMasterbagEntry.setWeight(15.00);
        tmsMasterbagEntry.setSealId(Long.parseLong(LMSUtils.randomGenn(8)));
        String payload=APIUtilities.convertXMLObjectToString(tmsMasterbagEntry);
        TMSMasterbagReponse tmsMasterbagReponse=(TMSMasterbagReponse) APIUtilities.convertXMLStringToObject(
                HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_RECEIVE_MASTERBAG,
                        new String[] {tenantId.toString(),hubCode.toString(),"false"},
                        SERVICE_TYPE.TMS_SVC.toString(),
                        HTTPMethods.POST,payload,Headers.getLmsHeaderXML()).getResponseBody(),
                new TMSMasterbagReponse());
        return tmsMasterbagReponse;
    };
    //receive container in transport hub
    public LambdaInterfaces.BiFunction receiveContainerInTransportHub=(containerId,hubId) -> APIUtilities.convertXMLStringToObject(
            HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_CONTAINER,
                    new String[] {containerId.toString(),"dropOffHubCode",hubId.toString(),"tenant",LMS_CONSTANTS.TENANTID},
                    SERVICE_TYPE.TMS_SVC.toString(),HTTPMethods.GET,null,
                    Headers.getLmsHeaderXML()).getResponseBody(),new ContainerResponse());
    String env=getEnvironment();
    LmsServiceHelper lmsServiceHelper=new LmsServiceHelper();
    LMSHelper lmsHelper=new LMSHelper();
    LMS_ReturnHelper lms_returnHelper=new LMS_ReturnHelper();
    RMSServiceHelper rmsServiceHelper=new RMSServiceHelper();
    ReturnValidator returnValidator=new ReturnValidator();
    MasterBagServiceHelper masterBagServiceHelper=new MasterBagServiceHelper();
    MasterBagClient masterBagClient=new MasterBagClient(env,LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    TMSServiceHelper tmsServiceHelper=new TMSServiceHelper();
    private String pathParam=null;
    private String FGPathparam;

    /**
     * tripCompleteATSDA
     * EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING enums
     *
     * @param tripOrderAssignmentId
     * @param trackingNo
     * @param returnId
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void tripCompleteAtSDA(String tripOrderAssignmentId,String status,String trackingNo,String returnId,Long tripId)
            throws IOException, JAXBException, InterruptedException {
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to complete Trip.");
        returnValidator.validateTripComplete(status,trackingNo,tripId);
    }

    public void tripComplete(String tripOrderAssignmentId,String status)
            throws IOException, JAXBException {
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to complete Trip.");
    }


    public void tripCompleteAtSDACCReject(String tripOrderAssignmentId,String status,String trackingNo,String returnId,Long tripId)
            throws IOException, JAXBException, InterruptedException {
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to complete Trip.");
        returnValidator.validateTripCompleteCCReject(status,trackingNo,tripId);
    }

    public void tripCompleteAtSDAAfterOnHold(String tripOrderAssignmentId,String status,String trackingNo,String returnId,Long tripId)
            throws IOException, JAXBException {
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to complete Trip.");
        returnValidator.validateTripStatus(tripId,com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED);
    }

    public void tripCompleteAtSDAPickupFailed(String tripOrderAssignmentId,String status,String trackingNo,String returnId,Long tripId)
            throws IOException, JAXBException, InterruptedException {
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to complete Trip.");
        returnValidator.validateTripCompletePickupFailed(status,trackingNo,tripId);
    }

    public void tripCompleteAtSDAPickupOnHold(String tripOrderAssignmentId,String status,String trackingNo,String returnId,Long tripId)
            throws IOException, JAXBException, InterruptedException {
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to complete Trip.");
        returnValidator.validateTripCompletePickupOnHold(status,trackingNo,tripId);
    }

    public void tripCompleteAtSDARejectOnHold(String tripOrderAssignmentId,String status,String trackingNo,String returnId,Long tripId)
            throws IOException, JAXBException, InterruptedException {
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to complete Trip.");
        returnValidator.validateTripCompleteRejectOnHold(status,trackingNo,tripId);
    }

    public void tripCompleteAtSDAFailedPickup(String tripOrderAssignmentId,String status,String trackingNo,String returnId,Long tripId)
            throws IOException, JAXBException, InterruptedException {
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to complete Trip.");
        returnValidator.validateTripCompletePickupFailed(status,trackingNo,tripId);
    }

    public void tripCompleteAtSDAPQCPReceviedInDC(String tripOrderAssignmentId,String status,String trackingNo,String returnId,Long tripId)
            throws IOException, JAXBException, InterruptedException {
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to complete Trip.");
        returnValidator.validateTripCompletePQCPRecevedInDC(status,trackingNo,tripId);
    }

    public void tripCompleteAtSDAPQCP(String tripOrderAssignmentId,String status,String trackingNo,String returnId,Long tripId)
            throws IOException, JAXBException, InterruptedException {
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to complete Trip.");
        returnValidator.validateTripCompletePQCP(status,trackingNo,tripId);
    }

    public void tripCompleteAtSDAPQCPCCApprove(String tripOrderAssignmentId,String status,String trackingNo,String returnId,Long tripId)
            throws IOException, JAXBException, InterruptedException {
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to complete Trip.");
        returnValidator.validateTripCompletePQCPCCApprove(status,trackingNo,tripId);
    }


    /**
     * tripUpdateAtSDA
     * marks trip update at DC Manager
     * <p>
     * EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING enums
     *
     * @param tripOrderAssignmentId
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void tripUpdateAtSDA(String tripOrderAssignmentId,String status,String trackingNo)
            throws IOException, JAXBException {
        TripOrderAssignmentResponse response=
                lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),status,EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update Trip.");
    }


    /**
     * shipmentUpdateAtDC
     * marks trip update at DC Manager
     * <p>
     * ShipmentUpdateEvent enums
     *
     * @param returnId
     * @param trackingNo
     * @param shipmentUpdateEvent
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void shipmentUpdateAtDC(String returnId,String trackingNo,ShipmentUpdateEvent shipmentUpdateEvent)
            throws IOException, JAXBException, InterruptedException {
        lms_returnHelper.mlOpenBoxQC(returnId,shipmentUpdateEvent,trackingNo);
        returnValidator.validateMLShipmentStatus(trackingNo,shipmentUpdateEvent.name());
    }

    /**
     * ohHoldPickUpWithDC
     * marks trip update at DC Manager
     * <p>
     * ShipmentUpdateEvent enums
     *
     * @param returnId
     * @param trackingNo
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void ohHoldPickUpWithDC(String returnId,String trackingNo,Long tripId,String attemptReasonCode) throws IOException, JAXBException, InterruptedException {
        lms_returnHelper.mlOpenBoxQC(returnId,ShipmentUpdateEvent.PICKUP_ON_HOLD,trackingNo);
        returnValidator.validateOnHoldPickUpWithDC(returnId,trackingNo,tripId,attemptReasonCode);

    }

    /**
     * pickUpSuccessfulWithDC
     * marks trip update at DC Manager
     * <p>
     * ShipmentUpdateEvent enums
     *
     * @param returnId
     * @param trackingNo
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void pickUpSuccessfulWithDC(String returnId,String trackingNo) throws IOException, JAXBException, InterruptedException {
        lms_returnHelper.mlOpenBoxQC(returnId,ShipmentUpdateEvent.PICKUP_SUCCESSFUL,trackingNo);
        returnValidator.validateMLShipmentStatus(trackingNo,ShipmentUpdateEvent.PICKUP_SUCCESSFUL.name());

    }

    /**
     * pickUpRejectedWithDC
     * marks trip update at DC Manager
     * <p>
     * ShipmentUpdateEvent enums
     *
     * @param returnId
     * @param trackingNo
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void pickUpRejectedWithDC(String returnId,String trackingNo) throws IOException, JAXBException, InterruptedException {
        lms_returnHelper.mlOpenBoxQC(returnId,ShipmentUpdateEvent.RETURN_REJECTED,trackingNo);
        returnValidator.validatePickUpShipmentStatus(returnId,
                com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_PICKUP);

    }

    /**
     * pickUpRejectedWithDC
     * marks trip update at DC Manager
     * <p>
     * ShipmentUpdateEvent enums
     *
     * @param returnId
     * @param trackingNo
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void pickUpRejectedWithDCFailed(String returnId,String trackingNo) throws IOException {
        lms_returnHelper.mlOpenBoxQC(returnId,ShipmentUpdateEvent.PICKUP_REJECTED,trackingNo);
        returnValidator.validateMLShipmentStatusFail(trackingNo,ShipmentUpdateEvent.PICKUP_REJECTED.name());

    }

    /**
     * approveAtCC
     *
     * @param returnId
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     */
    public void approveAtCC(String returnId)
            throws IOException, JAXBException, InterruptedException {
        lmsServiceHelper.approveOrRejectAtCC(returnId,EnumSCM.APPROVED);
        returnValidator.validateReturnShipmentCCApproval(returnId,PickupApprovalFlag.APPROVED);
    }

    /**
     * rejectAtCC
     *
     * @param returnId
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     */
    public void rejectAtCC(String returnId)
            throws IOException, JAXBException, InterruptedException {
        lmsServiceHelper.approveOrRejectAtCC(returnId,EnumSCM.REJECTED);
        returnValidator.validateReturnShipmentCCApproval(returnId,PickupApprovalFlag.REJECTED);
    }

    /**
     * createRequeue
     *
     * @param trackingNumber
     * @param dcId
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     */
    public String createRequeue(String trackingNumber,Long dcId) throws JAXBException, UnsupportedEncodingException {
        SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date=f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO=new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(dcId);
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("initiating RTO for shipment");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);

        String payload=APIUtilities.convertJavaObjectToJsonUsingGson(mlShipmentUpdateEntryForRTO);

        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.ML_SHIPMENT_UPDATEVersion2,
                new String[] {},SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST,payload,Headers.getCTSHeaderJSON());
        return service.getResponseBody();
    }

    /**
     * validateRefund
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void validateRefund(String returnId,String destWarehouseId,String deliveryCenterID)
            throws Exception {
        lmsServiceHelper.validateRmsStatusAndRefund(returnId,EnumSCM.RL,true,8000L);
    }

    public void receiveOrderAtDC(String trackingNo,Long tripId) throws UnsupportedEncodingException, JAXBException {
        //updating the operational tracking no.
        DBUtilities.exUpdateQuery("update `ml_shipment` set `operational_tracking_id`=\""+trackingNo.substring(
                2)+"\" where `tracking_number`=\""+trackingNo+"\"","lms");

        final TripOrderAssignmentResponse tripOrderAssignmentResponse=lmsServiceHelper.receiveOrderAtDC(trackingNo);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),"SUCCESS",
                "unable to receive in DC");

        //validating is received
        returnValidator.validateTripOrderAssignmentIsReceived(tripId,ShipmentType.PU,true);

    }

    /**
     * receive exchange orders
     * @param trackingNo
     * @param tripId
     * @throws UnsupportedEncodingException
     * @throws JAXBException
     */
    public void receiveOrderAtDC(String trackingNo,Long tripId,String tenantId) throws WebClientException {
        //updating the operational tracking no.
        DBUtilities.exUpdateQuery("update `ml_shipment` set `operational_tracking_id`=\""+trackingNo.substring(
                2)+"\" where `tracking_number`=\""+trackingNo+"\"","lms");

        final TripOrderAssignmentResponse tripOrderAssignmentResponse=lmsServiceHelper.receiveOrderAtDC(trackingNo);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),"SUCCESS",
                "unable to receive in DC");

        //validate isReceive
        TripOrderAssignmentResponse isReceiveResponse = tripClient_qa.findExchangesByTrip(tripId,tenantId);
        Assert.assertEquals(isReceiveResponse.getStatus().getStatusMessage(),"Success","trip details is retrieved successfully");
        Assert.assertTrue(isReceiveResponse.getTripOrders().get(0).getIsReceived(),"exchanged order is not received in DC");

    }

    public void receiveOrderAtDC(String trackingNo,String tripOrderAssignmentId,Long tripId) throws UnsupportedEncodingException, JAXBException {
        //updating the operational tracking no.
        DBUtilities.exUpdateQuery("update `ml_shipment` set `operational_tracking_id`=\""+trackingNo.substring(
                2)+"\" where `tracking_number`=\""+trackingNo+"\"","lms");

        final TripOrderAssignmentResponse tripOrderAssignmentResponse=lmsServiceHelper.receiveOrderAtDC(trackingNo);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),"SUCCESS",
                "unable to receive in DC");

        //validating is received
        returnValidator.validateTripOrderAssignmentIsReceived(tripId,ShipmentType.PU,true);
    }

    public void transferShipmentToWareHouse(String deliveryCenterId,String returnId)
            throws Exception {


        String tenantId=LMS_CONSTANTS.TENANTID;

        DeliveryCenterResponse deliveryCenterResponse=lmsServiceHelper.getDCDetails(deliveryCenterId);
        final String deliveryCenterCode=deliveryCenterResponse.getDeliveryCenters().get(0).getCode();

        final com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS=
                getReturnStatusInLMS(returnId,tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        final String returnHub=returnStatusInLMS.getDomainReturnShipments().get(0).getReturnHubCode();
        final String returnTrackingNumber=returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber();

        //CreateMB from DC To WH
        final MasterbagDomain masterBag=masterBagServiceHelper
                .createMasterBag(deliveryCenterCode,returnHub,ShippingMethod.NORMAL,"ML",tenantId);
        Long reverseMBId=masterBag.getId();
        //Validate the MB status
        ShipmentResponse shipmentResponse12=masterBagClient.getShipmentOrderMap(reverseMBId);
        Assert.assertEquals(shipmentResponse12.getStatus().getStatusMessage(),
                ResponseMessageConstants.retrievedShipment,"Not able to retrive Master bag details");
        returnValidator.validateMasterBagStatus(shipmentResponse12,ShipmentStatus.NEW);
        //Add order to MB
        masterBagServiceHelper.addShipmentsToMasterBag(reverseMBId,returnTrackingNumber,ShipmentType.PU,
                tenantId);
        //validate MB status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ShipmentResponse shipmentResponse3=masterBagClient.getShipmentOrderMap(Long.valueOf(reverseMBId));
        Assert.assertEquals(shipmentResponse3.getStatus().getStatusMessage(),
                ResponseMessageConstants.retrievedShipment,"Not able to retrive Master bag details");
        returnValidator.validateMasterBagStatus(shipmentResponse3,ShipmentStatus.NEW);
        //close Reverse MB
        masterBagServiceHelper.closeMasterBag(reverseMBId,tenantId);
        //validate MB status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ShipmentResponse shipmentResponse03=masterBagClient.getShipmentOrderMap(Long.valueOf(reverseMBId));
        Assert.assertEquals(shipmentResponse03.getStatus().getStatusMessage(),
                ResponseMessageConstants.retrievedShipment,"Not able to retrive Master bag details");
        returnValidator.validateMasterBagStatus(shipmentResponse03,ShipmentStatus.CLOSED);

        //Receive MB in TMS
        TMSMasterbagReponse
                tmsMasterbagReponse=(TMSMasterbagReponse) tmsReceiveMasterBag.apply(deliveryCenterCode,reverseMBId,
                tenantId);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(
                ResponseMessageConstants.receiveMasterBag),
                "Master bag is not revceived in TMS. Reason "+tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        ShipmentResponse shipmentResponse4=masterBagClient.getShipmentOrderMap(Long.valueOf(reverseMBId));
        Assert.assertEquals(shipmentResponse4.getStatus().getStatusMessage(),
                ResponseMessageConstants.retrievedShipment,"Not able to retrive Master bag details");
        returnValidator.validateMasterBagStatus(shipmentResponse4,ShipmentStatus.CLOSED);

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse=getMappedTransportHubDetails(tenantId,
                deliveryCenterCode);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(
                ResponseMessageConstants.success),
                "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub=hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(
                0).getTransportHubCode();
        //get lane configurations
        long laneId=((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterCode,
                transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId=((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(
                laneId)).getTransporterEntries().get(0).getId();


        //Create container
        ContainerResponse
                containerResponse=((ContainerResponse) tmsServiceHelper.createContainer.apply(deliveryCenterCode,
                transporterHub,laneId,transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(
                ResponseMessageConstants.addContainer));
        List <ContainerEntry> lstContainerEntry=containerResponse.getContainerEntries();
        long containerId=0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId=lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        returnValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse containerResponse2=(ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(
                containerId,reverseMBId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(),
                ResponseMessageConstants.success1,"Master bag is not added to a container");
        //validate MB status
        ShipmentResponse shipmentResponse5=masterBagClient.getShipmentOrderMap(Long.valueOf(reverseMBId));
        Assert.assertEquals(shipmentResponse5.getStatus().getStatusMessage(),
                ResponseMessageConstants.retrievedShipment,"Not able to retrive Master bag details");
        returnValidator.validateMasterBagStatus(shipmentResponse5,ShipmentStatus.CLOSED);

        //Ship container
        ContainerResponse containerResponse1=(ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(
                containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),
                "Container is not shipped");
        //validate MB status
        ShipmentResponse shipmentResponse6=masterBagClient.getShipmentOrderMap(Long.valueOf(reverseMBId));
        Assert.assertEquals(shipmentResponse6.getStatus().getStatusMessage(),
                ResponseMessageConstants.retrievedShipment,"Not able to retrive Master bag details");
        returnValidator.validateMasterBagStatus(shipmentResponse6,ShipmentStatus.IN_TRANSIT);

        //Receive Container In Transport Hub
        ContainerResponse containerResponse3=(ContainerResponse) receiveContainerInTransportHub.apply(containerId,
                transporterHub);
        Assert.assertTrue(containerResponse3.getStatus().getStatusMessage().equalsIgnoreCase(
                ResponseMessageConstants.receiveMasterBag),"Container is not received in TRansport hub");

        //MasterBag inscan in WH
        String mbInscanResponse=null;
        masterBagInscanInWHReturnProcess(returnTrackingNumber,reverseMBId,ShipmentType.PU,
                OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED,PremisesType.HUB,20l);

        mbInscanResponse=masterBagInscanInWHReturnProcess(returnTrackingNumber,reverseMBId,ShipmentType.PU,
                OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED,PremisesType.HUB,20l);
        Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success));
        //Validate ML shipment status in DC
        returnValidator.validateMLShipmentStatus(returnTrackingNumber,
                ShipmentUpdateEvent.RETURN_IN_TRANSIT.toString());

        //Validate the MB status
        ShipmentResponse shipmentResponse17=masterBagClient.getShipmentOrderMap(reverseMBId);
        Assert.assertEquals(shipmentResponse17.getStatus().getStatusMessage(),
                ResponseMessageConstants.retrievedShipment,"Not able to retrive Master bag details");
        returnValidator.validateMasterBagStatus(shipmentResponse17,ShipmentStatus.IN_TRANSIT);
        //validate return status in rms
        final com.myntra.returns.response.ReturnResponse returnResponse3=
                rmsServiceHelper.getReturnDetailsNew(returnId);
        returnValidator.validateReturnOrderStatusInRMS(returnResponse3,ReturnStatus.RRC);
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse4=getReturnStatusInLMS(returnId,
                tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        returnValidator.validateReturnOrderStatusInLMS(returnResponse4,
                com.myntra.lastmile.client.status.ShipmentStatus.DELIVERED_TO_SELLER);


    }

    public void acknowledgeOnHoldAtDC(String deliveryCenterID,String returnId) throws IOException {
        lmsServiceHelper.AcknowledgeApprovePickup(Long.parseLong(deliveryCenterID),
                rmsServiceHelper.getReturnDetailsNew(returnId).getData().get(
                        0).getReturnTrackingDetailsEntry().getTrackingNo());

    }

    public com.myntra.lms.client.domain.response.ReturnResponse getReturnStatusInLMS(String returnId,String tenantId,Long sourceId) throws IOException, JAXBException {
        String param=returnId+"?sourceId="+sourceId+"&tenantId="+tenantId;

        Svc service=HttpExecutorService
                .executeHttpService(Constants.LMS_PATH.RETURN_STATUS_LMS,new String[] {param},
                        SERVICE_TYPE.LMS_SVC.toString(),
                        HTTPMethods.GET,null,Headers.getLmsHeaderXML());
        com.myntra.lms.client.domain.response.ReturnResponse
                returnResponse=(com.myntra.lms.client.domain.response.ReturnResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(),
                        new com.myntra.lms.client.domain.response.ReturnResponse());
        return returnResponse;
    }

    public HubToTransportHubConfigResponse getMappedTransportHubDetails(String tenantId,String dcOrWHHubCOde) throws UnsupportedEncodingException, JAXBException {
        pathParam=tenantId+"/"+dcOrWHHubCOde;

        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_LOCATION_HUB_CONFIG_FW,
                new String[] {pathParam},SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET,null,Headers.getLmsHeaderXML());
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse=(HubToTransportHubConfigResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(),new HubToTransportHubConfigResponse());
        return hubToTransportHubConfigResponse;
    }

    public String masterBagInscanInWHReturnProcess(String trackingNumber,long masterBagId,ShipmentType shipmentType,OrderShipmentAssociationStatus orderShipmentAssociationStatus,
                                                   ShipmentStatus shipmentStatus,PremisesType premisesType,Long lastScannedPremisesId) throws NumberFormatException, JAXBException, UnsupportedEncodingException {
        // Order Scan After Shipment At DC

        OrderShipmentAssociationEntry orderShipmentAssociationEntries=new OrderShipmentAssociationEntry();
        orderShipmentAssociationEntries.setTrackingNumber(trackingNumber);
        orderShipmentAssociationEntries.setShipmentType(shipmentType);
        orderShipmentAssociationEntries.setStatus(orderShipmentAssociationStatus);

        List <OrderShipmentAssociationEntry> orderShipmentAssociation=new ArrayList <>();
        orderShipmentAssociation.add(orderShipmentAssociationEntries);

        ShipmentEntry shipmentEntryPayload=new ShipmentEntry();
        shipmentEntryPayload.setId(masterBagId);
        shipmentEntryPayload.setStatus(shipmentStatus);
        shipmentEntryPayload.setLastScannedCity("Bangalore returns hub-HUB");
        shipmentEntryPayload.setLastScannedPremisesId(lastScannedPremisesId);
        shipmentEntryPayload.setLastScannedPremisesType(premisesType);
        shipmentEntryPayload.setArrivedOn(new Date());
        shipmentEntryPayload.setLastScannedOn(new Date());
        shipmentEntryPayload.setOrderShipmentAssociationEntries(orderShipmentAssociation);

        String payload=APIUtilities.convertXMLObjectToString(shipmentEntryPayload);
        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.RECEIVE_SHIPMENT_IN_DC,
                new String[] {String.valueOf(masterBagId)},SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.PUT,payload,Headers.getLmsHeaderXML());
        return service.getResponseBody();
    }

    public Long getDeliveryStaffId(Long DCId,String tenantId,int start,int fetchSize,String sortBy,String sortOrder) throws UnsupportedEncodingException, JAXBException {
        pathParam="?q=deliveryCenter.id.eq:"+DCId+"___deleted.eq:false___tenantId.eq:"+tenantId+"&start="+start+"&fetchSize="+fetchSize+"&sortBy="+sortBy+"&sortOrder="+sortOrder;
        Svc service=HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.FILTER_SEARCH,
                new String[] {pathParam},SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.GET,null,Headers.getLmsHeaderXML());
        DeliveryStaffResponse deliveryStaffResponse=(DeliveryStaffResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(),new DeliveryStaffResponse());
        final Long deliveryStaffId=deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        return deliveryStaffId;
    }

    public void requeueAndProcessFurther(String trackingNo,String returnId,String deliveryCenterID,String destWarehouseId,String tripOrderAssignmentId,String toStatus)
            throws Exception {
        //requeue the return
        final String requeueResponse=createRequeue(trackingNo,Long.valueOf(deliveryCenterID));
        Assert.assertTrue(requeueResponse.contains(ResponseMessageConstants.updateOrder),
                "Order is not updated successfully");


        //process after requeue
        final ReturnResponse returnShipmentDetails=getReturnShipmentDetails(returnId,LMS_CONSTANTS.CLIENTID,
                LMS_CONSTANTS.TENANTID);
        LMS_OpenBoxReturnHelper lms_openBoxReturnHelper=new LMS_OpenBoxReturnHelper();
        lms_openBoxReturnHelper.processOpenBoxReturnInML(returnId,toStatus,trackingNo,destWarehouseId,
                deliveryCenterID,tripOrderAssignmentId,returnShipmentDetails);

    }


    /**
     * recreateTripAndProcessFurther
     *
     * @param trackingNo
     * @param returnId
     * @param deliveryCenterID
     * @param destWarehouseId
     * @param tripOrderAssignmentId
     * @param toStatus
     * @throws IOException
     * @throws JsonMappingException
     * @throws org.codehaus.jackson.JsonParseException
     */
    public void recreateTripAndProcessFurther(String trackingNo,String returnId,String deliveryCenterID,String destWarehouseId,String tripOrderAssignmentId,String toStatus)
            throws Exception {

        final ReturnResponse returnShipmentDetails=getReturnShipmentDetails(returnId,LMS_CONSTANTS.CLIENTID,
                LMS_CONSTANTS.TENANTID);
        LMS_OpenBoxReturnHelper lms_openBoxReturnHelper=new LMS_OpenBoxReturnHelper();
        lms_openBoxReturnHelper.processOpenBoxReturnInML(returnId,toStatus,trackingNo,destWarehouseId,
                deliveryCenterID,tripOrderAssignmentId,returnShipmentDetails);

    }


    /**
     * getReturnShipmentDetails for Return ID
     *
     * @param returnId
     * @return {@link ReturnResponse}
     * @throws IOException
     * @throws JsonMappingException
     * @throws org.codehaus.jackson.JsonParseException
     */
    public ReturnResponse getReturnShipmentDetails(String returnId,String sourceId,String tenantId)
            throws IOException, JAXBException {
        String param=returnId+"?sourceId="+sourceId+"&tenantId="+tenantId;
        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.RETURN_STATUS_LMS,
                new String[] {param},SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET,null,Headers.getLmsHeaderXML());
        ReturnResponse returnResponse=(ReturnResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(),new ReturnResponse());
        return returnResponse;
    }

    /**
     * getOrderTracking for tracking no ID
     *
     * @param trackingNumber
     * @return {@link ReturnResponse}
     * @throws IOException
     * @throws JsonMappingException
     * @throws org.codehaus.jackson.JsonParseException
     */
    public OrderTrackingResponse getOrderTracking(String trackingNumber, String clientId, String tenantId, CourierCode courierCode)
            throws IOException, JAXBException {
        String param="?courierOperator="+courierCode+"&trackingNumber="+trackingNumber+"&tenantId="+tenantId+"&clientId="+clientId;
        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_ORDER_TRACKING_DETAIL_V_2,
                new String[] {param},SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET,null,Headers.getLmsHeaderXML());
        OrderTrackingResponse returnResponse=(OrderTrackingResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(),new OrderTrackingResponse());
        return returnResponse;
    }


    /**
     * getOrderTrackingDetail
     *
     * @param courierCode
     * @param trackingNumber
     * @return
     * @throws Exception
     */
    public OrderTrackingResponse getOrderTrackingDetail(CourierCode courierCode,String trackingNumber,String clientId,String tenantId) throws UnsupportedEncodingException, JAXBException {
        String param="?clientId="+clientId+"&tenantId="+tenantId+"&level=LEVEL2";
        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_ORDER_TRACKING,
                new String[] {courierCode.name(),trackingNumber,param},SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET,null,Headers.getLmsHeaderXML());
        OrderTrackingResponse response=(OrderTrackingResponse) APIUtilities.convertXMLStringToObject(
                service.getResponseBody(),
                new OrderTrackingResponse());
        return response;
    }

    /**
     * getBulkOrderTrackingDetails for tracking no ID
     *
     * @param trackingNumber
     * @return {@link ReturnResponse}
     * @throws IOException
     * @throws JsonMappingException
     * @throws org.codehaus.jackson.JsonParseException
     */
    public OrderTrackingResponseV3 getBulkOrderTrackingDetails(String trackingNumber)
            throws IOException, JAXBException {
        String param="?trackingNumbers="+trackingNumber;
        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_BULK_ORDER_TRACKING_DETAIL,
                new String[] {param},SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET,null,Headers.getLmsHeaderXML());
        OrderTrackingResponseV3 returnResponse=(OrderTrackingResponseV3) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(),new OrderTrackingResponseV3());

        return returnResponse;
    }

    /**
     * getShipmentActivityDetails for tracking no ID
     *
     * @param returnId
     * @return {@link ReturnResponse}
     * @throws IOException
     * @throws JsonMappingException
     * @throws org.codehaus.jackson.JsonParseException
     */
    public ShipmentActivityResponse getShipmentActivityDetails(String returnId,ShipmentType shipmentType)
            throws IOException, JAXBException {
        String param="?shipmentId="+returnId+"&shipmentType="+shipmentType+"&level=LEVEL2";
        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_SHIPMENT_TRACKING_DETAILS+param,
                new String[] {},SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET,null,Headers.getLmsHeaderJSON());
        ShipmentActivityResponse shipmentActivityResponse=(ShipmentActivityResponse) APIUtilities.convertXMLStringToObject(
                String.valueOf(service.getResponseBody()),ShipmentActivityResponse.class);
        return shipmentActivityResponse;
    }

    /**
     * uploadSignature
     *
     * @param returnId
     * @return {@link ReturnResponse}
     * @throws IOException
     * @throws JsonMappingException
     * @throws org.codehaus.jackson.JsonParseException
     */
    public void uploadSignature(Long returnId) throws JAXBException, IOException, InterruptedException {
        Thread.sleep(LMS_CONSTANTS.wait_Time);
        String trackingNo=getPickupTrackingNoFromSourceReturnId(String.valueOf(returnId),
                LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        EmptyResponse emptyResponse=lmsServiceHelper.uploadSignature(trackingNo);
        Assert.assertEquals(emptyResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Signature not Uploaded properly");

    }


    /**
     * reship
     *
     * @param trackingNumber
     * @param dcId
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     */
    public String reship(String trackingNumber,Long dcId) throws UnsupportedEncodingException {
        SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date=f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO=new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(dcId);
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("RESHIP_TO_CUSTOMER");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RESHIP_TO_CUSTOMER);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);

        String payload=APIUtilities.convertJavaObjectToJsonUsingGson(mlShipmentUpdateEntryForRTO);

        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.ML_SHIPMENT_UPDATEVersion2,
                new String[] {},SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST,payload,Headers.getCTSHeaderJSON());
        return service.getResponseBody();

    }

    public void reshipTpCustomer(String trackingNo,String deliveryCenterID) throws UnsupportedEncodingException {
        final String requeueResponse=reship(trackingNo,Long.valueOf(deliveryCenterID));
        Assert.assertTrue(requeueResponse.contains(ResponseMessageConstants.updateOrder),
                "Order is not updated successfully");
    }

    public String returnRejectDC(String trackingNumber,Long dcId) throws JAXBException, UnsupportedEncodingException {
        SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date=f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO=new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(dcId);
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("initiating RTO for shipment");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RETURN_REJECTED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);

        String payload=APIUtilities.convertJavaObjectToJsonUsingGson(mlShipmentUpdateEntryForRTO);

        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.ML_SHIPMENT_UPDATEVersion2,
                new String[] {},SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST,payload,Headers.getCTSHeaderJSON());
        return service.getResponseBody();
    }

    public String getPickupTrackingNoFromSourceReturnId(String returnId,String tenantId,String sourceId) throws UnsupportedEncodingException {
        FGPathparam="?sourceReturnId="+returnId+"&tenantId="+tenantId+"&sourceId="+sourceId;
        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.PICKUP_SHIPMENT_STATUS_LMS,
                new String[] {FGPathparam},SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET,null,Headers.getLmsHeaderJSON());
        Assert.assertEquals(APIUtilities.getElement(service.getResponseBody(),"status.statusMessage","json"),
                "Pickup Shipment retrieved successfully.","pickup Shipment is not Retrieved successfully ");
        return APIUtilities.getElement(service.getResponseBody(),"data.trackingNumber","json");
    }

    /**
     * Used to process return from master bag operation to Delivered to seller operation
     *
     * @param returnId
     * @param returnTrackingNumber
     * @param deliveryCenterCode
     * @throws Exception
     */
    public void processReturnOrderFromDCToWareHouse(Long returnId, String returnTrackingNumber, String deliveryCenterCode,String destinationHubCode) throws Exception {
        //create master bag
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag(deliveryCenterCode, destinationHubCode, ShippingMethod.NORMAL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LMS_CONSTANTS.TENANTID);
        Long masterBagId = masterbagDomain.getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //assign order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, returnTrackingNumber, ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse closeMasterBag = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(closeMasterBag.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());

        //Receive MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(deliveryCenterCode, masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = storeHelper.getMappedTransportHubDetails(LMS_CONSTANTS.TENANTID, deliveryCenterCode);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterCode, transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(deliveryCenterCode, transporterHub, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse addMbIntoContainer = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId);
        Assert.assertEquals(addMbIntoContainer.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");

        //Ship container
        ContainerResponse shipContainer = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(shipContainer.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.IN_TRANSIT.toString());

        //Receive Container In Transport Hub
        ContainerResponse receiveContainerInTMS = (ContainerResponse) storeHelper.receiveContainerInTransportHub.apply(containerId, transporterHub);
        Assert.assertTrue(receiveContainerInTMS.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Container is not received in TRansport hub");

        //MasterBag inscan in WH
        String mbInscanResponse = null;
        mbInscanResponse = storeHelper.masterBagInscanProcess(returnTrackingNumber, masterBagId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                com.myntra.lms.client.status.ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
        if (!(mbInscanResponse.contains(ResponseMessageConstants.success))) {
            //TODO calling this API twice, since it fails 1st time.
            mbInscanResponse = storeHelper.masterBagInscanProcess(returnTrackingNumber, masterBagId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                    com.myntra.lms.client.status.ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
            Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success));
        }
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.IN_TRANSIT.toString());
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RRC.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.DELIVERED_TO_SELLER.toString());
    }


    /**
     * process the return order from Return created state to Pickup successful state
     * @param returnId
     * @param returnMode
     * @param tenantId
     * @param sourceId
     * @throws IOException
     * @throws InterruptedException
     * @throws WebClientException
     * @throws JAXBException
     * @throws XMLStreamException
     * @throws JSONException
     */
    public void processReturnOrderTillPSState(long returnId, ReturnMode returnMode, String tenantId, String sourceId) throws IOException, InterruptedException, WebClientException, JAXBException, XMLStreamException, JSONException {

        OrderResponse orderResponse = lmsServiceHelper.getReturnsInLMS(String.valueOf(returnId));
        String returnTrackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String courierCode = orderResponse.getOrders().get(0).getCourierOperator();
        if(returnMode.toString().equalsIgnoreCase(ReturnMode.OPEN_BOX_PICKUP.toString())){
            //manifest
            storeHelper.manifest(returnMode.toString(),courierCode,tenantId,sourceId);
            String isManifested = storeHelper.checkManifestDone(String.valueOf(returnId),tenantId,Long.parseLong(sourceId));
            Assert.assertEquals(isManifested,"true","manifest is completed successfully");
            //create trip
            Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
            String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
            Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
            TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
            Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
            Long tripId = tripResponse.getTrips().get(0).getId();
            storeValidator.isNull(String.valueOf(tripId));
            Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
            storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

            //assign pickup to trip
            TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId,returnTrackingNumber);
            Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Order is not assigned to Dc trip");

            //start trip
            TripOrderAssignmentResponse tripOrderAssignmentResponse6=lastmileClient.startTrip(tripId.toString(),"10");
            Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripUpdate),"trip is not started for the DC");

            //Validate ML shipment status in DC
            statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.OUT_FOR_PICKUP);

            //get tripOrderAssignment id
            TripOrderAssignmentResponse tripOrderAssignmentResponse = storeHelper.findOrdersByTripId(String.valueOf(tripId), ShipmentType.PU, LMS_CONSTANTS.TENANTID);
            Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
            storeValidator.isNull(storeTripOrderId.toString());
            //update order as Pickup Successful
            TripOrderAssignmentResponse tripOrderAssignmentResponse1 = storeHelper.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE,LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

            //Validate ML shipment status in DC
            statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.PICKUP_SUCCESSFUL);
        }else if(returnMode.toString().equalsIgnoreCase(ReturnMode.CLOSED_BOX_PICKUP.toString())){
            storeHelper.manifest(returnMode.toString(),courierCode,tenantId,sourceId);
            String isManifested = storeHelper.checkManifestDone(String.valueOf(returnId),tenantId,Long.parseLong(sourceId));
            Assert.assertEquals(isManifested,"true","manifest is completed successfully");
            String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(String.valueOf(returnId));
            String courier_code = "DE";
            String shipment_type = "PU";
            String activity_type1 = "PICKUP_DETAILS_UPDATED";
            String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

            String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
            lms_returnHelper.uploadClosedBoxStatusFile(file_path);
            String pickupResponse3= storeHelper.findPickupFromSourceReturnId(String.valueOf(returnId),LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
            //     storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3,ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

            String activity_type2 = "OUT_FOR_PICKUP";
            String activity_sub_type2 = "";
            String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
            lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
            String pickupResponse2= storeHelper.findPickupFromSourceReturnId(String.valueOf(returnId),LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
            storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_PICKUP);

            String activity_type3 = "PICKUP_DONE";
            String activity_sub_type3 = "";
            String file_path3 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
            lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
            String pickupResponse1= storeHelper.findPickupFromSourceReturnId(String.valueOf(returnId),LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
            storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1, com.myntra.logistics.platform.domain.ShipmentStatus.PICKUP_DONE);

            lms_returnHelper.updateReturnReceiveEvents(String.valueOf(returnId), "DE");
            String pickupResponse= storeHelper.findPickupFromSourceReturnId(String.valueOf(returnId),LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
            storeValidator.validatePickupShipmentStatusInLMS(pickupResponse, com.myntra.logistics.platform.domain.ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER);

            //mark QC PASS
            String closeBoxQCUpdate=storeHelper.performClosedBoxReturnQC(String.valueOf(returnId), ItemQCStatus.PASSED,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType);
            Assert.assertEquals(closeBoxQCUpdate,ResponseMessageConstants.qcUpdate,"before approved the green channel, Return order is not allowing to make order status as ON_HOLD");
            //validate return status in rms
            Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
            com.myntra.returns.response.ReturnResponse returnResponse2 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
            storeValidator.validateReturnOrderStatusInRMS(returnResponse2, ReturnStatus.RL);
            //validate return status in LMS
            com.myntra.lms.client.domain.response.ReturnResponse returnResponse3 = storeHelper.getReturnStatusInLMS(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
            storeValidator.validateReturnOrderStatusInLMS(returnResponse3, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL);

        }

    }


}