package com.myntra.apiTests.erpservices.lms.client;
import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.mnm.helpers.MobileNumberMaskingHelper;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.TripAction;
import com.myntra.lastmile.client.code.utils.UpdatedVia;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.TripOrderAssignementEntry;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.TripEntry;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.apache.log4j.Logger;
import org.testng.Assert;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LastmileClient {
    private static Logger log = Logger.getLogger(LastmileClient.class);
    LmsServiceHelper lmsServiceHelper=new LmsServiceHelper();

    public TripResponse createTrip(long deliveryCenterId, long deliveryStaffId)
            {
        TripEntry tripEntry = new TripEntry();
        tripEntry.setTripDate(new Date());
        tripEntry.setDeliveryCenterId(deliveryCenterId);
        tripEntry.setDeliveryStaffId(deliveryStaffId);
        tripEntry.setCreatedBy("Automation");
        tripEntry.setIsInbound(false);
        TripResponse tripResponse=null;
        try {
            String payload = APIUtilities.convertXMLObjectToString(tripEntry);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.TRIP_CREATEVersion2,  new String[]{}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
             tripResponse= (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                    new TripResponse());

        }catch (JAXBException | UnsupportedEncodingException jaxb){
            log.error("ERROR: While creating trip for Delivery center "+deliveryCenterId);
            Assert.fail("FAILED:: While creating trip for Delivery center "+deliveryCenterId+" "+jaxb.getMessage());
        }

        return tripResponse;
    }
    public TripOrderAssignmentResponse assignOrderToTrip(long tripId, String trackingNumber){
        String param="?tenantId="+ LMS_CONSTANTS.TENANTID+"&clientId="+LMS_CONSTANTS.CLIENTID;
        TripOrderAssignmentResponse tripOrderAssignmentResponse = new TripOrderAssignmentResponse();
        TripOrderAssignementEntry tripOrderAssignementEntry = new TripOrderAssignementEntry();
        tripOrderAssignementEntry.setTripId(tripId);
        tripOrderAssignementEntry.setTrackingNumber(trackingNumber);
        tripOrderAssignementEntry.setCreatedBy("Automation");
        List<TripOrderAssignementEntry> tripOrderAssignementEntries = new ArrayList<>();
        tripOrderAssignementEntries.add(tripOrderAssignementEntry);
        tripOrderAssignmentResponse.setTripOrderAssignmentEntries(tripOrderAssignementEntries);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1=null;
        try {
            String payload = APIUtilities.convertXMLObjectToString(tripOrderAssignmentResponse);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.TRIP_ASSIGN_ORDERVersion2, new String[]{param}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse1 = (TripOrderAssignmentResponse) APIUtilities
                    .convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        }catch(JAXBException |UnsupportedEncodingException e){
            log.error("ERROR:: While Assigning order "+trackingNumber+" to the Trip "+tripId);
            Assert.fail("FAILED:: While Assigning order "+trackingNumber+" to the Trip "+tripId+" "+e.getMessage());
        }
        return tripOrderAssignmentResponse1;
    }


    public TripOrderAssignmentResponse startTrip(String tripID, String odometerReading) {
        String param=""+tripID+"/null/scmadmin/"+odometerReading+"?tenantId="+LMS_CONSTANTS.TENANTID+"&clientId="+LMS_CONSTANTS.CLIENTID;
        TripOrderAssignmentResponse tripOrderAssignmentResponse=null;
        try {
            TripOrderAssignmentResponse tripOrderAssignmentResponse1=lmsServiceHelper.getTripOrderDetails(Long.valueOf(tripID));
            List<TripOrderAssignementEntry> tripOrderAssignementEntries=tripOrderAssignmentResponse1.getTripOrders();
            for(int i=0;i<=tripOrderAssignementEntries.size()-1;i++){
                MobileNumberMaskingHelper mobileNumberMaskingHelper=new MobileNumberMaskingHelper();
                mobileNumberMaskingHelper.generateVirtualNumberByTrackingId(tripOrderAssignementEntries.get(i).getTrackingNumber());
            }
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.TRIP_STARTVersion2,
                    new String[]{param}, SERVICE_TYPE.Last_mile.toString(), HTTPMethods.POST,
                    null, Headers.getLmsHeaderXML());
            tripOrderAssignmentResponse = (TripOrderAssignmentResponse) APIUtilities
                    .convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        }catch (UnsupportedEncodingException |JAXBException e){
            log.error("ERROR:: While Starting the Trip with TripId:- "+tripID);
            Assert.fail("FAILED:: While Starting the trip with Trip Id :-"+tripID+" "+e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripOrderAssignmentResponse;
    }

    public TripOrderAssignmentResponse updatePickupInTrip(long tripOrderAssignmentId, String status, String tripAction){
        String param="?tenantId="+ LMS_CONSTANTS.TENANTID+"&clientId="+LMS_CONSTANTS.CLIENTID;
        AttemptReasonCode reasonCode = AttemptReasonCode.PICKED_UP_SUCCESSFULLY;
        if (status.equalsIgnoreCase(EnumSCM.NOT_ABLE_TO_PICKUP)) {
            reasonCode = AttemptReasonCode.CANNOT_PICKUP;
        } else if (status.equalsIgnoreCase(EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING)) {
            reasonCode = AttemptReasonCode.PICKUP_SUCCESSFUL_QC_PENDING;
        } else if (status.equalsIgnoreCase(EnumSCM.RETURNS_CANCELLATION)) {
            reasonCode = AttemptReasonCode.OTHERS;
        } else if (status.equalsIgnoreCase(EnumSCM.RETURN_REJECTED)) {
            reasonCode = AttemptReasonCode.RETURN_QC_FAIL;
        } else if (status.equalsIgnoreCase(EnumSCM.ON_HOLD_DAMAGED_PRODUCT)) {
            reasonCode = AttemptReasonCode.PICKUP_ON_HOLD_DAMAGED_PRODUCT;
        } else if (status.equalsIgnoreCase(EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE)) {
            reasonCode = AttemptReasonCode.REQUESTED_RE_SCHEDULE;
        } else if (status.equals(EnumSCM.LOST)) {
            reasonCode = AttemptReasonCode.OTHERS;
        } else if (status.equals(EnumSCM.FAILED)) {
            reasonCode = AttemptReasonCode.REQUESTED_RE_SCHEDULE;
        }
        TripAction ta = TripAction.TRIP_COMPLETE;
        if (tripAction.equalsIgnoreCase(EnumSCM.UPDATE)) {
            ta = TripAction.UPDATE;
        } else if (tripAction.equalsIgnoreCase("TRIPSTART")) {
            ta = TripAction.TRIP_START;
        } else if (tripAction.equalsIgnoreCase("MARKRETURNSCAN")) {
            ta = TripAction.MARK_RETURNSCAN;
        } else if (tripAction.equalsIgnoreCase("MARKOUTSCAN")) {
            ta = TripAction.MARK_OUTSCAN;
        }
        TripOrderAssignmentResponse tripOrderAssignmentResponse = new TripOrderAssignmentResponse();
        TripOrderAssignementEntry tripOrderAssignementEntry = new TripOrderAssignementEntry();
        tripOrderAssignementEntry.setId(tripOrderAssignmentId);
        tripOrderAssignementEntry.setRemark("test");
        tripOrderAssignementEntry.setAttemptReasonCode(reasonCode);
        tripOrderAssignementEntry.setTripAction(ta);
        tripOrderAssignementEntry.setUpdatedVia(UpdatedVia.WEB);
        tripOrderAssignementEntry.setPaymentType("CASH");
        tripOrderAssignementEntry.setIsOutScanned(true);
        List<TripOrderAssignementEntry> tripOrderAssignementEntries = new ArrayList<>();
        tripOrderAssignementEntries.add(tripOrderAssignementEntry);
        tripOrderAssignmentResponse.setTripOrderAssignmentEntries(tripOrderAssignementEntries);
        TripOrderAssignmentResponse shipmentResponse=null;
        try {
            String payload = APIUtilities.convertXMLObjectToString(tripOrderAssignmentResponse);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.TRIP_UPDATEVersion2, new String[]{param}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
            shipmentResponse = (TripOrderAssignmentResponse) APIUtilities
                    .convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        }catch (JAXBException | UnsupportedEncodingException e){
            log.error("ERROR: While Updating the trip with triporder assignment id "+tripOrderAssignmentId+" to status "+status);
            Assert.fail("FAILED:: While Updating the trip with triporder assignment id "+tripOrderAssignmentId+" to status "+status+" "+e.getMessage());
        }
        return shipmentResponse;
    }

    /**
     * update reverse trip with tenant id
     * @param tripOrderAssignmentId
     * @param status
     * @param tripAction
     * @param tenantId
     * @return
     */
    public TripOrderAssignmentResponse updatePickupInTrip(long tripOrderAssignmentId, String status, String tripAction,String tenantId){
        String param="?tenantId="+tenantId;
        AttemptReasonCode reasonCode = AttemptReasonCode.PICKED_UP_SUCCESSFULLY;
        if (status.equalsIgnoreCase(EnumSCM.NOT_ABLE_TO_PICKUP)) {
            reasonCode = AttemptReasonCode.CANNOT_PICKUP;
        } else if (status.equalsIgnoreCase(EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING)) {
            reasonCode = AttemptReasonCode.PICKUP_SUCCESSFUL_QC_PENDING;
        } else if (status.equalsIgnoreCase(EnumSCM.RETURNS_CANCELLATION)) {
            reasonCode = AttemptReasonCode.OTHERS;
        } else if (status.equalsIgnoreCase(EnumSCM.RETURN_REJECTED)) {
            reasonCode = AttemptReasonCode.RETURN_QC_FAIL;
        } else if (status.equalsIgnoreCase(EnumSCM.ON_HOLD_DAMAGED_PRODUCT)) {
            reasonCode = AttemptReasonCode.PICKUP_ON_HOLD_DAMAGED_PRODUCT;
        } else if (status.equalsIgnoreCase(EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE)) {
            reasonCode = AttemptReasonCode.REQUESTED_RE_SCHEDULE;
        } else if (status.equals(EnumSCM.LOST)) {
            reasonCode = AttemptReasonCode.OTHERS;
        } else if (status.equals(EnumSCM.FAILED)) {
            reasonCode = AttemptReasonCode.REQUESTED_RE_SCHEDULE;
        }else if (status.equals(EnumSCM.HAPPY_WITH_PRODUCT)) {
            reasonCode = AttemptReasonCode.HAPPY_WITH_PRODUCT;
        }
        TripAction ta = TripAction.TRIP_COMPLETE;
        if (tripAction.equalsIgnoreCase(EnumSCM.UPDATE)) {
            ta = TripAction.UPDATE;
        } else if (tripAction.equalsIgnoreCase("TRIPSTART")) {
            ta = TripAction.TRIP_START;
        } else if (tripAction.equalsIgnoreCase("MARKRETURNSCAN")) {
            ta = TripAction.MARK_RETURNSCAN;
        } else if (tripAction.equalsIgnoreCase("MARKOUTSCAN")) {
            ta = TripAction.MARK_OUTSCAN;
        }
        TripOrderAssignmentResponse tripOrderAssignmentResponse = new TripOrderAssignmentResponse();
        List<TripOrderAssignementEntry> tripOrderAssignementEntries = new ArrayList<>();
        for(String trackingNumber : lmsServiceHelper.getTrackingNumbersByTripOrderAssignmentId(tripOrderAssignmentId)) {
            TripOrderAssignementEntry tripOrderAssignementEntry = new TripOrderAssignementEntry();
            tripOrderAssignementEntry.setId(tripOrderAssignmentId);
            tripOrderAssignementEntry.setRemark("test");
            tripOrderAssignementEntry.setAttemptReasonCode(reasonCode);
            tripOrderAssignementEntry.setTripAction(ta);
            tripOrderAssignementEntry.setUpdatedVia(UpdatedVia.WEB);
            tripOrderAssignementEntry.setPaymentType("CASH");
            tripOrderAssignementEntry.setIsOutScanned(true);
            tripOrderAssignementEntry.setTrackingNumber(trackingNumber);
            tripOrderAssignementEntries.add(tripOrderAssignementEntry);
        }
        tripOrderAssignmentResponse.setTripOrderAssignmentEntries(tripOrderAssignementEntries);
        TripOrderAssignmentResponse shipmentResponse=null;
        try {
            String payload = APIUtilities.convertXMLObjectToString(tripOrderAssignmentResponse);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.TRIP_UPDATEVersion2, new String[]{param}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
            shipmentResponse = (TripOrderAssignmentResponse) APIUtilities
                    .convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        }catch (JAXBException | UnsupportedEncodingException e){
            Assert.fail("FAILED:: While Updating the trip with triporder assignment id "+tripOrderAssignmentId+" to status "+status+" "+e.getMessage());
        }
        return shipmentResponse;
    }

    public TripResponse getTripByTripNumber(String tripNumber) {
        String param="search?q=tripNumber.like:"+tripNumber+"&start=0&fetchSize=1&sortBy=id&sortOrder=DESC?tenantId="+LMS_CONSTANTS.TENANTID+"&clientId="+LMS_CONSTANTS.CLIENTID;
        TripResponse tripResponse=null;
        try {
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.TRIPVersion2, new String[]{param}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripResponse= (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                    new TripResponse());
        }catch (UnsupportedEncodingException | JAXBException e){
            log.error("ERROR:: While getting the Trip details using trip Number "+tripNumber);
            Assert.fail("FAILED:: While getting the trip details using trip number"+tripNumber+e.getMessage());
        }
        return tripResponse;
    }
    public TripResponse getTripDetailsByTripId(String tripId){
        TripResponse tripResponse=null;
        String param=""+tripId+"?tenantId="+ LMS_CONSTANTS.TENANTID+"&clientId="+LMS_CONSTANTS.CLIENTID;
        try {
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.TRIPVersion2, new String[]{param}, SERVICE_TYPE.Last_mile.toString(),
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tripResponse = (TripResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                    new TripResponse());
        }catch (JAXBException | UnsupportedEncodingException e){
            log.error("ERROR:: While getting Trip details using Trip");
            Assert.fail("FAILED:: While Fetching Trip Details "+e.getMessage());
        }
        return tripResponse;
    }

    public TripOrderAssignmentResponse findOrdersByTrip(Long tripId, ShipmentType shipmentType) throws UnsupportedEncodingException, JAXBException {

        String param = "" + tripId + "/" + shipmentType + "?tenantId=" + LMS_CONSTANTS.TENANTID;
        Svc service = HttpExecutorService.executeHttpService(Constants.LASTMILE_PATH.FIND_ORDER_BY_TRIP, new String[]{param}, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.GET, null, Headers.getLmsHeaderXML());
        TripOrderAssignmentResponse tripResponse =
                (TripOrderAssignmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());

        return tripResponse;
    }


}
