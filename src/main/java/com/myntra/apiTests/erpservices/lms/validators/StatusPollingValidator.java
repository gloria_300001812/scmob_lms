package com.myntra.apiTests.erpservices.lms.validators;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClientV2_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Helper.NDRServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.exception.ManagerException;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lms.client.response.BulkJobResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.oms.client.response.PacketResponse;
import com.myntra.tms.container.ContainerResponse;
import lombok.SneakyThrows;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * @author Shanmugam Yuvaraj
 * Since: Jul:2019
 */

public class StatusPollingValidator implements StatusPoller {

    /**
     * Used to validate master bag status in Shipment_Order_Map table
     *
     * @param masterBagId
     * @param expectedStatus
     * @throws InterruptedException
     */
    public void validateShipmentBagStatus(Long masterBagId, String expectedStatus) throws InterruptedException {
        ShipmentResponse shipmentResponse=null;
        Integer attemptCount = 0;
        while (true) {
            shipmentResponse = masterBagClient.getShipmentOrderMap(masterBagId);
            if (shipmentResponse.getEntries().get(0).getStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_SHIPMENT * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + shipmentResponse.getEntries().get(0).getStatus() + ", BUt waiting for - " + expectedStatus);
            } else
                Assert.fail("MasterBag status is invalid, expected - " + expectedStatus + ". But found- " + shipmentResponse.getEntries().get(0).getStatus().toString() + " attempt count: " + attemptCount);
        }
    }

    /**
     * USed to validate Order status in Order_To_Ship table
     *
     * @param exchangePacketId
     * @param expectedStatus
     * @throws InterruptedException
     */
    public void validateOrderStatus(String exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus expectedStatus) throws InterruptedException {
        OrderResponse orderResponse;
        Integer attemptCount = 0;
        Thread.sleep(POLLING_INTERVAL);
        while (true) {
            orderResponse = lmsClient.getOrderByOrderId(exchangePacketId);
            if (orderResponse.getOrders().get(0).getPlatformShipmentStatus().toString().equalsIgnoreCase(expectedStatus.toString()))
                break;
            if (attemptCount++ <= (MAXWAIT_ORDER_TO_SHIP * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + orderResponse.getOrders().get(0).getPlatformShipmentStatus() + ", BUt waiting for - " + expectedStatus);
            } else
                Assert.fail("Order status is invalid, expected - " + expectedStatus + ". But found- " + orderResponse.getOrders().get(0).getPlatformShipmentStatus().toString() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate order status in Order_To_Ship table and we can use expected status as String
     *
     * @param exchangePacketId
     * @param expectedStatus
     * @throws InterruptedException
     */
    public void validateOrderStatus(String exchangePacketId, String expectedStatus) throws InterruptedException {
        OrderResponse orderResponse;
        Integer attemptCount = 0;
        Thread.sleep(POLLING_INTERVAL);
        while (true) {
            orderResponse = lmsClient.getOrderByOrderId(exchangePacketId);
            if (orderResponse.getOrders().get(0).getPlatformShipmentStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_ORDER_TO_SHIP * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + orderResponse.getOrders().get(0).getPlatformShipmentStatus() + ", BUt waiting for - " + expectedStatus);
            } else
                Assert.fail("Order status is invalid, expected - " + expectedStatus + ". But found- " + orderResponse.getOrders().get(0).getPlatformShipmentStatus().toString() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate ML_Shipment status in ML_Shipmenttable
     *
     * @param trackingNumber
     * @param tenantId
     * @param expectedStatus
     * @throws InterruptedException
     */
    public void validateML_ShipmentStatus(String trackingNumber, String tenantId, String expectedStatus) throws InterruptedException {
        MLShipmentResponse mlShipmentResponse;
        MLShipmentClientV2_QA mlShipmentServiceV2Client_qa = new MLShipmentClientV2_QA();
        Integer attemptCount = 0;
        int maxNullResponseCount = 10;
        System.out.println("Getting MLShipment status for trackingNumber:" + trackingNumber + " : tenantId:" + tenantId + " : expectedStatus:" + expectedStatus);
        Thread.sleep(1000);
        while (true) {
            mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, tenantId);
            if (mlShipmentResponse.getMlShipmentEntries() == null && (maxNullResponseCount-- <= 0)
                    || mlShipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase("Invalid Shipment Id")) {
                Thread.sleep(1000);
                continue;
            }
            if (mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_ML_SHIPMENT * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus() + ", BUt waiting for - " + expectedStatus);
            } else
                Assert.fail("ML_Shipment status is invalid, expected - " + expectedStatus + ". But found- " + mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().toString() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate Return order status in LMS DB
     *
     * @param returnId
     * @param tenantId
     * @param sourceId
     * @param expectedStatus
     * @throws InterruptedException
     * @throws NumberFormatException
     * @throws IOException
     * @throws JAXBException
     */
    public void validateReturnStatusLMS(String returnId, String tenantId, Long sourceId, String expectedStatus) throws InterruptedException, NumberFormatException, IOException, JAXBException {

        Integer attemptCount = 0;
        while (true) {
            String returnResponse = lmsServiceHelper.getReturnStatusInLMSAsString(returnId, tenantId, sourceId);
            if (returnResponse.equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_RETURN_SHIPMENT * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + returnResponse + ", BUt waiting for - " + expectedStatus);
            } else
                Assert.fail("Return Shipment status is invalid, expected - " + expectedStatus + ". But found- " + returnResponse + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate pickup status in Pickup_shipment table
     *
     * @param returnId
     * @param tenantId
     * @param clientId
     * @param expectedStatus
     * @throws InterruptedException
     * @throws NumberFormatException
     * @throws IOException
     * @throws JAXBException
     */
    public void validatePickupShipmentStatus(String returnId, String tenantId, Long clientId, String expectedStatus) throws InterruptedException, NumberFormatException, IOException, JAXBException {
        String pickupResponse;
        Integer attemptCount = 0;
        while (true) {
            pickupResponse = lmsServiceHelper.findPickupFromSourceReturnId(returnId.toString(), tenantId, clientId);

            if (pickupResponse.toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_PICKUP_SHIPMENT * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + pickupResponse + ", BUt waiting for - " + expectedStatus);

            } else
                Assert.fail("Pickup Shipment status is invalid, expected - " + expectedStatus + ". But found- " + pickupResponse.toString() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate trip status in trip table for virtual number masking concept,
     * it will wait only 2 min to validate trip status
     *
     * @param tripId
     * @param expectedStatus
     * @throws InterruptedException
     * @throws NumberFormatException
     * @throws IOException
     * @throws JAXBException
     */
    public void validateTripStatusWithVNM(Long tripId, String expectedStatus) throws InterruptedException, NumberFormatException, IOException, JAXBException {
        TripResponse tripResponse;
        Integer attemptCount = 0;
        Long MAXWAIT_TRIP_STATUS = 2l;
        while (true) {
            tripResponse = lastmileClient.getTripDetailsByTripId(String.valueOf(tripId));

            if (tripResponse.getTrips().get(0).getTripStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_TRIP_STATUS * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + tripResponse.getTrips().get(0).getTripStatus() + ", BUt waiting for - " + expectedStatus);
            } else
                Assert.fail("Trip status is invalid, expected - " + expectedStatus + ". But found- " + tripResponse.getTrips().get(0).getTripStatus().toString() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate trip status in trip table
     *
     * @param tripId
     * @param expectedStatus
     * @throws InterruptedException
     * @throws NumberFormatException
     * @throws IOException
     * @throws JAXBException
     */
    public void validateTripStatus(Long tripId, String expectedStatus) throws InterruptedException, NumberFormatException, IOException, JAXBException {
        TripResponse tripResponse;
        Integer attemptCount = 0;
        while (true) {
            tripResponse = lastmileClient.getTripDetailsByTripId(String.valueOf(tripId));

            if (tripResponse.getTrips().get(0).getTripStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_TRIP_STATUS * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + tripResponse.getTrips().get(0).getTripStatus() + ", BUt waiting for - " + expectedStatus);
            } else
                Assert.fail("Trip status is invalid, expected - " + expectedStatus + ". But found- " + tripResponse.getTrips().get(0).getTripStatus().toString() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate trip order status in trip_order_assignment table
     *
     * @param tripId
     * @param tenantId
     * @param expectedStatus
     * @throws InterruptedException
     * @throws NumberFormatException
     * @throws IOException
     * @throws JAXBException
     * @throws WebClientException
     */
    public void validateTripOrderStatus(Long tripId, String tenantId, String expectedStatus) throws InterruptedException, NumberFormatException, IOException, JAXBException, WebClientException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse;
        Integer attemptCount = 0;
        TripClient_QA tripClient_qa = new TripClient_QA();
        while (true) {
            tripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripId, tenantId);
            if (tripOrderAssignmentResponse.getTripOrders().get(0).getTripOrderStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_TRIPORDER_STATUS * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + tripOrderAssignmentResponse.getTripOrders().get(0).getTripOrderStatus() + ", BUt waiting for - " + expectedStatus);

            } else
                Assert.fail("Trip order status is invalid, expected - " + expectedStatus + ". But found- " + tripOrderAssignmentResponse.getTripOrders().get(0).getTripOrderStatus().toString() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate return order status in RMS table
     *
     * @param returnId
     * @param expectedStatus
     * @throws InterruptedException
     * @throws NumberFormatException
     * @throws IOException
     * @throws JAXBException
     */
    public void validateReturnStatusRMS(String returnId, String expectedStatus) throws InterruptedException, NumberFormatException, IOException, JAXBException {
        com.myntra.returns.response.ReturnResponse returnResponse;
        Integer attemptCount = 0;
        Thread.sleep(POLLING_INTERVAL);
        while (true) {
            returnResponse = rmsServiceHelper.getReturnDetailsNew(returnId);

            if (returnResponse.getData().get(0).getStatusCode().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_RETURN_STATUS * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + returnResponse.getData().get(0).getStatusCode() + ", BUt waiting for - " + expectedStatus);

            } else
                Assert.fail("Return status in RMS is invalid, expected - " + expectedStatus + ". But found- " + returnResponse.getData().get(0).getStatusCode().toString() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate container status in container table in TMS DB
     *
     * @param containerId
     * @param transporterHub
     * @param expectedStatus
     * @throws InterruptedException
     * @throws JSONException
     * @throws JAXBException
     * @throws ManagerException
     * @throws XMLStreamException
     * @throws IOException
     */
    public void validateContainerStatusInTMS(Long containerId, String transporterHub, String expectedStatus) throws InterruptedException, JSONException, JAXBException, ManagerException, XMLStreamException, IOException {
        ContainerResponse containerResponse;
        Integer attemptCount = 0;
        Thread.sleep(POLLING_INTERVAL);
        while (true) {
            containerResponse = (ContainerResponse) storeHelper.receiveContainerInTransportHub.apply(containerId, transporterHub);

            if (containerResponse.getContainerEntries().get(0).getStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_RETURN_STATUS * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + containerResponse.getContainerEntries().get(0).getStatus() + ", BUt waiting for - " + expectedStatus);

            } else
                Assert.fail("Container status in TMS is invalid, expected - " + expectedStatus + ". But found- " + containerResponse.getContainerEntries().get(0).getStatus() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate packet status in order_line table on OMS DB
     *
     * @param packetId
     * @param ownerId
     * @param buyerId
     * @param expectedStatus
     * @throws Exception
     */
    public void validate_PacketStatusOMS(String packetId, String ownerId, String buyerId, String expectedStatus) throws Exception {
        PacketResponse packetResponse;
        Integer attemptCount = 0;
        Thread.sleep(POLLING_INTERVAL);
        while (true) {
            packetResponse = omsServiceHelper.getPacket(packetId, ownerId, buyerId);

            if (packetResponse.getData().get(0).getStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_RETURN_STATUS * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + packetResponse.getData().get(0).getStatus() + ", BUt waiting for - " + expectedStatus);

            } else
                Assert.fail("Container status in TMS is invalid, expected - " + expectedStatus + ". But found- " + packetResponse.getData().get(0).getStatus() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate packet order line status in order_line table on OMS DB
     *
     * @param packetId
     * @param ownerId
     * @param buyerId
     * @param expectedStatus
     * @throws Exception
     */
    public void validate_PacketOrderLineStatusOMS(String packetId, String ownerId, String buyerId, String expectedStatus) throws Exception {
        PacketResponse packetResponse;
        Integer attemptCount = 0;
        Thread.sleep(POLLING_INTERVAL);
        while (true) {
            packetResponse = omsServiceHelper.getPacket(packetId, ownerId, buyerId);

            if (packetResponse.getData().get(0).getOrderLines().get(0).getStatus().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_RETURN_STATUS * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + packetResponse.getData().get(0).getStatus() + ", BUt waiting for - " + expectedStatus);

            } else
                Assert.fail("Container status in TMS is invalid, expected - " + expectedStatus + ". But found- " + packetResponse.getData().get(0).getStatus() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate the Bulk_job status
     *
     * @param bulkJobId
     * @param expectedStatus
     * @throws JSONException
     * @throws JAXBException
     * @throws XMLStreamException
     * @throws IOException
     * @throws InterruptedException
     */
    public void validate_BulkJob_Status(long bulkJobId, String expectedStatus) throws JAXBException, IOException, InterruptedException, JSONException, XMLStreamException {
        BulkJobResponse bulk_Job_status;
        Integer attemptCount = 0;
        Thread.sleep(POLLING_INTERVAL);
        while (true) {
            bulk_Job_status = NDRServiceHelper.validate_BulkJob_Status(bulkJobId);
            if (bulk_Job_status.getBulkJobEntryList().get(0).getStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_RETURN_STATUS * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("The job Id status" + bulk_Job_status.getStatus().getStatusMessage() + ",But waiting for -" + expectedStatus);
            } else
                Assert.fail("The job status is invalid, expected - " + expectedStatus + ". But found-" + bulk_Job_status.getStatus().getStatusMessage() + "attempt count:" + attemptCount);
        }
    }

    /**
     * Used to validate ML_Shipment NDR status in ML_Shipmenttable
     *
     * @param trackingNumber
     * @param tenantId
     * @param expectedStatus
     * @throws InterruptedException
     */
    public void validateML_ShipmentNDRStatus(String trackingNumber, String tenantId, String expectedStatus) throws InterruptedException {
        MLShipmentResponse mlShipmentResponse;
        MLShipmentClientV2_QA mlshipmentServiceV2Client_qa = new MLShipmentClientV2_QA();
        Integer attemptCount = 0;
        int maxNullResponseCount = 10;
        System.out.println("Getting MLShipment status for trackingNumber:" + trackingNumber + " : tenantId:" + tenantId + " : expectedStatus:" + expectedStatus);
        while (true) {
            mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, tenantId);
            if (mlShipmentResponse.getMlShipmentEntries() == null && (maxNullResponseCount-- > 0)) {
                Thread.sleep(1000);
                continue;
            }
            if (mlShipmentResponse.getMlShipmentEntries().get(0).getNdrStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_ML_SHIPMENT * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + mlShipmentResponse.getMlShipmentEntries().get(0).getNdrStatus() + ", BUt waiting for - " + expectedStatus);
            } else
                Assert.fail("ML_Shipment status is invalid, expected - " + expectedStatus + ". But found- " + mlShipmentResponse.getMlShipmentEntries().get(0).getNdrStatus().toString() + " attempt count: " + attemptCount);
        }
    }

    public TripOrderAssignmentResponse getActiveTripOrdersByDeliveryStaffMobile(String deliveryStaffMobile, String tenantId) throws InterruptedException, UnsupportedEncodingException, JAXBException {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = null;
        Integer attemptCount = 0;
        TripClient_QA tripClient_qa = new TripClient_QA();
        int maxNullResponseCount = 10;
        while (attemptCount < 20) {
            tripOrderAssignmentResponse = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(deliveryStaffMobile, tenantId);
            Thread.sleep(2000);
            Thread.sleep(2000);
            if (tripOrderAssignmentResponse.getTripOrders().size() == 0 && (maxNullResponseCount-- > 0)) {
                continue;
            }
            if (tripOrderAssignmentResponse.getTripOrders().get(0).getTripId() != null) {
                break;
            }
            if (attemptCount++ <= (MAXWAIT_TRIPORDER_STATUS * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - No data found ");

            } else
                Assert.fail("No data found in tripOrderAssignmentResponse.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(trackingNumber)).findFirst().get().getId() ");
        }
        tripOrderAssignmentResponse = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(deliveryStaffMobile, tenantId);
        return tripOrderAssignmentResponse;

    }


    /**
     * Used to validate master bag Order shipment association entries status in Shipment_Order_Map table
     * @param masterBagId
     * @param expectedStatus
     * @throws InterruptedException
     */
    public void validateShipmentBagStatusRespectToOrder(Long masterBagId,String expectedStatus) throws InterruptedException {
        ShipmentResponse shipmentResponse;
        Integer attemptCount = 0;
        while(true) {
            shipmentResponse = masterBagClient.getShipmentOrderMap(masterBagId);
            if(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if(attemptCount++ <= (MAXWAIT_SHIPMENT*30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - "+shipmentResponse.getEntries().get(0).getStatus()+", BUt waiting for - "+expectedStatus);
            }else
                Assert.fail("MasterBag status is invalid, expected - "+ expectedStatus+". But found- "+shipmentResponse.getEntries().get(0).getStatus().toString()+" attempt count: "+attemptCount);
        }
    }

    /**
     * Used to validate trip shipment status in trip_shipment_assignment table
     *
     * @param tripId
     * @param tenantId
     * @param expectedStatus
     * @throws InterruptedException
     * @throws NumberFormatException
     * @throws IOException
     * @throws JAXBException
     * @throws WebClientException
     */
    public void validateTripShipmentStatus(Long tripId, String tenantId, String expectedStatus) throws InterruptedException, NumberFormatException, IOException, JAXBException, WebClientException, XMLStreamException, JSONException {
        TripShipmentAssociationResponse tripOrderAssignmentResponse;
        Integer attemptCount = 0;
        int maxNullResponseCount = 10;
        while (true) {
            tripOrderAssignmentResponse = storeHelper.findShipmentByTrip(tripId, tenantId);
            if (tripOrderAssignmentResponse.getTripShipmentAssociationEntryList().size() == 0 && (maxNullResponseCount-- > 0)) {
                continue;
            }
            if (tripOrderAssignmentResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_TRIPORDER_STATUS * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + tripOrderAssignmentResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString() + ", BUt waiting for - " + expectedStatus);

            } else
                Assert.fail("Trip shipment status is invalid, expected - " + expectedStatus + ". But found- " + tripOrderAssignmentResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString() + " attempt count: " + attemptCount);
        }
    }

    /**
     * Used to validate order status in Order_To_Ship table by different tenant and clientId's
     *
     * @param exchangePacketId
     * @param expectedStatus
     * @throws InterruptedException
     */
    @SneakyThrows
    public void validateOrderStatus(String exchangePacketId,String tenantId,String clientId, String expectedStatus){
        OrderResponse orderResponse;
        Integer attemptCount = 0;
        Thread.sleep(POLLING_INTERVAL);
        while (true) {
            orderResponse = lmsServiceHelper.getOrderDetailsByOrderId(exchangePacketId,tenantId,clientId);
            if (orderResponse.getOrders().get(0).getPlatformShipmentStatus().toString().equalsIgnoreCase(expectedStatus))
                break;
            if (attemptCount++ <= (MAXWAIT_ORDER_TO_SHIP * 30)) {
                Thread.sleep(POLLING_INTERVAL);
                System.out.println("Current status - " + orderResponse.getOrders().get(0).getPlatformShipmentStatus() + ", BUt waiting for - " + expectedStatus);
            } else
                Assert.fail("Order status is invalid, expected - " + expectedStatus + ". But found- " + orderResponse.getOrders().get(0).getPlatformShipmentStatus().toString() + " attempt count: " + attemptCount);
        }
    }


}

