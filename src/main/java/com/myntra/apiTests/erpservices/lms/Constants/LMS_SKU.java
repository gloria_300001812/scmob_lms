package com.myntra.apiTests.erpservices.lms.Constants;

public class LMS_SKU {

	public static final String S1541_36_21_0 = "3866";
	public static final String S1541_36_21_1 = "3867";
	public static final String S1541_36_21_2 = "3868";
	public static final String S1541_36_21_3 = "3869";
	public static final String S1541_28_21_4 = "3870";

	public static final String S1542_28_25_0 = "3871";
	public static final String S1542_36_25_1 = "3872";
	public static final String S1542_36_25_2 = "3873";
	public static final String S1542_28_25_3 = "3874";
	public static final String S1542_36_25_4 = "3875";
	public static final String S1542_28_25_5 = "3876";

	public static final String S1543_36_19_0 = "3877";
	public static final String S1543_28_19_1 = "3878";
	public static final String S1543_36_19_2 = "3879";
	public static final String S1543_19_19_3 = "3880";
	public static final String S1543_36_19_4 = "3881";
}
