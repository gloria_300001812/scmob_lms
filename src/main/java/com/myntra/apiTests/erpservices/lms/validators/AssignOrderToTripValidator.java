package com.myntra.apiTests.erpservices.lms.validators;

import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLShipmentStatus;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import org.testng.Assert;

import java.util.Date;

public class AssignOrderToTripValidator {
    TripOrderAssignmentResponse tripOrderAssignmentResponse;

    public AssignOrderToTripValidator(TripOrderAssignmentResponse tripOrderAssignmentResponse) {
        this.tripOrderAssignmentResponse = tripOrderAssignmentResponse;
    }
    public void validateCreatedOnLastModifiedOn(Date lastModifiedOn,Date createdOn){
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getCreatedOn(),createdOn,"Created on not proper");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getLastModifiedOn(),lastModifiedOn,"Lastmodified on not proper");

    }
    public void validateTripOrderAssignmentId(long tripOrderAssignmentId){
        Assert.assertEquals((long)tripOrderAssignmentResponse.getData().get(0).getId(),tripOrderAssignmentId,"Trip Order Assignment id not proper");
    }
    public void validateTrackingNumber(String tripOrdertrackingNumber){
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getTrackingNumber(),tripOrdertrackingNumber,"Tracking number not proper");

    }
    public void validateTripIdNotificationIsTripCOmpleteIsOutscanned(String version,String tripId,String notificationStatus, boolean isTripCompleted,boolean isOutScanned ){
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getVersion().toString(),version,"version not proper");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getTripId().toString(),tripId,"Trip Id not proper");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getNotificationStatus().toString(),notificationStatus,"NotificationStatus not proper");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getIsTripCompleted(),Boolean.valueOf(isTripCompleted),"Trip complete status not proper");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getIsOutScanned(),Boolean.valueOf(isOutScanned),"Order Outscan status not proper");


    }
    public void validateIsTripStarted(boolean isTripStarted ){
        Assert.assertEquals((boolean)tripOrderAssignmentResponse.getData().get(0).getIsTripStarted(),isTripStarted,"Trip start status not proper");

    }
    public void validateMLShipmentDetails(String createdOn,String id,String orderEntryVersion, String firstName, String address, String city,String zipcode, String mobile,String email,String cod,String codAmount){
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getCreatedOn(),createdOn,"Created On mismatch");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getId().toString(),id,"ML Shipment Id mismatch");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getVersion().toString(),orderEntryVersion,"Ml Shipment version mismatch");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getFirstName(),firstName,"First name not proper");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getAddress(),address,"Address for the shipment is not proper");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getCity(),city,"City mismatch");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getZipcode(),zipcode,"Zipcode is not correct");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getMobile().toString(),mobile,"Mobile number not matching");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getEmail(),email,"Email id not matching");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getCod().toString(),cod,"cod  mismatch");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getCodAmount().toString(),codAmount,"COD Amount not correct");

    }
    public void validateShipmentTypeShippingMethod(ShipmentType shipmentType, ShippingMethod shippingMethod){
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getShipmentType(),shipmentType,"shipmentType  not matched");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getShippingMethod(),shippingMethod,"Shipping method not matched");

    }

    public void validateMLShipmentDetail( String failDeliveryCount,String total,String itemDescription ,String promiseDate, boolean containsHazmat, String rtoWarehouseId,String rtoHubCode,String sourceId){

        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getFailDeliveryCount().toString(),failDeliveryCount,"FailDeliveryCount  not matched");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getTotal().toString(),total,"Total amount  not matched");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getItemDescription(),itemDescription,"Item Description  not matched");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getPromiseDate().toString(),promiseDate,"Promise Date not matched");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getContainsHazmat(),Boolean.valueOf(containsHazmat),"The hazmat status not matched");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getRtoWarehouseId(),rtoWarehouseId,"RTO Ware House Id  not matched");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getRtoHubCode(),rtoHubCode,"RTO Hub Code Count  not matched");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getSourceId(),sourceId,"Source Id  not matched");
 }
    public void validateMLShipmentStatus(MLShipmentStatus status){
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getStatus(),status,"Status  not matched");

    }
    public void validateAssignOrderToTripStatusNdMessage(String statusType,String statusMessage,String statusCode){
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType(), statusType,"Unable to assign order to the trip");
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusMessage(),statusMessage,"Status message to assign order to trip incorrect");
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusCode(),statusCode,"Status code mismatch");
    }
    public void validateTripStatus(TripResponse tripResponse, TripStatus tripStatus){
        Assert.assertEquals(tripResponse.getTrips().get(0).getTripStatus(),tripStatus,"Trip status after trip order assignment not correct.");
    }
    public void validateTripOrderAssignmentStatus(TripOrderStatus tripOrderstatus){
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getTripOrderStatus(),tripOrderstatus,"Trip order assignment status not correct");
    }
    public void validateMLShipmentStatus(MLShipmentResponse mlShipmentResponse, String mlShipmentStatus){
        Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(),mlShipmentStatus,"Shipment status in mlShipment not Updated correctly.");
    }

    public void validateTenantIdClientId(String tenantId, String clientId){
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getTenantId(),tenantId,"Tenant id not proper");
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getOrderEntry().getClientId(),clientId,"Client Id  not matched");
    }
}
