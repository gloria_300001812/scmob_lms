package com.myntra.apiTests.erpservices.lms.lmsClient;

/**
 * Created by Shubham Gupta on 7/21/17.
 */
public class ItemAttribute {
    public static final String JEWELLERY = "JEWELLERY";
    public static final String FRAGILE = "FRAGILE";
    public static final String HAZMAT = "HAZMAT";
    public static final String LARGE = "LARGE";

}
