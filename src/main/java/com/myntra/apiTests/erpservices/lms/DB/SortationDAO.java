package com.myntra.apiTests.erpservices.lms.DB;

import com.myntra.lordoftherings.boromir.DBUtilities;

import java.util.Map;

/**
 * @author Shanmugam Yuvaraj
 * Since: Jul:2019
 */
public class SortationDAO {

    /**
     *  Get sortation config table data from sortation DB
     * @param tenantId
     * @param clientId
     * @param originLocation
     * @param destinationLocation
     * @return
     */
    public static Map<String, Object> getSortationConfigData(String tenantId,String clientId,String originLocation,String destinationLocation,String level) {
        String query = IQueries.formatQuery(IQueries.SortationConfig.sortationConfigData, tenantId,clientId,originLocation,destinationLocation,level);
        Map<String, Object> sortationConfigData = DBUtilities.exSelectQueryForSingleRecord(query, "myntra_sortation");
        return sortationConfigData;
    }


    /**
     *  Get sortLocation config table data from sortation DB
     * @param tenantId
     * @param sourceHub
     * @param name
     * @return
     */
    public static Map<String, Object> getSortLocationData(String tenantId,String sourceHub,String name) {
        String query = IQueries.formatQuery(IQueries.SortationLocationConfig.sortLocationConfigData, tenantId,sourceHub,name);
        Map<String, Object> sortLocationData = DBUtilities.exSelectQueryForSingleRecord(query, "myntra_sortation");
        return sortLocationData;
    }


}
