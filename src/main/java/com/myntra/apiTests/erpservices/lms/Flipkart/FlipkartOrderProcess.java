package com.myntra.apiTests.erpservices.lms.Flipkart;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.lms.client.status.ShippingMethod;
import lombok.SneakyThrows;

public class FlipkartOrderProcess implements StatusPoller {


    /**
     * create mock order for flipKart
     * @param toState
     * @param courierCode
     * @param pincode
     * @param warehouseId
     * @param shippingMethod
     */
    @SneakyThrows
    public String processOrder(String toState,String courierCode,String pincode,String warehouseId, ShippingMethod shippingMethod) {
        String packetId =flipkartHelper.createFlipKartOrder(courierCode,pincode, LMS_CONSTANTS.TENANTID,warehouseId, shippingMethod);
        statusPollingValidator.validateOrderStatus(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID,EnumSCM.PACKED);

        long masterBagId = 0;
        String containerId=null;
        long tripId;
        switch(toState){

            case EnumSCM.PK:
                break;

            case EnumSCM.IS:
                flipkartStateHelper.processPKToISState(packetId);
                break;

            case EnumSCM.ADDED_TO_MB:
                flipkartStateHelper.processPKToISState(packetId);
                flipkartStateHelper.processISToAMBState(packetId);
                break;

            case EnumSCM.SH:
                flipkartStateHelper.processPKToISState(packetId);
                masterBagId = flipkartStateHelper.processISToAMBState(packetId);
                flipkartStateHelper.processAMBToSHState(packetId,masterBagId);
                break;
            case EnumSCM.ASSIGNED_TO_SDA:
                flipkartStateHelper.processPKToISState(packetId);
                masterBagId = flipkartStateHelper.processISToAMBState(packetId);
                containerId = flipkartStateHelper.processAMBToSHState(packetId,masterBagId);
                tripId =flipkartStateHelper.process_SHToASSIGNED_TO_SDA_State(packetId);
                break;
            case EnumSCM.OFD:
                flipkartStateHelper.processPKToISState(packetId);
                masterBagId = flipkartStateHelper.processISToAMBState(packetId);
                containerId = flipkartStateHelper.processAMBToSHState(packetId,masterBagId);
                tripId =flipkartStateHelper.process_SHToASSIGNED_TO_SDA_State(packetId);
                flipkartStateHelper.process_ASSIGNED_TO_SDA_TO_OFD_State(packetId,tripId);
                break;
            case EnumSCM.DL:
                flipkartStateHelper.processPKToISState(packetId);
                masterBagId = flipkartStateHelper.processISToAMBState(packetId);
                containerId = flipkartStateHelper.processAMBToSHState(packetId,masterBagId);
                tripId =flipkartStateHelper.process_SHToASSIGNED_TO_SDA_State(packetId);
                flipkartStateHelper.process_ASSIGNED_TO_SDA_TO_OFD_State(packetId,tripId);
                flipkartStateHelper.process_OFD_TO_DL_State(packetId,tripId);
                break;
        }
        return packetId;
    }






}
