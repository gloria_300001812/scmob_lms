package com.myntra.apiTests.erpservices.lms.Flipkart;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import edu.emory.mathcs.backport.java.util.Arrays;
import lombok.SneakyThrows;

import java.util.Map;

public class FlipkartStateHelper implements StatusPoller {

    /**
     * process order status from PK to IS
     * @param packetId
     */
    @SneakyThrows
    public void processPKToISState(String packetId) {
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsByOrderId(String.valueOf(packetId), LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.FLIPKART_CLIENTID);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        String warehouseId = orderResponse.getOrders().get(0).getWarehouseId();
        lmsOperations.orderInscanV2(trackingNumber, warehouseId, false);
        statusPollingValidator.validateOrderStatus(String.valueOf(packetId), LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.FLIPKART_CLIENTID, EnumSCM.INSCANNED);
    }

    /**
     * process order status from IS to AMB
     * @param packetId
     */
    @SneakyThrows
    public Long processISToAMBState(String packetId) {
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsByOrderId(String.valueOf(packetId),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        String currentHub = orderResponse.getOrders().get(0).getDispatchHubCode();
        String destinationHub = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        Long masterBagId = lmsOperations.masterBagV2Operations(Arrays.asList(new String[] {trackingNumber}), ShippingMethod.NORMAL, CourierCode.ML.toString(),
                currentHub,destinationHub,LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateOrderStatus(String.valueOf(packetId),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID, EnumSCM.ADDED_TO_MB);
        return masterBagId;
    }

    /**
     * process order status from AMB to SH
     * @param packetId
     */
    @SneakyThrows
    public String processAMBToSHState(String packetId,Long masterBagId) {
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsByOrderId(String.valueOf(packetId),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        String currentHub = orderResponse.getOrders().get(0).getDispatchHubCode();
        String destinationHub = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        Map<String, String> containerDetails =tmsOperations.createAndShipForwardContainer(masterBagId,currentHub, destinationHub, LMS_CONSTANTS.TENANTID);
        String containerId = containerDetails.get("containerID");
        statusPollingValidator.validateOrderStatus(String.valueOf(packetId),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID, EnumSCM.SHIPPED);
        statusPollingValidator.validateShipmentBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT.toString());
        tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHub);
        storeHelper.masterBagInscanProcess(trackingNumber, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, deliveryCenterId);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber, LMS_CONSTANTS.TENANTID,EnumSCM.UNASSIGNED);
        return containerId;
    }

    /**
     * process order status from SH to ASSIGNED_TO_SDA
     * @param packetId
     */
    @SneakyThrows
    public Long process_SHToASSIGNED_TO_SDA_State(String packetId) {
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsByOrderId(String.valueOf(packetId),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        String zipcode = orderResponse.getOrders().get(0).getZipcode();

        long tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(zipcode)).get("tripId"));
        lastmileOperations.assignOrderToTripByTrackingNumber(trackingNumber, tripId);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber,LMS_CONSTANTS.TENANTID,EnumSCM.ASSIGNED_TO_SDA);
        return tripId;
    }

    /**
     * process order status from ASSIGNED_TO_SDA to OFD
     * @param packetId
     */
    @SneakyThrows
    public void process_ASSIGNED_TO_SDA_TO_OFD_State(String packetId,long tripId) {
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsByOrderId(String.valueOf(packetId),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        lastmileOperations.startTrip(tripId, trackingNumber);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber, LMS_CONSTANTS.TENANTID,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateOrderStatus(String.valueOf(packetId),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID, EnumSCM.OUT_FOR_DELIVERY);
    }

    /**
     * process order status from OFD to DL
     * @param packetId
     */
    @SneakyThrows
    public void process_OFD_TO_DL_State(String packetId,long tripId) {
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsByOrderId(String.valueOf(packetId),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        lastmileOperations.updateDeliveryTripByTrackingNumber(trackingNumber);
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.COMPLETED);
        statusPollingValidator.validateOrderStatus(String.valueOf(packetId),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID, EnumSCM.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber,LMS_CONSTANTS.TENANTID,EnumSCM.DELIVERED);
    }
}
