package com.myntra.apiTests.erpservices.lms.Constants;

public class LMS_CONSTANTS {
	public static final String CLIENTID="2297";
	public static final String TENANTID="4019";
	public static final String WAREHOUSE_36="36";
	public  static final boolean USE_OMS=false;
	public static final String JABONG_CLIENT_ID="3974";
	public static final String JABONG_CLIENT_ID_="4603";
	public static final String B2B_CLIENT_ID="9999";
	public static final String INTEGRATION_ID_JABONG="4603";
	public static final String SOURCE_PATH_JABONG="JABONG";
	public static final String SOURCE_PATH_MYNTRA="MYNTRA";
	//Usage: USE_TRACKING_NUMBER=true-will use v3 apis(tracking_number)
	public static final boolean USE_TRACKING_NUMBER=true;
	public static final String ML_JABONG_STORE_ID="5";
	public static final String B2B_STORE_ID="5";
	public static final String B2B_CLIENTID="9999";
    public static final String MOBILE_NO="0123456789";
    public static final String RETURN_APPROVE = "APPROVED";
	public static final String RETURN_REJECTED = "REJECTED";
	public static Boolean USE_MASTERBAG_SERVICE = true;
	public static final String FLIPKART_CLIENTID="4816";
	public static final String FLIPKART_SELLER_ID="4842";

	public static final Long wait_Time=5000l;
	public static final String courierType="ML";


	/*
	Usage:- USE_VERSION_V1 =true, use V1 apis.
	//BOTH: USE_TRACKING_NUMBER=false, USE_VERSION_V1 =false---- will use v3 apis
	*/
	public static final boolean USE_VERSION_V1=true;
	//FGVersion=0 uses -> V1 apis  & FGVersion=3 uses v2 Tenant/lastmile apis
	public  static  final int FGVersion=3;
	/*public  String storeid="2297";
	public	String ownerid="2297";
	public String partnerid="4019";*/
	public static final boolean USE_VERSION_V2=true;

    public static String email = "end2endauto1@gmail.com";
    public static String mobilenumber = "1234567890";
    public static String LmsReturnEmailId = "myntralms@myntralms.com";
    public static String sellerId = "25";

    //Constants used for NDR workflow
    public class NDR {
        public static final String NAME = "NDR Report";
        public static final String EXPECTEDSTATUS_REATTEMPT = "REATTEMPT";
        public static final String EXPECTEDSTATUS_IN_PROGRESS = "IN_PROGRESS";
        public static final String PAYLOAD_FORWARD = "forward,ML,4019";
        public static final String PAYLOAD_REVERSE="reverse,ML,4019";


    }

}