package com.myntra.apiTests.erpservices.lms.lmsClient;

/**
 * Created by Shubham Gupta on 7/21/17.
 */
public class ServiceType {
    public static final String DELIVERY = "DELIVERY";
    public static final String EXCHANGE = "EXCHANGE";
    public static final String TRYNBUY = "TRYNBUY";
    public static final String OPEN_BOX_PICKUP = "OPEN_BOX_PICKUP";
    public static final String CLOSED_BOX_PICKUP = "CLOSED_BOX_PICKUP";
    public static final String PICKUP = "PICKUP";

}
