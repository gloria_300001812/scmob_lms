package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.serviceability.client.request.ItemServiceabilityEntry;
import com.myntra.serviceability.client.request.OrderServiceabilityRequest;
import com.myntra.serviceability.client.response.OrderServiceabilityDetailResponse;
import com.myntra.serviceability.client.util.PaymentMode;
import com.myntra.serviceability.client.util.ServiceType;
import com.myntra.serviceability.client.util.ShippingMethod;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ServiceabilityHelper {

    /**
     * Used to check the serviceability for the pincode
     *
     * @param serviceType
     * @param shippingMethod
     * @param paymentMode
     * @param pincode
     * @param wareHouseId
     * @param clientId
     */
    public String checkPincodeServiceability(ServiceType serviceType, ShippingMethod shippingMethod, PaymentMode paymentMode, String pincode,
                                             String wareHouseId, String clientId) throws IOException {

        OrderServiceabilityRequest orderServiceabilityRequest = new OrderServiceabilityRequest();
        orderServiceabilityRequest.setServiceType(serviceType);
        orderServiceabilityRequest.setShippingMethod(shippingMethod);
        orderServiceabilityRequest.setPaymentMode(paymentMode);
        orderServiceabilityRequest.setPincode(Long.parseLong(pincode));
        orderServiceabilityRequest.setShipmentId("1234");
        orderServiceabilityRequest.setEnrichItemAttributes(false);
        orderServiceabilityRequest.setClientId(clientId);

        ItemServiceabilityEntry itemServiceabilityEntry = new ItemServiceabilityEntry();
        itemServiceabilityEntry.setIsHazmat(false);
        itemServiceabilityEntry.setIsJewellery(false);
        itemServiceabilityEntry.setIsLarge(false);
        itemServiceabilityEntry.setIsFragile(true);
        itemServiceabilityEntry.setOpenBoxPickupEnabled(true);
        itemServiceabilityEntry.setIsReturnable(true);
        itemServiceabilityEntry.setTryAndBuyEnabled(true);
        itemServiceabilityEntry.setSkuId("12345");
        Set<String> whId = new HashSet<>();
        whId.add(wareHouseId);
        itemServiceabilityEntry.setAvailableInWarehouses(whId);
        List<ItemServiceabilityEntry> lstItemServiceabilityEntry = new ArrayList<>();
        lstItemServiceabilityEntry.add(itemServiceabilityEntry);
        orderServiceabilityRequest.setItems(lstItemServiceabilityEntry);
        String payload = APIUtilities.getObjectToJSON(orderServiceabilityRequest);

        Svc service = HttpExecutorService.executeHttpService("promiseDate/fetchDetail", null, "promise",
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        Assert.assertEquals(APIUtilities.getElement(service.getResponseBody(), "status.statusMessage", "json"), ResponseMessageConstants.success,"unable to get the data for serviceability");
        return APIUtilities.getElement(service.getResponseBody(), "itemServiceabilityEntries[0].serviceTypeServiceabilityEntries[0].serviceType", "json");
    }

}
