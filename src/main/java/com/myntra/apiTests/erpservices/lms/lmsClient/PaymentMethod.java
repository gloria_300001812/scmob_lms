package com.myntra.apiTests.erpservices.lms.lmsClient;

/**
 * Created by Shubham Gupta on 7/21/17.
 */
public class PaymentMethod {
    public static final String cod = "cod";
    public static final String on = "on";

    public static String getCod() {
        return cod;
    }

    public static String getOn() {
        return on;
    }
}
