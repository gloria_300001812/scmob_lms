package com.myntra.apiTests.erpservices.lms.validators;

import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.DB.IQueries;
import com.myntra.lms.client.domain.response.ReturnResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.OrderTrackingDetailEntryV2;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class OrderTabSearchValidator implements StatusPoller{

    /**
     * Used to validate the from state and to states of order tracking details
     *
     * @param orderTrackingEntriesApi
     * @param orderTrackingEntriesOrdersTab
     */
    public void validateOrdersTrackDetails(List<OrderTrackingDetailEntryV2> orderTrackingEntriesApi, List<OrderTrackingDetailEntryV2> orderTrackingEntriesOrdersTab) {
        SoftAssert softAssert = new SoftAssert();
        for (int i = 0; i <= orderTrackingEntriesOrdersTab.size() - 1; i++) {
            if (orderTrackingEntriesApi.get(i).getActivityType().equalsIgnoreCase(orderTrackingEntriesOrdersTab.get(i).getActivityType())) {
                softAssert.assertEquals(orderTrackingEntriesApi.get(i).getFromStatus(), orderTrackingEntriesOrdersTab.get(i).getFromStatus(), "From state is not matching when activity type is " + orderTrackingEntriesApi.get(i).getActivityType());
                softAssert.assertEquals(orderTrackingEntriesApi.get(i).getToStatus(), orderTrackingEntriesOrdersTab.get(i).getToStatus(), "To state is not matching when activity type is " + orderTrackingEntriesApi.get(i).getActivityType());
            } else {
                if (i == orderTrackingEntriesOrdersTab.size() - 1) {
                    softAssert.fail("The following Activity type is not found in api " + orderTrackingEntriesApi.get(i).getActivityType());
                }
            }
        }
        softAssert.assertAll();
    }

    /**
     * Validate order to ship data in orders search tab
     * @param orderResponseDashBoard
     * @param getOrder_To_Ship_Data
     * @param noOfOrders
     */
    public void validate_DashBoardSearch_OrderToShip(OrderResponse orderResponseDashBoard, List<Map<String, Object>> getOrder_To_Ship_Data, int noOfOrders) {
        try {
            Assert.assertEquals(orderResponseDashBoard.getStatus().getTotalCount(), noOfOrders, "Order total count is not matching,passing two data but found record for only one");
            for (int i = 0; i <= noOfOrders - 1; i++) {
                OrderResponse orderResponseOrderToShip = lmsClient.getOrderByOrderId(getOrder_To_Ship_Data.get(i).get("order_id").toString());
                if (Objects.nonNull(orderResponseDashBoard.getOrders()) || orderResponseDashBoard.getOrders().size() > 0 && orderResponseOrderToShip.getOrders() != null || orderResponseOrderToShip.getOrders().size() > 0) {
                    Assert.assertEquals(orderResponseDashBoard.getOrders().get(i).getOrderId(), orderResponseOrderToShip.getOrders().get(0).getOrderId(), "OrderID is not matching from dash board response and order to ship api response ");
                    Assert.assertEquals(orderResponseDashBoard.getOrders().get(i).getTrackingNumber(), orderResponseOrderToShip.getOrders().get(0).getTrackingNumber(), "Tracking number is not matching from dash board response and order to ship api response ");
                    Assert.assertEquals(orderResponseDashBoard.getOrders().get(i).getStatus().toString(), orderResponseOrderToShip.getOrders().get(0).getStatus().toString(), " status is not matching from dash board response and order to ship api response ");
                    Assert.assertEquals(orderResponseDashBoard.getOrders().get(i).getPlatformShipmentStatus().toString(), orderResponseOrderToShip.getOrders().get(0).getPlatformShipmentStatus().toString(), "platform shipment status is not matching from dash board response and order to ship api response ");
                    Assert.assertEquals(orderResponseDashBoard.getOrders().get(i).getShipmentType().toString(), orderResponseOrderToShip.getOrders().get(0).getShipmentType().toString(), "Shipment Type is not matching from dash board response and order to ship api response ");
                } else {
                    Assert.fail("orders size is not >0 or orders is null");
                }
            }
        } catch (NullPointerException n) {
            n.printStackTrace();
            Assert.fail("null pointer exception accrued - "+n.getStackTrace());
        }
    }

    /**
     * Validate order to ship multiple data in orders tab
     * @param orderResponseDashBoard
     * @param getOrderToShipData
     * @param noOfOrders
     */
    public void validate_DashBoardOrdersTabSearch(OrderResponse orderResponseDashBoard, List<Map<String, Object>> getOrderToShipData, int noOfOrders) {
        try {
            Assert.assertEquals(orderResponseDashBoard.getStatus().getTotalCount(), noOfOrders, "Order total count is not matching,passing two data but found record for only one");
            for (int i = 0; i <= noOfOrders - 1; i++) {
                Map<String, Object> getOrderId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.get_OrderToShip_data, getOrderToShipData.get(i).get("tracking_number").toString()), "myntra_lms");
                OrderResponse orderResponseOrderToShip = lmsClient.getOrderByOrderId(getOrderId.get("order_id").toString());
                if (orderResponseDashBoard.getOrders().size() > 0 && orderResponseOrderToShip.getOrders().size() > 0) {
                    Assert.assertEquals(orderResponseDashBoard.getOrders().get(i).getOrderId(), orderResponseOrderToShip.getOrders().get(0).getOrderId(), "OrderID is not matching from dash board response and order to ship api response ");
                    Assert.assertEquals(orderResponseDashBoard.getOrders().get(i).getTrackingNumber(), orderResponseOrderToShip.getOrders().get(0).getTrackingNumber(), "Tracking number is not matching from dash board response and order to ship api response ");
                    Assert.assertEquals(orderResponseDashBoard.getOrders().get(i).getStatus().toString(), orderResponseOrderToShip.getOrders().get(0).getStatus().toString(), " status is not matching from dash board response and order to ship api response ");
                    Assert.assertEquals(orderResponseDashBoard.getOrders().get(i).getPlatformShipmentStatus().toString(), orderResponseOrderToShip.getOrders().get(0).getPlatformShipmentStatus().toString(), "platform shipment status is not matching from dash board response and order to ship api response ");
                    Assert.assertEquals(orderResponseDashBoard.getOrders().get(i).getShipmentType().toString(), orderResponseOrderToShip.getOrders().get(0).getShipmentType().toString(), "Shipment Type is not matching from dash board response and order to ship api response ");
                } else {
                    Assert.fail("orders size is not >0 or orders is null");
                }
            }
        } catch (NullPointerException n) {
            Assert.fail("null pointer exception accrued - "+n.getStackTrace());
        }
    }

    /**
     * validate pickup shipment data in orders tab
     * @param orderResponse
     * @param getPickUpShipmentData
     * @param noOfOrders
     */
    public void validate_pickupShipment_OrderSearchTab_Details(OrderResponse orderResponse, List<Map<String, Object>> getPickUpShipmentData, int noOfOrders) {
        try {
            Assert.assertEquals(orderResponse.getStatus().getTotalCount(), noOfOrders, "Order total count is not matching,passing two data but found record for only one");
            for (int i = 0; i <= noOfOrders - 1; i++) {
                Map<String, Object> getReturnShipmentData = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.get_ReturnShipment_Data, getPickUpShipmentData.get(i).get("tracking_number").toString()), "myntra_lms");

                ReturnResponse returnResponse = returnHelper.getReturnStatusInLMS(getReturnShipmentData.get("source_return_id").toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
                if (orderResponse.getOrders().size() > 0 && returnResponse.getDomainReturnShipments().size() > 0) {
                    Assert.assertEquals(orderResponse.getOrders().get(i).getTrackingNumber(), returnResponse.getDomainReturnShipments().get(0).getTrackingNumber(), "tracking Number is not matching from dashboard search to return shipment api");
                    Assert.assertEquals(orderResponse.getOrders().get(i).getStatus().toString(), returnResponse.getDomainReturnShipments().get(0).getStatus().toString(), "shipment status is not matching from dashboard search to return shipment api");
                } else {
                    Assert.fail("orders size is not >0 or orders is null");
                }
            }
        } catch (NullPointerException | IOException | JAXBException n) {
            Assert.fail("null pointer exception accrued -"+ n.getStackTrace());
        }
    }
}
