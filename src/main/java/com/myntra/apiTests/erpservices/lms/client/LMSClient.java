package com.myntra.apiTests.erpservices.lms.client;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;

public class LMSClient {

    public OrderResponse getOrderByOrderId(String orderId){
        OrderResponse orderResponse=null;
        try {
            String param = ""+orderId+"?tenantId=" + LMS_CONSTANTS.TENANTID + "&clientId=" + LMS_CONSTANTS.CLIENTID;
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.ORDER_BY_ID, new String[]{param}, SERVICE_TYPE.LMS_SVC.toString(),
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            orderResponse = (OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                    new OrderResponse());
        }catch (UnsupportedEncodingException | JAXBException e){
            Assert.fail("FAILED:: While getting the order details using  Order Id"+e.getMessage());

        }
        return orderResponse;

    }

}
