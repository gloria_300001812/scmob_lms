package com.myntra.apiTests.erpservices.lms.Helper;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.domain.response.DomainShipmentUpdateResponse;
import com.myntra.lms.client.domain.response.PickupResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.OrderTrackingResponse;
import com.myntra.lms.client.status.CourierCreationStatus;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.*;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.entry.ReturnEntry;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS.CLIENTID;
import static com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS.TENANTID;
import static com.myntra.logistics.platform.domain.Premise.PremiseType.DELIVERY_CENTER;

/*import com.myntra.apiTests.erpservices.lms.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.Headers;*/

public class LMS_ReturnHelper {
	static Initialize init = new Initialize("/Data/configuration");
	String env = System.getenv("environment");
	static Logger log = Logger.getLogger(LMS_ReturnHelper.class);
	LMSHelper lmsHelper = new LMSHelper();
	LmsServiceHelper lmsServiceHelper= new LmsServiceHelper();
	RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();

	/**
	 * validateRmsLmsReturnCreation
	 *
	 * @param returnId
	 * @return
	 */

	public boolean validateOpenBoxRmsLmsReturnCreationV2(String returnId) throws IOException, ManagerException,
			JAXBException, InterruptedException, XMLStreamException, JSONException, SQLException {

		RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
		//Get the return_shipment details as a OrderResponse object, get return from RMS
		Thread.sleep(10000);
		OrderResponse returnShipmentLMSObject = getReturnShipmentDetailsLMS(returnId);

		if(returnShipmentLMSObject.getOrders() != null && !returnShipmentLMSObject.getOrders().isEmpty()) {

			Map<String, Object> returnShipmentLMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from return_shipment where source_return_id = " + returnId, "lms");

			Map<String, Object> returnRMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from returns where id = " + returnId, "rms");
			ReturnResponse returnResponseRMSObj = rmsServiceHelper.getReturnDetailsNew(String.valueOf(Long.parseLong(returnId)));

			//Basic validations : return_type=OPEN_BOX, courier_code=ML
			Thread.sleep(Constants.LMS_PATH.sleepTime);
			ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getCourierOperator().equals("ML"), "Courier code assigned to this return is not ML");
			ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_type").equals("OPEN_BOX_PICKUP"), "The return type is not OPEN_BOX_PICKUP in LMS return");
			ExceptionHandler.handleTrue(returnRMSDB.get("return_mode").equals("OPEN_BOX_PICKUP"), "The return type is not OPEN_BOX_PICKUP in RMS return");

			//1. Validate if return_shipment  entry is created in LMS  with status "RETURN_CREATED"
			ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.RETURN_CREATED.toString(), 15), "Return-shipment is not created with status RETURN_CREATED in LMS");

			//2. Verify the status in RMS initially is LPI
			ExceptionHandler.handleTrue(rmsServiceHelper.validateReturnStatusInRMS(String.valueOf(Long.parseLong(returnId)), ReturnStatus.LPI.toString(), 15), "pickup is not created or not in LPI status in Returns");

			//3. Verify pickup is created with status PICKUP_CREATED
			ExceptionHandler.handleTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_CREATED.toString(), 15), "Pickup-shipment is not created with status PICKUP_CREATED in LMS");

			//4. Verify the right return warehouse id  and return hub code is populated in return_shipment
			String orderId = returnResponseRMSObj.getData().get(0).getOrderId().toString();
			OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
			OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
			String expectedReturnWHid = String.valueOf(orderReleaseEntry.getWarehouseId());

			String expectedReturnHub = lmsHelper.getReturnHubCodeForWarehouse.apply(Long.parseLong(expectedReturnWHid)).toString();
			String actualReturnHub = getReturnWarehouseHub(returnId);
			String actualReturnWHid = getReturnWarehouseId(returnId);
			ExceptionHandler.handleTrue(actualReturnWHid.equals(expectedReturnWHid), "The return warehouse id stamped in return_shipment is not = the return_hub_warehouse configured");
			ExceptionHandler.handleTrue(expectedReturnHub.equals(actualReturnHub), "The return hub stamped in return_shipment is not = the return_hub_warehouse configured");


			//5. Verify the DC code is populated correctly
			long actualDCcode = returnShipmentLMSObject.getOrders().get(0).getDeliveryCenterId();
			String packetId = omsServiceHelper.getPacketId(orderId);
			long expectedDCcode = (long) lmsHelper.getDCCodeForRelease.apply(packetId);
			ExceptionHandler.handleTrue(actualDCcode == expectedDCcode, "The DC code from order_release not equal to dc code stamped in return_shipment");

			//6. Verify return_shipment value = RMS refund value
//		ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getItemEntries().get(0).getItemValue().toString().equals(returnResponseRMSObj.getData().get(0).getReturnLineEntries().get(0).getReturnLineRefundDetailsEntry().getRefundAmount().toString()), "LMS Return_shipment shipment value not equal to RMS return refund value");


			//Verify return_shipment status = RIT
			ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getStatus().toString().equals(ShipmentStatus.valueOf("RETURN_CREATED").getShipmentStatusCode().toString()), "LMS return_shipment status is not RIT");

			//8. Verify rms (returns table) tracking number = LMS return_shipment tracking number
			log.info("The rms - return tracking number" + returnResponseRMSObj.getData().get(0).getReturnTrackingDetailsEntry());
			ExceptionHandler.handleTrue((returnResponseRMSObj.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo().toString().equals(returnShipmentLMSObject.getOrders().get(0).getTrackingNumber().toString())), "LMS return_shipment tracking number != RMS return tracking number");

			//9. Verify rms(return table) and lms (return_shipment) return entry data
			ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_type").toString().equals(returnResponseRMSObj.getData().get(0).getReturnMode().toString()), "return_mode/return_type Mismatch in LMS-RMS");

			//10 . Verify  LMS-RMS return mode
			ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_type").toString().equals(returnResponseRMSObj.getData().get(0).getReturnMode().toString()), "LMS RMS return_mode match error");

			//11. Verify RMS LMS email id
			String emailid=returnShipmentLMSObject.getOrders().get(0).getEmail();

			Assert.assertTrue(returnResponseRMSObj.getData().get(0).getEmail().trim().toLowerCase().equals("end2endauto1@gmail.com"), "email Mismatch in LMS-RMS");
			Assert.assertTrue(emailid.equals(LMS_CONSTANTS.LmsReturnEmailId), "email in LMS is not myntralms@myntralms.com");

			String pickup_shipment_id = getAssociatedPickupForReturn(returnId);

			//12. Using the pickup id , get the tracking number stamped in Order tracking table,Verify pickup tracking number and order_tracking's orderId and TrackingNumber match
			String trackingNumber_orderTracking = getOrderTrackingNumber(pickup_shipment_id);
			com.myntra.lms.client.domain.response.PickupResponse pickupResponse = getPickupShipmentDetailsLMS(pickup_shipment_id);
			String trackingNumber_pickupShipment = pickupResponse.getDomainPickupShipments().get(0).getTrackingNumber();

			ExceptionHandler.handleTrue(trackingNumber_orderTracking.toString().equals(trackingNumber_pickupShipment.toString()), "Order Tracking tracking_number !+ pickup_shipment tracking number");

			Map<String, Object> orderTrackingLMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from order_tracking where tracking_no = '" + trackingNumber_pickupShipment + "'", "lms");

			//13. Verify Order_tracking statuses
			ExceptionHandler.handleTrue(returnShipmentLMSDB.get("courier_code").toString().equals("ML"), "courier Mismatch in LMS-RMS");
			validateOrderTrackingShipmentStatus(trackingNumber_pickupShipment, ShipmentStatus.PICKUP_CREATED);
			//	validateOrderTrackingCourierCreationStatus(trackingNumber_pickupShipment, CourierCreationStatus.NOT_INITIATED);
			validateOrderTrackingDeliveryStatus(trackingNumber_pickupShipment, ShipmentStatus.RETURN_CREATED.getOrderTrackingStatusCode().toString());
			ExceptionHandler.handleTrue(orderTrackingLMSDB.get("shipment_status").toString().equals(ShipmentStatus.PICKUP_CREATED.toString()), "order_tracking table shipment_status is not PICKUP_CREATED");

			//14. Verify order_Tracking_details
			List<String> activityTypeStatusCombination = new ArrayList<>();
			activityTypeStatusCombination.add("PICKUP_CREATED:PICKUP_CREATED:PICKUP_CREATED");
			activityTypeStatusCombination.add("PICKUP_DETAILS_UPDATED:PICKUP_CREATED:PICKUP_CREATED");
			//TODO: Check why failing
			//validateOrderTrackingDetailsForActivity(trackingNumber_orderTracking,activityTypeStatusCombination);
		} else {

			log.error("ReturnShipmentLMSObject is null or empty - "+returnShipmentLMSObject);
			Assert.fail("Return is not created in LMS , check queue `rmsLogisticsCreationQueue` or check if LMS3pl/LMS /RMS is up");
		}
		return true;
	}


	/**
	 * validateRmsLmsReturnCreation
	 * above method is not working for nearest wh restock scenario
	 *
	 * @param returnId
	 * @return
	 */

	public boolean validateOpenBoxRmsLmsReturnCreationV2ForNearestWHRestock(String returnId,String expectedReturnWHid,String expectedReturnHub) throws IOException, ManagerException,
			JAXBException, InterruptedException, XMLStreamException, JSONException, SQLException {

		RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
		//Get the return_shipment details as a OrderResponse object, get return from RMS
		Thread.sleep(10000);
		OrderResponse returnShipmentLMSObject = getReturnShipmentDetailsLMS(returnId);

		if(returnShipmentLMSObject.getOrders() != null && !returnShipmentLMSObject.getOrders().isEmpty()) {

			Map<String, Object> returnShipmentLMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from return_shipment where source_return_id = " + returnId, "lms");

			Map<String, Object> returnRMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from returns where id = " + returnId, "rms");
			ReturnResponse returnResponseRMSObj = rmsServiceHelper.getReturnDetailsNew(String.valueOf(Long.parseLong(returnId)));

			//Basic validations : return_type=OPEN_BOX, courier_code=ML
			Thread.sleep(Constants.LMS_PATH.sleepTime);
			ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getCourierOperator().equals("ML"), "Courier code assigned to this return is not ML");
			ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_type").equals("OPEN_BOX_PICKUP"), "The return type is not OPEN_BOX_PICKUP in LMS return");
			ExceptionHandler.handleTrue(returnRMSDB.get("return_mode").equals("OPEN_BOX_PICKUP"), "The return type is not OPEN_BOX_PICKUP in RMS return");

			//1. Validate if return_shipment  entry is created in LMS  with status "RETURN_CREATED"
			ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.RETURN_CREATED.toString(), 15), "Return-shipment is not created with status RETURN_CREATED in LMS");

			//2. Verify the status in RMS initially is LPI
			ExceptionHandler.handleTrue(rmsServiceHelper.validateReturnStatusInRMS(String.valueOf(Long.parseLong(returnId)), ReturnStatus.LPI.toString(), 15), "pickup is not created or not in LPI status in Returns");

			//3. Verify pickup is created with status PICKUP_CREATED
			ExceptionHandler.handleTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_CREATED.toString(), 15), "Pickup-shipment is not created with status PICKUP_CREATED in LMS");

			/*//4. Verify the right return warehouse id  and return hub code is populated in return_shipment

			OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
			//String expectedReturnWHid = String.valueOf(orderReleaseEntry.getWarehouseId());

			String expectedReturnHub = lmsHelper.getReturnHubCodeForWarehouse.apply(Long.parseLong(expectedReturnWHid)).toString();*/
			String actualReturnHub = getReturnWarehouseHub(returnId);
			String actualReturnWHid = getReturnWarehouseId(returnId);
			ExceptionHandler.handleTrue(actualReturnWHid.equals(expectedReturnWHid), "The return warehouse id stamped in return_shipment is not = the return_hub_warehouse configured");
			ExceptionHandler.handleTrue(expectedReturnHub.equals(actualReturnHub), "The return hub stamped in return_shipment is not = the return_hub_warehouse configured");


			//5. Verify the DC code is populated correctly
			OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
			String orderId = returnResponseRMSObj.getData().get(0).getOrderId().toString();
			long actualDCcode = returnShipmentLMSObject.getOrders().get(0).getDeliveryCenterId();
			String packetId = omsServiceHelper.getPacketId(orderId);
			long expectedDCcode = (long) lmsHelper.getDCCodeForRelease.apply(packetId);
			ExceptionHandler.handleTrue(actualDCcode == expectedDCcode, "The DC code from order_release not equal to dc code stamped in return_shipment");

			//6. Verify return_shipment value = RMS refund value
//		ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getItemEntries().get(0).getItemValue().toString().equals(returnResponseRMSObj.getData().get(0).getReturnLineEntries().get(0).getReturnLineRefundDetailsEntry().getRefundAmount().toString()), "LMS Return_shipment shipment value not equal to RMS return refund value");


			//Verify return_shipment status = RIT
			ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getStatus().toString().equals(ShipmentStatus.valueOf("RETURN_CREATED").getShipmentStatusCode().toString()), "LMS return_shipment status is not RIT");

			//8. Verify rms (returns table) tracking number = LMS return_shipment tracking number
			log.info("The rms - return tracking number" + returnResponseRMSObj.getData().get(0).getReturnTrackingDetailsEntry());
			ExceptionHandler.handleTrue((returnResponseRMSObj.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo().toString().equals(returnShipmentLMSObject.getOrders().get(0).getTrackingNumber().toString())), "LMS return_shipment tracking number != RMS return tracking number");

			//9. Verify rms(return table) and lms (return_shipment) return entry data
			ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_type").toString().equals(returnResponseRMSObj.getData().get(0).getReturnMode().toString()), "return_mode/return_type Mismatch in LMS-RMS");

			//10 . Verify  LMS-RMS return mode
			ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_type").toString().equals(returnResponseRMSObj.getData().get(0).getReturnMode().toString()), "LMS RMS return_mode match error");

			//11. Verify RMS LMS email id
			String emailid=returnShipmentLMSObject.getOrders().get(0).getEmail();

			Assert.assertTrue(returnResponseRMSObj.getData().get(0).getEmail().trim().toLowerCase().equals("end2endauto1@gmail.com"), "email Mismatch in LMS-RMS");
			Assert.assertTrue(emailid.equals(LMS_CONSTANTS.LmsReturnEmailId), "email in LMS is not myntralms@myntralms.com");

			String pickup_shipment_id = getAssociatedPickupForReturn(returnId);

			//12. Using the pickup id , get the tracking number stamped in Order tracking table,Verify pickup tracking number and order_tracking's orderId and TrackingNumber match
			String trackingNumber_orderTracking = getOrderTrackingNumber(pickup_shipment_id);
			com.myntra.lms.client.domain.response.PickupResponse pickupResponse = getPickupShipmentDetailsLMS(pickup_shipment_id);
			String trackingNumber_pickupShipment = pickupResponse.getDomainPickupShipments().get(0).getTrackingNumber();

			ExceptionHandler.handleTrue(trackingNumber_orderTracking.toString().equals(trackingNumber_pickupShipment.toString()), "Order Tracking tracking_number !+ pickup_shipment tracking number");

			Map<String, Object> orderTrackingLMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from order_tracking where tracking_no = '" + trackingNumber_pickupShipment + "'", "lms");

			//13. Verify Order_tracking statuses
			ExceptionHandler.handleTrue(returnShipmentLMSDB.get("courier_code").toString().equals("ML"), "courier Mismatch in LMS-RMS");
			validateOrderTrackingShipmentStatus(trackingNumber_pickupShipment, ShipmentStatus.PICKUP_CREATED);
			//TODO: gloria asked to remove this assertion
			//	validateOrderTrackingCourierCreationStatus(trackingNumber_pickupShipment, CourierCreationStatus.NOT_INITIATED);
			validateOrderTrackingDeliveryStatus(trackingNumber_pickupShipment, ShipmentStatus.RETURN_CREATED.getOrderTrackingStatusCode().toString());
			ExceptionHandler.handleTrue(orderTrackingLMSDB.get("shipment_status").toString().equals(ShipmentStatus.PICKUP_CREATED.toString()), "order_tracking table shipment_status is not PICKUP_CREATED");

			//14. Verify order_Tracking_details
			List<String> activityTypeStatusCombination = new ArrayList<>();
			activityTypeStatusCombination.add("PICKUP_CREATED:PICKUP_CREATED:PICKUP_CREATED");
			activityTypeStatusCombination.add("PICKUP_DETAILS_UPDATED:PICKUP_CREATED:PICKUP_CREATED");
			//TODO: Check why failing
			//validateOrderTrackingDetailsForActivity(trackingNumber_orderTracking,activityTypeStatusCombination);
		} else {

			log.error("ReturnShipmentLMSObject is null or empty - "+returnShipmentLMSObject);
			Assert.fail("Return is not created in LMS , check queue `rmsLogisticsCreationQueue` or check if LMS3pl/LMS /RMS is up");
		}
		return true;
	}

	public void validateOrderTrackingCourierCreationStatus(String trackingNumber, CourierCreationStatus status) throws ManagerException {
		Map<String, Object> orderTrackingLMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from order_tracking where tracking_no = '" + trackingNumber + "'", "lms");
		ExceptionHandler.handleTrue(orderTrackingLMSDB.get("courier_creation_status").equals(status.toString()), "order_Tracking table - courier_creation_status is not " + status);
	}

	private void validateOrderTrackingShipmentStatus(String trackingNumber, ShipmentStatus status) throws ManagerException {
		Map<String, Object> orderTrackingLMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from order_tracking where tracking_no = '" + trackingNumber + "'", "lms");
		ExceptionHandler.handleTrue(orderTrackingLMSDB.get("shipment_status").equals(status.toString()), "order_Tracking table - shipment status is not " + status);

	}


	public void validateMLShipmentStatus(String trackingNumber, String status) throws ManagerException {
		Map<String, Object> orderTrackingLMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from ml_shipment where tracking_number = '" + trackingNumber + "'", "lms");
		ExceptionHandler.handleTrue(orderTrackingLMSDB.get("shipment_status").equals(status.toString()), "ML_shipment table - shipment status is not " + status);
	}


	private void validateOrderTrackingDeliveryStatus(String trackingNumber, String status) throws ManagerException {
		Map<String, Object> orderTrackingLMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from order_tracking where tracking_no = '" + trackingNumber + "'", "lms");
		ExceptionHandler.handleTrue(orderTrackingLMSDB.get("delivery_status").equals(status.toString()), "order_Tracking table - delivery status is not " + status);
	}

	private String getReturnWarehouseId(String returnId) {
		try {
			List list = DBUtilities.exSelectQuery("select return_warehouse_id from return_shipment where source_return_id =" + returnId, "lms");
			if (list == null) {
				return "false";
			}
			HashMap<String, Object> hm = (HashMap<String, Object>) list.get(0);
			return "" + hm.get("return_warehouse_id");
		} catch (Exception e) {
			log.error("Error in return_warehouse_id :- " + e.getMessage());
			return "false";
		}
	}

	private String getReturnWarehouseHub(String returnId) {
		try {
			List list = DBUtilities.exSelectQuery("select return_hub_code from return_shipment where source_return_id =" + returnId, "lms");
			if (list == null) {
				return "false";
			}
			HashMap<String, Object> hm = (HashMap<String, Object>) list.get(0);
			return "" + hm.get("return_hub_code");
		} catch (Exception e) {
			log.error("Error in return_hub_code :- " + e.getMessage());
			return "false";
		}
	}

	//GLORIA : added for ReturnsReengineering
	public void validateOrderTrackingDetailsForActivity(String tracking_no, List<String> ActivityTypeStatusCombination) {

		ActivityTypeStatusCombination.forEach(status -> {
			String[] temp = status.split(":");
			String activityType = temp[0];
			String fromStatus = temp[1];
			String toStatus = temp[2];
			try {
				ExceptionHandler.handleTrue(DBUtilities.exSelectQueryForSingleRecord("select * from order_tracking_detail where activity_type = '" + activityType + "' and " +
						"from_status = '" + fromStatus + "' and to_status = '" + toStatus + "' and order_tracking_id = (select id from order_tracking where tracking_no='" + tracking_no + "')", "lms") != null, "");
			} catch (ManagerException e) {
				e.printStackTrace();
			}
		});
	}
	//GLORIA : added for ReturnsReengineering


	/**
	 * validateReturnShipmentStatusInLMS - This goes to the retrun_shipment table and checks for status
	 *
	 * @param returnId
	 * @param status
	 * @param delaytoCheck
	 * @return
	 */

	public boolean validateLMS_ReturnStatus(String returnId, String status, int delaytoCheck) {
		log.info("Validate return  Shipment Status in LMS return_shipment table");
		boolean validateStatus = false;
		try {
			for (int i = 0; i < delaytoCheck; i++) {
				String status_code = getShipmentStatusFromLMS_return_shipment(returnId);
				if (status_code.equalsIgnoreCase(status) || status_code.equalsIgnoreCase(status)) {
					validateStatus = true;
					break;
				} else {
					Thread.sleep(4000);
					validateStatus = false;
				}

				log.info("waiting for return Status in LMS" + status + " .current status=" + status_code + "\t " + i);
			}
		} catch (Exception e) {
			e.printStackTrace();
			validateStatus = false;
		}
		return validateStatus;
	}
	//GLORIA : added for ReturnsReengineering


	/**
	 * This function gives the ShipmentStatus from LMS : return_shipment
	 *
	 * @param returnId
	 * @return
	 */
	public String getShipmentStatusFromLMS_return_shipment(String returnId) {
		try {
			List list = DBUtilities.exSelectQuery("select shipment_status from return_shipment where source_return_id =" + returnId, "lms");
			if (list == null) {
				return "false";
			}
			HashMap<String, Object> hm = (HashMap<String, Object>) list.get(0);
			return "" + hm.get("shipment_status");
		} catch (Exception e) {
			log.error("Error in getOrderStatusFromLMS :- " + e.getMessage());
			return "false";
		}
	}

	//GLORIA : added for ReturnsReengineering
	public String getAssociatedPickupForReturn(String sourceReturnId) {
		try {
			Thread.sleep(Constants.LMS_PATH.sleepTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String pickup_shipment_idQuery = "select pickup_shipment_id_fk from return_pickup_association where return_shipment_id_fk in (select id from return_shipment where source_return_id = " + sourceReturnId + ")";
		List<Map<String, Object>> TxQ = DBUtilities.exSelectQuery(pickup_shipment_idQuery, "lms");
		Map<String, Object> txResult = null;
		try {
			txResult = TxQ.get(0);
		}catch (NullPointerException e){
			Assert.fail("Pickup is not created, check if return is created, check if tracking number with status=0, exists for courier_code DE, if in courier table pickup_split enabled= 0, then select * from tracking_number where status = 0 and courier_code = 'DE-PICKUP'; else select * from tracking_number where status = 0 and courier_code = 'DE';" );
		}
		String pickup_shipment_id = txResult.get("pickup_shipment_id_fk").toString();
		return pickup_shipment_id;
	}

	//GLORIA : added for ReturnsReengineering
	public String getOrderTrackingNumber(String pickupShipmentId) {
		String pickupTrackingNumberQuery = " select tracking_no from order_tracking where pickup_id =" + Integer.valueOf(pickupShipmentId);
		List<Map<String, Object>> TxQ = DBUtilities.exSelectQuery(pickupTrackingNumberQuery, "lms");
		Map<String, Object> txResult = TxQ.get(0);
		String pickupTrackingNumber = txResult.get("tracking_no").toString();
		return pickupTrackingNumber;

	}
	//GLORIA : added for ReturnsReengineering


	/**
	 * This method gives the pickup details  details by passing the pickupId
	 *
	 * @param pickupShipmentID
	 * @return
	 * @throws IOException
	 * @throws JAXBException
	 */

	public PickupResponse getPickupShipmentDetailsLMS(String pickupShipmentID) throws IOException {

		String pathParam = "findById?pickupId=" + Integer.valueOf(pickupShipmentID);
		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.
						PICKUP_BY_ID, new String[]{pathParam}, SERVICE_TYPE.LMS_SVC.toString(),
				HTTPMethods.GET, null, Headers.getLmsHeaderJSON());

		PickupResponse response = (PickupResponse) getJsontoObjectUsingFasterXML(service.getResponseBody(),
				new PickupResponse());
		return response;
	}

	//Added to map the JSON response to the Pickup Response Object.
	public static Object getJsontoObjectUsingFasterXML(String json, Object className) throws IOException {
		com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		//mapper.configure(DeserializationFeature.WRAP_ROOT_VALUE, true);
		return mapper.readValue(json, className.getClass());
	}

	//GLORIA : added for ReturnsReengineering
	public OrderTrackingResponse getOrderTrackingDetailV2Json(String courierCode, String trackingNumber) throws IOException, JAXBException {
		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_ORDER_TRACKING_DETAIL_V2, new String[]{"getOrderTrackingDetail?courierOperator=" + courierCode + "&trackingNumber=" + trackingNumber + "&level=LEVEL2"}, SERVICE_TYPE.LMS_SVC.toString(),
				HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
		OrderTrackingResponse response = (OrderTrackingResponse) APIUtilities.getJsontoObject(service.getResponseBody(),
				new OrderTrackingResponse());
		return response;
	}
	//GLORIA : change of processReturnInLMS because of ReturnsReengineering

	/**
	 * processOpenBoxReturn : processes  the OpenBox return return till Shipping back to config source warehouse and
	 * receiving in WH. SO it performs the complete action
	 *
	 * @param returnId/in case of selfShip its orderId
	 * @param toStatus
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws JAXBException
	 * @throws InterruptedException
	 */

	public void processReturntillPS(String returnId , String toStatus)throws Exception{
		String destWarehouseId = null;
		String deliveryCenterID = null;
		String tripOrderAssignmentId = null;
		RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();

		ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(Long.parseLong(returnId))).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");

		//1. Validate the respective RMS and LMS return fields
		validateOpenBoxRmsLmsReturnCreationV2("" + returnId);

		manifestOpenBoxPickups(returnId);

		//TODO : rmsObject does not have delivery centre id , so comparing it with lms objectResponse- after manifestation only the dccode, wh and tracking number are updated in RMS
		//	ExceptionHandler.handleTrue(returnShipmentLMSDB.get("delivery_center_id").toString().equals((returnShipmentLMSObject.getOrders().get(0).getDeliveryCenterId().toString())), "return_warehouse_id mismatch");
		//TODO : getting below as null:returnShipmentLMSObject.getOrders().get(0).getReturnWarehouseId()- check after a delay
		//	ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getReturnWarehouseId().toString().equals(returnResponseRMSObj.getData().get(0).getReturnAdditionalDetailsEntry().getIdealReturnWarehouse().toString()), "return_warehouse_id mismatch");
		//TODO : getting returnResponseRMSObj.getData().get(0).getReturnAdditionalDetailsEntry().getIdealReturnWarehouse() as null-- check after a delay
		//	ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_warehouse_id").toString().equals(returnResponseRMSObj.getData().get(0).getReturnAdditionalDetailsEntry().getIdealReturnWarehouse().toString()), "return_warehouse_id mismatch");
		//TODO getting returnShipmentLMSObject.getOrders().get(0).getCourierOperator() null
		//	ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getCourierOperator().toString().equals(returnResponseRMSObj.getData().get(0).getEmail().toString()), "email Mismatch in LMS-RMS");
		//	ReturnHubWarehouseResponse repsonse = (ReturnHubWarehouseResponse)lmsServiceHelper.getReturnHubFromWarehouse.apply(returnShipmentLMSDB.get("return_warehouse_id").toString());
		//			ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_hub_code").toString().equals((repsonse.getReturnProcessingConfigEntries().get(0).getHubCode().toString())), "return_warehouse_id mismatch");
		Thread.sleep(5000);
		String trackingNo=null;
		OrderResponse returnShipmentLMSObject = getReturnShipmentDetailsLMS(returnId);

		if(returnShipmentLMSObject.getOrders()!=null && !returnShipmentLMSObject.getOrders().isEmpty() ) {
			trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
		}
		else
			log.info("Failed here : returnShipmentLMSObject.getOrders().get(0).getTrackingNumber(): "+returnShipmentLMSObject.getOrders().get(0).getTrackingNumber());

		Map<String, Object> return_shipment = DBUtilities.exSelectQueryForSingleRecord("select * from return_shipment where source_return_id = " + returnId, "lms");
		trackingNo = return_shipment.get("tracking_number").toString();
		destWarehouseId = return_shipment.get("return_warehouse_id").toString();
		deliveryCenterID = return_shipment.get("delivery_center_id").toString();
		Map<String, String> data = lmsServiceHelper.processReturnTillTripCreation(returnId, trackingNo, deliveryCenterID);
		tripOrderAssignmentId = data.get("tripOrderAssignmentId");

		if (return_shipment.get("courier_code").toString().equals("ML")){
			TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
			Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
			Thread.sleep(2000);
			String trackingNumber = response.getData().get(0).getTrackingNumber();
			lmsHelper.updateOperationalTrackingNum(trackingNumber);
			lmsServiceHelper.receiveShipment(trackingNumber.substring(2,trackingNumber.length()));
			TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
			Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
			// Assert.assertTrue(lms_returnHelper.validatePickupShipmentStatusInLMS(returnId,EnumSCM.PICKUP_SUCCESSFUL,5), "return is not PICKUP_SUCCESSFUL" );
			validateRmsStatusAndRefund(returnId,ReturnStatus.RL.toString(), true, Constants.LMS_PATH.sleepTime);

		}
		else if (return_shipment.get("courier_code").toString().equals("EK")) {

		} else if (return_shipment.get("courier_code").toString().equals("DE")) {

		}

		log.info("Your order processing has been completed successfully ReturnId: " + returnId);

	}


	public void processOpenBoxReturn(String returnId, String toStatus) throws Exception {
		String destWarehouseId = null;
		String deliveryCenterID = null;
		String tripOrderAssignmentId = null;
		RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();

		ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(Long.parseLong(returnId))).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");

		//1. Validate the respective RMS and LMS return fields
		validateOpenBoxRmsLmsReturnCreationV2("" + returnId);

		manifestOpenBoxPickups(returnId);

		//TODO : rmsObject does not have delivery centre id , so comparing it with lms objectResponse- after manifestation only the dccode, wh and tracking number are updated in RMS
		//	ExceptionHandler.handleTrue(returnShipmentLMSDB.get("delivery_center_id").toString().equals((returnShipmentLMSObject.getOrders().get(0).getDeliveryCenterId().toString())), "return_warehouse_id mismatch");
		//TODO : getting below as null:returnShipmentLMSObject.getOrders().get(0).getReturnWarehouseId()- check after a delay
		//	ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getReturnWarehouseId().toString().equals(returnResponseRMSObj.getData().get(0).getReturnAdditionalDetailsEntry().getIdealReturnWarehouse().toString()), "return_warehouse_id mismatch");
		//TODO : getting returnResponseRMSObj.getData().get(0).getReturnAdditionalDetailsEntry().getIdealReturnWarehouse() as null-- check after a delay
		//	ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_warehouse_id").toString().equals(returnResponseRMSObj.getData().get(0).getReturnAdditionalDetailsEntry().getIdealReturnWarehouse().toString()), "return_warehouse_id mismatch");
		//TODO getting returnShipmentLMSObject.getOrders().get(0).getCourierOperator() null
		//	ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getCourierOperator().toString().equals(returnResponseRMSObj.getData().get(0).getEmail().toString()), "email Mismatch in LMS-RMS");
		//	ReturnHubWarehouseResponse repsonse = (ReturnHubWarehouseResponse)lmsServiceHelper.getReturnHubFromWarehouse.apply(returnShipmentLMSDB.get("return_warehouse_id").toString());
		//			ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_hub_code").toString().equals((repsonse.getReturnProcessingConfigEntries().get(0).getHubCode().toString())), "return_warehouse_id mismatch");
		Thread.sleep(5000);
		String trackingNo=null;
		OrderResponse returnShipmentLMSObject = getReturnShipmentDetailsLMS(returnId);

		if(returnShipmentLMSObject.getOrders()!=null && !returnShipmentLMSObject.getOrders().isEmpty() ) {
			trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
		}
		else
			log.info("Failed here : returnShipmentLMSObject.getOrders().get(0).getTrackingNumber(): "+returnShipmentLMSObject.getOrders().get(0).getTrackingNumber());
		Map<String, Object> return_shipment = DBUtilities.exSelectQueryForSingleRecord("select * from return_shipment where source_return_id = " + returnId, "lms");
		if (return_shipment.get("courier_code").toString().equals("ML"))
			processReturnInML(returnId, toStatus, trackingNo, destWarehouseId, deliveryCenterID, tripOrderAssignmentId, return_shipment);
		else if (return_shipment.get("courier_code").toString().equals("EK")) {

		} else if (return_shipment.get("courier_code").toString().equals("DE")) {

		}

		log.info("Your order processing has been completed successfully ReturnId: " + returnId);
	}

	public void processReturnWithoutNewValidation(String returnId, String toStatus) throws Exception {
		String destWarehouseId = null;
		String deliveryCenterID = null;
		String tripOrderAssignmentId = null;
		RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();

		//manifestOpenBoxPickups(returnId);
		Thread.sleep(5000);
		String trackingNo=null;
		OrderResponse returnShipmentLMSObject = getReturnShipmentDetailsLMS(returnId);

		if(returnShipmentLMSObject.getOrders()!=null && !returnShipmentLMSObject.getOrders().isEmpty() ) {
			trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
		}
		else
			log.info("Failed here : returnShipmentLMSObject.getOrders().get(0).getTrackingNumber(): "+returnShipmentLMSObject.getOrders().get(0).getTrackingNumber());
		Map<String, Object> return_shipment = DBUtilities.exSelectQueryForSingleRecord("select * from return_shipment where source_return_id = " + returnId, "lms");
		if (return_shipment.get("courier_code").toString().equals("ML"))
			processReturnInML(returnId, toStatus, trackingNo, destWarehouseId, deliveryCenterID, tripOrderAssignmentId, return_shipment);
		else if (return_shipment.get("courier_code").toString().equals("EK")) {

		} else if (return_shipment.get("courier_code").toString().equals("DE")) {

		}

		log.info("Your order processing has been completed successfully ReturnId: " + returnId);

	}

	/**
	 * processReturnInML
	 * @param returnId
	 * @param toStatus
	 * @param trackingNo
	 * @param destWarehouseId
	 * @param deliveryCenterID
	 * @param tripOrderAssignmentId
	 * @throws IOException
	 * @throws JAXBException
	 * @throws InterruptedException
	 */
	public void processReturnInML(String returnId, String toStatus, String trackingNo, String destWarehouseId, String
			deliveryCenterID, String tripOrderAssignmentId, Map<String, Object> return_shipment) throws Exception {
		Map<String, String> data;
		if (!toStatus.equals(EnumSCM.SELF_SHIP)) {
			trackingNo = return_shipment.get("tracking_number").toString();
			destWarehouseId = return_shipment.get("return_warehouse_id").toString();
			deliveryCenterID = return_shipment.get("delivery_center_id").toString();
			data = lmsServiceHelper.processReturnTillTripCreation(returnId, trackingNo, deliveryCenterID);
			tripOrderAssignmentId = data.get("tripOrderAssignmentId");
		}
		switch (toStatus) {
			case EnumSCM.PICKED_UP_SUCCESSFULLY:
				markTripPickupSuccessful(returnId, destWarehouseId, deliveryCenterID, tripOrderAssignmentId);
				break;

			case EnumSCM.PQCP_APPROVED_After_trip_close:
				pickupPQCP_CCApproveReject(returnId, destWarehouseId, deliveryCenterID, tripOrderAssignmentId, trackingNo,"APPROVED");
				break;
			case EnumSCM.PQCP_REJECTED_After_trip_close:
				pickupPQCP_CCApproveReject(returnId, destWarehouseId, deliveryCenterID, tripOrderAssignmentId, trackingNo,"REJECTED");
				break;
			case EnumSCM.PICKUP_DONE_QC_PENDING:
				PQCP_Approve(returnId, destWarehouseId, deliveryCenterID, tripOrderAssignmentId, trackingNo,"APPROVED");
				break;
			case EnumSCM.FAILED_PICKUP:
				failPickup(returnId, destWarehouseId, deliveryCenterID, tripOrderAssignmentId);
				break;
			case EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE:
				reschedule(tripOrderAssignmentId);
				break;
			default:
				log.info("No matching status");
				break;
		}
	}

	public void reschedule(String tripOrderAssignmentId) throws IOException, JAXBException, InterruptedException {
		TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE, EnumSCM.UPDATE);
		Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
		Thread.sleep(2000);
		TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE, EnumSCM.TRIP_COMPLETE);
		Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

	}

	public void failPickup(String returnId, String destWarehouseId, String deliveryCenterID, String tripOrderAssignmentId) throws IOException, JAXBException, InterruptedException {
		TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.NOT_ABLE_TO_PICKUP, EnumSCM.UPDATE);
		Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
		Thread.sleep(2000);
		TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.NOT_ABLE_TO_PICKUP, EnumSCM.TRIP_COMPLETE);
		Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

	}


	/**
	 * markTripPickupSuccessful
	 * @param returnId
	 * @param destWarehouseId
	 * @param deliveryCenterID
	 * @param tripOrderAssignmentId
	 * @throws Exception
	 */
	private void markTripPickupSuccessful(String returnId, String destWarehouseId,
										  String deliveryCenterID, String tripOrderAssignmentId)
			throws Exception {
		TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
		Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
		Thread.sleep(2000);
		String trackingNumber = response.getData().get(0).getTrackingNumber();
		lmsHelper.updateOperationalTrackingNum(trackingNumber);
		lmsServiceHelper.receiveShipment(trackingNumber.substring(2,trackingNumber.length()));
		TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
		Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
		// Assert.assertTrue(lms_returnHelper.validatePickupShipmentStatusInLMS(returnId,EnumSCM.PICKUP_SUCCESSFUL,5), "return is not PICKUP_SUCCESSFUL" );
		validateRmsStatusAndRefund(returnId,ReturnStatus.RL.toString(), true, Constants.LMS_PATH.sleepTime);

		rmsServiceHelper.WaitRefundStatus(returnId,20);
		TMSServiceHelper tmsServiceHelper= new TMSServiceHelper();
		long masterBagId = (long)tmsServiceHelper.createNcloseMBforReturn.apply(returnId);
		tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);


		Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
				.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WH");
		Assert.assertEquals(
				lmsServiceHelper.receiveReturnShipmentFromMasterbag(masterBagId, String.valueOf(returnId)).getStatus().getStatusType().toString(),
				EnumSCM.SUCCESS, "Unable to receive return from masterbag in warehouse");


		ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.DELIVERED_TO_SELLER.toString(),10));
		lmsServiceHelper.validateRmsStatusAndRefund(returnId,EnumSCM.RRC, true, Constants.LMS_PATH.sleepTime);

	}
	public void manifestOpenBoxPickups(String returnId) throws IOException, JAXBException, InterruptedException,
			ManagerException {
		String start_date = LocalDateTime.now().toLocalDate().toString().replaceAll("-", "").concat("000000");
		String end_date = LocalDateTime.now().plusDays(1).toLocalDate().toString().replaceAll("-","").concat("000000");

		//String[] pathParam={"?tenantId="+LMS_CONSTANTS.TENANTID+"&sourceId="+LMS_CONSTANTS.CLIENTID+""};
		// Map<String,String> hm=LMSUtils.manifestDate();
		String[] pathParam={start_date+"/"+end_date+"?tenantId="+LMS_CONSTANTS.TENANTID+"&sourceId="+LMS_CONSTANTS.CLIENTID+""};
		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.OPEN_BOX_MANIFEST, pathParam, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, null, Headers.getLmsHeaderJSON());
		while (!isPickupManifested(returnId)) {
			Thread.sleep(Constants.LMS_PATH.sleepTime);

			service = HttpExecutorService.executeHttpService(Constants.LMS_PATH_V1.OPEN_BOX_MANIFEST, null, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, null, Headers.getLmsHeaderJSON());

		}
		//String response = APIUtilities.getElement(service.getResponseBody(), "status.statusType", "json");
	//	String trackingNumber = APIUtilities.getElement(service.getResponseBody(), "data[0].trackingNumber", "json");
        /*com.myntra.lms.client.domain.response.PickupResponse pickupResponse = (com.myntra.lms.client.domain.response.PickupResponse) getJsontoObjectUsingFasterXMLIgnoreRoot(service.getResponseBody(),
                new com.myntra.lms.client.domain.response.PickupResponse());*/

		service.getResponseBody().contains("SUCCESS");
		//Verify order_tracking courier creation status = APPROVED
	//	validateOrderTrackingCourierCreationStatus(trackingNumber, CourierCreationStatus.ACCEPTED);
		//validateMLShipmentStatus(pickupResponse.getDomainPickupShipments().get(0).getTrackingNumber().toString(),com.myntra.ml.domain.MLDeliveryShipmentStatus)
	}
	public static Object getJsontoObjectUsingFasterXMLIgnoreRoot(String json, Object className) throws IOException {
		com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, false);
		return mapper.readValue(json, className.getClass());
	}
	public boolean isPickupManifested(String returnId) throws IOException {
		String pickup_shipment_id = getAssociatedPickupForReturn(returnId);

		com.myntra.lms.client.domain.response.PickupResponse pickupResponse = getPickupShipmentDetailsLMS(pickup_shipment_id);

		return pickupResponse.getDomainPickupShipments().get(0).getManifested();
	}
	public void insertTrackingNumberClosedBox(String courierCode){
		String query;
		int random = (int)(Math.random()*1000000);

		query = "INSERT INTO `tracking_number` (`created_by`, `version`, `courier_code`, `tracking_number`, `status`, `courier`, `tenant_id`) VALUES "+
				"('automation', 1, '"+courierCode+"', 'DEAutu"+random+"', 0, '"+courierCode+"', "+TENANTID+");";
		DBUtilities.exUpdateQuery(query, "myntra_lms");
	}


	public void insertTrackingNumberRHD(String courierCode){
		String query;
		for (int i = 0 ; i < 10 ; i++) {
			int random = (int) (Math.random() * 1000000);
			query = "INSERT INTO `tracking_number` (`created_by`, `version`, `courier_code`, `tracking_number`, `status`, `courier`, `tenant_id`) VALUES " +
					"('automation', 1, '" + courierCode + "', 'DE" + random+61 + "', 0, '" + courierCode + "', " + TENANTID + ");";
			DBUtilities.exUpdateQuery(query, "myntra_lms");
		}
	}

	//Gloria : adding

	/**
	 * This method gives the return_shipment details in the form of ObjectResponse object
	 *
	 * @param returnId
	 * @return
	 * @throws IOException
	 * @throws JAXBException
	 */

	public OrderResponse getReturnShipmentDetailsLMS(String returnId)
			throws IOException, JAXBException {
		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.RETURN, new String[]{returnId}, SERVICE_TYPE.LMS_SVC.toString(),
				HTTPMethods.GET, null, Headers.getLmsHeaderXML());
		OrderResponse response = (OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
				new OrderResponse());
		return response;
	}

	/**
	 * validateReturnStatusInLMS
	 *
	 * @param returnId
	 * @param status
	 * @param delaytoCheck
	 * @return
	 */
	public boolean validateReturnShipmentStatusInLMS(String returnId, String status, int delaytoCheck) {
		log.info("Validate return Status in LMS Return Shipment table");
		boolean validateStatus = false;
		try {
			for (int i = 0; i < delaytoCheck; i++) {
				String status_code = getReturnShipmentStatusFromLMS(returnId);
				if (status_code.equalsIgnoreCase(status) || status_code.equalsIgnoreCase(status)) {
					validateStatus = true;
					break;
				} else {
					Thread.sleep(4000);
					validateStatus = false;
				}

				log.info("waiting for return Status in LMS" + status + " .current status=" + status_code + "\t " + i);
			}
		} catch (Exception e) {
			e.printStackTrace();
			validateStatus = false;
		}
		return validateStatus;
	}

	/**
	 * getReturnStatusFromLMS
	 *
	 * @param returnId
	 * @return
	 */
	public String getReturnShipmentStatusFromLMS(String returnId) {
		try {
			List list = DBUtilities.exSelectQuery("select shipment_status from return_shipment where source_return_id =" + returnId, "lms");
			if (list == null) {
				return "false";
			}
			HashMap<String, Object> hm = (HashMap<String, Object>) list.get(0);
			return "" + hm.get("shipment_status");
		} catch (Exception e) {
			log.error("Error in getOrderStatusFromLMS :- " + e.getMessage());
			return "false";
		}
	}

	/**
	 * validatePickupStatusInLMS
	 *
	 * @param returnId
	 * @param status
	 * @param delaytoCheck
	 * @return
	 */
	public boolean validatePickupShipmentStatusInLMS(String returnId, String status, int delaytoCheck) {
		log.info("Validate pickup Status in LMS Pickup Shipment table");
		boolean validateStatus = false;
		try {
			for (int i = 0; i < delaytoCheck; i++) {
				String status_code = getPickupShipmentStatusFromLMS(returnId);
				if (status_code.equalsIgnoreCase(status) || status_code.equalsIgnoreCase(status)) {
					validateStatus = true;
					break;
				} else {
					Thread.sleep(Constants.LMS_PATH.sleepTime);
					validateStatus = false;
				}

				log.info("waiting for pickup Status in LMS" + status + " .current status=" + status_code + "\t " + i);
			}
		} catch (Exception e) {
			e.printStackTrace();
			validateStatus = false;
		}
		return validateStatus;
	}

	/**
	 * getReturnStatusFromLMS
	 *
	 * @param returnId
	 * @return
	 */
	public String getPickupShipmentStatusFromLMS(String returnId) {
		try {
			List list = DBUtilities.exSelectQuery("select shipment_status from pickup_shipment where id in (select " +
					"pickup_shipment_id_fk from return_pickup_association where return_shipment_id_fk in  (select id " +
					"from return_shipment where   source_return_id in (" + returnId + ")))", "lms");
			if (list == null) {
				return "false";
			}
			HashMap<String, Object> hm = (HashMap<String, Object>) list.get(0);
			return "" + hm.get("shipment_status");
		} catch (Exception e) {
			log.error("Error in getOrderStatusFromLMS :- " + e.getMessage());
			return "false";
		}
	}

	/**
	 * validateRmsStatusAndRefund
	 *
	 * @param returnId
	 * @param status
	 * @param refund
	 */
	public boolean validateRmsStatusAndRefund(String returnId, String status, boolean refund, long wait) throws IOException, InterruptedException {
		RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
		Assert.assertTrue(rmsServiceHelper.validateReturnStatusInRMS(returnId, status, 10), "pickup is not in " + status + " status in Returns");
		if (refund) {
			log.info("sleeping for " + wait + " msc");
			Thread.sleep(wait);
			ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(returnId);
			Assert.assertEquals(returnResponse.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "" + refund, "is_refunded is not as expected. Expected: " + refund + " But not found");
			Assert.assertNotNull(returnResponse.getData().get(0).getReturnRefundDetailsEntry().getRefundPPSId().toString(), "Return PPSID is null in RMS");
		}
		return true;
	}

	public void uploadClosedBoxStatusFile(String filePath) throws UnsupportedEncodingException, JAXBException, InterruptedException {
		MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
		File file = new File(filePath);
		multipartEntityBuilder.addPart("file", new FileBody(file));
		multipartEntityBuilder.setContentType(ContentType.APPLICATION_OCTET_STREAM);

		Svc service = HttpExecutorService.executeHttpServiceForUpload(Constants.LMS_PATH.UPLOAD_CLOSEDBOX_STATUS, null
				, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, multipartEntityBuilder, getTokenForFileUpload());

		//TODO: Check why marshalling is failing
	/*	PickupResponse response = (PickupResponse) APIUtilities
				.convertXMLStringToObject(service.getResponseBody(), new PickupResponse());
		
		Assert.assertEquals(response.getStatus().getStatusMessage(),
				"Request is submitted. Please check status of the request and download it once complete from the Download Document section .");
*/
	}

	public HashMap<String, String> getTokenForFileUpload() {
		HashMap<String, String> stnHeaders = new HashMap<String, String>();
		stnHeaders.put("Authorization", "Basic RVJQIEFkbWluaXN0cmF0b3J+ZXJwYWRtaW46dGVzdA==");
		stnHeaders.put("Content-Type", "multipart/form-data");
		return stnHeaders;
	}

	/**
	 * processClosedBoxPickup
	 *
	 * @param returnId
	 * @param toStatus
	 * @throws IOException
	 * @throws JAXBException
	 * @throws InterruptedException
	 */
	public void processClosedBoxPickup(String returnId, String toStatus) throws IOException, JAXBException, InterruptedException, ManagerException {
		switch (toStatus) {
			case EnumSCM.PICKED_UP_SUCCESSFULLY:
				pickupSuccessFullyClosedBox(returnId,ItemQCStatus.PASSED);
				break;
			case EnumSCM.PICKUP_SUCCESSFUL_ONHOLD_APPROVE:
				PickupSuccessfulApproveRejectClosedBox(returnId,ItemQCStatus.ON_HOLD,"APPROVED");
				break;
			case EnumSCM.PICKUP_SUCCESSFUL_ONHOLD_REJECTED:
				PickupSuccessfulApproveRejectClosedBox(returnId,ItemQCStatus.ON_HOLD,"REJECTED");
				break;
		/*	case EnumSCM.PICKUP_SUCCESSFUL_CB_AT_RPU:
				pickupSuccessFulClosedBoxAtRPU(returnId);
				break;
			case EnumSCM.ONHOLD_PICKUP_WITH_DC_APPROVE:
				pickupSuccessFulClosedBoxOnHoldApprove(returnId);
				break;
			case EnumSCM.ONHOLD_PICKUP_WITH_DC_REJECT:
				pickupSuccessFulClosedBoxOnHoldReject(returnId);
				break;
			default:
				log.info("No matching status");
				break;*/
		}
	}
	public void PickupSuccessfulApproveRejectClosedBox(String returnId,ItemQCStatus status,String approveReject) throws IOException, JAXBException, InterruptedException,
			ManagerException {

		String tracking_number = getPickupTrackingNoOfReturn(returnId);

		String courier_code = "DE";
		String shipment_type = "PU";
		String activity_type1 = "PICKUP_DETAILS_UPDATED";
		String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";
		validateOrderTrackingCourierCreationStatus(tracking_number, CourierCreationStatus.NOT_INITIATED);

		String file_path = generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
		uploadClosedBoxStatusFile(file_path);


		String activity_type2 = "OUT_FOR_PICKUP";
		String activity_sub_type2 = "";
		String file_path2 = generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
		uploadClosedBoxStatusFile(file_path2);
		Assert.assertTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.OUT_FOR_PICKUP.toString(), 15));

		String activity_type3 = "PICKUP_DONE";
		String activity_sub_type3 = "";
		String file_path3 = generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
		uploadClosedBoxStatusFile(file_path3);
		Assert.assertTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_DONE.toString(), 15));
		Assert.assertTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_DONE.toString(), 15));

		updateReturnReceiveEvents(returnId,"DE");
		String pickupId = getPickupIdOfReturn(returnId);

		Assert.assertTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER.toString(),15));
		Assert.assertTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER.toString(),15));


		performClosedBoxReturnQC(returnId,status,courier_code);
		//Just to process the remaining returns in the pickup randomly , we mark the even return ids on_hold and odd ones shortage
		returnApproveOrRejectClosedBox(returnId,approveReject);
	}

	public void pickupSuccessFullyClosedBox(String returnId,ItemQCStatus status) throws IOException, JAXBException, InterruptedException,
			ManagerException {

		String tracking_number = getPickupTrackingNoOfReturn(returnId);

		String courier_code = "DE";
		String shipment_type = "PU";
		String activity_type1 = "PICKUP_DETAILS_UPDATED";
		String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";
//		validateOrderTrackingCourierCreationStatus(tracking_number, CourierCreationStatus.NOT_INITIATED);

		String file_path = generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
		uploadClosedBoxStatusFile(file_path);
		System.out.println(file_path);

		String activity_type2 = "OUT_FOR_PICKUP";
		String activity_sub_type2 = "";
		String file_path2 = generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
		uploadClosedBoxStatusFile(file_path2);
		Assert.assertTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.OUT_FOR_PICKUP.toString(), 15));

		String activity_type3 = "PICKUP_DONE";
		String activity_sub_type3 = "";
		String file_path3 = generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
		uploadClosedBoxStatusFile(file_path3);
		System.out.println(file_path3);
		Assert.assertTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_DONE.toString(), 15));
		Assert.assertTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_DONE.toString(), 15));

		updateReturnReceiveEvents(returnId,"DE");
		String pickupId = getPickupIdOfReturn(returnId);

		Assert.assertTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER.toString(),15));
		Assert.assertTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER.toString(),15));


		performClosedBoxReturnQC(returnId,status,courier_code);
		//Just to process the remaining returns in the pickup randomly , we mark the even return ids on_hold and odd ones shortage
	}

	public void performClosedBoxReturnQC(String returnId, ItemQCStatus status, String courier_code) throws IOException {
		//Find the pickup id
		String pickupId = getPickupIdOfReturn(returnId);
		String tracking_number = getPickupTrackingNoOfReturn(returnId);

		List<String> returnList = getReturnsInPickup(pickupId);
		//We have to process all the returns in the pickup, the one passed as a parameter will be processed for the status which is passed, rest of the returns will be randomly processed
		returnList.remove(returnId);

		Map<String, ItemQCStatus> returnStatusMap = new HashMap<>();
		for (String remainingReturn : returnList) {
			if (Integer.valueOf(remainingReturn) % 2 == 0) {
				returnStatusMap.put(remainingReturn, ItemQCStatus.ON_HOLD);
				log.info("Return- " + remainingReturn + " Status-> On hold");

			} else {
				returnStatusMap.put(remainingReturn, ItemQCStatus.SHORTAGE);
				log.info("Return- " + remainingReturn + " Status-> SHORTAGE");

			}
		}
		ArrayList<ReturnShipmentUpdate> returnShipmentUpdates = new ArrayList<>();

		ReturnShipmentUpdate returnShipmentUpdate1 = new ReturnShipmentUpdate();
		returnShipmentUpdate1.setSourceReturnId(returnId);
		returnShipmentUpdate1.setRemark("Remark");
		returnShipmentUpdate1.setItemQCStatus(status);
		returnShipmentUpdates.add(returnShipmentUpdate1);

		for (Map.Entry<String, ItemQCStatus> entry : returnStatusMap.entrySet()) {
			ReturnShipmentUpdate returnShipmentUpdate = new ReturnShipmentUpdate();
			returnShipmentUpdate.setSourceReturnId(entry.getKey());
			returnShipmentUpdate.setRemark("Remark");
			returnShipmentUpdate.setItemQCStatus(entry.getValue());
			returnShipmentUpdates.add(returnShipmentUpdate);
		}



		ClosedBoxPickupQCCompleteUpdate closedBoxPickupQCCompleteUpdate;
		closedBoxPickupQCCompleteUpdate = new ClosedBoxPickupQCCompleteUpdate(returnId,TENANTID, CLIENTID,
				courier_code, tracking_number, ShipmentUpdateEvent.QUALITY_CHECK_COMPLETE, null, 5L,
				new Premise("5", Premise.PremiseType.DELIVERY_CENTER), "Location", new DateTime(), "remarks", ShipmentType.PU, ShipmentUpdateActivityTypeSource.LogisticsPortal, "Gloria", null, returnShipmentUpdates,null,null,null,null,null,null,null);
		String payload = APIUtilities.getObjectToJSON(closedBoxPickupQCCompleteUpdate);

		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_CLOSEDBOX_QC_STATUS, null,
				SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());

		//TODO :
		/*	DomainShipmentUpdateResponse domainShipmentUpdateResponse = (DomainShipmentUpdateResponse) APIUtilities.getJsontoObject(service.getResponseBody(),
					new DomainShipmentUpdateResponse());
			
			Assert.assertTrue(domainShipmentUpdateResponse.getStatus().getStatusType().equals(StatusResponse.Type.SUCCESS));
		*/

		for (Map.Entry<String, ItemQCStatus> entry : returnStatusMap.entrySet()) {
			if(entry.getValue().toString().equals( ItemQCStatus.ON_HOLD.toString())) {

				Assert.assertTrue(validateReturnShipmentStatusInLMS(entry.getKey(), ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString(), 15));
				log.info("Marked return "+entry.getKey()+" - ON_HOLD_WITH_RETURN_PROCESSING_CENTER ");

			}else if(entry.getValue().toString().equals(ItemQCStatus.SHORTAGE.toString())) {
				Assert.assertTrue(validateReturnShipmentStatusInLMS(entry.getKey(), ShipmentStatus.RETURN_CREATED.toString(), 15));
				log.info("Marked return "+entry.getKey()+" - Shortage, hence status is RETURN_CREATED ");

			}else {
				Assert.assertTrue(validateReturnShipmentStatusInLMS(entry.getKey(), ShipmentStatus.RETURN_SUCCESSFUL.toString(), 15));
				log.info("Marked return "+returnId+" - Return Successful");

			}
		}

		if(status.toString().equals( ItemQCStatus.ON_HOLD.toString())) {
			Assert.assertTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString(), 15));
			log.info("Marked return "+returnId+" - ON_HOLD_WITH_RETURN_PROCESSING_CENTER ");


		}else if(status.toString().equals( ItemQCStatus.SHORTAGE.toString())) {
			Assert.assertTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.RETURN_CREATED.toString(), 15));
			log.info("Marked return "+returnId+" - Shortage, hence status is RETURN_CREATED ");

		}else {
			Assert.assertTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.RETURN_SUCCESSFUL.toString(), 15));
			log.info("Marked return "+returnId+" - Return Successful");

		}
		Assert.assertTrue(validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.PICKUP_SUCCESSFUL.toString(),10));
		log.info("Marked pickup "+pickupId+" - Pickup Successful");

	}

	public void performClosedBoxReturnQCNew(String returnId, Map<String, ItemQCStatus> returnStatusMap, String courier_code)
			throws IOException, InterruptedException {
		//Find the pickup id
		String pickupId = getPickupIdOfReturn(returnId);
		String tracking_number = getPickupTrackingNoOfReturn(returnId);

		List<String> returnList = getReturnsInPickup(pickupId);
		//We have to process all the returns in the pickup, the one passed as a parameter will be processed for the status which is passed, rest of the returns will be randomly processed
		returnList.remove(returnId);

		ArrayList<ReturnShipmentUpdate> returnShipmentUpdates = new ArrayList<>();

		for (Map.Entry<String, ItemQCStatus> entry : returnStatusMap.entrySet()) {
			ReturnShipmentUpdate returnShipmentUpdate = new ReturnShipmentUpdate();
			returnShipmentUpdate.setSourceReturnId(entry.getKey());
			returnShipmentUpdate.setRemark("Remark");
			returnShipmentUpdate.setItemQCStatus(entry.getValue());
			returnShipmentUpdates.add(returnShipmentUpdate);
		}

		ClosedBoxPickupQCCompleteUpdate closedBoxPickupQCCompleteUpdate;
		closedBoxPickupQCCompleteUpdate = new ClosedBoxPickupQCCompleteUpdate(returnId,TENANTID, CLIENTID,
				courier_code, tracking_number, ShipmentUpdateEvent.QUALITY_CHECK_COMPLETE, null, 5L,
				new Premise("5", Premise.PremiseType.DELIVERY_CENTER), "Location", new DateTime(), "remarks", ShipmentType.PU, ShipmentUpdateActivityTypeSource.LogisticsPortal, "Gloria", null, returnShipmentUpdates,null,null,null,null,null,null,null);
		String payload = APIUtilities.getObjectToJSON(closedBoxPickupQCCompleteUpdate);

		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_CLOSEDBOX_QC_STATUS, null,
				SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
		Assert.assertEquals(APIUtilities.getElement(service.getResponseBody(),"status.statusType","json"),StatusResponse.Type.SUCCESS.toString(),"its a fail");

		//TODO :
		/*	DomainShipmentUpdateResponse domainShipmentUpdateResponse = (DomainShipmentUpdateResponse) APIUtilities.getJsontoObject(service.getResponseBody(),
					new DomainShipmentUpdateResponse());

			Assert.assertTrue(domainShipmentUpdateResponse.getStatus().getStatusType().equals(StatusResponse.Type.SUCCESS));
		*/

		for (Map.Entry<String, ItemQCStatus> entry : returnStatusMap.entrySet()) {
			if(entry.getValue().toString().equals( ItemQCStatus.ON_HOLD.toString())) {

				Assert.assertTrue(validateReturnShipmentStatusInLMS(entry.getKey(), ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString(), 5));
				log.info("Marked return "+entry.getKey()+" - ON_HOLD_WITH_RETURN_PROCESSING_CENTER ");

			}else if(entry.getValue().toString().equals(ItemQCStatus.SHORTAGE.toString())) {
				Assert.assertTrue(validateReturnShipmentStatusInLMS(entry.getKey(), ShipmentStatus.RETURN_CREATED.toString(), 5));
				log.info("Marked return "+entry.getKey()+" - Shortage, hence status is RETURN_CREATED ");

			}else {
				Assert.assertTrue(validateReturnShipmentStatusInLMS(entry.getKey(), ShipmentStatus.RETURN_SUCCESSFUL.toString(), 5));
				log.info("Marked return "+returnId+" - Return Successful");

			}

		}
		Assert.assertTrue(validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.PICKUP_SUCCESSFUL.toString(),10));
		log.info("Marked pickup "+pickupId+" - Pickup Successful");

	}
	public void  mlOpenBoxQC(String returnId,ShipmentUpdateEvent event,String trackingNo) throws IOException {
		ShipmentUpdate.ShipmentUpdateBuilder shipmentUpdateBuilder = new ShipmentUpdate.ShipmentUpdateBuilder(
				returnId, event);
		shipmentUpdateBuilder.courierCodeAndTrackingNumber("ML",trackingNo);
		shipmentUpdateBuilder.tenantId(LMS_CONSTANTS.TENANTID);
		shipmentUpdateBuilder.clientId(LMS_CONSTANTS.CLIENTID);
		shipmentUpdateBuilder.eventLocation("location");
		shipmentUpdateBuilder.eventTime(new DateTime());
		shipmentUpdateBuilder.shipmentType(ShipmentType.PU);
		shipmentUpdateBuilder.shipmentUpdateActivitySource(ShipmentUpdateActivityTypeSource.LogisticsPortal);
		shipmentUpdateBuilder.userName("User");
		shipmentUpdateBuilder.remarks("remarks");
		shipmentUpdateBuilder.eventLocationPremise(new Premise("5", Premise.PremiseType.DELIVERY_CENTER) );
		DomainShipmentUpdateResponse openBoxPickupQCCompleteUpdate = new DomainShipmentUpdateResponse();
		String payload = APIUtilities.getObjectToJSON(shipmentUpdateBuilder.build() );

		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_OPENBOX_QC_STATUS, null,
				SERVICE_TYPE.Last_mile.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());

	}

	public void PQCP_Approve(String returnId, String destWarehouseId, String deliveryCenterID,
							 String tripOrderAssignmentId, String trackingNumber,String status) throws InterruptedException, JAXBException, IOException, JSONException, XMLStreamException, ManagerException {
		markTripPickupSuccessfulQCPending(returnId, destWarehouseId, deliveryCenterID,
				tripOrderAssignmentId);
		mlOpenBoxQC(returnId, ShipmentUpdateEvent.PICKUP_DONE, trackingNumber);
	}
	public void pickupPQCP_CCApproveReject(String returnId, String destWarehouseId, String deliveryCenterID,
										   String tripOrderAssignmentId, String trackingNumber,String status) throws Exception {
		markTripPickupSuccessfulQCPending(returnId, destWarehouseId, deliveryCenterID,
				tripOrderAssignmentId);
		mlOpenBoxQC(returnId, ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNumber);
		ExceptionHandler.handleTrue(validatePickupShipmentStatusInLMS(returnId, com.myntra.logistics.platform.domain.ShipmentStatus.ONHOLD_PICKUP_WITH_COURIER.toString(), 10));
		ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId, com.myntra.logistics.platform.domain.ShipmentStatus.ONHOLD_RETURN_WITH_COURIER.toString(), 10));
		returnApproveOrReject(returnId, status, false);
		if (status.equals("APPROVED")) {
			TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
			long masterBagId = (long) tmsServiceHelper.createNcloseMBforReturn.apply(returnId);
			tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);


			Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
					.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WH");
			Assert.assertEquals(
					lmsServiceHelper.receiveReturnShipmentFromMasterbag(masterBagId, String.valueOf(returnId)).getStatus().getStatusType().toString(),

					EnumSCM.SUCCESS, "Unable to receive shipment in DC");


			ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.DELIVERED_TO_SELLER.toString(), 10));
			lmsServiceHelper.validateRmsStatusAndRefund(returnId, EnumSCM.RRC, true, Constants.LMS_PATH.sleepTime);

		}
		else
		//Reship
		{
			Assert.assertEquals(lmsServiceHelper.mlShipmentUpdate(Long.parseLong("5"), (String)lmsHelper.getReturnsTrackingNumber.apply(returnId), MLShipmentUpdateEvent.RESHIP_TO_CUSTOMER,"Return Reshipped - GLoria", ShipmentType.PU),EnumSCM.SUCCESS);

			validateMLShipmentStatus(getPickupTrackingNoOfReturn(returnId),"RESHIPPED_TO_CUSTOMER");

		}
	}
	private void markTripPickupSuccessfulQCPending(String returnId, String destWarehouseId,
												   String deliveryCenterID, String tripOrderAssignmentId)
			throws IOException, JAXBException, InterruptedException, ManagerException {
		LMSHelper lmsHelper = new LMSHelper();
		LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
		TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
		Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
		Thread.sleep(Constants.LMS_PATH.sleepTime);
		lmsServiceHelper.receiveShipment(response.getData().get(0).getTrackingNumber());
		TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
		Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
		Thread.sleep(Constants.LMS_PATH.sleepTime);
		ExceptionHandler.handleTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_DONE.toString(),10));
		ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId,ShipmentStatus.PICKUP_DONE.toString(),10));
		rmsServiceHelper.validateReturnStatusInRMS(returnId,ReturnStatus.RL.toString(),10);
	}

	public void returnApproveOrReject(String returnId, String status, boolean onHoldWithCustomer) throws IOException, JAXBException,
			InterruptedException, ManagerException {
		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.APPROVE_REJECT_RETURN, new String[]{"" + returnId, status,"scmadmin?clientId="+CLIENTID+"&tenantId="+TENANTID+"" }, SERVICE_TYPE.LMS_SVC.toString(),
				HTTPMethods.POST, null, Headers.getLmsHeaderXML());
		String  response =String.valueOf(service.getResponseBody());
		response.contains("SUCCESS");
		if(!onHoldWithCustomer) {
			if (status.equalsIgnoreCase("APPROVED")) {
				ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.RETURN_SUCCESSFUL.toString(), 10));
				ExceptionHandler.handleTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_SUCCESSFUL.toString(), 10));
				validateMLShipmentStatus(getPickupTrackingNoOfReturn(returnId), "APPROVED_ONHOLD_PICKUP_WITH_DC");

				rmsServiceHelper.validateReturnStatusInRMS(returnId, ReturnStatus.RL.toString(), 10);

				ExceptionHandler.handleTrue(rmsServiceHelper.WaitRefundStatus(returnId, 30));
				LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
				//Acknowledge
				Assert.assertEquals(lmsServiceHelper.mlShipmentUpdate(Long.parseLong("5"), (String) lmsHelper.getReturnsTrackingNumber.apply(returnId), com.myntra.lastmile.client.status.MLShipmentUpdateEvent.ACKNOWLEDGE_APPROVE_ONHOLD_PICKUP_WITH_DC, "RETURN_APPROVED", ShipmentType.PU), EnumSCM.SUCCESS);
				ExceptionHandler.handleTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_SUCCESSFUL.toString(), 10));

			} else {
				ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId, com.myntra.logistics.platform.domain.ShipmentStatus.RETURN_REJECTED.toString(), 10));
				Assert.assertTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_REJECTED.toString(), 10));
				rmsServiceHelper.validateReturnStatusInRMS(returnId, ReturnStatus.RRSH.toString(), 10);
				validateMLShipmentStatus(getPickupTrackingNoOfReturn(returnId), "REJECTED_ONHOLD_PICKUP_WITH_DC");

				ReturnResponse returnResponseRMSObj = rmsServiceHelper.getReturnDetailsNew(String.valueOf(Long.parseLong(returnId)));
				ExceptionHandler.handleFalse(returnResponseRMSObj.getData().get(0).getReturnRefundDetailsEntry().getRefunded(), " REFUNDED");

			}
		}
	}
	public String returnApproveOrRejectClosedBox(String returnId, String status) throws IOException, JAXBException,
			InterruptedException, ManagerException {
		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.APPROVE_REJECT_RETURN,new String[]{"" + returnId, status,"gloriar?clientId="+CLIENTID+"&tenantId="+TENANTID+"" }, SERVICE_TYPE.LMS_SVC.toString(),
				HTTPMethods.POST, null, Headers.getLmsHeaderXML());
		String  response =String.valueOf(service.getResponseBody());

		Assert.assertTrue(response.contains("SUCCESS"), "Unable to Approve/Reject order");
		if (status.equalsIgnoreCase("APPROVED")) {
			ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId,  ShipmentStatus.APPROVED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER.toString(),10));
			ExceptionHandler.handleTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_SUCCESSFUL.toString(),10));
			Assert.assertTrue(rmsServiceHelper.WaitRefundStatus(returnId,20));
		}else{
			ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId,  ShipmentStatus.REJECTED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER.toString(),10));
			//TODO : gloria check failing
			Assert.assertTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_SUCCESSFUL.toString(),10));
			rmsServiceHelper.validateReturnStatusInRMS(returnId,ReturnStatus.RRSH.toString(),10);

			ReturnResponse returnResponseRMSObj = rmsServiceHelper.getReturnDetailsNew(String.valueOf(Long.parseLong(returnId)));
			Assert.assertFalse(returnResponseRMSObj.getData().get(0).getReturnRefundDetailsEntry().getRefunded()," REFUNDED");

		}
		return response;
	}

	public String getPickupTrackingNoOfReturn(String returnId) {
		Map<String, Object> hm = DBUtilities.exSelectQueryForSingleRecord("select tracking_number from pickup_shipment where id in (select pickup_shipment_id_fk  from `return_pickup_association` where return_shipment_id_fk in (select id from return_shipment where  source_return_id =" + returnId + "))", "myntra_lms");
		String tracking_number = (String) hm.get("tracking_number");
		log.info("Tracking number of the associated pickup for the given return is -  " + tracking_number);
		return tracking_number;
	}
	public List<String> getReturnsInPickup(String pickupId){

		List<Map<String, Object>> returnListDB= DBUtilities.exSelectQuery("select source_return_id from return_shipment where id in (select return_shipment_id_fk  from `return_pickup_association` where pickup_shipment_id_fk in (select id from pickup_shipment where id in ("+pickupId+")))","myntra_lms");
		List<String> returnIdList= new ArrayList<>();
		returnListDB.forEach(returnid ->
		{
			returnIdList.add((String) returnid.get("source_return_id"));

		});
		return returnIdList;
	}
	public String getPickupIdOfReturn(String returnId) {

		Map<String, Object> hm = DBUtilities.exSelectQueryForSingleRecord("select id from pickup_shipment where id in (select pickup_shipment_id_fk  from `return_pickup_association` where return_shipment_id_fk in (select id from return_shipment where  source_return_id =" + returnId + "))", "myntra_lms");
		BigInteger pickupId = (BigInteger) hm.get("id");
		log.info("Pickup Id of the associated pickup for the given return is -  " + pickupId);
		return String.valueOf(pickupId);
	}
	public String generateClosedBoxStatusCSV(String tracking_number, String courier_code, String shipment_type, String activity_type1, String activity_sub_type1) throws IOException {
		log.info("Activity Time is : " + new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date()));

		String time = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
		java.sql.Date currentTimestamp = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
		String uploadFileHeader = "Tracking Number,Courier Code,Shipment Type,Activity Type,Activity Sub Type,Activity Date,Remarks,Location";

		File file = new File("./Data/lms/ClosedBoxUpload" + currentTimestamp + Math.random() + ".csv");
		if (file.exists()) {
			file.delete();
		}
		FileWriter writer = new FileWriter(file, true);
		BufferedWriter bufferedWriter = new BufferedWriter(writer);
		bufferedWriter.write(uploadFileHeader);
		bufferedWriter.newLine();
		bufferedWriter.write(tracking_number + "," + courier_code + "," + shipment_type + "," + activity_type1 + "," + activity_sub_type1 + "," + time + "," + "Test Gloria," + "System");
		bufferedWriter.newLine();

		bufferedWriter.close();
		log.info("CLosed Box Upload file - " + file.getName() + " is created in Folder :" + file.getPath());
		return file.getPath();

	}

	public void manifestClosedBoxPickups(String returnId) throws IOException, JAXBException, InterruptedException,
			ManagerException {
		//Map<String,String> hm=LMSUtils.manifestDate();
		String start_date = LocalDateTime.now().toLocalDate().toString().replaceAll("-", "").concat("000000");
		String end_date = LocalDateTime.now().plusDays(1).toLocalDate().toString().replaceAll("-","").concat("000000");

		String[] pathParam={start_date+"/"+end_date+"?tenantId="+LMS_CONSTANTS.TENANTID+"&sourceId="+LMS_CONSTANTS.CLIENTID+""};
		Thread.sleep(Constants.LMS_PATH.sleepTime);
		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.CLOSED_BOX_MANIFEST, pathParam, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, null, Headers.getLmsHeaderJSON());
		while (!isPickupManifested(returnId)) {
			service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.CLOSED_BOX_MANIFEST, pathParam, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, null, Headers.getLmsHeaderJSON());

		}
		com.myntra.lms.client.domain.response.PickupResponse pickupResponse = (com.myntra.lms.client.domain.response.PickupResponse) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(),
				new com.myntra.lms.client.domain.response.PickupResponse());

		Assert.assertTrue(pickupResponse.getStatus().getStatusType().equals(StatusResponse.Type.SUCCESS));
		//Verify order_tracking courier creation status = APPROVED
		//validateMLShipmentStatus(pickupResponse.getDomainPickupShipments().get(0).getTrackingNumber().toString(),com.myntra.ml.domain.MLDeliveryShipmentStatus)
	}

	public void manifestPickups(String returnId,String returnType,String courierCode) throws IOException, JAXBException, InterruptedException,
			ManagerException {
		//Map<String,String> hm=LMSUtils.manifestDate();
		String start_date = LocalDateTime.now().toLocalDate().toString().replaceAll("-", "").concat("000000");
		String end_date = LocalDateTime.now().plusDays(1).toLocalDate().toString().replaceAll("-","").concat("000000");
		String[] pathParam={MessageFormat.format("{0}/{1}/{2}/{3}?tenantId={4}&sourceId={5}",returnType,courierCode,start_date,end_date,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID)};
		Thread.sleep(Constants.LMS_PATH.sleepTime);
		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.MANIFEST, pathParam, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, null, Headers.getLmsHeaderJSON());
		while (!isPickupManifested(returnId)) {
			service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.MANIFEST, pathParam, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, null, Headers.getLmsHeaderJSON());

		}
		String response = APIUtilities.getElement(service.getResponseBody(), "status.statusType", "json");

		Assert.assertTrue(response.equals("SUCCESS"),"Pickup is not manifested");
	}

	public void manifestClosedBoxPickups() throws IOException, JAXBException, InterruptedException,
			ManagerException {
		int cnt=0;
		int no_of_loops=2;
		Svc service=null;
		String[] pathParam={"?tenantId="+LMS_CONSTANTS.TENANTID+"&sourceId="+LMS_CONSTANTS.CLIENTID+""};
		while(cnt<no_of_loops) {

			service = HttpExecutorService.executeHttpService(Constants.LMS_PATH_V1.CLOSED_BOX_MANIFEST, pathParam, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, null, Headers.getLmsHeaderJSON());
			Thread.sleep(Constants.LMS_PATH.sleepTime);

			cnt++;
		}
		PickupResponse pickupResponse = (PickupResponse) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(),
				new com.myntra.lms.client.domain.response.PickupResponse());

		//	Assert.assertTrue(pickupResponse.getStatus().getStatusType().equals(StatusResponse.Type.SUCCESS));
	}

	public boolean verifyClosedBoxConsolidation(String returnID, String returnID2) {
		List<Map<String, Object>> list = DBUtilities.exSelectQuery("select return_pickup_association.`pickup_shipment_id_fk`,return_pickup_association.`return_shipment_id_fk`,return_shipment.`source_return_id` from return_pickup_association join return_shipment on return_pickup_association.`return_shipment_id_fk`= `return_shipment`.id   where  return_shipment.source_return_id  in (" + returnID + " , " + returnID2 + " )  group by return_pickup_association.`pickup_shipment_id_fk`,return_pickup_association.`return_shipment_id_fk`,return_shipment.`source_return_id` ;", "myntra_lms");
		Map<String, Object> hm = (Map<String, Object>) list.get(0);
		Long pickupId = (Long) hm.get("pickup_shipment_id_fk");
		log.info("Pickup for return " + returnID + " is : " + pickupId);

		Map<String, Object> hm2 = (Map<String, Object>) list.get(1);
		Long pickupId2 = (Long) hm2.get("pickup_shipment_id_fk");
		log.info("Pickup for return " + returnID2 + " is : " + pickupId2);
		return pickupId.equals(pickupId2);
	}

	public boolean verifyClosedBoxConsolidation(List<String> returnIds) {
		List<Long> pickupIds =new ArrayList<>();
		boolean isOnePickupId=false;
		String returns=returnIds.iterator() + ",";
		if (returnIds.size()>1) {
			List <Map <String, Object>> list=DBUtilities.exSelectQuery(
					"select return_pickup_association.`pickup_shipment_id_fk`,return_pickup_association.`return_shipment_id_fk`,return_shipment.`source_return_id` from return_pickup_association join return_shipment on return_pickup_association.`return_shipment_id_fk`= `return_shipment`.id   where  return_shipment.source_return_id  in"+
							" ("+returns+" )  group by return_pickup_association.`pickup_shipment_id_fk`,return_pickup_association.`return_shipment_id_fk`,return_shipment.`source_return_id` ;" ,
					"myntra_lms");
			for (int i=0; i < returnIds.size(); i++) {
				Map <String, Object> hm=(Map <String, Object>) list.get(i);
				Long pickupId=(Long) hm.get("pickup_shipment_id_fk");
				log.info("Pickup for return "+returnIds.get(i)+" is : "+pickupId);
				pickupIds.add(pickupId);
			}
			for (int i=1; i<pickupIds.size(); i++) {
				if (pickupIds.get(i-1) != pickupIds.get(i))
					isOnePickupId = false;
			}
		}return isOnePickupId;

	}

	public boolean allElementsTheSame(int[] array) {
		if (array.length == 0) {
			return true;
		} else {
			int first = array[0];
			for (int element : array) {
				if (element != first) {
					return false;
				}
			}
			return true;
		}
	}

	public boolean validateClosedBoxRmsLmsReturnCreationV2(String returnId, String courier_code) throws IOException, ManagerException,
			JAXBException, InterruptedException, XMLStreamException, JSONException, SQLException {

		//Get the return_shipment details as a OrderResponse object, get return from RMS
		OrderResponse returnShipmentLMSObject = getReturnShipmentDetailsLMS(returnId);
		try {
			returnShipmentLMSObject.getOrders().get(0);
		}catch(IndexOutOfBoundsException e){
			Assert.fail("Return is not created in LMS");
		}
		Map<String, Object> returnShipmentLMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from return_shipment where source_return_id = " + returnId, "lms");
		Map<String, Object> returnRMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from returns where id = " + returnId, "rms");
		ReturnResponse returnResponseRMSObj = rmsServiceHelper.getReturnDetailsNew(String.valueOf(Long.parseLong(returnId)));

		//Basic validations : return_type=CLosed, courier_code=ML
		ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getCourierOperator().equals(courier_code), "Courier code assigned to this return is not " + courier_code);
		ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_type").equals("CLOSED_BOX_PICKUP"), "The return type is not CLOSED_BOX_PICKUP in LMS return");
		ExceptionHandler.handleTrue(returnRMSDB.get("return_mode").equals("CLOSED_BOX_PICKUP"), "The return type is not CLOSED_BOX_PICKUP in RMS return");

		//1. Validate if return_shipment  entry is created in LMS  with status "RETURN_CREATED"
		ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.RETURN_CREATED.toString(), 15), "Return-shipment is not created with status RETURN_CREATED in LMS");

		//2. Verify the status in RMS initially is LPI
		ExceptionHandler.handleTrue(rmsServiceHelper.validateReturnStatusInRMS(String.valueOf(Long.parseLong(returnId)), ReturnStatus.LPI.toString(), 15), "pickup is not created or not in LPI status in Returns");

		//3. Verify pickup is created with status PICKUP_CREATED
		ExceptionHandler.handleTrue(validatePickupShipmentStatusInLMS(returnId, ShipmentStatus.PICKUP_CREATED.toString(), 15), "Pickup-shipment is not created with status PICKUP_CREATED in LMS");

		//4. Verify the right return warehouse id  and return hub code is populated in return_shipment
		String orderId = returnResponseRMSObj.getData().get(0).getOrderId().toString();
		OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
		OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
		String expectedReturnWHid = String.valueOf(orderReleaseEntry.getWarehouseId());

		String expectedReturnHub = lmsHelper.getReturnHubCodeForWarehouse.apply(Long.parseLong(expectedReturnWHid)).toString();
		String actualReturnHub = getReturnWarehouseHub(returnId);
		String actualReturnWHid = getReturnWarehouseId(returnId);
		ExceptionHandler.handleTrue(actualReturnWHid.equals(expectedReturnWHid), "The return warehouse id stamped in return_shipment is not = the return_hub_warehouse configured");
		ExceptionHandler.handleTrue(expectedReturnHub.equals(actualReturnHub), "The return hub stamped in return_shipment is not = the return_hub_warehouse configured");


		//6. Verify return_shipment value = RMS refund value
//		ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getItemEntries().get(0).getItemValue().toString().equals(returnResponseRMSObj.getData().get(0).getReturnLineEntries().get(0).getReturnLineRefundDetailsEntry().getRefundAmount().toString()), "LMS Return_shipment shipment value not equal to RMS return refund value");

		//Verify return_shipment status = RIT
		ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getStatus().toString().equals(ShipmentStatus.valueOf("RETURN_CREATED").getShipmentStatusCode().toString()), "LMS return_shipment status is not RIT");

		//8. Verify rms (returns table) tracking number = LMS return_shipment tracking number
		log.info("The rms - return tracking number" + returnResponseRMSObj.getData().get(0).getReturnTrackingDetailsEntry());
		ExceptionHandler.handleTrue((returnResponseRMSObj.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo().toString().equals(returnShipmentLMSObject.getOrders().get(0).getTrackingNumber().toString())), "LMS return_shipment tracking number != RMS return tracking number");

		//9. Verify rms(return table) and lms (return_shipment) return entry data
		ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_type").toString().equals(returnResponseRMSObj.getData().get(0).getReturnMode().toString()), "return_mode/return_type Mismatch in LMS-RMS");

		//10 . Verify  LMS-RMS return mode
		ExceptionHandler.handleTrue(returnShipmentLMSDB.get("return_type").toString().equals(returnResponseRMSObj.getData().get(0).getReturnMode().toString()), "LMS RMS return_mode match error");

		//11. Verify RMS LMS email id
//		ExceptionHandler.handleTrue(returnShipmentLMSObject.getOrders().get(0).getEmail().toString().equals(returnResponseRMSObj.getData().get(0).getEmail().toString()), "email Mismatch in LMS-RMS");

		String pickup_shipment_id = getAssociatedPickupForReturn(returnId);

		//12. Using the pickup id , get the tracking number stamped in Order tracking table,Verify pickup tracking number and order_tracking's orderId and TrackingNumber match
		String trackingNumber_orderTracking = getOrderTrackingNumber(pickup_shipment_id);
		com.myntra.lms.client.domain.response.PickupResponse pickupResponse = getPickupShipmentDetailsLMS(pickup_shipment_id);
		String trackingNumber_pickupShipment = pickupResponse.getDomainPickupShipments().get(0).getTrackingNumber();
		ExceptionHandler.handleTrue(trackingNumber_orderTracking.toString().equals(trackingNumber_pickupShipment.toString()), "Order Tracking tracking_number !+ pickup_shipment tracking number");

		Map<String, Object> orderTrackingLMSDB = DBUtilities.exSelectQueryForSingleRecord("select * from order_tracking where tracking_no = '" + trackingNumber_pickupShipment + "'", "lms");

		//13. Verify Order_tracking statuses
		ExceptionHandler.handleTrue(returnShipmentLMSDB.get("courier_code").toString().equals(courier_code), "courier Mismatch in LMS-RMS");
		validateOrderTrackingShipmentStatus(trackingNumber_pickupShipment, ShipmentStatus.PICKUP_CREATED);
		//TODO : why failing for CBT?
		validateOrderTrackingCourierCreationStatus(trackingNumber_pickupShipment,CourierCreationStatus.NOT_INITIATED);
		validateOrderTrackingDeliveryStatus(trackingNumber_pickupShipment, ShipmentStatus.RETURN_CREATED.getOrderTrackingStatusCode().toString());
		ExceptionHandler.handleTrue(orderTrackingLMSDB.get("shipment_status").toString().equals(ShipmentStatus.PICKUP_CREATED.toString()), "order_tracking table shipment_status is not PICKUP_CREATED");

		//14. Verify order_Tracking_details
		List<String> activityTypeStatusCombination = new ArrayList<>();
		activityTypeStatusCombination.add("PICKUP_CREATED:PICKUP_CREATED:PICKUP_CREATED");
		activityTypeStatusCombination.add("PICKUP_DETAILS_UPDATED:PICKUP_CREATED:PICKUP_CREATED");
		//validateOrderTrackingDetailsForActivity(trackingNumber_orderTracking,activityTypeStatusCombination);
		return true;
	}


	public void updateReturnReceiveEvents(String returnId,String courier_code)
			throws JAXBException,  IOException {
		String tracking_number = getPickupTrackingNoOfReturn(returnId);

		ShipmentUpdate.ShipmentUpdateBuilder pickupBuilder = new ShipmentUpdate.ShipmentUpdateBuilder(courier_code, tracking_number,
				ShipmentUpdateEvent.RECEIVE);
		pickupBuilder.clientId(LMS_CONSTANTS.CLIENTID);
		pickupBuilder.tenantId(LMS_CONSTANTS.TENANTID);
		pickupBuilder.eventLocation("Myntra");
		pickupBuilder.eventTime(new DateTime());
		pickupBuilder.shipmentType(ShipmentType.PU);
		pickupBuilder.shipmentUpdateActivitySource(ShipmentUpdateActivityTypeSource.LogisticsPlatform);
		pickupBuilder.userName(Context.getContextInfo().getLoginId());
		pickupBuilder.remarks("Pickup Received at Returns Processing Center");
		pickupBuilder.eventLocationPremise(new Premise("5", DELIVERY_CENTER));
		ShipmentUpdate updatePickupStatus = pickupBuilder.build();
		String payload = APIUtilities.getObjectToJSON(updatePickupStatus);

		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_PICKUP_STATUS_EVENT, null,
				SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
		
		
		/*ShipmentUpdateResponse shipmentUpdateResponse = (ShipmentUpdateResponse) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(),
				new ShipmentUpdateResponse());*/

	}

	public void processSelfShipReturn(String returnId, String toStatus) throws Exception {
		TMSServiceHelper tmsServiceHelper=new TMSServiceHelper();
		long masterBagId=0L;
		switch (toStatus) {
			case EnumSCM.SELF_SHIP_QC_PASS:
				selfShipPickupQCUpdates(returnId,"","5", ShipmentUpdateEvent.RETURN_SUCCESSFUL);
				masterBagId = (long)tmsServiceHelper.createNcloseMBforReturn.apply(returnId);
				tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);
				Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
						.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WareHouse");
				Assert.assertEquals(
						lmsServiceHelper.receiveReturnShipmentFromMasterbag(masterBagId, returnId).getStatus().getStatusType().toString(),
						EnumSCM.SUCCESS, "Unable to receive shipment at WareHouse");
				ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.DELIVERED_TO_SELLER.toString(),10));
				lmsServiceHelper.validateRmsStatusAndRefund(returnId,EnumSCM.RRC, true,10000);
				break;
			case EnumSCM.SELF_SHIP_ON_HOLD_APPROVE:
				selfShipPickupQCUpdates(returnId,"","5", ShipmentUpdateEvent.RETURN_ON_HOLD);
				selfShipApproveOrReject(returnId, EnumSCM.APPROVED);
				selfShipPickUpApproveByCC(returnId,"","",ShipmentUpdateEvent.ACKNOWLEDGE_APPROVE_ONHOLD_WITH_RETURN_PROCESSING_CENTER);
				masterBagId = (long)tmsServiceHelper.createNcloseMBforReturn.apply(returnId);
				tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);
				Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
						.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WareHouse");
				Assert.assertEquals(
						lmsServiceHelper.receiveReturnShipmentFromMasterbag(masterBagId, returnId).getStatus().getStatusType().toString(),
						EnumSCM.SUCCESS, "Unable to receive shipment at WareHouse");
				ExceptionHandler.handleTrue(validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.DELIVERED_TO_SELLER.toString(),10));
				lmsServiceHelper.validateRmsStatusAndRefund(returnId,EnumSCM.RRC, true,10000);
				break;
			case EnumSCM.SELF_SHIP_ON_HOLD_REJECT:
				selfShipPickupQCUpdates(returnId,"","5", ShipmentUpdateEvent.RETURN_ON_HOLD);
				selfShipApproveOrReject(returnId, EnumSCM.REJECTED);
                 /*  TODO
		          1. Rship to Customer--- As this flow  has been stopped for now, so commented. Once it is fixed will update it*/

				break;
			default:
				log.info("No matching status for Self ship found");
				break;
		}
	}
	public void selfShipPickupQCUpdates(String returnId,String courierCode,String dc,ShipmentUpdateEvent toEvent) throws Exception{
		ArrayList<ReturnShipmentUpdate> returnShipmentUpdates = new ArrayList<>();

		Thread.sleep(8000);
		ClosedBoxPickupQCCompleteUpdate closedBoxPickupQCCompleteUpdate = new ClosedBoxPickupQCCompleteUpdate(returnId,TENANTID, CLIENTID,
				courierCode, "", ShipmentUpdateEvent.QUALITY_CHECK_COMPLETE, null, 5L,
				new Premise("5", Premise.PremiseType.DELIVERY_CENTER), "Location", new DateTime(), "remarks", ShipmentType.PU, ShipmentUpdateActivityTypeSource.LogisticsPortal, "Gloria", null, returnShipmentUpdates,null,null,null,null,null,null,null);
		String payload = APIUtilities.getObjectToJSON(closedBoxPickupQCCompleteUpdate);

		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.SELF_SHIP_PICKUP_QC_UPDATE, null,
				SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());

		String response = APIUtilities.getElement(service.getResponseBody(), "status.statusType", "json");
		Assert.assertEquals(response,StatusResponse.Type.SUCCESS.toString(),"Unable to mark the QC");
		//TODO : NOT able to Convert the Response to the POJO DomainShipmentUpdateResponse
	/*	DomainShipmentUpdateResponse domainShipmentUpdateResponse = (DomainShipmentUpdateResponse) APIUtilities.getJsontoObject(service.getResponseBody(),
				new DomainShipmentUpdateResponse());
	*/

	}
	public void selfShipApproveOrReject(String returnId, String toStatus) throws UnsupportedEncodingException,IOException{
		String pathParam=returnId+"/"+toStatus+"/lmsadmin?clientId="+CLIENTID+"&tenantId="+TENANTID+"";
		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.SELF_SHIP_APPROVE_OR_REJECT, new String[]{pathParam},
				SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, "", Headers.getLmsHeaderJSON());

		ReturnResponse returnResponse=(ReturnResponse)APIUtilities.getJsontoObject(service.getResponseBody(),
				new ReturnResponse());
		Assert.assertEquals(returnResponse.getStatus().getStatusType(),StatusResponse.Type.SUCCESS,"Unable to mark return "+toStatus);
	}

	public void selfShipPickUpApproveByCC(String returnId,String courierCode,String trackingNumber,ShipmentUpdateEvent toEvnet)throws UnsupportedEncodingException,IOException{

		Map<String, Object> return_shipment = DBUtilities.exSelectQueryForSingleRecord("select * from return_shipment where source_return_id = " + returnId, "lms");
		trackingNumber=return_shipment.get("tracking_number").toString();
		courierCode=return_shipment.get("courier_code").toString();

		ShipmentUpdate.ShipmentUpdateBuilder pickupBuilder = new ShipmentUpdate.ShipmentUpdateBuilder(courierCode, trackingNumber,
				toEvnet);
		pickupBuilder.eventLocation("Return Processing Center");
		pickupBuilder.eventTime(new DateTime());
		pickupBuilder.shipmentId(returnId);
		pickupBuilder.shipmentType(ShipmentType.RETURN);
		pickupBuilder.tenantId(TENANTID);
		pickupBuilder.clientId(CLIENTID);
		pickupBuilder.shipmentUpdateActivitySource(ShipmentUpdateActivityTypeSource.LogisticsPlatform);
		pickupBuilder.userName(Context.getContextInfo().getLoginId());
		pickupBuilder.remarks("Acknowledge approve onHold pickup with  RPC");
		pickupBuilder.eventAdditionalInfo(null);
		ShipmentUpdate updatePickupStatus = pickupBuilder.build();
		String payload = APIUtilities.getObjectToJSON(updatePickupStatus);
		String pathParam="?tenantId="+TENANTID+"&clientId="+CLIENTID+"";
		Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_RETURN_STATUS_EVENT, new String[]{pathParam},
				SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.PUT, "["+payload+"]", Headers.getLmsHeaderJSON());

    }
    public String getShipmentStatusFromOrder_to_ship(String trackingNumber){

		String query="select shipment_status from order_to_ship where tracking_number = '"+trackingNumber.toUpperCase()+"'";
		Map<String, Object> hm = DBUtilities.exSelectQueryForSingleRecord(query, "myntra_lms");
		String shipmentStatus= (String) hm.get("shipment_status");
		return shipmentStatus;
	}



}
