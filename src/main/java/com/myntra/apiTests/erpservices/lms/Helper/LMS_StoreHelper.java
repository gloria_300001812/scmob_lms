package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.lastmile.client.entry.StoreEntry;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.testng.Assert;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * @author Gloria R
 */
public class LMS_StoreHelper {
    public StoreResponse createStore (String DcCode , String Code , String MobileNumber) throws UnsupportedEncodingException, IOException {
        StoreEntry storeEntry = new StoreEntry();
        storeEntry.setName(Code);
        storeEntry.setOwnerFirstName("LMS_Automation");
        storeEntry.setOwnerLastName("Automation");
        storeEntry.setAddress("Myntra Designs");
        storeEntry.setContactNumber(MobileNumber);
        storeEntry.setCity("Bangalore");
        storeEntry.setEmailId("lmsAutomation@myntra.com");
        storeEntry.setState("KARNATAKA");
        storeEntry.setState(LMS_CONSTANTS.TENANTID);
        storeEntry.setCode(Code);
        storeEntry.setStoreType(StoreType.MASTER);
        storeEntry.setMappedDcCode(DcCode);
        String payload = APIUtilities.getObjectToJSON(storeEntry);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.CREATE_STORE, null, SERVICE_TYPE.Last_mile.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        StoreResponse response ;
        response = (StoreResponse) APIUtilities.getJsontoObject(service.getResponseBody() ,new StoreResponse());
        return response;
    }


    public void validateStoreCreation(String StoreId , String Code , String MobileNumber , String DcCode) throws UnsupportedEncodingException, IOException {
        Map<String, Object> DeliveryCenter = DBUtilities.exSelectQueryForSingleRecord("select * from delivery_center where id = "+StoreId, "lms");
        Assert.assertEquals(DeliveryCenter.get("code"),Code);
        Assert.assertEquals(DeliveryCenter.get("contact_no"),MobileNumber);
        Assert.assertEquals(DeliveryCenter.get("courier_code"),Code);
        Assert.assertEquals(DeliveryCenter.get("type"),"LAST_MILE_PARTNER");
        String tenantId = DeliveryCenter.get("tenant_id").toString();
        Map<String, Object> DeliveryStaff = DBUtilities.exSelectQueryForSingleRecord("select * from delivery_staff where tenant_id = "+tenantId, "lms");
        Assert.assertEquals(DeliveryStaff.get("code"),Code);
        Assert.assertEquals(DeliveryStaff.get("mobile"),MobileNumber);
        Assert.assertEquals(DeliveryStaff.get("emp_code"),Code);
        Assert.assertEquals(DeliveryStaff.get("tenant_id"),"tenantId");
        Map<String, Object> Courier = DBUtilities.exSelectQueryForSingleRecord("select * from courier where logistics_partner_id = "+tenantId, "lms");
        Assert.assertEquals(Courier.get("code"),Code);
        Assert.assertEquals(Courier.get("type"),"LAST_MILE_PARTNER");
        Assert.assertEquals(Courier.get("tracking_no_source"),"STORED");
    }
}
