package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.client.DeliveryCenterClient;
import com.myntra.apiTests.erpservices.lastmile.client.TripClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.service.DeliveryCenterClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.client.MLShipmentClient;
import com.myntra.apiTests.erpservices.lms.validators.NDRValidator;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.TripAction;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.BulkJobResponse;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.S3FileResponse;
import com.myntra.lms.client.status.BulkJobType;
import com.myntra.lms.client.status.ShipmentType;

import com.myntra.logistics.platform.domain.NDRStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateAdditionalInfo;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class NDRServiceHelper {

    String env = getEnvironment();
    LMSHelper lmsHelper = new LMSHelper();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    HashMap<String, String> pincodeDCMap = new HashMap<>();
    public static String DcId;
    DeliveryCenterClient_QA deliveryCenterClient_qa = new DeliveryCenterClient_QA();
    TripClient tripClient = new TripClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private OrderEntry orderEntry = new OrderEntry();
    TripClient_QA tripClient_qa = new TripClient_QA();
    StatusPollingValidator statusPollingValidator = new StatusPollingValidator();

    /**
     *
     * Used to get NDR Bulkjob Id
     * @param name
     * @param payload
     * @param bulkJobType
     * @param tenantId
     * @return
     * @throws IOException
     * @throws JAXBException
     * @throws XMLStreamException
     * @throws JSONException
     */
    public static BulkJobResponse ndrCreateBulkJob(String name, String payload, BulkJobType bulkJobType, String tenantId) throws IOException, JAXBException, XMLStreamException, JSONException {

        String pathparam = "?tenantId=" + tenantId;

        String str = String.format("{\n" +
                "    \"simpleJobEntry\": {\n" +
                "        \"name\": \"%s\",\n" +
                "        \"payload\": \"%s\",\n" +
                "        \"bulkJobType\": \"%s\"\n" +
                "    }\n" +
                "}", name, payload, bulkJobType.name());

        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.CREATE_BULK_JOB, new String[]{pathparam},
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.POST, str, Headers.getLmsHeaderJSON());
        BulkJobResponse bulkJobResponse = (BulkJobResponse) APIUtilities.jsonToObject(service.getResponseBody(), BulkJobResponse.class);
        return bulkJobResponse;
    }

    /**
     * Used to get NDRdownload URL
     * @param bulkJobId
     * @return
     * @throws UnsupportedEncodingException
     * @throws JAXBException
     * @throws XMLStreamException
     * @throws JSONException
     */
    public static S3FileResponse getNDRDownloadURL(Long bulkJobId) throws UnsupportedEncodingException, JAXBException, XMLStreamException, JSONException {

        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_BULKJOB + "/" + bulkJobId + "/downloadUrl", null,
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        S3FileResponse s3FileResponse = (S3FileResponse) APIUtilities.jsonToObject(service.getResponseBody(),
                S3FileResponse.class);
        return s3FileResponse;


    }
    public Long createTrip() {
        //Get SDA
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);

        //Create Trip
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        return tripId;
    }

   public void startTrip(Long tripId) {
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to startTrip");
    }

    public void addShipmentToTrip(Long tripId, String trackingNumber) throws WebClientException {
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
    }

    public Map<String, Object> processFailedExchange(String packetId, String trackingNumber, AttemptReasonCode attemptReasonCode, Long tripId) throws WebClientException, IOException, JAXBException {

        //Start trip
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name(), 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, attemptReasonCode, TripAction.UPDATE.name(), packetId, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");

        //MarK orers as Failed Exchange
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        dataMap.put("AttemptReasonCode", attemptReasonCode);
        return dataMap;
    }

    public void verifyAndUpdateNdrStatusAfterFD(String trackingNumber) throws Exception {
        MLShipmentResponse mlShipmentResponse1 = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(trackingNumber);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse1, "SUCCESS", "Success", "3");
        Assert.assertNull(mlShipmentResponse1.getMlShipmentEntries().get(0).getNdrStatus(), "After Delivery NDR status should be NULL");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //Update Order NDR status NULL To IN_PROGRESS for tracking Number
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.IN_PROGRESS, trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber, NDRStatus.IN_PROGRESS, ShipmentType.EXCHANGE, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());
        Thread.sleep(Constants.LMS_PATH.sleepTime);



    }

    public void verifyInProgressToRtoBlockedAndRequeue(String packetId, String trackingNumber) throws Exception {
        //Verify that, IN_PROGRESS to RTO is blocked
        com.myntra.lastmile.client.entry.MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.EXCHANGE, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber, null);

        //IN_PROGRESS to REQueue is allowed at this point.
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponses = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponses.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");
    }

    public void verifyInProgressToReattemptIsAllowed(String trackingNumber) throws Exception {
        //Verify IN_PROGRESS to REATTEMPT is allowed for FD packet.
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.REATTEMPT, trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber, NDRStatus.REATTEMPT, ShipmentType.EXCHANGE, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());
    }

    public void varifyNDRConfig1(String trackingNumber) throws Exception {
        SimpleDateFormat f = new SimpleDateFormat();
        //String date = f.format(DateTimeHelper.referenceDateTimeConv());
        String date = DateTimeHelper.referenceDateTimeConv();
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("initiating RTO for shipment");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");
    }

    public void varifyNDRConfig2(String trackingNumber) throws Exception {
        com.myntra.lastmile.client.entry.MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.EXCHANGE, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR" , "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber, null);
    }

    /**
     * Used to get the bulk job status
     * @param bulkJobId
     * @return
     * @throws UnsupportedEncodingException
     * @throws JAXBException
     */
    public static BulkJobResponse validate_BulkJob_Status(long bulkJobId) throws UnsupportedEncodingException, JAXBException, JSONException, XMLStreamException {
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_BULKJOB + "/" + bulkJobId, null,
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        BulkJobResponse response_status = (BulkJobResponse) APIUtilities.jsonToObject(service.getResponseBody(),
                BulkJobResponse.class);
        return response_status;

    }

}
