package com.myntra.apiTests.erpservices.lms.Flipkart.Domain;

import lombok.Data;

@Data
public class Shipment {

    private String mrpTotal;

    private String promiseDate;

    private String packageWeight;

    private ShipmentItems[] shipmentItems;

    private String packageBreadth;

    private String shipmentValue;

    private String shipmentType;

    private String rtoWarehouseId;

    private String recipientContactNumber;

    private String multiSellerShipment;

    private String codAmount;

    private String recipientName;

    private String packageLength;

    private String isFragile;

    private String recipientAddress;

    private String landmark;

    private String trackingNumber;

    private String isHazmat;

    private String email;

    private String packedDate;

    private String pincode;

    private String contentsDescription;

    private String sourceReferenceId;

    private String shippingMethod;

    private String containsFootwear;

    private String locality;

    private String packageHeight;

    private String warehouseId;

    private String tenantId;

    private String alternateContactNumber;

    private String isLarge;

    private String containsJewellery;

    private String courierCode;
}
