package com.myntra.apiTests.erpservices.lms.lmsClient;

/**
 * Created by Shubham Gupta on 5/15/17.
 */
public class MasterbagShipmentEntry
{
    private String secondaryMobileNumber;

    private String customerName;

    private String tmsMasterbagId;

    private String consignorAddress;

    private String consignorName;

    private String weight;

    private String shipmentAttribute;

    private String status;

    private String codAmount;

    private String vat;

    private String state;

    private String consignorTin;

    private String primaryMobileNumber;

    private String city;

    private String country;

    private String pincode;

    private String shipmentValue;

    private String trackingNumber;

    private String email;

    private String customerAddress;

    private String itemDetails;

    private String orderId;

    public String getSecondaryMobileNumber() {
        return secondaryMobileNumber;
    }

    public void setSecondaryMobileNumber(String secondaryMobileNumber) {
        this.secondaryMobileNumber = secondaryMobileNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getTmsMasterbagId() {
        return tmsMasterbagId;
    }

    public void setTmsMasterbagId(String tmsMasterbagId) {
        this.tmsMasterbagId = tmsMasterbagId;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getShipmentAttribute() {
        return shipmentAttribute;
    }

    public void setShipmentAttribute(String shipmentAttribute) {
        this.shipmentAttribute = shipmentAttribute;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCodAmount() {
        return codAmount;
    }

    public void setCodAmount(String codAmount) {
        this.codAmount = codAmount;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getConsignorTin() {
        return consignorTin;
    }

    public void setConsignorTin(String consignorTin) {
        this.consignorTin = consignorTin;
    }

    public String getPrimaryMobileNumber() {
        return primaryMobileNumber;
    }

    public void setPrimaryMobileNumber(String primaryMobileNumber) {
        this.primaryMobileNumber = primaryMobileNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getShipmentValue() {
        return shipmentValue;
    }

    public void setShipmentValue(String shipmentValue) {
        this.shipmentValue = shipmentValue;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(String itemDetails) {
        this.itemDetails = itemDetails;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "MasterbagShipmentEntry{" +
                "secondaryMobileNumber='" + secondaryMobileNumber + '\'' +
                ", customerName='" + customerName + '\'' +
                ", tmsMasterbagId='" + tmsMasterbagId + '\'' +
                ", consignorAddress='" + consignorAddress + '\'' +
                ", consignorName='" + consignorName + '\'' +
                ", weight='" + weight + '\'' +
                ", shipmentAttribute='" + shipmentAttribute + '\'' +
                ", status='" + status + '\'' +
                ", codAmount='" + codAmount + '\'' +
                ", vat='" + vat + '\'' +
                ", state='" + state + '\'' +
                ", consignorTin='" + consignorTin + '\'' +
                ", primaryMobileNumber='" + primaryMobileNumber + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", pincode='" + pincode + '\'' +
                ", shipmentValue='" + shipmentValue + '\'' +
                ", trackingNumber='" + trackingNumber + '\'' +
                ", email='" + email + '\'' +
                ", customerAddress='" + customerAddress + '\'' +
                ", itemDetails='" + itemDetails + '\'' +
                ", orderId='" + orderId + '\'' +
                '}';
    }
}
