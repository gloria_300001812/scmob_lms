package com.myntra.apiTests.erpservices.lms.Helper;

import com.google.common.collect.Multimap;
import lombok.Data;

import java.util.ArrayList;

@Data
public class OrderUpdateStates {
    ArrayList<String> fromStates;
    ArrayList<String> toStates;
    ArrayList<String> orderId;
    ArrayList<String> trackingNUmber;
    ArrayList<String> orderToShipExpectedStatus;
    ArrayList<String> mlShipmentExpectedStatus;
    ArrayList<String> omsExpectedStatus;
    Multimap<String,String> fromToStates;
}
