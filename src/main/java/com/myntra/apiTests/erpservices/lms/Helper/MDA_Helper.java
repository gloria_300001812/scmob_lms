package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.client.tools.response.ApplicationPropertiesResponse;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.TripAction;
import com.myntra.lastmile.client.code.utils.UpdatedVia;
import com.myntra.lastmile.client.entry.ScanAndSortShipmentRequest;
import com.myntra.lastmile.client.entry.TripOrderAssignementEntry;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.ScanAndSortShipmentResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MDA_Helper {

    private static Logger log = Logger.getLogger(LmsServiceHelper.class);

    public static String authenticatePartnerAPI() throws WebClientException, IOException, JAXBException, XMLStreamException, JSONException {

        Svc service = HttpExecutorService.executeHttpService(Constants.PARTNERAPI.AUTHENTICATE, new String[]{},
                SERVICE_TYPE.PARTNER_API.toString(), HTTPMethods.POST, null, getPartnerAPIHeaders(false));
        String response = service.getResponseBody();
        return response;
    }

    public static String tokenExtraction(String response) {
        String pattern = "BEARER [\\w]*\\b";
        Matcher matcher = Pattern.compile(pattern).matcher(response);
        if (matcher.find())
            return matcher.group(0);
        else
            return null;
    }

    public static HashMap<String, String> getPartnerAPIHeaders(boolean getToken) throws JSONException, JAXBException, WebClientException, XMLStreamException, IOException {
        HashMap<String, String> createOrderHeaders = new HashMap<String, String>();
        createOrderHeaders.put("Authorization", "Basic Nzc2MDA4OTg1MDpteW50cmFAMTIz");
        if (getToken) {
            String response = authenticatePartnerAPI();
            String token = tokenExtraction(response);

            createOrderHeaders.put("Authorization", token);
        }
        createOrderHeaders.put("Content-Type", "application/xml");
        createOrderHeaders.put("Accept", "application/xml");
        return createOrderHeaders;
    }

    public static HashMap<String, String> getPartnerAPIHeadersJSON(boolean getToken) throws JSONException, JAXBException, WebClientException, XMLStreamException, IOException {
        HashMap<String, String> createOrderHeaders = new HashMap<String, String>();
        createOrderHeaders.put("Authorization", "Basic Nzc2MDA4OTg1MDpteW50cmFAMTIz");
        if (getToken) {
            String response = authenticatePartnerAPI();
            String token = tokenExtraction(response);

            createOrderHeaders.put("Authorization", token);
        }
        createOrderHeaders.put("Content-Type", "application/json");
        createOrderHeaders.put("Accept", "application/json");
        return createOrderHeaders;
    }

    public TripOrderAssignmentResponse activeTripOrders(String tenantId) throws IOException, JAXBException, WebClientException, JSONException, XMLStreamException {
        String param = "?tenantId=" + tenantId;
        Svc service = HttpExecutorService.executeHttpService(Constants.PARTNERAPI.ACTIVE_TRIP_ORDERS, new String[]{param}, SERVICE_TYPE.PARTNER_API.toString(),
                HTTPMethods.GET, null, getPartnerAPIHeaders(true));
        TripOrderAssignmentResponse shipmentResponse = (TripOrderAssignmentResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        return shipmentResponse;

    }

    public DeliveryStaffResponse findSDAByMobNo(String mobNo) throws
            IOException, JAXBException, JSONException, XMLStreamException, WebClientException {

        Svc service = HttpExecutorService.executeHttpService(Constants.PARTNERAPI.FIND_BY_MOB_NO, new String[]{mobNo},
                SERVICE_TYPE.PARTNER_API.toString(), HTTPMethods.GET, null, getPartnerAPIHeaders(true));
        DeliveryStaffResponse response = (DeliveryStaffResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new DeliveryStaffResponse());

        if (response.getStatus().getStatusMessage().contains("Unable to filter")) {
            System.out.println("Method name :getDC -Unable to get delivery centers - Check erpredis , or check if LASTMILE is UP");
            Assert.fail("Method name :getDC - Unable to get delivery centers - Check erpredis ,or check if LASTMILE is UP");
        }
        return response;
    }

    public DeliveryStaffResponse deliveryStaffForDeliveryCenter(String DcId) throws
            IOException, JAXBException, JSONException, XMLStreamException, WebClientException {
        Svc service = HttpExecutorService.executeHttpService(Constants.PARTNERAPI.FIND_DELIVERY_STAFF_BY_DELIVERY_CENTER, new String[]{DcId},
                SERVICE_TYPE.PARTNER_API.toString(), HTTPMethods.GET, null, getPartnerAPIHeaders(true));
        DeliveryStaffResponse response = (DeliveryStaffResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new DeliveryStaffResponse());
        if (response.getStatus().getStatusMessage().contains("Unable to filter")) {
            log.info("Method name :getDC -Unable to get delivery centers - Check erpredis , or check if LASTMILE is UP");
            Assert.fail("Method name :getDC - Unable to get delivery centers - Check erpredis ,or check if LASTMILE is UP");
        }
        return response;
    }

    public TripOrderAssignmentResponse getAllUnattemptedShipments(String DcId) throws IOException, JAXBException, WebClientException, JSONException, XMLStreamException {

        Svc service = HttpExecutorService.executeHttpService(Constants.PARTNERAPI.GET_ALL_UNATTEMPTED_SHIPMENTS, new String[]{DcId}, SERVICE_TYPE.PARTNER_API.toString(),
                HTTPMethods.GET, null, getPartnerAPIHeaders(true));
        TripOrderAssignmentResponse shipmentResponse = (TripOrderAssignmentResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        return shipmentResponse;
    }

    public ScanAndSortShipmentResponse scanAndSort(String eventLocation, String tenantId, String trackingNumber) throws IOException, JAXBException, WebClientException, JSONException, XMLStreamException {
        ScanAndSortShipmentRequest scanAndSortShipmentRequest = new ScanAndSortShipmentRequest() ;
        scanAndSortShipmentRequest.setTrackingNumber(trackingNumber);
        scanAndSortShipmentRequest.setTenantId(tenantId);
        scanAndSortShipmentRequest.setEventLocation(eventLocation);
        String payload = APIUtilities.convertXMLObjectToString(scanAndSortShipmentRequest);

        Svc service = HttpExecutorService.executeHttpService(Constants.PARTNERAPI.SCAN_SORT, new String[]{}, SERVICE_TYPE.PARTNER_API.toString(),
                HTTPMethods.POST, payload, getPartnerAPIHeaders(true));
        ScanAndSortShipmentResponse shipmentResponse = (ScanAndSortShipmentResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new ScanAndSortShipmentResponse());
        return shipmentResponse;

    }

    //TripShipmentAssociationResponse
    public TripShipmentAssociationResponse findShipmentsByTrip(String tripId, String shipmentType, String tenantId) throws IOException, JAXBException, XMLStreamException, JSONException, WebClientException {
        String param = tripId+"/"+shipmentType+"?"+"tenantId="+tenantId;
        Svc service = HttpExecutorService.executeHttpService(Constants.PARTNERAPI.FIND_SHIPMENT_BY_TRIP, new String[]{param},
                SERVICE_TYPE.PARTNER_API.toString(), HTTPMethods.GET, null, getPartnerAPIHeaders(true));
        TripShipmentAssociationResponse response = (TripShipmentAssociationResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                TripShipmentAssociationResponse.class);
        return response;
    }

    public TripOrderAssignmentResponse updateOrderInTrip(String orderId, String status, String tripAction, long tripId)
            throws IOException, JAXBException, WebClientException, XMLStreamException, JSONException {
        AttemptReasonCode s = AttemptReasonCode.DELIVERED;
        if (status.equals(EnumSCM.LOST)) {
            s = AttemptReasonCode.OTHERS;
        } else if (status.equals(EnumSCM.FAILED)) {
            s = AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE;
        }
        TripAction ta = TripAction.TRIP_COMPLETE;
        if (tripAction.equalsIgnoreCase(EnumSCM.UPDATE)) {
            ta = TripAction.UPDATE;
        } else if (tripAction.equalsIgnoreCase("TRIPSTART")) {
            ta = TripAction.TRIP_START;
        } else if (tripAction.equalsIgnoreCase("MARKRETURNSCAN")) {
            ta = TripAction.MARK_RETURNSCAN;
        } else if (tripAction.equalsIgnoreCase("MARKOUTSCAN")) {
            ta = TripAction.MARK_OUTSCAN;
        }
        TripOrderAssignmentResponse tripOrderAssignmentResponse = new TripOrderAssignmentResponse();
        TripOrderAssignementEntry tripOrderAssignementEntry = new TripOrderAssignementEntry();



        tripOrderAssignementEntry.setRemark("test");
        tripOrderAssignementEntry.setAttemptReasonCode(s);
        tripOrderAssignementEntry.setTripAction(ta);
        tripOrderAssignementEntry.setUpdatedVia(UpdatedVia.WEB);
        tripOrderAssignementEntry.setPaymentType("CASH");
        tripOrderAssignementEntry.setIsOutScanned(true);
        tripOrderAssignementEntry.setOrderId(orderId);
        tripOrderAssignementEntry.setTripId(tripId);
        tripOrderAssignementEntry.setReceivedAt("HOME");
        tripOrderAssignementEntry.setReceivedBy("SELF");
        tripOrderAssignementEntry.setCustomerSignature("iVBORw0KGgoAAAANSUhEUgAAAUIAAAH0CAIAAADHXLOjAAAAA3NCSVQFBgUzC42AAAAXkUlEQVR4\\nnO3dYWhcZb7H8edcUpiBFiZgYQZaMEHBCRVMUDCDfWGCwm1RaIKCCQrdVEETBa17QRv7Yrfdhbut\\nFzRxQZMuWBKhkhQUu3CXxBcujbCSCC5JQckILWTAwgy0MAMtzH2R3KSdeZ4zZ86cmZz/eb6fV7uT\\nkzOnm/Pb/3POec7/ccrlsgIg2X/s9gEAaBQxBsQjxoB4xBgQjxgD4hFjQDxiDIhHjAHxiDEgHjEG\\nxCPGgHjEGBCPGAPiEWNAPGIMiEeMAfGIMSAeMQbEI8aAeMQYEI8YA+IRY0A8YgyIR4wB8YgxIB4x\\nBsQjxoB4xBgQjxgD4hFjQDxiDIhHjAHxiDEgHjEGxCPGgHjEGBCPGAPiEWNAPGIMiEeMAfGIMSAe\\nMQbEI8aAeMQYEI8YA+IRY0A8YgyIR4wB8YgxIB4xBsQjxoB4xBgQjxgD4hFjQDxiDIhHjAHxiDEg\\nHjEGxCPGgHjEGBCPGAPiEWNAPGIMiEeMAfGIMSAeMQbEI8aAeMQYEI8YA+IRY0A8YgyIR4wB8Ygx\\nIB4xBsQjxoB4xBgQjxgD4hFjQDxiDIhHjAHxiDEgHjEGxCPGgHjEGBCPGAPiEWNAPGIMiEeMAfGI\\nMSAeMQbEI8aAeMQYEI8YA+IRY0A8YgyIR4wB8YgxIB4xBsQjxoB4xBgQjxgD4hFjQDxiDIhHjAHx\\niDEgHjEGxCPGgHjEGBCPGAPiEWNAPGIMiEeMAfGIMSAeMQbEI8aAeMQYEI8YA+IRY0A8YgyIR4wB\\n8YgxIB4xBsQjxoB4xBgQjxgD4hFjQDxiDIhHjAHxiDEgHjEGxCPGgHjEGBCPGAPiEWNAPGIMiEeM\\nAfGIMSAeMQbEI8aAeMQYEI8YA+IRY0A8YgyIR4wB8YgxIB4xBsQjxoB4xBgQjxgD4hFjQDxiDIhH\\njAHxiDEgHjEGxCPGgHjEGBCPGAPiEWNAPGIMiEeMAfGIMSAeMQbEI8aAeMQYEI8YA+IRY0A8YgyI\\nR4wB8YgxIB4xBsQjxoB4xBgQjxgD4hFjQDxiDIhHjAHxiDEgHjEGxCPGgHjEGBCPGAPiEWNAPGIM\\niEeMEQWlUql0t7TbR7FriDEEG3tnzNnjOI4Tj8fje+KO45x448RuH9QucMrl8m4fA1A3x3FMP0o8\\nkMj/lm/lwew6qjGE6X+23yXDSqnCzcLsl7MtO54woBpDEvcA38uqE5tqDBnmv573nmHbEGMIcPbP\\nZwefH6zrVwqFQpMOJoSIMcJu7K2x8ffH6/2tcx+ea8bBhBMxRqiNnx6f/HjSxy+2728P/GBCixgj\\nvM5/fP7sH8+6bDD65qjpVtbwS8PNOagw4k41Qmr2y9nhF92imM/nE4mEMty+turEJsYIo6XvlzK9\\nGeOP21T5ztZ5WygU2ts142erTmwG1Qidws2CS4YTDyS2M6yUmvt6riUHFWrEGKHjcncqtjdWMdEy\\ndyPX/CMKOwbVCJFSqRSPx00/jcVixWKx4kNnj6PuetoywqjGCBGXDCul9MmsyrBSavrz6YCOSAZi\\nHHbxfXHnHl3prt0+omZx9rjNtaxr2Jh+ON3w4UhCjMPr6PNHHccp3b7vbfi1a2uO47Tvby+VIvWW\\n/ODQoLaubqr30i/9iF0x5to4pDK9maXvl9y3SSaTa2trm89ORcveyHYe7DT9NH");


        List<TripOrderAssignementEntry> tripOrderAssignementEntries = new ArrayList<>();
        tripOrderAssignementEntries.add(tripOrderAssignementEntry);
        tripOrderAssignmentResponse.setTripOrderAssignmentEntries(tripOrderAssignementEntries);
        String payload = APIUtilities.convertXMLObjectToString(tripOrderAssignmentResponse);
        Svc service = HttpExecutorService.executeHttpService(Constants.PARTNERAPI.UPDATE_TRIP, new String[]{} ,SERVICE_TYPE.PARTNER_API.toString(),
                HTTPMethods.POST, payload, getPartnerAPIHeaders(true));
        TripOrderAssignmentResponse shipmentResponse = (TripOrderAssignmentResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new TripOrderAssignmentResponse());
        return shipmentResponse;
    }

    public ApplicationPropertiesResponse mdaConfig() throws IOException, JAXBException, WebClientException, JSONException, XMLStreamException {

        Svc service = HttpExecutorService.executeHttpService(Constants.PARTNERAPI.MDA_CONFIG, new String[]{}, SERVICE_TYPE.PARTNER_API.toString(),
                HTTPMethods.GET, null, getPartnerAPIHeaders(false));
        ApplicationPropertiesResponse shipmentResponse = (ApplicationPropertiesResponse) APIUtilities
                .convertXMLStringToObject(service.getResponseBody(), new ApplicationPropertiesResponse());
        return shipmentResponse;

    }

}