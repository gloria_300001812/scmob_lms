package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.common.Constants.LambdaInterfaces;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShipmentB2B;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShipmentItem;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.commons.exception.ManagerException;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.domain.shipment.Shipment;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import com.myntra.tms.config.lane_hub_config.LaneHubConfigResponse;
import com.myntra.tms.config.supported_transport_hub_config.SupportedTransportHubConfigResponse;
import com.myntra.tms.config.transporter_lane_config.TransporterLaneConfigResponse;
import com.myntra.tms.container.ContainerEntry;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.domain.TMSMasterbagStatus;
import com.myntra.tms.hub.TransportHubPendencyResponse;
import com.myntra.tms.hub.TransportHubPendencyEntry;
import com.myntra.tms.lane.LaneEntry;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagEntry;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import com.myntra.tms.masterbag.TMSMasterbagShipmentEntry;
import com.myntra.tms.pendency.TmsPreAlertResponse;
import com.myntra.tms.statemachine.masterbag.MasterbagUpdate;
import com.myntra.tms.statemachine.masterbag.MasterbagUpdateEvent;
import com.myntra.tms.track.TmsTrackingDetailResponse;
import com.myntra.tms.transporter.TMSTransporterEntry;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.joda.time.DateTime;
import org.testng.Assert;
import org.xbill.DNS.Master;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by Shubham Gupta on 5/15/17.
 */
public class TMSServiceHelper {

	private static Initialize init = new Initialize("/Data/configuration");
    static Logger log = Logger.getLogger(TMSServiceHelper.class);
	private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
	MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    MasterBagValidator masterBagValidator = new MasterBagValidator();

	private static String envName = init.Configurations.GetTestEnvironemnt().name();
	static String rabbitMqName = null;

    public TMSServiceHelper() {
    	
        if (envName.equalsIgnoreCase("fox7")) {
            rabbitMqName = "d7erprabbitmq.myntra.com";
        } else if (envName.equalsIgnoreCase("M7")) {
            rabbitMqName = "d7erprabbitmq.myntra.com";
        } else {
            rabbitMqName = "pp1erprabbitmq.myntra.com";
        }
    }

    /**
     * getTransporter
     * Object param
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getTransporter = param -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_TRANSPORTER, new String[]{param.toString()}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new TransporterResponse());

    /**
     * getTmsHub
     * Object param
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getTmsHub = param -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_HUB, new String[]{param.toString()}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getBasicHeaderXML()).getResponseBody(), new HubResponse());

    /**
     * getLane
     * Object param
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getLane = param -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.LANE, new String[]{param.toString()}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new LaneResponse());

    /**
     * addTMSTransporter
     * Object name
     * Object mode
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.BiFunction addTMSTransporter = (name, mode) -> {
    	
        TMSTransporterEntry transporter = new TMSTransporterEntry();
        transporter.setName(name.toString());
        transporter.setMode(mode.toString());
        transporter.setContactNumber("1234567890");
        transporter.setActive(false);
        transporter.setType("DIRECT");
        String payload = APIUtilities.convertXMLObjectToString(transporter);
        return APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_TRANSPORTER, null, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.POST, payload, Headers.getLmsHeaderXML()).getResponseBody(), new TransporterResponse());
    };

    /**
     * addLane
     * Object name
     * Object source
     * Object destination
     * Object type
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.FourFunction addLane = (name, source, destination, type) -> {
        LaneEntry lane = new LaneEntry();
        lane.setName(name.toString());
        lane.setActive(false);
        lane.setType((LaneType)type);
        lane.setSourceHubCode(source.toString());
        lane.setDestinationHubCode(destination.toString());
        String payload = APIUtilities.convertXMLObjectToString(lane);
        return APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.LANE, null, SERVICE_TYPE.TMS_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML()).getResponseBody(), new LaneResponse());
    };

    /**
     * addTMSHub
     * Object code
     * Object name
     * Object pincode
     * Object type
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.FourFunction addTMSHubDeprecated = (code, name, pincode, type) -> {
        HubEntry hub = new HubEntry();
        hub.setCode(code.toString());
        hub.setName(name.toString());
        hub.setActive(false);
        hub.setType((HubType)type);
        hub.setPincode(pincode.toString());
        hub.setContactNumber("1234567890");
        hub.setAddress("Automation test address");
        hub.setCity("Test city");
        hub.setState("KA");
        hub.setManager("Test manager");
        String payload = APIUtilities.convertXMLObjectToString(hub);
        return APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_HUB, null, SERVICE_TYPE.TMS_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML()).getResponseBody(), new HubResponse());
    };

    /**
     * addTMSHub
     * Object code
     * Object name
     * Object pincode
     * Object type
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.FourFunction addTMSHub = (code, name, pincode, type) -> {
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        HubEntry hub = new HubEntry();
        hub.setCode(code.toString());
        hub.setName(name.toString());
        hub.setActive(true);
        hub.setType((HubType)type);
        hub.setPincode(pincode.toString());
        hub.setContactNumber(""+contactNumber);
        hub.setAddress("Automation test address");
        hub.setCity("Test city");
        hub.setState("KA");
        hub.setManager("Test manager");
        hub.setTenantId(LMS_CONSTANTS.TENANTID);
        hub.setTmsTransportHubCode("TH-BLR");
        String payload = APIUtilities.convertXMLObjectToString(hub);
        return APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.LMS_HUB, null, SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML()).getResponseBody(), new HubResponse());
    };

    /**
     * getTmsMasterBag
     * Object param
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getTmsMasterBag = param -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_MASTERBAG, new String[]{param.toString()}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new TMSMasterbagReponse());

    /**
     * getTmsMasterBagById
     * id
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getTmsMasterBagById = id -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_MASTERBAG, new String[]{"search?q=masterbagId.eq:" + id}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new TMSMasterbagReponse());


    /**
     * getMasterbagTrackingDetail
     * masterbagId
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.BiFunction getMasterbagTrackingDetail = (tenantId , masterBagId) -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_MASTERBAG_TRACKINGDETAIL, new String[]{tenantId.toString(), masterBagId.toString()}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new TmsTrackingDetailResponse());

    /**
     * getTransportHubPendency
     * sourceCode, destinationCode
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.BiFunction getTransportHubPendency = (sourceCode, destinationCode) -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_TRANSPORTER_HUB_PENDENCY,
            new String[]{LMS_CONSTANTS.TENANTID,sourceCode.toString(), destinationCode.toString(), LocalDateTime.now().minusDays(3).toLocalDate().toString(), LocalDateTime.now().plusDays(1).toLocalDate().toString()}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new TransportHubPendencyResponse());

    /**
     * getTransporter
     * Object param
     */

    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getByHubCode = (HubCode) -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_HUB_BYCODE, new String[]{LMS_CONSTANTS.TENANTID , HubCode.toString()}, SERVICE_TYPE.LMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new HubResponse());

    /**
     * getMisrouteBagsHubPendency
     * sourceCode, destinationCode
     */
    @SuppressWarnings("rawtypes")

    public LambdaInterfaces.BiFunction getMisrouteBagsHubPendency = (sourceCode, destinationCode) -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_MISROUTE_PENDENCY,
            new String[]{LMS_CONSTANTS.TENANTID,sourceCode.toString(), destinationCode.toString(), LocalDateTime.now().minusDays(3).toLocalDate().toString(), LocalDateTime.now().plusDays(1).toLocalDate().toString()}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new TransportHubPendencyResponse());

    /**
     * getMasterbagPreAlert
     * sourceCode, destinationCode, transporter)
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.TriFunction getMasterbagPreAlert = (sourceCode, destinationCode, transporter) -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.MASTERBAG_PREALERT,
            new String[]{LMS_CONSTANTS.TENANTID,sourceCode.toString(),sourceCode.toString(), destinationCode.toString(),transporter.toString()}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new TmsPreAlertResponse());

    /**
     * getContainer
     * param
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getContainer = param -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.CONTAINER, new String[]{param.toString()}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new ContainerResponse());

    /**
     * getContainerTrackingDetail
     * param
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getContainerTrackingDetail = param -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.CONTAINER_TRACKING_DETAIL, new String[]{LMS_CONSTANTS.TENANTID,param.toString()}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new TmsTrackingDetailResponse());

    /**
     * getLocationHubConfig
     * locationHubCode
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getLocationHubConfig = locationHubCode -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_LOCATION_HUB_CONFIG, new String[]{LMS_CONSTANTS.TENANTID,locationHubCode.toString()}, SERVICE_TYPE.LMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new HubToTransportHubConfigResponse());

    public LambdaInterfaces.Function getLocationHubConfigFW = locationHubCode -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_LOCATION_HUB_CONFIG_FW, new String[]{LMS_CONSTANTS.TENANTID,locationHubCode.toString()}, SERVICE_TYPE.LMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new HubToTransportHubConfigResponse());

    /**
     * getLocationHubConfig
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Supplier downloadTransporterLaneConfig = () -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TRANSPORTER_LANE_CONFIG_DOWNLOAD, new String[]{LMS_CONSTANTS.TENANTID}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new TransporterLaneConfigResponse());

    /**
     * downloadLocationHubToTransportHubConfig
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Supplier downloadLocationHubToTransportHubConfig = () -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.LH_TH_CONFIG_DOWNLOAD, new String[]{LMS_CONSTANTS.TENANTID}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new HubToTransportHubConfigResponse());

    public LambdaInterfaces.Supplier downloadHubToTransportHubConfig = () -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.HUB_TO_TH_CONFIG_DOWNLOAD, new String[]{LMS_CONSTANTS.TENANTID}, SERVICE_TYPE.LMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new HubToTransportHubConfigResponse());


    /**
     * downloadLaneHubConfig
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Supplier downloadLaneHubConfig = () -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.LANE_HUB_CONFIG_DOWNLOAD, new String[]{LMS_CONSTANTS.TENANTID}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new LaneHubConfigResponse());

    /**
     * downloadSupportedTransportHubConfig
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Supplier downloadSupportedTransportHubConfig = () -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.SUPPORTED_TRANSPORTHUB_CONFIG_DOWNLOAD,  new String[]{LMS_CONSTANTS.TENANTID}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new SupportedTransportHubConfigResponse());

    /**
     * getContainerManifest
     * containerId
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getContainerManifest = containerId -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_CONTAINER_MANIFEST, new String[]{ "container/"+LMS_CONSTANTS.TENANTID+"?containerIds[]="+containerId}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new ManifestResponse());

    /**
     * getMasterBagManifest
     * Object masterBagId
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getMasterBagManifest = masterbagId -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_MASTERBAG_MANIFEST, new String[]{LMS_CONSTANTS.TENANTID,masterbagId.toString()}, SERVICE_TYPE.TMS_SVC.toString(),
            HTTPMethods.GET, null, Headers.getLmsHeaderformData()).getResponseBody(), new ManifestResponse());

    /**
     * tmsReceiveMasterBag
     * hubCode, masterBagId
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getContainerIdForMb = masterBagId -> DBUtilities.exSelectQueryForSingleRecord("select `container_id` from `container_masterbag_association` where `masterbag_id` in ( select id from masterbag where `source_reference_id` = " + masterBagId +");" , "myntra_tms");



    public LambdaInterfaces.BiFunction tmsReceiveMasterBag = (hubCode, masterBagId) -> {

        TMSMasterbagEntry tmsMasterbagEntry = new TMSMasterbagEntry();
        tmsMasterbagEntry.setMasterbagId(masterBagId.toString());
        tmsMasterbagEntry.setWeight(15.00);
        tmsMasterbagEntry.setSealId(Long.parseLong(LMSUtils.randomGenn(8)));
        String payload = APIUtilities.convertXMLObjectToString(tmsMasterbagEntry);
        return APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_RECEIVE_MASTERBAG, new String[]{LMS_CONSTANTS.TENANTID,hubCode.toString(), "false"}, SERVICE_TYPE.TMS_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML()).getResponseBody(), new TMSMasterbagReponse());
    };

    //Receive master bag in TMS
    public LambdaInterfaces.TriFunction receiveMasterBagInTMS = (hubCode, masterBagId,tenantId) -> {

        TMSMasterbagEntry tmsMasterbagEntry = new TMSMasterbagEntry();
        tmsMasterbagEntry.setMasterbagId(masterBagId.toString());
        tmsMasterbagEntry.setWeight(15.00);
        tmsMasterbagEntry.setSealId(Long.parseLong(LMSUtils.randomGenn(8)));
        String payload = APIUtilities.convertXMLObjectToString(tmsMasterbagEntry);
        TMSMasterbagReponse tmsMasterbagReponse=(TMSMasterbagReponse) APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_RECEIVE_MASTERBAG, new String[]{tenantId.toString(),hubCode.toString(), "false"}, SERVICE_TYPE.TMS_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML()).getResponseBody(), new TMSMasterbagReponse());
        return tmsMasterbagReponse;
    };


    public LambdaInterfaces.TriFunction tmsReceiveMasterBagNew = (hubCode, masterBagId, containerId) -> {
        MasterbagUpdate masterbagUpdate=new MasterbagUpdate();
        masterbagUpdate.setMasterbagId(masterBagId.toString());
        masterbagUpdate.setContainerId(Long.parseLong(containerId.toString()));
        masterbagUpdate.setEventLocation(hubCode.toString());
        masterbagUpdate.setMasterbagUpdateEvent(MasterbagUpdateEvent.RECEIVE);
        masterbagUpdate.setTenantId(LMS_CONSTANTS.TENANTID);
        masterbagUpdate.setForceReceive(false);
        String payload = APIUtilities.convertXMLObjectToString(masterbagUpdate);
        return APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_RECEIVE_MASTERBAG_NEW, null, SERVICE_TYPE.TMS_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML()).getResponseBody(), new TMSMasterbagReponse());
    };


    /**
     * tmsReceiveMasterBagForce
     * transportHubId, masterBagId
     */
    @SuppressWarnings("rawtypes")
	public LambdaInterfaces.BiFunction tmsReceiveMasterBagForce = (transportHubId, masterBagId) -> {
        TMSMasterbagEntry tmsMasterbagEntry = new TMSMasterbagEntry();
        tmsMasterbagEntry.setMasterbagId(masterBagId.toString());
        String payload = APIUtilities.convertXMLObjectToString(tmsMasterbagEntry);
        return APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_RECEIVE_MASTERBAG, new String[]{LMS_CONSTANTS.TENANTID,transportHubId.toString(), "true"}, SERVICE_TYPE.TMS_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML()).getResponseBody(), new TMSMasterbagReponse());
    };

    public LambdaInterfaces.TriFunction tmsReceiveMasterBagForceNew = (transportHubId, masterBagId ,containerId) -> {
        MasterbagUpdate masterbagUpdate=new MasterbagUpdate();
        masterbagUpdate.setMasterbagId(masterBagId.toString());
        masterbagUpdate.setContainerId(Long.parseLong(containerId.toString()));
        masterbagUpdate.setEventLocation(transportHubId.toString());
        masterbagUpdate.setMasterbagUpdateEvent(MasterbagUpdateEvent.RECEIVE);
        masterbagUpdate.setTenantId(LMS_CONSTANTS.TENANTID);
        masterbagUpdate.setForceReceive(false);
        String payload = APIUtilities.convertXMLObjectToString(masterbagUpdate);
        return APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_RECEIVE_MASTERBAG_NEW, null, SERVICE_TYPE.TMS_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML()).getResponseBody(), new TMSMasterbagReponse());
    };

    /**
     * createContainer
     * source, dest, laneId, transporterId
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.FourFunction createContainer = (source, dest, laneId, transporterId) -> {
        ContainerEntry container = new ContainerEntry();
        container.setOriginHubCode(source.toString());
        container.setDestinationHubCode(dest.toString());
        container.setLaneId((long) laneId);
        container.setTransporterId((long) transporterId);
        container.setDriverMobileNumber(LMSUtils.randomGenn(10));
        container.setDriverName("Automation driver");
        container.setDocketNumber(LMSUtils.randomGenn(8));
        container.setVehicleNumber("KA-01 TE-" + LMSUtils.randomGenn(4));
        String payload = APIUtilities.convertXMLObjectToString(container);
        return APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_CONTAINER, null, SERVICE_TYPE.TMS_SVC.toString(),
                HTTPMethods.POST, payload, Headers.getLmsHeaderXML()).getResponseBody(), new ContainerResponse());
    };

    /**
     * getMisrouteTransporter
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public LambdaInterfaces.Supplier getMisrouteTransporter = ()-> ((TransporterResponse) getTransporter.apply("search?q=name.like:MISROUT&start=0&fetchSize=-1&sortBy=id&sortOrder=ASC")).getTransporterEntries().get(0).getId();

    /**
     * getMisrouteLane
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public LambdaInterfaces.Supplier getMisrouteLane = ()-> ((LaneResponse) getLane.apply("search?q=name.like:MISROUTE___type.like:MISROUTE&start=0&fetchSize=20&sortBy=name&sortOrder=ASC")).getLaneEntries().get(0).getId();

    /**
     * addMasterBagToContainer
     * containerId, masterbagId
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.BiFunction addMasterBagToContainer = (containerId, masterbagId)-> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_CONTAINER, new String[]{LMS_CONSTANTS.TENANTID,containerId.toString(),"addMasterbag",masterbagId.toString()},
            SERVICE_TYPE.TMS_SVC.toString(), HTTPMethods.POST, null, Headers.getLmsHeaderXML()).getResponseBody(), new ContainerResponse());

    /**
     * shipContainer
     * containerId
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function shipContainer = containerId -> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.SHIP_CONTAINER, new String[]{LMS_CONSTANTS.TENANTID,containerId.toString()},
            SERVICE_TYPE.TMS_SVC.toString(), HTTPMethods.POST, null, Headers.getLmsHeaderXML()).getResponseBody(), new ContainerResponse());

    /**
     * containerInTransitScan
     * containerId, hubId
     */
    @SuppressWarnings("rawtypes")
	public LambdaInterfaces.BiFunction containerInTransitScan = (containerId, hubId)-> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_CONTAINER, new String[]{LMS_CONSTANTS.TENANTID,containerId.toString(),"inTransitScan",hubId.toString()},
            SERVICE_TYPE.TMS_SVC.toString(), HTTPMethods.POST, null, Headers.getLmsHeaderXML()).getResponseBody(), new ContainerResponse());

    /**
     * containerInTransitScan
     * sourceHubCode, destinationHubCode
     */
	@SuppressWarnings("rawtypes")
    public LambdaInterfaces.BiFunction getSupportedLanes = (sourceHubCode, destinationHubCode)-> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_SUPPORTED_LANES, new String[]{LMS_CONSTANTS.TENANTID,sourceHubCode.toString(),destinationHubCode.toString()},
            SERVICE_TYPE.TMS_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new LaneResponse());

    /**
     * getSupportedTransportersForLane
     * laneId
     */
    @SuppressWarnings("rawtypes")
    public LambdaInterfaces.Function getSupportedTransportersForLane = laneId-> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_SUPORTED_TRANSPORTER_FOR_LANE_, new String[]{LMS_CONSTANTS.TENANTID,laneId.toString()},
            SERVICE_TYPE.TMS_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new TransporterResponse());

    /**
     * getTHForLH_Marvel
     * lhCode
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public LambdaInterfaces.Function getTHForLH_Marvel = lhCode -> ((HubToTransportHubConfigResponse)getLocationHubConfigFW.apply(lhCode)).getHubToTransportHubConfigEntries().get(0).getTransportHubCode();

    /**
     * intracityTransferFw
     * Object masterBagId
     */
   
    @SuppressWarnings("unchecked")
	public LambdaInterfaces.Consumer<Object> intracityTransferFw = masterBagId -> {
        TMSMasterbagReponse masterBag = (TMSMasterbagReponse)getTmsMasterBagById.apply(masterBagId);
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        long laneId = ((LaneResponse)getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse)getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        long containerId = ((ContainerResponse)createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        ExceptionHandler.handleEquals(((ContainerResponse)addMasterBagToContainer.apply(containerId,masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse)shipContainer.apply(containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
        String courierCode;
        if(LMS_CONSTANTS.USE_MASTERBAG_SERVICE)
            courierCode = new MasterBagServiceHelper().searchMasterBagById(Long.parseLong(""+masterBagId)).getMasterbagEntries().get(0).getCourier();
        else
            courierCode = ((ShipmentResponse)lmsServiceHelper.getMasterBag.apply(masterBagId)).getEntries().get(0).getCourier();
        if(courierCode.equals("ML")) {
            ExceptionHandler.handleEquals(lmsServiceHelper.masterBagInScan((long) masterBagId, EnumSCM.RECEIVED, "Bangalore",
                    (long) lmsServiceHelper.getDCIdForDCCode.apply(masterBag.getMasterbagEntries().get(0).getDestinationHub()), "HUB").getStatus().getStatusType().toString(),
                    EnumSCM.SUCCESS, "Unable to receive masterBag in DC");
        }else {
            ExceptionHandler.handleEquals(lmsServiceHelper.masterBagInScan((long) masterBagId, EnumSCM.RECEIVED_AT_HANDOVER_CENTER, "Bangalore",
                    (long) lmsServiceHelper.getDCIdForDCCode.apply(masterBag.getMasterbagEntries().get(0).getDestinationHub()), "HUB").getStatus().getStatusType().toString(),
                    EnumSCM.SUCCESS, "Unable to receive masterBag in DC");
        }
    };

    /**
     * intracityTransferFwTillShipped
     * Object masterBagId
     */
    @SuppressWarnings({ "unchecked", "unused" })
	public LambdaInterfaces.Consumer<Object> intracityTransferFwTillShipped = masterBagId -> {
        TMSMasterbagReponse masterBag = (TMSMasterbagReponse)getTmsMasterBagById.apply(masterBagId);
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        long laneId = ((LaneResponse)getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse)getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        long containerId = ((ContainerResponse)createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        ExceptionHandler.handleEquals(((ContainerResponse)addMasterBagToContainer.apply(containerId,masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse)shipContainer.apply(containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
        String courierCode = ((ShipmentResponse)lmsServiceHelper.getMasterBag.apply(masterBagId)).getEntries().get(0).getCourier();
    };

    /**
     * intracityTransferRev
     * Object masterBagId
     */
    @SuppressWarnings("unchecked")
    public LambdaInterfaces.Consumer<Object> intracityTransferRev = masterBagId -> {
        TMSMasterbagReponse masterBag = (TMSMasterbagReponse)getTmsMasterBagById.apply(masterBagId);
        String sourceHub = masterBag.getMasterbagEntries().get(0).getSourceHub();
        String destHub = getTHForLH_Marvel.apply(sourceHub).toString();
        long laneId = ((LaneResponse)getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse)getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        long containerId = ((ContainerResponse)createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        ExceptionHandler.handleEquals(((ContainerResponse)addMasterBagToContainer.apply(containerId,masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse)shipContainer.apply(containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
        ExceptionHandler.handleEquals(((ContainerResponse)containerInTransitScan.apply(containerId, destHub)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to do intransit scan of the container");
        ExceptionHandler.handleEquals(((TMSMasterbagReponse)tmsReceiveMasterBagNew.apply(destHub,masterBagId,containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in dest TH");
    };

    /**
     * intracityTransferToReturnHub
     * For Now we will directly receive the masterbag in Returns hub once its available in the Corresponding Transport Hub. We have not defined config for TH->RT hubs as it is the same in prod.
     */
    @SuppressWarnings("unchecked")
    public LambdaInterfaces.Consumer<Object> intracityTransferToReturnHub = masterBagId -> {
        TMSMasterbagReponse masterBag = (TMSMasterbagReponse)getTmsMasterBagById.apply(masterBagId);
//        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        /*long laneId = ((LaneResponse)getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse)getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        long containerId = ((ContainerResponse)createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        ExceptionHandler.handleEquals(((ContainerResponse)addMasterBagToContainer.apply(containerId,masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse)shipContainer.apply(containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
        ExceptionHandler.handleEquals(((ContainerResponse)containerInTransitScan.apply(containerId, destHub)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to do intransit scan of the container");
        ExceptionHandler.handleEquals(((TMSMasterbagReponse)tmsReceiveMasterBag.apply(destHub,masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in dest TH");
*/        ExceptionHandler.handleEquals(lmsServiceHelper.masterBagInScan((long)masterBagId, EnumSCM.RECEIVED, "Bangalore",
                lmsServiceHelper.getHubByCode(destHub.toString()).getHub().get(0).getId(), "HUB").getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive masterBag in DC");
    };

    /**
     * intercityTransfer
     * Object masterBagId
     */
    @SuppressWarnings("unchecked")
    public LambdaInterfaces.Consumer<Object> intercityTransfer = masterBagId -> {
        TMSMasterbagReponse masterBag = (TMSMasterbagReponse)getTmsMasterBagById.apply(masterBagId);
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = (String) getTHForLH_Marvel.apply(masterBag.getMasterbagEntries().get(0).getDestinationHub());
        long laneId = ((LaneResponse)getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse)getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        long containerId = ((ContainerResponse)createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        ExceptionHandler.handleEquals(((ContainerResponse)addMasterBagToContainer.apply(containerId,masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse)shipContainer.apply(containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
        ExceptionHandler.handleEquals(((ContainerResponse)containerInTransitScan.apply(containerId, destHub)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to do intransit scan of the container");
        ExceptionHandler.handleEquals(((TMSMasterbagReponse)tmsReceiveMasterBagNew.apply(destHub,masterBagId,containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in dest TH");
    };

    /**
     * intercityTransferTillShipped
     * Object masterBagId
     */
    @SuppressWarnings("unchecked")
    public LambdaInterfaces.Consumer<Object> intercityTransferTillShipped = masterBagId -> {
        TMSMasterbagReponse masterBag = (TMSMasterbagReponse)getTmsMasterBagById.apply(masterBagId);
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = (String) getTHForLH_Marvel.apply(masterBag.getMasterbagEntries().get(0).getDestinationHub());
        long laneId = ((LaneResponse)getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse)getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        long containerId = ((ContainerResponse)createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        ExceptionHandler.handleEquals(((ContainerResponse)addMasterBagToContainer.apply(containerId,masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse)shipContainer.apply(containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
    };

    /**
     * intercityTransferWithSourceAndDest
     * masterBagId, sourceHub, destHub
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public LambdaInterfaces.TriConsumer intercityTransferWithSourceAndDest = (masterBagId, sourceHub, destHub) -> {
        long laneId = ((LaneResponse)getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse)getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        long containerId = ((ContainerResponse)createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        ExceptionHandler.handleEquals(((ContainerResponse)addMasterBagToContainer.apply(containerId,masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse)shipContainer.apply(containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
        ExceptionHandler.handleEquals(((ContainerResponse)containerInTransitScan.apply(containerId, destHub)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to do intransit scan of the container");
        ExceptionHandler.handleEquals(((TMSMasterbagReponse)tmsReceiveMasterBagForceNew.apply(destHub,masterBagId,containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in dest TH");
    };

    /**
     * intercityTransferWithSourceAndDestMultiMasterBag
     * @param masterBagIds
     * @param sourceHub
     * @param destHub
     * @throws InterruptedException
     * @throws JAXBException
     * @throws IOException
     * @throws ManagerException
     * @throws JSONException
     * @throws XMLStreamException
     */
    @SuppressWarnings("unchecked")
    public void intercityTransferWithSourceAndDestMultiMasterBag (List<Long> masterBagIds, String sourceHub, String destHub) throws Exception {
        long laneId = ((LaneResponse)getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse)getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        long containerId = ((ContainerResponse)createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();

        for (long masterBagId: masterBagIds) {
            ExceptionHandler.handleEquals(((ContainerResponse) addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container, masterBagId: "+masterBagId);
        }
        ExceptionHandler.handleEquals(((ContainerResponse)shipContainer.apply(containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
        ExceptionHandler.handleEquals(((ContainerResponse)containerInTransitScan.apply(containerId, destHub)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to do intransit scan of the container");
        for (long masterBagId: masterBagIds) {
                ExceptionHandler.handleEquals(((TMSMasterbagReponse)tmsReceiveMasterBagNew.apply(destHub, masterBagId,containerId )).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in dest TH");
        }
    }

    /**
     * intracityTransferWithSourceAndDestMultiMasterBag
     * @param masterBagIds
     * @param sourceHub
     * @param destHub
     * @throws InterruptedException
     * @throws JAXBException
     * @throws IOException
     * @throws ManagerException
     * @throws JSONException
     * @throws XMLStreamException
     */
    @SuppressWarnings("unchecked")
    public void intracityTransferWithSourceAndDestMultiMasterBag (List<Long> masterBagIds, String sourceHub, String destHub) throws InterruptedException, JAXBException, IOException, ManagerException, JSONException, XMLStreamException {
        long laneId = ((LaneResponse)getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse)getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        long containerId = ((ContainerResponse)createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        for (long masterBagId: masterBagIds) {
                ExceptionHandler.handleEquals(((ContainerResponse) addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container, masterBagId: "+masterBagId);
            }
        ExceptionHandler.handleEquals(((ContainerResponse)shipContainer.apply(containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
        for (long masterBagId: masterBagIds) {
                ExceptionHandler.handleEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore",
                        (long) lmsServiceHelper.getDCIdForDCCode.apply(destHub), "HUB").getStatus().getStatusType().toString(),
                        EnumSCM.SUCCESS, "Unable to receive masterBag in DC");
        }
    }

    /**
     * misrouteTransfer
     * Object masterBagId
     */
    @SuppressWarnings("unchecked")
    public LambdaInterfaces.Consumer<Object> misrouteTransfer = masterBagId -> {
        TMSMasterbagReponse masterBag = (TMSMasterbagReponse)getTmsMasterBagById.apply(masterBagId);
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        long laneId = (long)getMisrouteLane.get();
        long transporterId = (long)getMisrouteTransporter.get();
        long containerId = ((ContainerResponse)createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        ExceptionHandler.handleEquals(((ContainerResponse)addMasterBagToContainer.apply(containerId,masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse)shipContainer.apply(containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
        String courierCode = ((ShipmentResponse)lmsServiceHelper.getMasterBag.apply(masterBagId)).getEntries().get(0).getCourier();
        if(courierCode.equals("ML")) {
            ExceptionHandler.handleEquals(lmsServiceHelper.masterBagInScan((long) masterBagId, EnumSCM.RECEIVED, "Bangalore",
                    (long) lmsServiceHelper.getDCIdForDCCode.apply(masterBag.getMasterbagEntries().get(0).getDestinationHub()), "HUB").getStatus().getStatusType().toString(),
                    EnumSCM.SUCCESS, "Unable to receive masterBag in DC");
        }else {
            ExceptionHandler.handleEquals(lmsServiceHelper.masterBagInScan((long) masterBagId, EnumSCM.RECEIVED_AT_HANDOVER_CENTER, "Bangalore",
                    (long) lmsServiceHelper.getDCIdForDCCode.apply(masterBag.getMasterbagEntries().get(0).getDestinationHub()), "HUB").getStatus().getStatusType().toString(),
                    EnumSCM.SUCCESS, "Unable to receive masterBag in DC");
        }
    };

    /**
     * misrouteTransferTillShipped
     * Object masterBagId
     */
    @SuppressWarnings({ "unchecked", "unused" })
    public LambdaInterfaces.Consumer<Object> misrouteTransferTillShipped = masterBagId -> {
        TMSMasterbagReponse masterBag = (TMSMasterbagReponse)getTmsMasterBagById.apply(masterBagId);
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        long laneId = (long)getMisrouteLane.get();
        long transporterId = (long)getMisrouteTransporter.get();
        long containerId = ((ContainerResponse)createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        ExceptionHandler.handleEquals(((ContainerResponse)addMasterBagToContainer.apply(containerId,masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse)shipContainer.apply(containerId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
        String courierCode = ((ShipmentResponse)lmsServiceHelper.getMasterBag.apply(masterBagId)).getEntries().get(0).getCourier();
    };

    /**
     * processInTMSFromClosedToLastMileInTransit
     * Object masterBagId
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public com.myntra.apiTests.common.Constants.LambdaInterfaces.Consumer processInTMSFromClosedToLastMileInTransit = masterBagId -> {
        TMSMasterbagEntry masterBag = ((TMSMasterbagReponse)getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0);
        String sourceTH = (String) getTHForLH_Marvel.apply(masterBag.getSourceHub());
        String destTH = (String) getTHForLH_Marvel.apply(masterBag.getDestinationHub());
        ExceptionHandler.handleEquals(((TMSMasterbagReponse)tmsReceiveMasterBag.apply(sourceTH,masterBagId)).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS,"Unable to receive MasterBag in source TH");
//        try {
            if (sourceTH.equals(destTH))
                intracityTransferFw.accept(masterBagId);
            else {
                intercityTransfer.accept(masterBagId);
                intracityTransferFw.accept(masterBagId);
            }
//        }catch (ManagerException e){
//            misrouteTransfer.accept(masterBagId);
//        }
    };

    /**
     * processInTMSFromClosedToReturnHub
     * Object masterBagId
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public LambdaInterfaces.Consumer processInTMSFromClosedToReturnHub = masterBagId -> {
        TMSMasterbagEntry masterBag = ((TMSMasterbagReponse)getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0);
        String sourceTH = (String) getTHForLH_Marvel.apply(masterBag.getSourceHub());
        String destTH = (String) getTHForLH_Marvel.apply(masterBag.getDestinationHub());
        ExceptionHandler.handleError(((TMSMasterbagReponse)tmsReceiveMasterBag.apply(masterBag.getSourceHub(), masterBagId)).getStatus(),"Unable to receive MasterBag in source TH");
        try {
            if (sourceTH.equals(destTH)) {
                intracityTransferRev.accept(masterBagId);
                intracityTransferToReturnHub.accept(masterBagId);
            }
            else {
                intracityTransferRev.accept(masterBagId);
                intercityTransfer.accept(masterBagId);
                intracityTransferToReturnHub.accept(masterBagId);
            }
        }catch (ManagerException e){
            misrouteTransfer.accept(masterBagId);
        }
    };

    /**
     * processInTMSFromClosedShipped
     * Object masterBagId
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public LambdaInterfaces.Consumer processInTMSFromClosedShipped = masterBagId -> {
        TMSMasterbagEntry masterBag = ((TMSMasterbagReponse)getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0);
        String sourceTH = (String) getTHForLH_Marvel.apply(masterBag.getSourceHub());
        String destTH = (String) getTHForLH_Marvel.apply(masterBag.getSourceHub());
        ExceptionHandler.handleError(((TMSMasterbagReponse)tmsReceiveMasterBag.apply(sourceTH,masterBagId)).getStatus(),"Unable to receive MasterBag in source TH");
        try {
            if (sourceTH.equals(destTH))
                intracityTransferFwTillShipped.accept(masterBagId);
            else {
                intercityTransferTillShipped.accept(masterBagId);
            }
        }catch (ManagerException e){
            misrouteTransferTillShipped.accept(masterBagId);
        }
    };

    /**
     * createNcloseMBforRTO
     * Object masterBagId
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
      public LambdaInterfaces.Function createNcloseMBforRTO = releaseId -> {
        OrderResponse release = (OrderResponse)lmsServiceHelper.getOrderLMS.apply(releaseId);
        ShipmentResponse shipmentResponse = lmsServiceHelper.createMasterBag(release.getOrders().get(0).getDeliveryCenterId(), "DC", "Bangalore", lmsServiceHelper.getHubByCode(release.getOrders().get(0).getRtoHubCode()).getHub().get(0).getId(), "HUB", "Bangalore", EnumSCM.NORMAL);
        ExceptionHandler.handleEquals(shipmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "MasterBag creation failed");
        long masterBagId = shipmentResponse.getEntries().get(0).getId();
        ExceptionHandler.handleEquals(lmsServiceHelper.addAndSaveMasterBag(""+releaseId, "" + masterBagId, ShipmentType.DL), EnumSCM.SUCCESS, "Unable to save Master Bag");
        ExceptionHandler.handleEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close master bag");
        return masterBagId;
    };


    public Long createcloseMBforRTO(Long destPremiseId, String orderId, String DcId) throws IOException, JAXBException, ManagerException {
       ShipmentResponse shipmentResponse = lmsServiceHelper.createMasterBag(Long.valueOf(DcId),"DC","Bangalore", destPremiseId,"HUB", "Bangalore" ,EnumSCM.NORMAL);

        ExceptionHandler.handleEquals(shipmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "MasterBag creation failed");
        long masterBagId = shipmentResponse.getEntries().get(0).getId();
        ExceptionHandler.handleEquals(lmsServiceHelper.addAndSaveMasterBag("" + orderId, "" + masterBagId, ShipmentType.DL), EnumSCM.SUCCESS, "Unable to save Master Bag");
        ExceptionHandler.handleEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close master bag");
        return masterBagId;
    }



    public LambdaInterfaces.Function createNcloseMBforReturn = returnId -> {
       // OrderResponse release = (OrderResponse)lmsServiceHelper.getOrderLMS.apply(returnId);
        Map<String, Object> return_shipment = DBUtilities.exSelectQueryForSingleRecord("select * from return_shipment where source_return_id = " + returnId, "lms");
        LMSHelper lmsHelper= new LMSHelper();
        long masterBagId = 0;
        String  destWarehouseId = return_shipment.get("return_warehouse_id").toString();
        String  deliveryCenterID = return_shipment.get("delivery_center_id").toString();
        String courier = return_shipment.get("courier_code").toString();
        String tracking_number = return_shipment.get("tracking_number").toString();
        String expectedReturnHub = lmsHelper.getReturnHubCodeForWarehouse.apply(Long.parseLong(destWarehouseId)).toString();
        if (!LMS_CONSTANTS.USE_MASTERBAG_SERVICE) {
            ShipmentResponse shipmentResponse = lmsServiceHelper.createMasterBag(Long.parseLong(deliveryCenterID), "DC", "Bangalore", lmsServiceHelper.getHubByCode(expectedReturnHub).getHub().get(0).getId(), "HUB", "Bangalore", EnumSCM.NORMAL);
            ExceptionHandler.handleEquals(shipmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "MasterBag creation failed");
            masterBagId = shipmentResponse.getEntries().get(0).getId();
            ExceptionHandler.handleEquals(lmsServiceHelper.addAndSaveReturnIntoMasterBag("" + returnId, "" + masterBagId, ShipmentType.PU), EnumSCM.SUCCESS, "Unable to save Master Bag");
            ExceptionHandler.handleEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close master bag");
        }else{
            MasterbagDomain createMasterBagResponse = null;
            MasterbagUpdateResponse closeMBResponse;
            try {
                createMasterBagResponse = masterBagServiceHelper.createMasterBag(Long.parseLong(deliveryCenterID), lmsServiceHelper.getHubByCode(expectedReturnHub).getHub().get(0).getId(), ShippingMethod.NORMAL, courier);
                Assert.assertNotNull(createMasterBagResponse.getId(), "Unable to create MaterBag");
                masterBagId = createMasterBagResponse.getId();
                ExceptionHandler.handleEquals(masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, tracking_number, ShipmentType.PU, LMS_CONSTANTS.TENANTID).
                        getStatus().getStatusType().toString(), "SUCCESS", "Add shipment to MasterBag Failed");
                closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
                masterBagValidator.validateCloseMasterBag(closeMBResponse, masterBagId);
            }catch (Exception e){
                e.printStackTrace();
                Assert.fail(e.getMessage());
            }
        }
        return masterBagId;

    };

    /**
     * pushMockTMSMasterBag
     * @param source
     * @param destination
     * @param noOfShipments
     * @return
     * @throws JAXBException
     * @throws IOException
     * @throws ManagerException
     */
    public String pushMockTMSMasterBag(String source, String destination, int noOfShipments) throws JAXBException, IOException, ManagerException {
        TMSMasterbagEntry tmsMasterbagEntry = new TMSMasterbagEntry();
        List<TMSMasterbagShipmentEntry> masterbagShipmentEntries = new ArrayList<>();
        String masterBagId = "98"+LMSUtils.randomGenn(8);
        for (int i=0;i<noOfShipments;i++){
            TMSMasterbagShipmentEntry masterbagShipmentEntry = new TMSMasterbagShipmentEntry();
            masterbagShipmentEntry.setSecondaryMobileNumber("1234567890");
            masterbagShipmentEntry.setCustomerName("lmsautomation");
            masterbagShipmentEntry.setTmsMasterbagId(Long.parseLong(masterBagId));
            masterbagShipmentEntry.setConsignorAddress("Delhi");
            masterbagShipmentEntry.setConsignorName("lms automation");
            masterbagShipmentEntry.setWeight(123.00);
            masterbagShipmentEntry.setShipmentAttribute("Large");
            masterbagShipmentEntry.setStatus(TMSMasterbagStatus.NEW);
            masterbagShipmentEntry.setCodAmount(1099.00);
            masterbagShipmentEntry.setVat(159.00);
            masterbagShipmentEntry.setState("KA");
            masterbagShipmentEntry.setConsignorTin("1234567890");
            masterbagShipmentEntry.setPrimaryMobileNumber("1234567890");
            masterbagShipmentEntry.setCity("Bangalore");
            masterbagShipmentEntry.setCountry("INDIA");
            masterbagShipmentEntry.setPincode("560068");
            masterbagShipmentEntry.setShipmentValue(1599.00);
            masterbagShipmentEntry.setTrackingNumber("ML"+LMSUtils.randomGenn(10));
            masterbagShipmentEntry.setEmail("lmsautomation@myntra.com");
            masterbagShipmentEntry.setCustomerAddress("lmsautomation test address");
            masterbagShipmentEntry.setItemDetails("test item");
            masterbagShipmentEntry.setOrderId("987"+LMSUtils.randomGenn(8));
            if(LMS_CONSTANTS.USE_VERSION_V2)
            masterbagShipmentEntry.setTenantId(LMS_CONSTANTS.TENANTID);
            masterbagShipmentEntries.add(masterbagShipmentEntry);
        }
        if(LMS_CONSTANTS.USE_VERSION_V2)
        tmsMasterbagEntry.setTenantId(LMS_CONSTANTS.TENANTID);
        tmsMasterbagEntry.setLastScannedOn(new Date());
        tmsMasterbagEntry.setDestinationHub(destination);
        tmsMasterbagEntry.setSourceHub(source);
        tmsMasterbagEntry.setStatus(TMSMasterbagStatus.NEW);
        tmsMasterbagEntry.setLastScannedCity("Bangalore");
        tmsMasterbagEntry.setMasterbagId(masterBagId);
        tmsMasterbagEntry.setLastScannedHub(source);
        tmsMasterbagEntry.setMasterbagShipmentEntries(masterbagShipmentEntries);
        String payload = APIUtilities.convertXMLObjectToString(tmsMasterbagEntry);
        ExceptionHandler.handleEquals(((TMSMasterbagReponse)APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_CREATE_UPDATE, null,
                SERVICE_TYPE.TMS_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderXML()).getResponseBody(), new TMSMasterbagReponse())).getStatus().getStatusType().toString(),EnumSCM.SUCCESS);
         return masterBagId;
    }
    public TmsPreAlertResponse getContainerPreAlert(String sourceHub,String destinationHub, String transporter, String fromDate,String toDate){
        String pathParam=""+LMS_CONSTANTS.TENANTID+"/"+sourceHub+"/"+destinationHub+"/"+transporter+"?startDate="+fromDate+"&endDate="+toDate+"";
        TmsPreAlertResponse tmsPreAlertResponse=null;
        try{
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.B2B_CONTAINER_PRE_ALERT,
                    new String[]{pathParam }, SERVICE_TYPE.TMS_SVC.toString(),
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tmsPreAlertResponse= (TmsPreAlertResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                    new TmsPreAlertResponse());
        }catch (IOException | JAXBException e){
            log.info("ERROR:: While getting the Container Pre-Alert");
            Assert.fail("FAILED:: While getting the Container Pre-Alert");

        }
        return tmsPreAlertResponse;
    }
    public TmsPreAlertResponse getMasterbagPreAlert(String currentHub,String sourceHub,String destinationHub, String transporter, String fromDate,String toDate){
        String pathParam=LMS_CONSTANTS.TENANTID+"/"+currentHub+"/"+sourceHub+"/"+destinationHub+"/"+transporter+"?startDate="+fromDate+"&endDate="+toDate+"";
        TmsPreAlertResponse tmsPreAlertResponse=null;
        try{
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.MASTERBAG_PREALERT,
                    new String[]{pathParam }, SERVICE_TYPE.TMS_SVC.toString(),
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            tmsPreAlertResponse= (TmsPreAlertResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                    new TmsPreAlertResponse());
        }catch (IOException | JAXBException e){
            log.info("ERROR:: While getting the Masterbag Pre-Alert");
            Assert.fail("FAILED:: While getting the Masterbag Pre-Alert");
        }
        return tmsPreAlertResponse;
    }
    public ShipmentResponse autoMasterBag(String trackingNumber){
        ShipmentResponse shipmentResponse=null;
        String pathParam=""+trackingNumber+"/"+LMS_CONSTANTS.TENANTID;
        try{
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.AUTO_MASTERBAGGING,
                    new String[]{pathParam }, SERVICE_TYPE.LMS_SVC.toString(),
                    HTTPMethods.POST, null, Headers.getLmsHeaderXML());
            shipmentResponse= (ShipmentResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                    new ShipmentResponse());
        }catch (IOException | JAXBException e){
            log.info("ERROR:: While auto Masterbagging.");
            Assert.fail("FAILED:: While auto Masterbagging.");

        }
        return shipmentResponse;
    }
    public ShipmentResponse deliverMasterBag(ShipmentResponse shipmentResponse){
        ShipmentEntry shipment=new ShipmentEntry();
        shipment.setId(shipmentResponse.getEntries().get(0).getId());
        shipment.setStatus(ShipmentStatus.RECEIVED);
        shipment.setLastScannedCity("Delhi Returns Hub");
        shipment.setLastScannedPremisesId(21l);
        shipment.setLastScannedPremisesType(shipmentResponse.getEntries().get(0).getDestinationPremisesType());
        shipment.setArrivedOn(shipmentResponse.getEntries().get(0).getShippedOn());
        shipment.setLastScannedOn(shipmentResponse.getEntries().get(0).getLastScannedOn());



        ShipmentResponse shipmentResponse1=null;
        try {
            String payload = APIUtilities.getObjectToJSON(shipment);
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.DELIVERY_MASTER_BAG,
                    new String[]{shipmentResponse.getEntries().get(0).getId().toString()}, SERVICE_TYPE.LMS_SVC.toString(),
                    HTTPMethods.PUT, payload, Headers.getLmsHeaderJSON());
            shipmentResponse1= (ShipmentResponse) APIUtilities.getJsontoObject(service.getResponseBody(),
                    new ShipmentResponse());
        }catch (IOException  e){
            log.info("ERROR:: Delivering Masterbag failed");
            Assert.fail("FAILED:: While Marking Masterbag DELIVERED:- ",e.getCause());
        }
        return shipmentResponse1;
    }
    public void getBulkOrderTrackingReport(String level,String sourcePath,String shipmentType,Date startDateTimeStamp,Date endDateTimeStamp){

        /*OrderTrackingReportResponse  orderTrackingReportResponse=new OrderTrackingReportResponse();
        try {
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.BULK_ORDER_TRACKING_REPORT,
                    new String[]{}, SERVICE_TYPE.LMS_SVC.toString(),
                    HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
            orderTrackingReportResponse= (OrderTrackingReportResponse) APIUtilities.getJsontoObjectUsingFasterXML(service.getResponseBody(),
                    new ShipmentResponse());
        }catch (IOException e){
            log.info("ERROR:: Fetching Bulk Order Tracking");
            Assert.fail("FAILED :: While Fetching Bulk order Tracking Report.");
        }*/

    }
    public void b2bShippingLabe(String trackingNumber){
        String param="?trackingNumber="+trackingNumber+"&tenantId="+LMS_CONSTANTS.TENANTID+"";
        try {
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.B2B_SHIPPING_LABEL,
                    new String[]{param}, SERVICE_TYPE.LMS_SVC.toString(),
                    HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        }catch (UnsupportedEncodingException e){
            log.info("ERROR:: While getting the B2B shipping Label");
            Assert.fail("FAILED:: B2B shipping Label Creation Failed:- ",e.getCause());
        }
    }
    public Svc createShipment(String clientId,String trackingNumber,String warehouseId, String rtoWarehouseId,String integrationId,String sourcePath, String pincode, ShippingMethod shippingMethod, String courierCode, int noOfItem, boolean isCod, String destinationPremisesId, String shipmentType){

        Svc service=null;
        try {
            ShipmentB2B shipment=new ShipmentB2B();
            List<ShipmentItem> shipmentItems = new ArrayList();
            ShipmentItem shipmentItem = new ShipmentItem();
            shipment.setSourceReferenceId("B2B"+LMSUtils.randomGenn(10));
            shipment.setTrackingNumber(trackingNumber);
            shipment.setCourierCode(courierCode);
            shipment.setShipmentType(shipmentType);
            shipment.setRecipientName("LMS automation");
            shipment.setRecipientAddress("LMS test address");
            shipment.setLocality("Kudlu gate");
            shipment.setLandmark("Kudlu gate");
            shipment.setStateCode("KA");
            shipment.setCountry("India");
            shipment.setPincode(pincode);
            shipment.setRecipientContactNumber("1234567890");
            shipment.setAlternateContactNumber("1234567890");
            shipment.setEmail("lmsautomation@gmail.com");
            shipment.setContentsDescription("Puma Shoes");
            shipment.setStoreId(5L);
            shipment.setWarehouseId(warehouseId);
            shipment.setRtoWarehouseId(rtoWarehouseId);
            shipment.setDestinationPremisesId(destinationPremisesId);
            shipment.setCod(isCod);
            shipment.setPackedDate(DateTime.now());
            shipment.setShippingMethod(shippingMethod);
            shipment.setPackageWeight((float)213);
            shipment.setPackageLength(214.00);
            shipment.setPackageBreadth(123.00);
            shipment.setPackageHeight(314.00);
            shipment.setSourcePath(sourcePath);
            shipment.setIntegrationId(integrationId);
            shipment.setClientId(clientId);
            shipment.setTenantId(LMS_CONSTANTS.TENANTID);
            while (noOfItem!=0) {
                Float itemMRP = 400 + new Random().nextFloat() * (2000 - 400);
                shipmentItem.setSourceItemReferenceId("43" + LMSUtils.randomGenn(8));
                shipmentItem.setStyleId("99" + LMSUtils.randomGenn(4));
                shipmentItem.setSkuId("77" + LMSUtils.randomGenn(5));
                shipmentItem.setItemDescription("test skuId for B2B");
                shipmentItem.setImageURL("http://logos-download.com/wp-content/uploads/2016/05/Jabong_logo_logotype.png");
                shipmentItem.setItemBarcode("11" + LMSUtils.randomGenn(8));
                shipmentItem.setItemMRP(itemMRP);
                shipmentItem.setItemValue(itemMRP-100);
                shipmentItem.setCodAmount((float) 0);
                shipmentItem.setIntegratedGoodsAndServiceTax((float) 0);
                shipmentItem.setCentralGoodsAndServiceTax((float) 0);
                shipmentItem.setStateGoodsAndServiceTax((float) 0);
                shipmentItem.setTaxAmountPaid(50 + new Random().nextDouble() * (200 - 50));
                shipmentItem.setAdditionalCharges((float) 0);
                shipmentItem.setSellerName("B2B seller 1");
                shipmentItem.setSellerAddress("B2B test address");
                shipmentItem.setSellerId("123");
                shipmentItem.setSellerTaxIdentificationNumber("123459876");
                shipmentItem.setSellerCentralSalesTaxNumber("1234598768");
                shipmentItem.setGoodsAndServiceTaxIdentificationNumber("1234598769");
                shipmentItem.setInvoiceId("INVID" + LMSUtils.randomGenn(7));
                shipmentItem.setElectronicReferenceNumber("987654321");
                shipmentItems.add(shipmentItem);
                noOfItem--;
            }
            shipment.setShipmentItems(shipmentItems);
            String payload = APIUtilities.getObjectToJSON(shipment);
             service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.B2B_CREATE_SHIPMENT, null, SERVICE_TYPE.LMS_SVC.toString(),
                    HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());

        }catch (IOException  e){
            log.error("ERROR:: B2B Shipment Creation Failed");
            Assert.fail("FAILURE:: B2B Shipment Creation Failed",e.getCause());
        }

        return service;

    }
    public Svc shipmentUpload(String sourceReferenceId,String warehouseId, String rtoWarehouseId,String integrationId,String sourcePath, String pincode, ShippingMethod shippingMethod, String courierCode, int noOfItem, boolean isCod, String destinationPremisesId, String shipmentType) {
        Svc service =null;
        try {
            ShipmentB2B shipment=new ShipmentB2B();
            List<ShipmentItem> shipmentItems = new ArrayList();
            ShipmentItem shipmentItem = new ShipmentItem();
            shipment.setSourceReferenceId(sourceReferenceId);
            shipment.setCourierCode(courierCode);
            shipment.setShipmentType(shipmentType);
            shipment.setReceivedBy("LMS automation");
            shipment.setReceivedAt("LMS test address");
            shipment.setLocality("Kudlu gate");
            shipment.setLandmark("Kudlu gate");
            shipment.setStateCode("KA");
            shipment.setCountry("India");
            shipment.setPincode(pincode);
            shipment.setRecipientContactNumber("1234567890");
            shipment.setAlternateContactNumber("1234567890");
            shipment.setEmail("lmsautomation@gmail.com");
            shipment.setContentsDescription("Puma Shoes");
            shipment.setStoreId(5L);
            shipment.setWarehouseId(warehouseId);
            shipment.setRtoWarehouseId(rtoWarehouseId);
            shipment.setDestinationPremisesId(destinationPremisesId);
            shipment.setCod(isCod);
            shipment.setPackedDate(DateTime.now());
            shipment.setShippingMethod(shippingMethod);
            shipment.setPackageWeight((float) 213);
            shipment.setPackageLength(214.00);
            shipment.setPackageBreadth(123.00);
            shipment.setPackageHeight(314.00);
            shipment.setSourcePath(sourcePath);
            shipment.setIntegrationId(integrationId);
            shipment.setClientId(LMS_CONSTANTS.B2B_CLIENTID);
            shipment.setTenantId(LMS_CONSTANTS.TENANTID);
            while (noOfItem != 0) {
                Float itemMRP = 400 + new Random().nextFloat() * (2000 - 400);
                shipmentItem.setSourceItemReferenceId("43" + LMSUtils.randomGenn(8));
                shipmentItem.setStyleId("99" + LMSUtils.randomGenn(4));
                shipmentItem.setSkuId("77" + LMSUtils.randomGenn(5));
                shipmentItem.setItemDescription("test skuId for B2B");
                shipmentItem.setImageURL("http://logos-download.com/wp-content/uploads/2016/05/Jabong_logo_logotype.png");
                shipmentItem.setItemBarcode("11" + LMSUtils.randomGenn(8));
                shipmentItem.setItemMRP(itemMRP);
                shipmentItem.setItemValue(itemMRP - 100);
                shipmentItem.setCodAmount((float) 0);
                shipmentItem.setIntegratedGoodsAndServiceTax((float) 0);
                shipmentItem.setCentralGoodsAndServiceTax((float) 0);
                shipmentItem.setStateGoodsAndServiceTax((float) 0);
                shipmentItem.setTaxAmountPaid(50 + new Random().nextDouble() * (200 - 50));
                shipmentItem.setAdditionalCharges((float) 0);
                shipmentItem.setSellerName("B2B seller 1");
                shipmentItem.setSellerAddress("B2B test address");
                shipmentItem.setSellerId("123");
                shipmentItem.setSellerTaxIdentificationNumber("123459876");
                shipmentItem.setSellerCentralSalesTaxNumber("1234598768");
                shipmentItem.setGoodsAndServiceTaxIdentificationNumber("1234598769");
                shipmentItem.setInvoiceId("INVID" + LMSUtils.randomGenn(7));
                shipmentItem.setElectronicReferenceNumber("987654321");
                shipmentItems.add(shipmentItem);
                noOfItem--;
            }
            shipment.setShipmentItems(shipmentItems);
            String payload = APIUtilities.getObjectToJSON(shipment);
            service= HttpExecutorService.executeHttpService(Constants.LMS_PATH.B2B_CREATE_SHIPMENT, null, SERVICE_TYPE.LMS_SVC.toString(),
                    HTTPMethods.POST, payload, Headers.getLmsHeaderJSON());
        }catch (IOException e){
            log.error("ERROR:: While creating B2B Shipments through Upload");
            Assert.fail("FAILED:: While creating B2B Shipment through Upload");

        }
        return service;
    }
    public Svc shipmentReportB2B(String sourceHub,String destinationHub,String shipmentStatus,String client,String sourcePath,String fromDate, String endDate){

        Svc service=null;
        try {
            String param = "?sourceHub="+sourceHub+"&destinationHub="+destinationHub+"&status="+shipmentStatus+"&shipmentType=B2B&sourcePath="+sourcePath+"&tenantId="+LMS_CONSTANTS.TENANTID+"&clientId="+client+"&startDate="+fromDate+"&endDate="+endDate+"";
           service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.SHIPMENT_REPORT, new String[]{param}, SERVICE_TYPE.LMS_SVC.toString(),
                    HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        }catch (UnsupportedEncodingException e){
            log.error("ERROR:: Unable to Fetch shipment Report");
            Assert.fail("Failed:: While getting Shipment Report");
        }
        return  service;
    }

    /*
     *receiveContainerInTransportHub
     * @param containerId
     * @param hubId
     * @author bairagi muduli
     */
    public LambdaInterfaces.BiFunction receiveContainerInTransportHub = (containerId, hubId)-> APIUtilities.convertXMLStringToObject(HttpExecutorService.executeHttpService(Constants.LMS_PATH.TMS_CONTAINER, new String[]{containerId.toString(),"dropOffHubCode",hubId.toString(),"tenant",LMS_CONSTANTS.TENANTID},
            SERVICE_TYPE.TMS_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderXML()).getResponseBody(), new ContainerResponse());

    /*
     *masterBagInscanInWHReturnProcess
     * @param trackingNumber
     * @param masterBagId
     * @param shipmentType
     * @param orderShipmentAssociationStatus
     * @param shipmentStatus
     * @author bairagi muduli
     */
    public String masterBagInscanInWHReturnProcess(String trackingNumber, long masterBagId, ShipmentType shipmentType, OrderShipmentAssociationStatus orderShipmentAssociationStatus,
                                                   ShipmentStatus shipmentStatus, PremisesType premisesType, Long lastScannedPremisesId) throws NumberFormatException, JAXBException, UnsupportedEncodingException {
        // Order Scan After Shipment At DC

        OrderShipmentAssociationEntry orderShipmentAssociationEntries = new OrderShipmentAssociationEntry();
        orderShipmentAssociationEntries.setTrackingNumber(trackingNumber);
        orderShipmentAssociationEntries.setShipmentType(shipmentType);
        orderShipmentAssociationEntries.setStatus(orderShipmentAssociationStatus);

        List<OrderShipmentAssociationEntry> orderShipmentAssociation = new ArrayList<>();
        orderShipmentAssociation.add(orderShipmentAssociationEntries);

        ShipmentEntry shipmentEntryPayload = new ShipmentEntry();
        shipmentEntryPayload.setId(masterBagId);
        shipmentEntryPayload.setStatus(shipmentStatus);
        shipmentEntryPayload.setLastScannedCity("Bangalore returns hub-HUB");
        shipmentEntryPayload.setLastScannedPremisesId(lastScannedPremisesId);
        shipmentEntryPayload.setLastScannedPremisesType(premisesType);
        shipmentEntryPayload.setArrivedOn(new Date());
        shipmentEntryPayload.setLastScannedOn(new Date());
        shipmentEntryPayload.setOrderShipmentAssociationEntries(orderShipmentAssociation);

        String payload = APIUtilities.convertXMLObjectToString(shipmentEntryPayload);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.RECEIVE_SHIPMENT_IN_DC, new String[]{String.valueOf(masterBagId)} , SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        return service.getResponseBody();
    }

}
