package com.myntra.apiTests.erpservices.lms.Helper;
import org.apache.log4j.Logger;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class OrderUpdateTabHelper {
    static Logger log = Logger.getLogger(LMS_ReturnHelper.class);

    /**
     * @param tenantId
     * @param trackingNumber
     * @param activityType
     * @param location
     * @param remarks
     * @return
     * @throws IOException
     * @author : Shanmugam Yuvaraj
     * @description : used to generate Order Update csv file and it returns path of the csv file
     */

    public String generateOrderUpdateCSV(Long tenantId, ArrayList<String> trackingNumber, ArrayList<String> activityType, String location, ArrayList<String> remarks) throws IOException {

        String time = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());

        String uploadFileHeader = "Tenant Id,Tracking Number,Activity Type,Location,Action Date (dd-MM-yyyy HH:mm:ss),Remark";

        File file = new File("./Data/lms/csv/OrderUpdate.csv");
        if (file.exists()) {
            file.delete();
            file = new File("./Data/lms/csv/OrderUpdate.csv");
        }
        FileWriter writer = new FileWriter(file, true);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(uploadFileHeader);
        bufferedWriter.newLine();
        for (int i = 0; i <= trackingNumber.size() - 1; i++) {
            bufferedWriter.write("\"" + tenantId + "\",\"" + trackingNumber.get(i) + "\",\"" + activityType.get(i) + "\",\"" + location + "\",\"" + time + "\",\"" + "Test " + remarks.get(i) + "\"");
            bufferedWriter.newLine();
        }

        bufferedWriter.close();
        log.info("Order update Upload file - " + file.getName() + " is created in Folder :" + file.getPath());
        return file.getPath();
    }
}
