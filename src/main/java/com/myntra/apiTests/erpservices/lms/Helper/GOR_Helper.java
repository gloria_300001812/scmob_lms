package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.lmsClient.Client_data;
import com.myntra.apiTests.erpservices.lms.lmsClient.ConsolidationShipment;
import com.myntra.lms.client.response.PlatformMLSharedEntry;
import com.myntra.logistics.platform.domain.Premise;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.joda.time.DateTime;
import org.testng.Assert;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class GOR_Helper {

    private static HashMap<String, String> getMultipartTypeHeader() {
        HashMap<String, String> createOrderHeaders = new HashMap<String, String>();
        createOrderHeaders.put("Authorization", "Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==");
        createOrderHeaders.put("Content-Type", "multipart/form-data");
        createOrderHeaders.put("Accept", "multipart/form-data");
        return createOrderHeaders;
    }

    public int OrderInscan(String premiseId, Premise.PremiseType premisesType, String packetId) throws Exception {
        //premiseId = hubCode
        PlatformMLSharedEntry platformMLSharedEntry = new PlatformMLSharedEntry();
        platformMLSharedEntry.setEventLocation("bangalore White field Warehouse");
        platformMLSharedEntry.setShipmentId(packetId);
        platformMLSharedEntry.setRemarks("GOR Inscan");
        platformMLSharedEntry.setEvent("INSCAN");
        platformMLSharedEntry.setEventTime(new DateTime());
        Premise premise = new Premise();
        premise.setPremisesType(premisesType);
        premise.setPremiseId(premiseId);
        platformMLSharedEntry.setEventLocationPremise(premise);
        String payload = APIUtilities.getObjectToJSON(platformMLSharedEntry);
        Svc service = HttpExecutorService.executeHttpService(Constants.GOR_PATH.ORDER_INSCAN, new String[]{},
                SERVICE_TYPE.PARTNER_API.toString(), HTTPMethods.POST, payload, MDA_Helper.getPartnerAPIHeadersJSON(true));
        return service.getResponseStatus();
    }

    public int createAndGetConsolodationBagID(String hubCode, Integer noOfBags) throws TikaException, SAXException, IOException {
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpGet httpget = null;
        String specialSpace = " ";
        String pathParam = String.format("?hubCode=%s&noOfBags=%s",hubCode, noOfBags);
        Svc service = HttpExecutorService.executeHttpService(Constants.GOR_PATH.CREATE_CONSOLIDATIONBAG, new String[] {pathParam}, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.GET, null, getMultipartTypeHeader());
        httpget = new HttpGet("http://"+service.getIp()+service.getPath());
        httpget.addHeader("Authorization", "Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==");
        httpget.addHeader("Content-Type", "multipart/form-data");
        HttpResponse response = httpclient.execute(httpget);
        HttpEntity resEntity = response.getEntity();
        InputStream is = resEntity.getContent();
        ParseContext pcontext = new ParseContext();

        //parsing the document using PDF parser
        PDFParser pdfparser = new PDFParser();
        BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        pdfparser.parse(is, handler, metadata,pcontext);

        String pdfData=handler.toString().replaceAll(specialSpace, " ");
        System.out.println(pdfData);
        String pattern = "Bag ID: [\\d]+";
        Matcher matcher = Pattern.compile(pattern).matcher(pdfData);
        if (matcher.find())
            return Integer.parseInt(matcher.group(0).replace("Bag ID: ","").trim());
        else
            Assert.fail("Creation of ConsolodationBag Failed - lable does not have any digits");
            return 0;
    }

    public int addShipmentToConsolidationBagAndClose(Integer consolidationBagId, String packetID,  String destinationCode, String shippingMethod, String courierCode) throws Exception {
        List<ConsolidationShipment> shipments = new ArrayList<>();
        Client_data client_data = new Client_data();
        ConsolidationShipment shipment = new ConsolidationShipment();
        com.myntra.apiTests.erpservices.lms.lmsClient.ConsolidationBagShipmentResponse cb = new com.myntra.apiTests.erpservices.lms.lmsClient.ConsolidationBagShipmentResponse();
        shipment.setBreadth("26.1");
        shipment.setHeight("6.5");
        shipment.setLength("34.8");
        shipment.setWeight("445.0");
        shipment.setAwb(""+packetID);
        shipments.add(shipment);
        cb.setShipments(shipments);
        client_data.setDestinationCode(destinationCode);
        client_data.setShippingMethod(shippingMethod);
        client_data.setCourierCode(courierCode);
        client_data.setIsFootwear("false");
        cb.setClient_data(client_data);
        cb.setBagseal(""+consolidationBagId);
        cb.setShipment_count(1);
        cb.setArm_id("6");
        cb.setPptl_id("1001");
        cb.setTimestamp(DateTime.now());
        String payload = APIUtilities.getObjectToJSON(cb);
        Svc service = HttpExecutorService.executeHttpService(Constants.GOR_PATH.ADD_SHIPMENT_TO_CONSOLIDATIONBAG_AND_CLOSE, new String[]{},
                SERVICE_TYPE.PARTNER_API.toString(), HTTPMethods.POST, payload, MDA_Helper.getPartnerAPIHeadersJSON(true));
        Thread.sleep(10000); //introducing delay here for create MB sync
        return service.getResponseStatus();
    }


    public int addShipmentsToConsolidationBagAndClose(Integer consolidationBagId, List<String> packetIds,  String destinationCode, String shippingMethod, String courierCode) throws Exception {
        List<ConsolidationShipment> shipments = new ArrayList<>();
        Client_data client_data = new Client_data();
        com.myntra.apiTests.erpservices.lms.lmsClient.ConsolidationBagShipmentResponse cb = new com.myntra.apiTests.erpservices.lms.lmsClient.ConsolidationBagShipmentResponse();
        for(String packetId : packetIds) {
            ConsolidationShipment shipment = new ConsolidationShipment();
            shipment.setBreadth("26.1");
            shipment.setHeight("6.5");
            shipment.setLength("34.8");
            shipment.setWeight("445.0");
            shipment.setAwb("" + packetId);
            shipments.add(shipment);
        }
        cb.setShipments(shipments);
        client_data.setDestinationCode(destinationCode);
        client_data.setShippingMethod(shippingMethod);
        client_data.setCourierCode(courierCode);
        client_data.setIsFootwear("false");
        cb.setClient_data(client_data);
        cb.setBagseal(""+consolidationBagId);
        cb.setShipment_count(1);
        cb.setArm_id("6");
        cb.setPptl_id("1001");
        cb.setTimestamp(DateTime.now());
        String payload = APIUtilities.getObjectToJSON(cb);
        Svc service = HttpExecutorService.executeHttpService(Constants.GOR_PATH.ADD_SHIPMENT_TO_CONSOLIDATIONBAG_AND_CLOSE, new String[]{},
                SERVICE_TYPE.PARTNER_API.toString(), HTTPMethods.POST, payload, MDA_Helper.getPartnerAPIHeadersJSON(true));
        Thread.sleep(10000); //introducing delay here for create MB sync
        return service.getResponseStatus();
    }
}
