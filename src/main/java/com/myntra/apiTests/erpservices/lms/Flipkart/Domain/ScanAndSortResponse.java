package com.myntra.apiTests.erpservices.lms.Flipkart.Domain;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.sortation.domain.SortationConfig;

import java.util.List;

public class ScanAndSortResponse  extends AbstractResponse {

    private List<SortationConfig> sortationConfigList;
    private String sellerId;

    public ScanAndSortResponse() {
    }

    public List<SortationConfig> getSortationConfigList() {
        return this.sortationConfigList;
    }

    public void setSortationConfigList(List<SortationConfig> sortationConfigList) {
        this.sortationConfigList = sortationConfigList;
    }

    public String getSellerId ()
    {
        return sellerId;
    }

    public void setSellerId (String sellerId)
    {
        this.sellerId = sellerId;
    }
}
