package com.myntra.apiTests.erpservices.lms.Helper;

import com.fasterxml.jackson.core.JsonParseException;
import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.validators.ReturnValidator;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.domain.response.ReturnResponse;
import com.myntra.lms.client.domain.shipment.ReturnShipment;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.OrderStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.codehaus.jackson.map.JsonMappingException;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.util.Calendar;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper.getTokenForFileUpload;

@Slf4j
public class LMS_OpenBoxReturnHelper {
    String env=getEnvironment();
    LmsServiceHelper lmsServiceHelper=new LmsServiceHelper();
    LMSHelper lmsHelper=new LMSHelper();
    RMSServiceHelper rmsServiceHelper=new RMSServiceHelper();
    TMSServiceHelper tmsServiceHelper=new TMSServiceHelper();
    ReturnValidator returnValidator=new ReturnValidator();
    StatusPollingValidator statusPollingValidator=new StatusPollingValidator();

    ReturnHelper returnHelper=new ReturnHelper();

    /**
     * processOpenBoxReturn : processes  the OpenBox return return till Shipping back to config source warehouse and
     * receiving in WH. SO it performs the complete action
     *
     * @param returnId/in case of selfShip its orderId
     * @param toStatus
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws NumberFormatException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void processOpenBoxReturn(String returnId,String toStatus)
            throws Exception {
        LMS_ReturnHelper lms_returnHelper=new LMS_ReturnHelper();

        String destWarehouseId=null;
        String deliveryCenterID=null;
        String tripOrderAssignmentId=null;

        ExceptionHandler.handleTrue(
                rmsServiceHelper.getReturnDetailsNew(String.valueOf(Long.parseLong(returnId))).getData().get(0)
                        .getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP),"");

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(""+returnId);

        lms_returnHelper.manifestOpenBoxPickups(returnId);

        Thread.sleep(Constants.LMS_PATH.sleepTime);
        String trackingNo=null;
        OrderResponse returnShipmentLMSObject=lms_returnHelper.getReturnShipmentDetailsLMS(returnId);

        if (returnShipmentLMSObject.getOrders() != null && !returnShipmentLMSObject.getOrders().isEmpty()) {
            trackingNo=returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        } else
            log.info(
                    "Failed here : returnShipmentLMSObject.getOrders().get(0).getTrackingNumber(): "+returnShipmentLMSObject
                            .getOrders().get(0).getTrackingNumber());

        final ReturnResponse returnShipmentDetails=returnHelper.getReturnShipmentDetails(returnId,
                LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
        final String courierCode=returnShipmentDetails.getDomainReturnShipments().get(0).getCourierCode();

        if (courierCode.equals("ML"))
            processOpenBoxReturnInML(returnId,toStatus,trackingNo,destWarehouseId,deliveryCenterID,
                    tripOrderAssignmentId,returnShipmentDetails);
        else if (courierCode.equals("EK")) {

        } else if (courierCode.equals("DE")) {

        }

        log.info("Your order processing has been completed successfully ReturnId: "+returnId);
    }

    /**
     * processOpenBoxReturnInML
     *
     * @param returnId
     * @param toStatus
     * @param trackingNo
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void processOpenBoxReturnInML(String returnId,String toStatus,String trackingNo,String destWarehouseId,String
            deliveryCenterID,String tripOrderAssignmentId,ReturnResponse returnShipmentDetails)
            throws Exception {
        Long tripId=null;
        final ReturnShipment returnShipment=returnShipmentDetails.getDomainReturnShipments().get(0);
        if (!toStatus.equals(EnumSCM.SELF_SHIP)) {
            trackingNo      =returnShipment.getTrackingNumber();
            destWarehouseId =returnShipment.getReturnFacilityId();
            deliveryCenterID=String.valueOf(returnShipment.getDeliveryCenterId());
            final TripOrderAssignmentResponse tripOrderAssignmentResponse=
                    lmsServiceHelper.processReturnTillTripCreation(trackingNo,deliveryCenterID);
            tripOrderAssignmentId=String.valueOf(tripOrderAssignmentResponse.getTripOrders().get(0).getId());
            tripId               =tripOrderAssignmentResponse.getTripOrders().get(0).getTripId();
        }
        switch (toStatus) {
            case EnumSCM.PICKUP_SUCCESSFUL:
                markTripPickupSuccessful(returnId,trackingNo,destWarehouseId,deliveryCenterID,tripOrderAssignmentId,
                        tripId);
                break;
            case EnumSCM.PQCP_APPROVED_After_trip_close:
                markTripPickupSuccessfulQCPendingAfterTripClose(returnId,trackingNo,destWarehouseId,deliveryCenterID,
                        tripOrderAssignmentId,tripId);
                break;

            case EnumSCM.PQCP_APPROVED_Before_trip_close:
                markTripPickupSuccessfulQCPendingBeforeTripClose(returnId,trackingNo,destWarehouseId,
                        deliveryCenterID,tripOrderAssignmentId,tripId);
                break;

            case EnumSCM.PQCP_ONHOLD_APPROVED_After_trip_close:
                markTripPickupSuccessfulQCPendingOnHoldApproveAfterTripClose(returnId,trackingNo,tripOrderAssignmentId,
                        destWarehouseId,deliveryCenterID,tripId);
                break;

            case EnumSCM.PQCP_ONHOLD_BEFORE_TRIP_CLOSE_APPROVED_BEFORE_TRIP_CLOSE:
                markTripPickupSuccessfulQCPendingOnHoldApproveBeforeTripClose(returnId,trackingNo,
                        tripOrderAssignmentId,destWarehouseId,deliveryCenterID,tripId);
                break;

            case EnumSCM.PQCP_TRIP_CLOSE_ONHOLD_APPROVE:
                markTripPickupSuccessfulQCPendingTripCloseOnHoldApprove(returnId,trackingNo,tripOrderAssignmentId,
                        destWarehouseId,deliveryCenterID,tripId);
                break;

            case EnumSCM.PQCP_ONHOLD_BEFORE_TRIP_CLOSE_REJECT_AFTER_TRIP_CLOSE:
                markTripPickupSuccessfulQCPendingOnHoldTripCloseReject(returnId,trackingNo,tripOrderAssignmentId,
                        tripId,deliveryCenterID);
                break;

            case EnumSCM.PQCP_ONHOLD_BEFORE_TRIP_CLOSE_REJECT_BEFORE_TRIP_CLOSE:
                markTripPickupSuccessfulQCPendingOnHoldRejectTripClose(returnId,trackingNo,tripOrderAssignmentId,
                        tripId,deliveryCenterID);
                break;

            case EnumSCM.PQCP_TRIP_CLOSE_ONHOLD_REJECT:
                markTripPickupSuccessfulQCPendingTripCloseOnHoldReject(returnId,trackingNo,tripOrderAssignmentId,
                        tripId,deliveryCenterID);
                break;

            case EnumSCM.RESCHEDULED_Trip_close_ONHOLD_APPROVE:
                markTripPickupSuccessfulQCPendingRescheduleTripCloseOnHoldApprove(returnId,trackingNo,
                        tripOrderAssignmentId,destWarehouseId,deliveryCenterID,tripId);
                break;

            case EnumSCM.RESCHEDULED_Trip_close_ONHOLD_REJECT:
                markTripRescheduleTripCloseOnHoldReject(returnId,trackingNo,
                        tripOrderAssignmentId,destWarehouseId,deliveryCenterID,tripId);
                break;

            case EnumSCM.HAPPY_REQUEUE:
                sdaMarkTripHappyRequeue(returnId,trackingNo,tripOrderAssignmentId,
                        destWarehouseId,deliveryCenterID,tripId);
                break;

            case EnumSCM.HAPPY_REJECT:
                sdaMarkTripHappyReject(returnId,trackingNo,tripOrderAssignmentId,
                        tripId);
                break;

            case EnumSCM.PICKED_UP_ONHOLD_DAMAGED_APPROVE_TripCLose_Reship:
                markTripPickupOnHoldDamageProductApproveTripCloseReship(returnId,trackingNo,
                        tripOrderAssignmentId,tripId,deliveryCenterID,destWarehouseId);
                break;

            case EnumSCM.PICKED_UP_ONHOLD_DAMAGED_REJECT_TRIP_CLOSE:
                markTripPickupOnHoldDamageProductRejectTripClose(returnId,trackingNo,
                        tripOrderAssignmentId,tripId,deliveryCenterID,destWarehouseId);
                break;

            case EnumSCM.PICKED_UP_ONHOLD_DAMAGED_TripCLose_APPROVE:
                markTripPickupSuccessfulQCPendingOnHoldDamageProductTripCloseApprove(returnId,trackingNo,
                        tripOrderAssignmentId,tripId,deliveryCenterID,destWarehouseId);
                break;

            case EnumSCM.SDA_ONHOLD_CC_APPROVE_SDA_ONHOLD_Fail:
                markTripPickupSuccessfulSDAOnHoldCCApproveSDAOnHold(returnId,trackingNo,tripOrderAssignmentId,
                        destWarehouseId,deliveryCenterID);
                break;

            case EnumSCM.CC_PreApprove_After_Manifest:
                markTripPickupSuccessfulCCApproveTripComplete(returnId,trackingNo,destWarehouseId,deliveryCenterID,
                        tripOrderAssignmentId,tripId);
                break;

            case EnumSCM.TripStart_CCPreApprove_PQCP:
                markTripPickupSuccessfulCCApprovePQCP(returnId,trackingNo,destWarehouseId,deliveryCenterID,
                        tripOrderAssignmentId,tripId);
                break;

            case EnumSCM.TripStart_CCPreApprove_PickupOnHold:
                markTripPickupSuccessfulCCApprovePickUpOnHold(returnId,trackingNo,
                        tripOrderAssignmentId,tripId);
                break;

            case EnumSCM.SDA_REASON_OTHERS_REQUEUE:
                markTripPickupSuccessfulReasonOtherRequeue(returnId,trackingNo,destWarehouseId,deliveryCenterID,
                        tripOrderAssignmentId,tripId);
                break;

            case EnumSCM.SDA_REASON_OTHERS_REJECT:
                markTripPickupSuccessfulReasonOtherReject(returnId,trackingNo,tripId,
                        tripOrderAssignmentId);
                break;

            case EnumSCM.SDA_Reason_Happy_REQUEUE:
                markTripPickupSuccessfulReasonHappyRequeue(returnId,trackingNo,destWarehouseId,deliveryCenterID,
                        tripOrderAssignmentId,tripId);
                break;

            case EnumSCM.SDA_REASON_HAPPY_REJECT:
                markTripPickupSuccessfulReasonHappyReject(returnId,trackingNo,tripId,
                        tripOrderAssignmentId,deliveryCenterID);
                break;

            case EnumSCM.SDA_PickUp_ONHOLD_DamagedProduct_CC_REJECT_CloseTrip:
                markTripPickupSuccessfulPickUpOnHoldDamagedProductCCRejectCloseTrip(returnId,trackingNo,
                        tripOrderAssignmentId,tripId,deliveryCenterID);
                break;

            case EnumSCM.SDA_PickUp_ONHOLD_DamagedProduct_Trip_close_CC_REJECT:
                markTripPickupSuccessfulPickUpOnHoldDamagedProductCloseTripCCReject(returnId,trackingNo,
                        tripOrderAssignmentId,
                        tripId,deliveryCenterID);
                break;

            case EnumSCM.TripStart_CCPreApprove_PQCP_Refund_Trip_Close:
                markTripPickupSuccessfulCCApprovePQCPRefundCloseTrip(returnId,trackingNo,tripOrderAssignmentId,
                        destWarehouseId,
                        deliveryCenterID,tripId);
                break;

            case EnumSCM.PQCP_CC_Approve_DC_RejectFailed:
                markTripPickupSuccessfulPQCPCCApproveDCRejectFailed(returnId,trackingNo,tripOrderAssignmentId,tripId,
                        deliveryCenterID);
                break;

            case EnumSCM.FAILED_PICKUP:
                markTripFailedPickup(tripOrderAssignmentId,returnId,trackingNo,tripId);
                break;

            case EnumSCM.FAILED_PICK_UPS_DC_REJECT:
                failedPickups_DC_Reject(tripOrderAssignmentId,returnId,trackingNo,tripId,deliveryCenterID,
                        destWarehouseId);
                break;

            default:
                log.info("No matching status");
                break;
        }
    }

    private void failedPickups_DC_Reject(String tripOrderAssignmentId,String returnId,String trackingNo,Long tripId,String deliveryCenterID,String destWarehouseId) throws Exception {
        markTripFailedPickup(tripOrderAssignmentId,returnId,trackingNo,tripId);
        returnHelper.requeueAndProcessFurther(trackingNo,returnId,deliveryCenterID,destWarehouseId,
                tripOrderAssignmentId,EnumSCM.FAILED_PICKUP);
        returnHelper.returnRejectDC(trackingNo,5l);
        returnValidator.validateMLShipmentStatus(trackingNo,EnumSCM.RETURN_REJECTED);
    }

    private void markTripFailedPickup(String tripOrderAssignmentId,String returnId,String trackingNo,Long tripId) throws InterruptedException, JAXBException, IOException {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.NOT_ABLE_TO_PICKUP,trackingNo);
        returnHelper.tripComplete(tripOrderAssignmentId,EnumSCM.NOT_ABLE_TO_PICKUP);

    }

    /**
     * markTripPickupSuccessfulPQCPCCApproveDCRejectFailed
     *
     * @param returnId
     * @param tripId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulPQCPCCApproveDCRejectFailed(String returnId,String trackingNo,String tripOrderAssignmentId,Long tripId,String deliveryCenterID)
            throws Exception {
        //cc Approve
        returnHelper.approveAtCC(returnId);

        //update trip PQCP
        TripOrderAssignmentResponse response=
                lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),
                        EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update orders in Trip.");
        returnValidator.validatePQCPpreApproved(returnId,trackingNo,tripId);


        //receive order in DC
        returnHelper.receiveOrderAtDC(trackingNo,tripId);

        //complete the trip
        returnHelper.tripCompleteAtSDAPQCPReceviedInDC(tripOrderAssignmentId,EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,
                trackingNo,returnId,tripId);

        //reject at DC
        returnHelper.pickUpRejectedWithDCFailed(returnId,trackingNo);


    }


    /**
     * markTripPickupSuccessfulCCApprovePQCPRefundCloseTrip
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulCCApprovePQCPRefundCloseTrip(String returnId,String trackingNo,String tripOrderAssignmentId,String destWarehouseId,String deliveryCenterID,Long tripId)
            throws Exception {
        //cc Approve
        returnHelper.approveAtCC(returnId);

        //update trip PQCP
        TripOrderAssignmentResponse response=
                lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),
                        EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update orders in Trip.");
        returnValidator.validatePQCPpreApproved(returnId,trackingNo,tripId);

        returnHelper.receiveOrderAtDC(trackingNo,tripId);

        //validate refund and restock
        returnHelper.validateRefund(returnId,destWarehouseId,deliveryCenterID);

        //complete the trip
        returnHelper.tripCompleteAtSDAPQCPCCApprove(tripOrderAssignmentId,EnumSCM.PICKED_UP_SUCCESSFULLY,trackingNo,
                returnId,tripId);

        //transfer shipment back to warehouse
        returnHelper.transferShipmentToWareHouse(deliveryCenterID,returnId);

    }

    /**
     * markTripPickupSuccessfulPickUpOnHoldDamagedProductCloseTripCCReject
     *
     * @param returnId
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulPickUpOnHoldDamagedProductCloseTripCCReject(String returnId,String trackingNo,String tripOrderAssignmentId,Long tripId,String deliveryCenterId)
            throws Exception {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.ON_HOLD_DAMAGED_PRODUCT,trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),
                EnumSCM.PICKUP_ON_HOLD_DAMAGED_PRODUCT,"shipment status mismatch");
        returnHelper.tripCompleteAtSDAPickupOnHold(tripOrderAssignmentId,EnumSCM.ON_HOLD_DAMAGED_PRODUCT,trackingNo,
                returnId,tripId);
        returnHelper.rejectAtCC(returnId);
    }

    /**
     * markTripPickupSuccessfulPickUpOnHoldDamagedProductCCRejectCloseTrip
     *
     * @param returnId
     * @param tripId
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulPickUpOnHoldDamagedProductCCRejectCloseTrip(String returnId,String trackingNo,String tripOrderAssignmentId,Long tripId,String deliveryCenterId)
            throws Exception {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.ON_HOLD_DAMAGED_PRODUCT,trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),
                EnumSCM.PICKUP_ON_HOLD_DAMAGED_PRODUCT,"shipment status mismatch");
        returnHelper.rejectAtCC(returnId);
        returnHelper.tripCompleteAtSDACCReject(tripOrderAssignmentId,EnumSCM.ON_HOLD_DAMAGED_PRODUCT,trackingNo,
                returnId,tripId);
    }

    /**
     * markTripPickupSuccessfulReasonHappyRequeue
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulReasonHappyRequeue(String returnId,String trackingNo,String destWarehouseId,String deliveryCenterID,String tripOrderAssignmentId,long tripId)
            throws Exception {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.HAPPY_WITH_PRODUCT,trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),EnumSCM.HAPPY_WITH_PRODUCT,
                "shipment status mismatch");
        returnHelper.tripCompleteAtSDAFailedPickup(tripOrderAssignmentId,EnumSCM.HAPPY_WITH_PRODUCT,trackingNo,returnId,
                tripId);

        returnHelper.requeueAndProcessFurther(trackingNo,returnId,deliveryCenterID,destWarehouseId,
                tripOrderAssignmentId,EnumSCM.PICKUP_SUCCESSFUL);
    }

    /**
     * markTripPickupSuccessfulReasonHappyReject
     *
     * @param returnId
     * @param tripId
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulReasonHappyReject(String returnId,String trackingNo,Long tripId,String tripOrderAssignmentId,String deliveryCenterId)
            throws Exception {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.HAPPY_WITH_PRODUCT,trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),EnumSCM.HAPPY_WITH_PRODUCT,
                "shipment status mismatch");
        returnHelper.tripCompleteAtSDAFailedPickup(tripOrderAssignmentId,EnumSCM.HAPPY_WITH_PRODUCT,trackingNo,returnId,
                tripId);

        // reject at DC
        //NDR Config DB Update:- `is_enabled`=0,`is_rto_blocked`=0
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=0 where `service_type`='PICKUP'and `attempt_reason_code`='CUSTOMER_SATISFIED_WITH_PRODUCT' and `client_id`=2297;", "lms");
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        returnHelper.returnRejectDC(trackingNo,Long.valueOf(deliveryCenterId));
        returnValidator.validateMLShipmentStatus(trackingNo,EnumSCM.RETURN_REJECTED);
        returnValidator.validateReturnShipmentStatus(returnId,OrderStatus.REJ);
        returnValidator.validatePickUpShipmentStatus(returnId,ShipmentStatus.PICKUP_REJECTED);
    }

    /**
     * markTripPickupSuccessfulReasonOtherReject
     *
     * @param returnId
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulReasonOtherReject(String returnId,String trackingNo,Long tripId,String tripOrderAssignmentId)
            throws Exception {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.RETURNS_CANCELLATION,trackingNo);
        returnHelper.tripCompleteAtSDAFailedPickup(tripOrderAssignmentId,EnumSCM.RETURNS_CANCELLATION,trackingNo,
                returnId,tripId);
    }

    /**
     * markTripPickupSuccessfulReasonOtherRequeue
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulReasonOtherRequeue(String returnId,String trackingNo,String destWarehouseId,String deliveryCenterID,String tripOrderAssignmentId,Long tripId)
            throws Exception {
        //canceling the pickup
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.RETURNS_CANCELLATION,trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),EnumSCM.OTHERS,
                "shipment status mismatch");
        returnHelper.tripCompleteAtSDAFailedPickup(tripOrderAssignmentId,EnumSCM.RETURNS_CANCELLATION,trackingNo,
                returnId,tripId);
        returnHelper.requeueAndProcessFurther(trackingNo,returnId,deliveryCenterID,destWarehouseId,
                tripOrderAssignmentId,EnumSCM.PICKUP_SUCCESSFUL);

    }

    /**
     * markTripPickupSuccessfulCCApprovePickUpOnHold
     *
     * @param returnId
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulCCApprovePickUpOnHold(String returnId,String trackingNo,String tripOrderAssignmentId,Long tripId)
            throws Exception {
        returnHelper.approveAtCC(returnId);
        TripOrderAssignmentResponse response=
                lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),
                        EnumSCM.ON_HOLD_DAMAGED_PRODUCT,EnumSCM.UPDATE);
        Assert.assertNotEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update Trip.");
        returnValidator.validatePreApproveOnHoldPickUpWithDC(returnId,trackingNo,tripId);
    }

    /**
     * markTripPickupSuccessfulCCApprovePQCP
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulCCApprovePQCP(String returnId,String trackingNo,String destWarehouseId,String deliveryCenterID,String tripOrderAssignmentId,Long tripId)
            throws Exception {
        returnHelper.approveAtCC(returnId);
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,AttemptReasonCode.PICKUP_SUCCESSFUL_QC_PENDING.name(),
                trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,
                "shipment status mismatch");
        returnHelper.receiveOrderAtDC(trackingNo,tripId);
        returnHelper.tripCompleteAtSDA(tripOrderAssignmentId,AttemptReasonCode.PICKED_UP_SUCCESSFULLY.name(),trackingNo,
                returnId,tripId);
        returnHelper.validateRefund(returnId,destWarehouseId,deliveryCenterID);
        returnHelper.transferShipmentToWareHouse(deliveryCenterID,returnId);
    }

    /**
     * markTripPickupSuccessfulCCApproveTripComplete
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulCCApproveTripComplete(String returnId,String trackingNo,String destWarehouseId,String deliveryCenterID,String tripOrderAssignmentId,Long tripId)
            throws Exception {
        returnHelper.approveAtCC(returnId);
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.PICKED_UP_SUCCESSFULLY,trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),EnumSCM.PICKED_UP_SUCCESSFULLY,
                "shipment status mismatch");
        returnHelper.receiveOrderAtDC(trackingNo,tripId);
        returnHelper.tripCompleteAtSDAPQCPCCApprove(tripOrderAssignmentId,EnumSCM.PICKED_UP_SUCCESSFULLY,trackingNo,
                returnId,tripId);
        returnHelper.validateRefund(returnId,destWarehouseId,deliveryCenterID);
        returnHelper.transferShipmentToWareHouse(deliveryCenterID,returnId);
    }

    /**
     * markTripPickupSuccessful
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessful(String returnId,String trackingNo,String destWarehouseId,String deliveryCenterID,String tripOrderAssignmentId,Long tripId)
            throws Exception {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.PICKED_UP_SUCCESSFULLY,trackingNo);
        returnHelper.receiveOrderAtDC(trackingNo,tripOrderAssignmentId,tripId);
        returnHelper.tripCompleteAtSDA(tripOrderAssignmentId,AttemptReasonCode.PICKED_UP_SUCCESSFULLY.name(),trackingNo,
                returnId,tripId);
        returnHelper.pickUpSuccessfulWithDC(returnId,trackingNo);
        returnHelper.validateRefund(returnId,destWarehouseId,deliveryCenterID);
        returnHelper.transferShipmentToWareHouse(deliveryCenterID,returnId);
    }

    /**
     * markTripPickupSuccessfulSDAOnHoldCCApproveSDAOnHold
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulSDAOnHoldCCApproveSDAOnHold(String returnId,String trackingNo,String tripOrderAssignmentId,String destWarehouseId,String deliveryCenterID)
            throws Exception {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.ON_HOLD_DAMAGED_PRODUCT,trackingNo);
        returnHelper.approveAtCC(returnId);
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.ON_HOLD_DAMAGED_PRODUCT,trackingNo);
        returnValidator.validateMLShipmentStatus(trackingNo,EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER);
        returnValidator.validateReturnShipmentStatus(returnId,OrderStatus.RIT);
    }


    /**
     * markTripPickupSuccessfulQCPendingOnHoldDamageProductTripCloseApprove
     *
     * @param returnId
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulQCPendingOnHoldDamageProductTripCloseApprove(String returnId,String trackingNo,String tripOrderAssignmentId,Long tripId,String deliveryCenterId,String destWarehouseId)
            throws Exception {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.ON_HOLD_DAMAGED_PRODUCT,trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),
                EnumSCM.PICKUP_ON_HOLD_DAMAGED_PRODUCT,"shipment status mismatch");

        returnHelper.tripComplete(tripOrderAssignmentId,EnumSCM.ON_HOLD_DAMAGED_PRODUCT);

        returnValidator.validateTripOrderAssignment(tripId,ShipmentType.PU,
                com.myntra.lastmile.client.status.TripOrderStatus.OHP,EnumSCM.ON_HOLD_DAMAGED_PRODUCT);

        returnValidator.validateMLShipmentStatus(trackingNo,EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER);

        returnHelper.approveAtCC(returnId);

        returnValidator.validateMLShipmentStatus(trackingNo,EnumSCM.UNASSIGNED);

        returnHelper.recreateTripAndProcessFurther(trackingNo,returnId,deliveryCenterId,destWarehouseId,
                tripOrderAssignmentId,EnumSCM.PICKUP_SUCCESSFUL);

    }


    /**
     * markTripPickupOnHoldDamageProductApproveTripCloseReship
     *
     * @param returnId
     * @param tripId
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupOnHoldDamageProductApproveTripCloseReship(String returnId,String trackingNo,String tripOrderAssignmentId,Long tripId,String deliveryCenterId,String destWarehouseId)
            throws Exception {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.ON_HOLD_DAMAGED_PRODUCT,trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),
                EnumSCM.PICKUP_ON_HOLD_DAMAGED_PRODUCT,"shipment status mismatch");
        returnHelper.approveAtCC(returnId);

        //complete the trip
        returnHelper.tripComplete(tripOrderAssignmentId,EnumSCM.PICKUP_ON_HOLD_DAMAGED_PRODUCT);

        //validating tripOrderAssignment status and attempt reason code
        returnValidator.validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.OHP,
                EnumSCM.PICKUP_ON_HOLD_DAMAGED_PRODUCT);

        //validating ml
        returnValidator.validateMLShipmentStatus(trackingNo,EnumSCM.UNASSIGNED);

        returnHelper.recreateTripAndProcessFurther(trackingNo,returnId,deliveryCenterId,destWarehouseId,
                tripOrderAssignmentId,EnumSCM.PICKUP_SUCCESSFUL);


    }

    private void markTripPickupOnHoldDamageProductRejectTripClose(String returnId,String trackingNo,String tripOrderAssignmentId,Long tripId,String deliveryCenterId,String destWarehouseId)
            throws Exception {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.ON_HOLD_DAMAGED_PRODUCT,trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),
                EnumSCM.PICKUP_ON_HOLD_DAMAGED_PRODUCT,"shipment status mismatch");

        returnHelper.rejectAtCC(returnId);

        //complete the trip
        returnHelper.tripComplete(tripOrderAssignmentId,EnumSCM.PICKUP_ON_HOLD_DAMAGED_PRODUCT);

        //validating tripOrderAssignment status and attempt reason code
        returnValidator.validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.OHP,
                EnumSCM.PICKUP_ON_HOLD_DAMAGED_PRODUCT);

        //validating ml
        returnValidator.validateMLShipmentStatus(trackingNo,EnumSCM.RETURN_REJECTED);

    }

    /**
     * sdaMarkTripHappyReject
     *
     * @param returnId
     * @param tripId
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void sdaMarkTripHappyReject(String returnId,String trackingNo,String tripOrderAssignmentId,Long tripId)
            throws Exception {
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.HAPPY_WITH_PRODUCT,trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),EnumSCM.HAPPY_WITH_PRODUCT,
                "shipment status mismatch");
        returnHelper.tripCompleteAtSDAFailedPickup(tripOrderAssignmentId,EnumSCM.HAPPY_WITH_PRODUCT,trackingNo,returnId,
                tripId);
        returnHelper.rejectAtCC(returnId);
    }

    /**
     * sdaMarkTripHappyRequeue
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void sdaMarkTripHappyRequeue(String returnId,String trackingNo,String tripOrderAssignmentId,String destWarehouseId,String deliveryCenterID,Long tripId)
            throws Exception {
        //cancel return with happy
        returnHelper.tripUpdateAtSDA(tripOrderAssignmentId,EnumSCM.HAPPY_WITH_PRODUCT,trackingNo);
        Assert.assertEquals(lmsServiceHelper.getLastAttemptReasonCode(trackingNo),EnumSCM.HAPPY_WITH_PRODUCT,
                "shipment status mismatch");
        returnHelper.tripCompleteAtSDAPickupFailed(tripOrderAssignmentId,EnumSCM.HAPPY_WITH_PRODUCT,trackingNo,returnId,
                tripId);

        returnHelper.requeueAndProcessFurther(trackingNo,returnId,deliveryCenterID,destWarehouseId,
                tripOrderAssignmentId,EnumSCM.PICKUP_SUCCESSFUL);
    }

    /**
     * markTripRescheduleTripCloseOnHoldReject
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripRescheduleTripCloseOnHoldReject(String returnId,String trackingNo,String tripOrderAssignmentId,String destWarehouseId,String deliveryCenterID,Long tripId)
            throws Exception {
        //rescheduling the pickup
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE,
                        EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update orders in Trip.");
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        ReturnResponse returnStatusInLMS=returnHelper.getReturnStatusInLMS(returnId,LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        OrderStatus status=returnStatusInLMS.getDomainReturnShipments().get(0).getStatus();
        Assert.assertEquals(status.name(),EnumSCM.RIT);

        returnHelper.tripCompleteAtSDAPickupFailed(tripOrderAssignmentId,EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE,
                trackingNo,returnId,tripId);

        returnHelper.requeueAndProcessFurther(trackingNo,returnId,deliveryCenterID,destWarehouseId,
                tripOrderAssignmentId,EnumSCM.PQCP_TRIP_CLOSE_ONHOLD_REJECT);


    }

    /**
     * markTripPickupSuccessfulQCPendingRescheduleTripCloseOnHoldApprove
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulQCPendingRescheduleTripCloseOnHoldApprove(String returnId,String trackingNo,String tripOrderAssignmentId,String destWarehouseId,String deliveryCenterID,Long tripId)
            throws Exception {
        //update trip with PICKUP_SUCCESSFUL_QC_PENDING at SDA
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE,
                        EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update orders in Trip.");
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        ReturnResponse returnStatusInLMS=returnHelper.getReturnStatusInLMS(returnId,LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        OrderStatus status=returnStatusInLMS.getDomainReturnShipments().get(0).getStatus();
        Assert.assertEquals(status.name(),EnumSCM.RIT);

        returnHelper.tripCompleteAtSDAPickupFailed(tripOrderAssignmentId,EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE,
                trackingNo,returnId,tripId);
        returnHelper.requeueAndProcessFurther(trackingNo,returnId,deliveryCenterID,destWarehouseId,
                tripOrderAssignmentId,EnumSCM.PQCP_ONHOLD_APPROVED_After_trip_close);

    }

    /**
     * markTripPickupSuccessfulQCPendingOnHoldRejectTripClose
     *
     * @param returnId
     * @param tripId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulQCPendingTripCloseOnHoldReject(String returnId,String trackingNo,String tripOrderAssignmentId,Long tripId,String deliveryCenterID)
            throws Exception {
        //update trip PQCP
        TripOrderAssignmentResponse response=
                lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),
                        EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update orders in Trip.");
        returnValidator.validatePQCP(returnId,trackingNo,tripId);

        //receive order in DC
        returnHelper.receiveOrderAtDC(trackingNo,tripOrderAssignmentId,tripId);

        //complete the trip
        returnHelper.tripCompleteAtSDAPQCPReceviedInDC(tripOrderAssignmentId,EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,
                trackingNo,returnId,tripId);

        returnHelper.ohHoldPickUpWithDC(returnId,trackingNo,tripId,EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING);

        returnHelper.rejectAtCC(returnId);

        returnHelper.reshipTpCustomer(trackingNo,deliveryCenterID);


    }

    /**
     * markTripPickupSuccessfulQCPendingOnHoldRejectTripClose
     *
     * @param returnId
     * @param tripId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulQCPendingOnHoldRejectTripClose(String returnId,String trackingNo,String tripOrderAssignmentId,Long tripId,String deliveryCenterID)
            throws Exception {
        markTripPQCPAtSDA(returnId,tripOrderAssignmentId,trackingNo,tripId);
        returnHelper.ohHoldPickUpWithDC(returnId,trackingNo,tripId,EnumSCM.OTHERS);

        returnHelper.rejectAtCC(returnId);
        //acknowledge

        returnHelper.tripCompleteAtSDARejectOnHold(tripOrderAssignmentId,AttemptReasonCode.OTHERS.name(),trackingNo,
                returnId,tripId);

    }


    /**
     * markTripPickupSuccessfulQCPendingOnHoldTripCloseReject
     *
     * @param returnId
     * @param tripId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulQCPendingOnHoldTripCloseReject(String returnId,String trackingNo,String tripOrderAssignmentId,Long tripId,String deliveryCenterID)
            throws Exception {
        //update trip PQCP
        TripOrderAssignmentResponse response=
                lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),
                        EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update orders in Trip.");
        returnValidator.validatePQCP(returnId,trackingNo,tripId);

        //receive order in DC
        returnHelper.receiveOrderAtDC(trackingNo,tripId);

        returnHelper.ohHoldPickUpWithDC(returnId,trackingNo,tripId,EnumSCM.OTHERS);


        //complete the trip
        returnHelper.tripCompleteAtSDAAfterOnHold(tripOrderAssignmentId,EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,trackingNo,
                returnId,tripId);

        returnHelper.rejectAtCC(returnId);

        returnHelper.reship(trackingNo,Long.valueOf(deliveryCenterID));
    }

    /**
     * markTripPickupSuccessfulQCPendingTripCloseOnHoldApprove
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulQCPendingTripCloseOnHoldApprove(String returnId,String trackingNo,String tripOrderAssignmentId,String destWarehouseId,String deliveryCenterID,Long tripId)
            throws Exception {

        //update trip PQCP
        TripOrderAssignmentResponse response=
                lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),
                        EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update orders in Trip.");
        returnValidator.validatePQCP(returnId,trackingNo,tripId);
        //receive order in DC
        returnHelper.receiveOrderAtDC(trackingNo,tripId);


        //complete the trip
        returnHelper.tripCompleteAtSDAPQCPReceviedInDC(tripOrderAssignmentId,EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,
                trackingNo,returnId,tripId);

        returnHelper.ohHoldPickUpWithDC(returnId,trackingNo,tripId,EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING);

        returnHelper.approveAtCC(returnId);

        //validate refund
        returnHelper.validateRefund(returnId,destWarehouseId,deliveryCenterID);

        //acknowledge
        returnHelper.acknowledgeOnHoldAtDC(deliveryCenterID,returnId);

        returnHelper.transferShipmentToWareHouse(deliveryCenterID,returnId);

    }


    /**
     * markTripPickupSuccessfulQCPendingAfterTripClose
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @param trackingNo
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulQCPendingBeforeTripClose(String returnId,String trackingNo,String destWarehouseId,String deliveryCenterID,String tripOrderAssignmentId,Long tripId)
            throws Exception {

        //update trip PQCP
        TripOrderAssignmentResponse response=
                lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),
                        EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update orders in Trip.");
        returnValidator.validatePQCP(returnId,trackingNo,tripId);

        //receive order in DC
        returnHelper.receiveOrderAtDC(trackingNo,tripId);

        //mark qc pass at DC
        returnHelper.pickUpSuccessfulWithDC(returnId,trackingNo);

        //complete the trip
        returnHelper.tripCompleteAtSDAPQCP(tripOrderAssignmentId,EnumSCM.PICKED_UP_SUCCESSFULLY,trackingNo,returnId,
                tripId);

        //validate refund and restock
        returnHelper.validateRefund(returnId,destWarehouseId,deliveryCenterID);

        returnHelper.transferShipmentToWareHouse(deliveryCenterID,returnId);
    }

    /**
     * markTripPickupSuccessfulQCPendingAfterTripClose
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @param trackingNo
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulQCPendingAfterTripClose(String returnId,String trackingNo,String destWarehouseId,String deliveryCenterID,String tripOrderAssignmentId,Long tripId)
            throws Exception {

        markTripPQCPAtSDA(returnId,tripOrderAssignmentId,trackingNo,tripId);
        returnHelper.tripCompleteAtSDAPQCP(tripOrderAssignmentId,EnumSCM.PICKED_UP_SUCCESSFULLY,trackingNo,returnId,
                tripId);
        returnHelper.pickUpSuccessfulWithDC(returnId,trackingNo);
        returnHelper.validateRefund(returnId,destWarehouseId,deliveryCenterID);
        returnHelper.transferShipmentToWareHouse(deliveryCenterID,returnId);
    }

    /**
     * markTripPickupSuccessfulQCPendingOnHoldApproveAfterTripClose
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulQCPendingOnHoldApproveAfterTripClose(String returnId,String trackingNo,String tripOrderAssignmentId,String destWarehouseId,String deliveryCenterID,Long tripId)
            throws Exception {

        //update trip PQCP
        TripOrderAssignmentResponse response=
                lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),
                        EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update orders in Trip.");
        returnValidator.validatePQCP(returnId,trackingNo,tripId);

        //receive order in DC
        returnHelper.receiveOrderAtDC(trackingNo,tripOrderAssignmentId,tripId);

        //onhold at DC
        returnHelper.ohHoldPickUpWithDC(returnId,trackingNo,tripId,EnumSCM.OTHERS);


        //complete the trip
        returnHelper.tripCompleteAtSDAAfterOnHold(tripOrderAssignmentId,EnumSCM.PICKED_UP_SUCCESSFULLY,trackingNo,
                returnId,tripId);


        //cc Approve
        returnHelper.approveAtCC(returnId);

        //validate refund
        returnHelper.validateRefund(returnId,destWarehouseId,deliveryCenterID);

        //acknowledge
        returnHelper.acknowledgeOnHoldAtDC(deliveryCenterID,returnId);

        //validate restock
        returnHelper.transferShipmentToWareHouse(deliveryCenterID,returnId);

    }

    /**
     * markTripPickupSuccessfulQCPendingOnHoldApproveBeforeTripClose
     * EnumSCM.PQCP_ONHOLD_BEFORE_TRIP_CLOSE_APPROVED_BEFORE_TRIP_CLOSE
     *
     * @param returnId
     * @param destWarehouseId
     * @param deliveryCenterID
     * @param tripOrderAssignmentId
     * @throws org.codehaus.jackson.JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    private void markTripPickupSuccessfulQCPendingOnHoldApproveBeforeTripClose(String returnId,String trackingNo,String tripOrderAssignmentId,String destWarehouseId,String deliveryCenterID,Long tripId)
            throws Exception {

        //update trip PQCP
        TripOrderAssignmentResponse response=
                lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),
                        EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update orders in Trip.");
        returnValidator.validatePQCP(returnId,trackingNo,tripId);

        //receive order in DC
        returnHelper.receiveOrderAtDC(trackingNo,tripId);


        //onhold at DC
        returnHelper.ohHoldPickUpWithDC(returnId,trackingNo,tripId,EnumSCM.OTHERS);


        //cc Approve
        returnHelper.approveAtCC(returnId);

        //acknowledge
        returnHelper.acknowledgeOnHoldAtDC(deliveryCenterID,returnId);

        //validate refund and restock
        returnHelper.validateRefund(returnId,destWarehouseId,deliveryCenterID);

        //complete the trip
        returnHelper.tripComplete(tripOrderAssignmentId,EnumSCM.OTHERS);

        //validating tripOrderAssignment status and attempt reason code
        returnValidator.validateTripOrderAssignment(tripId,ShipmentType.PU,TripOrderStatus.OHP,"OTHERS");

        //validating ml
        returnValidator.validateMLShipmentStatus(trackingNo,EnumSCM.APPROVED_ONHOLD_PICKUP_WITH_DC);


    }


    /**
     * markTripPQCPAtSDA
     * marks trip with PICKUP_SUCCESSFUL_QC_PENDING at SDA
     *
     * @param returnId
     * @param tripOrderAssignmentId
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void markTripPQCPAtSDA(String returnId,String tripOrderAssignmentId,String trackingNumber,Long tripId)
            throws IOException, JAXBException, InterruptedException {

        //update trip with PICKUP_SUCCESSFUL_QC_PENDING at SDA
        TripOrderAssignmentResponse response=lmsServiceHelper
                .updatePickupInTrip(Long.parseLong(tripOrderAssignmentId),EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING,
                        EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Unable to update orders in Trip.");
        returnValidator.validatePQCP(returnId,trackingNumber,tripId);

        //receive order in DC
        returnHelper.receiveOrderAtDC(trackingNumber,tripId);


    }

    public String createPreAlertReportCSV(String trackingNumber,String courierCode,String expectedReceiveDate,String warehouseId,String tenantId) throws IOException {
        String uploadFileHeader = "Tracking Number,Courier Code,Expected Receive Date(dd-MM-yyyy),Warehouse Id,Tenant Id";

        File file = new File("./Data/lms/csv/PreAlertReport.csv");
        if (file.exists()) {
            file.delete();
            file = new File("./Data/lms/csv/PreAlertReport.csv");
        }
        FileWriter writer = new FileWriter(file, true);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(uploadFileHeader);
        bufferedWriter.newLine();
        bufferedWriter.write("\""+trackingNumber+"\",\""+courierCode+"\",\""+expectedReceiveDate+"\",\""+warehouseId+"\",\""+tenantId+"\"");
        bufferedWriter.newLine();
        bufferedWriter.close();
        return file.getPath();
    }

    public String uploadPreAlertReport(String filePath) throws UnsupportedEncodingException {

        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
        multipartEntityBuilder.addPart("file",new FileBody(new File(filePath)));
        multipartEntityBuilder.setContentType(ContentType.MULTIPART_FORM_DATA);
        Svc service = HttpExecutorService.executeHttpServiceForUpload(Constants.LMS_PATH.UPLOADPREALERTREPORT,null, SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.POST,multipartEntityBuilder,getTokenForFileUpload());
        String response=service.getResponseBody();

        return response;

    }

}
