package com.myntra.apiTests.erpservices.lms.DB;

import org.joda.time.DateTime;

import java.text.MessageFormat;

/**
 * @author Bharath.MC
 * @since Feb-2019
 */
public interface IQueries {

    interface MobileNumberMasking {
        String VirtualNumberStatusByTrackingNumber = "SELECT virtual_number_allocation_status " +
                "FROM virtual_number_request_mapping " +
                "WHERE tracking_number = '{0}';";

        String VirtualNumberStatusByTripNumber = "SELECT virtual_number_allocation_status " +
                "FROM virtual_number_request_mapping " +
                "WHERE entity_reference_id = '{0}';";

        String AllVirtualNumberTripStatusByTripId = "SELECT tracking_number ,virtual_number_allocation_status , trip_status , trip_order_status " +
                "FROM virtual_number_request_mapping VN " +
                "INNER JOIN trip TRP " +
                "ON VN.entity_reference_id = TRP.id " +
                "INNER JOIN trip_order_assignment TOA " +
                "ON TRP.id = TOA.trip_id " +
                "WHERE TRP.id = {0};";

        String AllVirtualNumberTripStatusByTrackingNumber = "SELECT tracking_number , virtual_number_allocation_status , trip_status , trip_order_status " +
                "FROM virtual_number_request_mapping VN " +
                "INNER JOIN trip TRP " +
                "ON VN.entity_reference_id = TRP.id " +
                "INNER JOIN trip_order_assignment TOA " +
                "ON TRP.id = TOA.trip_id " +
                "WHERE VN.tracking_number = '{0}';";

        String VirtualNumberByTrackingNumber = "SELECT * " +
                "FROM virtual_number_request_mapping " +
                "WHERE tracking_number = '{0}';";

        String PartyAPinByTrackingNumber = "SELECT party_A_pin " +
                "FROM virtual_number_request_mapping " +
                "WHERE tracking_number = '{0}';";

        String PartyBPinByTrackingNumber = "SELECT party_B_pin " +
                "FROM virtual_number_request_mapping " +
                "WHERE tracking_number = '{0}';";

        String TrackingNumbersByTripId = "SELECT VNM.tracking_number " +
                "FROM virtual_number_request_mapping AS VNM " +
                "INNER JOIN trip TRP " +
                "ON VNM.entity_reference_id = TRP.id " +
                "WHERE entity_reference_id={0};";

        String VirualNumbersByTripId = "SELECT VNM.virtual_number " +
                "FROM virtual_number_request_mapping AS VNM " +
                "INNER JOIN trip TRP " +
                "ON VNM.entity_reference_id = TRP.id " +
                "WHERE TRP.id={0};";

        String DistinctVNumberByTrip = "SELECT COUNT(DISTINCT VNM.virtual_number) AS virtual_number " +
                "FROM virtual_number_request_mapping AS VNM " +
                "INNER JOIN trip TRP " +
                "ON VNM.entity_reference_id = TRP.id " +
                "WHERE TRP.id={0};";

        String VirtualNumberEntriesByTripId = "SELECT virtual_Number , party_A_Pin , party_B_Pin , tenant_Id " +
                "FROM virtual_number_request_mapping " +
                "WHERE entity_reference_id = {0};";

        String VirtualNumberEntriesByTrackingNumber ="SELECT virtual_Number , party_A_Pin , party_B_Pin , tenant_Id " +
                "FROM virtual_number_request_mapping " +
                "WHERE tracking_number = '{0}';";

        String InvalidEntriesVirtualNumber = "SELECT COUNT(*) AS InvalidEntryCount " +
                "FROM virtual_number_request_mapping " +
                "WHERE virtual_number IS NOT NULL " +
                "AND (party_A_pin IS NULL OR party_A_pin  = '' " +
                "OR party_B_pin IS NULL OR party_B_pin  = '' " +
                "OR call_bridge_connection_id IS NULL OR call_bridge_connection_id  = '') " +
                "AND entity_reference_id = {0};";

        String InvalidEntriesByStatusVN = "SELECT COUNT(*) AS InvalidEntryCount " +
                "FROM virtual_number_request_mapping " +
                "WHERE virtual_number_allocation_status = 'RECEIVED' " +
                "AND ( virtual_number IS NULL OR virtual_number = '' " +
                "OR party_A_pin IS NULL OR party_A_pin  = '' " +
                "OR party_B_pin IS NULL OR party_B_pin  = '' " +
                "OR call_bridge_connection_id IS NULL OR call_bridge_connection_id  = '') " +
                "AND entity_reference_id = {0};";

        String InvalidEntriesByNotRecievedStatusInVN = "SELECT COUNT(*) AS InvalidEntryCount " +
                "FROM virtual_number_request_mapping " +
                "WHERE virtual_number_allocation_status = 'NOT_RECEIVED' " +
                "AND ( virtual_number IS NOT NULL OR virtual_number <> '' " +
                "OR party_A_pin IS NOT NULL OR party_A_pin  <> '' " +
                "OR party_B_pin IS NOT NULL OR party_B_pin  <> '' " +
                "OR call_bridge_connection_id IS NOT NULL OR call_bridge_connection_id  <> '') " +
                "AND entity_reference_id = {0};";

        String DuplicateVirtualNumberEntries = "SELECT virtual_number , party_A_pin , party_B_pin , COUNT(*) " +
                "FROM virtual_number_request_mapping " +
                "WHERE virtual_number_allocation_status = 'RECEIVED' " +
                "GROUP BY virtual_number , party_A_pin , party_B_pin " +
                "HAVING COUNT(*) > 1;";

        String UpdateMLShipmentMobileNumbers = "UPDATE ml_shipment " +
                "SET recipient_contact_number = {0} , alternate_contact_number = {1} " +
                "WHERE tracking_number = '{2}';";

        String DeliveryStaffIdByTrackingNumber ="SELECT TRP.delivery_staff_id " +
                "FROM trip_order_assignment TOA " +
                "INNER JOIN trip TRP " +
                "ON TOA.trip_id = TRP.id " +
                "WHERE TOA.tracking_no = '{0}' " +
                "LIMIT 1;";

        String MLShipments = "SELECT tracking_number " +
                "FROM ml_shipment " +
                "WHERE delivery_center_id = {0} " +
                "AND shipment_status = 'UNASSIGNED' " +
                "AND created_on BETWEEN CONCAT(DATE_SUB(CURDATE(), INTERVAL 30 DAY),' 00:00:00') AND CONCAT(CURDATE(),' 23:59:59');";

        String TripDeliveriesOrders = "SELECT OTS.order_id , MLS.tracking_number  , MLS.recipient_contact_number " +
                "FROM order_to_ship AS OTS " +
                "INNER JOIN ml_shipment AS MLS " +
                "ON OTS.order_id = MLS.source_reference_id " +
                "WHERE MLS.shipment_type = 'DL' "+
                "AND MLS.shipment_status = 'UNASSIGNED' " +
                "AND MLS.received_date BETWEEN CONCAT(DATE_SUB(CURDATE(), INTERVAL 30 DAY),' 00:00:00') AND CONCAT(CURDATE(),' 23:59:59') " +
                "AND OTS.tenant_id=4019 " +
                "AND MLS.delivery_center_id = {0} "+
                "ORDER BY MLS.recipient_contact_number {1};";

        String TripDeliveriesOrdersWithLimit = TripDeliveriesOrders.replace(";","  ") + "LIMIT {2};";

        String TripPickUpOrders ="SELECT * " +
                "FROM return_shipment AS RS " +
                "INNER JOIN ml_shipment AS MLS ON RS.source_return_id = MLS.source_reference_id " +
                "INNER JOIN pickup_shipment AS PS ON  RS.tracking_number = PS.tracking_number " +
                "WHERE MLS.shipment_type = 'PU' " +
                "AND MLS.shipment_status = 'UNASSIGNED' " +
                "AND RS.shipment_status='RETURN_CREATED' " +
                "AND RS.created_on BETWEEN CONCAT(DATE_SUB(CURDATE(), INTERVAL 30 DAY),' 00:00:00') AND CONCAT(CURDATE(),' 23:59:59') " +
                "AND MLS.delivery_center_id = {0} " +
                "AND PS.slot_type IS NULL " +
                "ORDER BY MLS.recipient_contact_number {1}  " +
                "LIMIT {2};";

        String TripSchedulePickUpOrders = "SELECT RS.source_return_id , MLS.tracking_number  , MLS.recipient_contact_number " +
                "FROM return_shipment AS RS INNER JOIN ml_shipment AS MLS ON RS.source_return_id = MLS.source_reference_id " +
                "WHERE MLS.shipment_type = 'PU' " +
                "AND RS.return_type='OPEN_BOX_PICKUP' " +
                "AND MLS.shipment_status = 'UNASSIGNED' " +
                "AND RS.slot_type IS NOT NULL " +
                "AND MLS.delivery_center_id = {0} " +
                "ORDER BY MLS.recipient_contact_number {1}  " +
                "LIMIT {2};";

        String TripExchangeOrders = "SELECT tracking_number , recipient_contact_number " +
                "FROM ml_shipment " +
                "WHERE delivery_center_id = {0} " +
                "AND shipment_status = 'UNASSIGNED' " +
                "AND shipment_type = 'EXCHANGE' " +
                "AND created_on BETWEEN CONCAT(DATE_SUB(CURDATE(), INTERVAL 30 DAY),' 00:00:00') AND CONCAT(CURDATE(),' 23:59:59') " +
                "ORDER BY recipient_contact_number {1}  " +
                "LIMIT {2};";

        String TripTryAndBuyOrders = "SELECT tracking_number , recipient_contact_number , source_reference_id AS order_id " +
                "FROM ml_shipment " +
                "WHERE delivery_center_id = {0} " +
                "AND shipment_status = 'UNASSIGNED'  " +
                "AND shipment_type = 'TRY_AND_BUY' " +
                "AND created_on BETWEEN CONCAT(DATE_SUB(CURDATE(), INTERVAL 30 DAY),' 00:00:00') AND CONCAT(CURDATE(),' 23:59:59')  " +
                "ORDER BY recipient_contact_number {1}  " +
                "LIMIT {2};";

        String UnAssignedShipments = "SELECT MLS.tracking_number " +
                "FROM order_to_ship OTS " +
                "INNER JOIN ml_shipment MLS " +
                "ON OTS.tracking_number = MLS.tracking_number " +
                "WHERE OTS.shipment_status = 'SHIPPED' " +
                "AND MLS.shipment_status = 'UNASSIGNED' " +
                "LIMIT 1;";

        String UpdateConnectionIDForVN = "UPDATE virtual_number_request_mapping " +
                "SET call_bridge_connection_id = '{0}' " +
                "WHERE tracking_number = '{1}';";
    }


    interface SortationConfig {
        String sortationConfigData = "SELECT * FROM sortation_config " +
                "WHERE tenant_id={0} " +
                "AND client_id={1} AND " +
                "origin_location='{2}' AND " +
                "destination_location='{3}' AND " +
                "sort_level={4};";
    }

    interface SortationLocationConfig {

        String sortLocationConfigData = "SELECT * FROM sort_location " +
                "WHERE tenant_id='{0}' " +
                "AND source_hub='{1}' " +
                "AND name='{2}';";

    }

    interface Tools{
        String deleteProperties="delete from core_application_properties where NAME='{0}' and id={1};";
    }

    static String formatQuery(String query, String... parms) {
        waitForDuration(5000);  //added this as DB was query was not returning latest resultset
        query = query.replace("'", "''");
        return MessageFormat.format(query, parms);
    }

    //added this as update/Insert quries does not require any delay
    static String formatQueryNoDelay(String query, String... parms) {
        query = query.replace("'", "''");
        return MessageFormat.format(query, parms);
    }

    static void waitForDuration(long milliseconds){
        try {
            System.out.println("Sleeping for "+milliseconds+" milliseconds "+ DateTime.now());
            Thread.sleep(milliseconds);
            System.out.println("waking after sleeping "+milliseconds+" milliseconds "+ DateTime.now());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    interface MasterbagV2SearchQuery {

        String multiple_MBId_Search = "SELECT * FROM shipment " +
                "WHERE id IN({0});";
        String origin_DestField_Search = "SELECT * FROM shipment " +
                "WHERE origin_premises_id={0} " +
                "AND origin_premises_type='{1}' " +
                "AND dest_premises_id={2} " +
                "AND dest_premises_type='{3}' " +
                "AND (last_modified_on BETWEEN '{4}' AND '{5}') ORDER BY created_on DESC;";
        String shippedOn_LastModifiedOn_Search = "SELECT * FROM shipment " +
                "WHERE (shipped_on BETWEEN '{0}' AND '{1}') " +
                "AND (last_modified_on BETWEEN '{2}' AND '{3}');";
        String status_origin_DestField_Search = "SELECT * FROM shipment " +
                "WHERE status='{0}' " +
                "AND origin_premises_id={1} " +
                "AND origin_premises_type='{2}' " +
                "AND dest_premises_id={3} " +
                "AND dest_premises_type='{4}' " +
                "AND (last_modified_on BETWEEN '{5}' " +
                "AND '{6}') ORDER BY created_on DESC ;";
        String search_All_Fields="SELECT * FROM shipment " +
                "WHERE shipping_method='{0}' AND" +
                " origin_premises_id={1} " +
                "AND origin_premises_type='{2}' " +
                "AND dest_premises_id={3} " +
                "AND dest_premises_type='{4}' " +
                "AND (shipped_on BETWEEN '{5}' AND '{6}') AND (last_modified_on BETWEEN '{7}' AND '{8}') ORDER BY created_on DESC;";

    }

    interface CashValidationQuery{
        String orderLineTableData = "SELECT * FROM order_line WHERE packet_id_fk='{0}';";
        String shipmentItemTableData = "SELECT SI.* " +
                "FROM shipment_item " +
                "SI INNER JOIN " +
                "order_to_ship OTS ON OTS.id = SI.order_to_ship_id " +
                "WHERE OTS.order_id = '{0}';";
        String mlDeliveryShipmentTableData="SELECT MLD.* " +
                "FROM ml_shipment " +
                "MLS INNER JOIN " +
                "ml_delivery_shipment MLD ON MLS.id = MLD.ml_shipment_id " +
                "WHERE source_reference_id = '{0}';";
        String orderAdditionalInfoTableData = "SELECT OI.* " +
                "FROM order_additional_info " +
                "OI INNER JOIN " +
                "order_to_ship OTS ON OTS.order_additional_info_id = OI.id " +
                "WHERE OTS.order_id = '{0}';";
        String orderReleaseTableData ="SELECT * FROM order_release WHERE id='{0}';";
    }

    interface ShipmentItemTableValue{
        String getShipmentItemTabelData="SELECT * FROM shipment_item " +
                "WHERE order_to_ship_id IN " +
                "(SELECT id FROM order_to_ship " +
                "WHERE tracking_number='{0}');";
    }

    interface OrdersTabSearchQuery {
        String getOrderId = "SELECT * FROM order_to_ship WHERE shipment_status='{0}' AND shipment_type='{1}' AND client_id={2};";
        String getTrackingNumber = "SELECT * FROM order_to_ship WHERE shipment_status='{0}' AND shipment_type='{1}' AND client_id={2};";
        String getMasterBagId = "SELECT * FROM shipment_order_map WHERE tracking_number='{0}';";
        String getPickupId = "SELECT * FROM pickup_shipment WHERE shipment_status='{0}';";
        String get_OrderToShip_data = "SELECT * FROM order_to_ship WHERE tracking_number='{0}';";
        String get_ReturnShipment_Data = "SELECT * FROM return_shipment WHERE tracking_number='{0}';";
        String get_ShipmentOrderMap_Data = "SELECT * FROM shipment_order_map WHERE tracking_number='{0}';";
        String get_OrderToShip_data1 = "SELECT * FROM order_to_ship WHERE shipment_status='{0}'";
    }

    interface Lastmile_TripUpdateSearch{
        String getDBDetailsUsingTripId = "SELECT * FROM trip TP " +
                "INNER JOIN trip_order_assignment TOA " +
                "ON TP.id = TOA.trip_id " +
                "WHERE trip_id='{0}' " +
                "AND shipment_type='{1}';";
        String getDBDetailsUsingTripNumber = "SELECT * FROM trip TP " +
                "INNER JOIN trip_order_assignment TOA " +
                "ON TP.id = TOA.trip_id " +
                "WHERE trip_number='{0}'" +
                "AND shipment_type='{1}';";
        String getTripShipmentDetailsByTripId = "SELECT * FROM trip TP " +
                "INNER JOIN trip_shipment_association TSA " +
                "ON TP.id = TSA.trip_id " +
                "WHERE trip_id='{0}' ;";
        String getTripShipmentDetailsBytripNumber ="SELECT * FROM trip TP " +
                "INNER JOIN trip_shipment_association TSA " +
                "ON TP.id = TSA.trip_id " +
                "WHERE trip_number='{0}' ;";
    }

    interface Lastmile_Trip_Planning_Search {

        String DCID_SDAID_QUERY = "SELECT * FROM trip " +
                "WHERE delivery_center_id={0} AND " +
                "delivery_staff_id={1}";

        String DCID_TRIPSTATUS_ASOFD_QUERY = "SELECT * FROM trip " +
                "WHERE delivery_center_id={0} AND " +
                "trip_status='{1}'";

        String DCID_TRIPSTATUS_AS_CREATED_QUERY = "SELECT * FROM trip " +
                "WHERE delivery_center_id={0} AND " +
                "trip_status='{1}'";

        String DCID_TRIPSTATUS_AS_COMPLETED_QUERY = "SELECT * FROM trip " +
                "WHERE delivery_center_id={0} AND " +
                "trip_status='{1}'";

        String DCID_SHOW_DELIVERY_STAFF_TRUE_IS_CARD_ENABLED_BOTH_QUERY = "SELECT * FROM trip " +
                "WHERE delivery_center_id={0} AND " +
                "is_inbound=0 AND " +
                "is_deleted=0&1";

        String DCID_SHOW_DELIVERY_STAFF_TRUE_IS_DELETED_NO_QUERY = "SELECT * FROM trip " +
                "WHERE delivery_center_id={0} AND " +
                "is_deleted={1}";

        String DCID_SHOW_DELIVERY_STAFF_TRUE_IS_DELETED_YES_QUERY = "SELECT * FROM trip " +
                "WHERE delivery_center_id={0} AND " +
                "is_deleted={1}";

        String DCID_CREATED_ON_DATE_TRIP_START_TRIP_END_DATE_QUERY = "SELECT * FROM trip " +
                "WHERE delivery_center_id={0} AND  " +
                "(created_on BETWEEN '{1}' AND '{2}')" +
                "AND (trip_start_time BETWEEN '{3}' AND '{4}') AND " +
                "(trip_end_time BETWEEN '{5}' AND '{6}') AND " +
                "trip_status='{7}'";

        String SEARCH_WITH_ALLFIELDS_QUERY = "SELECT * FROM trip " +
                "WHERE trip_number ='{0}' " +
                "ORDER BY trip_date DESC LIMIT 1";

        String REFRESH_DL_TnB_ORDERS="SELECT OTS.* " +
                "FROM order_to_ship AS OTS " +
                "INNER JOIN ml_shipment AS MLS " +
                "ON OTS.order_id = MLS.source_reference_id " +
                "WHERE MLS.shipment_type = '{0}' " +
                "AND MLS.shipment_status = '{1}' " +
                "AND MLS.received_date BETWEEN CONCAT(DATE_SUB(CURDATE(), INTERVAL 30 DAY),' 00:00:00') AND CONCAT(CURDATE(),' 23:59:59') " +
                "AND OTS.tenant_id={2} " +
                "AND MLS.delivery_center_id = {3} ORDER BY last_modified_on DESC;";

        String REFRESH_EX_ORDERS ="SELECT * FROM ml_shipment " +
                "WHERE delivery_center_id = {0} " +
                "AND shipment_status = '{1}' " +
                "AND shipment_type = '{2}' " +
                "AND created_on BETWEEN CONCAT(DATE_SUB(CURDATE(), INTERVAL 30 DAY),' 00:00:00') AND CONCAT(CURDATE(),' 23:59:59') ORDER BY last_modified_on DESC;";

        String REFRESH_PU_ORDERS ="SELECT * " +
                "FROM return_shipment AS RS " +
                "INNER JOIN ml_shipment AS MLS ON RS.source_return_id = MLS.source_reference_id " +
                "INNER JOIN pickup_shipment AS PS ON  RS.tracking_number = PS.tracking_number " +
                "WHERE MLS.shipment_type = '{0}' " +
                "AND MLS.shipment_status = '{1}' " +
                "AND RS.shipment_status='{2}' " +
                "AND RS.created_on BETWEEN CONCAT(DATE_SUB(CURDATE(), INTERVAL 30 DAY),' 00:00:00') AND CONCAT(CURDATE(),' 23:59:59') " +
                "AND MLS.delivery_center_id = {3} " +
                "AND PS.slot_type IS NULL " +
                "ORDER BY RS.last_modified_on DESC;";
    }

    interface Lastmile_Trip_Result_Search {

        String DCID_CREATED_DATE_ONHOLD_PICKUP_SEARCH_QUERY = "SELECT * FROM ml_shipment " +
                "WHERE delivery_center_id={0} AND " +
                "tenant_id={1} AND " +
                "last_modified_on BETWEEN '{2}' AND '{3}' AND " +
                "shipment_status='{4}' ORDER BY created_on DESC";
        String DCID_CREATED_DATE_FAILED_PICKUP_SEARCH_QUERY = "SELECT * FROM ml_shipment " +
                "WHERE delivery_center_id={0} AND " +
                "tenant_id={1} And " +
                "last_modified_on BETWEEN '{2}' AND '{3}' AND " +
                "shipment_type='{4}' AND " +
                "shipment_status='{5}'" +
                " ORDER BY created_on DESC";

        String DCID_AND_CREATED_DATE_FAILED_DELIVERY_SEARCG_QUERY = "SELECT * FROM ml_shipment " +
                "WHERE delivery_center_id={0} AND " +
                "tenant_id={1} AND " +
                "shipment_status='{2}' AND " +
                "last_modified_on BETWEEN '{3}' AND '{4}' AND " +
                "shipment_type='{5}' ORDER BY created_on DESC";

        String DCID_AND_CREATED_DATE_TRY_AND_BUY_SEARCH_QUERY = "SELECT * FROM ml_shipment " +
                "WHERE delivery_center_id={0} AND " +
                "tenant_id={1} AND " +
                "last_modified_on BETWEEN '{2}' AND '{3}' AND " +
                "shipment_type='{4}' AND " +
                "shipment_status='{5}' ORDER BY created_on DESC";

        String DCID_AND_CREATED_DATE_FAILED_EXCHANGE_SEARCH_QUERY = "SELECT * FROM ml_shipment " +
                "WHERE delivery_center_id={0} AND " +
                "tenant_id={1} AND " +
                "last_modified_on BETWEEN '{2}' AND '{3}' AND " +
                "shipment_type='{4}' AND " +
                "shipment_status='{5}' ORDER BY created_on DESC";
    }
}