package com.myntra.apiTests.erpservices.lms.validators;

import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.response.OrderResponse;
import org.testng.Assert;

public class StartTripValidator {
    TripOrderAssignmentResponse tripOrderAssignmentResponse=null;
    LastmileClient lastmileClient =new LastmileClient();

    //1. Trip
    //2. Trip Order Assignment
    //3. ML_Shipment
    //4. order_to_ship

    public StartTripValidator(TripOrderAssignmentResponse tripOrderAssignmentResponse) {
        this.tripOrderAssignmentResponse = tripOrderAssignmentResponse;
    }
    public void validateStartTrip(String statusType,String statusMessage,String statusCode){
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType(), statusType,"Trip not started");
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusMessage(),statusMessage,"Trip status message not updated");
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusCode(),statusCode, "Status not matched");
    }
    public void validateStartTripInTrip(TripResponse tripResponse, String tripStatus){
        Assert.assertEquals(tripResponse.getTrips().get(0).getTripStatus().toString(),tripStatus,"Trip status not Proper");
    }
    public void validateMLShipment(MLShipmentResponse mlShipmentResponse, String shipmentStatus ){
        Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(),shipmentStatus,"Shipment status not correct after Trip starts.");

    }

}
