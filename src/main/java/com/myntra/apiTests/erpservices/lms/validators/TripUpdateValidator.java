package com.myntra.apiTests.erpservices.lms.validators;

import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import org.testng.Assert;

public class TripUpdateValidator {
    TripOrderAssignmentResponse tripOrderAssignmentResponse=null;

    public TripUpdateValidator(TripOrderAssignmentResponse tripOrderAssignmentResponse) {
        this.tripOrderAssignmentResponse = tripOrderAssignmentResponse;
    }
    public void validateTrackingNumber(String trackingNumber){
        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getTrackingNumber(),trackingNumber,"Tracking number mismatch.");
    }
    public void validateTripOrderAssignmentstatus(String tripOrderStatus){

        Assert.assertEquals(tripOrderAssignmentResponse.getData().get(0).getTripOrderStatus().toString(),tripOrderStatus,"Trip Order status mismatch");
    }
    public void validateTripStaus(TripResponse tripResponse,String tripStatus){
        Assert.assertEquals(tripResponse.getTrips().get(0).getTripStatus(),tripStatus,"Validating trip status");
    }
}
