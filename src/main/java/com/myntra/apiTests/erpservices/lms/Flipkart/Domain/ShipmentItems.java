package com.myntra.apiTests.erpservices.lms.Flipkart.Domain;

import lombok.Data;

@Data
public class ShipmentItems {

    private String stateGoodsAndServiceTax;

    private String integratedGoodsAndServiceTax;

    private String sourceItemReferenceId;

    private String imageURL;

    private String itemBarcode;

    private String centralGoodsAndServiceTax;

    private String sellerName;

    private String goodsAndServiceTaxIdentificationNumber;

    private String sellerAddress;

    private String itemDescription;

    private String sellerId;
}
