package com.myntra.apiTests.erpservices.lms.DB;

import com.myntra.lordoftherings.boromir.DBUtilities;

import java.util.List;
import java.util.Map;

/**
 * @author Bharath.MC
 * @since Feb-2019
 */
//TODO postfix all methods with "DB"
public class LastMileDAO {

    public static Map<String, Object> GetVirtualNumberEntriesByTrackingNumberDB(String trackingNumber) {
        String query = IQueries.formatQuery(IQueries.MobileNumberMasking.VirtualNumberByTrackingNumber, trackingNumber);
        Map<String, Object> virtualNumberRS = DBUtilities.exSelectQueryForSingleRecord(query, "lms");
        return virtualNumberRS;
    }

    public static String GetVirtualNumberRequestIdDB(String trackingNumber) {
        String query = IQueries.formatQuery(IQueries.MobileNumberMasking.VirtualNumberByTrackingNumber, trackingNumber);
        Map<String, Object> virtualNumberRS = DBUtilities.exSelectQueryForSingleRecord(query, "lms");
        return virtualNumberRS.get("virtual_number_request_id").toString();
    }

    public static Map<String, String> GetTripAndVNStatusByTrackingNumberDB(String trackingNumber) {
        String allVNAndTripStatusByTrackingNumber = IQueries.formatQuery(IQueries.MobileNumberMasking.AllVirtualNumberTripStatusByTrackingNumber, trackingNumber);
        List<Map<String, String>> tripAndVNStatus = DBUtilities.exSelectQuery(allVNAndTripStatusByTrackingNumber, "lms");
        return tripAndVNStatus.get(0);
    }

    public static Map<String, String> getVirtualNumberEntriesByTrackingNumberDB(String trackingNumber) {
        String virtualNumberEntriesByTripId = IQueries.formatQuery(IQueries.MobileNumberMasking.VirtualNumberEntriesByTrackingNumber, trackingNumber);
        List<Map<String, String>> virtualNumberEntries = DBUtilities.exSelectQuery(virtualNumberEntriesByTripId, "lms");
        return virtualNumberEntries.get(0); //since one tracking no : 1 entry
    }

    public static String GenerateRandomPhoneNumber() {
        Long number = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
        return String.valueOf(number);
    }

    /**
     * Created this method to update the mobile numbers to look "realistic".
     * Shit, I should have fixed the global file where we are generating mobile number. wil do that later.
     */
    public static void updatePhoneNumbersInMLShipmentDB(String deliveryCenterId) {
        String trackingNumber = "";
        String updateMobileNumberQuery = "";
        String deliverisQuery = IQueries.formatQueryNoDelay(IQueries.MobileNumberMasking.MLShipments, deliveryCenterId);
        List<Map<String, String>> mlShipments = DBUtilities.exSelectQuery(deliverisQuery, "lms");
        for (Map<String, String> record : mlShipments) {
            updateMobileNumberQuery = IQueries.formatQueryNoDelay(IQueries.MobileNumberMasking.UpdateMLShipmentMobileNumbers, GenerateRandomPhoneNumber(), GenerateRandomPhoneNumber(), record.get("tracking_number"));
            DBUtilities.exUpdateQuery(updateMobileNumberQuery, "lms");
        }
    }

    public static void updatePhoneNumberInMLShipmentDB(String trackingNumber) {
        String updateMobileNumberQuery = "";
        updateMobileNumberQuery = IQueries.formatQueryNoDelay(IQueries.MobileNumberMasking.UpdateMLShipmentMobileNumbers, GenerateRandomPhoneNumber(), GenerateRandomPhoneNumber(), trackingNumber);
        DBUtilities.exUpdateQuery(updateMobileNumberQuery, "lms");
    }

    public static List<Map<String, String>> getDeliveriesOrdersMLShipmentDB(String deliveryCenterId, String sortOrder) {
        String trackingNumber = "";
        String updateMobileNumberQuery = "";
        String deliverisQuery = IQueries.formatQuery(IQueries.MobileNumberMasking.TripDeliveriesOrdersWithLimit, deliveryCenterId, sortOrder, "20");
        List<Map<String, String>> mlShipments = DBUtilities.exSelectQuery(deliverisQuery, "lms");
        return mlShipments;
    }

    public static List<Map<String, String>> getPickupOrdersMLShipmentDB(String deliveryCenterID, String sortOrder) {
        String deliverisQuery = IQueries.formatQuery(IQueries.MobileNumberMasking.TripPickUpOrders, deliveryCenterID, sortOrder, "20");
        List<Map<String, String>> mlShipments = DBUtilities.exSelectQuery(deliverisQuery, "lms");
        return mlShipments;
    }

    public static List<Map<String, String>> getExchangeOrdersMLShipmentDB(String deliveryCenterID, String sortOrder) {
        String deliverisQuery = IQueries.formatQuery(IQueries.MobileNumberMasking.TripExchangeOrders, deliveryCenterID, sortOrder, "20");
        List<Map<String, String>> mlShipments = DBUtilities.exSelectQuery(deliverisQuery, "lms");
        return mlShipments;
    }

    public static List<Map<String, String>> getTryAndBuyOrdersMLShipmentDB(String deliveryCenterID, String sortOrder) {
        String deliverisQuery = IQueries.formatQuery(IQueries.MobileNumberMasking.TripTryAndBuyOrders, deliveryCenterID, sortOrder, "20");
        List<Map<String, String>> mlShipments = DBUtilities.exSelectQuery(deliverisQuery, "lms");
        return mlShipments;
    }

    public static String GetUnAssignedShipment() {
        String query = IQueries.formatQuery(IQueries.MobileNumberMasking.UnAssignedShipments);
        Map<String, Object> vnStatus = DBUtilities.exSelectQueryForSingleRecord(query, "lms");
        return vnStatus.get("tracking_number").toString();
    }

    public static String getDeliveryStaffIdByTrackingNumberDB(String trackingNumber) {
        String query = IQueries.formatQuery(IQueries.MobileNumberMasking.DeliveryStaffIdByTrackingNumber, trackingNumber);
        Map<String, Object> deliveryStaffId = DBUtilities.exSelectQueryForSingleRecord(query, "lms");
        return deliveryStaffId.get("delivery_staff_id").toString();
    }

    public static void updateCallBridgeConnectionId(String trackingNumber, String connectionId) {
        String updateConnectionId = "";
        updateConnectionId = IQueries.formatQueryNoDelay(IQueries.MobileNumberMasking.UpdateConnectionIDForVN, connectionId, trackingNumber );
        DBUtilities.exUpdateQuery(updateConnectionId, "lms");
    }


    public static void main(String[] args) {
        //System.out.println(GetTripStatusDB("72774"));
        updatePhoneNumbersInMLShipmentDB("5");
        //System.out.println(GetTripAndVNStatusByTrackingNumberDB("ML0001336145"));
        //System.out.println(GetUnAssignedShipment());
    }
}

