package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.DB.IQueries;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.lms.client.domain.response.ReturnResponse;
import com.myntra.lms.client.response.*;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

public class OrderTabSearchHelper {
    private static Logger log = Logger.getLogger(LmsServiceHelper.class);
    LMSClient lmsClient = new LMSClient();
    ReturnHelper returnHelper = new ReturnHelper();


    /**
     * Build the Api query param
     *
     * @param searchParam
     * @return
     */
    public String buildQueryParam(String... searchParam) {
        String finalQueryParam = "?";
        for (int i = 0; i <= searchParam.length - 1; i++) {


            if (searchParam.length == 1) {
                finalQueryParam = finalQueryParam + searchParam[i];
            } else if (i == searchParam.length - 1) {
                finalQueryParam = finalQueryParam + searchParam[i];
            } else {
                finalQueryParam = finalQueryParam + searchParam[i] + "&";
            }
        }
        return finalQueryParam;
    }

    /**
     * Used to get the order details in ordersTab
     *
     * @param queryParam
     * @return
     */
    public OrderResponse getOrdersTabDashBoardSearch(String queryParam) {
        OrderResponse orderResponse = null;
        try {
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_ORDER_TRACKINGNUMBER, new String[]{queryParam}, SERVICE_TYPE.LMS_SVC.toString(),
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            orderResponse = (OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderResponse());
        } catch (UnsupportedEncodingException | JAXBException e) {
            e.getMessage();
            log.error("ERROR:: While Getting the order details using Tracking Number - "+e.getMessage());
            Assert.fail("FAILED:: While Getting the order details in Orders Tab Search - "+e.getMessage());
        }
        return orderResponse;
    }

    /**
     * Used to get the master bag details in ordersTab
     *
     * @param queryParam
     * @return
     */
    public OrderResponse getOrdersTabDashBoardSearchForMasterBagField(String queryParam) {
        OrderResponse orderResponse = null;
        try {
            Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_ALL_SHIPMENTS, new String[]{queryParam}, SERVICE_TYPE.LMS_SVC.toString(),
                    HTTPMethods.GET, null, Headers.getLmsHeaderXML());
            orderResponse = (OrderResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(), new OrderResponse());
        } catch (UnsupportedEncodingException | JAXBException e) {
            log.error("ERROR:: While Getting the order details using Tracking Number - "+e.getMessage());
            Assert.fail("FAILED:: While Getting the master bag details in Orders Tab Search - "+e.getMessage());
        }
        return orderResponse;
    }
}
