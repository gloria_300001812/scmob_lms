package com.myntra.apiTests.erpservices.lms.Constants;

public class LMS_PINCODE {

	public static final String ML_BLR = "560068"; // BLRP DC : ELC 5 DC-Code: ELC
	public static final String CASH_RECON_ML_BLR = "560068"; // BLRP DC : ELC 5 DC-Code: ELC
	public static final String PUNE_EK = "411001"; // PUNE
	public static final String MUMBAI_DE_RHD = "400053"; // MUDE
	public static final String NORTH_DE = "400053"; // DENORTH
	public static final String NORTH_DE_NEW = "400050"; // DENORTH
	public static final String NORTH_CGH = "160019"; // MLCGH DC-CODE: CAR
	public static final String NORTH_DELHI = "110011"; // MLDELHI
	public static final String KERALA_BD = "682001"; // CO
	public static final String JAMMU_IP = "180001"; // IPJAMMU
	public static final String ODISHA_BD = "751001"; // BDODISHA
	public static final String DELHIS_DC = "100002";

	public static final String SEC_ML = "500003"; // SECML secundrabad DC: T-SEC

	public static final String KKC_ML = "521180"; // KKCML karchanapalli DC: T-KKC
	public static final String BBN_ML = "750122"; // BBNML Bhuvneswar DC: T-BBN
	public static final String JPR_ML = "302005"; // JPRML DC: T-JPR
	public static final String KOTA_ML = "325001"; // KOTAML KOTA -- Aamli DC : T-KT

	public static final String GGN_ML = "120001"; // GGNML Gurgaon DC: T-GGN
	public static final String GRM_ML = "122223"; // GRNML Gurgaon remote DC: T-GRM
	public static final String LXR_ML = "209101"; // LXRML Kanpur DC: T-LXR
	public static final String ND_ML = "201301"; // NDAML Noida DC: T-ND
	public static final String DRD_ML = "248003"; // DRDML DEHRADUN DC: T-DRD
	public static final String GWH_ML = "781001"; // GWHML Guwahati Aizol DC : DC-CODE: T-AZ
	public static final String KKT_ML = "700001";// KKTML kolkata park DC T-PK
	public static final String WHF_ML = "560066"; // WhiteFeild WHF DC WHF
	public static final String HSR_ML = "560102"; // HSR DC:
	public static final String BSK_ML = "560085";// Bandsankri DC: T-BSK
	public static final String KRL_ML = "688001";// KERALA Region: Kerala-ML(ALPKL) DC: ALP Kerala Allepuzha DC

}
