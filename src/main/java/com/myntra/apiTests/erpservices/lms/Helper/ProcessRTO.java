package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateAdditionalInfo;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.testng.Assert;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class ProcessRTO {

    String env = getEnvironment();
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    TMSServiceHelper tmsServiceHelper= new TMSServiceHelper();
    TripClient_QA tripClient_qa=new TripClient_QA();


    public List<String> RTOTest(String DcId, String wareHouseId, Long destPremiseId) throws Exception {

        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", wareHouseId, EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(tracking_num);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(com.myntra.lastmile.client.status.MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.DL);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 =lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        long masterBagId = (long)tmsServiceHelper.createcloseMBforRTO(destPremiseId, packetId, DcId);
        tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);
        List<String> data = new ArrayList<>();
        data.add(tracking_num);
        data.add(packetId);
        return data;

    }
}
