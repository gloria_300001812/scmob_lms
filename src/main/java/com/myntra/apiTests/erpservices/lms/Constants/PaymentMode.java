package com.myntra.apiTests.erpservices.lms.Constants;

/**
 * Created by Shubham Gupta on 10/15/16.
 */
public enum PaymentMode {
	CC, DC, WALLET, LP, NETBANKING, COD
}
