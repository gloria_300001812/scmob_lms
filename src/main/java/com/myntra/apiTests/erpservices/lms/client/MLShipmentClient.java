package com.myntra.apiTests.erpservices.lms.client;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import org.testng.Assert;

import java.io.IOException;

public class MLShipmentClient {

    public static MLShipmentResponse getMLShipmentDetailsByTrackingNumber(String trackingNumber){
        MLShipmentResponse mlShipmentResponse=null;
        String param=""+trackingNumber+"?tenantId=" + LMS_CONSTANTS.TENANTID + "&clientId=" + LMS_CONSTANTS.CLIENTID;
        try {
            mlShipmentResponse=(MLShipmentResponse) APIUtilities.getJsontoObjectUsingFasterXML(HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_ML_SHIPMENT_BY_TRACKING_NOVersion2,
                    new String[]{param}, SERVICE_TYPE.Last_mile.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON()).getResponseBody(),
                    new MLShipmentResponse());
        }catch ( IOException e){
            Assert.fail("FAILED:: While getting Shipment details from ML_SHipment using Tracking Number"+e.getMessage());
        }
        return mlShipmentResponse;
    }

    /**
     * Creating this as, exchage return entries are not present in MLShipmentResponse POJO
     */
    public static String GetMLShipmentDetailsByTrackingNumber(String trackingNumber){
        String param=""+trackingNumber+"?tenantId=" + LMS_CONSTANTS.TENANTID + "&clientId=" + LMS_CONSTANTS.CLIENTID;
        String mlShipmentResponse = null;
        try {
            mlShipmentResponse = HttpExecutorService.executeHttpService(Constants.LMS_PATH.GET_ML_SHIPMENT_BY_TRACKING_NOVersion2,
                    new String[]{param}, SERVICE_TYPE.Last_mile.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON()).getResponseBody();
        }catch ( IOException e){
            Assert.fail("FAILED:: While getting Shipment details from ML_SHipment using Tracking Number"+e.getMessage());
        }
        return mlShipmentResponse;
    }

}
