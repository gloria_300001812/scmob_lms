package com.myntra.apiTests.erpservices.lms.Helper;

import com.fasterxml.jackson.core.JsonParseException;
import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.response.EmptyResponse;
import com.myntra.commons.utils.Context;
import com.myntra.lms.client.status.CourierCreationStatus;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.*;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import static com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS.CLIENTID;
import static com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS.TENANTID;
import static com.myntra.logistics.platform.domain.Premise.PremiseType.DELIVERY_CENTER;

@Slf4j
public class LMS_CloseBoxReturnHelper {
    LMS_ReturnHelper lms_returnHelper=new LMS_ReturnHelper();
    LMSHelper lmsHelper=new LMSHelper();
    RMSServiceHelper rmsServiceHelper=new RMSServiceHelper();
    LmsServiceHelper lmsServiceHelper=new LmsServiceHelper();
    private String FGPathparam;

    /**
     * processClosedBoxPickup
     *
     * @param returnId
     * @param toStatus
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void processClosedBoxPickup(String returnId,String toStatus,String receivedAt)
            throws IOException, JAXBException, InterruptedException, ManagerException {
        switch (toStatus) {
            case EnumSCM.PICKED_UP_SUCCESSFULLY:
                pickupSuccessFullyClosedBox(returnId,ItemQCStatus.PASSED);
                break;
            case EnumSCM.FAILED_PICKUP_AND_SUCCESS:
                failedPickupClosedBox(returnId,ItemQCStatus.PASSED,receivedAt);
                break;
            case EnumSCM.PICKED_UP_SHORTAGE:
                pickupSuccessFullyClosedBox(returnId,ItemQCStatus.SHORTAGE);
                break;
            case EnumSCM.PICKUP_SUCCESSFUL_ONHOLD_APPROVE:
                PickupSuccessfulApproveRejectClosedBox(returnId,ItemQCStatus.ON_HOLD,"APPROVED",receivedAt);
                break;
            case EnumSCM.PICKUP_SUCCESSFUL_ONHOLD_REJECTED:
                PickupSuccessfulApproveRejectClosedBox(returnId,ItemQCStatus.ON_HOLD,"REJECTED",receivedAt);
                break;
        }
    }

    /**
     * processClosedBoxPickup
     *
     * @param returnId
     * @param status
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void pickupSuccessFullyClosedBox(String returnId,ItemQCStatus status)
            throws IOException, JAXBException, InterruptedException {

        String tracking_number=getPickupTrackingNoFromSourceReturnId(
                String.valueOf(returnId),TENANTID,CLIENTID);

        String courier_code="DE";
        String shipment_type="PU";
        String activity_type1="PICKUP_DETAILS_UPDATED";
        String activity_sub_type1="SHIPMENT_ACCEPTED_BY_COURIER";
//		validateOrderTrackingCourierCreationStatus(tracking_number, CourierCreationStatus.NOT_INITIATED);

        String file_path=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type1,
                        activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        System.out.println(file_path);

        String activity_type2="OUT_FOR_PICKUP";
        String activity_sub_type2="";
        String file_path2=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type2,
                        activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        Assert.assertTrue(lms_returnHelper
                .validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.OUT_FOR_PICKUP.toString(),
                        15));

        String activity_type3="PICKUP_DONE";
        String activity_sub_type3="";
        String file_path3=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type3,
                        activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        System.out.println(file_path3);
        Assert.assertTrue(lms_returnHelper
                .validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.PICKUP_DONE.toString(),
                        15));
        Assert.assertTrue(lms_returnHelper
                .validateReturnShipmentStatusInLMS(returnId,ShipmentStatus.PICKUP_DONE.toString(),
                        15));

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");
        String pickupId=lms_returnHelper.getPickupIdOfReturn(returnId);


        performClosedBoxReturnQC(returnId,status,courier_code);
    }


    public void failedPickupClosedBox(String returnId,ItemQCStatus status,String receivedAt)
            throws IOException, JAXBException, InterruptedException {

        String tracking_number=getPickupTrackingNoFromSourceReturnId(
                String.valueOf(returnId),TENANTID,CLIENTID);

        String courier_code="DE";
        String shipment_type="PU";
        String activity_type1=EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED;
        String activity_sub_type1="SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type1,
                        activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        System.out.println(file_path);

        String activity_type2=EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP;
        String activity_sub_type2="";
        String file_path2=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type2,
                        activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        Assert.assertTrue(lms_returnHelper
                .validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.OUT_FOR_PICKUP.toString(),
                        15));

        String activity_type3=EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP;
        String activity_sub_type3="";
        String file_path3=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type3,
                        activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        System.out.println(file_path3);
        Assert.assertTrue(lms_returnHelper
                .validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.FAILED_PICKUP.toString(),
                        15));
        Assert.assertTrue(lms_returnHelper
                .validateReturnShipmentStatusInLMS(returnId,ShipmentStatus.FAILED_PICKUP.toString(),
                        15));

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");
        String pickupId=lms_returnHelper.getPickupIdOfReturn(returnId);

        Assert.assertTrue(
                lms_returnHelper.validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.FAILED_PICKUP.toString(),
                        15));

        lms_returnHelper.performClosedBoxReturnQC(returnId,status,courier_code);
    }

    public void PickupSuccessfulApproveRejectClosedBox(String returnId,ItemQCStatus status,String approveReject,String receivedAt)
            throws IOException, JAXBException, InterruptedException,
            ManagerException {

        String tracking_number=getPickupTrackingNoFromSourceReturnId(
                String.valueOf(returnId),TENANTID,CLIENTID);

        String courier_code="DE";
        String shipment_type="PU";
        String activity_type1="PICKUP_DETAILS_UPDATED";
        String activity_sub_type1="SHIPMENT_ACCEPTED_BY_COURIER";
        lms_returnHelper
                .validateOrderTrackingCourierCreationStatus(tracking_number,CourierCreationStatus.NOT_INITIATED);

        String file_path=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type1,
                        activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);


        String activity_type2="OUT_FOR_PICKUP";
        String activity_sub_type2="";
        String file_path2=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type2,
                        activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        Assert.assertTrue(
                lms_returnHelper.validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.OUT_FOR_PICKUP.toString(),
                        15));

        String activity_type3="PICKUP_DONE";
        String activity_sub_type3="";
        String file_path3=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type3,
                        activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        Assert.assertTrue(lms_returnHelper
                .validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.PICKUP_DONE.toString(),
                        15));
        Assert.assertTrue(lms_returnHelper
                .validateReturnShipmentStatusInLMS(returnId,ShipmentStatus.PICKUP_DONE.toString(),
                        15));

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");
        String pickupId=lms_returnHelper.getPickupIdOfReturn(returnId);


        performClosedBoxReturnQC(returnId,status,courier_code);
        //Just to process the remaining returns in the pickup randomly , we mark the even return ids on_hold and odd ones shortage
        lms_returnHelper.returnApproveOrRejectClosedBox(returnId,approveReject);
    }

    public void uploadPickupStatus(String returnId,String activityType,String activitySubType)
            throws IOException, JAXBException, InterruptedException {
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        String tracking_number=getPickupTrackingNoFromSourceReturnId(
                String.valueOf(returnId),TENANTID,CLIENTID);

        String courier_code="DE";
        String shipment_type="PU";

        String file_path=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activityType,
                        activitySubType);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        System.out.println(file_path);

    }

    public void uploadPickupStatusSuccessfully(String returnId)
            throws ManagerException, IOException, JAXBException, InterruptedException {
        String tracking_number=getPickupTrackingNoFromSourceReturnId(
                String.valueOf(returnId),TENANTID,CLIENTID);

        String courier_code="DE";
        String shipment_type="PU";
        String activity_type1="PICKUP_DETAILS_UPDATED";
        String activity_sub_type1="SHIPMENT_ACCEPTED_BY_COURIER";
        lms_returnHelper
                .validateOrderTrackingCourierCreationStatus(tracking_number,CourierCreationStatus.NOT_INITIATED);

        String file_path=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type1,
                        activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);


        String activity_type2="OUT_FOR_PICKUP";
        String activity_sub_type2="";
        String file_path2=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type2,
                        activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        Assert.assertTrue(lms_returnHelper
                .validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.OUT_FOR_PICKUP.toString(),
                        15));

        String activity_type3="PICKUP_DONE";
        String activity_sub_type3="";
        String file_path3=lms_returnHelper
                .generateClosedBoxStatusCSV(tracking_number,courier_code,shipment_type,activity_type3,
                        activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        Assert.assertTrue(lms_returnHelper
                .validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.PICKUP_DONE.toString(),
                        15));
        Assert.assertTrue(lms_returnHelper
                .validateReturnShipmentStatusInLMS(returnId,ShipmentStatus.PICKUP_DONE.toString(),
                        15));

    }

    public String updateReturnReceiveEventsByTrackingNo(String trackingNo,String courier_code) throws IOException {

        ShipmentUpdate.ShipmentUpdateBuilder pickupBuilder=
                new ShipmentUpdate.ShipmentUpdateBuilder(courier_code,trackingNo,
                        ShipmentUpdateEvent.RECEIVE);
        pickupBuilder.clientId(LMS_CONSTANTS.CLIENTID);
        pickupBuilder.tenantId(LMS_CONSTANTS.TENANTID);
        pickupBuilder.eventLocation("Myntra");
        pickupBuilder.eventTime(new DateTime());
        pickupBuilder.shipmentType(ShipmentType.PU);
        pickupBuilder.shipmentUpdateActivitySource(ShipmentUpdateActivityTypeSource.LogisticsPlatform);
        pickupBuilder.userName(Context.getContextInfo().getLoginId());
        pickupBuilder.remarks("Pickup Received at Returns Processing Center");
        pickupBuilder.eventLocationPremise(new Premise("5",DELIVERY_CENTER));
        ShipmentUpdate updatePickupStatus=pickupBuilder.build();
        String payload=APIUtilities.getObjectToJSON(updatePickupStatus);

        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_PICKUP_STATUS_EVENT,null,
                SERVICE_TYPE.LMS_SVC.toString(),HTTPMethods.PUT,payload,
                Headers
                        .getLmsHeaderJSON());
        final String statusMessage=APIUtilities.getElement(service.getResponseBody(),"status.statusMessage","json");
        return statusMessage;

    }

    public String updateReturnReceiveEventsByPremise(String returnId,String courier_code,Premise premise)
            throws IOException, InterruptedException {

        String trackingNo=getPickupTrackingNoFromSourceReturnId(
                String.valueOf(returnId),TENANTID,CLIENTID);
        ShipmentUpdate.ShipmentUpdateBuilder pickupBuilder=
                new ShipmentUpdate.ShipmentUpdateBuilder(courier_code,trackingNo,
                        ShipmentUpdateEvent.RECEIVE);
        pickupBuilder.clientId(LMS_CONSTANTS.CLIENTID);
        pickupBuilder.tenantId(LMS_CONSTANTS.TENANTID);
        pickupBuilder.eventLocation("Myntra");
        pickupBuilder.eventTime(new DateTime());
        pickupBuilder.shipmentType(ShipmentType.PU);
        pickupBuilder.shipmentUpdateActivitySource(ShipmentUpdateActivityTypeSource.LogisticsPlatform);
        pickupBuilder.userName(Context.getContextInfo().getLoginId());
        pickupBuilder.remarks("Pickup Received at Returns Processing Center");
        pickupBuilder.eventLocationPremise(premise);
        ShipmentUpdate updatePickupStatus=pickupBuilder.build();
        String payload=APIUtilities.getObjectToJSON(updatePickupStatus);

        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_PICKUP_STATUS_EVENT,null,
                SERVICE_TYPE.LMS_SVC.toString(),HTTPMethods.PUT,payload,
                Headers
                        .getLmsHeaderJSON());

        final String statusMessage=APIUtilities.getElement(service.getResponseBody(),"status.statusMessage","json");
        return statusMessage;

    }

    public String getReturnTrackingNo(String returnId) throws IOException {
        final ReturnResponse returnDetailsNew=rmsServiceHelper.getReturnDetailsNew(returnId);
        final String trackingNo=returnDetailsNew.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        return trackingNo;
    }


    // @Step("Mark close box return as QC fail ON_HOLD or SHORTAGE")
    public String performClosedBoxReturnQC(String returnId,ItemQCStatus status,String courier_code) throws IOException {
        //Find the pickup id
        String pickupId=lms_returnHelper.getPickupIdOfReturn(returnId);
        String tracking_number=lms_returnHelper.getPickupTrackingNoOfReturn(returnId);

        List <String> returnList=lms_returnHelper.getReturnsInPickup(pickupId);
        //We have to process all the returns in the pickup, the one passed as a parameter will be processed for the status which is passed, rest of the returns will be randomly processed
        returnList.remove(returnId);

        Map <String, ItemQCStatus> returnStatusMap=new HashMap <>();
        for (String remainingReturn : returnList) {
            if (status.toString().equalsIgnoreCase(ItemQCStatus.ON_HOLD.toString())) {
                returnStatusMap.put(remainingReturn,ItemQCStatus.ON_HOLD);

            } else if (status.toString().equalsIgnoreCase(ItemQCStatus.SHORTAGE.toString())) {
                returnStatusMap.put(remainingReturn,ItemQCStatus.SHORTAGE);

            }
        }
        ArrayList <ReturnShipmentUpdate> returnShipmentUpdates=new ArrayList <>();

        ReturnShipmentUpdate returnShipmentUpdate1=new ReturnShipmentUpdate();
        returnShipmentUpdate1.setSourceReturnId(returnId);
        returnShipmentUpdate1.setRemark("Remark");
        returnShipmentUpdate1.setItemQCStatus(status);
        returnShipmentUpdates.add(returnShipmentUpdate1);

        for (Map.Entry <String, ItemQCStatus> entry : returnStatusMap.entrySet()) {
            ReturnShipmentUpdate returnShipmentUpdate=new ReturnShipmentUpdate();
            returnShipmentUpdate.setSourceReturnId(entry.getKey());
            returnShipmentUpdate.setRemark("Remark");
            returnShipmentUpdate.setItemQCStatus(entry.getValue());
            returnShipmentUpdates.add(returnShipmentUpdate);
        }


        ClosedBoxPickupQCCompleteUpdate closedBoxPickupQCCompleteUpdate;
        closedBoxPickupQCCompleteUpdate=new ClosedBoxPickupQCCompleteUpdate(returnId,TENANTID,CLIENTID,
                courier_code,tracking_number,ShipmentUpdateEvent.QUALITY_CHECK_COMPLETE,null,5L,
                new Premise("5",Premise.PremiseType.DELIVERY_CENTER),"Location",new DateTime(),"remarks",
                ShipmentType.PU,ShipmentUpdateActivityTypeSource.LogisticsPortal,"Gloria",null,returnShipmentUpdates,
                null,null,null,null,null,null,null);
        String payload=APIUtilities.getObjectToJSON(closedBoxPickupQCCompleteUpdate);

        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_CLOSEDBOX_QC_STATUS,null,
                SERVICE_TYPE.LMS_SVC.toString(),HTTPMethods.POST,payload,Headers.getLmsHeaderJSON());
        String response=APIUtilities.getElement(service.getResponseBody(),"status.statusMessage","json");
        return response;
    }

    public void unableToPerformClosedBoxReturnQC(String returnId,ItemQCStatus status,String courier_code)
            throws IOException {
        String trackingNo=getPickupTrackingNoFromSourceReturnId(
                String.valueOf(returnId),TENANTID,CLIENTID);

        ArrayList <ReturnShipmentUpdate> returnShipmentUpdates=new ArrayList <>();


        ReturnShipmentUpdate returnShipmentUpdate=new ReturnShipmentUpdate();
        returnShipmentUpdate.setSourceReturnId(returnId);
        returnShipmentUpdate.setRemark("Remark");
        returnShipmentUpdate.setItemQCStatus(status);
        returnShipmentUpdates.add(returnShipmentUpdate);


        ClosedBoxPickupQCCompleteUpdate closedBoxPickupQCCompleteUpdate=
                new ClosedBoxPickupQCCompleteUpdate(returnId,TENANTID,CLIENTID,
                        courier_code,trackingNo,
                        ShipmentUpdateEvent.QUALITY_CHECK_COMPLETE,null,5L,
                        new Premise("5",Premise.PremiseType.DELIVERY_CENTER),
                        "Location",new DateTime(),
                        "remarks",ShipmentType.PU,
                        ShipmentUpdateActivityTypeSource.LogisticsPortal,"Gloria",
                        null,returnShipmentUpdates,null,null,null,null,null,null,
                        null);
        String payload=APIUtilities.getObjectToJSON(closedBoxPickupQCCompleteUpdate);

        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_CLOSEDBOX_QC_STATUS,null,
                SERVICE_TYPE.LMS_SVC.toString(),HTTPMethods.POST,payload,
                Headers.getLmsHeaderJSON());
        final String statusType=APIUtilities.getElement(service.getResponseBody(),"status.statusType","json");
        Assert.assertNotEquals(statusType,"SUCCESS","qc update failed.");

    }

    public void performClosedBoxReturnQCByReturnTrackingNo(String returnId,ItemQCStatus status,String courier_code)
            throws IOException {
        String trackingNo=getReturnTrackingNo(returnId);

        ArrayList <ReturnShipmentUpdate> returnShipmentUpdates=new ArrayList <>();


        ReturnShipmentUpdate returnShipmentUpdate=new ReturnShipmentUpdate();
        returnShipmentUpdate.setSourceReturnId(returnId);
        returnShipmentUpdate.setRemark("Remark");
        returnShipmentUpdate.setItemQCStatus(status);
        returnShipmentUpdates.add(returnShipmentUpdate);


        ClosedBoxPickupQCCompleteUpdate closedBoxPickupQCCompleteUpdate=
                new ClosedBoxPickupQCCompleteUpdate(returnId,TENANTID,CLIENTID,
                        courier_code,trackingNo,
                        ShipmentUpdateEvent.QUALITY_CHECK_COMPLETE,null,5L,
                        new Premise("5",Premise.PremiseType.DELIVERY_CENTER),
                        "Location",new DateTime(),
                        "remarks",ShipmentType.PU,
                        ShipmentUpdateActivityTypeSource.LogisticsPortal,"Gloria",
                        null,returnShipmentUpdates,null,null,null,null,null,null,
                        null);
        String payload=APIUtilities.getObjectToJSON(closedBoxPickupQCCompleteUpdate);

        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.UPDATE_CLOSEDBOX_QC_STATUS,null,
                SERVICE_TYPE.LMS_SVC.toString(),HTTPMethods.POST,payload,
                Headers.getLmsHeaderJSON());
        final String statusType=APIUtilities.getElement(service.getResponseBody(),"status.statusType","json");
        Assert.assertNotEquals(statusType,"SUCCESS","qc update failed.");

    }


    public void uploadSignature(String returnId) throws JAXBException, IOException, InterruptedException {
        String tracking_number=getPickupTrackingNoFromSourceReturnId(
                String.valueOf(returnId),TENANTID,CLIENTID);
        EmptyResponse emptyResponse=lmsServiceHelper.uploadSignature(tracking_number);
        Assert.assertEquals(emptyResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Signature not Uploaded properly");

    }

    /**
     * approveAtCC
     *
     * @param returnId
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void approveAtCC(String returnId)
            throws IOException, JAXBException {
        lmsServiceHelper.approveOrRejectAtCC(returnId,EnumSCM.APPROVED);
        Assert.assertEquals(lmsHelper.getReturnApprovalStatus(returnId),EnumSCM.APPROVED,
                "Approval status is not APPROVED");
    }

    /**
     * rejectAtCC
     *
     * @param returnId
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     */
    public void rejectAtCC(String returnId)
            throws IOException, JAXBException {
        lmsServiceHelper.approveOrRejectAtCC(returnId,EnumSCM.REJECTED);
        Assert.assertEquals(lmsHelper.getReturnApprovalStatus(returnId),EnumSCM.REJECTED,
                "Approval status is not APPROVED");
    }

    public static String findPickupFromSourceReturnId(String returnId,String tenantId,Long sourceId) throws UnsupportedEncodingException {
        String FGPathparam=null;
        FGPathparam="?sourceReturnId="+returnId+"&tenantId="+tenantId+"&sourceId="+sourceId;
        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.PICKUP_SHIPMENT_STATUS_LMS,
                new String[] {FGPathparam},SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET,null,Headers.getLmsHeaderJSON());
        Assert.assertEquals(APIUtilities.getElement(service.getResponseBody(),"status.statusMessage","json"),
                ResponseMessageConstants.pickupShipmentRetrieved,"pickup Shipment is not Retrieved successfully ");

        return APIUtilities.getElement(service.getResponseBody(),"data.shipmentStatus","json");
    }

    public String getPickupTrackingNoFromSourceReturnId(String returnId,String tenantId,String sourceId) throws UnsupportedEncodingException {
        FGPathparam="?sourceReturnId="+returnId+"&tenantId="+tenantId+"&sourceId="+sourceId;
        Svc service=HttpExecutorService.executeHttpService(Constants.LMS_PATH.PICKUP_SHIPMENT_STATUS_LMS,
                new String[] {FGPathparam},SERVICE_TYPE.LMS_SVC.toString(),
                HTTPMethods.GET,null,Headers.getLmsHeaderJSON());
        Assert.assertEquals(APIUtilities.getElement(service.getResponseBody(),"status.statusMessage","json"),
                "Pickup Shipment retrieved successfully.","pickup Shipment is not Retrieved successfully ");
        return APIUtilities.getElement(service.getResponseBody(),"data.trackingNumber","json");
    }


}
