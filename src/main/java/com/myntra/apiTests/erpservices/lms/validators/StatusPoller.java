package com.myntra.apiTests.erpservices.lms.validators;

import com.myntra.apiTests.erpservices.LMSCancellation.validators.OrderValidator;
import com.myntra.apiTests.erpservices.lastmile.client.TripClient;
import com.myntra.apiTests.common.Utils.csvutility.CSVReader;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileTripPlanningSearchHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileTripResultSearchHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.StoreClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.LMSOrderDetailsClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.DeliveryCenterClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.LastmileServiceHelper;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripOrderAssignmentClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Flipkart.FlipkartHelper;
import com.myntra.apiTests.erpservices.lms.Flipkart.FlipkartStateHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.OrderUpdateTabHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.tools.ToolsHelper;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public interface StatusPoller {
    Long MAXWAIT_ML_SHIPMENT = 2l; //Minutes
    Long MAXWAIT_ORDER_TO_SHIP = 2l;
    Long MAXWAIT_RETURN_SHIPMENT = 2l;
    Long MAXWAIT_PICKUP_SHIPMENT = 2l;
    Long MAXWAIT_SHIPMENT = 2l;
    Long MAXWAIT_TRIP_STATUS = 2l;
    Long MAXWAIT_TRIPORDER_STATUS = 2l;
    Long MAXWAIT_RETURN_STATUS = 2l;
    Long POLLING_INTERVAL = 2000l; //Seconds
    MasterBagClient masterBagClient = new MasterBagClient(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMSClient lmsClient = new LMSClient();
    RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    LastmileClient lastmileClient = new LastmileClient();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripClient tripClient = new TripClient(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    StoreHelper storeHelper=new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LastmileHelper lastmileHelper=new LastmileHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    OMSServiceHelper omsServiceHelper=new OMSServiceHelper();
    LMSHelper lmsHelper=new LMSHelper();
    StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    CSVReader csvReader= new CSVReader();
    StoreValidator storeValidator=new StoreValidator();
    MasterBagServiceHelper masterBagServiceHelper =new MasterBagServiceHelper();
    TMSServiceHelper tmsServiceHelper=new TMSServiceHelper();
    TripClient_QA tripClient_qa=new TripClient_QA();
    OrderUpdateTabHelper orderUpdateTabHelper=new OrderUpdateTabHelper();
    LMSOperations lmsOperations=new LMSOperations();
    LastmileTripPlanningSearchHelper lastmileTripPlanningSearchHelper = new LastmileTripPlanningSearchHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LastmileServiceHelper lastmileServiceHelper=new LastmileServiceHelper();
    LastmileTripResultSearchHelper lastmileTripResultSearchHelper = new LastmileTripResultSearchHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa = new TripOrderAssignmentClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa = new DeliveryCenterClient_QA();
    LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    OrderEntry orderEntry = new OrderEntry();
    LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    ReturnHelper returnHelper=new ReturnHelper();
    OrderTabSearchValidator orderTabSearchValidator=new OrderTabSearchValidator();
    TMSOperations tmsOperations=new TMSOperations();
    LastmileOperations lastmileOperations=new LastmileOperations();
    FlipkartHelper flipkartHelper=new FlipkartHelper();
    FlipkartStateHelper flipkartStateHelper=new FlipkartStateHelper();
    OrderValidator orderValidator = new OrderValidator();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa = new LMSOrderDetailsClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();

}
