package com.myntra.apiTests.erpservices.lms.Constants;

public class FileUploadConstants {
    public static final String sortConfigFileName="./Data/lms/csv/sortConfig.csv";
    public static final String sortLocationFileName="./Data/lms/csv/sortLocation.csv";
    public static final String bulkUploadCancellation="src/test/resources/scm/lms/rules/BulkUploadCancellation.csv";
    public static final String bulkOrderUpload = "src/test/resources/scm/lms/rules/BulkOrderUploadRules.csv";
}
