package com.myntra.apiTests.erpservices.lms.validators;


import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.response.TripResponse;
import org.testng.Assert;

public class CreateTripValidator {
    TripResponse tripResponse=null;

    public CreateTripValidator(TripResponse tripResponse) {
        this.tripResponse = tripResponse;
    }

    public void validateCreateTripStatus(int statusCode,String statusMessage,String statusType){

        Assert.assertEquals(tripResponse.getStatus().getStatusCode(),statusCode,"Status Code mismatch");
        Assert.assertEquals(tripResponse.getStatus().getStatusMessage(), statusMessage,"Trip massage not correct");
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(),statusType,"Trip creation Status type is not correct");

    }
    public  void validateCreatedByCreatedOn(String createdBy, String createdOn,String lastModifiedOn){
        Assert.assertEquals(tripResponse.getTrips().get(0).getCreatedBy(),createdBy,"Created by incorrect");
        Assert.assertEquals(tripResponse.getTrips().get(0).getCreatedOn().toString(),createdOn,"CreatedOn not correct");
        Assert.assertEquals(tripResponse.getTrips().get(0).getLastModifiedOn().toString(),lastModifiedOn,"LastmodifiedOn not correct");
    }
    public void validateTripIdTripNumber(String tripId,String tripNumber){
        Assert.assertEquals(tripResponse.getTrips().get(0).getId().toString(),tripId,"Trip Id mismatch");
        Assert.assertEquals(tripResponse.getTrips().get(0).getTripNumber(),tripNumber);
    }
    public void  validateDeliveryCenteridDeliveryStaffId(String deliveryCenterId,String deliveryStaffId){
        Assert.assertEquals(tripResponse.getTrips().get(0).getDeliveryStaffId().toString(),deliveryStaffId,"Delivery Staff mismatch");
        Assert.assertEquals(tripResponse.getTrips().get(0).getDeliveryCenterId().toString(),deliveryCenterId,"Delivery center id mismatch");
    }
    public void validateTripStatusTripDate(TripStatus tripStatus, String tripDate){
        Assert.assertEquals(tripResponse.getTrips().get(0).getTripStatus(),tripStatus,"Trip status Mismatch");
        Assert.assertEquals(tripResponse.getTrips().get(0).getTripDate().toString(),tripDate,"Trip date mismatch");
    }

    public void validateIsCompleteIsDeletedIsInbound(String isComplete,String isDeleted,String isInbound){
        Assert.assertEquals(tripResponse.getTrips().get(0).getIsComplete().toString(),isComplete," Is Trip complete not proper");
        Assert.assertEquals(tripResponse.getTrips().get(0).getIsDeleted().toString(),isDeleted,"Trip isDeleted status not proper");
        Assert.assertEquals(tripResponse.getTrips().get(0).getIsInbound().toString(),isInbound,"Trip isInboud status not proper");

    }
    public void validateOdometerReading(long startOdometerReading,long endOdometerReading){
        Assert.assertEquals(tripResponse.getTrips().get(0).getStartOdometerReading(),Long.valueOf(startOdometerReading),"Start Odometer Reading not correct");
        Assert.assertEquals(tripResponse.getTrips().get(0).getEndOdometerReading(),Long.valueOf(endOdometerReading),"End odometer reading not matching");
    }
    public void validateVersionCardinalityIsCardEnabled(String version,String cardinality, String isCardEnabled){
        Assert.assertEquals(tripResponse.getTrips().get(0).getVersion().toString(),version,"Version mis-match");
        Assert.assertEquals(tripResponse.getTrips().get(0).getIsCardEnabled().toString(),isCardEnabled,"Is card enabled status not correct");
        Assert.assertEquals(tripResponse.getTrips().get(0).getCardinality().toString(),cardinality,"Cardinality status not correct");
    }
    public void validateTenantId(String tenantId){
        Assert.assertEquals(tripResponse.getTrips().get(0).getTenantId(),tenantId,"TenantId not correct");
    }

}
