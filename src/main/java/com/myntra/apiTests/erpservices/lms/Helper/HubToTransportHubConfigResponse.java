package com.myntra.apiTests.erpservices.lms.Helper;

import com.myntra.commons.response.AbstractResponse;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(
        name = "hubToTransportHubConfigResponse"
)
public class HubToTransportHubConfigResponse extends AbstractResponse {
    private List<HubToTransportHubConfigEntry> hubToTransportHubConfigEntries;
    private int addedCount;
    private int modifiedCount;
    private int unchangedCount;

    public HubToTransportHubConfigResponse() {
    }

    public int getAddedCount() {
        return this.addedCount;
    }

    public void setAddedCount(int addedCount) {
        this.addedCount = addedCount;
    }

    public int getModifiedCount() {
        return this.modifiedCount;
    }

    public void setModifiedCount(int modifiedCount) {
        this.modifiedCount = modifiedCount;
    }

    public int getUnchangedCount() {
        return this.unchangedCount;
    }

    public void setUnchangedCount(int unchangedCount) {
        this.unchangedCount = unchangedCount;
    }

    @XmlElementWrapper(
            name = "data"
    )
    @XmlElement(
            name = "hubToTransportHubConfig"
    )
    public List<HubToTransportHubConfigEntry> getHubToTransportHubConfigEntries() {
        return this.hubToTransportHubConfigEntries;
    }

    public void setHubToTransportHubConfigEntries(List<HubToTransportHubConfigEntry> hubToTransportHubConfigEntries) {
        this.hubToTransportHubConfigEntries = hubToTransportHubConfigEntries;
    }
}