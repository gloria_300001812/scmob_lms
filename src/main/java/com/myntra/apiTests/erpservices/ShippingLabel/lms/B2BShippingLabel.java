package com.myntra.apiTests.erpservices.ShippingLabel.lms;

import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class B2BShippingLabel {
    public Map<String, String> b2bShippingLabelData(String orderId, String tenantId, String clientId) throws IOException, TikaException, SAXException {
        Map<String, String> map = new HashMap<>();
        LabelParsers labelParsers = new LabelParsers();
        String praseLabel = labelParsers.parseB2BShippingLabel(orderId, tenantId, clientId);
        String strReplace = praseLabel.trim().replace("\n", " ");
        String[] strFinal1 = strReplace.split(" ");
        map.put("sourceHub", strFinal1[0]);
        map.put("destinationHub", strFinal1[1]);
        String[] strFinal2 = strReplace.split("Shipment ID  : ");
        String[] strFinal3 = strFinal2[1].split(" ");
        map.put("ShipmentID", strFinal3[0]);
        return map;
    }
}
