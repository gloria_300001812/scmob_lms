package com.myntra.apiTests.erpservices.ShippingLabel.lms;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;

public class LabelParsers {

    public String parseShipmentLabel(String trackingNumber, String tenantId, String clientId) throws TikaException, SAXException, IOException {
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpGet httpget = null;
        String specialSpace = " ";
        String pathParam = MessageFormat.format("{0}?tenantId={1}&clientId={2}",trackingNumber,tenantId,clientId);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.SHIPPING_LABEL_PDF, new String[] {pathParam}, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.GET, null, Headers.getMultipartTypeHeader());
        httpget = new HttpGet("http://"+service.getIp()+service.getPath());
        httpget.addHeader("Authorization", "Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==");
        httpget.addHeader("Content-Type", "multipart/form-data");
        HttpResponse response = httpclient.execute(httpget);
        HttpEntity resEntity = response.getEntity();
        InputStream is = resEntity.getContent();
        ParseContext pcontext = new ParseContext();

        //parsing the document using PDF parser
        PDFParser pdfparser = new PDFParser();
        BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        pdfparser.parse(is, handler, metadata,pcontext);

        String pdfData=handler.toString().replaceAll(specialSpace, " ");
        System.out.println(pdfData);
        return pdfData;
    }

    public String parseShippingLabelV2(String orderID, String tenantId, String clientId) throws TikaException, SAXException, IOException {
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpGet httpget = null;
        String specialSpace = " ";
        String pathParam = MessageFormat.format("{0}?tenantId={1}&clientId={2}",orderID,tenantId,clientId);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.SHIPPING_LABELV2_PDF, new String[] {pathParam}, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.GET, null, Headers.getMultipartTypeHeader());
        httpget = new HttpGet("http://"+service.getIp()+service.getPath());
        httpget.addHeader("Authorization", "Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==");
        httpget.addHeader("Content-Type", "multipart/form-data");
        HttpResponse response = httpclient.execute(httpget);
        HttpEntity resEntity = response.getEntity();
        InputStream is = resEntity.getContent();
        ParseContext pcontext = new ParseContext();

        //parsing the document using PDF parser
        PDFParser pdfparser = new PDFParser();
        BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        pdfparser.parse(is, handler, metadata,pcontext);

        String pdfData=handler.toString().replaceAll(specialSpace, " ");
        System.out.println(pdfData);
        return pdfData;
    }

    public String parseB2BShippingLabel(String orderID, String tenantId, String clientId) throws TikaException, SAXException, IOException {
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpGet httpget = null;
        String specialSpace = " ";
        String pathParam = MessageFormat.format("?shipmentId={0}&clientId={1}&tenantId={2}",orderID,clientId,tenantId);
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.B2B_SHIPPING_LABEL, new String[] {pathParam}, SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.GET, null, Headers.getMultipartTypeHeader());
        httpget = new HttpGet("http://"+service.getIp()+service.getPath());
        httpget.addHeader("Authorization", "Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==");
        httpget.addHeader("Content-Type", "multipart/form-data");
        HttpResponse response = httpclient.execute(httpget);
        HttpEntity resEntity = response.getEntity();
        InputStream is = resEntity.getContent();
        ParseContext pcontext = new ParseContext();

        //parsing the document using PDF parser
        PDFParser pdfparser = new PDFParser();
        BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        pdfparser.parse(is, handler, metadata,pcontext);

        String pdfData=handler.toString().replaceAll(specialSpace, " ");
        System.out.println(pdfData);
        return pdfData;
    }
}
