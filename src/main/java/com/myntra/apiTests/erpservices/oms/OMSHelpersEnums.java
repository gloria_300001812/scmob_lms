package com.myntra.apiTests.erpservices.oms;

import com.myntra.returns.common.enums.code.ReturnLineStatus;

public interface OMSHelpersEnums {
	
	public enum ReadyToDispatchType{
		POSITIVE, //This is the positive behaviour, payload data will be taken from DB
		
		//These are negative and payload data will be modified during runtime
		DIFFERENT_SKU,
		MISSING_SKU,
		ZERO_QTY,
		GREATER_QTY,
		
		//These are negative behaviour and data will be modified in DB
		MISSING_COURIER, NEGATIVE,
		
	}
	
	//********** Packet Events Codes ****************
	public enum PacketEvents{
		MARK_PACKET_LOST,
		MARK_PACKET_RTO,
		MARK_PACKET_SHIPPED,
		MARK_PACKET_DELIVERED,
		CANCEL_PACKET,
		MARK_PACKET_READY_TO_DISPATCH,
		MARK_PACKET,
		LOST_PACKET
	}
	
	//**************Types of Orders***************
	public enum OrderType{
		ORDER_RELEASE,ORDER_LINE,PACKET,ORDER
	}
	
	//**************Types of ReasonCode and Description***************
	public enum ReasonCodeDescription{
		
		COURIER_INFO_NOT_FOUND(32L,"Courier info could not be found from LMS for this release"),
		PINCODE_NOT_SERVICABLE(31L,"Pincode not serviceable"),
		SKU_OOS(34L,"SKU in this release is out of stock"),
		COD_LIMIT_EXCEEDED(35L,"Cod Amount Limit Exceeded"),
		DUPLICATE_ORDER(37L,"Potential Duplicate Order. Some items of this order is already part of a previous order placed in last 24 hours."),
		PAYMENT_VERIFICATION_PENDING(39L,"On hold pending confirmation from payments"),
		FRAUD_USER(2L,"For investigating fraud in an order"),
		ORDER_PROCESSING_ERROR(4L,"ORDER_PROCESSING_ERROR, While placing the order, stock information was not available"),
		RELEASE_PROCESSING_ERROR(27L,"RELEASE_PROCESSING_ERROR, While placing the order, stock information was not available");
		
		Long reasonId;
		String description;
		
		private ReasonCodeDescription(Long reasonId,String description){
			this.reasonId = reasonId;
			this.description = description;
		}
		
		public static String getReasonCodeDescriptionFromId(Long reasonId){
			for(ReasonCodeDescription rcd:ReasonCodeDescription.values()){
				if(rcd.getReasonId().equals(reasonId)){
					return rcd.getDescription().toString();
				}
			}
			
			return null;
		}
		
		public static String getReasonCodeFromDescription(String description){
			for(ReasonCodeDescription rcd:ReasonCodeDescription.values()){
				if(rcd.getDescription().equalsIgnoreCase(description)){
					return rcd.getDescription().toString();
				}
			}
			
			return null;
		}
		
    	private String getDescription(){
    		return this.description;
    	}
    	
    	private Long getReasonId(){
    		return this.reasonId;
    	}

	}
	
	//*******************AddressType**************
	public enum AdrressEntryType{
		NEW_ADDRESS,
		OLD_ADDRESS
	}
	
	//****************New Address Data*************
	public enum NewAddressData{
		FIRST_NAME("chandra"),
		LAST_NAME("shekhar"),
		CITY("Bangalore"),
		PINCODE("560068"),
		COUNTRY("India"),
		STATE_CODE("KA"),
		MOBILE("1234567890"),
		EMAIL("addresschange@myntra.com"),
		LOCALITY("New Locality"),
		STATE("KA"),
		NEW_ADDRESS("new address"),
		ADDRESSID("6145700");
		
		String description;
		
		private NewAddressData(String description) {
			// TODO Auto-generated constructor stub
			this.description = description;
		}
		
		public String getValue() {
			return this.description;
		}
	}
	
	//****************Refund cases*************
	public enum RefundType{
		POSITIVE,
		NEGATIVE
	}
	
	public enum ReassignType{
		POSITIVE,
		INVALID_SKU,
		INVALID_QUANTITY,
		MISSING_ACTION_CODE
	}

	public enum MTTenantVariables{
		OWNER_ID("{ownerId}"),
		BUYER_ID("{buyerId}");
		String variable;

		private MTTenantVariables(String variable){
			this.variable = variable;
		}

		public String getValue() {
			return this.variable;
		}

	}

	public enum OrderStatus{
		PP,PV,Decline,Cancelled,RFR,WP,Q,PK,Shipped,Delivered,RTO,LOST,Completed
	}

	public enum PackmanActionCode{
		QCPASS(61L),
		QCFAIL(1L);
		private Long reasonId;

		private PackmanActionCode(Long reasonId){
			this.reasonId = reasonId;
		}

		public Long getReasonId(){
			return this.reasonId;
		}
	}


	public enum ReturnCreationType{
		POSITIVE,
		INVALID_ORDERID,
		DIFFERENT_INVALID_SKU,
		DIFFERENT_VALID_SKU,
		INVALID_LINEID
	}

	public enum ReturnLineUpdateType{
		RETURN_QC_PASS("QC48","Q1","chandra",ReturnLineStatus.RRC,ReturnLineStatus.RQP,"abc"),
		RETURN_QC_FAIL("QC48","Q1","chandra",ReturnLineStatus.RRC,ReturnLineStatus.RQF,"abc");

		private String qcDesk;
		private String quality;
		private String createdBy;
		private ReturnLineStatus fromStatus;
		private ReturnLineStatus toStatus;
		private String qcReason;

		ReturnLineUpdateType(String qcDesk, String quality, String createdBy, ReturnLineStatus fromStatus, ReturnLineStatus toStatus,String qcReason) {
			this.qcDesk = qcDesk;
			this.quality = quality;
			this.createdBy = createdBy;
			this.fromStatus = fromStatus;
			this.toStatus = toStatus;
			this.qcReason = qcReason;
		}

		public String getQcDesk() {
			return qcDesk;
		}

		public void setQcDesk(String qcDesk) {
			this.qcDesk = qcDesk;
		}

		public String getQuality() {
			return quality;
		}

		public void setQuality(String quality) {
			this.quality = quality;
		}

		public String getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		public ReturnLineStatus getFromStatus() {
			return fromStatus;
		}

		public void setFromStatus(ReturnLineStatus fromStatus) {
			this.fromStatus = fromStatus;
		}

		public ReturnLineStatus getToStatus() {
			return toStatus;
		}

		public void setToStatus(ReturnLineStatus toStatus) {
			this.toStatus = toStatus;
		}

		public String getQcReason() {
			return qcReason;
		}

		public void setQcReason(String qcReason) {
			this.qcReason = qcReason;
		}
	}

	public enum ValidateResponseType{
		SUCCESS,
		FAILURE
	}

}
