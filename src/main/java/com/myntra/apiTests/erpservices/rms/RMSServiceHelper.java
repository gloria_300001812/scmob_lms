package com.myntra.apiTests.erpservices.rms;

import com.jayway.jsonpath.JsonPath;
import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.APINAME;

import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lms.Helper.LMSUtils;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.Premise;
import com.myntra.logistics.platform.domain.ShipmentUpdate;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.SlackMessenger;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.lordoftherings.gandalf.MyntraService;
import com.myntra.lordoftherings.gandalf.PayloadType;
import com.myntra.lordoftherings.gandalf.RequestGenerator;
import com.myntra.oms.client.entry.OrderEntry;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.CancellationType;
import com.myntra.returns.common.enums.PickupStatus;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnActionCode;
import com.myntra.returns.common.enums.code.ReturnLineStatus;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.entry.ReturnAddressDetailsEntry;
import com.myntra.returns.entry.ReturnEntry;
import com.myntra.returns.entry.ReturnLineEntry;
import com.myntra.returns.entry.ReturnLineUpdateRequestEntry;
import com.myntra.returns.entry.ReturnRefundDetailsEntry;
import com.myntra.returns.entry.ReturnTrackingDetailsEntry;
import com.myntra.returns.entry.ReturnUpdateRequestEntry;
import com.myntra.returns.entry.RtoEntry;
import com.myntra.returns.response.ReturnLineResponse;
import com.myntra.returns.response.ReturnResponse;

import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.testng.Assert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

public class RMSServiceHelper {
	
	private static Logger log = Logger.getLogger(OMSServiceHelper.class);
	static Initialize init = new Initialize("/Data/configuration");
	ReturnEntry returnEntry = new ReturnEntry();
	ReturnAddressDetailsEntry returnAddressDetailsEntry = new ReturnAddressDetailsEntry();
	ReturnLineEntry returnLineEntry = new ReturnLineEntry();
	List<ReturnLineEntry> returnlineEntries = new ArrayList<>();
	OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
	private OrderLineEntry orderLineEntry;
	private OrderEntry orderEntry;
	private OrderReleaseEntry orderReleaseEntry;
	Date date = new Date(System.currentTimeMillis());

	/**
	 * Get RMS Headers to Make API Call
	 *
	 * @return HashMap
	 */
	private static HashMap<String, String> getRMSHeader() {
		HashMap<String, String> createOrderHeaders = new HashMap<String, String>();
		createOrderHeaders.put("Authorization",
				"Basic bW9iaWxlfm1vYmlsZTptb2JpbGU");
		createOrderHeaders.put("Content-Type", "Application/json");
		createOrderHeaders.put("Accept", "Application/json");
		return createOrderHeaders;
	}

	/**
	 * Revamped create return with address as input for a Given Line ID
	 * @param lineID,returnType,returnMode,quantity,returnReasonID , refundMode , refundAccountId , address,addressId,pincode, city, state,country, mobileNumber
	 * @return {@link ReturnResponse}
	 * @throws JAXBException
	 * @throws IOException
	 */
	public ReturnResponse createReturn(Long lineID,ReturnType returnType,ReturnMode returnMode,int quantity, Long returnReasonID, RefundMode refundMode,String refundAccountId,String address,String addressId, String pincode,String city,String state,String country,String mobileNumber){

		OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
		OrderLineEntry orderLineEntry = omsServiceHelper.getOrderLineEntry(""+lineID);
		OrderEntry orderEntry = omsServiceHelper.getOrderEntry(orderLineEntry.getOrderId().toString());
		OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderEntry.getId().toString()));

		ReturnEntry returnEntry = new ReturnEntry();
		returnEntry.setOrderId(orderLineEntry.getOrderId());
		returnEntry.setReturnType(returnType);
		returnEntry.setReturnMode(returnMode);
		returnEntry.setStoreId(1);
		returnEntry.setLogin(orderEntry.getLogin());
		returnEntry.setCustomerName(orderReleaseEntry.getReceiverName());
		//returnEntry.setStoreOrderId(orderLineEntry.getStoreOrderId());
		returnEntry.setComment("Return Creation Entry For Line ID: "+ lineID + " Quantity : " + quantity);
		returnEntry.setEmail(orderReleaseEntry.getEmail());
		returnEntry.setMobile(mobileNumber);



		//Set ReturnAddressDetailsEntry
		ReturnAddressDetailsEntry returnAddressDetailsEntry = new ReturnAddressDetailsEntry();
		returnAddressDetailsEntry.setAddress(address);
		returnAddressDetailsEntry.setAddressId(Long.valueOf(addressId));
		returnAddressDetailsEntry.setCity(city);
		returnAddressDetailsEntry.setCountry(country);
		returnAddressDetailsEntry.setState(state);
		returnAddressDetailsEntry.setZipcode(pincode);
		returnEntry.setReturnAddressDetailsEntry(returnAddressDetailsEntry);

		//Set Return Line Entries
		ReturnLineEntry returnLineEntry = new ReturnLineEntry();
		returnLineEntry.setComment("Line Entry Comment For Line ID "+lineID);
		returnLineEntry.setOrderId(orderLineEntry.getOrderId());
		returnLineEntry.setOrderLineId(lineID);
		returnLineEntry.setOrderReleaseId(orderLineEntry.getOrderReleaseId());
		returnLineEntry.setQuantity(quantity);
		returnLineEntry.setSupplyType(orderLineEntry.getSupplyType());
		returnLineEntry.setReturnReasonId(returnReasonID);
		returnLineEntry.setSkuId(orderLineEntry.getSkuId());
		returnLineEntry.setOptionId(orderLineEntry.getOptionId());
		returnLineEntry.setStyleId(orderLineEntry.getStyleId());
		List<ReturnLineEntry> returnlineEntries = new ArrayList<>();
		returnlineEntries.add(returnLineEntry);
		returnEntry.setReturnLineEntries(returnlineEntries);

		ReturnRefundDetailsEntry returnRefundDetailsEntry = new ReturnRefundDetailsEntry();
		returnRefundDetailsEntry.setRefundMode(refundMode);
		//returnRefundDetailsEntry.setRefundAccountId("418");
		returnRefundDetailsEntry.setRefundAccountId(refundAccountId);
		returnEntry.setReturnRefundDetailsEntry(returnRefundDetailsEntry);

		//returnRefundDetailsEntry.setRefundAccountId();

		//ReturnTrackingDetailsEntry returnTrackingDetailsEntry = new ReturnTrackingDetailsEntry();
		//returnTrackingDetailsEntry.setCourierCode(courierCode);

		//returnEntry.setReturnTrackingDetailsEntry(returnTrackingDetailsEntry);
		String payLoad = null;
		try {
			payLoad = APIUtilities.getObjectToJSON(returnEntry);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Svc service = null;
		try {
			service = HttpExecutorService.executeHttpService(Constants.RMS_PATH.CREATE_RETURN, null, SERVICE_TYPE.RMS_SVC.toString(), HTTPMethods.POST, payLoad, getRMSHeader());
		} catch (UnsupportedEncodingException e) {
			Assert.fail("Test fail : Return creation failed due to UnsupportedEncodingException");
		}
		ReturnResponse returnResponse = null;
		try {
			returnResponse = (ReturnResponse) APIUtilities.getJsontoObject(service.getResponseBody(), new ReturnResponse());
		} catch (IOException e) {
			Assert.fail("Test fail : Return creation failed due to IOException");
		}
		return returnResponse;
	}
	/**
	 * Get Return Details for Return ID
	 *
	 * @param returnID
	 * @return {@link ReturnResponse}
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public ReturnResponse getReturnDetailsNew(String returnID) throws IOException {
		Svc service = HttpExecutorService.executeHttpService(Constants.RMS_PATH.GET_RETURN_DETAILS, new String[] { returnID }, SERVICE_TYPE.RMS_SVC.toString(), HTTPMethods.GET, null, getRMSHeader());
		ReturnResponse returnResponse =(ReturnResponse) APIUtilities.getJsontoObject(service.getResponseBody(), new ReturnResponse());
		return returnResponse;
	}


	/**
	 * validateReturnStatusInRMS
	 * @param returnId
	 * @param status
	 * @param delaytoCheck
	 * @return
	 */
	public boolean validateReturnStatusInRMS(String returnId, String status, int delaytoCheck) {
		log.info("Validate Order Status in LMS order_to_ship table");
		boolean validateStatus = false;
		try {
			for (int i = 0; i < delaytoCheck; i++) {
				String status_code = getReturnStatusFromRMS(returnId);
				if (status_code.equalsIgnoreCase(status) || status_code.equalsIgnoreCase(status)) {
					validateStatus = true;
					break;
				} else {
					Thread.sleep(4000);
					validateStatus = false;
				}

				log.info("waiting for Order Status in LMS" + status + " .current status=" + status_code + "\t " + i);
			}
		} catch (Exception e) {
			e.printStackTrace();
			validateStatus = false;
		}
		return validateStatus;
	}
	/**
	 * getReturnStatusFromRMS
	 * @param returnId
	 * @return
	 */
	public String getReturnStatusFromRMS(String returnId) {
		String status="false";
		try {
			List list = DBUtilities.exSelectQuery("select status_code from returns where id=" + returnId, "rms");
			if (list == null) {
				return "false";
			}
			Map<String, Object> hm = (Map<String, Object>) list.get(0);
			status = hm.get("status_code").toString();
		} catch (Exception e) {
			log.error("Error in getReturnStatusFromRMS :- " + e.getMessage());
			return "false";
		}
		return status;
	}
	/**
	 * WaitRefundStatus
	 * @param returnId
	 * @param delaytoCheck
	 * @return
	 */
	public boolean WaitRefundStatus(String returnId, int delaytoCheck) {
		log.info("Wait for Refund Status");
		boolean validateStatus = false;
		try {
			for (int i = 0; i < delaytoCheck; i++) {
				ReturnResponse returnResponse = getReturnDetailsNew(returnId);
				ReturnEntry returnEntry = returnResponse.getData().get(0);
				if (returnEntry.getReturnRefundDetailsEntry().getRefundPPSId()!=null & returnEntry.getReturnRefundDetailsEntry().getRefunded()== true){
					validateStatus = true;
					break;
				}else {
					Thread.sleep(5000);
					validateStatus = false;
				}

				log.info("waiting for Refund Status in RMS ,refund pps id=" + returnEntry.getReturnRefundDetailsEntry().getRefundPPSId()+"\t " + i);
			}
		} catch (Exception e) {
			e.printStackTrace();
			validateStatus = false;
		}
		return validateStatus;
	}

}


