package com.myntra.apiTests.erpservices.tools;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.client.tools.response.ApplicationPropertiesEntry;
import com.myntra.client.tools.response.ApplicationPropertiesResponse;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.codehaus.jettison.json.JSONException;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.HashMap;

/**
 * @author Shanmugam Yuvaraj
 * Since: Jul:2019
 */
public class ToolsHelper {

    /***
     * Used to get the search property by name
     * @param name
     * @return
     * @throws UnsupportedEncodingException
     * @throws JAXBException
     */
    public static ApplicationPropertiesResponse getApplicationProperty(String name) throws UnsupportedEncodingException, JAXBException {
        String pathParams = MessageFormat.format("?q=name.like:{0}&start=0&fetchSize=20&sortBy=id&sortOrder=DESC", name);
        Svc service = HttpExecutorService.executeHttpService(Constants.TOOLS_PATH.CANCELLATIONPROPERTY, new String[]{pathParams},
                SERVICE_TYPE.TOOLS_SVC.toString(), HTTPMethods.GET, null,getXMLHeader());
        ApplicationPropertiesResponse applicationPropertiesResponse = (ApplicationPropertiesResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
                new ApplicationPropertiesResponse());
        return applicationPropertiesResponse;
    }
    public static HashMap<String, String> getXMLHeader() {
        HashMap<String, String> createOrderHeaders = new HashMap<String, String>();
        createOrderHeaders.put("Authorization", "Basic bW9iaWxlfm1vYmlsZTptb2JpbGU");
        createOrderHeaders.put("Content-Type", "Application/xml");
        return createOrderHeaders;
    }
    /**
     * Used to get the Search property by ID
     *
     * @param propertyId
     * @return
     * @throws UnsupportedEncodingException
     * @throws JAXBException
     */
    public static ApplicationPropertiesResponse getApplicationPropertyById(long propertyId) throws UnsupportedEncodingException, JAXBException {
        Svc service = HttpExecutorService.executeHttpService(Constants.TOOLS_PATH.QUERYAPPLICATIONPROPERTY,
                new String[]{String.valueOf((propertyId))}, SERVICE_TYPE.TOOLS_SVC.toString(), HTTPMethods.GET, null, getXMLHeader());
        ApplicationPropertiesResponse applicationPropertiesResponse = (ApplicationPropertiesResponse)
                APIUtilities.convertXMLStringToObject(service.getResponseBody(), new ApplicationPropertiesResponse());
        return applicationPropertiesResponse;
    }

    /**
     * Used to refresh the properties
     *
     * @return
     * @throws UnsupportedEncodingException
     * @throws JAXBException
     */
    public static ApplicationPropertiesResponse getPropertiesRefresh() throws UnsupportedEncodingException, JAXBException, XMLStreamException, JSONException {
        Svc service = HttpExecutorService.executeHttpService(Constants.LMS_PATH.REFRESHAPPLICATIONPROPERTIES, null,
                SERVICE_TYPE.LMS_SVC.toString(), HTTPMethods.GET, null, Headers.getLmsHeaderJSON());
        ApplicationPropertiesResponse applicationPropertiesResponse = (ApplicationPropertiesResponse)
                APIUtilities.jsonToObject(service.getResponseBody(), ApplicationPropertiesResponse.class);
        return applicationPropertiesResponse;
    }

    /**
     * Used to create Property
     *
     * @param propertyName
     * @param propertyValue
     * @param propertyOwner
     * @param propertyType
     * @return
     * @throws UnsupportedEncodingException
     */
    public static ApplicationPropertiesResponse createProperty(String propertyName, String propertyValue, String propertyOwner, String propertyType) throws UnsupportedEncodingException, JAXBException {
        ApplicationPropertiesEntry propertiesEntry = new ApplicationPropertiesEntry();
        propertiesEntry.setName(propertyName);
        propertiesEntry.setOwner(propertyOwner);
        propertiesEntry.setValue(propertyValue);
        propertiesEntry.setOwner(propertyType);
        String payload = APIUtilities.convertXMLObjectToString(propertiesEntry);
        Svc service = HttpExecutorService.executeHttpService(Constants.TOOLS_PATH.QUERYAPPLICATIONPROPERTY, null,
                SERVICE_TYPE.TOOLS_SVC.toString(), HTTPMethods.POST, payload, Headers.getLmsHeaderXML());
        ApplicationPropertiesResponse applicationPropertiesResponse = (ApplicationPropertiesResponse)
                APIUtilities.convertXMLStringToObject(service.getResponseBody(), new ApplicationPropertiesResponse());
        return applicationPropertiesResponse;
    }

    /**
     * Used to Update/Edit the property which we created already
     *
     * @param name
     * @param value
     * @param owner
     * @param type
     * @param id
     * @return
     * @throws JAXBException
     */
    public static ApplicationPropertiesResponse updateProperty(String name, String value, String owner, String type, long id) throws JAXBException, UnsupportedEncodingException {
        ApplicationPropertiesEntry propertiesEntry = new ApplicationPropertiesEntry();
        propertiesEntry.setOwner(owner);
        propertiesEntry.setName(name);
        propertiesEntry.setValue(value);
        propertiesEntry.setType(type);
        String payload = APIUtilities.convertXMLObjectToString(propertiesEntry);
        Svc service = HttpExecutorService.executeHttpService(Constants.TOOLS_PATH.QUERYAPPLICATIONPROPERTY + "/" + id,
                null, SERVICE_TYPE.TOOLS_SVC.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        ApplicationPropertiesResponse applicationPropertiesResponse = (ApplicationPropertiesResponse)
                APIUtilities.convertXMLStringToObject(service.getResponseBody(), new ApplicationPropertiesResponse());
        return applicationPropertiesResponse;
    }

    /**
     * update property based on name
     * @param name
     * @param value
     * @return
     * @throws JAXBException
     * @throws UnsupportedEncodingException
     */
    public static ApplicationPropertiesResponse updateProperty(String name, String value) throws JAXBException, UnsupportedEncodingException {
        ApplicationPropertiesResponse applicationPropertiesResponse = getApplicationProperty(name);
        Long propertyId = applicationPropertiesResponse.getData().get(0).getId();
        ApplicationPropertiesEntry propertiesEntry = new ApplicationPropertiesEntry();
        propertiesEntry.setName(name);
        propertiesEntry.setValue(value);
        String payload = APIUtilities.convertXMLObjectToString(propertiesEntry);
        Svc service = HttpExecutorService.executeHttpService(Constants.TOOLS_PATH.QUERYAPPLICATIONPROPERTY + "/" + propertyId,
                null, SERVICE_TYPE.TOOLS_SVC.toString(), HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
        ApplicationPropertiesResponse applicationPropertiesResponse1 = (ApplicationPropertiesResponse)
                APIUtilities.convertXMLStringToObject(service.getResponseBody(), new ApplicationPropertiesResponse());
        return applicationPropertiesResponse1;

    }

}
