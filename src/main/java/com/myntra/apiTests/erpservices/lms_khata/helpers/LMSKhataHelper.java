package com.myntra.apiTests.erpservices.lms_khata.helpers;

import com.myntra.apiTests.SERVICE_TYPE;
import com.myntra.apiTests.common.Constants.Headers;
import com.myntra.apiTests.erpservices.lms_khata.constants.LmsKhata_constants;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.lms.khata.domain.Event;
import com.myntra.lms.khata.domain.EventType;
import com.myntra.lms.khata.domain.PaymentMode;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.HTTPMethods;
import com.myntra.test.commons.service.HttpExecutorService;
import com.myntra.test.commons.service.Svc;
import org.testng.log4testng.Logger;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;

public class LMSKhataHelper {
    private static Logger log = Logger.getLogger(LMSKhataHelper.class);

//    public EventResponse createEvent (EventType eventType , String trackingNumber , String packetId , String fromAccountId , String toAccountId , String clientId , String tenantId , Float amount , Boolean isCod , Float amountToBeCollected  , PaymentMode paymentMode , Float shipmentValue , AccountType sourceAccountType)throws UnsupportedEncodingException, JAXBException {
//        EventResponse response = null;
//        Event event = new Event();
//        event.setEventType(eventType);
//        event.setTrackingNumber(trackingNumber);
//        event.setSourceReferenceId(packetId);
//        event.setFromAccountId(fromAccountId);
//        event.setToAccountId(toAccountId);
//        event.setClientId(clientId);
//        event.setAmount(amount);
//        event.setTenantId(tenantId);
//        event.setIsCod(isCod);
//        event.setAmountToBeCollected(amountToBeCollected);
//        event.setPaymentMode(paymentMode);
//        event.setShipmentValue(shipmentValue);
//        event.setSourceAccountType(sourceAccountType);
//        String payload = APIUtilities.convertXMLObjectToString(event);
//        Svc service = HttpExecutorService.executeHttpService(LmsKhata_constants.LMS_KHATA_PATH.CREATE_EVENT,  new String[]{}, SERVICE_TYPE.KHATA_SVC.toString(),
//                HTTPMethods.PUT, payload, Headers.getLmsHeaderXML());
//        response = (EventResponse) APIUtilities.convertXMLStringToObject(service.getResponseBody(),
//                new EventResponse());
//        return response;
//    }
}
