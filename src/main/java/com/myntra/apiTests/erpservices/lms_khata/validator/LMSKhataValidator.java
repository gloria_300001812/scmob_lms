package com.myntra.apiTests.erpservices.lms_khata.validator;

import com.myntra.lms.khata.domain.EventType;
import com.myntra.lms.khata.domain.PaymentMode;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public class LMSKhataValidator {

    static Logger log = Logger.getLogger(LMSKhataValidator.class);


    public static void validateEventCreation(String trackingNumber , String eventType , String fromAccountId , String toAccountId , String is_cod , String paymentMode , String shipmentValue , String amount_to_be_collected , String amountCollected , String tenantId , String clientId , String sourceRefId ) throws UnsupportedEncodingException, JAXBException {
        Map<String, Object> eventEntries = null;
        try {


             eventEntries = DBUtilities.exSelectQueryForSingleRecord("select * from event where tracking_number = '" + trackingNumber + "' and `from_account_id` = " + fromAccountId + " and `to_account_id` = " + toAccountId + " and type = '" + eventType + "' order by `last_modified_on` desc limit 1", "myntra_lms_khata");

             if (eventEntries == null)
                 throw new NullPointerException();

        }
        catch(Exception e)
        {
            Assert.assertTrue( false,"[WARNING] :::: " + eventType + " event is not created for tracking num: " + trackingNumber + " from account: " + fromAccountId + " to account:"+ toAccountId);
        }
        String actualEventType = null;
        if(eventEntries.get("type") != null) {
            actualEventType = eventEntries.get("type").toString();
            Assert.assertEquals(eventType.toString(), actualEventType, "Event Type is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);

        }
        else
        {
            Assert.assertEquals(eventEntries.get("type"), actualEventType, "Event Type is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);

        }


            if (sourceRefId != "Null") {
                String actual_source_ref_id = eventEntries.get("source_reference_id").toString();
                Assert.assertEquals(actual_source_ref_id, sourceRefId, "Source ref Id mismatch for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);
            }

            if (paymentMode != "Null") {

                String actualPaymentMode = "";
                if(eventEntries.get("payment_mode")!= null) {
                    actualPaymentMode = eventEntries.get("payment_mode").toString();
                    Assert.assertEquals(actualPaymentMode, paymentMode, "paymentMode is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:" + toAccountId);
                }
                else
                {
                    Assert.assertEquals(eventEntries.get("payment_mode"), paymentMode, "paymentMode is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:" + toAccountId);

                }
            }

            if (amount_to_be_collected != "Null") {
                String actualAmountToBeCollected = eventEntries.get("amount").toString();
                Assert.assertEquals(actualAmountToBeCollected, amount_to_be_collected, "Amount to be collected is not matching for "  + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);

            }

//            if (amountCollected != "Null") {
//                String actualAmountCollected = eventEntries.get("amount_collected").toString();
//                Assert.assertEquals(actualAmountCollected, amountCollected, "Amount collected is not matching for "  + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);
//
//            }

            if (is_cod != "Null") {
                String actualIsCod = eventEntries.get("is_cod").toString();
                Assert.assertEquals(actualIsCod, is_cod, "is_Cod is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);

            }


        String actualIsProcessed = eventEntries.get("is_processed").toString();

            if(actualIsProcessed!="Null"){
        Assert.assertEquals(actualIsProcessed, "true", "Event is not processed");}

        String actualShipmentValue = eventEntries.get("shipment_value").toString();
        Assert.assertEquals(actualShipmentValue, shipmentValue, "shipment value is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);
        String actualTenantId = eventEntries.get("tenant_id").toString();
        String actualClientId = eventEntries.get("client_id").toString();

        Assert.assertEquals(actualTenantId, tenantId, "tenant Id is not matching for "  + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);
        Assert.assertEquals(actualClientId, clientId, "Client Id is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);

    }


    public static void validateShipmentPendency(String trackingNumber,String accountId, String cash_pending_amount, String cash_collected_amount, String goods_pending_amount, String is_settled, String tenant_id, String to_account_id) {
        Map<String, Object> eventEntries = null;
       try {


           eventEntries = DBUtilities.exSelectQueryForSingleRecord("select * from shipment_pendency where tracking_number = '" + trackingNumber + "' and `account_id` = " + accountId, "myntra_lms_khata");
           if (eventEntries == null)
               throw new NullPointerException();

       }
       catch (Exception e)
       {
           Assert.assertTrue(false,"No shipment pendency  data found for tracking num: " + trackingNumber + " account: " + accountId );

       }

           String actual_cash_pending_amount = eventEntries.get("cash_pending_amount").toString();
           String actual_cash_collected_amount = eventEntries.get("cash_collected_amount").toString();
           String actual_goods_pending_amount = eventEntries.get("goods_pending_amount").toString();
           String actual_is_settled = eventEntries.get("is_settled").toString();
        if (tenant_id != "Null") {

            String actual_tenant_id = eventEntries.get("tenant_id").toString();
             Assert.assertEquals(actual_tenant_id, tenant_id, "In Shipment Pendency Tenant Id for tracking number:" + trackingNumber + " account: " + accountId );
        }
           if (to_account_id != "Null") {
               String actual_to_account_id = eventEntries.get("to_account_id").toString();
               Assert.assertEquals(actual_to_account_id, to_account_id, "Actual account id mismatch for tracking number:" + trackingNumber + " account: " + accountId );
           }

           Assert.assertEquals(actual_cash_pending_amount, cash_pending_amount, "In Shipment Pendency Cash Pending amount mismatch for tracking number:" + trackingNumber + " account: " + accountId );
           Assert.assertEquals(actual_goods_pending_amount, goods_pending_amount, "In Shipment Pendency Goods pending amount mismatch for tracking number:" + trackingNumber + " account: " + accountId );
           Assert.assertEquals(actual_cash_collected_amount, cash_collected_amount, "In Shipment Pendency Cash collected amount for tracking number:" + trackingNumber + " account: " + accountId );
           Assert.assertEquals(actual_is_settled, is_settled, "In Shipment Pendency  Is_settled mismatch for tracking number:" + trackingNumber + " account: " + accountId );

    }




    public static void validateShipmentPendencyforDC(String accountId, String cash_pending_amount, String cash_collected_amount, String goods_pending_amount, String is_settled, String tenant_id, String to_account_id) {

        Map<String, Object> eventEntries = null;
        try {


            eventEntries = DBUtilities.exSelectQueryForSingleRecord(" select * from shipment_pendency where `account_id` = " + accountId + " and is_settled = 0 and cash_pending_amount > 0.00 and to_account_id = " + to_account_id + " order by created_on desc", "myntra_lms_khata");
            if (eventEntries == null)
                throw new NullPointerException();

        }
        catch (Exception e)
        {
            System.out.println("No shipment pendency  data found for tracking num: account: " + accountId );

        }
            String actual_cash_pending_amount = eventEntries.get("cash_pending_amount").toString();
            String actual_cash_collected_amount = eventEntries.get("cash_collected_amount").toString();
            String actual_goods_pending_amount = eventEntries.get("goods_pending_amount").toString();
            String actual_is_settled = eventEntries.get("is_settled").toString();
            String actual_tenant_id = eventEntries.get("tenant_id").toString();

            if (to_account_id != "Null") {
                String actual_to_account_id = eventEntries.get("to_account_id").toString();
                Assert.assertEquals(actual_to_account_id, to_account_id, "Actual account id mismatch");
            }

            Assert.assertEquals(actual_cash_pending_amount, cash_pending_amount, "In Shipment Pendency  Cash Pending amount mismatch");
            Assert.assertEquals(actual_goods_pending_amount, goods_pending_amount, "In Shipment Pendency  Goods pending amount mismatch");
            Assert.assertEquals(actual_cash_collected_amount, cash_collected_amount, "In Shipment Pendency  Cash collected amount");
            Assert.assertEquals(actual_is_settled, is_settled, "In Shipment Pendency  Is_settled mismatch");
            Assert.assertEquals(actual_tenant_id, tenant_id, "In Shipment Pendency  Tenant Id mismatched");


    }


    public static void validateShortageamount(Long accountId , Long to_account_id , String tenant_id, Float ExpectedPendingAmount ) {
        Map<String, Object> shipmentPendency = null;
        try {

            shipmentPendency = DBUtilities.exSelectQueryForSingleRecord("select sum(`cash_pending_amount`) from shipment_pendency where `account_id` = " + accountId + " and is_settled = 0 and cash_pending_amount > 0.00 and to_account_id = " + to_account_id + " order by created_on desc", "myntra_lms_khata");
            if (shipmentPendency == null)
                throw new NullPointerException();

        }
        catch (Exception e) {
            System.out.println("No shipment pendency  data found for tracking num:  account: " + accountId);
        }
            Float ActualPendingAmount = Float.parseFloat(shipmentPendency.get("sum(`cash_pending_amount`)").toString());
            System.out.println(ActualPendingAmount);
            Assert.assertEquals(ExpectedPendingAmount,ActualPendingAmount,"Amount pendency in shipment pendency Not changed after settlement of " +ExpectedPendingAmount );




    }

    public static void validateAllSettlement(Long accountId, Long to_account_id, String tenant_id, Float ExpectedPendingAmount) {
        Map<String, Object> shipmentPendency = null;
        try {

            shipmentPendency = DBUtilities.exSelectQueryForSingleRecord("select sum(`cash_pending_amount`) from shipment_pendency where `account_id` = " + accountId + " and is_settled = 1 and cash_pending_amount >= 0.00 and to_account_id = " + to_account_id + " order by action_date desc", "myntra_lms_khata");
            if (shipmentPendency == null)
                throw new NullPointerException();

        } catch (NullPointerException e) {
            System.out.println("No shipment pendency  data found for tracking num:  account: " + accountId);
        }
        Float ActualPendingAmount = Float.parseFloat(shipmentPendency.get("sum(`cash_pending_amount`)").toString());
        System.out.println(ActualPendingAmount);
        Assert.assertEquals(ExpectedPendingAmount, ActualPendingAmount, "Amount pendency in shipment pendency Not changed after settlement of " + ExpectedPendingAmount);
    }


    public static void validateEventCreation(String trackingNumber , String eventType , String fromAccountId , String toAccountId , String is_cod , String paymentMode , String shipmentValue , String amount_to_be_collected , String amountCollected , String tenantId , String clientId , String sourceRefId, String is_processed ) throws UnsupportedEncodingException, JAXBException {
        Map<String, Object> eventEntries = null;
        try {


            eventEntries = DBUtilities.exSelectQueryForSingleRecord("select * from event where tracking_number = '" + trackingNumber + "' and `from_account_id` = " + fromAccountId + " and `to_account_id` = " + toAccountId + " and type = '" + eventType + "' order by `last_modified_on` desc limit 1", "myntra_lms_khata");
            if (eventEntries == null)
                throw new NullPointerException();

        }
        catch(Exception e)
        {
            Assert.assertTrue( false,"[WARNING] :::: " + eventType + " event is not created for tracking num: " + trackingNumber + " from account: " + fromAccountId + " to account:"+ toAccountId);
        }
        String actualEventType = null;
        if(eventEntries.get("type") != null) {
            actualEventType = eventEntries.get("type").toString();
            Assert.assertEquals(eventType.toString(), actualEventType, "Event Type is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);

        }
        else
        {
            Assert.assertEquals(eventEntries.get("type"), actualEventType, "Event Type is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);

        }


        if (sourceRefId != "Null") {
            String actual_source_ref_id = eventEntries.get("source_reference_id").toString();
            Assert.assertEquals(actual_source_ref_id, sourceRefId, "Source ref Id mismatch for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);
        }

        if (paymentMode != "Null") {

            String actualPaymentMode = "";
            if(eventEntries.get("payment_mode")!= null) {
                actualPaymentMode = eventEntries.get("payment_mode").toString();
                Assert.assertEquals(actualPaymentMode, paymentMode, "paymentMode is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:" + toAccountId);
            }
            else
            {
                Assert.assertEquals(eventEntries.get("payment_mode"), paymentMode, "paymentMode is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:" + toAccountId);

            }
        }

        if (amount_to_be_collected != "Null") {
            String actualAmountToBeCollected = eventEntries.get("amount").toString();
            Assert.assertEquals(actualAmountToBeCollected, amount_to_be_collected, "Amount to be collected is not matching for "  + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);

        }

        if (amountCollected != "Null") {
            String actualAmountCollected = eventEntries.get("amount_collected").toString();
            Assert.assertEquals(actualAmountCollected, amountCollected, "Amount collected is not matching for "  + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);

        }

        if (is_cod != "Null") {
            String actualIsCod = eventEntries.get("is_cod").toString();
            Assert.assertEquals(actualIsCod, is_cod, "is_Cod is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);

        }


        if (is_processed != "Null") {
            String actualIsProcessed = eventEntries.get("is_processed").toString();
            Assert.assertEquals(actualIsProcessed, "true", "Event is not processed");
        }


//        String actualShipmentValue = eventEntries.get("shipment_value").toString();
//        Assert.assertEquals(actualShipmentValue, shipmentValue, "shipment value is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);
//        String actualTenantId = eventEntries.get("tenant_id").toString();
//        String actualClientId = eventEntries.get("client_id").toString();
//
//        Assert.assertEquals(actualTenantId, tenantId, "tenant Id is not matching for "  + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);
//        Assert.assertEquals(actualClientId, clientId, "Client Id is not matching for " + eventType + " event is not created for tracking num:" + trackingNumber + " from account: " + fromAccountId + "to account:"+ toAccountId);

    }

    // to validate given event does not exist
    public static void validateEventCreationNeg(String trackingNumber , String eventType ) throws UnsupportedEncodingException, JAXBException {
        Map<String, Object> eventEntries = null;

            eventEntries = DBUtilities.exSelectQueryForSingleRecord("select * from event where tracking_number = '" + trackingNumber + "' and type = '" + eventType + "' order by `last_modified_on` desc limit 1", "myntra_lms_khata");

            if (eventEntries != null)
                Assert.assertTrue(false,"Event found");

    }
    public static void validateEventCreationNeg(String trackingNumber ) throws UnsupportedEncodingException, JAXBException {
        Map<String, Object> eventEntries = null;

        eventEntries = DBUtilities.exSelectQueryForSingleRecord("select * from event where tracking_number = '" + trackingNumber + "' order by `last_modified_on` desc limit 1", "myntra_lms_khata");

        if (eventEntries != null)
            Assert.assertTrue(false,"Event found");

    }


}
