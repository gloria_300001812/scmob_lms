package com.myntra.apiTests;

/**
 * Created by 8403 on 04/05/17.
 */
public enum SERVICE_TYPE{

    LMS_SVC("lms"),
    LMS3PL_SVC("lms3pl"),
    TMS_SVC("tms"),
    KHATA_SVC("khata"),
    LMS_KHATA_SVC("lms_khata"),
    CSS_SVC("css"),
    IMS_SVC("ims"),
    OMS_SVC("oms"),
    OMSCLUSTER_SVC("oms_test"),
    TOOLS_SVC("tools"),
    PARTNER_API("partnerapi"),
    Last_mile("lastmile"),
    MASTERBAG_SVC("masterbag"),
    SORTATION_SVC("sortation"),
    DELHIVERY("lmsdelhivery"),
    CTS("cts"),
    SERVICEABILITY_UTIL("serviceabilityUtil"),
    RABBITMQ_SVC("rabbitmq"),
    DOCKINS_SVC("dockins"),
    AUGUSTUS_SVC("augustus"),
    MWS_SVC("mws"),
    SHIPMENT_PLATFORM_SVC("shipment-platform"),
    RMS_SVC("rms"),

    AMS_SVC("alteration-service");

    private String svcName;

    private SERVICE_TYPE(String svcName){
        this.svcName = svcName;
    }

    @Override
    public String toString(){
        return svcName;
    }

}
