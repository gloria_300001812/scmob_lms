package com.myntra.apiTests.logistics.statemachine.actions;

import com.myntra.apiTests.erpservices.tripService.helper.TripServiceHelper;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.code.utils.AlterationMode;
import com.myntra.lastmile.client.response.tripservice.ShipmentPendencyCreationResponse;
import com.myntra.lastmile.client.status.MLShipmentStatus;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lastmile.client.status.ShipmentPendencyUpdateResponse;
import com.myntra.lms.client.status.ShipmentSourceEnum;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.joda.time.DateTime;
import org.springframework.statemachine.StateContext;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.springframework.statemachine.action.Action;
import java.lang.Override;


import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;


public class AlterationShipment implements Action<MLShipmentStatus, MLShipmentUpdateEvent> {
    private static Logger log = Logger.getLogger(AlterationShipment.class);

    private TripServiceHelper tripServiceHelper;

 /*   public ShipmentPendencyCreationResponse createAlterationShipment(String sourceId, String sourceRefernceId, String trackingNumber, long deliveryCenterId, ShipmentType shipmentType, ShippingMethod shippingmethod,
                                         String pincode, Float shipmentValue, DateTime promiseDate, Boolean deleted, String tenantId, String clientId, ShipmentSourceEnum sourcePath, AlterationMode alterationMode, int noOfItem, String status) throws JAXBException, IOException, JSONException, XMLStreamException {
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
        return shipmentPendencyCreationResponse;
    }

    public ShipmentPendencyCreationResponse updateAlterationShipment(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                                                   String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws JAXBException, IOException, JSONException, XMLStreamException {
        ShipmentPendencyCreationResponse shipmentPendencyUpdate = tripServiceHelper.updateAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem);
        Assert.assertEquals(shipmentPendencyUpdate.getStatus().getStatusType().toString(), status);
        return shipmentPendencyUpdate;
    }*/

    @Override
    public void execute(StateContext<MLShipmentStatus, MLShipmentUpdateEvent> stateContext) {

    }


    }
