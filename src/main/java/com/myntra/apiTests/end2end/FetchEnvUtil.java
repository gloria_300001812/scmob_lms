package com.myntra.apiTests.end2end;

import com.myntra.test.commons.topology.Topology;
import com.myntra.underworld.viktor.Setup;

public class FetchEnvUtil {

	private static Setup setup=null;
	private static String envirnmentOrCluster=null;

	private static Setup getSetupInstance(){
		if(setup==null){
    		setup = (Setup) Topology.topologyContext.getBean("setup");
    	}
		return setup;
	}
	
    public static String getEnvironment() {
    	setup = getSetupInstance();
    	envirnmentOrCluster = setup.getEnvironmentOrClusterName();;    	
    	return envirnmentOrCluster;
    }

}
