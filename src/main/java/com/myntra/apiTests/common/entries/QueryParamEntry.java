package com.myntra.apiTests.common.entries;

import java.util.List;

public class QueryParamEntry {
	
	List<String> keys;
	List<String> values;
	public QueryParamEntry() {
		
	}
	public QueryParamEntry(List<String> keys, List<String> values) {
		this.keys = keys;
		this.values = values;
	}
	
	public List<String> getKeys() {
		return keys;
	}
	public void setKeys(List<String> keys) {
		this.keys = keys;
	}
	public List<String> getValues() {
		return values;
	}
	public void setValues(List<String> values) {
		this.values = values;
	}
	@Override
	public String toString() {
		return "QueryParamEntry [keys=" + keys + ", values=" + values + "]";
	}
	
	
	

}
