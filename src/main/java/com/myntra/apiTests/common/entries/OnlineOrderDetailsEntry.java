package com.myntra.apiTests.common.entries;

public class OnlineOrderDetailsEntry {

	String clientCode ;
	String cartContext ;
	String userGroup ;
	String wallet_enabled;
	String other_cards;
	String card_number;
	String bill_name ;
	String card_month ;
	String card_year ;
	String cvv_code ;
	String useSavedCard ;
	String manualGCAmount;
	String manualGCCount ;
	String autoGCAmount ;
	String gcNumber ;
	String gcPin ;
	String tenantId ;
	
	public OnlineOrderDetailsEntry()
	{
		  this.clientCode = "responsive";
			this.cartContext = "default";
			this.userGroup = "normal";
			this.wallet_enabled = "false";
			this.other_cards = "false";
			this.card_number = "4556053172667502";
			this.bill_name = "Test card";
			this.card_month = "07";
			this.card_year = "22";
			this.cvv_code = "123";
			this.useSavedCard = "false";
			this.manualGCAmount = "";
			this.manualGCCount = "1";
			this.autoGCAmount = "";
			this.gcNumber = "6991201057856234";
			this.gcPin = "240465";
			this.tenantId = "myntra";
		  
	}
	
	public OnlineOrderDetailsEntry(String card_number,String card_month, String card_year, String cvv_code)
	{
		this.card_month = card_month;
		this.card_year = card_year;
		this.cvv_code = cvv_code;
		this.card_number = card_number;
		
		//Default Values
		this.clientCode = "responsive";
		this.cartContext = "default";
		this.userGroup = "normal";
		this.wallet_enabled = "false";
		this.other_cards = "false";
		this.bill_name = "Test card";
		this.useSavedCard = "false";
		this.manualGCAmount = "";
		this.manualGCCount = "1";
		this.autoGCAmount = "";
		this.gcNumber = "6991201057856234";
		this.gcPin = "240465";
		this.tenantId = "myntra";
		  
	}
	  
	public OnlineOrderDetailsEntry(String clientCode, String cartContext, String userGroup, String wallet_enabled,
			String other_cards, String card_number, String bill_name, String card_month, String card_year,
			String cvv_code, String useSavedCard, String manualGCAmount, String manualGCCount, String autoGCAmount,
			String gcNumber, String gcPin, String tenantId) {
		super();
		this.clientCode = clientCode;
		this.cartContext = cartContext;
		this.userGroup = userGroup;
		this.wallet_enabled = wallet_enabled;
		this.other_cards = other_cards;
		this.card_number = card_number;
		this.bill_name = bill_name;
		this.card_month = card_month;
		this.card_year = card_year;
		this.cvv_code = cvv_code;
		this.useSavedCard = useSavedCard;
		this.manualGCAmount = manualGCAmount;
		this.manualGCCount = manualGCCount;
		this.autoGCAmount = autoGCAmount;
		this.gcNumber = gcNumber;
		this.gcPin = gcPin;
		this.tenantId= tenantId;
	}


	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getCartContext() {
		return cartContext;
	}
	public void setCartContext(String cartContext) {
		this.cartContext = cartContext;
	}
	public String getUserGroup() {
		return userGroup;
	}
	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}
	public String getWallet_enabled() {
		return wallet_enabled;
	}
	public void setWallet_enabled(String wallet_enabled) {
		this.wallet_enabled = wallet_enabled;
	}
	public String getOther_cards() {
		return other_cards;
	}
	public void setOther_cards(String other_cards) {
		this.other_cards = other_cards;
	}
	public String getCard_number() {
		return card_number;
	}
	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}
	public String getBill_name() {
		return bill_name;
	}
	public void setBill_name(String bill_name) {
		this.bill_name = bill_name;
	}
	public String getCard_month() {
		return card_month;
	}
	public void setCard_month(String card_month) {
		this.card_month = card_month;
	}
	public String getCard_year() {
		return card_year;
	}
	public void setCard_year(String card_year) {
		this.card_year = card_year;
	}
	public String getCvv_code() {
		return cvv_code;
	}
	public void setCvv_code(String cvv_code) {
		this.cvv_code = cvv_code;
	}
	public String getUseSavedCard() {
		return useSavedCard;
	}
	public void setUseSavedCard(String useSavedCard) {
		this.useSavedCard = useSavedCard;
	}
	public String getManualGCAmount() {
		return manualGCAmount;
	}
	public void setManualGCAmount(String manualGCAmount) {
		this.manualGCAmount = manualGCAmount;
	}
	public String getManualGCCount() {
		return manualGCCount;
	}
	public void setManualGCCount(String manualGCCount) {
		this.manualGCCount = manualGCCount;
	}
	public String getAutoGCAmount() {
		return autoGCAmount;
	}
	public void setAutoGCAmount(String autoGCAmount) {
		this.autoGCAmount = autoGCAmount;
	}
	public String getGcNumber() {
		return gcNumber;
	}
	public void setGcNumber(String gcNumber) {
		this.gcNumber = gcNumber;
	}
	public String getGcPin() {
		return gcPin;
	}
	public void setGcPin(String gcPin) {
		this.gcPin = gcPin;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Override
	public String toString() {
		return "OnlineOrderDetailsEntry [clientCode=" + clientCode + ", cartContext=" + cartContext + ", userGroup="
				+ userGroup + ", wallet_enabled=" + wallet_enabled + ", other_cards=" + other_cards + ", card_number="
				+ card_number + ", bill_name=" + bill_name + ", card_month=" + card_month + ", card_year=" + card_year
				+ ", cvv_code=" + cvv_code + ", useSavedCard=" + useSavedCard + ", manualGCAmount=" + manualGCAmount
				+ ", manualGCCount=" + manualGCCount + ", autoGCAmount=" + autoGCAmount + ", gcNumber=" + gcNumber
				+ ", gcPin=" + gcPin + ", tenantId=" + tenantId + "]";
	}
}
