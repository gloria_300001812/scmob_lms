package com.myntra.apiTests.common.entries;

public class ReadyToDispatchEntry {
	String orderReleaseId;
	Long dispatchWarehouseId;
	
	public ReadyToDispatchEntry() {
		
	}

	public ReadyToDispatchEntry(String orderReleaseId, Long dispatchWarehouseId) {
		this.orderReleaseId = orderReleaseId;
		this.dispatchWarehouseId = dispatchWarehouseId;
	}

	public String getOrderReleaseId() {
		return orderReleaseId;
	}

	public void setOrderReleaseId(String orderReleaseId) {
		this.orderReleaseId = orderReleaseId;
	}

	public Long getDispatchWarehouseId() {
		return dispatchWarehouseId;
	}

	public void setDispatchWarehouseId(Long dispatchWarehouseId) {
		this.dispatchWarehouseId = dispatchWarehouseId;
	}

	@Override
	public String toString() {
		return "ReadyToDispatchEntry [orderReleaseId=" + orderReleaseId + ", dispatchWarehouseId=" + dispatchWarehouseId
				+ "]";
	}
	
	
	
}
