/**
 * 
 */
package com.myntra.apiTests.common.entries;

/**
 * @author 17727
 *
 */
public class OrderEntryStatus {
	String lineStatus;
	String packetStatus;
	String releaseStatus;
	
	
	public OrderEntryStatus() {
		
	}


	public OrderEntryStatus(String lineStatus, String packetStatus, String releaseStatus) {
		this.lineStatus = lineStatus;
		this.packetStatus = packetStatus;
		this.releaseStatus = releaseStatus;
	}


	public String getLineStatus() {
		return lineStatus;
	}


	public void setLineStatus(String lineStatus) {
		this.lineStatus = lineStatus;
	}


	public String getPacketStatus() {
		return packetStatus;
	}


	public void setPacketStatus(String packetStatus) {
		this.packetStatus = packetStatus;
	}


	public String getReleaseStatus() {
		return releaseStatus;
	}


	public void setReleaseStatus(String releaseStatus) {
		this.releaseStatus = releaseStatus;
	}


	@Override
	public String toString() {
		return "OrderEntryStatus [lineStatus=" + lineStatus + ", packetStatus=" + packetStatus + ", releaseStatus="
				+ releaseStatus + "]";
	}
	
	
	
	
}
