package com.myntra.apiTests.common.Constants;

public interface EnumSCM {

	String XPRESS = "XPRESS";
	public static final String RETURN_OUT_FOR_PICKUP="RETURN_OUT_FOR_PICKUP";
	public static final String PACKED = "PACKED";
	public static final String DELIVERED = "DELIVERED";
	public static final String INSCANNED = "INSCANNED";
	public static final String RTO_LOST = "RTO_LOST";
	public static final String LOST = "LOST";
	public static final String LOST_IN_HUB = "LOST_IN_HUB";
	public static final String LOST_IN_DC = "LOST_IN_DC";
	public static final String LOST_IN_TRANSIT = "LOST_IN_TRANSIT";
	public static final String RTO_FOUND_ORDER = "RTO_FOUND_ORDER";
	public static final String SHIPPED = "SHIPPED";
	public static final String UNASSIGN = "UNASSIGN";
	public static final String PICKUP_REJECTED = "PICKUP_REJECTED";
	public static final String ONHOLD_PICKUP_WITH_PLATFORM = "ONHOLD_PICKUP_WITH_PLATFORM";
	public static final String RTO_RECEIVED = "RTO_RECEIVED";
	public static final String RECEIVED_IN_RETURNS_HUB = "RECEIVED_IN_RETURNS_HUB";
	public static final String RECEIVED_AT_TRANSPORT_HUB = "RECEIVED_AT_TRANSPORT_HUB";
	public static final String RTO_DISPATCHED = "RTO_DISPATCHED";
	public static final String DELIVERED_TO_SELLER = "DELIVERED_TO_SELLER";
	public static final String RETURN_RECEIVED = "RETURN_RECEIVED";
	public static final String RETURN_REJECTED_RESHIP_PENDING = "RETURN_REJECTED_RESHIP_PENDING";
	public static final String CANCELLED_AT_HUB = "CANCELLED_AT_HUB";
	public static final String ON_HOLD = "ON_HOLD";
	public static final String ASSIGN_TO_SDA = "ASSIGN_TO_SDA";
	public static final String ASSIGNED_TO_SDA = "ASSIGNED_TO_SDA";
	public static final String ASSIGNED_TO_DC = "ASSIGNED_TO_DC";
	public static final String DISPATCH_TO_OTHER_DC = "DISPATCH_TO_OTHER_DC";
	public static final String START_TRIP = "START_TRIP";
	public static final String PICKUP_QC_COMPLETE = "PICKUP_QC_COMPLETE";
	public static final String ASSIGN_TO_LAST_MILE_PARTNER = "ASSIGN_TO_LAST_MILE_PARTNER";
	public static final String ASSIGNED_TO_OTHER_DC = "ASSIGNED_TO_OTHER_DC";
	public static final String HAND_OVER_TO_LAST_MILE_PARTNER = "HAND_OVER_TO_LAST_MILE_PARTNER";
	public static final String PICKUP_APPROVED_BY_CC = "PICKUP_APPROVED_BY_CC";
	public static final String PICKUP_REJECTED_BY_CC = "PICKUP_REJECTED_BY_CC";
	public static final String RETURN_IN_TRANSIT = "RETURN_IN_TRANSIT";
	public static final String RIT = "RIT";
	public static final String COMPLETE_TRIP = "COMPLETE_TRIP";
	public static final String RESHIP_TO_CUSTOMER = "RESHIP_TO_CUSTOMER";
	public static final String RESHIPPED_TO_CUSTOMER = "RESHIPPED_TO_CUSTOMER";
	public static final String COMPLETED = "COMPLETED";
	public static final String CREATED = "CREATED";
	public static final String CREATE = "CREATE";
	public static final String PROCESSING = "PROCESSING";
	public static final String DL = "DL";
	public static final String PU = "PU";
	public static final String FD = "FD";
	public static final String FP = "FP";
	public static final String OFD = "OFD";
	public static final String TRY_AND_BUY = "TRY_AND_BUY";
	public static final String WFD = "WFD";
	public static final String IS = "IS";
	public static final String PK = "PK";
	public static final String RCVD_IN_DISPATCH_HUB = "RCVD_IN_DISPATCH_HUB";
	public static final String RTO = "RTO";
	public static final String RTOL = "RTOL";
	public static final String RT = "RT";
	public static final String RTO_R = "RTO_R";
	public static final String SH = "SH";
	public static final String RL = "RL";
	public static final String CANCELLED_IN_HUB = "CANCELLED_IN_HUB";
	public static final String ADDED_TO_MB = "ADDED_TO_MB";
	public static final String ADDED_TO_SDA_TRIP = "ADDED_TO_SDA_TRIP";
	public static final String RETURN_SUCCESSFUL = "RETURN_SUCCESSFUL";

	public static final String RECEIVED_IN_REGIONAL_HANDOVER_CENTER = "RECEIVED_IN_REGIONAL_HANDOVER_CENTER";
	public static final String RTO_IN_TRANSIT = "RTO_IN_TRANSIT";
	public static final String RECEIVED_IN_DISPATCH_HUB = "RECEIVED_IN_DISPATCH_HUB";
	public static final String PICKED_UP_SUCCESSFULLY = "PICKED_UP_SUCCESSFULLY";
	public static final String PICKED_UP_SHORTAGE = "PICKED_UP_SHORTAGE";
	public static final String PQCP_APPROVED = "PQCP_APPROVED";
	public static final String PQCP_REJECTED = "PQCP_REJECTED";
	public static final String PQCP_APPROVED_Before_trip_close = "PQCP_APPROVED_Before_trip_close";
	public static final String PQCP_REJECTED_Before_trip_close = "PQCP_REJECTED_Before_trip_close";
	public static final String PQCP_APPROVED_After_trip_close = "PQCP_APPROVED_After_trip_close";//dc is approving
	public static final String PQCP_REJECTED_After_trip_close = "PQCP_REJECTED_After_trip_close";

	public static final String ONHOLD_PICKUP_WITH_CUSTOMER_REJECT = "ONHOLD_PICKUP_WITH_CUSTOMER_REJECT";
	public static final String FAILED_PICKUP_ONHOLD_PICKUP_WITH_CUSTOMER_REJECT = "FAILED_PICKUP_ONHOLD_PICKUP_WITH_CUSTOMER_REJECT";
	public static final String ONHOLD_PICKUP_WITH_CUSTOMER_APPROVE = "ONHOLD_PICKUP_WITH_CUSTOMER_APPROVE";
	public static final String ONHOLD_PICKUP_WITH_DC_REJECT = "ONHOLD_PICKUP_WITH_DC_REJECT";
	public static final String ONHOLD_PICKUP_WITH_DC_APPROVE = "ONHOLD_PICKUP_WITH_DC_APPROVE";
	public static final String FAILED_PICKUP_AND_SUCCESS = "FAILED_PICKUP_AND_SUCCESS";
	public static final String PICKUP_DONE_QC_PENDING = "PICKUP_DONE_QC_PENDING";
	public static final String FAILED_PICKUP_FAILED_PICKUP_AND_SUCCESS = "FAILED_PICKUP_FAILED_PICKUP_AND_SUCCESS";
	public static final String FAILED_PICKUP_AND_SUCCESS_ON_SAMETRIP = "FAILED_PICKUP_AND_SUCCESS_ON_SAMETRIP";
	public static final String APPROVED_ONHOLD_PICKUP_WITH_DC = "APPROVED_ONHOLD_PICKUP_WITH_DC";
	public static final String ASSIGNED_TO_LAST_MILE_PARTNER = "ASSIGNED_TO_LAST_MILE_PARTNER";
	public static final String FAILED_TO_HANDOVER_TO_LAST_MILE_PARTNER = "FAILED_TO_HANDOVER_TO_LAST_MILE_PARTNER";
	public static final String HANDED_TO_LAST_MILE_PARTNER = "HANDED_TO_LAST_MILE_PARTNER";
	public static final String PQCP_ONHOLD_APPROVED_Before_trip_close = "PQCP_ONHOLD_APPROVED_Before_trip_close";
	public static final String PQCP_ONHOLD_APPROVED_After_trip_close = "PQCP_ONHOLD_APPROVED_After_trip_close";
	public static final String PQCP_ONHOLD_Before_trip_close_APPROVED_After_trip_close = "PQCP_ONHOLD_Before_trip_close_APPROVED_After_trip_close";
	public static final String PQCP_ONHOLD_BEFORE_TRIP_CLOSE_REJECT_AFTER_TRIP_CLOSE = "PQCP_ONHOLD_BEFORE_TRIP_CLOSE_REJECT_AFTER_TRIP_CLOSE";
	public static final String PQCP_ONHOLD_BEFORE_TRIP_CLOSE_REJECT_BEFORE_TRIP_CLOSE = "PQCP_ONHOLD_BEFORE_TRIP_CLOSE_REJECT_BEFORE_TRIP_CLOSE";
	public static final String PQCP_ONHOLD_BEFORE_TRIP_CLOSE_APPROVED_BEFORE_TRIP_CLOSE = "PQCP_ONHOLD_BEFORE_TRIP_CLOSE_APPROVED_BEFORE_TRIP_CLOSE";
	public static final String PQCP_TRIP_CLOSE_ONHOLD_APPROVE = "PQCP_TRIP_CLOSE_ONHOLD_APPROVE";
	public static final String PQCP_TRIP_CLOSE_ONHOLD_REJECT = "PQCP_TRIP_CLOSE_ONHOLD_REJECT";
	public static final String HAPPY_REQUEUE = "HAPPY_REQUEUE";
	public static final String HAPPY_REJECT = "HAPPY_REJECT";
	public static final String PICKED_UP_ONHOLD_DAMAGED_APPROVE_TripCLose_Reship = "PICKED_UP_ONHOLD_DAMAGED_APPROVE_TripCLose_Reship";
	public static final String PICKED_UP_ONHOLD_DAMAGED_REJECT_TRIP_CLOSE= "PICKED_UP_ONHOLD_DAMAGED_REJECT_TRIP_CLOSE";
	public static final String PICKED_UP_ONHOLD_DAMAGED_TripCLose_APPROVE = "PICKED_UP_ONHOLD_DAMAGED_TripCLose_APPROVE";
	public static final String SDA_ONHOLD_CC_APPROVE_SDA_ONHOLD_Fail = "SDA_ONHOLD_CC_APPROVE_SDA_ONHOLD_Fail";
	public static final String CC_PreApprove_Before_Manifest = "CC_PreApprove_Before_Manifest";
	public static final String CC_PreApprove_After_Manifest = "CC_PreApprove_After_Manifest";
	public static final String TripStart_CCPreApprove_PQCP = "TripStart_CCPreApprove_PQCP";
	public static final String TripStart_CCPreApprove_PickupOnHold = "TripStart_CCPreApprove_PickupOnHold";
	public static final String SDA_REASON_OTHERS_REQUEUE = "SDA_REASON_OTHERS_REQUEUE";
	public static final String SDA_REASON_OTHERS_REJECT= "SDA_REASON_OTHERS_REJECT";
	public static final String SDA_REASON_HAPPY_REJECT= "SDA_REASON_HAPPY_REJECT";
	public static final String PQCP_Trip_close_ONHOLD_REQUEUE = "PQCP_Trip_close_ONHOLD_REQUEUE";
	public static final String SDA_Reason_Happy_REQUEUE = "SDA_Reason_Happy_REQUEUE";
	public static final String SDA_PickUp_ONHOLD_DamagedProduct_CC_REJECT_CloseTrip = "SDA_PickUp_ONHOLD_DamagedProduct_CC_REJECT_CloseTrip";
	public static final String SDA_PickUp_ONHOLD_DamagedProduct_Trip_close_CC_REJECT = "SDA_PickUp_ONHOLD_DamagedProduct_Trip_close_CC_REJECT";
	public static final String TripStart_CCPreApprove_PQCP_Refund_Trip_Close = "TripStart_CCPreApprove_PQCP_Refund_Trip_Close";
	public static final String PQCP_CC_Approve_DC_RejectFailed = "PQCP_CC_Approve_DC_RejectFailed";
	public static final String PrintReturnLable = "PrintReturnLable";
	public static final String VerifySignatureUpload = "VerifySignatureUpload";
	public static final String FAILED_PICK_UPS_DC_REJECT= "FAILED_PICK_UP_DC_REJECT_USER_CREATE_RETURN_PROCESS";

	public static final String EXPECTED_IN_DC = "EXPECTED_IN_DC";
	public static final String FAILED_DELIVERY = "FAILED_DELIVERY";
	public static final String FAILED_PICKUP = "FAILED_PICKUP";
	public static final String INCOMPLETE_INCORRECT_ADDRESS = "INCOMPLETE_INCORRECT_ADDRESS";
	public static final String ONHOLD_PICKUP_WITH_CUSTOMER = "ONHOLD_PICKUP_WITH_CUSTOMER";
	public static final String OUT_FOR_DELIVERY = "OUT_FOR_DELIVERY";
	public static final String OUT_FOR_PICKUP = "OUT_FOR_PICKUP";
	public static final String PICKED_UP = "PICKED_UP";
	public static final String PICKUP_CREATED = "PICKUP_CREATED";
	public static final String PICKUP_SUCCESSFUL = "PICKUP_SUCCESSFUL";
	public static final String PICKUP_SUCCESSFUL_CB_AT_DC = "PICKUP_SUCCESSFUL_CB_AT_DC";
	public static final String PICKUP_SUCCESSFUL_CB_AT_WH = "PICKUP_SUCCESSFUL_CB_AT_WH";
	public static final String PICKUP_SUCCESSFUL_CB_AT_RPU = "PICKUP_SUCCESSFUL_CB_AT_RPU";
	public static final String RESCHEDULED_CUSTOMER_NOT_AVAILABLE = "RESCHEDULED_CUSTOMER_NOT_AVAILABLE";
	public static final String RECEIVED_IN_DC = "RECEIVED_IN_DC";
	public static final String NOT_REACHABLE_UNAVAILABLE = "NOT_REACHABLE_UNAVAILABLE";
	public static final String RTO_CONFIRMED = "RTO_CONFIRMED";
	public static final String REJECTED_ONHOLD_PICKUP_WITH_DC = "REJECTED_ONHOLD_PICKUP_WITH_DC";
	public static final String REJECTED_ONHOLD_PICKUP_WITH_COURIER = "REJECTED_ONHOLD_PICKUP_WITH_COURIER";
	public static final String RESHIPPED = "RESHIPPED";
	public static final String UNASSIGNED = "UNASSIGNED";
	public static final String HANDED_OVER_TO_3PL = "HANDED_OVER_TO_3PL";
	public static final String RECEIVED_AT_TRANSPORTER_HUB = "RECEIVED_AT_TRANSPORTER_HUB";
	public static final String IN_TRANSIT_DELAYED = "IN_TRANSIT_DELAYED";
	public static final String CLOSED = "CLOSED";
	public static final String NEW = "NEW";
	public static final String IN_TRANSIT = "IN_TRANSIT";
	public static final String RECEIVED = "RECEIVED";
	public static final String SUCCESS = "SUCCESS";
	public static final String ERROR = "ERROR";
	public static final String WARNING = "WARNING";
	public static final String DELIVERY = "DELIVERY";
	public static final String PICKUP = "PICKUP";
	public static final String TRYNBUY = "TRYNBUY";
	public static final String TRY_BUY = "TRY_BUY";
	public static final String SELF_SHIP = "SELF_SHIP";
	public static final String RECEIVE_IN_DC = "RECEIVE_IN_DC";
	public static final String SELF_SHIP_REJECT = "SELF_SHIP_REJECT";
	public static final String SELF_SHIP_PICKUP_SUCCESSFUL = "SELF_SHIP_PICKUP_SUCCESSFUL";
	public static final String SELF_SHIP_ON_HOLD_APPROVE = "SELF_SHIP_ON_HOLD_APPROVE";
	public static final String ONHOLD_PICKUP_WITH_DC = "ONHOLD_PICKUP_WITH_DC";
	public static final String ONHOLD_PICKUP_WITH_COURIER = "ONHOLD_PICKUP_WITH_COURIER";
	public static final String SELF_SHIP_ON_HOLD_REJECT = "SELF_SHIP_ON_HOLD_REJECT";
	public static final String FAILED = "FAILED";
	public static final String HAPPY_WITH_PRODUCT = "HAPPY_WITH_PRODUCT";
	public static final String ON_HAND = "ON_HAND";
	public static final String JIT = "JIT";
	public static final String JUST_IN_TIME = "JUST_IN_TIME";
	public static final String OPEN_BOX_PICKUP = "OPEN_BOX_PICKUP";
	public static final String CLOSED_BOX_PICKUP = "CLOSED_BOX_PICKUP";
	public static final String VALUE_SHIPPING = "VALUE_SHIPPING";
	public static final  String PICKUP_SUCCESSFUL_ONHOLD_APPROVE="PICKUP_SUCCESSFUL_ONHOLD_APPROVE";
	public static final  String PICKUP_SUCCESSFUL_ONHOLD_REJECTED="PICKUP_SUCCESSFUL_ONHOLD_REJECTED";
	public static final  String IN_TRANSIT_WRONG_ROUTE="IN_TRANSIT_WRONG_ROUTE";
	public static final  String SELF_SHIP_QC_PASS="SELF_SHIP_QC_PASS";
	public static final String RESCHEDULED_Trip_close_ONHOLD_APPROVE = "RESCHEDULED_Trip_close_ONHOLD_APPROVE";
	public static final String RESCHEDULED_Trip_close_ONHOLD_REJECT = "RESCHEDULED_Trip_close_ONHOLD_REJECT";


	public static final String Q = "Q";
	public static final String RFR = "RFR";
	public static final String PP = "PP";
	public static final String PV = "PV";
	public static final String WP = "WP";
	public static final String C = "C";
	public static final String L = "L";
	public static final String A = "A";
	public static final String D = "D";
	public static final String IC = "IC";
	public static final String QD = "QD";
	public static final String S = "S";
	public static final String F = "F";
	public static final String NORMAL = "NORMAL";
	public static final String EXPRESS = "EXPRESS";
	public static final String SDD = "SDD";
	public static final String EXCHANGE = "EXCHANGE";
	public static final String RETURN = "RETURN";
	public static final String RETURNED = "RETURNED";
	public static final String CANCELLED = "CANCELLED";
	public static final String ORDER = "ORDER";
	public static final String ORDERNUMBER = "ORDER";
	public static final String RELEASE = "RELEASE";
	public static final String LINE = "LINE";
	public static final String ISOC = "ISOC";
	public static final String CCR = "CCR";
	public static final String PQCP = "PQCP";
	public static final String REJECTED = "REJECTED";
	public static final String NOT_INITIATED = "NOT_INITIATED";
	public static final String AWAITING = "AWAITING";
	public static final String ACCEPTED = "ACCEPTED";
	public static final String FIT = "FIT";
	public static final String UNRTO = "UNRTO";
	public static final String SMDL = "SMDL";
	public static final String FDDL = "FDDL";
	public static final String FDTODL = "FDTODL";
	public static final String FDFDDL = "FDFDDL";
	public static final String RECEIVED_AT_HANDOVER_CENTER = "RECEIVED_AT_HANDOVER_CENTER";
	public static final String SHORTAGE = "SHORTAGE";
	public static final String RECEIVED_DAMAGED = "RECEIVED_DAMAGED";
	public static final String UPDATE = "UPDATE";
	public static final String NOT_ABLE_TO_PICKUP = "NOT_ABLE_TO_PICKUP";
	public static final String PICKUP_SUCCESSFUL_QC_PENDING = "PICKUP_SUCCESSFUL_QC_PENDING";
	public static final String RETURNS_CANCELLATION = "RETURNS_CANCELLATION";
	public static final String RETURN_REJECTED = "RETURN_REJECTED";
	public static final String ON_HOLD_DAMAGED_PRODUCT = "ON_HOLD_DAMAGED_PRODUCT";
	public static final String TRIP_COMPLETE = "TRIP_COMPLETE";
	public static final String APPROVED = "APPROVED";
	public static final String COURIER_HANDOVER = "COURIER_HANDOVER";
	public static final String TRANSITION_NOT_CONFIGURED = "TRANSITION_NOT_CONFIGURED";
	public static final String RETURN_CREATED = "RETURN_CREATED";
	public static final String PICKUP_DONE = "PICKUP_DONE";
	public static final String ONHOLD_RETURN_WITH_COURIER = "ONHOLD_RETURN_WITH_COURIER";
	public static final String PICKUP_ON_HOLD_DAMAGED_PRODUCT = "PICKUP_ON_HOLD_DAMAGED_PRODUCT";
	public static final String OTHERS = "OTHERS";
	public static final String RESHIPPED_DELIVERED_TO_CUSTOMER = "RESHIPPED_DELIVERED_TO_CUSTOMER";

	//************ Close Box Pickup Shipment Status*************

	public static final String RECEIVED_AT_RETURNS_PROCESSING_CENTER = "RECEIVED_AT_RETURNS_PROCESSING_CENTER";

	//************ Close Box Pickup Activity type*************

	public static final String ACTIVITY_TYPE_PICKUP_CREATED = "PICKUP_CREATED";
	public static final String ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED = "PICKUP_DETAILS_UPDATED";
	public static final String ACTIVITY_TYPE_OUT_FOR_PICKUP = "OUT_FOR_PICKUP";
	public static final String ACTIVITY_TYPE_PICKUP_SUCCESSFUL = "PICKUP_SUCCESSFUL";
	public static final String ACTIVITY_TYPE_PICKUP_DONE = "PICKUP_DONE";
	public static final String ACTIVITY_TYPE_PICKUP_IN_TRANSIT = "PICKUP_IN_TRANSIT";
	public static final String ACTIVITY_TYPE_FAILED_PICKUP = "FAILED_PICKUP";

	//************ Close Box Pickup Activity type Status *************
	public static final String ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED_STATUS = "PICKUP_CREATED";
	public static final String ACTIVITY_TYPE_OUT_FOR_PICKUP_STATUS = "OUT_FOR_PICKUP";
	public static final String ACTIVITY_TYPE_PICKUP_DONE_STATUS = "RECEIVED_AT_RETURNS_PROCESSING_CENTER";
	public static final String ACTIVITY_TYPE_FAILED_PICKUP_STATUS = "FAILED_PICKUP";


	// **********RMS status ****************
	public static final String CFDC = "CFDC";
	public static final String CPDC = "CPDC";
	public static final String RADC = "RADC";
	public static final String RDU = "RDU";
	public static final String RIS = "RIS";
	public static final String RJDC = "RJDC";
	public static final String RJS = "RJS";
	public static final String RJUP = "RJUP";
	public static final String RNC = "RNC";
	public static final String RPF = "RPF";
	public static final String RPF2 = "RPF2";
	public static final String RPF3 = "RPF3";
	public static final String RPI = "RPI";
	public static final String RPI2 = "RPI2";
	public static final String RPI3 = "RPI3";
	public static final String RPU = "RPU";
	public static final String RPUQ2 = "RPUQ2";
	public static final String RPUQ3 = "RPUQ3";
	public static final String RQCF = "RQCF";
	public static final String RQCP = "RQCP";
	public static final String RQF = "RQF";
	public static final String RQP = "RQP";
	public static final String RQSF = "RQSF";
	public static final String RQSP = "RQSP";
	public static final String RRC = "RRC";
	public static final String RRD = "RRD";
	public static final String RRJ = "RRJ";
	public static final String RRQP = "RRQP";
	public static final String RRQS = "RRQS";
	public static final String LPI = "LPI";
	public static final String DEC = "DEC";
	public static final String RRRS = "RRRS";
	public static final String RRS = "RRS";
	public static final String RSD = "RSD";

	// ********** TOD Status ****************
	public static final String NOT_TRIED = "NOT_TRIED";
	public static final String TRIED_AND_BOUGHT = "TRIED_AND_BOUGHT";
	public static final String TRIED_AND_NOT_BOUGHT = "TRIED_AND_NOT_BOUGHT";
	public static final String SNATCHED = "SNATCHED";
	public static final String wareHouseIdJabong = "110";

    //********** Courier Codes ****************
	public static final   String COURIER_CODE_ML = "ML";
	public static final String COURIER_CODE_EK = "EK";
	public static final String COURIER_CODE_BD = "BD";	
	public static final String COURIER_CODE_IP = "IP";
	
	// ********** Payment Methods ****************
	public static final String ONLINE_PAYMENT = "on";
	public static final String COD_PAYMENT = "cod";
	
	// ********** Item Types ****************
	public static final String ITEM_TYPE = "E_GIFT_CARD";

    //*********Store Codes*********************
    
	public static final Long STORE_ID_MYNTRA=1L;
	public static final Long STORE_ID_FKART2=2L;
	public static final Long STORE_ID_FKART3=3L;
	public static final Long STORE_ID_FKART4=4L;
	public static final Long STORE_ID_JABONG=5L;
	
    //*********Store PartnerIds *********************
    
	public static final String STORE_Partner_ID_MYNTRA="2297";
	public static final String STORE_Partner_ID_JABONG="4603";
    
    
	public static final String SELLER_PACKET_ITEM_STATUS_A="A";
	public static final String SELLER_PACKET_ITEM_STATUS_QD="QD";
	public static final String SELLER_PACKET_ITEM_STATUS_PK="PK";
	public static final String SELLER_PACKET_ITEM_STATUS_IC="IC";
	public static final String SELLER_PACKET_ITEM_STATUS_PP="PP";
    
    
	public static final  String SELLER_PACKET_STATUS_Q="Q";
	public static final String SELLER_PACKET_STATUS_PK="PK";
	public static final String SELLER_PACKET_STATUS_SH="SH";
	public static final String SELLER_PACKET_STATUS_F="F";
	public static final String SELLER_PACKET_STATUS_L="L";
    
    //********** ORDER ADDITIONAL INFO ****************
	public static final String ADDITIONAL_INFO_PACKAGING_TYPE = "PACKAGING_TYPE";
	public static final String ADDITIONAL_INFO_PACKAGING_VALUE_PREMIUM="PREMIUM";
	public static final String ADDITIONAL_INFO_PACKAGING_VALUE_NORMAL="NORMAL";
	public static final String EXPRESS_REFUND_PPS_ID="EXPRESS_REFUND_PPS_ID";
	public static final String EXPRESS_REFUND_TX_REF_ID = "EXPRESS_REFUND_TX_REF_ID";
	public static final Long QC_PASS_CODE=61L;
	public static final Long QC_FAIL_CODE = 62L;
	public static final Long QC_DESK_CODE=1L;
	public static final String QC_QUALITY_Q2 = "Q2";
	public static final String QC_REJECT_REASON = "DAMAGED_CTH";
	public static final String QC_REJECT_DESCRIPTION = "Damaged (Cut/Torn/Hole)";
	public static final String wareHouse_BLR="36";
	public static final String MFB_Jabong_StoreID="5";
	public static final String owner_partner_id_jabong="4602";
	public static final String owner_partner_id_myntra="3974";
	public static final String owner_partner_id_flipkart="3854";
	public static final String ADDITIONAL_INFO_HAZMAT_TYPE = "HAZMAT";


	
	//********** Application Properties ****************
	public static final String USE_GST_ENGINE="taxmaster.use_customer_gst_engine";
	public static final String IS_LMC_ON="oms.lmc_on";
	
	//********** Cancellation Reason Codes ****************
	public static final long LOST_ORDER_CANCELLATION=49;
	public static final long PACKET_CANCELLATION_REASON=15;
	
	//********** ONHold Reason Codes ****************
	public static final String DULICATE_ORDER_37="37";
	public static final String COD_AMOUNT_LIMIT_35="35";
	public static final String PINCODE_NOT_SERVICABLE_31="31";
	public static final String COURIERINFO_NOT_FOUND_32="32";
	public static final String FRAUD_SYSTEM_CHECK_2="2";
	public static final String FRAUD_USER_23="23";
	public static final String IMS_OOS_34="34";
	
	//********** Client and Tenant Data for Jabong and Myntra ****************
	public static final String clientId_myntra= "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61";
	public static final String client_jabong="jabong-a6aafa2f-ed53-4cb0-8db0-46f8c94f99b5";
	public static final String tenantId_myntra="2297";
	public static final String tenantId_jabong="4603";
	public static final String dataSource = "myntra";

	//***************Busniess Units***********************
	public static final String BUSINESS_UNIT_MYNTRA="MYNTRA";
	public static final String BUSINESS_UNIT_JABONG="JABONG";

	public static final long RETURN_REASON_ID=49;
	
	
	
}
