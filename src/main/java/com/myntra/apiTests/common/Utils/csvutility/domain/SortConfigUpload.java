package com.myntra.apiTests.common.Utils.csvutility.domain;

import lombok.Data;

import java.util.List;


/**
 * @author Shanmugam Yuvaraj
 * Since: Jul:2019
 */

@Data
public class SortConfigUpload {

    private List<String> tenantId;
    private List<String> clientId;
    private List<String> origin;
    private List<String> destination;
    private List<String> sortLevel;
    private List<String> sourceClientId;
    private List<String> courierCode;
    private List<String> shippingMethod;
    private List<String> nextLocation;
    private List<String> isActive;
}
