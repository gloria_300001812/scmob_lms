package com.myntra.apiTests.common.Utils;

public interface DBHelperEnums {
	
	//**************DB Name, All DB names will go here***************
	public enum DBName{
		myntra_oms,myntra_lms,myntra_wms,myntra_atp,myntra_ims,myntra_tms,myntra_packman
	}
	
	//**************Transaction Tables, All new Tables will go here***************
	public enum TablesName{
		order_release,order_line,packet,orders, //OMS Tables
		inventory, // ATP Tables
		order_to_ship, //LMS Tables
		wh_inventory, //IMS Tables
	}
	
	//**************Additional Tables, Additional Info Tables details will come here***************
	public enum AdditionalInfoTables{
		order_release_additional_info("order_release_additional_info","order_release_id_fk"),
		order_line_additional_info("order_line_additional_info","order_line_id_fk"),
		order_additional_info("order_additional_info","order_id_fk");

		String table_name;
		String foreignKeyId;

		AdditionalInfoTables(String table_name,String fk){
			this.table_name=table_name;
			this.foreignKeyId = fk;
		}
		public String getValue(){
			return foreignKeyId;
		}

		public String toString(){
			return this.table_name+":"+this.foreignKeyId;
		}

	}

}
