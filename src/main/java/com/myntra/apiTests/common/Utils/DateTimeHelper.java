package com.myntra.apiTests.common.Utils;

import com.rethinkdb.ast.query.gen.EpochTime;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

/**
 * @author Bharath.MC
 * @since Aug-2019
 */
public class DateTimeHelper {


    /**
     * Format: yyyy-MM-dd'T'HH:mm:ssZZZZZ
     * ex: 2019-02-15T11:55:23+05:30
     */
    public static String GetDateWithTZ(Integer... plusDays) {
        ZonedDateTime date;
        if (plusDays != null && plusDays.length != 0) {
            date = ZonedDateTime.now();
        } else {
            date = ZonedDateTime.now().plusDays(plusDays[0]);
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZZZZZ");
        System.out.println("Date format " + formatter.format(date));
        return formatter.format(date).toString();
    }

    /**
     * @param timeWithTZ : 2019-02-23T06:00:00.000+05:30
     * @return : 2019-02-23 06:00:00
     */
    public static String GetDateWithoutTZFormat01(String timeWithTZ) {
        System.out.println("GetDateWithoutTZFormat01 Input="+timeWithTZ);
        ZonedDateTime result = ZonedDateTime.parse(timeWithTZ, DateTimeFormatter.ISO_DATE_TIME);
        LocalDateTime localDate = result.toLocalDateTime();
        System.out.println(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDate));
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDate);
    }

    /**
     * @param timeWithTZ : 2019-03-05T09:00:00+05:30
     * @return : 2019-02-23
     */
    public static String GetDateWithoutTZFormat02(String timeWithTZ) {
        ZonedDateTime result = ZonedDateTime.parse(timeWithTZ, DateTimeFormatter.ISO_DATE_TIME);
        LocalDateTime localDate = result.toLocalDateTime();
        System.out.println(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDate));
        return DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate);
    }

    /**
     * @param timeWithTZ : 2019-03-05T09:00:00+05:30
     * @return :06:00:00
     */
    public static String GetTimeWithoutTZFormat02(String timeWithTZ) {
        ZonedDateTime result = ZonedDateTime.parse(timeWithTZ, DateTimeFormatter.ISO_DATE_TIME);
        LocalDateTime localDate = result.toLocalDateTime();
        System.out.println(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDate));
        return DateTimeFormatter.ofPattern("HH:mm:ss").format(localDate);
    }


    /**
     * @param timeWithTZ : 2019-02-23T06:00:00.000+05:30
     * @return : 2019-02-23 06:00:00
     */
    public static String GetDateWithoutTZPlusDays(String timeWithTZ, Integer plusDays) {
        ZonedDateTime result = ZonedDateTime.parse(timeWithTZ, DateTimeFormatter.ISO_DATE_TIME);
        LocalDateTime localDate = result.toLocalDateTime().plusDays(plusDays);
        System.out.println(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDate));
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDate);
    }

    // @Step("Create epoch time and return in DateTime formatt ")
    public static DateTime convertTimetoEpoch(int noOfDay){
        DateTime date = DateTime.now().plus(noOfDay);
        return date;
    }

    /**
     * @param timeWithTZ : 2019-02-25T02:00:00+05:30
     * @return : java.util.Date
     */
    public static Date ConvertStringToDate(String timeWithTZ) {
        ZonedDateTime result = ZonedDateTime.parse(timeWithTZ, DateTimeFormatter.ISO_DATE_TIME);
        Date date = Date.from(result.toInstant());
        return date;
    }

    /**
     * @param timeWithTZ : 2019-02-23T06:00:00.000+05:30
     * @return : 2019-02-23 06:00:00
     */
    public static String GetDateWithoutTZPlusHours(String timeWithTZ, Integer plusHours) {
        ZonedDateTime result = ZonedDateTime.parse(timeWithTZ, DateTimeFormatter.ISO_DATE_TIME);
        LocalDateTime localDate = result.toLocalDateTime().plusHours(plusHours);
        System.out.println(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDate));
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDate);
    }

    /**
     * @param timeWithTZ : 2019-02-23T06:00:00.000+05:30
     * @return : 2019-02-23T06:00:00.000+05:30
     */
    public static String GetDateWithTZPlusHours(String timeWithTZ, Integer plusHours) {
        ZonedDateTime result = ZonedDateTime.parse(timeWithTZ, DateTimeFormatter.ISO_DATE_TIME);
        return DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ").format(result.plusHours(plusHours));
    }

    /**
     * @param timeWithTZ : 2019-02-23T06:00:00.000+05:30
     * @return : 2019-02-23T06:00:00.000+05:30
     */
    public static String GetDateWithTZPlusDays(String timeWithTZ, Integer plusDays) {
        ZonedDateTime result = ZonedDateTime.parse(timeWithTZ, DateTimeFormatter.ISO_DATE_TIME);
        return DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ").format(result.plusDays(plusDays));
    }

    /**
     * @param timeWithTZ (java.util.date): Tue Feb 26 09:00:00 IST 2019
     * @return : 2019-02-23 06:00:00
     */
    public static String GetTimeWithoutZone(String timeWithTZ) {
        Date date = null;
        try {
            date = new SimpleDateFormat("E MMM dd HH:mm:ss zzz yyyy").parse("Tue Feb 26 09:00:00 IST 2019");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("HH:mm:ss").format(date);
    }

    /**
     * @param timeWithTZ (java.util.date): Tue Feb 26 09:00:00 IST 2019
     * @return : 2019-02-23 06:00:00
     */
    public static String GetDateWithoutZone(String timeWithTZ) {
        Date date = null;
        try {
            date = new SimpleDateFormat("E MMM dd HH:mm:ss zzz yyyy").parse("Tue Feb 26 09:00:00 IST 2019");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("dd-MM-YYYY").format(date);
    }

    public static String GetTimeWithoutZone(Date scheduleStart) {
        return new SimpleDateFormat("HH:mm:ss").format(scheduleStart);
    }

    public static String GetDateWithoutZone(Date scheduleStart) {
        return new SimpleDateFormat("dd-MM-YYYY").format(scheduleStart);
    }


    /**
     * Generate Schedule Start and End Time in Date
     */

    public static Date generateScheduleStart() {
        DateTime dateTime = new DateTime().plusDays(1).plusHours(10);
        Date date = dateTime.toDate();
        return  date;
    }

    public static Date generateScheduleEnd() {
        DateTime dateTime = new DateTime().plusDays(1).plusHours(14);
        Date date = dateTime.toDate();
        return  date;
    }


    /**
     * DateTime helper Functions
     */
    public static String getTomorrowsDate() {
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
        return formatter.format(tomorrow);
    }

    public static String getNextDayDate(String date) {
        DateTimeFormatter sourceFormatter = DateTimeFormatter.ofPattern("d-MM-yyyy");  // default localdate format is return YYYY-MM-dd
        LocalDate nextDay = LocalDate.parse(date, sourceFormatter).plusDays(1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
        return formatter.format(nextDay);
    }

    public static String getCurrentDate(){
        LocalDate today = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
        return formatter.format(today);
    }

    public static String getCurrentDate(Integer plusDays){
        LocalDate today = LocalDate.now().plusDays(plusDays);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
        return formatter.format(today);
    }


    public static String getTime(Integer plusHours) {
        String timeStr = LocalTime.now().toString();
        DateTimeFormatter inFormat = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
        LocalTime time = LocalTime.parse(timeStr, inFormat).plusHours(plusHours);
        DateTimeFormatter outFormat = DateTimeFormatter.ofPattern("HH:mm:ss");
        return outFormat.format(time);
    }

    public static String getTime(String timeStr, Integer plusHours) {
        DateTimeFormatter inFormat = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime time = LocalTime.parse(timeStr, inFormat).plusHours(plusHours);
        DateTimeFormatter outFormat = DateTimeFormatter.ofPattern("HH:mm:ss");
        return outFormat.format(time);
    }

    public  static String referenceDateTimeConv(){
        String date = "2019-03-07 12:05:12.0";
        LocalDateTime ldt = LocalDateTime.parse(date , DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S"));
       System.out.println(ldt.minusDays(1));
        System.out.println(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(ldt));
        return ldt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * This method will generate the Date based on used format for current,future,past date
     * eg. (-1: past date, 0: current date, 1: future Date)
     * @param pattern
     * @param days
     * @return
     */
    public static String generateDate(String pattern,int days) {
        LocalDateTime tomorrow = LocalDateTime.now().plusDays(days);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return formatter.format(tomorrow);
    }

    /**
     * It will convert the date one format another format
     *
     * @param actualPattern   eg. E MMM dd HH:mm:ss Z yyyy
     * @param expectedPattern eg. yyyy-MM-dd HH:mm:ss
     * For Pattern refer this link : https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
     * @param date
     * @return
     * @throws ParseException
     */
    public static String convertDateFormat(String actualPattern, String expectedPattern, String date) throws ParseException {
        DateFormat originalFormat = new SimpleDateFormat(actualPattern, Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat(expectedPattern);
        String formattedDate = targetFormat.format(originalFormat.parse(date));
        //System.out.println(formattedDate);
        return formattedDate;
    }
}
