/**
 * 
 */
package com.myntra.apiTests.common.Utils;

import com.myntra.apiTests.common.Utils.DBHelperEnums.DBName;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;

/**
 * @author 17727
 * This helper will be used for ALL DB related Values
 *
 */
public class DBHelper {
	private static Logger log = Logger.getLogger(DBHelper.class);
	private enum ConditionType{
		where_condition,
		set_condition
	}
	
	/**
	 * @param keyValuePair
	 * @return
	 */
	private static String getParsedDataFromMap(Map<String,String> keyValuePair,ConditionType conditionType){
		Collection<String> keys = keyValuePair.keySet();
		int mapSize = keyValuePair.size();
		int i=1;
		String additionType = conditionType.equals(ConditionType.set_condition) ? ",":"and";
		
		String setQuery="";
		
		for(String key:keys){
			setQuery += "`"+key +"`="+ keyValuePair.get(key)+"";
			if(i!=mapSize){
				setQuery += additionType;
			}
			i++;
		}
		
		return setQuery;
	}
	
	/**
	 * @param <E>
	 * @param keyValuePair
	 * @param id
	 * @param tableName
	 */
	public static <E>  void updateDataInTable(Map<String,String> keyValuePair, Map<String,String> whereConditionPair,E tableName,DBName dbName){
		String query = "update `"+tableName+"` set "+getParsedDataFromMap(keyValuePair,ConditionType.set_condition)+" where "+getParsedDataFromMap(whereConditionPair,ConditionType.where_condition);
		System.out.println("Query: "+query);
		DBUtilities.exUpdateQuery(query,dbName.toString());
		//DBUtilities.exSelectQueryForSingleRecord()
	}
	
	
	 /**
     * @param <E> -- Name of the key we can take from EnumSCM ADDITIONAL_INFO_PACKAGING_TYPE
	 * @param tableName
	  * @throws SecurityException
	 * @throws NoSuchFieldException 
     * @throws UnsupportedEncodingException
     * @throws InterruptedException
     */
	public static <E extends Enum<E>> void updateDataInAdditionInfoTable(Map<String,String> keyValuePair, String id, E tableName, DBName dbName) throws NoSuchFieldException, SecurityException{
		
		String[] additionalTableEntry = tableName.toString().split(":");
		String additionalTableName = additionalTableEntry[0];
		String foreignKeyOfTable = additionalTableEntry[1];
		
		for(Map.Entry<String, String> entry:keyValuePair.entrySet()){
			String key = entry.getKey();
			String value = entry.getValue();
			String query = "update `"+additionalTableName+"` set `value`='"+value+"' where `key`='"+key+"' and `"+foreignKeyOfTable+"`='"+id+"';";
            log.info("Query: "+query);
            DBUtilities.exUpdateQuery(query, dbName.toString());
			
		}		

    }


}
