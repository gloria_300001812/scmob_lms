package com.myntra.apiTests.common.Utils.csvutility;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Shanmugam Yuvaraj
 * Since: Jul:2019
 */
public class CSVReader {

    /**
     * @param fileName
     * @return
     * @description : used to read CSV file and it will return List of data
     */
    public List<String> readCSVFile(String fileName) {
        Path pathToFile = Paths.get(fileName);
        List<String> lineString = new ArrayList<>();
        try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) {
            String line = br.readLine();
            while (line != null) {
                lineString.add(line);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return lineString;
    }

    /**
     * Used to Read all the lines in csv file and return List of map
     *
     * @param filePath
     * @return
     * @author : Shanmugam Yuvaraj
     */
    public List<Map<String, String>> readCsvAsListOfMap(String filePath) {
        Map<String, String> record = null;
        List<Map<String, String>> records = new ArrayList<>();
        Path pathToFile = Paths.get(filePath);
        int index = 0;
        try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) {
            String line = br.readLine();
            int lineNumber = 0;
            List<String> headers = new ArrayList<>();
            while (line != null) {
                if (lineNumber++ == 0) {
                    headers = Arrays.asList(line.split(","));
                } else {
                    record = new HashMap<>();
                    index = 0;
                    for (String value : line.split(",")) {
                        record.put(headers.get(index++), value);
                    }
                    records.add(record);
                }
                line = br.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return records;
    }


}

