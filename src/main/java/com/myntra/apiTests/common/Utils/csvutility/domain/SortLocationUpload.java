package com.myntra.apiTests.common.Utils.csvutility.domain;

import lombok.Data;
import java.util.List;


/**
 * @author Shanmugam Yuvaraj
 * Since: Jul:2019
 */

@Data
public class SortLocationUpload {
    private List<String> tenantId;
    private List<String> currentHub;
    private List<String> binLabel;
    private List<String> destinationLocationHubCode;
    private List<String> type;
    private List<String> shape;
    private List<String> color;
    private List<String> isActive;

}
