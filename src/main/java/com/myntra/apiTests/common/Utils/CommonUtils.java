/**
 * 
 */
package com.myntra.apiTests.common.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

import com.myntra.apiTests.end2end.FetchEnvUtil;
import com.myntra.apiTests.erpservices.oms.OMSHelpersEnums;
import com.myntra.scm.enums.Tenant;
import org.apache.lucene.analysis.util.CharArrayMap;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.testng.Assert;

import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.entries.ATPandIMSDataEntry;
import com.myntra.apiTests.common.entries.QueryParamEntry;
import com.myntra.apiTests.erpservices.atp.ATPServiceHelper;
import com.myntra.apiTests.erpservices.ims.IMSServiceHelper;
import com.myntra.commons.exception.ManagerException;

/**
 * @author 17727
 *
 */
public class CommonUtils {
	
	ATPServiceHelper atpServiceHelper = new ATPServiceHelper();
	IMSServiceHelper imsServiceHelper = new IMSServiceHelper();
	
	
	/**
	 * @param keys
	 * @param values
	 * @return
	 * This is a helper to get QueryParam Entry used by Test classes
	 */
	public QueryParamEntry queryParamEntry(List<String> keys,List<String> values){
		QueryParamEntry queryParamEntry = new QueryParamEntry();
		queryParamEntry.setKeys(keys);
		queryParamEntry.setValues(values);
		return queryParamEntry;
	}
	
	/**
	 * @param queryParamEntry
	 * @return
	 * @throws ManagerException
	 */
	public String getQueryParam(QueryParamEntry queryParamEntry) throws ManagerException{
		String queryparam = "?";
		List<String> keys = queryParamEntry.getKeys();
		List<String> values = queryParamEntry.getValues();
		int keysLength = keys.size();
		
		if(keys.isEmpty()||values.isEmpty()||(keys.size()!=values.size())){
			ExceptionHandler.fail("Either keys and Values are not correct or they are empty");
		}else{
			for(int i=0;i<keysLength;i++){
				queryparam += keys.get(i) +"="+values.get(i);
				if(i+1!=keysLength){
					queryparam += "&";
				}
			}
		}
		
		return queryparam;
	}
	
    public ATPandIMSDataEntry getATPAndIMSInventoryAndBOC(String skuID, Long warehouse28, String storeID, long sellerId_HEAL) throws Exception{
    	ATPandIMSDataEntry atPandIMSDataEntry = new ATPandIMSDataEntry();
    	
        HashMap<String, int[]> inventoryAndBOCCountInATP = atpServiceHelper
                .getAtpInvAndBlockOrderCount(new String[] { skuID });
        int[] blockAndInventoryCountATP = inventoryAndBOCCountInATP.get(skuID);

        atPandIMSDataEntry.setAtpInventoryCount(blockAndInventoryCountATP[0]);
        atPandIMSDataEntry.setAtpBOCCount(blockAndInventoryCountATP[1]);

        HashMap<String, int[]> inventoryAndBOCCountInIMS = imsServiceHelper
                .getIMSInvAndBlockOrderCount(new String[] { skuID }, ""+warehouse28,storeID,""+sellerId_HEAL);       
        int[] blockAndInventoryCountInIMS = inventoryAndBOCCountInIMS.get(skuID);

        atPandIMSDataEntry.setImsInventoryCount(blockAndInventoryCountInIMS[0]);
        atPandIMSDataEntry.setImsBOCCount(blockAndInventoryCountInIMS[1]);
        
        return atPandIMSDataEntry;
    }
    
    /**
     * This function is to verify Data in IMS and ATP
     * @param inventoryCountATPBeforePlacingOrder
     * @param inventoryCountATPAfterPlacingOrder
     * @param blockCountATPBeforePlacingOrder
     * @param blockCountATPAfterPlacingOrder
     * @param inventoryCountIMSBeforePlacingOrder
     * @param inventoryCountIMSAfterPlacingOrder
     * @param blockCountIMSBeforePlacingOrder
     * @param blockCountIMSAfterPlacingOrder
     */
    public void verfyIMSAndATPData ( int inventoryCountATPBefore, int inventoryCountATPAfter,
    int blockCountATPBefore, int blockCountATPAfter,
    int inventoryCountIMSBefore,
    int inventoryCountIMSAfter, int blockCountIMSBefore, int blockCountIMSAfter)
    {
    	Assert.assertEquals(inventoryCountATPAfter,inventoryCountATPBefore, "ATP Inventory Count After Order Place");
        Assert.assertEquals( blockCountATPAfter, blockCountATPBefore,"ATP Block Order Count After Order Place");
        Assert.assertEquals( inventoryCountIMSAfter, inventoryCountIMSBefore,"IMS Inventory Count After Order Place");
        Assert.assertEquals( blockCountIMSAfter, blockCountIMSBefore,"IMS Block Order Count After Order Place");
    }

	/**
	 * This function is to replace Variables
	 * @param path
	 * @param buyerId
	 * @param ownerId
	 * @return
	 */


    public String replaceMTVariablesWithValues(String path,String buyerId,String ownerId){

    	if(!buyerId.isEmpty()||!buyerId.equalsIgnoreCase("")||!buyerId.equals(null)){
			path = path.replace(OMSHelpersEnums.MTTenantVariables.BUYER_ID.getValue(),buyerId);
		}

		if(!ownerId.isEmpty()||!ownerId.equalsIgnoreCase("")||!ownerId.equals(null)){
			path = path.replace(OMSHelpersEnums.MTTenantVariables.OWNER_ID.getValue(),ownerId);
		}

		return path;
	}

	public String replaceMTVariablesWithValues(HashMap<String,String> pathparams,String url){
    	for(String key:pathparams.keySet()){
    		url=url.replace(key,pathparams.get(key));
		}
		return url;
	}
	public static ArrayList getDateWithGap(int gapInDays,String plusDaysOrMinus){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		DateTimeFormatter dateFormat8 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
		ArrayList arrayList=new ArrayList();
		Date currentDate = new Date();
		LocalDateTime localDateTime = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		arrayList.add(dateFormat8.format(localDateTime));
		if(plusDaysOrMinus.equalsIgnoreCase("plus")){
			localDateTime=localDateTime.plusDays(gapInDays);
		}else{
			localDateTime = localDateTime.minusDays(gapInDays);
		}
		Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
		arrayList.add(dateFormat.format(currentDatePlusOneDay));
		String value=dateFormat.format(currentDatePlusOneDay);
		System.out.println(value);
		return arrayList;
	}


	public static Date getDate() {
		DateTime date = new DateTime();
		org.joda.time.format.DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		String str = fmt.print(date);
		DateTime dt = fmt.parseDateTime(str);
		return dt.toDate();
	}
	
	public static String replaceMTVariablesWithValues(String path,String ownerId){

		if(!ownerId.isEmpty()||!ownerId.equalsIgnoreCase("")||!ownerId.equals(null)){
			path = path.replace(OMSHelpersEnums.MTTenantVariables.OWNER_ID.getValue(),ownerId);
		}

		return path;
	}

	public static String generateString(int size){
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < size) {
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		return salt.toString();
	}

}
