package com.myntra.apiTests.common.Utils;

public class RamdomGenerators {

    /**
     * used to generate random String with used size
     * @param sizeOfString
     * @return
     */
    public String generateRandomString(int sizeOfString) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        StringBuilder randomString = new StringBuilder(sizeOfString);
        for (int i = 0; i < sizeOfString; i++) {
            randomString.append(AlphaNumericString.charAt((int) (AlphaNumericString.length() * Math.random())));
        }
        return randomString.toString();
    }

}
