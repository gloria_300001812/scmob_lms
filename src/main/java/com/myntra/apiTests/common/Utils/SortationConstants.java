package com.myntra.apiTests.common.Utils;

/**
 * @author Shanmugam Yuvaraj
 * Since: Jul:2019
 */
public class SortationConstants {
    public static final String sortConfigFileName = "./Data/lms/csv/sortConfig.csv";
    public static final String sortLocationFileName = "./Data/lms/csv/sortLocation.csv";
}
