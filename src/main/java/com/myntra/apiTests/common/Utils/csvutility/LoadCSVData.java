package com.myntra.apiTests.common.Utils.csvutility;

import com.myntra.apiTests.common.Utils.csvutility.domain.SortConfigUpload;
import com.myntra.apiTests.common.Utils.csvutility.domain.SortLocationUpload;
import com.myntra.apiTests.erpservices.lms.Constants.FileUploadConstants;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Shanmugam Yuvaraj
 * Since: Jul:2019
 */
public class LoadCSVData implements StatusPoller {

    /***
     * @description : load all the data from CSV file to SortConfig POJO
     * @return sortConfigUpload
     */
    public SortConfigUpload loadSortConfigCSVDataToPOJO() {

        SortConfigUpload sortConfigUpload = new SortConfigUpload();
        List<String> tenantId = new ArrayList<>();
        List<String> clientId = new ArrayList<>();
        List<String> origin = new ArrayList<>();
        List<String> destination = new ArrayList<>();
        List<String> sortLevel = new ArrayList<>();
        List<String> sourceClientId = new ArrayList<>();
        List<String> courierCode = new ArrayList<>();
        List<String> shippingMethod = new ArrayList<>();
        List<String> nextLocation = new ArrayList<>();
        List<String> isActive = new ArrayList<>();

        List<Map<String, String>> lstLineData = csvReader.readCsvAsListOfMap(FileUploadConstants.sortConfigFileName);
        for (Map<String, String> strValue : lstLineData) {
            tenantId.add(strValue.get("tenantId"));
            clientId.add(strValue.get("clientId"));
            origin.add(strValue.get("origin"));
            destination.add(strValue.get("destination"));
            sortLevel.add(strValue.get("sortLevel"));
            sourceClientId.add(strValue.get("sourceClientId"));
            courierCode.add(strValue.get("courierCode"));
            shippingMethod.add(strValue.get("shippingMethod"));
            nextLocation.add(strValue.get("nextLocation"));
            isActive.add(strValue.get("Is Active"));

        }
        sortConfigUpload.setTenantId(tenantId);
        sortConfigUpload.setClientId(clientId);
        sortConfigUpload.setOrigin(origin);
        sortConfigUpload.setDestination(destination);
        sortConfigUpload.setSortLevel(sortLevel);
        sortConfigUpload.setSourceClientId(sourceClientId);
        sortConfigUpload.setCourierCode(courierCode);
        sortConfigUpload.setShippingMethod(shippingMethod);
        sortConfigUpload.setNextLocation(nextLocation);
        sortConfigUpload.setIsActive(isActive);
        return sortConfigUpload;
    }


    /***
     * @description : load all the data from CSV file to SortLocation POJO
     * @return sortLocationUpload
     */
    public SortLocationUpload loadSortLocationCSVDataToPOJO() {

        SortLocationUpload sortLocationUpload = new SortLocationUpload();

        List<String> tenantId = new ArrayList<>();
        List<String> currentHub = new ArrayList<>();
        List<String> binLabel = new ArrayList<>();
        List<String> destinationLocationHubCode = new ArrayList<>();
        List<String> type = new ArrayList<>();
        List<String> shape = new ArrayList<>();
        List<String> color = new ArrayList<>();
        List<String> isActive = new ArrayList<>();

        List<Map<String, String>> lstLineData = csvReader.readCsvAsListOfMap(FileUploadConstants.sortLocationFileName);
        for (Map<String, String> strValue : lstLineData) {
            tenantId.add(strValue.get("Tenant Id"));
            currentHub.add(strValue.get("Current Hub"));
            binLabel.add(strValue.get("Bin Label"));
            destinationLocationHubCode.add(strValue.get("Destination Location Hub Code"));
            type.add(strValue.get("Type"));
            shape.add(strValue.get("Shape"));
            color.add(strValue.get("Color"));
            isActive.add(strValue.get("Is Active"));

        }
        sortLocationUpload.setTenantId(tenantId);
        sortLocationUpload.setCurrentHub(currentHub);
        sortLocationUpload.setBinLabel(binLabel);
        sortLocationUpload.setDestinationLocationHubCode(destinationLocationHubCode);
        sortLocationUpload.setType(type);
        sortLocationUpload.setShape(shape);
        sortLocationUpload.setColor(color);
        sortLocationUpload.setIsActive(isActive);
        return sortLocationUpload;
    }

}
