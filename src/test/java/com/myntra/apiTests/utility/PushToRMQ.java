package com.myntra.apiTests.utility;

import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.client.wms.codes.WmsErrorCodes;
import com.myntra.client.wms.codes.WmsSuccessCodes;
import com.myntra.commons.amqp.AmqpMessagePublisher;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.RabbitConfigNotFoundException;
import com.myntra.commons.exception.RabbitMessageCouldNotBeSentException;
import com.myntra.commons.response.EmptyResponse;
import com.myntra.oms.client.entry.ReleaseUpdateEntry;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by Sneha.Agarwal on 15/09/18.
 */
public class PushToRMQ {
    static org.slf4j.Logger log = LoggerFactory.getLogger(PushToRMQ.class);
    public EmptyResponse sendAcknowledgment(Long orderRelaseId) {
        ReleaseUpdateEntry releaseUpdateEntry = new ReleaseUpdateEntry();
        releaseUpdateEntry.setReleaseId(orderRelaseId);
        releaseUpdateEntry.setStatus(ReleaseStatus.WP.name());
        releaseUpdateEntry.setUpdatedOn(new Date());
        EmptyResponse emptyResponse = new EmptyResponse();
        try {
            AmqpMessagePublisher.sendMessageAsXml(releaseUpdateEntry, "oms", "orderEvents", null);
            emptyResponse.setStatus(new StatusResponse(WmsSuccessCodes.ORDER_RELEASE_UPDATED, StatusResponse.Type.SUCCESS, 1));

        } catch (RabbitConfigNotFoundException e) {
            log.error("Error pushing OrderRelease acknowledgment ", e);
            emptyResponse.setStatus(new StatusResponse(WmsErrorCodes.ERR_INSERTING, StatusResponse.Type.ERROR));
        } catch (RabbitMessageCouldNotBeSentException e) {
            log.error("Error pushing OrderRelease acknowledgment", e);
            emptyResponse.setStatus(new StatusResponse(WmsErrorCodes.ERR_INSERTING, StatusResponse.Type.ERROR));
        }
        return emptyResponse ;
    }
}
