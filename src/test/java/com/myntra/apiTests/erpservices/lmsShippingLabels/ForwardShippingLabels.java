package com.myntra.apiTests.erpservices.lmsShippingLabels;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.ShippingLabel.lms.B2BShippingLabel;
import com.myntra.apiTests.erpservices.ShippingLabel.lms.LabelParsers;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.tests.LastMileTestDP;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.test.SortationReturnFlowsTemp;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.Svc;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.apache.tika.exception.TikaException;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ForwardShippingLabels implements StatusPoller {

    private B2BShippingLabel b2BShippingLabel=new B2BShippingLabel();
    LabelParsers labelParsers = new LabelParsers();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    SortationReturnFlowsTemp sortationReturnFlowsTemp=new SortationReturnFlowsTemp();
    SortationHelper sortationHelper = new SortationHelper();
    LMSOperations lmsOperations = new LMSOperations();


    @Test(enabled = true,priority = 2,description = "C22047 : create B2B Shipment and check the validation for shipping label")
    public void validate_b2bShippingLabel() throws IOException, TikaException, SAXException {
        String clientId = LMS_CONSTANTS.B2B_CLIENTID;
        String warehouseId = LMS_CONSTANTS.WAREHOUSE_36;
        String rtoWarehouseId = LMS_CONSTANTS.WAREHOUSE_36;
        String integrationId = "2297";
        String sourcePath = "WMS";
        String pincode = "100002";
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        String courierCode = "ML";
        int noOfItem = 2;
        boolean isCod = false;
        String destinationPremisesId = "28";
        String shipmentType = "B2B";
        String statusMessage = "Shipment Creation Successful";
        String statusType = EnumSCM.SUCCESS;
        String cod = "";
        String trackingNumber ="";
        if (isCod) {
            cod = "cod";
        }
        trackingNumber=lmsServiceHelper.getTrackingNumber(courierCode, warehouseId, cod, pincode, "").getTrackingNumberEntry().getTrackingNumber();
        Svc service=tmsServiceHelper.createShipment(clientId,trackingNumber,warehouseId,rtoWarehouseId,integrationId,sourcePath,pincode,shippingMethod,courierCode,noOfItem,isCod,destinationPremisesId,shipmentType);
        lmsServiceHelper.validateCreateShipment(service,statusMessage,statusType);
        Map<String, Object> dbUtilesData= DBUtilities.exSelectQueryForSingleRecord("select * from order_to_ship where tracking_number='"+trackingNumber+"'","lms");
        String orderId= (String) dbUtilesData.get("order_id");
        String hubCode= (String) dbUtilesData.get("dispatch_hub_code");
        String destinationHubCode= (String) dbUtilesData.get("destination_hub_code");
        if(APIUtilities.getElement(service.getResponseBody(),"status.statusMessage","json").equalsIgnoreCase("Shipment Creation Successful")) {
            //Data from DB Validation Using API
            OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsUsingTrackingNumber(trackingNumber);
            Assert.assertEquals(orderResponse.getOrders().get(0).getTrackingNumber(), trackingNumber, "Tracking number mismatch");
            Assert.assertEquals(orderResponse.getOrders().get(0).getWarehouseId(),warehouseId,"Warehouse mismatch");
            Assert.assertEquals(orderResponse.getOrders().get(0).getRtoWarehouseId(),rtoWarehouseId,"RTO Ware house Id mismatch");
        }
        System.out.println(String.format("B2B trackingNumber[%s]",trackingNumber));
        Map<String, String> orderShippingLabelData=b2BShippingLabel.b2bShippingLabelData(orderId, LASTMILE_CONSTANTS.TENANT_ID,"9999");
        Assert.assertEquals(orderId, orderShippingLabelData.get("ShipmentID"), "Shipment id is not matching ");
        Assert.assertEquals(hubCode,orderShippingLabelData.get("sourceHub"),"Source hub is not matching ");
        Assert.assertEquals(destinationHubCode,orderShippingLabelData.get("destinationHub"),"destination hub is not matching ");
    }

    @Test( enabled = true,priority = 3, dataProviderClass = LastMileTestDP.class, dataProvider = "checkShippingMenthod", description = "ID: C22049 , Check shipping method type in shipping label for NORMAL,EXPRESS,SDD")
    public void testShippingMethod_InForwardShippingLabel(String shippingMethod) throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, shippingMethod, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.PACKED);
        String shippingLabelV2 = labelParsers.parseShippingLabelV2(packetId, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(shippingLabelV2.contains(shippingMethod),true,"shipping method "+shippingMethod+" is not matching");
    }

    @Test( enabled = true,priority = 4, dataProviderClass = LastMileTestDP.class, dataProvider = "forwardOrdersShippingLabel", description = "ID: C22049 , Check forward, TryAndBuy,Exchange Shipping labels")
    public void process_ShippingValidationFwdOrders_Successful(String orderType,String expectedOrderValue,boolean bFlag) throws Exception {
        if(!(orderType.equalsIgnoreCase("Exchange"))){
            String orderID = lmsHelper.createMockOrder(EnumSCM.PK, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", bFlag, true);
            String packetId = omsServiceHelper.getPacketId(orderID);
            OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
            String codAmount= String.valueOf(orderResponse.getOrders().get(0).getCodAmount());
            statusPollingValidator.validateOrderStatus(packetId,ShipmentStatus.PACKED);
            String strShippingLabel = labelParsers.parseShippingLabelV2(packetId, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
            Assert.assertEquals(strShippingLabel.contains(expectedOrderValue),true,"Forward orders Fwd is not matching with shipping label");
            Assert.assertEquals(strShippingLabel.contains(codAmount),true,"Cod amount is not matching for Fwd orders");
        }
        else{
            String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR,  LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
            String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.PK, LMS_PINCODE.ML_BLR,  LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
            String packetId = omsServiceHelper.getPacketId(exchangeOrder);
            OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
            String codAmount= String.valueOf(orderResponse.getOrders().get(0).getCodAmount());
            statusPollingValidator.validateOrderStatus(packetId,ShipmentStatus.PACKED);
            String strShippingLabel = labelParsers.parseShippingLabelV2(packetId, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
            Assert.assertEquals(strShippingLabel.contains(expectedOrderValue),true,"Forward orders Fwd is not matching with shipping label");
        }
    }

    @Test(enabled = true,priority = 5,description = "verify shipping label functionality for forward orders")
    public void test_SortationShippingLablel_ForwardOrders() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, "100002", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.PACKED);
        String shippingLabelV2 = labelParsers.parseShippingLabelV2(packetId, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(shippingLabelV2.contains("100002"),true,"shipping pincode is not matching");
        Assert.assertEquals(shippingLabelV2.contains("DELHIS"),true,"shipping DC is not matching");
        Assert.assertEquals(shippingLabelV2.contains("NORTH"),true,"shipping NORTH is not matching");
        Assert.assertEquals(shippingLabelV2.contains("DDC"),true,"shipping DDC is not matching");
        Assert.assertEquals(shippingLabelV2.contains("Rs.1099.0"),true,"shipping amount is not matching");
        Assert.assertEquals(shippingLabelV2.contains("Fwd"),true,"Forward order is not matching");
        Assert.assertEquals(shippingLabelV2.contains("NORMAL"),true,"shipping Method is not matching");
    }

    @Test(enabled = true,priority = 6,description = "verify shipping label functionality for TnB orders")
    public void test_SortationShippingLablel_TnBOrders() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, "100002", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.PACKED);
        String shippingLabelV2 = labelParsers.parseShippingLabelV2(packetId, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(shippingLabelV2.contains("100002"),true,"shipping pincode is not matching");
        Assert.assertEquals(shippingLabelV2.contains("DELHIS"),true,"shipping DC is not matching");
        Assert.assertEquals(shippingLabelV2.contains("NORTH"),true,"shipping NORTH is not matching");
        Assert.assertEquals(shippingLabelV2.contains("DDC"),true,"shipping DDC is not matching");
        Assert.assertEquals(shippingLabelV2.contains("Rs.1099.0"),true,"shipping amount is not matching");
        Assert.assertEquals(shippingLabelV2.contains("TnB"),true,"Forward order is not matching");
        Assert.assertEquals(shippingLabelV2.contains("NORMAL"),true,"shipping Method is not matching");
    }

    @Test(enabled = true,priority = 7,description = "verify shipping label functionality for Exchange orders")
    public void test_SortationShippingLablel_ForExchangeOrders() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        Map<String, Object> orderMap, exchangeOrderMap;
        List<String> trackingNumbers, packetIds, orderIds, returnIds;
        Map<String, Object> returnData;
        Map<String, String> containerDetails;
        List<String> exchangeTrackingNumbers, returnTrackingNumbers, exchangePacketIds, exchangeOrderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null, null);
        SortConfig returnSortConfig = new SortConfig(SortConfig.SortConfigType.RETURN);
        Map<String, SortationData> returnSortConfigData = returnSortConfig.getSortConfig(null, null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setSortConfigReturn(returnSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setPaymentMode("on");
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = sortationReturnFlowsTemp.CreateDLOrderUsingSotation(numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");

        exchangeOrderMap = lmsOperations.createMockExchangeOrders(orderData, EnumSCM.PK);
        exchangeOrderIds = (List) exchangeOrderMap.get("exchangeOrderIds");
        exchangeTrackingNumbers = (List) exchangeOrderMap.get("exTrackingNumbers");
        exchangePacketIds = (List) exchangeOrderMap.get("exPacketIds");
        String shippingLabelV2 = labelParsers.parseShippingLabelV2(exchangePacketIds.get(0), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(shippingLabelV2.contains("Rs.0.0"),true,"shipping method Rs.0.0 is not matching");
        Assert.assertEquals(shippingLabelV2.contains("100002"),true,"shipping pincode is not matching");
        Assert.assertEquals(shippingLabelV2.contains("DELHIS"),true,"shipping DC is not matching");
        Assert.assertEquals(shippingLabelV2.contains("NORTH"),true,"shipping NORTH is not matching");
        Assert.assertEquals(shippingLabelV2.contains("DDC"),true,"shipping DDC is not matching");
        Assert.assertEquals(shippingLabelV2.contains("EXCHANGE"),true,"Forward order is not matching");
        Assert.assertEquals(shippingLabelV2.contains("NORMAL"),true,"shipping Method is not matching");
    }
}
