package com.myntra.apiTests.erpservices.sortation.test;

import com.myntra.apiTests.common.Utils.csvutility.LoadCSVData;
import com.myntra.apiTests.erpservices.lms.Constants.FileUploadConstants;
import com.myntra.apiTests.common.Utils.csvutility.domain.SortConfigUpload;
import com.myntra.apiTests.common.Utils.csvutility.domain.SortLocationUpload;
import com.myntra.apiTests.erpservices.sortation.helpers.SortationUploadValidator;
import com.myntra.apiTests.erpservices.lmsUploder.LMSUploadHelper;
import com.myntra.lms.client.status.BulkJobType;
import org.testng.annotations.Test;

/**
 * @author Shanmugam Yuvaraj
 * @since July-2019
 */

public class SortationConfigurationTest {
    SortationUploadValidator sortationUploadValidator = new SortationUploadValidator();
    LoadCSVData loadCSVData = new LoadCSVData();
    LMSUploadHelper lmsUploadHelper=new LMSUploadHelper();

    @Test(enabled = true, description = "Validate sort config upload")
    public void sortConfigUploadConfiguration() throws Exception {
        SortConfigUpload sortConfigUpload = loadCSVData.loadSortConfigCSVDataToPOJO();
        lmsUploadHelper.configurationUpload(BulkJobType.SORTATION_CONFIG_UPLOAD.name(), FileUploadConstants.sortConfigFileName);
        //validate uploaded data with DB data
        sortationUploadValidator.validateSortConfigUploadSuccessful(sortConfigUpload);

    }

    @Test(enabled = true, description = "Validate sortLocation upload")
    public void sortLocationUploadConfiguration() throws Exception {

        SortLocationUpload sortLocationUpload = loadCSVData.loadSortLocationCSVDataToPOJO();
        lmsUploadHelper.configurationUpload(BulkJobType.SORT_LOCATION_UPLOAD.name(), FileUploadConstants.sortLocationFileName);
        //validate uploaded data with DB data
        sortationUploadValidator.validateSortLocationUploadSuccessful(sortLocationUpload);
    }
}


