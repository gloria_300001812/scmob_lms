package com.myntra.apiTests.erpservices.sortation.test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Jun-2019
 */
public class SortationReturnFlowsTemp {
    String env = getEnvironment();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TMSOperations tmsOperations = new TMSOperations();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();


    public Map<String, Object> CreateDLOrderUsingSotation(Integer numberOfShipments) throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Last Mile Operations
        lastmileOperations.deliverOrders(orderData.getZipcode(), trackingNumbers);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("trackingNumbers:[%s], packetIds:[%s], orderIds:[%s]",trackingNumbers, packetIds, orderIds));
        return orderMap;
    }

    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DH-BLR -> LABEL(NORTH) -> DH-DEL -> LABEL(DDC) -> DELHIS
     * Note: All packets go to same destination.
     */
    @Test(description = "ID:[C19417 , C19414, C19423, C19424]")
    public void testSortationExchange() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap, exchangeOrderMap;
        List<String> trackingNumbers , packetIds,  orderIds, returnIds;
        Map<String, Object> returnData;
        Map<String, String>  containerDetails;
        List<String> exchangeTrackingNumbers , returnTrackingNumbers,  exchangePacketIds,  exchangeOrderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        SortConfig returnSortConfig = new SortConfig(SortConfig.SortConfigType.RETURN);
        Map<String, SortationData> returnSortConfigData = returnSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setSortConfigReturn(returnSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setPaymentMode("on");
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = CreateDLOrderUsingSotation(numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");

        exchangeOrderMap = lmsOperations.createMockExchangeOrders(orderData , EnumSCM.PK);
        exchangeOrderIds = (List) exchangeOrderMap.get("exchangeOrderIds");
        exchangeTrackingNumbers = (List) exchangeOrderMap.get("exTrackingNumbers");
        exchangePacketIds = (List) exchangeOrderMap.get("exPacketIds");
        orderData.setExchangeOrderMap(exchangeOrderMap);
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2ForExchangeOrders(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, exchangeTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, exchangeTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, exchangeTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, exchangeTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Last Mile Operations
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        Long deliveryCenterId = lmsOperations.getDeliveryCenterID(orderData.getZipcode());
        System.out.println(lmsOperations.getDeliveryCenterID(orderData.getZipcode()));
        long tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(orderData.getZipcode())).get("tripId"));
        lastmileOperations.assignMultipleOrdersToTripByTrackingNumbers(exchangeTrackingNumbers, tripId);
        lastmileOperations.startTrip(tripId, exchangeTrackingNumbers);

        List<OrderEntry> orderEntries;
        //Trip Update
        orderEntries = lastmileOperations.setOperationalTrackingIds(exchangeTrackingNumbers);
        returnData = lastmileOperations.updateExchangeTrip(tripId, exchangePacketIds, orderEntries);
        returnTrackingNumbers = (List<String>) returnData.get("returnTrackingNumbers");
        returnIds = (List<String>) returnData.get("returnIds");
        //Recieve Shipments
        lastmileOperations.recieveExchangeTripOrders(exchangeTrackingNumbers);
        //Trip Complete
        lastmileOperations.completeExchangeTrip(tripId, exchangePacketIds, exchangeOrderIds);

        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("exchangeTrackingNumbers:%s , returnTrackingNumbers:%s returnIds:%s",exchangeTrackingNumbers, returnTrackingNumbers, returnIds));

        //Proceed further with returnTrackingNumbers to return back to WH

        //Return Path: DELHIS -> LABEL(RT-NORTH-DEL) -> RT-DEL -> LABEL(RT-SOUTH-BLR) -> RT-BLR

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);
        orderData.setRtOriginHubCode(currentHub);
        orderData.setRtDestinationHubCode(sortationHelper.getDestinationSortLocation(returnTrackingNumbers.get(0), orderData.getCurrentHub()));

        sortationHelper.computeAndPrintCompletePath(orderData.getRtOriginHubCode(), orderData.getRtDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.RETURN_SUCCESSFUL, com.myntra.lms.client.status.OrderStatus.RT);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi Sortation DC -> Delhi Transport Hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //TMS Operations (Delhi transport hub -> Delhi returns hub (RT-DEL))
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Create Master Bag:-Delhi returns hub-HUB  To Bangalore returns hub-HUB - 276777

        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi transport hub to Bangalore transport hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.DELIVERED_TO_SELLER, com.myntra.lms.client.status.OrderStatus.DELIVERED_TO_SELLER);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("exchangeTrackingNumbers:%s , returnTrackingNumbers:%s returnIds:%s",exchangeTrackingNumbers, returnTrackingNumbers, returnIds));
    }

    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DELHIS -> LABEL(RT-NORTH-DEL) -> RT-DEL -> LABEL(RT-SOUTH-BLR) -> RT-BLR
     * Note: All packets go to same destination.
     */
    @Test(description = "ID:[C19412, C19414, C19423, C19424]")
    public void testSortationReturnFlow() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        List<String> trackingNumbers, returnTrackingNumbers, packetIds, returnPacketIds, orderIds;
        List<String> returnIds = new ArrayList<>();
        Map<String, Object> orderMap;
        Map<String, Object> returnOrderMap;
        Map<String, String>  containerDetails;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        SortConfig returnSortConfig = new SortConfig(SortConfig.SortConfigType.RETURN);
        orderData.setSortConfigForward(forwardSortConfigData);
        Map<String, SortationData> returnSortConfigData = returnSortConfig.getSortConfig(null,null);
        orderData.setSortConfigReturn(returnSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setRtOriginHubCode("DELHIS");
        orderData.setRtDestinationHubCode("RT-BLR");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setReturnType(ReturnType.NORMAL);
        orderData.setCurrentHub(orderData.getRtOriginHubCode());
        String currentHub = orderData.getRtOriginHubCode();
        String nextHub = orderData.getRtDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        sortationHelper.computeAndPrintCompletePath(orderData.getRtOriginHubCode(), orderData.getRtDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = CreateDLOrderUsingSotation(numberOfShipments);
        returnOrderMap = lmsOperations.createMockReturnOrders((List) orderMap.get("orderIds"), orderData.getZipcode(), orderData.getReturnType());
        orderData.setReturnOrderMap(returnOrderMap);
        returnTrackingNumbers = (List<String>) returnOrderMap.get("returnTrackingNumbers");
        returnPacketIds = (List) returnOrderMap.get("returnPacketIds");
        returnIds = (List) returnOrderMap.get("returnIds");
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_CREATED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_CREATED, ShipmentUpdateEvent.PICKUP_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);


        //Create Return Trip -> assign Pickup Trip -> Start Trip -> Update Trip -> Recieve Order in DC
        //Create MB and so on

        long returnTripId;
        returnTripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(orderData.getZipcode())).get("tripId"));
        lastmileOperations.scanTrackingNumbersInTrip(returnTripId, returnTrackingNumbers);
        lastmileOperations.startTrip(returnTripId, returnTrackingNumbers);
        lastmileOperations.updatePickupTrip(returnTripId, returnTrackingNumbers);
        lastmileOperations.recieveTripOrders(returnTripId, returnTrackingNumbers);
        lastmileOperations.completeReturnTrip(returnTripId);

        //Get next Hub
        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        System.out.println(String.format("returnTrackingNumbers[%s] , returnIds[%s]",returnTrackingNumbers, returnIds));

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getRtOriginHubCode(),nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        //sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode());

        //TMS Operations (Delhi Sortation DC -> Delhi Transport Hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //TMS Operations (Delhi transport hub -> Delhi returns hub (RT-DEL))
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Create Master Bag:-Delhi returns hub-HUB  To Bangalore returns hub-HUB - 276777

        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi transport hub to Bangalore transport hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.DELIVERED_TO_SELLER, com.myntra.lms.client.status.OrderStatus.DELIVERED_TO_SELLER);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("returnTrackingNumbers[%s] , returnIds[%s]",returnTrackingNumbers, returnIds));
    }
}
