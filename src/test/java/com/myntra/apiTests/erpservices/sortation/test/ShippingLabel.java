package com.myntra.apiTests.erpservices.sortation.test;

import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import org.testng.annotations.Test;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class ShippingLabel {
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();

    @Test
    public void testShippingLabelV2() throws Exception {
        String pdfTextBody = lmsServiceHelper.parseShippingLabelV2("3864127471697783", LMS_CONSTANTS.TENANTID,  LMS_CONSTANTS.CLIENTID);
        String[] pdfTextByLines = pdfTextBody.trim().split("[\\r\\n]+");

        System.out.println(pdfTextByLines);
        String sort1 =  pdfTextByLines[pdfTextByLines.length-1];
        String sort2 =  pdfTextByLines[pdfTextByLines.length-2];
        String sort3 =  pdfTextByLines[pdfTextByLines.length-3];
    }

    @Test
    public void testMasterBagLabel(){

    }
}
