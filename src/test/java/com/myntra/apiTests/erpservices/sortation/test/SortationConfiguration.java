package com.myntra.apiTests.erpservices.sortation.test;

import com.myntra.apiTests.erpservices.sortation.configuploader.SortationConfigUploader;
import com.myntra.apiTests.erpservices.sortation.configuploader.pojos.UploadMetadataResponse;
import org.testng.annotations.Test;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class SortationConfiguration {

    /**
     * Testcases to be automated from below testrail link:
     * https://myntra.testrail.net/index.php?/suites/view/6&group_by=cases:section_id&group_id=2177&group_order=asc
     * enum is not yet defined : JobTypes: SORT_LOCATION_UPLOAD, SORTATION_CONFIG_UPLOAD
     */
    @Test
    public void testConfiguration() throws Exception {
        String filePath = "src/test/resources/SortationConfigs/SortLocationConfig.csv";
        String awsFileUrl;
        Integer jobId;
        Boolean jobStatus = false;
        SortationConfigUploader sortationConfigUploader = new SortationConfigUploader();
        UploadMetadataResponse uploadMetadataResponse =  sortationConfigUploader.getMetadataForUpload();
        awsFileUrl =  sortationConfigUploader.uploadFileToAWS(filePath, uploadMetadataResponse);
        jobId = sortationConfigUploader.submitS3Job(awsFileUrl, filePath , "SORT_LOCATION_UPLOAD");
        jobStatus = sortationConfigUploader.getJobFinalStatus(jobId);
        if(jobStatus){
            //continue testing configuration
        }
    }
}
