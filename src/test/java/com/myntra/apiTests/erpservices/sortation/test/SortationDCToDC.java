package com.myntra.apiTests.erpservices.sortation.test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class SortationDCToDC {
    String env = getEnvironment();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TMSOperations tmsOperations = new TMSOperations();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    MasterBagValidator masterBagValidator = new MasterBagValidator();

    /**
     * Path: DH-BLR -> LABEL(NORTH) -> DH-DEL -> LABEL(DDC) -> DELHIS -> Delhi dc
     */
    @Test(description = "ID:[C19429 , C19430, C19431, C19432]" )//, retryAnalyzer = Retry.class)
    public void testSortationDC2DC() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        String destinationDC = "DEL-DC", destinationDCpinCode = "100009";
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //DC to DC Transfer

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);
        nextHub = destinationDC;
        orderData.setNextHub(destinationDC);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Last Mile Operations
        lastmileOperations.deliverOrders(destinationDCpinCode, trackingNumbers);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println("trackingNumbers:"+trackingNumbers);
    }

}
