package com.myntra.apiTests.erpservices.sortation.test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.SortationProcessHelper;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.TryAndBuyItemStatus;
import com.myntra.sortation.domain.SortLocationType;
import com.myntra.sortation.response.SortationConfigResponse;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import edu.emory.mathcs.backport.java.util.Arrays;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Jul-2019
 */
public class SortatationOptimized {
    String env = getEnvironment();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TMSOperations tmsOperations = new TMSOperations();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    SortationProcessHelper sortationProcessHelper = new SortationProcessHelper();


    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DH-BLR -> LABEL(NORTH) -> DH-DEL -> LABEL(DDC) -> DELHIS
     * Note: All packets go to same destination.
     */
    @Test(description = "ID:[C19423, C19424]")
    public void testSortationForwardFlowMultiHop() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        Map<String, Object> orderMap;
        List<String> trackingNumbers, packetIds, orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null, null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s", currentHub, nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s", currentHub, nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(), nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId, orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s", currentHub, nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(), nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId, orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s", currentHub, nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Last Mile Operations
        lastmileOperations.deliverOrders(orderData.getZipcode(), trackingNumbers);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("trackingNumbers:[%s], packetIds:[%s], orderIds:[%s]", trackingNumbers, packetIds, orderIds));
    }


    @Test(description = "ID:[C19423, C19424]")
    public void testSortationForwardFlowMultiHopOptimized() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        Map<String, Object> orderMap;
        List<String> trackingNumbers, packetIds, orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null, null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s", currentHub, nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s", currentHub, nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        sortationProcessHelper.processFromIStoSH(currentHub, trackingNumbers, orderData.getShippingMethod(), orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,orderData.getTenantId(), LMS_CONSTANTS.CLIENTID);

        //Last Mile Operations
        lastmileOperations.deliverOrders(orderData.getZipcode(), trackingNumbers);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        System.out.println(String.format("trackingNumbers:[%s], packetIds:[%s], orderIds:[%s]", trackingNumbers, packetIds, orderIds));
    }

    @Test(invocationCount = 1, enabled = true)
    public void testSanity() throws Exception{
        LMSHelper lmsHelper = new LMSHelper();
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        String orderId = lmsHelper.createMockOrder(EnumSCM.IS, "560068", "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));
        //currentHub, trackingNumbers, orderData.getShippingMethod(), orderData.getCourierCode(), orderData.getTenantId()
        sortationProcessHelper.processFromIStoSH("DH-BLR", Arrays.asList(new String[]{trackingNumber}), ShippingMethod.NORMAL, CourierCode.ML.toString(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Last Mile Operations
        lastmileOperations.deliverOrders("560068", Arrays.asList(new String[]{trackingNumber}));
        sortationValidator.validateOrderTrackingDetail(Arrays.asList(new String[]{trackingNumber}), ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, CourierCode.ML.toString(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));
    }


    @Test(description = "ID:[C19413, C19414, C19423, C19424]")
    public void testSortationTriedAndNotBought() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap;
        Map<String, Object> returnData;
        Map<String, String>  containerDetails;
        List<String> trackingNumbers , packetIds,  orderIds, returnTrackingNumbers, returnIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        SortConfig returnSortConfig = new SortConfig(SortConfig.SortConfigType.RETURN);
        Map<String, SortationData> returnSortConfigData = returnSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setSortConfigReturn(returnSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setRtDestinationHubCode("RT-BLR");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(true);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Last Mile Operations
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        Long deliveryCenterId = lmsOperations.getDeliveryCenterID(orderData.getZipcode());
        System.out.println(lmsOperations.getDeliveryCenterID(orderData.getZipcode()));
        long tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(orderData.getZipcode())).get("tripId"));
        lastmileOperations.assignMultipleOrdersToTripByTrackingNumbers(trackingNumbers, tripId);
        lastmileOperations.startTrip(tripId, trackingNumbers);
        returnData = lastmileOperations.updateTryAndBuyTrip(String.valueOf(tripId),
                orderData.getTrackingNumbers(),
                orderData.getPacketIds(),
                orderData.getOrderIds(),
                TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT, deliveryCenterId);
        returnTrackingNumbers = (List<String>) returnData.get("returnTrackingNumbers");
        returnIds = (List<String>) returnData.get("returnIds");

        //reconcile
        lastmileOperations.receiveTryAndBuyTripOrders(returnTrackingNumbers);

        lastmileOperations.completeTrip(trackingNumbers);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("trackingNumbers:%s , returnTrackingNumbers:%s returnIds:%s",trackingNumbers, returnTrackingNumbers, returnIds));

        //Proceed further with returnTrackingNumbers to return back to WH
        //get next location
        //Create MB from DELHIS (6309) to RT-DEL (Delhi Returns Hub 21)
        //Add shipment PUT: /masterbag-service/masterbag/status
        //{"tenantId":"4019","id":"276759","forceUpdate":"false","event":"SHIPMENTS_ADDED","masterbagShipments":[{"trackingNumber":"ML0001554000","shipmentType":"PU","orderId":null,"sourceReturnId":"4001557785"}]}
        //MBID: 276759 , containerId: 215071, Recieve MB
        //Create Container [Delhi Sortation DC to Delhi Transport Hub , lane: Delhi Intracity Lane , Transporter: Rivgo]
        //Recieve Container at ['Delhi transport hub']
        // Recieve MasterBag
        // Create Container(215080) [Delhi transport hub To Delhi returns hub] lane: Delhi intercity lane , Transporter: Rivigo
        // add MB to container and ship -> Recieve container at[Delhi returns hub]
        // MasterBag Inscan V2 [Delhi returns hub-HUB] -> Scan Tracking number & validate the next location
        // Update scan masterBAg
        // Create Master Bag:-Delhi returns hub-HUB  To Bangalore returns hub-HUB - 276777
        // Received masterBag:-Delhi transport hub
        // Container -Delhi transport hub to Bangalore transport hub , lane: DEL-BLR , transporter: Rivigo
        // ContainerId: 215086
        // Recieve Container: Bangalore transport hub and recieve MB 276777
        // masterBagInscan V2:- Bangalore returns hub-HUB
        // sacn TN: ML0001554000 - Verify Next sort Loaction  -- return_shipment DELIVERED_TO_SELLER , pickup_shipment , order_tracking,
        //shipment_activity_detail


        //Return Path: DELHIS -> LABEL(RT-NORTH-DEL) -> RT-DEL -> LABEL(RT-SOUTH-BLR) -> RT-BLR

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);
        orderData.setRtDestinationHubCode(sortationHelper.getDestinationSortLocation(returnTrackingNumbers.get(0), orderData.getCurrentHub()));

        sortationHelper.computeAndPrintCompletePath(orderData.getDestinationHubCode(), orderData.getRtDestinationHubCode() , orderData.getClientId(), orderData.getCourierCode());
        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.RETURN_SUCCESSFUL, com.myntra.lms.client.status.OrderStatus.RT);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi Sortation DC -> Delhi Transport Hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //TMS Operations (Delhi transport hub -> Delhi returns hub (RT-DEL))
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Create Master Bag:-Delhi returns hub-HUB  To Bangalore returns hub-HUB - 276777

        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi transport hub to Bangalore transport hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.DELIVERED_TO_SELLER, com.myntra.lms.client.status.OrderStatus.DELIVERED_TO_SELLER);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("trackingNumbers:%s , returnTrackingNumbers:%s returnIds:%s",trackingNumbers, returnTrackingNumbers, returnIds));
    }


    @Test(description = "ID:[C19413, C19414, C19423, C19424]")
    public void testSortationTriedAndNotBoughtOptimized() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap;
        Map<String, Object> returnData;
        Map<String, String>  containerDetails;
        List<String> trackingNumbers , packetIds,  orderIds, returnTrackingNumbers, returnIds;
        Long masterBagId = null, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        SortConfig returnSortConfig = new SortConfig(SortConfig.SortConfigType.RETURN);
        Map<String, SortationData> returnSortConfigData = returnSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setSortConfigReturn(returnSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setRtDestinationHubCode("RT-BLR");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(true);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        sortationProcessHelper.processFromIStoSH(currentHub, trackingNumbers, orderData.getShippingMethod(), orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, orderData.getTenantId(), LMS_CONSTANTS.CLIENTID);

        //Last Mile Operations
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        Long deliveryCenterId = lmsOperations.getDeliveryCenterID(orderData.getZipcode());
        System.out.println(lmsOperations.getDeliveryCenterID(orderData.getZipcode()));
        long tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(orderData.getZipcode())).get("tripId"));
        lastmileOperations.assignMultipleOrdersToTripByTrackingNumbers(trackingNumbers, tripId);
        lastmileOperations.startTrip(tripId, trackingNumbers);
        returnData = lastmileOperations.updateTryAndBuyTrip(String.valueOf(tripId),
                orderData.getTrackingNumbers(),
                orderData.getPacketIds(),
                orderData.getOrderIds(),
                TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT, deliveryCenterId);
        returnTrackingNumbers = (List<String>) returnData.get("returnTrackingNumbers");
        returnIds = (List<String>) returnData.get("returnIds");

        //reconcile
        lastmileOperations.receiveTryAndBuyTripOrders(returnTrackingNumbers);

        lastmileOperations.completeTrip(trackingNumbers);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        System.out.println(String.format("trackingNumbers:%s , returnTrackingNumbers:%s returnIds:%s",trackingNumbers, returnTrackingNumbers, returnIds));

        //Proceed further with returnTrackingNumbers to return back to WH
        //get next location
        //Create MB from DELHIS (6309) to RT-DEL (Delhi Returns Hub 21)
        //Add shipment PUT: /masterbag-service/masterbag/status
        //{"tenantId":"4019","id":"276759","forceUpdate":"false","event":"SHIPMENTS_ADDED","masterbagShipments":[{"trackingNumber":"ML0001554000","shipmentType":"PU","orderId":null,"sourceReturnId":"4001557785"}]}
        //MBID: 276759 , containerId: 215071, Recieve MB
        //Create Container [Delhi Sortation DC to Delhi Transport Hub , lane: Delhi Intracity Lane , Transporter: Rivgo]
        //Recieve Container at ['Delhi transport hub']
        // Recieve MasterBag
        // Create Container(215080) [Delhi transport hub To Delhi returns hub] lane: Delhi intercity lane , Transporter: Rivigo
        // add MB to container and ship -> Recieve container at[Delhi returns hub]
        // MasterBag Inscan V2 [Delhi returns hub-HUB] -> Scan Tracking number & validate the next location
        // Update scan masterBAg
        // Create Master Bag:-Delhi returns hub-HUB  To Bangalore returns hub-HUB - 276777
        // Received masterBag:-Delhi transport hub
        // Container -Delhi transport hub to Bangalore transport hub , lane: DEL-BLR , transporter: Rivigo
        // ContainerId: 215086
        // Recieve Container: Bangalore transport hub and recieve MB 276777
        // masterBagInscan V2:- Bangalore returns hub-HUB
        // sacn TN: ML0001554000 - Verify Next sort Loaction  -- return_shipment DELIVERED_TO_SELLER , pickup_shipment , order_tracking,
        //shipment_activity_detail


        //Return Path: DELHIS -> LABEL(RT-NORTH-DEL) -> RT-DEL -> LABEL(RT-SOUTH-BLR) -> RT-BLR

        //save current node (origin at current hop)
        currentHub = orderData.getDestinationHubCode();
        orderData.setCurrentHub(currentHub);
        orderData.setRtDestinationHubCode(sortationHelper.getDestinationSortLocation(returnTrackingNumbers.get(0), orderData.getCurrentHub()));

        sortationHelper.computeAndPrintCompletePath(orderData.getDestinationHubCode(), orderData.getRtDestinationHubCode() , orderData.getClientId(), orderData.getCourierCode());
        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,  LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.RETURN_SUCCESSFUL, com.myntra.lms.client.status.OrderStatus.RT);

        //final String presentHub, List<String> trackingNumbers, ShippingMethod shippingMethod, String courierCode, String tenantId
        processFromSHtoDTS(orderData.getCurrentHub() ,returnTrackingNumbers, orderData.getShippingMethod(), orderData.getCourierCode(), orderData.getTenantId());

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi Sortation DC -> Delhi Transport Hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //TMS Operations (Delhi transport hub -> Delhi returns hub (RT-DEL))
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Create Master Bag:-Delhi returns hub-HUB  To Bangalore returns hub-HUB - 276777

        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi transport hub to Bangalore transport hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.DELIVERED_TO_SELLER, com.myntra.lms.client.status.OrderStatus.DELIVERED_TO_SELLER);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("trackingNumbers:%s , returnTrackingNumbers:%s returnIds:%s",trackingNumbers, returnTrackingNumbers, returnIds));
    }

    public void processFromSHtoDTS(final String presentHub, List<String> returnTrackingNumbers, ShippingMethod shippingMethod, String courierCode, String tenantId) throws Exception {
        String nextHub = null;
        String currentHub = presentHub;
        Long masterBagId, containerId = null;
        Integer hopCount = 0;
        Map<String, String>  containerDetails;
        while (nextHub != null || sortationHelper.isNextSortLocationFound(returnTrackingNumbers.get(0), currentHub, SortLocationType.EXTERNAL)) {
            if (nextHub == null) {
                SortationConfigResponse sortationConfigResponse = sortationHelper.getNextSortLocation(returnTrackingNumbers.get(0), currentHub, SortLocationType.EXTERNAL);
                nextHub = sortationConfigResponse.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
            }

            //MasterBag V2
            masterBagId = lmsOperations.masterBagV2OperationsReverse(returnTrackingNumbers, currentHub, nextHub, shippingMethod, courierCode, tenantId);
            sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, shippingMethod, tenantId);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

            //TMS Operations
            containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,currentHub, nextHub, tenantId);
            sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, courierCode, OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

            //save current node (origin at current hop)
            currentHub = nextHub;
            nextHub = containerDetails.get("destinationHubcode");

            hopCount++;
        }
    }

}
