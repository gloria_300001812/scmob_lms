package com.myntra.apiTests.erpservices.sortation.test;

import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathResponse;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationService;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.sortation.domain.ShipmentInfo;
import com.myntra.sortation.domain.SortLocationType;
import com.myntra.sortation.response.ShipmentInfoResponse;
import com.myntra.sortation.response.SortationConfigResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class SortationAPI {
    SortationService sortationService = new SortationService();

    /**
     * Config: source: WH(36) to PIN(560068)
     * Path: DH-BLR -> ELC
     * Assuming this default path should work, even without uploading any sortation configuration.
     */
    @Test(description = "Fetch Entire Path for Shipping Label")
    public void testComputePathForShipmentWitoutConfig(){
        String origin = "DH-BLR" , destination = "ELC";
        SortationPathResponse sortationPathResponse = sortationService.getComputedPath( origin, destination, ShippingMethod.NORMAL, "ML", "2297" , "4019", "4019");
        com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry sortationPathEntry = sortationPathResponse.getData().get(0);
        Assert.assertEquals(sortationPathEntry.getOrigin() , origin , "Invalid Origin");
        Assert.assertEquals(sortationPathEntry.getDestination() , destination , "Invalid Destination");
        Assert.assertEquals(sortationPathEntry.getTenantId() , LMSConstants.DEFAULT_TENANT_ID , "Invalid tenantId");
        Assert.assertEquals(sortationPathEntry.getClientData().get("shippingMethod"), ShippingMethod.NORMAL.toString(), "Invalid Shipping method");
        Iterator<com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail> pathItr = sortationPathEntry.getPath().iterator();
        while(pathItr.hasNext()) {
            com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail hopDetail = pathItr.next();
            Assert.assertEquals(hopDetail.getOrigin(), origin);
            Assert.assertEquals(hopDetail.getNextLocation(), destination);
            Assert.assertEquals(hopDetail.getLocationType(), SortLocationType.EXTERNAL.name());
            Assert.assertEquals(hopDetail.getHopNumber().toString(), "1");
        }
        System.out.println("sortationPathEntry="+sortationPathResponse);
    }


    /**
     * Config: source: WH(36) to PIN(560068)
     * Path: DH-BLR -> ELC
     * Assuming this default path should work, even without uploading any sortation configuration.
     */
    @Test(description = "Fetch Entire Path for Shipping Label")
    public void testComputePathForShipmentWithConfig(){
        String origin = "DH-BLR" , destination = "ELC";
        SortationPathResponse sortationPathResponse = sortationService.getComputedPath( origin, destination, ShippingMethod.NORMAL, "ML", "2297" , "4019", "4019");
        com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry sortationPathEntry = sortationPathResponse.getData().get(0);
        Assert.assertEquals(sortationPathEntry.getOrigin() , origin , "Invalid Origin");
        Assert.assertEquals(sortationPathEntry.getDestination() , destination , "Invalid Destination");
        Assert.assertEquals(sortationPathEntry.getTenantId() , LMSConstants.DEFAULT_TENANT_ID , "Invalid tenantId");
        Assert.assertEquals(sortationPathEntry.getClientData().get("shippingMethod"), ShippingMethod.NORMAL.toString(), "Invalid Shipping method");
        Iterator<com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail> pathItr = sortationPathEntry.getPath().iterator();
        while(pathItr.hasNext()) {
            com.myntra.apiTests.erpservices.sortation.service.Pojos.SortationPathEntry.HopDetail hopDetail = pathItr.next();
            Assert.assertEquals(hopDetail.getOrigin(), origin);
            Assert.assertEquals(hopDetail.getNextLocation(), destination);
            Assert.assertEquals(hopDetail.getLocationType(), SortLocationType.EXTERNAL.name());
            Assert.assertEquals(hopDetail.getHopNumber().toString(), "1");
        }
        System.out.println("sortationPathEntry="+sortationPathResponse);
    }

    @Test
    public void printCompletePath(){
        new SortationHelper().computeAndPrintCompletePath("DELHIS", "RT-BLR", "2297", "ML");
        new SortationHelper().getCompleteCompletedPath("DH-BLR", "DELHIS");
    }

    /**
     * test next sort location and validate with compute path
     * remove parameters like &sortLevel=1 and locationtype and test the output
     */
    @Test(description = "Fetch Next Sort Location")
    public void testFetchNextSortLoction(){
        String trackingNumber = "ML0001468788",  currentLocation = "DH-BLR",  clientId = "4019", tenantId = "4019";
        SortLocationType sortLocationType = SortLocationType.EXTERNAL;
        SortationConfigResponse sortationConfigResponse = sortationService.getNextSortLocation(trackingNumber, currentLocation, sortLocationType  , clientId, tenantId);
        System.out.println("SortationConfigResponse="+sortationConfigResponse);
    }

    @Test(description = "Fetch Next Sort Location")
    public void testFetchNextSortLoctionBySortLevel(){
        String trackingNumber = "ML0001468788", currentLocation ="DH-BLR"  , clientId = "4019", tenantId ="4019";
        Integer sortLevel =1;
        SortationConfigResponse sortationConfigResponse = sortationService.getNextSortLocation(trackingNumber, sortLevel, currentLocation  , clientId, tenantId);
        System.out.println("SortationConfigResponse="+sortationConfigResponse);
    }

    @Test(description = "Manifest Order In Soration Platform ")
    public void testManifestOrderInSorationPlatform(){
        ShipmentInfo shipmentInfo = new ShipmentInfo();
        shipmentInfo.setTenantId("4019");
        shipmentInfo.setClientId("4019");
        shipmentInfo.setTrackingNumber("ML0001430439");
        shipmentInfo.setDestinationLocation("ELC");

        Map<String, String> clientDetails = new HashMap<>();
        clientDetails.put("shippingMethod", ShippingMethod.NORMAL.name());
        clientDetails.put("courierCode", "ML");
        clientDetails.put("sourceClientId", "2297");
        shipmentInfo.setClientDetails(clientDetails);
        ShipmentInfoResponse shipmentInfoResponse = sortationService.manifestShipmentInSortationPlatform(shipmentInfo);
        System.out.println("ShipmentInfoResponse="+shipmentInfoResponse);
    }

    @Test(description = "Get Next sort location")
    public void testNextSortatonLocation(){
        String origin = "DH-BLR" , destination = "DELHIS";
        SortationHelper sortationHelper = new SortationHelper();
        Map<String, String> map = sortationHelper.getCompleteCompletedPath(origin, destination);
        String trackingNumber = "ML0001636774";
        String currentHub = origin;
        while(sortationHelper.isNextSortLocationFound(trackingNumber, currentHub, SortLocationType.EXTERNAL)){
            SortationConfigResponse nextSortationLocation = sortationHelper.getNextSortLocation(trackingNumber, currentHub, SortLocationType.EXTERNAL);
            String nextHub = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
            System.out.println("currentHub="+currentHub+" nextHub ="+nextHub);
            currentHub = nextHub;
        }
    }

    @Test(description = "Sortation Configuration upload and Verification of uploaded configuration")
    public void testSortatonConfigurationUpload(){

    }

    /**
     * Valid Colors for sort location: BLUE, RED, GREEN, PURPLE, BLACK, ORANGE, YELLOW, WHITE, PINK, BROWN
     * Valid Shapes: SQUARE, RECTANGLE, CIRCLE
     * Expected: Config with other Color/Shape listed above should fail.
     */
    @Test
    public void testColorAndShapeConfiguration(){

    }
}
