package com.myntra.apiTests.erpservices.sortation.test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_ReturnHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.mvel2.util.Make;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class SortationRHD {
    String env = getEnvironment();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TMSOperations tmsOperations = new TMSOperations();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();


    @BeforeClass(alwaysRun = true)
    public void initilize(){
        String query = "update tracking_number set status = 0 \n" +
                "where courier_code = \"DE-COD\" and tenant_id = \"4019\" \n" +
                "ORDER BY `last_modified_on` ASC limit 10;";
        DBUtilities.exUpdateQuery(query, "myntra_lms");
    }

    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DH-BLR -> LABEL(NORTH-RHD) -> DH-DEL -> LABEL(DEL-R]HD) -> RHD
     * Detail: [DH-BLR ---> (hop=1,type=EXTERNAL) ---> Label(NORTH-RHD) ---> DH-DEL ---> (hop=2,type=EXTERNAL) ---> Label(DEL-RHD) ---> RHD]
     * Note: All packets go to same destination.
     */
    @Test(description = "ID:[C19442, C19549, C19550, C19552]", retryAnalyzer = Retry.class)
    public void testRHDFlow() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId;
        Map<String, Object> orderMap;
        Map<String, String>  containerDetails;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig rhdSortConfig = new SortConfig(SortConfig.SortConfigType.RHD);
        Map<String, SortationData> rhdSortConfigData = rhdSortConfig.getSortConfig(null,null);
        //orderData.setSortConfigForward(rhdSortConfigData);
        orderData.setSortConfigRHD(rhdSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("RHD");
        orderData.setCourierCode("DE");
        orderData.setRHDDestinationHubCode("DE");
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("400053");  //110001    122002
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        lmsReturnHelper.insertTrackingNumberClosedBox("DE");
        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //in case of RHD, we need to update status as accepted manually
        lmsHelper.updateOrderTrackingToAccepted(packetIds);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.RHD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RHD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        containerDetails = tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2RHD(masterBagId, orderData);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED_AT_HANDOVER_CENTER);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //ship master bag (Delhivery (DE)-DC)
        ShipmentResponse shipmentMasterBag = lmsServiceHelper.shipMasterBag(masterBagId);
        Assert.assertEquals(shipmentMasterBag.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString(), "Ship Masterbag to 3PL failed") ;
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.HANDED_OVER_TO_3PL);

        //Deliver the shipments
        for(String trackingNumber : trackingNumbers) {
            lmsServiceHelper.updateShipmentfor3PL(trackingNumber, ShipmentUpdateEvent.OUT_FOR_DELIVERY, "DE", ShipmentType.DL);
            lmsServiceHelper.updateShipmentfor3PL(trackingNumber, ShipmentUpdateEvent.DELIVERED, "DE", ShipmentType.DL);
        }
        System.out.println("trackingNumbers:"+trackingNumbers);
    }
}
