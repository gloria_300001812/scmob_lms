package com.myntra.apiTests.erpservices.sortation.test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class SortationFlowPKToDL {

    String env = getEnvironment();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TMSOperations tmsOperations = new TMSOperations();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();

    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DH-BLR -> LABEL(NORTH) -> DH-DEL -> LABEL(DDC) -> DELHIS
     * Note: All packets go to same destination.
     */
    @Test(description = "ID:[C19423, C19424]")
    public void testSortationForwardFlowMultiHop() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());

        //orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderMap = getOrdersInPKStatus();

        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        //sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode());

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        //sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode());
        //masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Last Mile Operations
        lastmileOperations.deliverOrders(orderData.getZipcode(), trackingNumbers);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println("trackingNumbers:"+trackingNumbers);
    }

    public Map<String, Object> getOrdersInPKStatus(){
        String packetIDArray;
        //packetIDArray = System.getProperty("packetIDs");
        packetIDArray = "3864127471761347,3864127471761350,3864127471761351,3864127471761353";
        String[] paketIds;
        String packetId, orderId, trackingNumber;
        Map<String, Object> orderMap = new HashMap<>();
        List<String> trackingNumbers = new ArrayList<>();
        List<String> packetIds = new ArrayList<>();
        List<String> orderIds = new ArrayList<>();

        if(packetIDArray.contains(",")){
            paketIds = packetIDArray.split(",");
            for(String storeOrderID : paketIds) {
                packetId = storeOrderID.trim();
                trackingNumber = lmsHelper.getTrackingNumber(packetId);
                trackingNumbers.add(trackingNumber);
                packetIds.add(packetId);
            }
        }else{
            packetId = packetIDArray;
            trackingNumber = lmsHelper.getTrackingNumber(packetId);
            trackingNumbers.add(trackingNumber);
            packetIds.add(packetId);
        }
        orderMap.put("packetIds", packetIds);
        orderMap.put("trackingNumbers", trackingNumbers);
        return orderMap;
    }


    @Test
    public void test1() throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, "560068", "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("trackingNumber[%s], orderID[%s], packetId[%s] ",trackingNumber, orderID, packetId));
    }

    @Test
    public void setOrderStatus() throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        ReleaseStatus releaseStatus = ReleaseStatus.DL;
        String packetId = "3864127471785759";
        String courierCode = "ML";
        String query = "SELECT order_release_id_fk " +
                "FROM order_line " +
                "WHERE packet_id_fk = '%s';";
        query = String.format(query, packetId);
        Map<String, Object> resultSet = DBUtilities.exSelectQueryForSingleRecord(query,"myntra_oms");
        String releaseId = resultSet.get("order_release_id_fk").toString();
        lmsHelper.processOrderInSCM(releaseStatus.toString(), courierCode, releaseId, packetId);
    }



    @Test(description = "ID:[C19423, C19424]")
    public void testSortationExchangeOrdersFlowMultiHop() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap, exchangeOrderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        List<String> exchangeTrackingNumbers , returnTrackingNumbers,  exchangePacketIds,  exchangeOrderIds;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 7;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Last Mile Operations
        lastmileOperations.deliverOrders(orderData.getZipcode(), trackingNumbers);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("trackingNumbers:[%s], packetIds:[%s], orderIds:[%s]",trackingNumbers, packetIds, orderIds));


        exchangeOrderMap = lmsOperations.createMockExchangeOrders(orderData , EnumSCM.PK);
        exchangeOrderIds = (List) exchangeOrderMap.get("exchangeOrderIds");
        exchangeTrackingNumbers = (List) exchangeOrderMap.get("exTrackingNumbers");
        exchangePacketIds = (List) exchangeOrderMap.get("exPacketIds");

        System.out.println(String.format("exchangeTrackingNumbers:[%s], exchangePacketIds:[%s], exchangeOrderIds:[%s]",exchangeTrackingNumbers, exchangePacketIds, exchangeOrderIds));
    }

}
