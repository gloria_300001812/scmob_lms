package com.myntra.apiTests.erpservices.sortation.test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.ShipmentEntry;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.response.TransporterResponse;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.TryAndBuyItemStatus;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.sortation.domain.SortLocationType;
import com.myntra.sortation.response.SortationConfigResponse;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneEntry;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Apr-2019
 * @usage All Shipment and TMS Operations
 */

public class SortationFlows {
    String env = getEnvironment();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TMSOperations tmsOperations = new TMSOperations();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();


    /**
     * Config: source: WH(36) to PIN(560068)
     * Path: DH-BLR -> ELC
     *
     * 1. Order Inscan V2:
     *    Validate : shipment_tracking , order_to_ship.shipment_status table , ml_shipment.shipment_status
     * 2. Create MasterBag and add shipment to masterbag & Close MasterBag.
     *    Validate: order_to_ship , shipment_order_map.status.tracking numbers
     *    Validate: shipment_order_map.status.tracking numbers
     * 3. TMS Operations
     *    Validate:
     * 4. MasterBag In-scan V2
     *    Validate:
     * 5. Last Mile Operations
     *    Validate:
     */
    @Test(description = "ID:[Regular flow should not Break]")
    public void testRegularFlow() throws Exception {
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        String inScanResponse;
        Long masterBagId;
        String clientId = LMS_CONSTANTS.CLIENTID;
        TMSMasterbagReponse tmsMasterbagReponse;
        String originHubCode = "DH-BLR" , destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        String zipcode = "560068", wareHouseId = "36";
        long laneId = 13 , transporterId = 1; //LMS_PINCODE.ML_BLR

        sortationHelper.computeAndPrintCompletePath(originHubCode, destinationHubCode, clientId, courierCode);

        orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));

        //Order Inscan V2
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, wareHouseId,false, LMS_CONSTANTS.TENANTID);
        System.out.println("response="+inScanResponse);
        System.out.println("inScanResponse=");

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId="+masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL,  LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse= (HubToTransportHubConfigResponse)tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode= hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode="+transportHubCode);
        tmsMasterbagReponse= (TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse="+tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId="+containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse)tmsServiceHelper.containerInTransitScan.apply(containerId,destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //MasterBag In-scan V2
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, tenantId);
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        while(shipmentEntryItr.hasNext()){
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), shipmentEntry.getOrderShipmentAssociationEntries().get(0).getTrackingNumber(),
                    shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId() ,
                    ShipmentType.DL , PremisesType.DC);
        }

        //Last Mile Operations
        long tripId;
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        System.out.println(lmsOperations.getDeliveryCenterID(zipcode));
        tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(zipcode)).get("tripId"));
        lastmileOperations.assignOrderToTripByTrackingNumber(trackingNumber, tripId);
        lastmileOperations.startTrip(tripId, trackingNumber);
        lastmileOperations.updateDeliveryTripByTrackingNumber(trackingNumber);
        lastmileOperations.completeTrip(trackingNumber);
        System.out.println("");
    }

    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DH-BLR -> DH-DEL -> DELHIS
     */
    @Test(description = "ID:[C19383, C19385, C19389, C19390, C19393]")
    public void testSortationFlowMultiHop() throws Exception {
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId;
        TMSMasterbagReponse tmsMasterbagReponse;
        String originHubCode = "DH-BLR";
        String destinationHubCode = "DELHIS";
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String tenantId = LMS_CONSTANTS.TENANTID;
        String clientId = LMS_CONSTANTS.CLIENTID;
        String currentHub = originHubCode;
        String nextHub = null;
        String zipcode = "100002", wareHouseId = "36";
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 2;
        SortConfig sortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        sortConfig.getSortConfig(null,null);

        sortationHelper.computeAndPrintCompletePath(originHubCode, destinationHubCode, clientId, courierCode);

        orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, wareHouseId, ShippingMethod.NORMAL.name(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));

        //Order Inscan V2
        String orderInScanResponse= lmsOperations.orderInscanV2(trackingNumber, wareHouseId, false);

        //Sortation
        SortationConfigResponse nextSortationLocation = sortationHelper.getNextSortLocation(trackingNumber, currentHub,1);
        String sortType = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getType();
        if(SortLocationType.EXTERNAL.name().equalsIgnoreCase(sortType)) {
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getOriginLocation(), originHubCode, "Invalid originHubCode");
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getDestinationLocation(), destinationHubCode, "Invalid destinationHubCode");
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("courierCode"), courierCode);
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("shippingMethod"), ShippingMethod.NORMAL.name());
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("sourceClientId"), "2297");
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getName(), "NORTH");
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceHub(), originHubCode, "Invalid originHubCode");
            nextHub = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
        }

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, nextHub, ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId="+masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL,  LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse originHubToTransportHubConfig= (HubToTransportHubConfigResponse)tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String originTransportHubCode= originHubToTransportHubConfig.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("originTransportHubCode="+originTransportHubCode);

        HubToTransportHubConfigResponse nextHubToTransportHubConfig= (HubToTransportHubConfigResponse)tmsServiceHelper.getLocationHubConfigFW.apply(nextHub);
        String nextTransportHubCode= nextHubToTransportHubConfig.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("nextTransportHubCode="+nextTransportHubCode);

        //find laneID for given path
        List<LaneEntry> laneEntries = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(originTransportHubCode, nextTransportHubCode)).getLaneEntries();
        for(LaneEntry laneEntry : laneEntries){
            if(LaneType.INTERCITY.name().equalsIgnoreCase(laneEntry.getType().name()) &&
                    laneEntry.getActive() &&
                    laneEntry.getSourceHubCode().equalsIgnoreCase(originTransportHubCode) &&
                    laneEntry.getDestinationHubCode().equalsIgnoreCase(nextTransportHubCode)){
                laneId = laneEntry.getId();
                break;
            }
        }

        //Find Transporter Id
        transporterId = ((TransporterResponse)tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        tmsMasterbagReponse= (TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(originTransportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse="+tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(originTransportHubCode, nextTransportHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId="+containerId);

        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse)tmsServiceHelper.containerInTransitScan.apply(containerId,nextTransportHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBagNew.apply(nextTransportHubCode, masterBagId, containerId);

        //MasterBag In-scan V2
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, tenantId);
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        while(shipmentEntryItr.hasNext()){
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), shipmentEntry.getOrderShipmentAssociationEntries().get(0).getTrackingNumber(),
                    shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId() ,
                    ShipmentType.DL , PremisesType.DC);
        }

        //----------------------------------------------

        //save current node (origin at current hop)
        currentHub = nextHub;
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));

        //Validate Next Sort Location
        nextSortationLocation = sortationHelper.getNextSortLocation(trackingNumber, currentHub,1);
        sortType = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getType();
        if(SortLocationType.EXTERNAL.name().equalsIgnoreCase(sortType)) {
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getOriginLocation(), currentHub, "Invalid originHubCode");
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getDestinationLocation(), destinationHubCode, "Invalid destinationHubCode");
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("courierCode"), courierCode);
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("shippingMethod"), ShippingMethod.NORMAL.name());
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getClientVariables().get("sourceClientId"), "2297");
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getName(), "DDC");
            Assert.assertEquals(nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceHub(), currentHub, "Invalid originHubCode");
            nextHub = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
        }

        //MasterBag V2
        System.out.println(String.format("Creating MasterBag from %s to %s",currentHub,nextHub));
        mbCreateResponse = masterBagServiceHelper.createMasterBag(currentHub, nextHub, ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId="+masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL,  LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        originHubToTransportHubConfig= (HubToTransportHubConfigResponse)tmsServiceHelper.getLocationHubConfigFW.apply(currentHub);
        originTransportHubCode= originHubToTransportHubConfig.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("originTransportHubCode="+originTransportHubCode);

        nextHubToTransportHubConfig= (HubToTransportHubConfigResponse)tmsServiceHelper.getLocationHubConfigFW.apply(nextHub);
        nextTransportHubCode= nextHubToTransportHubConfig.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("nextTransportHubCode="+nextTransportHubCode);

        //find laneID for given path
        laneEntries = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(originTransportHubCode, nextHub)).getLaneEntries();
        for(LaneEntry laneEntry : laneEntries){
            if(LaneType.INTRACITY.name().equalsIgnoreCase(laneEntry.getType().name()) &&
                    laneEntry.getActive() &&
                    laneEntry.getSourceHubCode().equalsIgnoreCase(originTransportHubCode) &&
                    laneEntry.getDestinationHubCode().equalsIgnoreCase(nextHub)){
                laneId = laneEntry.getId();
                break;
            }
        }

        //Find Transporter Id (DELHIS -TH is not configured properly)
        transporterId = ((TransporterResponse)tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        tmsMasterbagReponse= (TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(originTransportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse="+tmsMasterbagReponse.getStatus());
        containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(originTransportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println(String.format("Created Container from %s to %s , containerId=%s",originTransportHubCode, destinationHubCode,containerId));

        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse)tmsServiceHelper.containerInTransitScan.apply(containerId,destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        tmsMasterbagReponse1 = (TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //MasterBag In-scan V2
        mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, tenantId);
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        while(shipmentEntryItr.hasNext()){
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), shipmentEntry.getOrderShipmentAssociationEntries().get(0).getTrackingNumber(),
                    shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId() ,
                    ShipmentType.DL , PremisesType.DC);
        }

        //Last Mile Operations
        long tripId;
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        System.out.println(lmsOperations.getDeliveryCenterID(zipcode));
        tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(zipcode)).get("tripId"));
        lastmileOperations.assignOrderToTripByTrackingNumber(trackingNumber, tripId);
        lastmileOperations.startTrip(tripId, trackingNumber);
        lastmileOperations.updateDeliveryTripByTrackingNumber(trackingNumber);
        lastmileOperations.completeTrip(trackingNumber);
        System.out.println("");
    }


    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DH-BLR -> LABEL(NORTH) -> DH-DEL -> LABEL(DDC) -> DELHIS
     * Note: All packets go to same destination.
     */
    @Test(description = "ID:[C19423, C19424]")
    public void testSortationForwardFlowMultiHop() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Last Mile Operations
        lastmileOperations.deliverOrders(orderData.getZipcode(), trackingNumbers);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("trackingNumbers:[%s], packetIds:[%s], orderIds:[%s]",trackingNumbers, packetIds, orderIds));
    }

    //TOdO :  validate MasterbagStatus.RECEIVED_AT_TRANSPORTER_HUB


    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DH-BLR -> LABEL(NORTH) -> DH-DEL -> LABEL(DDC) -> DELHIS
     * Note: All packets go to same destination.
     * Minimum shipments: (n%2)=0 [NOT_TRIED, TRIED_AND_BOUGHT]
     */
    @Test(description = "ID:[C19383]")
    public void testSortationForwardFlowTriedAndBoughtMultiHop() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(true);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 2;

        Assert.assertTrue(numberOfShipments%2==0,"Invalid number of shipments, #shipments should be in %2");
        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Last Mile Operations
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        int trackingNumbersSize = orderData.getTrackingNumbers().size();
        Long deliveryCenterId = lmsOperations.getDeliveryCenterID(orderData.getZipcode());
        System.out.println(lmsOperations.getDeliveryCenterID(orderData.getZipcode()));
        long tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(orderData.getZipcode())).get("tripId"));
        lastmileOperations.assignMultipleOrdersToTripByTrackingNumbers(trackingNumbers, tripId);
        lastmileOperations.startTrip(tripId, trackingNumbers);
        lastmileOperations.updateTryAndBuyTrip(String.valueOf(tripId),
                new ArrayList(orderData.getTrackingNumbers().subList(0,(trackingNumbersSize/2))),
                new ArrayList(orderData.getPacketIds().subList(0,(trackingNumbersSize/2))),
                new ArrayList(orderData.getOrderIds().subList(0,(trackingNumbersSize/2))),
                TryAndBuyItemStatus.TRIED_AND_BOUGHT, deliveryCenterId);
        lastmileOperations.updateTryAndBuyTrip(String.valueOf(tripId),
                new ArrayList(orderData.getTrackingNumbers().subList((trackingNumbersSize/2),(trackingNumbersSize))),
                new ArrayList(orderData.getPacketIds().subList((trackingNumbersSize/2),(trackingNumbersSize))),
                new ArrayList(orderData.getOrderIds().subList((trackingNumbersSize/2),(trackingNumbersSize))),
                TryAndBuyItemStatus.NOT_TRIED, deliveryCenterId);
        lastmileOperations.completeTrip(trackingNumbers);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println("trackingNumbers:"+trackingNumbers);
    }


    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DH-BLR -> LABEL(NORTH) -> DH-DEL -> LABEL(DDC) -> DELHIS
     * Return Path: DELHIS -> LABEL(RT-NORTH-DEL) -> RT-DEL -> LABEL(RT-SOUTH-BLR) -> RT-BLR
     * Note: All packets go to same destination.
     */
    @Test(description = "ID:[C19413, C19414, C19423, C19424]")
    public void testSortationTriedAndNotBought() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap;
        Map<String, Object> returnData;
        Map<String, String>  containerDetails;
        List<String> trackingNumbers , packetIds,  orderIds, returnTrackingNumbers, returnIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        SortConfig returnSortConfig = new SortConfig(SortConfig.SortConfigType.RETURN);
        Map<String, SortationData> returnSortConfigData = returnSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setSortConfigReturn(returnSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setRtoHubCode("RT-BLR");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(true);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Last Mile Operations
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        Long deliveryCenterId = lmsOperations.getDeliveryCenterID(orderData.getZipcode());
        System.out.println(lmsOperations.getDeliveryCenterID(orderData.getZipcode()));
        long tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(orderData.getZipcode())).get("tripId"));
        lastmileOperations.assignMultipleOrdersToTripByTrackingNumbers(trackingNumbers, tripId);
        lastmileOperations.startTrip(tripId, trackingNumbers);
        returnData = lastmileOperations.updateTryAndBuyTrip(String.valueOf(tripId),
                orderData.getTrackingNumbers(),
                orderData.getPacketIds(),
                orderData.getOrderIds(),
                TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT, deliveryCenterId);
        returnTrackingNumbers = (List<String>) returnData.get("returnTrackingNumbers");
        returnIds = (List<String>) returnData.get("returnIds");

        //reconcile
        lastmileOperations.receiveTryAndBuyTripOrders(returnTrackingNumbers);

        lastmileOperations.completeTrip(trackingNumbers);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("trackingNumbers:%s , returnTrackingNumbers:%s returnIds:%s",trackingNumbers, returnTrackingNumbers, returnIds));

        //Proceed further with returnTrackingNumbers to return back to WH
        //get next location
        //Create MB from DELHIS (6309) to RT-DEL (Delhi Returns Hub 21)
        //Add shipment PUT: /masterbag-service/masterbag/status
        //{"tenantId":"4019","id":"276759","forceUpdate":"false","event":"SHIPMENTS_ADDED","masterbagShipments":[{"trackingNumber":"ML0001554000","shipmentType":"PU","orderId":null,"sourceReturnId":"4001557785"}]}
        //MBID: 276759 , containerId: 215071, Recieve MB
        //Create Container [Delhi Sortation DC to Delhi Transport Hub , lane: Delhi Intracity Lane , Transporter: Rivgo]
        //Recieve Container at ['Delhi transport hub']
        // Recieve MasterBag
        // Create Container(215080) [Delhi transport hub To Delhi returns hub] lane: Delhi intercity lane , Transporter: Rivigo
        // add MB to container and ship -> Recieve container at[Delhi returns hub]
        // MasterBag Inscan V2 [Delhi returns hub-HUB] -> Scan Tracking number & validate the next location
        // Update scan masterBAg
        // Create Master Bag:-Delhi returns hub-HUB  To Bangalore returns hub-HUB - 276777
        // Received masterBag:-Delhi transport hub
        // Container -Delhi transport hub to Bangalore transport hub , lane: DEL-BLR , transporter: Rivigo
        // ContainerId: 215086
        // Recieve Container: Bangalore transport hub and recieve MB 276777
        // masterBagInscan V2:- Bangalore returns hub-HUB
        // sacn TN: ML0001554000 - Verify Next sort Loaction  -- return_shipment DELIVERED_TO_SELLER , pickup_shipment , order_tracking,
        //shipment_activity_detail


        //Return Path: DELHIS -> LABEL(RT-NORTH-DEL) -> RT-DEL -> LABEL(RT-SOUTH-BLR) -> RT-BLR

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);
        orderData.setRtDestinationHubCode(sortationHelper.getDestinationSortLocation(returnTrackingNumbers.get(0), orderData.getCurrentHub()));

        sortationHelper.computeAndPrintCompletePath(orderData.getDestinationHubCode(), orderData.getRtoHubCode(), orderData.getClientId(), orderData.getCourierCode());
        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.RETURN_SUCCESSFUL, com.myntra.lms.client.status.OrderStatus.RT);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi Sortation DC -> Delhi Transport Hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //TMS Operations (Delhi transport hub -> Delhi returns hub (RT-DEL))
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Create Master Bag:-Delhi returns hub-HUB  To Bangalore returns hub-HUB - 276777

        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi transport hub to Bangalore transport hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.DELIVERED_TO_SELLER, com.myntra.lms.client.status.OrderStatus.DELIVERED_TO_SELLER);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("trackingNumbers:%s , returnTrackingNumbers:%s returnIds:%s",trackingNumbers, returnTrackingNumbers, returnIds));
    }

    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DH-BLR -> LABEL(NORTH) -> DH-DEL -> LABEL(DDC) -> DELHIS
     * Note: All packets go to same destination.
     */
    @Test(description = "ID:[C19417 , C19414, C19423, C19424]", enabled = false)
    public void testSortationExchange() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap, exchangeOrderMap;
        List<String> trackingNumbers , packetIds,  orderIds, returnIds;
        Map<String, Object> returnData;
        Map<String, String>  containerDetails;
        List<String> exchangeTrackingNumbers , returnTrackingNumbers,  exchangePacketIds,  exchangeOrderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        SortConfig returnSortConfig = new SortConfig(SortConfig.SortConfigType.RETURN);
        Map<String, SortationData> returnSortConfigData = returnSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setSortConfigReturn(returnSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setPaymentMode("on");
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.DL, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");

        exchangeOrderMap = lmsOperations.createMockExchangeOrders(orderData , EnumSCM.PK);
        exchangeOrderIds = (List) exchangeOrderMap.get("exchangeOrderIds");
        exchangeTrackingNumbers = (List) exchangeOrderMap.get("exTrackingNumbers");
        exchangePacketIds = (List) exchangeOrderMap.get("exPacketIds");
        orderData.setExchangeOrderMap(exchangeOrderMap);
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2ForExchangeOrders(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, exchangeTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, exchangeTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, exchangeTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, exchangeTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Last Mile Operations
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        Long deliveryCenterId = lmsOperations.getDeliveryCenterID(orderData.getZipcode());
        System.out.println(lmsOperations.getDeliveryCenterID(orderData.getZipcode()));
        long tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(orderData.getZipcode())).get("tripId"));
        lastmileOperations.assignMultipleOrdersToTripByTrackingNumbers(exchangeTrackingNumbers, tripId);
        lastmileOperations.startTrip(tripId, exchangeTrackingNumbers);

        List<OrderEntry> orderEntries;
        //Trip Update
        orderEntries = lastmileOperations.setOperationalTrackingIds(exchangeTrackingNumbers);
        returnData = lastmileOperations.updateExchangeTrip(tripId, exchangePacketIds, orderEntries);
        returnTrackingNumbers = (List<String>) returnData.get("returnTrackingNumbers");
        returnIds = (List<String>) returnData.get("returnIds");
        //Recieve Shipments
        lastmileOperations.recieveExchangeTripOrders(exchangeTrackingNumbers);
        //Trip Complete
        lastmileOperations.completeExchangeTrip(tripId, exchangePacketIds, exchangeOrderIds);

        sortationValidator.validateOrderTrackingDetail(exchangeTrackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("exchangeTrackingNumbers:%s , returnTrackingNumbers:%s returnIds:%s",exchangeTrackingNumbers, returnTrackingNumbers, returnIds));

        //Proceed further with returnTrackingNumbers to return back to WH

        //Return Path: DELHIS -> LABEL(RT-NORTH-DEL) -> RT-DEL -> LABEL(RT-SOUTH-BLR) -> RT-BLR

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);
        orderData.setRtOriginHubCode(currentHub);
        orderData.setRtDestinationHubCode(sortationHelper.getDestinationSortLocation(returnTrackingNumbers.get(0), orderData.getCurrentHub()));

        sortationHelper.computeAndPrintCompletePath(orderData.getRtOriginHubCode(), orderData.getRtDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.RETURN_SUCCESSFUL, com.myntra.lms.client.status.OrderStatus.RT);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi Sortation DC -> Delhi Transport Hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //TMS Operations (Delhi transport hub -> Delhi returns hub (RT-DEL))
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Create Master Bag:-Delhi returns hub-HUB  To Bangalore returns hub-HUB - 276777

        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi transport hub to Bangalore transport hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.DELIVERED_TO_SELLER, com.myntra.lms.client.status.OrderStatus.DELIVERED_TO_SELLER);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("exchangeTrackingNumbers:%s , returnTrackingNumbers:%s returnIds:%s",exchangeTrackingNumbers, returnTrackingNumbers, returnIds));
    }

    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DELHIS -> LABEL(RT-NORTH-DEL) -> RT-DEL -> LABEL(RT-SOUTH-BLR) -> RT-BLR
     * Note: All packets go to same destination.
     */
    @Test(description = "ID:[C19412, C19414, C19423, C19424]", enabled = false)
    public void testSortationReturnFlow() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        List<String> trackingNumbers, returnTrackingNumbers, packetIds, returnPacketIds, orderIds;
        List<String> returnIds = new ArrayList<>();
        Map<String, Object> orderMap;
        Map<String, Object> returnOrderMap;
        Map<String, String>  containerDetails;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        SortConfig returnSortConfig = new SortConfig(SortConfig.SortConfigType.RETURN);
        orderData.setSortConfigForward(forwardSortConfigData);
        Map<String, SortationData> returnSortConfigData = returnSortConfig.getSortConfig(null,null);
        orderData.setSortConfigReturn(returnSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setRtOriginHubCode("DELHIS");
        orderData.setRtDestinationHubCode("RT-BLR");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setReturnType(ReturnType.NORMAL);
        orderData.setCurrentHub(orderData.getRtOriginHubCode());
        String currentHub = orderData.getRtOriginHubCode();
        String nextHub = orderData.getRtDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        sortationHelper.computeAndPrintCompletePath(orderData.getRtOriginHubCode(), orderData.getRtDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(numberOfShipments, EnumSCM.DL, orderData.getWareHouseId(), orderData.getZipcode(), ShippingMethod.NORMAL.toString(), orderData.getCourierCode(),  false, "cod");
        returnOrderMap = lmsOperations.createMockReturnOrders((List) orderMap.get("orderIds"), orderData.getZipcode(), orderData.getReturnType());
        orderData.setReturnOrderMap(returnOrderMap);
        returnTrackingNumbers = (List<String>) returnOrderMap.get("returnTrackingNumbers");
        returnPacketIds = (List) returnOrderMap.get("returnPacketIds");
        returnIds = (List) returnOrderMap.get("returnIds");
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_CREATED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_CREATED, ShipmentUpdateEvent.PICKUP_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);


        //Create Return Trip -> assign Pickup Trip -> Start Trip -> Update Trip -> Recieve Order in DC
        //Create MB and so on

        long returnTripId;
        returnTripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(orderData.getZipcode())).get("tripId"));
        lastmileOperations.scanTrackingNumbersInTrip(returnTripId, returnTrackingNumbers);
        lastmileOperations.startTrip(returnTripId, returnTrackingNumbers);
        lastmileOperations.updatePickupTrip(returnTripId, returnTrackingNumbers);
        lastmileOperations.recieveTripOrders(returnTripId, returnTrackingNumbers);
        lastmileOperations.completeReturnTrip(returnTripId);

        //Get next Hub
        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        System.out.println(String.format("returnTrackingNumbers[%s] , returnIds[%s]",returnTrackingNumbers, returnIds));

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getRtOriginHubCode(),nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        //sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode());

        //TMS Operations (Delhi Sortation DC -> Delhi Transport Hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //TMS Operations (Delhi transport hub -> Delhi returns hub (RT-DEL))
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(containerDetails.get("destinationHubcode"));

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Create Master Bag:-Delhi returns hub-HUB  To Bangalore returns hub-HUB - 276777

        nextHub = lmsOperations.getNextSortationLocation(returnTrackingNumbers, orderData);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2OperationsReverse(orderData, returnTrackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, currentHub,nextHub, returnTrackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations (Delhi transport hub to Bangalore transport hub)
        containerDetails = tmsOperations.createAndShipReturnContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(returnTrackingNumbers, ShipmentStatus.PICKUP_SUCCESSFUL, ShipmentUpdateEvent.PICKUP_SUCCESSFUL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RETURN);
        lmsOperations.validateReturnStatus(returnIds, ShipmentStatus.DELIVERED_TO_SELLER, com.myntra.lms.client.status.OrderStatus.DELIVERED_TO_SELLER);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        System.out.println(String.format("returnTrackingNumbers[%s] , returnIds[%s]",returnTrackingNumbers, returnIds));
    }

    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DH-BLR -> LABEL(NORTH) -> DH-DEL -> LABEL(DDC) -> DELHIS -> FD -> RTO
     * Return Path: DELHIS -> LABEL(RT-NORTH-DEL) -> RT-DEL -> LABEL(RT-SOUTH-BLR) -> RT-BLR
     *
     * 1. SH -> FD -> Recieve In DC ->  Complete Trip ->Trip Result -> Create RTO -> Process RETURN
     *
     * Note: All packets go to same destination.
     */
    @Test(description = "ID:[C19420]", enabled = true)
    public void testSortationRTOFlow() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId , packetId, trackingNumber;
        Map<String, Object> orderMap, exchangeOrderMap;
        List<String> trackingNumbers , packetIds,  orderIds, returnIds;
        Map<String, Object> returnData;
        Map<String, String>  containerDetails;
        List<String> exchangeTrackingNumbers , returnTrackingNumbers,  exchangePacketIds,  exchangeOrderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        SortConfig returnSortConfig = new SortConfig(SortConfig.SortConfigType.RETURN);
        Map<String, SortationData> returnSortConfigData = returnSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setSortConfigReturn(returnSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setPaymentMode("on");
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 2;
        long tripId;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTRACITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(orderData.getZipcode())).get("tripId"));
        lastmileOperations.scanTrackingNumbersInTrip(tripId, trackingNumbers);
        lastmileOperations.startTrip(tripId, trackingNumbers);
        System.out.println("hi");

//        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
//                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
//        Thread.sleep(2000);
//        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");
//        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);
//
//        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
//                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
//
//        lastmileOperations.updateFailedTrip(tripId, trackingNumbers);
//
//        lastmileOperations.recieveFailedDeliveryOrders(tripId, trackingNumbers);
//        lastmileOperations.completeFailedTrip(trackingNumbers);
    }

    /**
     * Include all shipment types with sortation: DL, T&B, T&NB, EXchange, Return
     */
    @Test(description = "ID:[]", enabled = false)
    public void testSortationFlowWithDiffShipmentTypes() throws Exception {

    }

    /**
     * Config: source: WH(36) to PIN(100002)
     * Path: DH-BLR -> I-LABEL(NORTH) -> E-LABEL(DELHI) -> DH-DEL -> I-LABEL(NORTH) -> E-LABEL(DDC) -> DELHIS
     * Note: All packets go to same destination.
     * Test SDD: ShipmentType
     */
    @Test(description = "ID:[C19392]", enabled = false)
    public void testSortationWithSortationLocationType() throws Exception {

    }

    /**
     * 2 shipments , 2 MB's
     */
    @Test(description = "ID:[Need to add in test trail]", enabled = false)
    public void testMultipleMBsInSortation() throws Exception {

    }
}
