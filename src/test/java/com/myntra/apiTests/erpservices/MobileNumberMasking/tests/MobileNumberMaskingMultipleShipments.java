package com.myntra.apiTests.erpservices.MobileNumberMasking.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.mnm.helpers.MobileNumberMaskingHelper;
import com.myntra.apiTests.erpservices.lastmile.mnm.helpers.MobileNumberMaskingValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.lastmile.client.code.utils.VirtualNumberAllocationStatus;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.VirtualNumberResponse;
import com.myntra.lms.client.response.ItemEntry;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.logistics.platform.domain.TryAndBuyItemStatus;
import com.myntra.test.commons.testbase.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Mar-2019
 */
public class MobileNumberMaskingMultipleShipments  extends BaseTest {
    MobileNumberMaskingValidator mobileNumberMaskingValidator = new MobileNumberMaskingValidator();
    MobileNumberMaskingHelper mobileNumberMaskingHelper = new MobileNumberMaskingHelper();
    String env = getEnvironment();
    public String DcId = MobileNumberMaskingHelper.PincodeDCMap.get(LMS_PINCODE.ML_BLR);
    final Integer DEFAULT_NUMBER_OF_SHIPMENTS = 2;


    @BeforeMethod
    public void nameBefore(Method method)
    {
        System.out.println("\n\n**************************************************************************************************************\n");
        System.out.println("Executing Test name: " + method.getName());
        System.out.println("\n\n**************************************************************************************************************\n\n");
    }

    /**
     * Flows: (With And without VN Generation) - multiple shipments
     *  1. Delivery
     *  2. Return/Pickup (OPEN BOX)
     *  3. Try And Buy
     *  4. Exchange
     *  5. Schendule Pickup (We don't automate scheduled pickups - Vinodhini/Shuvendu Comfirmed)
     *  6. Store Flows (PENDING because the automation branch has not been merged)
     *
     *  Basic Scenarios in multiple shipments:
     *    a. Virtual Number is  Generated  -> All Shipments.
     *    b. Virtual Number Not Generated -> ALL Shipments.
     *    c. Virtual Number is generated for partial Shipments. Some Virtual Number Generated for SH And Some aren't.
     *    (IGNORE) d. When mobile number is same for 2 Shipments and VN is generated for 1 and not for other one.
     */

    /**
     * TestCases: All Forward orders (DELIVERED)
     */

    @Test(description = "Covers Delivery order case - Where Virtual number is generated for ALL shipments and trip started and completed successfully")
    public void testDeliveryWithAllShipmentsVN() {
        Map<String, Object> orderMap = null;
        String tripId, tripNumber;
        List<String> trackingNumbers;
        List<String> packetIds;
        Map<String, String> tripDetails;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        VirtualNumberResponse virtualNumberResponse;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.SH, false);
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumbers);
            //check generated Virtual number using API and verify with DB
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumbers);
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumbers, tripOrderAssignmentResponses);
            mobileNumberMaskingHelper.updateDeliveryTrip(tripId, packetIds);
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    /**
     * Scenario: In case of virtual number not generated before trip start. We start the trip after 2 mins (We check if VN is generated meanwhile).
     */
    @Test(description = "Covers Delivery order case - Where Virtual number is not generated for NONE and trip started after delay(2 Minutes)")
    public void testDeliveryWithoutVNs() {
        Map<String, Object> orderMap;
        String tripId , tripNumber;
        List<String> trackingNumbers;
        List<String> packetIds;
        Map<String, String> tripDetails;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.SH, false);
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));
            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumbers, tripOrderAssignmentResponses);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(trackingNumbers, tripId);
            mobileNumberMaskingHelper.updateDeliveryTrip(tripId, packetIds);
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers, VirtualNumberAllocationStatus.NOT_RECEIVED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    @Test(description = "Covers Delivery order case - Where Virtual number is generated for ONLY/FEW Shipments and trip started after delay(2 Minutes)")
    public void testDeliveryPartialShipmentsWithVN() {
        Map<String, Object> orderMap;
        List<String> trackingNumbers;
        List<String> packetIds;
        Map<String, String> tripDetails;
        String tripId, tripNumber;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        Integer numberOfShipments = 2;  // Let's limit it to 2, to keep it simple & make debugging easier.
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.SH, false);
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumbers.get(0));
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumbers.get(0));
            //Start Trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.AWAITING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(0), EnumSCM.PROCESSING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(1), EnumSCM.PROCESSING);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);
            mobileNumberMaskingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
            mobileNumberMaskingHelper.updateDeliveryTrip(tripId, packetIds);
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    /**
     * TestCases: All Exchange orders
     */

    @Test(description = "Covers Exchange order case - Where VN is generated for ALL Shipments and trip started and completed successfully")
    public void testExchangeWithAllShipmentsVN() {
        TripResponse tripResponse;
        List<OrderEntry> orderEntries;
        Map<String, String> tripDetails;
        Map<String, Object> orderMap = null;
        Map<String, Object> exchangeOrderMap;
        List<String> trackingNumbers = null;
        List<String> packetIds = null;
        List<String> exchangeOrderIds = null;
        String tripId, tripNumber;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.DL, false);
            exchangeOrderMap = mobileNumberMaskingHelper.createMockExchangeOrderMultipleShipments((List) orderMap.get("orderIds"));
            exchangeOrderIds = (List) exchangeOrderMap.get("exchangeOrderIds");
            trackingNumbers = (List) exchangeOrderMap.get("trackingNumbers");
            packetIds = (List) exchangeOrderMap.get("exPacketIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));

            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumbers);
            //check generated Virtual number using API and verify with DB
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumbers);

            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumbers, tripOrderAssignmentResponses);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);

            mobileNumberMaskingValidator.validateExchangesByTrip(tripNumber, tripId, trackingNumbers, packetIds);
            //Trip Update
            orderEntries = mobileNumberMaskingHelper.setOperationalTrackingIds(trackingNumbers);
            mobileNumberMaskingHelper.updateExchangeTrip(tripId, packetIds, orderEntries);
            //Recieve Shipments
            mobileNumberMaskingHelper.recieveExchangeTripOrders(trackingNumbers);
            //Trip Complete
            mobileNumberMaskingHelper.completeExchangeTrip(tripId, packetIds, exchangeOrderIds);
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers Exchange order case - Where VN is generated for NONE Shipments and trip started (After DELAY) and completed successfully")
    public void testExchangeWithoutVNs() {
        TripResponse tripResponse;
        List<OrderEntry> orderEntries;
        Map<String, String> tripDetails;
        Map<String, Object> orderMap = null;
        Map<String, Object> exchangeOrderMap;
        List<String> trackingNumbers = null;
        List<String> packetIds = null;
        List<String> exchangeOrderIds = null;
        String tripId, tripNumber;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.DL, false);
            exchangeOrderMap = mobileNumberMaskingHelper.createMockExchangeOrderMultipleShipments((List) orderMap.get("orderIds"));
            exchangeOrderIds = (List) exchangeOrderMap.get("exchangeOrderIds");
            trackingNumbers = (List) exchangeOrderMap.get("trackingNumbers");
            packetIds = (List) exchangeOrderMap.get("exPacketIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));
            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);

            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumbers, tripOrderAssignmentResponses);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(trackingNumbers, tripId);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);
            //validate exchanges by trip
            mobileNumberMaskingValidator.validateExchangesByTrip(tripNumber, tripId, trackingNumbers, packetIds);
            //Trip Update
            orderEntries = mobileNumberMaskingHelper.setOperationalTrackingIds(trackingNumbers);
            mobileNumberMaskingHelper.updateExchangeTrip(tripId, packetIds, orderEntries);
            //Recieve Shipments
            mobileNumberMaskingHelper.recieveExchangeTripOrders(trackingNumbers);
            //Trip Complete
            mobileNumberMaskingHelper.completeExchangeTrip(tripId, packetIds, exchangeOrderIds);
            //Validate VN's not recieved
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers, VirtualNumberAllocationStatus.NOT_RECEIVED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers Exchange order case - Where VN is generated for ONLY/FEW Shipments and trip started (After DELAY) and completed successfully")
    public void testExchangePartialShipmentsWithVN() {
        TripResponse tripResponse;
        List<OrderEntry> orderEntries;
        Map<String, String> tripDetails;
        Map<String, Object> orderMap = null;
        Map<String, Object> exchangeOrderMap;
        List<String> trackingNumbers = null;
        List<String> packetIds = null;
        List<String> exchangeOrderIds = null;
        String tripId, tripNumber;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        Integer numberOfShipments = 2;  // Let's limit it to 2, to keep it simple & make debugging easier.
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(numberOfShipments, EnumSCM.DL, false);
            exchangeOrderMap = mobileNumberMaskingHelper.createMockExchangeOrderMultipleShipments((List) orderMap.get("orderIds"));
            exchangeOrderIds = (List) exchangeOrderMap.get("exchangeOrderIds");
            trackingNumbers = (List) exchangeOrderMap.get("trackingNumbers");
            packetIds = (List) exchangeOrderMap.get("exPacketIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumbers.get(0));
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumbers.get(0));

            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.AWAITING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(0), EnumSCM.PROCESSING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(1), EnumSCM.PROCESSING);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);
            mobileNumberMaskingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);
            //validate exchanges by trip
            mobileNumberMaskingValidator.validateExchangesByTrip(tripNumber, tripId, trackingNumbers, packetIds);
            //Trip Update
            orderEntries = mobileNumberMaskingHelper.setOperationalTrackingIds(trackingNumbers);
            mobileNumberMaskingHelper.updateExchangeTrip(tripId, packetIds, orderEntries);
            //Recieve Shipments
            mobileNumberMaskingHelper.recieveExchangeTripOrders(trackingNumbers);
            //Trip Complete
            mobileNumberMaskingHelper.completeExchangeTrip(tripId, packetIds, exchangeOrderIds);
            //Validate VN's not recieved
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    /**
     * TestCases: All Return/Pickup orders (RETURNED/PICKUP)
     */

    @Test(description = "Covers Return order case - Where VN is generated for ALL Shipments and trip started and completed successfully")
    public void testReturnWithAllShipmentsVN() {
        TripResponse tripResponse;
        Map<String, Object> orderMap = null;
        Map<String, String> tripDetails;
        Map<String, Object> returnOrderMap;
        List<String> returnTrackingNumbers;
        List<String> returnPacketIds;
        OrderEntry orderEntry = new OrderEntry();
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        String tripId, tripNumber;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.DL, false);
            returnOrderMap = mobileNumberMaskingHelper.createMockReturnOrderMultipleShipments((List) orderMap.get("orderIds"));
            returnTrackingNumbers = (List) returnOrderMap.get("returnTrackingNumbers");
            returnPacketIds = (List) returnOrderMap.get("returnPacketIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");

            // scan tracking number in trip
            mobileNumberMaskingHelper.scanTrackingNumbersInTrip(tripId, returnTrackingNumbers);

            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(returnTrackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(returnTrackingNumbers);
            //check generated Virtual number using API and verify with DB
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(returnTrackingNumbers);


            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, returnTrackingNumbers); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumbers, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(returnTrackingNumbers, tripOrderAssignmentResponses);

            //Update Trip
            mobileNumberMaskingHelper.updatePickupTrip(tripId, returnTrackingNumbers);
            //Receive Trip Orders
            mobileNumberMaskingHelper.recieveTripOrders(tripId, returnTrackingNumbers);
            //complete trip
            mobileNumberMaskingHelper.completeReturnTrip(tripId);
            //mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers Return order case - Where VN is generated for NONE Shipments and trip started (After DELAY) and completed successfully")
    public void testReturnWithoutVNs() {
        TripResponse tripResponse;
        Map<String, Object> orderMap = null;
        Map<String, String> tripDetails;
        Map<String, Object> returnOrderMap;
        List<String> returnTrackingNumbers;
        List<String> returnPacketIds;
        OrderEntry orderEntry = new OrderEntry();
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        String tripId, tripNumber;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.DL, false);
            returnOrderMap = mobileNumberMaskingHelper.createMockReturnOrderMultipleShipments((List) orderMap.get("orderIds"));
            returnTrackingNumbers = (List) returnOrderMap.get("returnTrackingNumbers");
            returnPacketIds = (List) returnOrderMap.get("returnPacketIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");

            // scan tracking number in trip
            mobileNumberMaskingHelper.scanTrackingNumbersInTrip(tripId, returnTrackingNumbers);

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(returnTrackingNumbers);

            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, returnTrackingNumbers);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(returnTrackingNumbers, tripOrderAssignmentResponses);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(returnTrackingNumbers, tripId);

            //Update Trip
            mobileNumberMaskingHelper.updatePickupTrip(tripId, returnTrackingNumbers);
            //Receive Trip Orders
            mobileNumberMaskingHelper.recieveTripOrders(tripId, returnTrackingNumbers);
            //complete trip
            mobileNumberMaskingHelper.completeReturnTrip(tripId);
            //Validate VN's not recieved
            mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumbers, VirtualNumberAllocationStatus.NOT_RECEIVED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers Return order case - - Where VN is generated for ONLY/FEW Shipments and trip started (After DELAY) and completed successfully")
    public void testReturnPartialShipmentsWithVN() {
        TripResponse tripResponse;
        Map<String, Object> orderMap = null;
        Map<String, String> tripDetails;
        Map<String, Object> returnOrderMap;
        List<String> returnTrackingNumbers;
        List<String> returnPacketIds;
        OrderEntry orderEntry = new OrderEntry();
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        String tripId, tripNumber;
        Integer numberOfShipments = 2;  // Let's limit it to 2, to keep it simple & make debugging easier.
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(numberOfShipments, EnumSCM.DL, false);
            returnOrderMap = mobileNumberMaskingHelper.createMockReturnOrderMultipleShipments((List) orderMap.get("orderIds"));
            returnTrackingNumbers = (List) returnOrderMap.get("returnTrackingNumbers");
            returnPacketIds = (List) returnOrderMap.get("returnPacketIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");

            // scan tracking number in trip
            mobileNumberMaskingHelper.scanTrackingNumbersInTrip(tripId, returnTrackingNumbers);

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(returnTrackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(returnTrackingNumbers.get(0));
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(returnTrackingNumbers.get(0));

            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, returnTrackingNumbers);
            mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumbers.get(1), VirtualNumberAllocationStatus.AWAITING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(0), EnumSCM.PROCESSING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(1), EnumSCM.PROCESSING);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);
            mobileNumberMaskingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);

            //Update Trip
            mobileNumberMaskingHelper.updatePickupTrip(tripId, returnTrackingNumbers);
            //Receive Trip Orders
            mobileNumberMaskingHelper.recieveTripOrders(tripId, returnTrackingNumbers);
            //complete trip
            mobileNumberMaskingHelper.completeReturnTrip(tripId);
            //Validate VN's not recieved
            mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    /**
     * TestCases: All Try And Buy Orders
     */

    /**
     * 1. Try And Buy - Sub Category: Tried And Brought
     */
    @Test(description = "Covers TriedAndBrought order case - Where VN is generated for ALL Shipments and trip started and completed successfully")
    public void testTriedAndBroughtWithVN() {
        Map<String, Object> orderMap = null;
        List<String> trackingNumbers;
        List<String> packetIds;
        List<String> orderIds;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        OrderEntry orderEntry = new OrderEntry();
        ItemEntry itemEntry = new ItemEntry();
        Map<String, String> tripDetails;
        String tripId, tripNumber;
        try {

            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.SH, true);
            orderIds = (List<String>) orderMap.get("orderIds");
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));

            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumbers);
            //check generated Virtual number using API and verify with DB
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumbers);

            //Start Trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumbers, tripOrderAssignmentResponses);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);

            //Trip Update
            mobileNumberMaskingHelper.updateTryAndBuyTrip(tripId , trackingNumbers ,  packetIds, orderIds , TryAndBuyItemStatus.TRIED_AND_BOUGHT);

            //complete trip
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers TriedAndBrought order case - Where VN is generated for NONE Shipments and trip started (After DELAY) and completed successfully")
    public void testTriedAndBroughtWithoutVNs() {
        Map<String, Object> orderMap = null;
        List<String> trackingNumbers;
        List<String> packetIds;
        List<String> orderIds;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        OrderEntry orderEntry = new OrderEntry();
        ItemEntry itemEntry = new ItemEntry();
        Map<String, String> tripDetails;
        String tripId, tripNumber;
        try {

            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.SH, true);
            orderIds = (List<String>) orderMap.get("orderIds");
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);


            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumbers, tripOrderAssignmentResponses);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(trackingNumbers, tripId);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);

            //Trip Update
            mobileNumberMaskingHelper.updateTryAndBuyTrip(tripId , trackingNumbers ,  packetIds, orderIds , TryAndBuyItemStatus.TRIED_AND_BOUGHT);

            //complete trip
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers, VirtualNumberAllocationStatus.NOT_RECEIVED);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers TriedAndBrought order case - Where VN is generated for NONE Shipments and trip started (After DELAY) and completed successfully")
    public void testTriedAndBroughtPartialShipmentsWithVN() {
        Map<String, Object> orderMap = null;
        List<String> trackingNumbers;
        List<String> packetIds;
        List<String> orderIds;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        OrderEntry orderEntry = new OrderEntry();
        ItemEntry itemEntry = new ItemEntry();
        Map<String, String> tripDetails;
        String tripId, tripNumber;
        Integer numberOfShipments = 2;  // Let's limit it to 2, to keep it simple & make debugging easier.
        try {

            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(numberOfShipments, EnumSCM.SH, true);
            orderIds = (List<String>) orderMap.get("orderIds");
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumbers.get(0));
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumbers.get(0));

            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.AWAITING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(0), EnumSCM.PROCESSING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(1), EnumSCM.PROCESSING);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);
            mobileNumberMaskingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);

            //Trip Update
            mobileNumberMaskingHelper.updateTryAndBuyTrip(tripId , trackingNumbers ,  packetIds, orderIds , TryAndBuyItemStatus.TRIED_AND_BOUGHT);

            //complete trip
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            //Validate VN's not recieved
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    /**
     * 2. Try And Buy - Sub Category: Not Tried And Brought
     */

    @Test(description = "Covers NotTriedAndBrought order case - Where VN is generated for ALL Shipments and trip started and completed successfully")
    public void testNotTriedAndBroughtWithVN() {
        Map<String, Object> orderMap = null;
        List<String> trackingNumbers;
        List<String> packetIds;
        List<String> orderIds;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        Map<String, String> tripDetails;
        String tripId, tripNumber;
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();
        ItemEntry itemEntry = new ItemEntry();
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.SH, true);
            orderIds = (List<String>) orderMap.get("orderIds");
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));

            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumbers);
            //check generated Virtual number using API and verify with DB
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumbers);

            //Start Trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumbers, tripOrderAssignmentResponses);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);

            //Trip Update
            mobileNumberMaskingHelper.updateTryAndBuyTrip(tripId , trackingNumbers ,  packetIds, orderIds , TryAndBuyItemStatus.NOT_TRIED);

            //complete trip
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers NotTriedAndBrought order case - Where VN is generated for NONE Shipments and trip started (After DELAY) and completed successfully")
    public void testNotTriedAndBroughtWithoutVNs() {
        Map<String, Object> orderMap = null;
        List<String> trackingNumbers;
        List<String> packetIds;
        List<String> orderIds;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        Map<String, String> tripDetails;
        String tripId, tripNumber;
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();
        ItemEntry itemEntry = new ItemEntry();
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.SH, true);
            orderIds = (List<String>) orderMap.get("orderIds");
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);

            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumbers, tripOrderAssignmentResponses);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(trackingNumbers, tripId);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);

            //Trip Update
            mobileNumberMaskingHelper.updateTryAndBuyTrip(tripId , trackingNumbers ,  packetIds, orderIds , TryAndBuyItemStatus.NOT_TRIED);

            //complete trip
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers, VirtualNumberAllocationStatus.NOT_RECEIVED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers NotTriedAndBrought order case - Where VN is generated for NONE Shipments and trip started (After DELAY) and completed successfully")
    public void testNotTriedAndBroughtPartialShipmentsWithVN() {
        Map<String, Object> orderMap = null;
        List<String> trackingNumbers;
        List<String> packetIds;
        List<String> orderIds;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        Map<String, String> tripDetails;
        String tripId, tripNumber;
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();
        ItemEntry itemEntry = new ItemEntry();
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.SH, true);
            orderIds = (List<String>) orderMap.get("orderIds");
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumbers.get(0));
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumbers.get(0));

            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.AWAITING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(0), EnumSCM.PROCESSING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(1), EnumSCM.PROCESSING);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);
            mobileNumberMaskingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);

            //Trip Update
            mobileNumberMaskingHelper.updateTryAndBuyTrip(tripId , trackingNumbers ,  packetIds, orderIds , TryAndBuyItemStatus.NOT_TRIED);

            //complete trip
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            //Validate VN's not recieved
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    /**
     * 3. Try And Buy - Sub Category: Tried And Not Brought
     */

    @Test(description = "Covers TriedAndNotBrought order case - Where VN is generated for ALL Shipments and trip started and completed successfully")
    public void testTriedAndNotBroughtWithVN() {
        Map<String, Object> orderMap = null;
        List<String> trackingNumbers;
        List<String> packetIds;
        List<String> orderIds;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        Map<String, String> tripDetails;
        List<String> returnTrackingNumbers = new ArrayList<>();
        String tripId, tripNumber;
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();
        ItemEntry itemEntry = new ItemEntry();
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.SH, true);
            orderIds = (List<String>) orderMap.get("orderIds");
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));

            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumbers);
            //check generated Virtual number using API and verify with DB
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumbers);

            //Start Trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumbers, tripOrderAssignmentResponses);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);

            //reconcile fail
            //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

            //Trip Update
            returnTrackingNumbers = mobileNumberMaskingHelper.updateTryAndBuyTrip(tripId , trackingNumbers ,  packetIds, orderIds , TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);

            //reconcile
            mobileNumberMaskingHelper.receiveTryAndBuyTripOrders(returnTrackingNumbers);

            //complete trip
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers TriedAndNotBrought order case - Where VN is generated for NONE Shipments and trip started (After DELAY) and completed successfully")
    public void testTriedAndNotBroughtWithoutVNs() {
        Map<String, Object> orderMap = null;
        List<String> trackingNumbers;
        List<String> packetIds;
        List<String> orderIds;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        Map<String, String> tripDetails;
        List<String> returnTrackingNumbers = new ArrayList<>();
        String tripId, tripNumber;
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();
        ItemEntry itemEntry = new ItemEntry();
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.SH, true);
            orderIds = (List<String>) orderMap.get("orderIds");
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);

            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumbers, tripOrderAssignmentResponses);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(trackingNumbers, tripId);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);

            //Trip Update
            returnTrackingNumbers = mobileNumberMaskingHelper.updateTryAndBuyTrip(tripId , trackingNumbers ,  packetIds, orderIds , TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);

            //reconcile
            mobileNumberMaskingHelper.receiveTryAndBuyTripOrders(returnTrackingNumbers);

            //complete trip
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers, VirtualNumberAllocationStatus.NOT_RECEIVED);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers TriedAndNotBrought order case - Where VN is generated for NONE Shipments and trip started (After DELAY) and completed successfully")
    public void testTriedAndNotBroughtPartialShipmentsWithVN() {
        Map<String, Object> orderMap = null;
        List<String> trackingNumbers;
        List<String> packetIds;
        List<String> orderIds;
        List<TripOrderAssignmentResponse> tripOrderAssignmentResponses;
        Map<String, String> tripDetails;
        List<String> returnTrackingNumbers = new ArrayList<>();
        String tripId, tripNumber;
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();
        ItemEntry itemEntry = new ItemEntry();
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderMultipleShipments(DEFAULT_NUMBER_OF_SHIPMENTS, EnumSCM.SH, true);
            orderIds = (List<String>) orderMap.get("orderIds");
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");
            mobileNumberMaskingHelper.assignMultipleOrdersToTrip(packetIds, Long.valueOf(tripId));

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumbers);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumbers.get(0));
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumbers.get(0));

            //start trip
            tripOrderAssignmentResponses = mobileNumberMaskingHelper.startTrip(tripId, trackingNumbers);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.AWAITING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(0), EnumSCM.PROCESSING);
            mobileNumberMaskingValidator.validateTripStatus(tripOrderAssignmentResponses.get(1), EnumSCM.PROCESSING);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);
            mobileNumberMaskingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
            mobileNumberMaskingValidator.validateOrderStatusInLMSAndML(packetIds);

            //Trip Update
            returnTrackingNumbers = mobileNumberMaskingHelper.updateTryAndBuyTrip(tripId , trackingNumbers ,  packetIds, orderIds , TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);

            //reconcile
            mobileNumberMaskingHelper.receiveTryAndBuyTripOrders(returnTrackingNumbers);

            //complete trip
            mobileNumberMaskingHelper.completeTrip(tripId, packetIds);
            //Validate VN's not recieved
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(0), VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumbers.get(1), VirtualNumberAllocationStatus.NOT_RECEIVED);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }
}
