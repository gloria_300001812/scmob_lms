package com.myntra.apiTests.erpservices.MobileNumberMasking.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.mnm.helpers.MobileNumberMaskingHelper;
import com.myntra.apiTests.erpservices.lastmile.mnm.helpers.MobileNumberMaskingValidator;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClientV2_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripOrderAssignmentClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_ReturnHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.lms.tests.TripOrderAssignmentResponseValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.code.utils.VirtualNumberAllocationStatus;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.VirtualNumberResponse;
import com.myntra.lms.client.response.ItemEntry;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.logistics.platform.domain.TryAndBuyItemStatus;
import com.myntra.logistics.platform.domain.TryAndBuyNotBoughtReason;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Mar-2019
 */
public class MobileNumberMaskingFlows {
    MobileNumberMaskingValidator mobileNumberMaskingValidator = new MobileNumberMaskingValidator();
    MobileNumberMaskingHelper mobileNumberMaskingHelper = new MobileNumberMaskingHelper();
    public String DcId = MobileNumberMaskingHelper.PincodeDCMap.get(LMS_PINCODE.ML_BLR);
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    String env = getEnvironment();
    TripClient_QA tripClient_qa = new TripClient_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa = new TripOrderAssignmentClient_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa = new MLShipmentClientV2_QA();

    /**
     * Flows: (With And without VN Generation) - Single shipment
     *  1. Delivery
     *  2. Return/Pickup (OPEN BOX)
     *  3. Try And Buy
     *  4. Exchange
     *  5. Schendule Pickup (We don't automate scheduled pickups - Vinodhini/Shuvendu Comfirmed)
     *  6. Store Flows (PENDING because the automation branch has not been merged)
     */

    /**
     * TestCases: All Forward orders (DELIVERED)
     */

    @Test(description = "Covers Delivery order case - Where Virtual number is generated and trip started and completed successfully",
            retryAnalyzer = Retry.class)
    public void testDeliveryWithVN() {
        Map<String, String> orderMap;
        Map<String, String> postedVNentries;
        String trackingNumber, tripId, packetId;
        TripResponse tripResponse;
        VirtualNumberResponse virtualNumberResponse;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderAndAssignShipmentToTrip();
            trackingNumber = orderMap.get("trackingNumber");
            tripId = orderMap.get("tripId");
            packetId = orderMap.get("packetId");
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumber);
            //Generate Virtual Number Using API
            postedVNentries = mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumber);
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumber);
            tripResponse = mobileNumberMaskingHelper.startTrip(tripId, packetId); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumber, tripResponse);
            mobileNumberMaskingHelper.updateDeliveryTrip(tripId, packetId);
            mobileNumberMaskingHelper.completeTrip(tripId, packetId);
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    /**
     * Scenario: In case of virtual number not generated before trip start. We start the trip after 2 mins (We check if VN is generated meanwhile).
     */
    @Test(description = "Covers Delivery order case - Where Virtual number is not generated and trip started after delay(2 Minutes)",
            retryAnalyzer = Retry.class)
    public void testDeliveryWithoutVN() {
        Map<String, String> orderMap;
        String trackingNumber, tripId, packetId;
        TripResponse tripResponse;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderAndAssignShipmentToTrip();
            trackingNumber = orderMap.get("trackingNumber");
            tripId = orderMap.get("tripId");
            packetId = orderMap.get("packetId");
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumber);
            tripResponse = mobileNumberMaskingHelper.startTrip(tripId, packetId);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumber, tripResponse);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(trackingNumber, tripId);
            mobileNumberMaskingHelper.updateDeliveryTrip(tripId, packetId);
            mobileNumberMaskingHelper.completeTrip(tripId, packetId);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.NOT_RECEIVED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    /**
     * TestCases: All Exchange orders
     */

    @Test(description = "Covers Exchange order case - Where Virtual number is generated and trip started and completed successfully",
            retryAnalyzer = Retry.class)
    public void testExchangeWithVN() {
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();
        try {

            String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
            String packetId = omsServiceHelper.getPacketId(exchangeOrder);
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);

            //Create Trip
            String deliveryStaffID = mobileNumberMaskingHelper.getDeliveryStaffID(DcId);
            tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            long tripId = tripResponse.getTrips().get(0).getId();
            String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
            //Assign Trip
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumber);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumber);
            //check generated Virtual number using API and verify with DB
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumber);

            //start trip
            //Assert.assertEquals(tripClient.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
            tripResponse = mobileNumberMaskingHelper.startTrip(String.valueOf(tripId), packetId); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumber, tripResponse);

            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            //TODO this tripOrderAssignmentId is wrong as packetid remains same in case of return/exchange
            long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
            TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripNumber, LMS_CONSTANTS.TENANTID);
            TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
            tripOrderAssignmentResponseValidator.validateTripOrderStatus(EnumSCM.OFD);
            tripOrderAssignmentResponseValidator.validateTripId(tripId);
            tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNumber);
            tripOrderAssignmentResponseValidator.validateExchangeOrderId(packetId);
            Assert.assertEquals(tripOrderAssignmentResponse.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant Id in response");
            //Trip Update
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                    EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
            //Recieve Shipments
            TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
            //Trip Complete
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                    EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);
            System.out.println("trackingNumber="+trackingNumber);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers Exchangea order case - Where Virtual number is not generated and trip started after delay(2 Minutes)",
            retryAnalyzer = Retry.class)
    public void testExchangeWithoutVN() {
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();

        try {
            String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
            String packetId = omsServiceHelper.getPacketId(exchangeOrder);
            String deliveryStaffID = mobileNumberMaskingHelper.getDeliveryStaffID(DcId);
            tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            long tripId = tripResponse.getTrips().get(0).getId();
            String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumber);


            //start trip
            //Assert.assertEquals(tripClient.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
            tripResponse = mobileNumberMaskingHelper.startTrip(String.valueOf(tripId), packetId);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumber, tripResponse);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(trackingNumber, String.valueOf(tripId));

            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
            TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripNumber, LMS_CONSTANTS.TENANTID);
            TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
            tripOrderAssignmentResponseValidator.validateTripOrderStatus(EnumSCM.OFD);
            tripOrderAssignmentResponseValidator.validateTripId(tripId);
            tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNumber);
            tripOrderAssignmentResponseValidator.validateExchangeOrderId(packetId);
            Assert.assertEquals(tripOrderAssignmentResponse.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant Id in response");
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                    EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
            TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                    EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.NOT_RECEIVED);
            System.out.println("trackingNumber="+trackingNumber);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    /**
     * TestCases: All Return/Pickup orders (RETURNED/PICKUP)
     */

    @Test(description = "Covers Return order case - Where Virtual number is generated and trip started and completed successfully",
            retryAnalyzer = Retry.class)
    public void testReturnWithVN() {
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();
        Map<String, String> tripDetails;
        String tripId, tripNumber;
        try {
            String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
            OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
            ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
            Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
            Long returnID = returnResponse.getData().get(0).getId();
            String returnOrderID = String.valueOf(returnResponse.getData().get(0).getOrderId());
            String returnPacketId = omsServiceHelper.getPacketId(returnOrderID);
            String forwardDLTrackingNumber = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));

            ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
            lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
            lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));


            OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
            String returnTrackingNumber = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
            System.out.println("forwardDLTrackingNumber="+forwardDLTrackingNumber+" - returnTrackingNumber="+returnTrackingNumber);

            //Create Trip
            tripDetails = mobileNumberMaskingHelper.createTrip(DcId);
            tripId = tripDetails.get("tripId");
            tripNumber = tripDetails.get("tripNumber");

            // scan tracking number in trip
            TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(Long.valueOf(tripId), returnTrackingNumber);
            Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(returnTrackingNumber);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(returnTrackingNumber);
            //check generated Virtual number using API and verify with DB
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(returnTrackingNumber);


            // Start Trip
            //TripOrderAssignmentResponse startTripRes = tripClient.startTrip(tripId);
            mobileNumberMaskingHelper.startTrip(String.valueOf(tripId), returnPacketId); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumber, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNStatusWhenVNGenerated(returnTrackingNumber);
            //TODO Uncomment below line and check
            //mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(returnTrackingNumber, tripResponse);

            Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
            String tripOrderAssignmentId = toaId.get("id").toString();


            TripOrderAssignmentResponse updateTripResponse = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
            Assert.assertEquals(updateTripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
            Thread.sleep(2000);
            lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
            TripOrderAssignmentResponse receiveTripOrderResponse = tripOrderAssignmentClient_qa.receiveTripOrder(returnTrackingNumber.substring(2, returnTrackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(receiveTripOrderResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

            //reconcile not required for failed pickup
            Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(returnTrackingNumber), EnumSCM.PICKUP_SUCCESSFUL, "shipment status mismatch");
            Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

            //complete trip
            TripOrderAssignmentResponse updatePickupInTripResponse = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
            Assert.assertEquals(updatePickupInTripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
            //mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);
            System.out.println("trackingNumber="+returnTrackingNumber);
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    @Test(description = "Covers Return order case - Where Virtual number is not generated and trip started after delay(2 Minutes)",
            retryAnalyzer = Retry.class)
    public void testReturnWithoutVN() {
        TripResponse tripResponse;


        OrderEntry orderEntry = new OrderEntry();

        try {

            String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID(DcId);
            String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
            OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
            ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
            Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
            Long returnID = returnResponse.getData().get(0).getId();
            String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
            String packetId = omsServiceHelper.getPacketId(orderID1);
            String forwardDLTrackingNumber = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));

            ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
            lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
            lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));


            //Trip
            OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
            String returnTrackingNumber = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
            System.out.println("forwardDLTrackingNumber="+forwardDLTrackingNumber+" - returnTrackingNumber="+returnTrackingNumber);
            DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
            tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
            Long tripId = tripResponse.getTrips().get(0).getId();

            // scan tracking number in trip
            TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, returnTrackingNumber);
            Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(returnTrackingNumber);

            // Start Trip
            //TripOrderAssignmentResponse startTripRes = tripClient.startTrip(tripId);
            mobileNumberMaskingHelper.startTrip(String.valueOf(tripId), packetId);
            tripResponse = tripClient_qa.searchByTripId(Long.valueOf(tripId));
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(returnTrackingNumber, tripResponse);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(returnTrackingNumber, String.valueOf(tripId));

            Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
            String tripOrderAssignmentId = toaId.get("id").toString();


            TripOrderAssignmentResponse updateTripResponse = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
            Assert.assertEquals(updateTripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
            Thread.sleep(2000);
            lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
            TripOrderAssignmentResponse receiveTripOrderResponse = tripOrderAssignmentClient_qa.receiveTripOrder(returnTrackingNumber.substring(2, returnTrackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(receiveTripOrderResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

            //reconcile not required for failed pickup
            Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(returnTrackingNumber), EnumSCM.PICKUP_SUCCESSFUL, "shipment status mismatch");
            Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

            //complete trip
            TripOrderAssignmentResponse updatePickupInTripResponse = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
            Assert.assertEquals(updatePickupInTripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
            mobileNumberMaskingValidator.validateVNAllocationStatus(returnTrackingNumber, VirtualNumberAllocationStatus.NOT_RECEIVED);
            System.out.println("trackingNumber="+returnTrackingNumber);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    /**
     * TestCases: All Try And Buy Orders
     */

    @Test(description = "Covers TriedAndBrought order case - Where Virtual number is generated and trip started and completed successfully",
            retryAnalyzer = Retry.class)
    public void testTriedAndBroughtWithVN() {
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();
        ItemEntry itemEntry = new ItemEntry();
        try {
            String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
            String packetId = omsServiceHelper.getPacketId(orderID);
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);
            String deliveryStaffID = mobileNumberMaskingHelper.getDeliveryStaffID(DcId);
            tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
            long tripId = tripResponse.getTrips().get(0).getId();
            //assign trip
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumber);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumber);
            //check generated Virtual number using API and verify with DB
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumber);

            //Start Trip
            //Assert.assertEquals(tripClient.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
            tripResponse = mobileNumberMaskingHelper.startTrip(String.valueOf(tripId), packetId); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumber, tripResponse);
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

            //Update Trip
            com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
            Double amount = orderEntryDetails.getFinalAmount();

            long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
            OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            orderEntry.setOrderId(packetId);
            orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
            orderEntry.setCodAmount(amount);
            itemEntry.setId(Id);
            itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
            itemEntry.setRemarks("TRIED AND BOUGHT");
            List<ItemEntry> entries = new ArrayList<>();
            entries.add(itemEntry);
            orderEntry.setItemEntries(entries);
            TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            //complete trip
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                    EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    @Test(description = "Covers TriedAndBrought order case - Where Virtual number is not generated and trip started after delay(2 Minutes)",
            retryAnalyzer = Retry.class)
    public void testTriedAndBroughtWithoutVN() {
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();


        ItemEntry itemEntry = new ItemEntry();
        try {

            String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
            String packetId = omsServiceHelper.getPacketId(orderID);
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);
            String deliveryStaffID = mobileNumberMaskingHelper.getDeliveryStaffID(DcId);
            tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
            long tripId = tripResponse.getTrips().get(0).getId();
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumber);

            //Start Trip
            //Assert.assertEquals(tripClient.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
            tripResponse = mobileNumberMaskingHelper.startTrip(String.valueOf(tripId), packetId);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumber, tripResponse);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(trackingNumber, String.valueOf(tripId));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
            Double amount = orderEntryDetails.getFinalAmount();

            long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
            OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            orderEntry.setOrderId(packetId);
            orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
            orderEntry.setCodAmount(amount);
            itemEntry.setId(Id);
            itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
            itemEntry.setRemarks("TRIED AND BOUGHT");
            List<ItemEntry> entries = new ArrayList<>();
            entries.add(itemEntry);
            orderEntry.setItemEntries(entries);
            TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            //complete trip
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                    EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.NOT_RECEIVED);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers NotTriedAndBrought order case - Where Virtual number is generated and trip started and completed successfully",
            retryAnalyzer = Retry.class)
    public void testNotTriedAndBroughtWithVN() {
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();


        ItemEntry itemEntry = new ItemEntry();
        try {
            String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
            String packetId = omsServiceHelper.getPacketId(orderID);
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);
            String deliveryStaffID = mobileNumberMaskingHelper.getDeliveryStaffID(DcId);
            tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
            long tripId = tripResponse.getTrips().get(0).getId();
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumber);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumber);
            //check generated Virtual number using API and verify with DB
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumber);

            //Start Trip
            //Assert.assertEquals(tripClient.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
            tripResponse = mobileNumberMaskingHelper.startTrip(String.valueOf(tripId), packetId); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumber, tripResponse);
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
            Double amount = orderEntryDetails.getFinalAmount();


            long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
            OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            orderEntry.setOrderId(packetId);
            orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
            orderEntry.setCodAmount(amount);
            itemEntry.setId(Id);
            itemEntry.setStatus(TryAndBuyItemStatus.NOT_TRIED);
            itemEntry.setRemarks("NOT TRIED And Bought");
            List<ItemEntry> entries = new ArrayList<>();
            entries.add(itemEntry);
            orderEntry.setItemEntries(entries);
            TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            //complete trip
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                    EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    @Test(description = "Covers NotTriedAndBrought order case - Where Virtual number is not generated and trip started after delay(2 Minutes)",
            retryAnalyzer = Retry.class)
    public void testNotTriedAndBroughtWithoutVN() {
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();
        ItemEntry itemEntry = new ItemEntry();
        try {
            String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
            String packetId = omsServiceHelper.getPacketId(orderID);
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);
            String deliveryStaffID = mobileNumberMaskingHelper.getDeliveryStaffID(DcId);
            tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
            long tripId = tripResponse.getTrips().get(0).getId();
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumber);

            //Start Trip
            //Assert.assertEquals(tripClient.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
            tripResponse = mobileNumberMaskingHelper.startTrip(String.valueOf(tripId), packetId);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumber, tripResponse);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(trackingNumber, String.valueOf(tripId));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
            Double amount = orderEntryDetails.getFinalAmount();


            long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
            OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            orderEntry.setOrderId(packetId);
            orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
            orderEntry.setCodAmount(amount);
            itemEntry.setId(Id);
            itemEntry.setStatus(TryAndBuyItemStatus.NOT_TRIED);
            itemEntry.setRemarks("NOT TRIED And Bought");
            List<ItemEntry> entries = new ArrayList<>();
            entries.add(itemEntry);
            orderEntry.setItemEntries(entries);
            TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            //complete trip
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                    EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.NOT_RECEIVED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers TriedAndNotBrought order case - Where Virtual number is generated and trip started and completed successfully",
            retryAnalyzer = Retry.class)
    public void testTriedAndNotBroughtWithVN() {
        TripResponse tripResponse;
        OrderEntry orderEntry = new OrderEntry();


        ItemEntry itemEntry = new ItemEntry();
        try {
            String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
            String packetId = omsServiceHelper.getPacketId(orderID);
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);
            String deliveryStaffID = mobileNumberMaskingHelper.getDeliveryStaffID(DcId);
            tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
            long tripId = tripResponse.getTrips().get(0).getId();
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

            //Get VN
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumber);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumber);
            //check generated Virtual number using API and verify with DB
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumber);

            //Start Trip
            //Assert.assertEquals(tripClient.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
            tripResponse = mobileNumberMaskingHelper.startTrip(String.valueOf(tripId), packetId); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumber, tripResponse);
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

            //reconcile fail
            //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
            com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);

            long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
            Double amount = orderEntryDetails.getFinalAmount();
            OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            orderEntry.setOrderId(packetId);
            orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
            orderEntry.setCodAmount(amount);
            itemEntry.setId(Id);
            itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);
            itemEntry.setRemarks("tried and not bought");
            itemEntry.setQcStatus(ItemQCStatus.PASSED);
            itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
            List<ItemEntry> entries = new ArrayList<>();
            entries.add(itemEntry);
            orderEntry.setItemEntries(entries);
            // mark delivered and reconcile shouldn't work
            //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

            TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            String ml_shipment_id = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
            String return_tracking_num = lmsHelper.updateOperationalTrackingNumForTryAndBuy(ml_shipment_id);

            //reconcile
            //checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
            //validateDuringReconciliation(deliveryStaffID, DcId, return_tracking_num.substring(2,return_tracking_num.length()),1L,EnumSCM.SUCCESS);

            TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(return_tracking_num.substring(2, return_tracking_num.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

            //TODO Uncomment later, we need to do this validation, as of now it is failing, will debug this later.
            //Long tripOrderAssignmentIdForReturn = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(return_tracking_num);
            //Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForReturn)), "true", "Wrong status in trip_order_assignment");
            //Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(return_tracking_num), EnumSCM.PICKUP_SUCCESSFUL, "Status not updated to received in DC");

            //complete trip
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                    EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);

        } catch (Exception e) {
                        e.printStackTrace();
            System.out.println(e);
            System.out.println(e);
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "Covers TriedAndNotBrought order case - Where Virtual number is not generated and trip started after delay(2 Minutes)",
            retryAnalyzer = Retry.class)
    public void testTriedAndNotBroughtWithoutVN() {
        TripResponse tripResponse;
        String env = getEnvironment();
        OrderEntry orderEntry = new OrderEntry();

        ItemEntry itemEntry = new ItemEntry();
        try {
            String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
            String packetId = omsServiceHelper.getPacketId(orderID);
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);
            String deliveryStaffID = mobileNumberMaskingHelper.getDeliveryStaffID(DcId);
            tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
            long tripId = tripResponse.getTrips().get(0).getId();
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

            //Get VN API
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumber);

            //Start Trip
            //Assert.assertEquals(tripClient.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
            tripResponse = mobileNumberMaskingHelper.startTrip(String.valueOf(tripId), packetId);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumber, tripResponse);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(trackingNumber, String.valueOf(tripId));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

            //reconcile fail
            //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
            com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);

            long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
            Double amount = orderEntryDetails.getFinalAmount();
            OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            orderEntry.setOrderId(packetId);
            orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
            orderEntry.setCodAmount(amount);
            itemEntry.setId(Id);
            itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);
            itemEntry.setRemarks("tried and not bought");
            itemEntry.setQcStatus(ItemQCStatus.PASSED);
            itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
            List<ItemEntry> entries = new ArrayList<>();
            entries.add(itemEntry);
            orderEntry.setItemEntries(entries);
            // mark delivered and reconcile shouldn't work
            //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

            TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
            Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
            String ml_shipment_id = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
            String return_tracking_num = lmsHelper.updateOperationalTrackingNumForTryAndBuy(ml_shipment_id);

            //reconcile
            //checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
            //validateDuringReconciliation(deliveryStaffID, DcId, return_tracking_num.substring(2,return_tracking_num.length()),1L,EnumSCM.SUCCESS);
            TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(return_tracking_num.substring(2, return_tracking_num.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

            //TODO Uncomment later, we need to do this validation, as of now it is failing, will debug this later.
            //Long tripOrderAssignmentIdForReturn = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(return_tracking_num);
            //Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForReturn)), "true", "Wrong status in trip_order_assignment");
            //Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(return_tracking_num), EnumSCM.PICKUP_SUCCESSFUL, "Status not updated to received in DC");

            //complete trip
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                    EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.NOT_RECEIVED);
        } catch (Exception e) {
                        e.printStackTrace();
            System.out.println(e);
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }
}
