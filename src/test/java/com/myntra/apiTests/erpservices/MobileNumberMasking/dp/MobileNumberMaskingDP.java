package com.myntra.apiTests.erpservices.MobileNumberMasking.dp;

import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.mnm.helpers.MobileNumberMaskingHelper;
import com.myntra.apiTests.erpservices.lms.DB.LastMileDAO;
import com.myntra.lordoftherings.Toolbox;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

/**
 * @author Bharath.MC
 * @since Feb-2019
 */
public class MobileNumberMaskingDP {
    public static Boolean isMobileNumberUpdatedInDB = false;

    @DataProvider(name="Sort Order")
    public static Object[][] GenerateSortOrders(ITestContext testContext){
        Long start = System.currentTimeMillis();
        if(!isMobileNumberUpdatedInDB) {
            LastMileDAO.updatePhoneNumbersInMLShipmentDB(MobileNumberMaskingHelper.deliveryCenterId);
            isMobileNumberUpdatedInDB = true;
        }
        System.out.println("Time taken to update PhoneNumbersInMLShipment in DB "+(System.currentTimeMillis() - start)+" millisecods");
        Object[][] sortOrders =  new Object[][] { { LASTMILE_CONSTANTS.SORT_ORDER_ASC }, { LASTMILE_CONSTANTS.SORT_ORDER_DESC } };
        return Toolbox.returnReducedDataSet(sortOrders, testContext.getIncludedGroups(), 2, 2);
    }

}
