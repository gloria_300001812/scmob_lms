package com.myntra.apiTests.erpservices.MobileNumberMasking.tests;

import com.myntra.apiTests.erpservices.MobileNumberMasking.dp.MobileNumberMaskingDP;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.mnm.helpers.MobileNumberMaskingHelper;
import com.myntra.apiTests.erpservices.lastmile.mnm.helpers.MobileNumberMaskingValidator;
import com.myntra.apiTests.erpservices.lastmile.service.LastmileServiceHelper;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.DB.LastMileDAO;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.lastmile.client.code.utils.VirtualNumberAllocationStatus;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.VirtualNumberResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShipmentType;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;

/**
 * @author Bharath.MC
 * @since Feb-2019
 */

public class MobileNumberMaskingSanity {

    LastmileServiceHelper lastmileServiceHelper = new LastmileServiceHelper();
    TripClient_QA tripClient_qa = new TripClient_QA();
    MobileNumberMaskingValidator mobileNumberMaskingValidator = new MobileNumberMaskingValidator();
    MobileNumberMaskingHelper mobileNumberMaskingHelper = new MobileNumberMaskingHelper();
    public String DcId = MobileNumberMaskingHelper.PincodeDCMap.get(LMS_PINCODE.ML_BLR);

    @Test(description = "API: Get Virtual Number for MyMyntra ", retryAnalyzer = Retry.class)
    public void testGetVirualNumberByTrackingNumberAPI() {
        Map<String, String> orderMap;
        Map<String, String> postedVNentries;
        String trackingNumber , tripId, packetId;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderAndAssignShipmentToTrip();
            trackingNumber = orderMap.get("trackingNumber");
            tripId = orderMap.get("tripId");
            packetId = orderMap.get("packetId");
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.AWAITING);
            VirtualNumberResponse virtualNumberResponse = lastmileServiceHelper.getVirualNumberByTrackingNumber(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            mobileNumberMaskingValidator.validateVirtualNumberResponseBasic(virtualNumberResponse);
            mobileNumberMaskingValidator.validateGetVirtualNumberByTrackingNumberBefore(virtualNumberResponse, trackingNumber);
            //Generate Virtual Number Using API
            postedVNentries = mobileNumberMaskingHelper.generateVirtualNumberByTrackingId(trackingNumber);
            mobileNumberMaskingValidator.validateVirtualNumberEntriesDB(trackingNumber , postedVNentries);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
            //check generated Virtual number using API and verify with DB
            virtualNumberResponse = lastmileServiceHelper.getVirualNumberByTrackingNumber(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            System.out.println(virtualNumberResponse);
            mobileNumberMaskingValidator.validateVirtualNumberResponseBasic(virtualNumberResponse);
            mobileNumberMaskingValidator.validateGetVirtualNumberByTrackingNumberAfter(virtualNumberResponse, trackingNumber);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    /**
     * Virtual Number generation API for DC Manager
     *   Case1: MLXX1 -> VN Not allocated - customer Number should be the output 
     *   Case2: MLXX2 -> VN -> Assigned -> gen DC manager VN -> still customer Number will be the output. 
     *   If CTI response is in time, we update VN in DB and show VN, else we just show original number.
     */
    @Test(description = "API: Generate Virtual Number For DC Manager", retryAnalyzer = Retry.class)
    public void testGenerateVirtualNumberForDCManagerWithNoVN() {
        Map<String, String> orderMap;
        Map<String, String> postedVNentries;
        String trackingNumber, tripId, packetId;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderAndAssignShipmentToTrip();
            trackingNumber = orderMap.get("trackingNumber");
            tripId = orderMap.get("tripId");
            packetId = orderMap.get("packetId");
            LastMileDAO.updatePhoneNumberInMLShipmentDB(trackingNumber);
            VirtualNumberResponse virtualNumberResponse = lastmileServiceHelper.generateVirtualNumberDCManager(LASTMILE_CONSTANTS.TENANT_ID, DcId, trackingNumber);
            //Get VN API
            mobileNumberMaskingValidator.validateVNGeneratedByDCManagerWithNoVN(virtualNumberResponse, trackingNumber);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    //TODO This is pending as it is sync API. Which invokes exotel(3rd party) and retrives the VN and show in UI, as well as update it in DB.
    @Test(description = "API: Generate Virtual Number For DC Manager", retryAnalyzer = Retry.class)
    public void testGenerateVirtualNumberForDCManagerWithVN() {
        Map<String, String> orderMap;
        Map<String, String> postedVNentries;
        String trackingNumber, tripId, packetId;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderAndAssignShipmentToTrip();
            trackingNumber = orderMap.get("trackingNumber");
            tripId = orderMap.get("tripId");
            packetId = orderMap.get("packetId");
            LastMileDAO.updatePhoneNumberInMLShipmentDB(trackingNumber);
            mobileNumberMaskingValidator.validateGetVNafterTripAssignment(trackingNumber);
            //Generate Virtual Number Using API
            mobileNumberMaskingHelper.generateVirtualNumberAndValidate(trackingNumber);
            mobileNumberMaskingValidator.validateGeneratedVNbyGetVNAPI(trackingNumber);
            //Generate Virtual Number Using Dc Manager API
            VirtualNumberResponse virtualNumberResponse = lastmileServiceHelper.generateVirtualNumberDCManager(LASTMILE_CONSTANTS.TENANT_ID, DcId, trackingNumber);
            //Get VN API
            mobileNumberMaskingValidator.validateVNGeneratedByDCManagerWithVN(virtualNumberResponse, trackingNumber);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "API: BulkUpdate Virtual Numbers", enabled = false, retryAnalyzer = Retry.class)
    public void testGenerateVirtualNumbersAndPinAPI() {
        String event = "";
        String clientReferenceId ="";
        String connectionId = "";
        String virtualNumber = "";
        String partyOnePins = "";
        String partyTwoPins = "";
        try {
            lastmileServiceHelper.genrateVirtualNumbersAndPin(event , clientReferenceId , connectionId , virtualNumber , partyOnePins , partyTwoPins);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "API: initiateStartTrip", retryAnalyzer = Retry.class)
    public void testInitiateStartTripAPI() {
        Object payloadClass = null;
        String tripId;
        try {
            tripId = mobileNumberMaskingHelper.createMockOrderAndAssignShipmentToTrip().get("tripId");
            TripOrderAssignmentResponse tripOrderResponse = tripClient_qa.initiateStartTripForMNM(Long.valueOf(tripId));
            String statusType = tripOrderResponse.getStatus().getStatusType().toString();
            String statusMessage = tripOrderResponse.getStatus().getStatusMessage().toString();
            System.out.println("statusType=" + statusType);
            Assert.assertEquals(statusType, "SUCCESS", statusType + ":" + statusMessage);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    @Test(description = "API: refreshTrips" , enabled = false, retryAnalyzer = Retry.class)
    public void testRefreshTripsAPI() {
        Object payloadClass = null;
        String tripId;
        try {
            TripResponse responseBody = lastmileServiceHelper.refreshTrips(DcId);
            Assert.assertEquals(responseBody.getStatus(), "SUCCESS", responseBody.getStatus().toString());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    /**
     * Flows which covers All API's of mobile number masking
     */

    @Test(description = "Covers the straight forward case of mobile number masking - Where Virtual number is generated and trip started and completed successfully"
    ,retryAnalyzer = Retry.class)
    public void testForwardFlowMobileNumberMasking(){
        Map<String, String> orderMap;
        Map<String, String> postedVNentries;
        String trackingNumber , tripId, packetId;
        TripResponse tripResponse;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderAndAssignShipmentToTrip();
            trackingNumber = orderMap.get("trackingNumber");
            tripId = orderMap.get("tripId");
            packetId = orderMap.get("packetId");
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.AWAITING);
            VirtualNumberResponse virtualNumberResponse = lastmileServiceHelper.getVirualNumberByTrackingNumber(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            mobileNumberMaskingValidator.validateVirtualNumberResponseBasic(virtualNumberResponse);
            mobileNumberMaskingValidator.validateGetVirtualNumberByTrackingNumberBefore(virtualNumberResponse, trackingNumber);
            //Generate Virtual Number Using API
            postedVNentries = mobileNumberMaskingHelper.generateVirtualNumberByTrackingId(trackingNumber);
            mobileNumberMaskingValidator.validateVirtualNumberEntriesDB(trackingNumber , postedVNentries);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
            //check generated Virtual number using API and verify with DB
            virtualNumberResponse = lastmileServiceHelper.getVirualNumberByTrackingNumber(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            System.out.println(virtualNumberResponse);
            mobileNumberMaskingValidator.validateVirtualNumberResponseBasic(virtualNumberResponse);
            mobileNumberMaskingValidator.validateGetVirtualNumberByTrackingNumberAfter(virtualNumberResponse, trackingNumber);
            tripResponse = mobileNumberMaskingHelper.startTrip(tripId, packetId); //perform initiate trip
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumber, tripResponse);
            mobileNumberMaskingHelper.updateDeliveryTrip(tripId, packetId);
            mobileNumberMaskingHelper.completeTrip(tripId, packetId);
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    /**
     * Scenario: In case of virtual number not generated before trip start. We start the trip after 2 mins (We check if VN is generated meanwhile).
     */
    @Test(description = "Covers the straight forward case of mobile number masking - Where Virtual number is not generated and trip started"
    ,retryAnalyzer = Retry.class)
    public void testForwardFlowMobileNumberMaskingNegative(){
        Map<String, String> orderMap;
        Map<String, String> postedVNentries;
        String trackingNumber , tripId, packetId;
        TripResponse tripResponse;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderAndAssignShipmentToTrip();
            trackingNumber = orderMap.get("trackingNumber");
            tripId = orderMap.get("tripId");
            packetId = orderMap.get("packetId");
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.AWAITING);
            VirtualNumberResponse virtualNumberResponse = lastmileServiceHelper.getVirualNumberByTrackingNumber(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            mobileNumberMaskingValidator.validateVirtualNumberResponseBasic(virtualNumberResponse);
            mobileNumberMaskingValidator.validateGetVirtualNumberByTrackingNumberBefore(virtualNumberResponse, trackingNumber);
            tripResponse = mobileNumberMaskingHelper.startTrip(tripId, packetId);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumber, tripResponse);
            //Force Start Again - This option has been removed from requirement - As per Kiran
            //tripResponse = mobileNumberMaskingHelper.startTrip(tripId, packetId);
            //mobileNumberMaskingValidator.validateVNandTripStatusReInitiateStart(trackingNumber, tripResponse);
            //after 2 mins
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusAfterDelayWhenNoVN(trackingNumber, tripId);
            mobileNumberMaskingHelper.updateDeliveryTrip(tripId, packetId);
            mobileNumberMaskingHelper.completeTrip(tripId, packetId);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.NOT_RECEIVED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }

    /**
     * Scenario: In case of virtual number is generated after trip start.
     * Ideally We start the trip after 2 mins (We check if VN is generated meanwhile).
     */
    @Test(description = "Covers the straight forward case of mobile number masking - Where Virtual number is not generated and trip started", enabled = false,
            retryAnalyzer = Retry.class)
    public void testVNGeneratedAfterTripStart(){
        Map<String, String> orderMap;
        Map<String, String> postedVNentries;
        String trackingNumber , tripId, packetId;
        TripResponse tripResponse;
        try {
            orderMap = mobileNumberMaskingHelper.createMockOrderAndAssignShipmentToTrip();
            trackingNumber = orderMap.get("trackingNumber");
            tripId = orderMap.get("tripId");
            packetId = orderMap.get("packetId");
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.AWAITING);
            VirtualNumberResponse virtualNumberResponse = lastmileServiceHelper.getVirualNumberByTrackingNumber(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
            mobileNumberMaskingValidator.validateVirtualNumberResponseBasic(virtualNumberResponse);
            mobileNumberMaskingValidator.validateGetVirtualNumberByTrackingNumberBefore(virtualNumberResponse, trackingNumber);
            tripResponse = mobileNumberMaskingHelper.startTrip(tripId, packetId);
            mobileNumberMaskingValidator.validateVNandTripStatusInitialStart(trackingNumber, tripResponse);
            //Force Start Again
            tripResponse = mobileNumberMaskingHelper.startTrip(tripId, packetId);
            mobileNumberMaskingValidator.validateVNandTripStatusReInitiateStart(trackingNumber, tripResponse);
            //Generate Virtual Number Using API
            postedVNentries = mobileNumberMaskingHelper.generateVirtualNumberByTrackingId(trackingNumber);
            mobileNumberMaskingValidator.validateVirtualNumberEntriesDB(trackingNumber , postedVNentries);
            mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.RECEIVED);
            //Force Start Again
            tripResponse = mobileNumberMaskingHelper.startTrip(tripId, packetId);
            mobileNumberMaskingValidator.waitForDuration(LASTMILE_CONSTANTS.MAX_TIMEOUT_VIRTUAL_NUMBER_GEN);
            mobileNumberMaskingValidator.validateVNandTripStatusWhenVNGenerated(trackingNumber, tripResponse);
            mobileNumberMaskingHelper.updateDeliveryTrip(tripId, packetId);
            mobileNumberMaskingHelper.completeTrip(tripId, packetId);
            //mobileNumberMaskingValidator.validateVNAllocationStatus(trackingNumber, VirtualNumberAllocationStatus.DEALLOCATED);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    /**
     * UI Cases on Mobile Number Masking
     */
    //TODO (Bharath) use trip client getAllIncompleteOrdersForDC
    @Test(description = "API: Trip Planning - Pick-ups section - Mobile numbers should be masked")
    public void testMobileNumberMaskingPickupsAPI() throws Exception {
        OrderResponse tripOrderResponse = null;
        tripOrderResponse = lastmileServiceHelper.getAllIncompleteOrdersForDC(Long.parseLong(DcId), ShipmentType.PU, 0, 20, LASTMILE_CONSTANTS.SORT_BY_ZIPCODE, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());
        mobileNumberMaskingValidator.validateMaskedNumber(tripOrderResponse);
    }

    @Test(description = "API: Trip Planning - Scheduled Pick-ups section - Mobile numbers should be masked")
    public void testMobileNumberMaskingScheduledPickupsAPI() throws Exception {
        OrderResponse tripOrderResponse = null;
        tripOrderResponse = lastmileServiceHelper.getAllIncompleteScheduledShipmentsForDC(Long.parseLong(DcId), ShipmentType.PU, 0, 20, LASTMILE_CONSTANTS.SORT_BY_ZIPCODE, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());
        mobileNumberMaskingValidator.validateMaskedNumber(tripOrderResponse);
    }

    @Test(description = "API: Trip Planning - Deliveries section - Mobile numbers should be masked")
    public void testMobileNumberMaskingDeliveriesAPI() throws Exception {
        OrderResponse tripOrderResponse = null;
        tripOrderResponse = lastmileServiceHelper.getAllIncompleteOrdersForDC(Long.parseLong(DcId), ShipmentType.DL, 0, 20, LASTMILE_CONSTANTS.SORT_BY_ZIPCODE, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());
        mobileNumberMaskingValidator.validateMaskedNumber(tripOrderResponse);
    }

    @Test(description = "API: Trip Planning - Exchanges section - Mobile numbers should be masked")
    public void testMobileNumberMaskingExchangesAPI() throws Exception {
        OrderResponse tripOrderResponse = null;
        tripOrderResponse = lastmileServiceHelper.getAllIncompleteExchangesForDC(Long.parseLong(DcId), 0, 20, LASTMILE_CONSTANTS.SORT_BY_ZIPCODE, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());
        mobileNumberMaskingValidator.validateMaskedNumber(tripOrderResponse);
    }

    @Test(description = "API: Trip Planning - Try and Buys - Mobile numbers should be masked")
    public void testMobileNumberMaskingTryAndBuysAPI() throws Exception {
        OrderResponse tripOrderResponse = null;
        tripOrderResponse = lastmileServiceHelper.getAllIncompleteOrdersForDC(Long.parseLong(DcId), ShipmentType.TRY_AND_BUY, 0, 20, LASTMILE_CONSTANTS.SORT_BY_ZIPCODE, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());
        mobileNumberMaskingValidator.validateMaskedNumber(tripOrderResponse);
    }

    @Test(description = "API: Trip Planning - Deliveries section -  Verify Mobile number field Sorting - w.r.t shipment tracking number",   dataProvider = "Sort Order", dataProviderClass = MobileNumberMaskingDP.class)
    public void testMobileNumberMaskingSortDL(String sortOrder) throws Exception {
        OrderResponse tripOrderResponse = null;
        tripOrderResponse = lastmileServiceHelper.getAllIncompleteOrdersForDC(Long.parseLong(DcId), ShipmentType.DL, 0, 20, LASTMILE_CONSTANTS.SORT_BY_MOBILE, sortOrder, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());
        mobileNumberMaskingValidator.validateMobileNumberSorting(ShipmentType.DL, tripOrderResponse, sortOrder);
    }

    @Test(description = "API: Trip Planning - Pickup section -  Verify Mobile number field Sorting - w.r.t shipment tracking number", dataProvider = "Sort Order" , dataProviderClass = MobileNumberMaskingDP.class)
    public void testMobileNumberMaskingSortPU(String sortOrder) throws Exception {
        OrderResponse tripOrderResponse = null;
        tripOrderResponse = lastmileServiceHelper.getAllIncompleteOrdersForDC(Long.parseLong(DcId), ShipmentType.PU, 0, 20, LASTMILE_CONSTANTS.SORT_BY_MOBILE, sortOrder, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());
        mobileNumberMaskingValidator.validateMobileNumberSorting(ShipmentType.PU, tripOrderResponse, sortOrder);
    }

    @Test(description = "API: Trip Planning - Exchange section -  Verify Mobile number field Sorting - w.r.t shipment tracking number" ,
            dataProvider = "Sort Order", dataProviderClass = MobileNumberMaskingDP.class,
            retryAnalyzer = Retry.class)
    public void testMobileNumberMaskingSortExchange(String sortOrder) throws Exception {
        OrderResponse tripOrderResponse = null;
        tripOrderResponse = lastmileServiceHelper.getAllIncompleteExchangesForDC(Long.parseLong(DcId), 0, 20, LASTMILE_CONSTANTS.SORT_BY_MOBILE, sortOrder, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());
        mobileNumberMaskingValidator.validateMobileNumberSorting(ShipmentType.EXCHANGE, tripOrderResponse, sortOrder);
    }

    @Test(description = "API: Trip Planning - TryAndBuy section -  Verify Mobile number field Sorting - w.r.t shipment tracking number"
            , dataProvider = "Sort Order", dataProviderClass = MobileNumberMaskingDP.class,
            retryAnalyzer = Retry.class)
    public void testMobileNumberMaskingSortTryAndBuy(String sortOrder) throws Exception {
        OrderResponse tripOrderResponse = null;
        tripOrderResponse = lastmileServiceHelper.getAllIncompleteOrdersForDC(Long.parseLong(DcId), ShipmentType.TRY_AND_BUY, 0, 20, LASTMILE_CONSTANTS.SORT_BY_MOBILE, sortOrder, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());
        mobileNumberMaskingValidator.validateMobileNumberSorting(ShipmentType.TRY_AND_BUY, tripOrderResponse, sortOrder);
    }

    @Test(description = "API: Trip Planning - Scheduled Pickups section -  Verify Mobile number field Sorting - w.r.t shipment tracking number" , dataProvider = "Sort Order" ,
            enabled=false, retryAnalyzer = Retry.class)
    public void testMobileNumberMaskingSortTryAndBuy() throws Exception {
        OrderResponse tripOrderResponse = null;
        String sortOrder = "ASC";
        tripOrderResponse = lastmileServiceHelper.getAllIncompleteScheduledShipmentsForDC(Long.parseLong(DcId), ShipmentType.PU, 0, 20, LASTMILE_CONSTANTS.SORT_BY_MOBILE, sortOrder, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());
        mobileNumberMaskingValidator.validateMobileNumberSorting(ShipmentType.PO, tripOrderResponse, sortOrder);
    }

    //Negative Cases

    @Test(description = "Negative Case : If no shipments are assigned to trip and we try to start trip, it should throw error"
    ,retryAnalyzer = Retry.class)
    public void testUnassignedShipmentInitiateStartTrip() {
        Object payloadClass = null;
        String tripId;
        try {
            tripId = mobileNumberMaskingHelper.createTrip(DcId).get("tripId");
            TripOrderAssignmentResponse tripOrderResponse = tripClient_qa.initiateStartTrip(tripId);
            String statusType = tripOrderResponse.getStatus().getStatusType().toString();
            String statusMessage = tripOrderResponse.getStatus().getStatusMessage().toString();
            Assert.assertEquals(statusType, "ERROR", statusType + ":" + statusMessage);
            Assert.assertEquals(statusMessage, ". Trip does not contain any deliveries or pickups", "Bug: Trip started without shipments!");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }


    @Test(description = "Negative Case : If Shipment is Completed/unassigined, it should throw error"
            , retryAnalyzer = Retry.class)
    public void testUnAssignedShipmentGetVNWithTrackingNumber() {
        Object payloadClass = null;
        String tripId;
        VirtualNumberResponse virtualNumberResponse;
        try {
            virtualNumberResponse = lastmileServiceHelper.getVirualNumberByTrackingNumber(LASTMILE_CONSTANTS.TENANT_ID, LastMileDAO.GetUnAssignedShipment());
            String statusType = virtualNumberResponse.getStatus().getStatusType().toString();
            String statusMessage = virtualNumberResponse.getStatus().getStatusMessage().toString();
            System.out.println("statusType=" + statusType);
            Assert.assertEquals(statusType, "ERROR", statusType + ":" + statusMessage);
            Assert.assertEquals(statusMessage, "No trip details found", "Bug: Unassigned Shipments should not return any Virtual Numbers!");
            System.out.println(virtualNumberResponse.getVirtualNumberEntries());
            Assert.assertNull(virtualNumberResponse.getVirtualNumberEntries(), "Bug: Unassigned Shipments should not return any Virtual Numbers Entries!");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Exception Caught " + e.getLocalizedMessage());
        }
    }
}
