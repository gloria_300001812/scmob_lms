package com.myntra.apiTests.erpservices.lastmile.dp;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.Toolbox;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class LastmileDP {


    static Initialize init = new Initialize("/Data/configuration");
    static String FGTenantID = "";

    public LastmileDP() {
        if (LMS_CONSTANTS.FGVersion == 3) {
            FGTenantID = "tenantId.eq:" + LMS_CONSTANTS.TENANTID + "___";
        }
        if (LMS_CONSTANTS.FGVersion == 0) {
            FGTenantID = "";
        }
    }

    @DataProvider
    public static Object[][] addDCdata(ITestContext testContext) {
        Object[] arr1 = {"MADC", "myntra_auto_dc_1", "auto_test_manger_1", "1", "auto_test_test_123", "Banaglore", "YLH_12", "KA",
                "998877", "0", "0", "1", "1", "ML", "1200002213", "ML", "801", "DELIVERY_CENTER added successfully", EnumSCM.SUCCESS};
        Object[] arr2 = {"MADC", "myntra_auto_dc_1", "auto_test_manger_1", "1", "auto_test_test_123", "Banaglore", "YLH_12", "KA",
                "989898", "0", "0", "1", "1", "ML", "1200002213", "FRANCHISE", "801", "DELIVERY_CENTER added successfully", EnumSCM.SUCCESS};

        Object[][] dataSet = new Object[][]{arr1, arr2};
        //Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 5, 6);
    }
}