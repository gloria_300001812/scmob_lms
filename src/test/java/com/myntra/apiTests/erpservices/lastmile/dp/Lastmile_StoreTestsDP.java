package com.myntra.apiTests.erpservices.lastmile.dp;

import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;

import com.myntra.lastmile.client.code.utils.ReconType;

import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lordoftherings.Toolbox;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class Lastmile_StoreTestsDP {


    @DataProvider
    public static Object[][] createStore(ITestContext testContext) {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");

        Integer masterBagCapacity = new Integer(100);
        Long store = new Long(1l);
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));

        Object[] arr1 = {"BA" + String.valueOf(contactNumber).substring(5, 9), "LA_FN" + String.valueOf(contactNumber).substring(5, 9), "LA_LN" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.MENSA_OLD_STORE_DC_CODE,
                "LA_gstin" + String.valueOf(contactNumber).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, "LA" + String.valueOf(contactNumber).substring(5, 9)};
        Object[] arr2 = {"BA" + String.valueOf(contactNumber1).substring(5, 9), "LA_FN" + String.valueOf(contactNumber).substring(5, 9), "LA_LN" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber1).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber1,
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.MENSA_OLD_STORE_DC_CODE,
                "LA_gstin" + String.valueOf(contactNumber1).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.ROLLING_RECON, 500, "LA" + String.valueOf(contactNumber1).substring(5, 9)};
        Object[][] dataSet = new Object[][]{arr1, arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }

    @DataProvider
    public static Object[][] updateStore(ITestContext testContext) {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");

        Integer masterBagCapacity = new Integer(100);
        Long store = new Long(1l);
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        Object[] arr1 = {"BA" + String.valueOf(contactNumber).substring(5, 9), "LA_FN" + String.valueOf(contactNumber).substring(5, 9), "LA_LN" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.MENSA_OLD_STORE_DC_CODE,
                "LA_gstin" + String.valueOf(contactNumber).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, "LA" + String.valueOf(contactNumber).substring(5, 9), ReconType.ROLLING_RECON};
        Object[] arr2 = {"BA" + String.valueOf(contactNumber1).substring(5, 9), "LA_FN" + String.valueOf(contactNumber).substring(5, 9), "LA_LN" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber1).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber1,
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.MENSA_OLD_STORE_DC_CODE,
                "LA_gstin" + String.valueOf(contactNumber1).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.ROLLING_RECON, 500, "LA" + String.valueOf(contactNumber1).substring(5, 9), ReconType.EOD_RECON};
        Object[][] dataSet = new Object[][]{arr1, arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 4, 4);
    }
}
