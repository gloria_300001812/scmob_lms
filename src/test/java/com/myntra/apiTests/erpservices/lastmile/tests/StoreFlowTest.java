package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.Utils.CommonUtils;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_StoreFlowsDP;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Helper.HubToTransportHubConfigResponse;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.commons.response.EmptyResponse;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;
import com.myntra.khata.response.PaymentResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.LastmileConstants;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.*;
import com.myntra.lms.client.status.ShipmentStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.*;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class StoreFlowTest {

    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private TripClient_QA tripClient_qa = new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa = new DeliveryCenterClient_QA();
    private LastmileHelper lastmileHelper;
    private MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    StoreClient_QA storeClient_qa = new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa = new MasterBagClient_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa = new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa = new MLShipmentClientV2_QA();
    ReturnHelper returnHelper=new ReturnHelper();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TMSServiceHelper tmsServiceHelper=new TMSServiceHelper();
    LastmileClient lastmileClient=new LastmileClient();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    static String searchParams;
    static Long storeHlPId;
    static Long storeSDAId;
    static String storeTenantId;
    static String tenantId;
    static String pincode;
    static String mobileNumber;
    static String storeCourierCode;
    static String code;

    static String code1;
    static String storeCourierCode1;
    static String searchParams1;
    static Long storeHlPId1;
    static Long storeSDAId1;
    static String storeTenantId1;
    static String mobileNumber1;

    static String code2;
    static String storeCourierCode2;
    static String searchParams2;
    static Long storeHlPId2;
    static Long storeSDAId2;
    static String storeTenantId2;
    static String mobileNumber2;

    @BeforeTest
    public void createStore() {
        Random r = new Random(System.currentTimeMillis());
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        int contactNumber2 = Math.abs(2000000000 + r.nextInt(300000000));
        int contactNumber3 = Math.abs(2000000000 + r.nextInt(300000000));
        String value1 = CommonUtils.generateString(3);
        String value2 = CommonUtils.generateString(3);
        String value3 = CommonUtils.generateString(3);

        String name = value1 + String.valueOf(contactNumber1).substring(5, 9);
        String ownerFirstName = value1 + String.valueOf(contactNumber1).substring(5, 9);
        String ownerLastName = value1 + String.valueOf(contactNumber1).substring(5, 9);
        code = value1 + String.valueOf(contactNumber1).substring(5, 9);
        code1 = value2 + String.valueOf(contactNumber2).substring(5, 9);
        code2 = value3 + String.valueOf(contactNumber3).substring(5, 9);
        String latLong = "LA_latlong" + String.valueOf(contactNumber1).substring(5, 9);
        String emailId = "test@lastmileAutomation.com";
        mobileNumber = String.valueOf(contactNumber1);
        String address = "Automation Address";
        pincode = LASTMILE_CONSTANTS.STORE_PINCODE;
        String city = "Bangalore";
        String state = "Karnataka";
        String mappedDcCode = LASTMILE_CONSTANTS.ROLLING_DC_CODE;
        String gstin = "LA_gstin" + String.valueOf(contactNumber1).substring(5, 9);
        tenantId = LMS_CONSTANTS.TENANTID;

        String name1 = value2 + String.valueOf(contactNumber2).substring(4, 8);
        String ownerFirstName1 = value2 + String.valueOf(contactNumber2).substring(4, 8);
        String ownerLastName1 = value2 + String.valueOf(contactNumber2).substring(4, 8);
        String latLong1 = "LA_latlong" + String.valueOf(contactNumber2).substring(4, 8);
        mobileNumber1 = String.valueOf(contactNumber2);
        String gstin1 = "LA_gstin" + String.valueOf(contactNumber2).substring(4, 8);

        String name2 = value3 + String.valueOf(contactNumber3).substring(3, 7);
        String ownerFirstName2 = value3 + String.valueOf(contactNumber3).substring(3, 7);
        String ownerLastName2 = value3 + String.valueOf(contactNumber3).substring(3, 7);
        String latLong2 = "LA_latlong" + String.valueOf(contactNumber3).substring(3, 7);
        mobileNumber2 = String.valueOf(contactNumber3);
        String gstin2 = "LA_gstin" + String.valueOf(contactNumber3).substring(3, 7);


        searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId = deliveryStaffEntry.getId();
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeCourierCode = storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //create 2nd store
        searchParams1 = "code.like:" + code1;
        //Create Store
        StoreResponse storeResponse1 = lastmileHelper.createStore(name1, ownerFirstName1, ownerLastName1, latLong1, "test1@lastmileAutomation.com", mobileNumber1, address, pincode, city, state, mappedDcCode,
                gstin1, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code1);
        //validate Store created successfully
        Assert.assertTrue(storeResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId1 = storeResponse1.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry1 = storeResponse1.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId1 = deliveryStaffEntry1.getId();
        storeTenantId1 = storeResponse1.getStoreEntries().get(0).getTenantId();
        storeCourierCode1=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //3rd store
        searchParams2 = "code.like:" + code2;
        //Create Store
        StoreResponse storeResponse2 = lastmileHelper.createStore(name2, ownerFirstName2, ownerLastName2, latLong2, "test1@lastmileAutomation.com", mobileNumber2, address, LASTMILE_CONSTANTS.EOD_PINCODE, city, state, LASTMILE_CONSTANTS.EOD_DC_CODE,
                gstin2, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code2);
        //validate Store created successfully
        Assert.assertTrue(storeResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId2 = storeResponse2.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry2 = storeResponse2.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId2 = deliveryStaffEntry2.getId();
        storeTenantId2 = storeResponse2.getStoreEntries().get(0).getTenantId();
        storeCourierCode2=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();
        //update EOD _Recon to ROLLING_Recon
        lastmileHelper.updateStore(name2, address,  LASTMILE_CONSTANTS.EOD_PINCODE, city, state, tenantId,true, false, true, code2, ReconType.ROLLING_RECON, storeHlPId2);
        StoreResponse updateStoreAsEOD = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams2, "");
        Assert.assertEquals(updateStoreAsEOD.getStoreEntries().get(0).getReconType().toString(),ReconType.ROLLING_RECON.toString(),"EOD_RECON store is not updated as ROLLING_RECON");

    }

   @Test(enabled = true,dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "storeCreation",description = "TC ID : 28039, Create a new store and tag it as EOD_RECON and attach it to a DC which has been tagged as EOD_RECON")
   public void process_CreateEODReconStore_AttachEODReconDCToStore_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address,  String city, String state,
                                                                              String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType,  Integer capacity, String code){
       searchParams = "code.like:" + code;
       //Create Store
       StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, LASTMILE_CONSTANTS.STORE_PINCODE, city, state, LASTMILE_CONSTANTS.ROLLING_DC_CODE,
               gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, ReconType.EOD_RECON, capacity, code);
       //validate Store created successfully
       Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
   }

    @Test(enabled = true,dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "storeCreation",description = "TC ID : 28040, Create a new store and tag it as EOD_RECON and attach it to a DC which has been tagged as ROLLING_RECON")
    public void process_CreateEODReconStore_AttachRollingReconDCToStore_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address,  String city, String state,
                                                                                   String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType,  Integer capacity, String code){

        searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, LASTMILE_CONSTANTS.EOD_PINCODE, city, state, LASTMILE_CONSTANTS.EOD_DC_CODE,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, ReconType.EOD_RECON, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        //check teh store has mapped to ROLLING dc or not
        Map<String, Object> dcDetails = storeHelper.getMappedDCDetails(String.valueOf(storeHlPId));
        Assert.assertEquals(dcDetails.get("delivery_center_id").toString(),"1","EOD_ROCON store is not mapped to ROLLING_RECON DC");

    }

    @Test(enabled = true,dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "storeCreation",description = "TC ID : 28041, Create a new store and tag it as ROLLING_RECON and attach it to a DC which has been tagged as EOD_RECON - should throw error")
    public void process_CreateROLLINGReconStore_AttachEODReconDCToStore_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address,  String city, String state,
                                                                                   String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType,  Integer capacity, String code){

       searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address,  LASTMILE_CONSTANTS.STORE_PINCODE, city, state, LASTMILE_CONSTANTS.ROLLING_DC_CODE,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, ReconType.EOD_RECON, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        //update Rolling _Recon to EOD_Recon
        StoreResponse storeResponse1 = lastmileHelper.updateStore(name, address,  LASTMILE_CONSTANTS.STORE_PINCODE, city, state, tenantId,
                isAvailable, isDeleted, isCardEnabled, code, ReconType.ROLLING_RECON, storeHlPId);
        Assert.assertEquals(storeResponse1.getStatus().getStatusType().toString(),EnumSCM.ERROR,"EOD_RECON store is updated as ROLLING_RECON");
    }

    @Test(enabled = true,dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "storeCreation",description = "TC ID : 28042, Create a new store and tag it as ROLLING_RECON and attach it to a DC which has been tagged as ROLLING_RECON")
    public void process_CreateROLLINGReconStore_AttachRollingReconDCToStore_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address,  String city, String state,
                                                                                       String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType,  Integer capacity, String code){

        searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, LASTMILE_CONSTANTS.EOD_PINCODE, city, state, LASTMILE_CONSTANTS.EOD_DC_CODE,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, ReconType.EOD_RECON, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        StoreResponse updateStoreAsRolling = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Assert.assertEquals(updateStoreAsRolling.getStoreEntries().get(0).getReconType().toString(),ReconType.EOD_RECON.toString(),"ROLLING_RECON store is not updated as EOD_RECON");

        //update EOD _Recon to ROLLING_Recon
        lastmileHelper.updateStore(name, address, LASTMILE_CONSTANTS.EOD_PINCODE, city, state, tenantId,isAvailable, isDeleted, isCardEnabled, code, ReconType.ROLLING_RECON, storeHlPId);
        StoreResponse updateStoreAsEOD = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Assert.assertEquals(updateStoreAsEOD.getStoreEntries().get(0).getReconType().toString(),ReconType.ROLLING_RECON.toString(),"EOD_RECON store is not updated as ROLLING_RECON");

    }

    @Test(enabled = true,dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "storeCreation",description = "TC ID : 28042, Create a new store and tag it as ROLLING_RECON and attach it to a DC which has been tagged as ROLLING_RECON")
    public void process_CreateROLLINGReconStore_UpdateAsEODRECON_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address,  String city, String state,
                                                                            String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType,  Integer capacity, String code) {

        searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, LASTMILE_CONSTANTS.EOD_PINCODE, city, state, LASTMILE_CONSTANTS.EOD_DC_CODE,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, ReconType.EOD_RECON, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId = deliveryStaffEntry.getId();
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeCourierCode = storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //update EOD _Recon to ROLLING_Recon
        lastmileHelper.updateStore(name, address,  LASTMILE_CONSTANTS.EOD_PINCODE, city, state, tenantId,isAvailable, isDeleted, isCardEnabled, code, ReconType.ROLLING_RECON, storeHlPId);
        StoreResponse updateStoreAsRolling = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Assert.assertEquals(updateStoreAsRolling.getStoreEntries().get(0).getReconType().toString(),ReconType.ROLLING_RECON.toString(),"ROLLING_RECON store is not updated as EOD_RECON");

        //update Rolling _Recon to EOD_Recon
       lastmileHelper.updateStore(name, address,  LASTMILE_CONSTANTS.STORE_PINCODE, city, state, tenantId,isAvailable, isDeleted, isCardEnabled, code, ReconType.EOD_RECON, storeHlPId);
        StoreResponse updateStoreAsEOD = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Assert.assertEquals(updateStoreAsEOD.getStoreEntries().get(0).getReconType().toString(),ReconType.EOD_RECON.toString(),"ROLLING_RECON store is not updated as EOD_RECON");
    }


    @Test(enabled = true, description = "TC ID: C28045, Negative:-Scanning of a shipment in state PK")
    public void process_ScanShipmentWhichIsInPKState_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.PACKED);
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(deliveryCenterId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        Assert.assertEquals(response, EnumSCM.ERROR, "Shipment added into master bag when the shipment status is in PACKED state");

    }

    @Test(enabled = true, description = "TC ID: C28046, POSITIVE :-Scanning of a shipment in state IS")
    public void process_ScanShipmentWhichIsInISState_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.INSCANNED);
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(deliveryCenterId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        Assert.assertEquals(response, EnumSCM.ERROR, "Shipment added into master bag when the shipment status is in PACKED state");

    }

    @Test(enabled = true, description = "TC ID: C28047 & C28085, Scanning of a shipment which is another store bag")
    public void process_ScanShipmentWhichIsInAnotherMasterBag_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(deliveryCenterId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        Assert.assertEquals(response, EnumSCM.SUCCESS, "shipment is not added into mb ");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        ShipmentResponse createMB = lastmileHelper.createMasterBag(deliveryCenterId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(createMB.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = createMB.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo addShipmentIntoMB = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String addShipmentIntoMBResponse = masterBagClient_qa.addShipmentToStoreBag(masterBagId1, addShipmentIntoMB);
        Assert.assertEquals(addShipmentIntoMBResponse, EnumSCM.SUCCESS, "shipment is not added into another store bag when the shipment already present into another store bag");
    }

    @Test(enabled = true, description = "TC ID: C28048, Scanning of shipment which is added directly into another Myntra SDA trip (Customer Order)")
    public void process_ScanShipmentWhichIsAddedIntoMyntraSDATrip_Successful() throws Exception {

       String orderId = lmsHelper.createMockOrder(EnumSCM.PK, "560068", "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));

        //Order Inscan V2
        String inScanResponse = lmsServiceHelper.orderInScanNew(packetId, "36",false, LMS_CONSTANTS.TENANTID);
        System.out.println("response="+inScanResponse);
        System.out.println("inScanResponse=");

        //MasterBag V2
        MasterbagDomain mbCreateResponse = masterBagServiceHelper.createMasterBag("DH-BLR", "ELC", ShippingMethod.NORMAL, "ML", tenantId);
        Long masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId="+masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL,  LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse= (HubToTransportHubConfigResponse)tmsServiceHelper.getLocationHubConfigFW.apply("DH-BLR");
        String transportHubCode= hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode="+transportHubCode);
        TMSMasterbagReponse tmsMasterbagReponse= (TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply("TH-BLR", masterBagId);
        System.out.println("tmsMasterbagReponse="+tmsMasterbagReponse.getStatus());
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply("TH-BLR", "ELC")).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, "ELC", laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId="+containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse)tmsServiceHelper.containerInTransitScan.apply(containerId,"ELC")).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBagNew.apply("ELC", masterBagId, containerId);

        //MasterBag In-scan V2
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, tenantId);
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        while(shipmentEntryItr.hasNext()){
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), shipmentEntry.getOrderShipmentAssociationEntries().get(0).getTrackingNumber(),
                    shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId() ,
                    ShipmentType.DL , PremisesType.DC);
        }

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        String mobileNumber = tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not " + LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        lastmileOperations.assignOrderToTripByTrackingNumber(trackingNumber, tripId);
        lastmileOperations.startTrip(tripId, trackingNumber);

        //Create storeBag and try to scan shipment which is in myntra SDA trip
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(5, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo);
        Assert.assertEquals(response, EnumSCM.ERROR, "shipment is added into mb when that shipment present in myntra SDA trip ");

    }

    @Test(enabled = true, description = "TC ID: C28049, Scanning of a shipment which is already assigned to this same store trip")
    public void process_ScanShipmentWhichIsAssignToSameStore_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(deliveryCenterId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        Assert.assertEquals(response, EnumSCM.SUCCESS, "shipment is not added into mb ");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(deliveryCenterId));
        TripResponse tripResponse = tripClient_qa.createTrip(deliveryCenterId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);
        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());

        //create store bag for same store and try to add shipment
        ShipmentResponse createMB = lastmileHelper.createMasterBag(deliveryCenterId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(createMB.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = createMB.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo addShipmentIntoMB = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String addShipmentIntoMBResponse = masterBagClient_qa.addShipmentToStoreBag(masterBagId1, addShipmentIntoMB);
        Assert.assertEquals(addShipmentIntoMBResponse, EnumSCM.SUCCESS, "shipment is not added into store bag when the shipment present into same store bag trip");
    }

    @Test(enabled = true, description = "TC ID: C28050, Scanning of a shipment which is already assigned to another store's trip")
    public void process_ScanShipmentWhichIsAssignToAnotherStore_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(deliveryCenterId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        Assert.assertEquals(response, EnumSCM.SUCCESS, "shipment is not added into mb ");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(deliveryCenterId));
        TripResponse tripResponse = tripClient_qa.createTrip(deliveryCenterId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);
        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());

        //create another store bag
        String originDCCity1 = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode1 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams1, "");
        String destinationStoreCity1 = searchStoreByCode1.getStoreEntries().get(0).getCity();
        ShipmentResponse shipmentResponse1 = lastmileHelper.createMasterBag(deliveryCenterId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity1, destinationStoreCity1);
        Assert.assertTrue(shipmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = shipmentResponse1.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo addShipmentIntoMB = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String addShipmentIntoMBResponse = masterBagClient_qa.addShipmentToStoreBag(masterBagId1, addShipmentIntoMB);
        Assert.assertEquals(addShipmentIntoMBResponse, EnumSCM.SUCCESS, "shipment not added into store bag when the shipment present into same store bag trip");
    }

    @Test(enabled = true, description = "TC ID: C28051, Scanning of a Return shipment which is PickedUp Successful")
    public void process_ScanReturnShipmentWhichIsPickedUpSuccessfulState_Successful() throws Exception {
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());
        //getReturnTrackingNumber
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        ReturnResponse LMSReturnTrackingNumber = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Thread.sleep(2000);
        String returnTrackingNumber = LMSReturnTrackingNumber.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.logistics.platform.domain.ShipmentStatus.RETURN_CREATED.toString());

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.UNASSIGNED);

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        Long storeTripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.OUT_FOR_PICKUP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storeTripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successful in store
        TripOrderAssignmentResponse updatePickupInTrip = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE, tenantId);
        Assert.assertTrue(updatePickupInTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.PICKUP_SUCCESSFUL);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(deliveryCenterId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo addShipmentIntoMB = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String addShipmentIntoMBResponse = masterBagClient_qa.addShipmentToStoreBag(masterBagId, addShipmentIntoMB);
        Assert.assertEquals(addShipmentIntoMBResponse, EnumSCM.ERROR, "shipment added into store bag when the status is PickedUp successful");
    }

    @Test(enabled = true, description = "TC ID: C28052,Removing shipment from trip")
    public void process_RemoveStoreShipmentFromTrip_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(deliveryCenterId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        Assert.assertEquals(response, EnumSCM.SUCCESS, "shipment is not added into mb ");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(deliveryCenterId));
        TripResponse tripResponse = tripClient_qa.createTrip(deliveryCenterId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);
        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());

        //Remove Store shipment from trip
        EmptyResponse removeShipmentResponse = storeHelper.removeStoreShipmentFromTrip(masterBagId, tripId);
        Assert.assertEquals(removeShipmentResponse.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Store shipment is not removed from trip");
        //validate trip_sShipment_Association status
        Map<String, Object> tripShipmentAssociation = DBUtilities.exSelectQueryForSingleRecord("SELECT * FROM trip_shipment_association WHERE trip_id="+tripId,"myntra_lms");
        Assert.assertEquals(tripShipmentAssociation.get("status").toString(),"REM"," trip shipment association status is not expected");
    }

    @Test(enabled = true, description = "TC ID: C28053, Assign a pickup which is assigned to some other store")
    public void process_AssignPickupToAnotherStore_Successful() throws Exception {
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        //getReturnTrackingNumber
        ReturnResponse getReturnTrackingNumber = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = getReturnTrackingNumber.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.logistics.platform.domain.ShipmentStatus.RETURN_CREATED.toString());

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.UNASSIGNED);

        //assign pickup to another store
        String assignPickup = (String) storeHelper.assignPickupToHLP.apply(returnTrackingNumber, code1, tenantId);
        Assert.assertTrue(assignPickup.contains("0/1 Shipment assigned/UnAssigned successfully"), "Return Order is assigned to new store which is already assigned to store");
    }

    @Test( enabled =false,  description = "TC ID:C28055,EOD_RECON flows: On - Hold Pickup - CC APPROVES -  should be processed after CC approves")
    public void process_PQCP_ONHoldWithCustomer_CCApprove_ReturnBackToWH_Successful() throws Exception {

        //Create Mock order Till DL
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        //getReturnTrackingNumber
        ReturnResponse LMSReturnTrackingNumber = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = LMSReturnTrackingNumber.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String assignPickupToHLP = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(assignPickupToHLP.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.UNASSIGNED);

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        Long storeTripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        //validate trip status
        statusPollingValidator.validateTripStatus(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.OUT_FOR_PICKUP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storeTripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successful in store
        TripOrderAssignmentResponse updatePickup = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE, tenantId);
        Assert.assertTrue(updatePickup.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //update QC ONHOLD
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.ONHOLD_RETURN_WITH_COURIER.toString());

        //QC onhold approve from prism
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.APPROVED);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        //process return order
        returnHelper.processReturnOrderFromDCToWareHouse(returnId,returnTrackingNumber,deliveryCenterCode,"RT-BLR");
    }

    @Test( enabled =false,  description = "TC ID:C28056, Update order as PQCP and mark as On - Hold Pickup - CC REJECTS")
    public void process_PQCP_ONHoldWithCustomer_CCReject_ReshipToCustomer_Successful() throws Exception {

        //Create Mock order Till DL
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //getReturnTrackingNumber
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        ReturnResponse LMSReturnTrackingNumber = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = LMSReturnTrackingNumber.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String assignPickupToHLP = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(assignPickupToHLP.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.UNASSIGNED);

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        Long storeTripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateTripStatus(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.OUT_FOR_PICKUP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storeTripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successful in store
        TripOrderAssignmentResponse updatePickup = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE, tenantId);
        Assert.assertTrue(updatePickup.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //update QC ON-HOLD
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.ONHOLD_RETURN_WITH_COURIER.toString());

        //QC on-hold Reject from prism
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.REJECTED);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.DEC.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.RETURN_REJECTED.toString());
    }

    @Test(enabled = true, description = "TC ID: C28086, Add shipment which is delivered to customer")
    public void process_ScanDeliveredShipmentIntoStoreBag_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        //Create MasterBag
        ShipmentResponse createStoreBag = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(createStoreBag.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = createStoreBag.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String addShipmentToStoreBag = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (addShipmentToStoreBag.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus4 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus4.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse createDCTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(createDCTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = createDCTrip.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(createDCTrip.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(createDCTrip, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse6 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse6.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus6 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus6.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse8 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse deliverStoreBagToStore = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(deliverStoreBagToStore.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(deliverStoreBagToStore.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(deliverStoreBagToStore.getStatus().getTotalCount() == 1);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Complete DC trip
        TripOrderAssignmentResponse closeMyntraTripWithStoreBag = tripClient_qa.closeMyntraTripWithStoreBag(tenantId, tripId);
        Assert.assertTrue(closeMyntraTripWithStoreBag.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());

        //Assign order to store SDA
        TripResponse createStoreTrip = mlShipmentClientV2_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(createStoreTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successful");
        //Validate Myntra SDA trip is OFD
        Long storeTripId = createStoreTrip.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Mark it as Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment2 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId3 = tripOrderAssignment2.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId3));
        MLShipmentResponse mlShipmentResponse45 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId2 = String.valueOf(mlShipmentResponse45.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId2);
        TripOrderAssignmentResponse tripOrderAssignmentResponse9 = tripClient_qa.deliverStoreOrders(storeTripId, mlShipmentEntriesId2, tripOrderAssignmentId3, storeTenantId,"CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse9.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.DELIVERED.toString());

        //Create MasterBag and try to add delivered shipment to store
        String originDCCity1 = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode1 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity1 = searchStoreByCode1.getStoreEntries().get(0).getCity();
        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity1, destinationStoreCity1);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        Assert.assertEquals(response, EnumSCM.ERROR, "The Shipment which has Delivered state is added into Store Bag");
    }

    @Test(enabled = true, description = "TC ID: C28087, Add shipment which is outForDelivery for another store trip")
    public void process_ScanOFDShipmentIntoAnotherStoreBag_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        //Create MasterBag
        ShipmentResponse createStoreBag = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(createStoreBag.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = createStoreBag.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String addShipmentToStoreBag = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (addShipmentToStoreBag.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus4 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus4.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse createDCTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(createDCTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = createDCTrip.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(createDCTrip.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(createDCTrip, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse6 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse6.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus6 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus6.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse8 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse deliverStoreBagToStore = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(deliverStoreBagToStore.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(deliverStoreBagToStore.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(deliverStoreBagToStore.getStatus().getTotalCount() == 1);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Complete DC trip
        TripOrderAssignmentResponse closeMyntraTripWithStoreBag = tripClient_qa.closeMyntraTripWithStoreBag(tenantId, tripId);
        Assert.assertTrue(closeMyntraTripWithStoreBag.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());

        //Assign order to store SDA
        TripResponse createStoreTrip = mlShipmentClientV2_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(createStoreTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successful");
        //Validate Myntra SDA trip is OFD
        Long storeTripId = createStoreTrip.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //create another store bag and try to add OFD shipment
        String originDCCity1 = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode1 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams1, "");
        String destinationStoreCity1 = searchStoreByCode1.getStoreEntries().get(0).getCity();
        ShipmentResponse createStoreMB = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity1, destinationStoreCity1);
        Assert.assertTrue(createStoreMB.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long storeMasterBagId = createStoreMB.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(storeMasterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(storeMasterBagId, ShipmentStatus.NEW.toString());

        //Add shipment to MB
        ShipmentUpdateInfo addShipmentIntoMB = new ShipmentUpdateInfo.Builder(Long.toString(storeMasterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String addShipmentIntoMBResponse = masterBagClient_qa.addShipmentToStoreBag(storeMasterBagId, addShipmentIntoMB);
        Assert.assertEquals(addShipmentIntoMBResponse, EnumSCM.ERROR, "shipment added into store bag when the shipment present into same store bag trip");
    }

    @Test( enabled = true,description = "TC ID:C28057,ROLLING_RECON flows: All TRIED_NOT_BOUGHT items has not to be picked up . But this should not block store bag creation and addition of items on next day")
    public void processTriedAndNotBoughtOrdersSuccessful() throws Exception {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.HSR_ML, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(LASTMILE_CONSTANTS.EOD_PINCODE, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams2, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId2, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId2, EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentClientV2_qa.createStoreTrip(listTrackingNumbers, storeSDAId2);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId2, EnumSCM.OUT_FOR_DELIVERY);

        //update the trip order with status as Tried_And_Bought
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber2, storeTenantId2);
        Long tripOrderAssignmentId = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));
        OrderResponse orderResponse6 = mlShipmentClientV2_qa.getTryAndBuyItems(storeTenantId2, trackingNo);
        Long itemEntriesId = orderResponse6.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        storeValidator.isNull(String.valueOf(itemEntriesId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = storeHelper.updateTryAndBuyOrders(tripOrderAssignmentId, AttemptReasonCode.DELIVERED, com.myntra.lastmile.client.code.utils.TripAction.UPDATE, packetId, originPremiseId, amount,
                itemEntriesId, storeTenantId2, TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT, storeTripId);
        Assert.assertEquals(tripOrderAssignmentResponse6.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId2,EnumSCM.DELIVERED);

        //Create one more order Till SH and add it to store bag without reconcile
        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.HSR_ML, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId1 = omsServiceHelper.getPacketId(orderId);

        //validate OrderToShip status
        OrderResponse orderResponse1 = lmsClient.getOrderByOrderId(packetId1);
        Assert.assertTrue(orderResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        String trackingNo1 = orderResponse1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create MasterBag for same store and add the shipment on next day, it should allow
        ShipmentResponse createMBResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId2, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(createMBResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = createMBResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String addShipmentToStoreBagResponse = masterBagClient_qa.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        if (addShipmentToStoreBagResponse.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
    }

    @Test(enabled = true, description = "TC ID: C28058, AmountToBeCollected should be calculated for the Delivered Items of FORWARD order where payment method is CASH (not CARD_ON_DELIVERY)")
    public void process_AmountToBeCollectedShouldBeCalculatedForCASHPaymentType_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        Double amountToBeCollected = orderResponse.getOrders().get(0).getCodAmount();

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        //Create MasterBag
        ShipmentResponse createStoreBag = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(createStoreBag.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = createStoreBag.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String addShipmentToStoreBag = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (addShipmentToStoreBag.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus4 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus4.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse createDCTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(createDCTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = createDCTrip.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(createDCTrip.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(createDCTrip, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse6 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse6.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus6 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus6.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse8 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse deliverStoreBagToStore = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(deliverStoreBagToStore.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(deliverStoreBagToStore.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(deliverStoreBagToStore.getStatus().getTotalCount() == 1);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.EXPECTED_IN_DC);

        //Complete DC trip
        TripOrderAssignmentResponse closeMyntraTripWithStoreBag = tripClient_qa.closeMyntraTripWithStoreBag(tenantId, tripId);
        Assert.assertTrue(closeMyntraTripWithStoreBag.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.EXPECTED_IN_DC);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());

        //Assign order to store SDA
        TripResponse createStoreTrip = mlShipmentClientV2_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(createStoreTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successful");
        //Validate Myntra SDA trip is OFD
        Long storeTripId = createStoreTrip.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Mark it as Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment2 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId3 = tripOrderAssignment2.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId3));
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId2 = String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId2);
        TripOrderAssignmentResponse tripOrderAssignmentResponse9 = tripClient_qa.deliverStoreOrders(storeTripId, mlShipmentEntriesId2, tripOrderAssignmentId3, storeTenantId, "CASH", LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse9.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, ShipmentUpdateEvent.DELIVERED.toString());

        //get store account id
        KhataServiceHelper khataServiceHelper=new KhataServiceHelper();
        AccountResponse accountResponse = khataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId),AccountType.PARTNER);
        Assert.assertEquals(accountResponse.getStatus().getStatusMessage(),"Account Retrieved Successfully","Store account information is not retrieved ");
        Long storeAccountId = accountResponse.getAccounts().get(0).getId();
        //get amount to be paid for the store
        AccountPendencyResponse accountPendencyResponse = khataServiceHelper.getAccountPendencyByaccount(storeAccountId);
        Assert.assertEquals(accountPendencyResponse.getStatus().getStatusMessage(),"Account pendency Retrieved Successfully","Store account pendency is not retrieved ");
        //validate amount to be paid
        Assert.assertEquals(amountToBeCollected.toString(),accountPendencyResponse.getAccountPendency().get(0).getCash().toString(),"amount to be collected is not matching");

        //create reverse trip
        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();

        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);

        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,Float.parseFloat(String.valueOf(amountToBeCollected)),LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), com.myntra.lastmile.client.status.TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        //get amount to be paid for the store
        AccountPendencyResponse accountPendencyResponse1 = khataServiceHelper.getAccountPendencyByaccount(storeAccountId);
        Assert.assertEquals(accountPendencyResponse1.getStatus().getStatusMessage(),"Account pendency Retrieved Successfully","Store account pendency is not retrieved ");
        //validate amount after collected from store
        Assert.assertEquals(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString(),"0.0","amount to be collected is not matching");

        //get SDA Account number
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long sdaAccountId = accountResponse1.getAccounts().get(0).getId();
        PaymentResponse response =  khataServiceHelper.createAndApprove(originPremiseId, sdaAccountId, Float.parseFloat(String.valueOf(amountToBeCollected)), PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + sdaAccountId);

    }

    @Test(enabled = true, description = "TC ID: C28059, Verify Previous day's shortage amount")
    public void process_checkPreviousDayShortageAmount_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        Double amountToBeCollected = orderResponse.getOrders().get(0).getCodAmount();

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        //Create MasterBag
        ShipmentResponse createStoreBag = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(createStoreBag.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = createStoreBag.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String addShipmentToStoreBag = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (addShipmentToStoreBag.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus4 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus4.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse createDCTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(createDCTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = createDCTrip.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(createDCTrip.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(createDCTrip, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse6 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse6.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus6 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus6.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse8 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse deliverStoreBagToStore = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(deliverStoreBagToStore.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(deliverStoreBagToStore.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(deliverStoreBagToStore.getStatus().getTotalCount() == 1);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.EXPECTED_IN_DC);

        //Complete DC trip
        TripOrderAssignmentResponse closeMyntraTripWithStoreBag = tripClient_qa.closeMyntraTripWithStoreBag(tenantId, tripId);
        Assert.assertTrue(closeMyntraTripWithStoreBag.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.EXPECTED_IN_DC);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());

        //Assign order to store SDA
        TripResponse createStoreTrip = mlShipmentClientV2_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(createStoreTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successful");
        //Validate Myntra SDA trip is OFD
        Long storeTripId = createStoreTrip.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Mark it as Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment2 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId3 = tripOrderAssignment2.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId3));
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId2 = String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId2);
        TripOrderAssignmentResponse tripOrderAssignmentResponse9 = tripClient_qa.deliverStoreOrders(storeTripId, mlShipmentEntriesId2, tripOrderAssignmentId3, storeTenantId, "CASH", LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse9.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, ShipmentUpdateEvent.DELIVERED.toString());

        //get store account id
        KhataServiceHelper khataServiceHelper=new KhataServiceHelper();
        AccountResponse accountResponse = khataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId),AccountType.PARTNER);
        Assert.assertEquals(accountResponse.getStatus().getStatusMessage(),"Account Retrieved Successfully","Store account information is not retrieved ");
        Long storeAccountId = accountResponse.getAccounts().get(0).getId();
        //get amount to be paid for the store
        AccountPendencyResponse accountPendencyResponse = khataServiceHelper.getAccountPendencyByaccount(storeAccountId);
        Assert.assertEquals(accountPendencyResponse.getStatus().getStatusMessage(),"Account pendency Retrieved Successfully","Store account pendency is not retrieved ");
        //validate amount to be paid
        Assert.assertEquals(amountToBeCollected.toString(),accountPendencyResponse.getAccountPendency().get(0).getCash().toString(),"amount to be collected is not matching");

        //create reverse trip
        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();

        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);

        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,500.0f,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), com.myntra.lastmile.client.status.TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        //get amount to be paid for the store
        Thread.sleep(2000);
        AccountPendencyResponse accountPendencyResponse1 = khataServiceHelper.getAccountPendencyByaccount(storeAccountId);
        Assert.assertEquals(accountPendencyResponse1.getStatus().getStatusMessage(),"Account pendency Retrieved Successfully","Store account pendency is not retrieved ");
        //validate amount after collected from store
        double shortageAmount=amountToBeCollected-500;
        Assert.assertEquals(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString(),String.valueOf(shortageAmount),"Shortage amount is correct , " +
                "collected from store is 500.0, but its not updates correctly");

        //get SDA Account number
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long sdaAccountId = accountResponse1.getAccounts().get(0).getId();
        PaymentResponse response =  khataServiceHelper.createAndApprove(originPremiseId, sdaAccountId, Float.parseFloat(String.valueOf(amountToBeCollected)), PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + sdaAccountId);
    }

    @Test(enabled = true, description = "TC ID: C28060, Verify Previous day's Excess amount")
    public void process_checkPreviousDayExcessAmount_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        Double amountToBeCollected = orderResponse.getOrders().get(0).getCodAmount();

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse searchStoreByCode = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = searchStoreByCode.getStoreEntries().get(0).getCity();
        //Create MasterBag
        ShipmentResponse createStoreBag = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(createStoreBag.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = createStoreBag.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String addShipmentToStoreBag = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (addShipmentToStoreBag.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus4 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus4.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse createDCTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(createDCTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = createDCTrip.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(createDCTrip.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(createDCTrip, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);
        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse6 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse6.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus6 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus6.name().equals(OrderShipmentAssociationStatus.NEW.name()));
        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse8 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse deliverStoreBagToStore = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(deliverStoreBagToStore.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(deliverStoreBagToStore.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(deliverStoreBagToStore.getStatus().getTotalCount() == 1);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.EXPECTED_IN_DC);

        //Complete DC trip
        TripOrderAssignmentResponse closeMyntraTripWithStoreBag = tripClient_qa.closeMyntraTripWithStoreBag(tenantId, tripId);
        Assert.assertTrue(closeMyntraTripWithStoreBag.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.EXPECTED_IN_DC);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());

        //Assign order to store SDA
        TripResponse createStoreTrip = mlShipmentClientV2_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(createStoreTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successful");
        //Validate Myntra SDA trip is OFD
        Long storeTripId = createStoreTrip.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Mark it as Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment2 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId3 = tripOrderAssignment2.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId3));
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId2 = String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId2);
        TripOrderAssignmentResponse tripOrderAssignmentResponse9 = tripClient_qa.deliverStoreOrders(storeTripId, mlShipmentEntriesId2, tripOrderAssignmentId3, storeTenantId, "CASH", LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse9.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, ShipmentUpdateEvent.DELIVERED.toString());

        //get store account id
        KhataServiceHelper khataServiceHelper=new KhataServiceHelper();
        AccountResponse accountResponse = khataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId),AccountType.PARTNER);
        Assert.assertEquals(accountResponse.getStatus().getStatusMessage(),"Account Retrieved Successfully","Store account information is not retrieved ");
        Long storeAccountId = accountResponse.getAccounts().get(0).getId();
        //get amount to be paid for the store
        AccountPendencyResponse accountPendencyResponse = khataServiceHelper.getAccountPendencyByaccount(storeAccountId);
        Assert.assertEquals(accountPendencyResponse.getStatus().getStatusMessage(),"Account pendency Retrieved Successfully","Store account pendency is not retrieved ");
        //validate amount to be paid
        Assert.assertEquals(amountToBeCollected.toString(),accountPendencyResponse.getAccountPendency().get(0).getCash().toString(),"amount to be collected is not matching");

        //create reverse trip
        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();

        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);

        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,2000.0f,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), com.myntra.lastmile.client.status.TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        //get amount to be paid for the store
        Thread.sleep(2000);
        AccountPendencyResponse accountPendencyResponse1 = khataServiceHelper.getAccountPendencyByaccount(storeAccountId);
        Assert.assertEquals(accountPendencyResponse1.getStatus().getStatusMessage(),"Account pendency Retrieved Successfully","Store account pendency is not retrieved ");
        //validate amount after collected from store
        //TODO check excess amount here
        double excessAmount = (2000.0)-amountToBeCollected;
        Assert.assertEquals(String.valueOf(accountPendencyResponse1.getAccountPendency().get(0).getExcess()),String.valueOf(excessAmount),"excess amount is not matching");
        Assert.assertEquals(String.valueOf(accountPendencyResponse1.getAccountPendency().get(0).getCash()),"0.0","cash collected from store is not expected ");

        //get SDA Account number
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long sdaAccountId = accountResponse1.getAccounts().get(0).getId();
        PaymentResponse response =  khataServiceHelper.createAndApprove(originPremiseId, sdaAccountId, Float.parseFloat(String.valueOf(amountToBeCollected)), PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + sdaAccountId);

    }

}
