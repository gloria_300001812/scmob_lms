package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.erpservices.lastmile.client.StoreClient;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_StoreTestsDP;
import com.myntra.apiTests.erpservices.lastmile.service.StoreClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.lastmile.client.code.utils.ReconType;

import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.StoreEntry;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.status.StoreType;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;


import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class Lastmile_StoreTests {

    StoreClient storeClient;
    StoreClient_QA storeClient_qa=new StoreClient_QA();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);

    }

    @Test(groups = {"Sanity",
    }, priority = 0, dataProviderClass = Lastmile_StoreTestsDP.class, dataProvider = "createStore", description = " Create a Store and attach it to a DC")
    public void createStore(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                            String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) {

        StoreEntry storeEntry = new StoreEntry();
        storeEntry.setName(name);
        storeEntry.setOwnerFirstName(ownerFirstName);
        storeEntry.setOwnerLastName(ownerLastName);
        storeEntry.setLatLong(latLong);
        storeEntry.setEmailId(emailId);
        storeEntry.setContactNumber(mobileNumber);
        storeEntry.setAddress(address);
        storeEntry.setPincode(pincode);
        storeEntry.setCity(city);
        storeEntry.setState(state);
        storeEntry.setMappedDcCode(mappedDcCode);
        storeEntry.setGstin(gstin);
        storeEntry.setTenantId(tenantId);
        storeEntry.setIsAvailable(isAvailable);
        storeEntry.setIsDeleted(isDeleted);
        storeEntry.setIsCardEnabled(isCardEnabled);
        storeEntry.setRating(rating);
        storeEntry.setStoreType(storeType);
        storeEntry.setReconType(hlpReconType);
        storeEntry.setCapacity(capacity);
        storeEntry.setCode(code);

        StoreResponse storeResponse = storeClient_qa.createStore(storeEntry);

        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getCode().equals(code));
        Assert.assertNotNull(storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId());
        String searchParams = "code.like:" + code;
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Assert.assertTrue(storeResponse2.getStoreEntries().get(0).getContactNumber().equals(mobileNumber));
        StoreResponse storeResponse3 = storeClient_qa.findStoreById(storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId());
        Assert.assertTrue(storeResponse3.getStoreEntries().get(0).getCode().equals(code));
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(hlpReconType.name()), "The Store Type is NOT matching");
    }

    //TODO : vinodhi create store with     private Date hlpReconTypeLastChangedOn;, then update
    //TODO : validate the store Type
    //TODO create Store with both EOD and ROLLING

    @Test(groups = {"Sanity",
    }, priority = 0, dataProviderClass = Lastmile_StoreTestsDP.class, dataProvider = "updateStore", description = " Create a Store and attach it to a DC")
    public void updateStore(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                            String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code, ReconType updatehlpReconType) {
        StoreEntry storeEntry = new StoreEntry();
        storeEntry.setName(name);
        storeEntry.setOwnerFirstName(ownerFirstName);
        storeEntry.setOwnerLastName(ownerLastName);
        storeEntry.setLatLong(latLong);
        storeEntry.setEmailId(emailId);
        storeEntry.setContactNumber(mobileNumber);
        storeEntry.setAddress(address);
        storeEntry.setPincode(pincode);
        storeEntry.setCity(city);
        storeEntry.setState(state);
        storeEntry.setMappedDcCode(mappedDcCode);
        storeEntry.setGstin(gstin);
        storeEntry.setTenantId(tenantId);
        storeEntry.setIsAvailable(isAvailable);
        storeEntry.setIsDeleted(isDeleted);
        storeEntry.setIsCardEnabled(isCardEnabled);
        storeEntry.setRating(rating);
        storeEntry.setStoreType(storeType);
        storeEntry.setReconType(hlpReconType);
        storeEntry.setCapacity(capacity);
        storeEntry.setCode(code);
        StoreResponse storeResponse = storeClient_qa.createStore(storeEntry);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getCode().equals(code));
        long HlpDCId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String TenantId = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry().getTenantId();
        Assert.assertNotNull(HlpDCId);
        storeEntry.setName(name);
        storeEntry.setAddress(address);
        storeEntry.setPincode(pincode);
        storeEntry.setCity(city);
        storeEntry.setState(state);
        storeEntry.setTenantId(TenantId);
        storeEntry.setIsAvailable(isAvailable);
        storeEntry.setIsDeleted(isDeleted);
        storeEntry.setIsCardEnabled(isCardEnabled);
        storeEntry.setReconType(updatehlpReconType);
        storeEntry.setCode(code);
        StoreResponse storeResponse1 = storeClient_qa.updateStore(storeEntry, HlpDCId);
        Assert.assertTrue(storeResponse1.getStoreEntries().get(0).getCode().equals(code));
    }
}
