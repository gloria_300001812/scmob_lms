package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileTripPlanningSearchHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.DB.IQueries;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.ShipmentStatus;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lordoftherings.boromir.DBUtilities;
import lombok.SneakyThrows;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.MessageFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class LastmileSearch_TripPlanningPage implements StatusPoller {


    @Test(enabled = true, description = "TC IC - C28026 - Search Trip planning page combination of Delivery center id ,Delivery staff id , validate in DB it should have same records")
    @SneakyThrows
    public void testTripPlanningPageSearchBasedOnDCIDSDAID() {
        TripResponse apiResponse = lastmileTripPlanningSearchHelper.tripPlanningSerchBasedOnFilterSearch(
                MessageFormat.format(LASTMILE_CONSTANTS.Trip_Planning_End_URL.DCID_SDAID_END_URL, LASTMILE_CONSTANTS.ML_BLR_DCID, "5522", LMS_CONSTANTS.TENANTID));
        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.DCID_SDAID_QUERY, "5", "5522"));
        lastmileTripPlanningSearchHelper.compareAPIDBValues(apiResponse, dbQuery);
    }

    @Test(enabled = true, description = "TC IC - C28027 - Search Trip planning page combination of Delivery centre id ,TripStatus as OFD , validate in DB it should have same records")
    @SneakyThrows
    public void testTripPlanningPageSearchBasedOnDCIDTripStatusAsOFD() {
        TripResponse apiResponse = lastmileTripPlanningSearchHelper.tripPlanningSerchBasedOnFilterSearch(MessageFormat.format(LASTMILE_CONSTANTS.Trip_Planning_End_URL.DCID_TRIPSTATUS_ASOFD_END_URL,
                LASTMILE_CONSTANTS.ML_BLR_DCID, "false", EnumSCM.OUT_FOR_DELIVERY, LMS_CONSTANTS.TENANTID));
        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.DCID_TRIPSTATUS_ASOFD_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID, EnumSCM.OUT_FOR_DELIVERY));
        lastmileTripPlanningSearchHelper.compareAPIDBValues(apiResponse, dbQuery);
    }

    @Test(enabled = true, description = "TC IC - C28029 - Search Trip planning page combination of Delivery centre id ,TripStatus as CREATED , validate in DB it should have same records")
    @SneakyThrows
    public void testTripPlanningPageSearchBasedOnDCIDTripStatusAsCREATED() {
        TripResponse apiResponse = lastmileTripPlanningSearchHelper.tripPlanningSerchBasedOnFilterSearch(MessageFormat.format(LASTMILE_CONSTANTS.Trip_Planning_End_URL.DCID_TRIPSTATUS_AS_CREATED_END_URL,
                LASTMILE_CONSTANTS.ML_BLR_DCID, "false", EnumSCM.CREATED, LMS_CONSTANTS.TENANTID));
        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.DCID_TRIPSTATUS_AS_CREATED_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID, EnumSCM.CREATED));
        lastmileTripPlanningSearchHelper.compareAPIDBValues(apiResponse, dbQuery);
    }

    @Test(enabled = true, description = "TC IC - C28030 - Search Trip planning page combination of Delivery centre id ,TripStatus as COMPLETED , validate in DB it should have same records")
    @SneakyThrows
    public void testTripPlanningPageSearchBasedOnDCIDTripStatusAsCOMPETED() {
        TripResponse apiResponse = lastmileTripPlanningSearchHelper.tripPlanningSerchBasedOnFilterSearch(MessageFormat.format(LASTMILE_CONSTANTS.Trip_Planning_End_URL.DCID_TRIPSTATUS_AS_COMPLETED_END_URL,
                LASTMILE_CONSTANTS.ML_BLR_DCID, EnumSCM.COMPLETED));
        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.DCID_TRIPSTATUS_AS_COMPLETED_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID, EnumSCM.COMPLETED));
        lastmileTripPlanningSearchHelper.compareAPIDBValues(apiResponse, dbQuery);
    }

    @Test(enabled = true, description = "TC IC - C28031 - Search Trip planning page combination of DC id and Show delivery staff true and is card enabled both, validate in DB it should have same records")
    @SneakyThrows
    public void testTripPlanningPageSearchBasedOnDCIDSHOWDELIVERYSTAFFTRUEISDELETED_BOTH() {
        TripResponse apiResponse = lastmileTripPlanningSearchHelper.tripPlanningSerchBasedOnFilterSearch(MessageFormat.format(LASTMILE_CONSTANTS.Trip_Planning_End_URL.DCID_SHOW_DELIVERY_STAFF_TRUE_IS_CARD_ENABLED_BOTH_END_URL,
                LASTMILE_CONSTANTS.ML_BLR_DCID, "true", "false", "false"));
        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.DCID_SHOW_DELIVERY_STAFF_TRUE_IS_CARD_ENABLED_BOTH_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID));
        lastmileTripPlanningSearchHelper.compareAPIDBValues(apiResponse, dbQuery);
    }

    @Test(enabled = true, description = "TC IC - C28032 - Search Trip planning page of DC id ,show_delivery_staff as true, is_deleted as NO combinations , validate in DB it should have same records")
    @SneakyThrows
    public void testTripPlanningPageSearchBasedOnDCIDSHOWDELIVERYSTAFFTRUEISDELETEDNO() {
        TripResponse apiResponse = lastmileTripPlanningSearchHelper.tripPlanningSerchBasedOnFilterSearch(MessageFormat.format(LASTMILE_CONSTANTS.Trip_Planning_End_URL.DCID_SHOW_DELIVERY_STAFF_TRUE_IS_DELETED_NO_END_URL,
                LASTMILE_CONSTANTS.ML_BLR_DCID, "true", "false"));
        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.DCID_SHOW_DELIVERY_STAFF_TRUE_IS_DELETED_NO_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID, "0"));
        lastmileTripPlanningSearchHelper.compareAPIDBValues(apiResponse, dbQuery);
    }

    @Test(enabled = true, description = "TC IC - C29101 - Search Trip planning page of DC id ,show_delivery_staff as true, is_deleted as YES combinations , validate in DB it should have same records")
    @SneakyThrows
    public void testTripPlanningPageSearchBasedOnDCIDSHOWDELIVERYSTAFFTRUEISDELETEDYES() {
        TripResponse apiResponse = lastmileTripPlanningSearchHelper.tripPlanningSerchBasedOnFilterSearch(MessageFormat.format(LASTMILE_CONSTANTS.Trip_Planning_End_URL.DCID_SHOW_DELIVERY_STAFF_TRUE_IS_DELETED_YES_END_URL,
                LASTMILE_CONSTANTS.ML_BLR_DCID, "true", "true", LMS_CONSTANTS.TENANTID));
        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.DCID_SHOW_DELIVERY_STAFF_TRUE_IS_DELETED_YES_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID, "1"));
        lastmileTripPlanningSearchHelper.compareAPIDBValues(apiResponse, dbQuery);
    }

    @Test(enabled = true, description = "TC IC - C29102 - Search Trip planning page of DC id ,CREATED_ON date ,TRIP_START date and TRIP_END date, validate in DB it should have same records")
    @SneakyThrows
    public void testTripPlanningPageSearchBasedOnDCIDCREATEDONTRIPSTARTTRIPENDDATES() {
        TripResponse apiResponse = lastmileTripPlanningSearchHelper.tripPlanningSerchBasedOnFilterSearch(IQueries.formatQuery(LASTMILE_CONSTANTS.Trip_Planning_End_URL.DCID_CREATED_ON_DATE_TRIP_START_TRIP_END_DATE_END_URL,
                LASTMILE_CONSTANTS.ML_BLR_DCID, "true", EnumSCM.COMPLETED, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -27).replace(" ", "%20"), DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -27).replace(" ", "%20"), DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -27).replace(" ", "%20"), DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1).replace(" ", "%20")));

        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.DCID_CREATED_ON_DATE_TRIP_START_TRIP_END_DATE_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -27), DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -27), DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -27), DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1), EnumSCM.COMPLETED));
        lastmileTripPlanningSearchHelper.compareAPIDBValues(apiResponse, dbQuery);
    }

    @Test(enabled = true, description = "TC ID :C29103 - Search Trip planning page based on DC ID, Show_All_delivers_staff as true, Trip status,  trip number, Is_Deleted as False, Is_inbound as false, Validate with DB data that should be match ")
    @SneakyThrows
    public void testTripPlanningPageSearchBasedOnAllFields() {
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(Long.parseLong(LASTMILE_CONSTANTS.ML_BLR_DCID), LMS_CONSTANTS.TENANTID, 0, 20, "code", "DESC");
        Long deliveryStaffId = deliveryStaffResponse.getDeliveryStaffs().get(new Random().nextInt(deliveryStaffResponse.getDeliveryStaffs().size())).getId();
        TripResponse tripResponseData = lmsServiceHelper.createTrip(Long.parseLong(LASTMILE_CONSTANTS.ML_BLR_DCID), deliveryStaffId);
        String tripNumber = tripResponseData.getTrips().get(0).getTripNumber();
        String dcAndDeliveryStaff = MessageFormat.format(LASTMILE_CONSTANTS.Trip_Planning_End_URL.SEARCH_WITH_ALLFIELDS_END_URL,
                LASTMILE_CONSTANTS.ML_BLR_DCID, "true", tripNumber, EnumSCM.CREATED, "false", "false", LMS_CONSTANTS.TENANTID);

        TripResponse tripResponse = lastmileTripPlanningSearchHelper.tripPlanningSerchBasedOnFilterSearch(dcAndDeliveryStaff);
        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.SEARCH_WITH_ALLFIELDS_QUERY, tripNumber));
        lastmileTripPlanningSearchHelper.compareAPIDBValues(tripResponse, dbQuery);
    }

    @Test(enabled = true, description = "TC ID :C28028 - TKT NO:-2148  Trip Planning Restrict Only 30 Days Default Loading and giveTrip Planning Restrict date as 31 Days and check its allowing")
    @SneakyThrows
    public void testTripPlanningPageSearchBasedOnCreateOnDateLimits() {

        //give createdOn date range as 30 days from today for all incompleteOrders
        OrderResponse dateRangeAs30Days = lastmileServiceHelper.getAllIncompleteOrdersForDC(Long.parseLong(LASTMILE_CONSTANTS.ML_BLR_DCID), ShipmentType.DL, 0, 20,
                "zipcode", "DESC", LMS_CONSTANTS.TENANTID, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -30).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0).replace(" ", "%20"));
        Assert.assertEquals(dateRangeAs30Days.getStatus().getStatusMessage(), "Success", "data is not retrieved successfully");

        //give createdOn date range as 31 days from today for all incompleteOrders
        OrderResponse dateRangeAs31Days = lastmileServiceHelper.getAllIncompleteOrdersForDC(Long.parseLong(LASTMILE_CONSTANTS.ML_BLR_DCID), ShipmentType.DL, 0, 20,
                "zipcode", "DESC", LMS_CONSTANTS.TENANTID, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -31).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0).replace(" ", "%20"));
        Assert.assertEquals(dateRangeAs31Days.getStatus().getStatusMessage(), "Please provide the valid date range.", "data is retrieved successfully");
        Assert.assertTrue(dateRangeAs31Days.getStatus().getTotalCount() == 0, "data retrieved successfully when the date range is given as 31 days");

        //give createdOn date range as 30 days from today for all incompleteOrders
        OrderResponse dateRangeAs30DaysForScheduleShipment = lastmileServiceHelper.getAllIncompleteScheduledShipmentsForDC(Long.parseLong(LASTMILE_CONSTANTS.ML_BLR_DCID), ShipmentType.DL, 0, 20,
                "zipcode", "DESC", LMS_CONSTANTS.TENANTID, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -30).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0).replace(" ", "%20"));
        Assert.assertEquals(dateRangeAs30DaysForScheduleShipment.getStatus().getStatusMessage(), "Success", "data is not retrieved successfully");

        //give createdOn date range as 31 days from today for all incompleteOrders
        OrderResponse dateRangeAs31DaysForScheduleShipment = lastmileServiceHelper.getAllIncompleteScheduledShipmentsForDC(Long.parseLong(LASTMILE_CONSTANTS.ML_BLR_DCID), ShipmentType.DL, 0, 20,
                "zipcode", "DESC", LMS_CONSTANTS.TENANTID, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -31).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0).replace(" ", "%20"));
        Assert.assertEquals(dateRangeAs31DaysForScheduleShipment.getStatus().getStatusMessage(), "Please provide the valid date range.", "data is retrieved successfully");
        Assert.assertTrue(dateRangeAs31DaysForScheduleShipment.getStatus().getTotalCount() == 0, "data retrieved successfully when the date range is given as 31 days");
    }

    @Test(enabled = true, description = "TC ID :C28334 - Hit refresh of all tabs and validate the records with DB")
    @SneakyThrows
    public void testTripPlanningPageRefreshFunctionality() {
        String dcId = "5";
        //refresh for Deliveries orders
        OrderResponse orderResponseDL = lastmileServiceHelper.getAllIncompleteOrdersForDC(Long.parseLong(dcId), ShipmentType.DL, 0,
                20, "lastModifiedOn", LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());

        List<Map<String, Object>> dbQueryForDL = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.REFRESH_DL_TnB_ORDERS, ShipmentType.DL.toString(), EnumSCM.UNASSIGNED,
                LMS_CONSTANTS.TENANTID, dcId));
        lastmileTripPlanningSearchHelper.validateRefreshAPIResponseAndDBValues(orderResponseDL, dbQueryForDL);

        //refresh for TnB orders
        OrderResponse orderResponseForTnB = lastmileServiceHelper.getAllIncompleteOrdersForDC(Long.parseLong(dcId), ShipmentType.TRY_AND_BUY, 0,
                20, "lastModifiedOn", LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());

        List<Map<String, Object>> dbQueryForTnB = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.REFRESH_DL_TnB_ORDERS, ShipmentType.TRY_AND_BUY.toString(), EnumSCM.UNASSIGNED,
                LMS_CONSTANTS.TENANTID, dcId));
        lastmileTripPlanningSearchHelper.validateRefreshAPIResponseAndDBValues(orderResponseForTnB, dbQueryForTnB);

        //refresh api for Exchange order
        OrderResponse orderResponseForExchangeOrder = lastmileServiceHelper.getAllIncompleteExchangesForDC(Long.parseLong(dcId), 0, 20, "id", LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());

        List<Map<String, Object>> dbQueryForExchange = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.REFRESH_EX_ORDERS, dcId, EnumSCM.UNASSIGNED,
                ShipmentType.EXCHANGE.toString(), LMS_CONSTANTS.TENANTID));
        Assert.assertEquals(String.valueOf(orderResponseForExchangeOrder.getStatus().getTotalCount()), String.valueOf(dbQueryForExchange.size()), "total count is not matching from api response and db records");
        lastmileTripPlanningSearchHelper.validateRefreshAPIResponseAndDBValues(orderResponseForExchangeOrder,dbQueryForExchange);

        //refresh api's for PU order
        OrderResponse orderResponseForPU = lastmileServiceHelper.getAllIncompleteOrdersForDC(Long.parseLong(dcId), ShipmentType.PU, 0,
                20, "lastModifiedOn", LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, LastmileHelper.GetStartDate(), LastmileHelper.GetEndDate());

        List<Map<String, Object>> dbQueryForPU = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Planning_Search.REFRESH_PU_ORDERS, ShipmentType.PU.toString(), EnumSCM.UNASSIGNED,
                ShipmentStatus.RETURN_CREATED.toString(),dcId));
        lastmileTripPlanningSearchHelper.validateRefreshAPIResponseAndDBValues(orderResponseForPU, dbQueryForPU);

    }




}
