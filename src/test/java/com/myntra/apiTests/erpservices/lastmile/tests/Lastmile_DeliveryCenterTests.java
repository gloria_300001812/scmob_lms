package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.erpservices.Redis.RedisDeleteRequest;
import com.myntra.apiTests.erpservices.Redis.RedisService;
import com.myntra.apiTests.erpservices.lastmile.client.DeliveryCenterClient;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_DeliveryCenterTestsDP;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.DeliveryCenterClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.lastmile.client.code.utils.DeliveryCenterRegion;
import com.myntra.lastmile.client.code.utils.DeliveryCenterType;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.DeliveryCenterEntry;

import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;


import java.util.ArrayList;
import java.util.List;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class Lastmile_DeliveryCenterTests {

    LastmileHelper lastmileHelper;
    DeliveryCenterClient deliveryCenterClient;
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        deliveryCenterClient = new DeliveryCenterClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);

    }
//


    @Test(groups = {"Sanity",
    }, priority = 0, dataProviderClass = Lastmile_DeliveryCenterTestsDP.class, dataProvider = "createDC", description = " Add delivery centres with diff combinations")
    public void createRollingReconDC(DeliveryCenterType type, String code, String name, String courier, Long store, String manager, String contactNumber, String address, String city,
                                     String cityCode, String state, String pincode, Integer masterbagCapacity, String transportHub, String registeredAddressLine1, String registeredAddressLine2,
                                     String registeredCity, String registeredPincode, String registeredStateName, DeliveryCenterRegion deliveryCenterRegion, String registeredCompanyName, String companyIdentificationNumber,
                                     Boolean selfShipSupported, Boolean active, Boolean bulkInvoiceEnabled, Boolean isCardEnabled, Boolean isStrictServiceable, ReconType hlpReconType) {
        RedisDeleteRequest redisDeleteRequest = new RedisDeleteRequest();
        List<String> keys = new ArrayList<>();
        keys.add("deliverCenterSearchCacheV1*");

        redisDeleteRequest.setHost("52.172.27.253");
        redisDeleteRequest.setPort(6379);
        redisDeleteRequest.setKeys(keys);
        RedisService redisService = new RedisService();
        redisService.deleteRedisMutiKeys(redisDeleteRequest);
        // lastmileHelper.flushRedisForDC.call();

        //Search existing DC and delete it
        String searchParams = "pincode.like:" + pincode;
        DeliveryCenterResponse dcResponse = deliveryCenterClient_qa.searchDCByParam(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        //without client:
        // DeliveryCenterResponse dcResponse = deliveryCenterClient.searchWithoutClient(pincode);
        String DC_Code = null;
        try {
            DC_Code = dcResponse.getDeliveryCenters().get(0).getCode();
        } catch (NullPointerException e) {
            System.out.println("There is no DC existing for the pincode " + pincode);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("There is no DC existing for the pincode " + pincode);
        }
        System.out.println("The existing DC code for the pincode- " + pincode + " is " + DC_Code);

        //Delete this DC and its staffs and its trips
        lastmileHelper.deleteTripsForDC(pincode);
        lastmileHelper.deleteDCStaffForPincode(pincode);
        lastmileHelper.deleteDC(pincode);


        //Flush Redis
        //TODO : get the keys
        //  lastmileHelper.flushRedisForDC.call();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DeliveryCenterResponse dcResponseAfterDel = deliveryCenterClient_qa.searchDCByParam(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String DC_CodeAfterDel = null;
        try {
            DC_CodeAfterDel = dcResponseAfterDel.getDeliveryCenters().get(0).getCode();
        } catch (NullPointerException e) {
            System.out.println("The existing DC code for the pincode has been deleted");

        } catch (IndexOutOfBoundsException e) {
            System.out.println("There is no DC existing for the pincode " + pincode);
        }
        if (DC_CodeAfterDel != null)
            Assert.fail("There still exists a DC  for this pincode- " + pincode + " is " + DC_CodeAfterDel);

        DeliveryCenterEntry deliveryCenterEntry = new DeliveryCenterEntry();
        deliveryCenterEntry.setType(type);
        deliveryCenterEntry.setCode(code);
        deliveryCenterEntry.setName(name);
        deliveryCenterEntry.setCourierCode(courier);
        deliveryCenterEntry.setStoreId(store);
        deliveryCenterEntry.setManager(manager);
        deliveryCenterEntry.setContactNumber(contactNumber);
        deliveryCenterEntry.setAddress(address);
        deliveryCenterEntry.setCity(city);
        deliveryCenterEntry.setCityCode(cityCode);
        deliveryCenterEntry.setState(state);
        deliveryCenterEntry.setPincode(pincode);
        deliveryCenterEntry.setMasterbagCapacity(masterbagCapacity);
        deliveryCenterEntry.setTmsHubCode(transportHub);
        deliveryCenterEntry.setRegisteredAddressLine1(registeredAddressLine1);
        deliveryCenterEntry.setRegisteredAddressLine2(registeredAddressLine2);
        deliveryCenterEntry.setRegisteredCity(registeredCity);
        deliveryCenterEntry.setRegisteredPincode(registeredPincode);
        deliveryCenterEntry.setRegisteredStateName(registeredStateName);
        deliveryCenterEntry.setDcRegion(deliveryCenterRegion);
        deliveryCenterEntry.setRegisteredCompanyName(registeredCompanyName);
        deliveryCenterEntry.setCompanyIdentificationNumber(companyIdentificationNumber);
        deliveryCenterEntry.setSelfShipSupported(selfShipSupported);
        deliveryCenterEntry.setActive(active);
        deliveryCenterEntry.setBulkInvoiceEnabled(bulkInvoiceEnabled);
        deliveryCenterEntry.setIsCardEnabled(isCardEnabled);
        deliveryCenterEntry.setIsStrictServiceable(isStrictServiceable);
        deliveryCenterEntry.setReconType(hlpReconType);
        //TODO : If this is complusory, how it creates without providing
        // deliveryCenterEntry.setReconTypeLastChangedOn(hlpReconTypeLastChangedOn);
        deliveryCenterClient_qa.createDeliveryCenter(deliveryCenterEntry);


        //Check if it is added

        DeliveryCenterResponse dcResponseAfterAdding = deliveryCenterClient_qa.searchDCByParam(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        //without client:
        // DeliveryCenterResponse dcResponse = deliveryCenterClient.searchWithoutClient(pincode);
        String dcCode = null;
        try {
            dcCode = dcResponseAfterAdding.getDeliveryCenters().get(0).getCode();
        } catch (NullPointerException e) {
            Assert.fail("DC creation failed for the pincode " + pincode);
        } catch (IndexOutOfBoundsException e) {
            Assert.fail("DC creation failed for the pincode " + pincode);
        }

        System.out.println("The DC " + dcCode + " has been created for pincode " + pincode);
        Assert.assertTrue(dcResponseAfterAdding.getDeliveryCenters().get(0).getReconType().name().equals(hlpReconType.name()), "The RECON_TYPE is not matching");
    }
    //Validate defaul DC type is EOD
}

//TODO : Add different searches , these are called by love
//TODO : Address: http://d7lastmile.myntra.com/lastmile-service/deliveryCenter/search?q=courierCode.like:ML___active.eq:true___tenantId.eq:4019&start=0&fetchSize=100000&sortBy=&sortOrder=
//TODO :Address: http://d7lastmile.myntra.com/lastmile-service/deliveryCenter/findAllStoreForDC/5


