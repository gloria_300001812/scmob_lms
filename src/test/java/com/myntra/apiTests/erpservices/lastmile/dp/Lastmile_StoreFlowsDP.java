
package com.myntra.apiTests.erpservices.lastmile.dp;

import com.myntra.apiTests.common.Utils.CommonUtils;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;

import com.myntra.lastmile.client.code.utils.ReconType;

import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lordoftherings.Toolbox;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class Lastmile_StoreFlowsDP {


    @DataProvider
    public static Object[][] eodReconAllFlows(ITestContext testContext) {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");

        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));

        Object[] arr1 = {"BA" + String.valueOf(contactNumber).substring(5, 9), "BA_FN" + String.valueOf(contactNumber).substring(5, 9), "BA_LN" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.EOD_DC_CODE,
                "LA_gstin" + String.valueOf(contactNumber).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.ROLLING_RECON, 500, "BA" + String.valueOf(contactNumber).substring(5, 9)};

        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }

    @DataProvider(name = "rollingReconAllFlows")
    public static Object[][] rollingReconAllFlows(ITestContext testContext) {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String value = CommonUtils.generateString(3);
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));

        Object[] arr1 = {value + String.valueOf(contactNumber).substring(4, 8), value + String.valueOf(contactNumber).substring(4, 8), value + String.valueOf(contactNumber).substring(4, 8),
                "LA_latlong" + String.valueOf(contactNumber).substring(4, 5), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.ROLLING_DC_CODE,
                "LA_gstin" + String.valueOf(contactNumber).substring(4, 8), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, value + String.valueOf(contactNumber).substring(4, 8)};

        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }

    @DataProvider(name = "rollingReconAllFlows2")
    public static Object[][] rollingReconAllFlows2(ITestContext testContext) {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");

        Random r = new Random(System.currentTimeMillis());
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        int contactNumber2 = Math.abs(1000000000 + r.nextInt(2000000000));

        String name1="BSH" + String.valueOf(contactNumber1).substring(4, 8);
        String name2="BSH" + String.valueOf(contactNumber2).substring(4, 8);
        String ownerFirstName1= "BSH" + String.valueOf(contactNumber1).substring(4, 8);
        String ownerFirstName2= "BSH" + String.valueOf(contactNumber2).substring(4, 8);
        String ownerLastName1= "BSH" + String.valueOf(contactNumber1).substring(4, 8);
        String ownerLastName2= "BSH" + String.valueOf(contactNumber2).substring(4, 8);
        String code1="BSH" + String.valueOf(contactNumber1).substring(4, 8);
        String code2="BSH" + String.valueOf(contactNumber2).substring(4, 8);

        Object[] arr1 = {name1, ownerFirstName1,ownerLastName1,
                "LA_latlong" + String.valueOf(contactNumber1).substring(4, 5), "test@lastmileAutomation.com", String.valueOf(contactNumber1),
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.ROLLING_DC_CODE,
                "LA_gstin" + String.valueOf(contactNumber1).substring(4, 8), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER,
                ReconType.EOD_RECON, 500, code1,name2,ownerFirstName2,ownerLastName2,String.valueOf(contactNumber2),code2};

        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }

    @DataProvider
    public static Object[][] rollingReconAllwithHSR(ITestContext testContext) {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");

        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));

        Object[] arr1 = {"ZZ" + String.valueOf(contactNumber).substring(5, 9), "ZZ" + String.valueOf(contactNumber).substring(5, 9), "ZZ" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", LASTMILE_CONSTANTS.EOD_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.EOD_DC_CODE,
                "LA_gstin" + String.valueOf(contactNumber).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 1, StoreType.MASTER, ReconType.ROLLING_RECON, 500, "ZZ" + String.valueOf(contactNumber).substring(5, 9)};

        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }



    @DataProvider
    public static Object[][] mensaEODWithoutScan(ITestContext testContext) {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");

        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));

        Object[] arr1 = {"BA" + String.valueOf(contactNumber).substring(5, 9), "BA_FN" + String.valueOf(contactNumber).substring(5, 9), "BA_LN" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.MENSA_OLD_STORE_DC_CODE,
                "LA_gstin" + String.valueOf(contactNumber).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, "BA" + String.valueOf(contactNumber).substring(5, 9)};

        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }

    @DataProvider(name = "storeCreation")
    public static Object[][] storeCreation(ITestContext testContext) {

        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));

        String value = CommonUtils.generateString(3);
        Object[] arr1 = {value + String.valueOf(contactNumber).substring(4, 8), value + String.valueOf(contactNumber).substring(4, 8), value+ String.valueOf(contactNumber).substring(4, 8),
                "LA_latlong" + String.valueOf(contactNumber).substring(4, 5), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", "Bangalore", "Karnataka",
                "LA_gstin" + String.valueOf(contactNumber).substring(4, 8), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER,  500, value + String.valueOf(contactNumber).substring(4, 8)};

        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }




}
