package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.time.LocalDate;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class Schedule_Pickup {

    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LastmileClient lastmileClient = new LastmileClient();
    TripClient_QA tripClient_qa=new TripClient_QA();

    private MLShipmentClientV2 shipmentServiceV2Client;
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    /**
     * Sample file :
     * Group Id, Facility Type, Destination, Transporter, Service Type, Shipping Method, Schedule Type, Schedule Date, Start Time, End Time, Capacity, Active, Guaranteed
     * 4,LASTMILE,560068,ML,RETURN,NORMAL,SLOT,04-06-2019,09:00:00,12:00:00,10,true,1
     * 4,LASTMILE,560068,ML,RETURN,NORMAL,SLOT,04-06-2019,20:00:00,23:00:00,10,true,1
     * @throws Exception
     */
    //TODO before running this test case upload the schedular serviceability for time space from 9:00:00 to 12:00:00
    @Test(  priority = 1,dataProviderClass = LMSTestsDP.class,enabled = true, description = " TC ID:C24691, Process Schedule pickup ")
    public void process_SchedulePickUp_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        OrderResponse orderResponse01 = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse01, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse01.getOrders().get(0).getDeliveryCenterId();

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        String ppsId=orderReleaseEntry.getOrderLines().get(0).getPaymentPpsId();
        LocalDate localDate = LocalDate.now();
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = storeHelper.createReturnWithSchedulePickup(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071",
                "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO,ppsId,
                storeHelper.convertTimetoEpoch(String.format("%sT%s+05:30",localDate, "09:00:00")),storeHelper.convertTimetoEpoch(String.format("%sT%s+05:30",localDate, "12:00:00")));
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        //manifest return order
        storeHelper.manifest(ReturnMode.OPEN_BOX_PICKUP.toString(), CourierCode.ML.toString(),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        //check manifest done
        String ismanifested=storeHelper.checkManifestDone(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(ismanifested,"true","manifest is completed successfully");

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign pickup to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId,returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Order is not assigned to Dc trip");

        //start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6=lastmileClient.startTrip(tripId.toString(),"10");
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripUpdate),"trip is not started for the DC");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripId), ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successfull
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.PICKUP_SUCCESSFUL);

    }
}
