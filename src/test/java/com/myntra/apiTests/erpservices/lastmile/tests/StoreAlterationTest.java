package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_StoreFlowsDP;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.response.CourierResponse;
import com.myntra.lms.client.status.CourierType;
import lombok.SneakyThrows;
import org.testng.Assert;
import org.testng.annotations.Test;

public class StoreAlterationTest implements StatusPoller {


    @Test(enabled = true,dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "storeCreation",description = "Create a store with alteration allowed is true ")
    public void process_CreateStore_WithAlterationAllowed_True_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String city, String state,
                                                                          String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, Integer capacity, String code){
        String searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, LASTMILE_CONSTANTS.STORE_PINCODE, city, state, LASTMILE_CONSTANTS.ROLLING_DC_CODE,
                gstin, tenantId,  isDeleted, isCardEnabled, rating, storeType, ReconType.EOD_RECON, capacity, code,true,true);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");

        StoreResponse searchStore = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Assert.assertEquals(searchStore.getStatus().getStatusMessage(),"Store retrieved","Store is not retrieved successfully");
        Assert.assertEquals(String.valueOf(searchStore.getStoreEntries().get(0).getCode()),code,"Retrieved store and created Store code value is not matching");

        //validate alteration allows true in courier table
        CourierResponse courierResponse=storeClient_qa.searchCourierDataForStore(code, CourierType.LAST_MILE_PARTNER.name());
        Assert.assertEquals(courierResponse.getStatus().getStatusMessage(),"Courier retrieved successfully","Courier is not retrieved successfully");
        Assert.assertTrue(courierResponse.getCourierEntries().get(0).getAllowsAlteration(),"The store allow alteration is not false");
    }

    @Test(enabled = true,dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "storeCreation",description = "Create a store with alteration allowed is true ")
    public void process_CreateStore_WithAlterationAllowed_False_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String city, String state,
                                                                          String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, Integer capacity, String code){
        String searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, LASTMILE_CONSTANTS.STORE_PINCODE, city, state, LASTMILE_CONSTANTS.ROLLING_DC_CODE,
                gstin, tenantId, isDeleted, isCardEnabled, rating, storeType, ReconType.EOD_RECON, capacity, code,false,true);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");

        StoreResponse searchStore = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Assert.assertEquals(searchStore.getStatus().getStatusMessage(),"Store retrieved","Store is not retrieved successfully");
        Assert.assertEquals(String.valueOf(searchStore.getStoreEntries().get(0).getCode()),code,"Retrieved store and created Store code value is not matching");

        //validate alteration allows false in courier table
        CourierResponse courierResponse=storeClient_qa.searchCourierDataForStore(code, CourierType.LAST_MILE_PARTNER.name());
        Assert.assertEquals(courierResponse.getStatus().getStatusMessage(),"Courier retrieved successfully","Courier is not retrieved successfully");
        Assert.assertFalse(courierResponse.getCourierEntries().get(0).getAllowsAlteration(),"The store allow alteration is not false");
    }

    @Test(enabled = true,dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "storeCreation",description = "Create a store with alteration allowed is true ")
    public void process_UpdateStore_AllowAlterationAsTrueAndThenFalse_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String city, String state,
                                                                          String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, Integer capacity, String code){

        boolean isActive=true;
        String searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, LASTMILE_CONSTANTS.STORE_PINCODE, city, state, LASTMILE_CONSTANTS.ROLLING_DC_CODE,
                gstin, tenantId, isDeleted, isCardEnabled, rating, storeType, ReconType.EOD_RECON, capacity, code,false,true);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        StoreResponse searchStore = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        Assert.assertEquals(searchStore.getStatus().getStatusMessage(),"Store retrieved","Store is not retrieved successfully");
        Assert.assertEquals(String.valueOf(searchStore.getStoreEntries().get(0).getCode()),code,"Retrieved store and created Store code value is not matching");

        //validate alteration allows false in courier table
        CourierResponse courierResponse=storeClient_qa.searchCourierDataForStore(code, CourierType.LAST_MILE_PARTNER.name());
        Assert.assertEquals(courierResponse.getStatus().getStatusMessage(),"Courier retrieved successfully","Courier is not retrieved successfully");
        Assert.assertFalse(courierResponse.getCourierEntries().get(0).getAllowsAlteration(),"The store allow alteration is not false");

        //update store alteration allowed as true
        StoreResponse updateStoreResponse=lastmileHelper.updateStore(name, ownerFirstName,ownerLastName,address,LASTMILE_CONSTANTS.STORE_PINCODE, city,
                isActive,isCardEnabled,true,mobileNumber,storeType,"ELC", code, storeHlPId);
        Assert.assertEquals(updateStoreResponse.getStatus().getStatusMessage(),"Store updated","Store is not updated successfully");

        //validate alteration allows True in courier table
        CourierResponse courierResponseTrue=storeClient_qa.searchCourierDataForStore(code, CourierType.LAST_MILE_PARTNER.name());
        Assert.assertEquals(courierResponseTrue.getStatus().getStatusMessage(),"Courier retrieved successfully","Courier is not retrieved successfully");
        Assert.assertTrue(courierResponseTrue.getCourierEntries().get(0).getAllowsAlteration(),"After updating the store allow alteration As allowed, in courier table value is not changing as true");

        //update store alteration allowed as false
        StoreResponse updateStoreResponse1=lastmileHelper.updateStore(name, ownerFirstName,ownerLastName,address,LASTMILE_CONSTANTS.STORE_PINCODE, city,
                isActive,isCardEnabled,false,mobileNumber,storeType,"ELC", code, storeHlPId);
        Assert.assertEquals(updateStoreResponse1.getStatus().getStatusMessage(),"Store updated","Store is not updated successfully");

        //validate alteration allows False in courier table
        CourierResponse courierResponseFalse=storeClient_qa.searchCourierDataForStore(code, CourierType.LAST_MILE_PARTNER.name());
        Assert.assertEquals(courierResponseFalse.getStatus().getStatusMessage(),"Courier retrieved successfully","Courier is not retrieved successfully");
        Assert.assertFalse(courierResponseFalse.getCourierEntries().get(0).getAllowsAlteration(),"After updating the store allow alteration As not allowed, in courier table value is not changing as false");

    }

}
