package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_StoreFlowsDP;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.DeliveryStaffType;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.OrderShipmentAssociationStatus;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import org.apache.commons.collections.ListUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class Lastmile_EODReconFlows {

    static Logger log = Logger.getLogger(Lastmile_RollingTripSheet.class);
    StoreClient storeClient;
    DeliveryCenterClient deliveryCenterClient;

    LMSOrderDetailsClient lmsOrderDetailsClient;
    MasterBagClient masterBagClient;
    TripOrderAssignmentClient tripOrderAssignmentClient;

    MLShipmentClientV2 mlShipmentServiceV2Client;
    DeliveryStaffClient deliveryStaffClient;
    private LMSHelper lmsHepler = new LMSHelper();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private LastmileHelper lastmileHelper;
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa=new DeliveryStaffClient_QA();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa=new TripOrderAssignmentClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        deliveryCenterClient = new DeliveryCenterClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        deliveryStaffClient = new DeliveryStaffClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    @Test(groups = {"Sanity",
    }, priority = 0, dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "eodReconAllFlows", description = " Create a Store and attach it to a DC then inscan the shipments and deliver it")
    public void EODRecon_AllFlows_WithScan_HSR(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                           String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws UnsupportedEncodingException, JAXBException {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        int noOfOrders = 8;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> failedDlTrackingList = new ArrayList<>();
        List<String> DlCashTrackingList = new ArrayList<>();
        List<String> UnattemptedDLTrackingList = new ArrayList<>();
        List<String> DlPrepaidTrackingList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        Map<String, Long> trackingInstakartMlIdMap = new HashMap<>();
        Map<String, Long> trackingStoreMlIdMap = new HashMap<>();
        Map<String, Float> trackingShipmentValue = new HashMap<>();
        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        Float cashAmountToBeCollected = 0.0f;
        List<String> failedUnattemptedFinalList = new ArrayList<>();

        //
        //1. Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();

        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());


        //Create a MasterBag from pincode : LMS_PINCODE.ML_BLR (order's pincode)
//TODO : GLoria check why dc id is returned wrong for
        // originPremiseId = deliveryCenterClient.getOriginPremiseIdOfDC(LASTMILE_CONSTANTS.EOD_PINCODE, tenantId);
        originPremiseId = 5l;
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //2. Create Forward Order Till SH

        for (int i = 0; i < noOfOrders; i++) {
            try {
                orderID = lmsHepler.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.EOD_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
                log.info("The orderId is : +" + orderID);
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Order creation in SH state failed : " + e.toString());
            }


            //Add the order to the MB

            String packetId = omsServiceHelper.getPacketId(orderID);
            log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
            forwardTrackingNumbersList.add(trackingNo);
            trackingNoOrderIdMap.put(trackingNo, packetId);
            log.info("Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
            System.out.println("\n_____________Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

            ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
            try {
                //TODO : Try to use addShipmentToMasterBag
                //TODO : negative cases here
                String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
                if (response.toLowerCase().contains("error")) {
                    Assert.fail("Error while adding shipment to store bag");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
            System.out.println("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

            //Verify if the order is added in Bag
            OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
            Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
            System.out.println("\n______________The status of the " + i + " orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
            log.info("_____________The status of the " + i + " order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());

        }


        //Orders are created
        failedDlTrackingList.add(forwardTrackingNumbersList.get(3));
        failedDlTrackingList.add(forwardTrackingNumbersList.get(4));
        DlCashTrackingList.add(forwardTrackingNumbersList.get(0));
        DlCashTrackingList.add(forwardTrackingNumbersList.get(1));
        DlPrepaidTrackingList.add(forwardTrackingNumbersList.get(2));
        DlPrepaidTrackingList.add(forwardTrackingNumbersList.get(5));
        UnattemptedDLTrackingList.add(forwardTrackingNumbersList.get(6));
        UnattemptedDLTrackingList.add(forwardTrackingNumbersList.get(7));


        //Creating a trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");

        //TODO : TO BE CHECKED BY DEV
      /*  Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentItems().size().equals(noOfOrders));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().name().equals(ShipmentType.FORWARD_BAG.name());
        */


        //Validate the MB status is ADDED_TO_TRIP
        //TODO : Add in validator

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, noOfOrders, code, originPremiseId);


        //TODO : to check after adding bag to trip, sipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));
        }

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        //TODO : TO BE CHECKED BY DEV
        //Validate status  of this above response is OFD
        searchParams = "tripNumber.like:" + tripNumber;
        TripResponse tripResponse1 = tripClient_qa.searchTripByParam(0, 20, "id", "ASC", Boolean.parseBoolean(null), searchParams, "");
        //TODO : to add in validation
        Assert.assertTrue(tripResponse1.getTrips().get(0).getDeliveryStaffEntry().getDeliveryStaffType().name().equals(DeliveryStaffType.MYNTRA_PAYROLL.name()));
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        lastmileHelper.validateTripStartedForMB(masterBagId);

        //Before delivering the MB validate that there is nothing in the store
        //Validate getUnattempted api in the store is empty
        List<String> forwardUnattemptedShipmentsB4DL = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        Assert.assertTrue(forwardUnattemptedShipmentsB4DL.isEmpty(), "Store has some unAttempted items before delivering the MB to the stpre itself");
        //Before delivering the bag to store, validate only one entry in ML

        //Vaidate ONLY one  ML entry created for tenant
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            // Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Saving the ml id for tenant 4019 , this will be source ref id for mlResponse with storeTenant
            trackingInstakartMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            trackingShipmentValue.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentValue());
            //Validate status of Instakart ML shipment is MLDeliveryShipmentStatus
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.name()));

        }
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            //Validate there is no ML shipment created for Store
            Assert.assertTrue(mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId).getStatus().getStatusMessage().equals("Invalid Shipment Id"));

        }

        //Deliver the MB

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);

        //Validate if shipment_order_map status is DELIVERED

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);

            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }
        //Validate getUnattempted api in the store
            List<String> forwardUnattemptedShipments = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        System.out.println("\n\n\n\n\n\n  ***************************************************** The list of forward Unattempted Shipments is         : " + forwardUnattemptedShipments + "  \n\n\n\n*****************************************************");
        Assert.assertTrue(forwardTrackingNumbersList.containsAll(forwardUnattemptedShipments), "Store does not right number of unattempted items");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Vaidate new ML entry created for tenant
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            // For 4019 no entry of getMlLastMilePartnerShipmentAssignment should return NULL
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            //   mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();

            //Validate tenant is 4019 and client is 2297
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.CLIENTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID));

            //Validate the source reference id is the orderId
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId().equals(trackingNoOrderIdMap.get(track)));

            //Validate the status for the Store Tenant ML entry as HANDED_OVER_TO_LASTMILE_PARTNER
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.name()));

            //Validate the DC id is 5

            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(originPremiseId));


        }
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.EXPECTED_IN_DC.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));

        }
        //After delivering the store bag to store , the shipment order map status should be DELIVERED
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }


        //Get delivery staff for Store by searching using dc id
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        long expectedstoreDeliveryStaffId = deliveryStaffClient_qa.findDeliveryStaffByMobNo2(mobileNumber);
        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
//TODO ------------------------ storeTrip.getTrips().get(0).getTripStatus() -> CREATED instead of OFD, sda trip : tripResponse1.getTrips().get(0).getTripStatus() is CREATED
        //Validate after creating of Store Bag it becomes OFD
        Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        //Validate Myntra SDA trip is OFD
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        //Validate the Store ML Shipment goes for OFD
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));


            //VAlidate the ORderToShip (Platform ) status is OFD
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            OrderResponse orderResponse = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(track),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.OUT_FOR_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.OUT_FOR_DELIVERY.name());

        }

        //Validate Store Trip tenant and status and code
        //TODO : Check with Azeem why storeTrip.getTrips().get(0).getTripStatus() is returning CREATED instead of OFD
        //   Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        //Get the store summary before delivering the shipments to Customer validate
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateStoreSummaryBeforeDelivering(originPremiseId, storeHlPId, forwardTrackingNumbersList, noOfOrders);

        //Now deliver forwardTrackingNumbersList.get(0) order to customer with PREPAID

        //TODO : ask the payment methods to dev
        //Get the tripOrderId for Store trip
        int count = 0;
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        for (String DlPrepaidTrack : DlPrepaidTrackingList) {
            count++;
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(DlPrepaidTrack)).findFirst().get().getId();

            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);

            //TODO :___________________Validate this trip is in OFD
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (count % 2 == 0)
                //Assert.assertTrue(tripOrderAssignment.getTripOrders().get(0).getTripOrderStatus().name().equals("Out for delivery"));
                tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(DlPrepaidTrack).toString(), tripOrderAssignmentId, storeTenantId, "PRE_PAID",LASTMILE_CONSTANTS.TENANT_ID);
            else
                tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(DlPrepaidTrack).toString(), tripOrderAssignmentId, storeTenantId, "DEBIT_CARD",LASTMILE_CONSTANTS.TENANT_ID);

        }

        //Delver the cash orders
        for (String DlCashTrack : DlCashTrackingList) {
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(DlCashTrack)).findFirst().get().getId();

            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);

            //TODO :___________________Validate this trip is in OFD
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //Assert.assertTrue(tripOrderAssignment.getTripOrders().get(0).getTripOrderStatus().name().equals("Out for delivery"));
            tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(DlCashTrack).toString(), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

            cashAmountToBeCollected = cashAmountToBeCollected + trackingShipmentValue.get(DlCashTrack);
        }
        // Validate                "totalAmountToBeCollected": 0,
       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 1,
                "totalUnAttempted": 1,*/

        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), noOfOrders, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), UnattemptedDLTrackingList.size() + failedDlTrackingList.size());

        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        //Validate Store ML shipment is Delivered

        for (String t : DlPrepaidTrackingList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));


            //VAlidate the ORderToShip (Platform ) status is OFD

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(t),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.DELIVERED), "The platform  orderToship status is not : " + ShipmentStatus.DELIVERED.name());

            lastmileHelper.validateStoreMLEntryDelivered(originPremiseId, storeHlPId, storeTenantId, t);

        }

        for (String t : DlCashTrackingList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));


            //VAlidate the ORderToShip (Platform ) status is OFD

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(t),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.DELIVERED), "The platform  orderToship status is not : " + ShipmentStatus.DELIVERED.name());

            lastmileHelper.validateStoreMLEntryDelivered(originPremiseId, storeHlPId, storeTenantId, t);
        }


        //Now fail the other  forwardTrackingNumbersList.get(1)/ failedDlTrackingList.get(0)
        //Get the tripOrderAssignment id for the store trip
        tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);

        for (String FDTrack : failedDlTrackingList) {
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(FDTrack)).findFirst().get().getId();
            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);

            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(FDTrack).toString(), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        }
       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 2,
                "totalUnAttempted": 0,   "totalAmountToBeCollected": 0,
*/

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, DlCashTrackingList.size() + DlPrepaidTrackingList.size() + failedDlTrackingList.size(), noOfOrders, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), UnattemptedDLTrackingList.size());
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);

        for (String FDTrack : failedDlTrackingList) {

            lastmileHelper.validateStoreMLEntryFailedDeliveredb4CloseTrip(originPremiseId, storeHlPId, storeTenantId, FDTrack);
        }
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);

        for (String FDTrackNo : failedDlTrackingList) {

            //Validate Store ML shipment is Failed Delivered
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //VAlidate the ORderToShip (Platform ) status is FAILED_DELIVERY

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(FDTrackNo),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.FAILED_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.FAILED_DELIVERY.name());
        }
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Close the Myntra SDA trip with store bag
        TripOrderAssignmentResponse closeMyntraTrip = tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
//Validate if the store trip is closed
        Assert.assertTrue(closeMyntraTrip.getStatus().getStatusType().toString().toLowerCase().contains("success"));

//Try to  close the store  trip, Since there are undelivered items we should not be able to close


        TripOrderAssignmentResponse closeTrip = tripClient_qa.closeStoreTrip(storeTripId, tripOrderIdAttemptReasonMap, storeTenantId);

//Validate that trip cannot be closed since there are Unattempted items
            Assert.assertTrue(closeTrip.getStatus().getStatusType().toString().toLowerCase().contains("error"));
            //   Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().contains("Shipments are in (WFD, OFD) state for this trip"));



        tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);

        //Mark the unattempted orders are Failed and then close
        for (String FDTrack : UnattemptedDLTrackingList) {
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(FDTrack)).findFirst().get().getId();
            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);

            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(FDTrack).toString(), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        }


        //Now validate that the shipments that were unattempted are now FD


        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);

        for (String FDTrackNo : UnattemptedDLTrackingList) {

            //Validate Store ML shipment is Failed Delivered
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //VAlidate the ORderToShip (Platform ) status is FAILED_DELIVERY

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(FDTrackNo),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.FAILED_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.FAILED_DELIVERY.name());
        }

//Now the total failed and Unattempted are marked as Failed so the total failed is failedUnattemptedFinalList
     /*   failedUnattemptedFinalList.addAll(failedDlTrackingList);
        failedUnattemptedFinalList.addAll(failedUnattemptedFinalList);*/

        failedUnattemptedFinalList = ListUtils.union(failedDlTrackingList, UnattemptedDLTrackingList);


        //We cannot close the trip without receiving shipments
        if (closeTrip.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.assertTrue(closeTrip.getStatus().getStatusType().toString().toLowerCase().contains("error"));
            //   Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().contains("Shipments are in (WFD, OFD) state for this trip"));
        }

        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");

//Validate store summary before creating reverse bag
        //Now total attemped will have the earlier unattemped ones which were later marked FD
        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, DlCashTrackingList.size() + DlPrepaidTrackingList.size() + failedUnattemptedFinalList.size(), noOfOrders, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), 0);

        // lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, 2, noOfOrders, 1, 0);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);

//TODO : gloria even before receiving it is now FAILED_DELIVERY


        for (String FDTrackNo : failedUnattemptedFinalList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

        }


//Create Trip
        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + failedDlTrackingList.get(0) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + failedDlTrackingList.get(0) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the amount collected for CASH shipments + " + DlCashTrackingList.toString() + " , is : Rs . " + cashAmountToBeCollected + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the PREPAID shipments + " + DlPrepaidTrackingList.toString() + " \n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the Unattempted and Failed  shipments are + " + failedUnattemptedFinalList.toString() + "\n\n\n ");
        //Validate the reverse bag contents displayed in love :http://lastmile.scmqa.myntra.com/lastmile-service/trip/v2/findShipmentsByTripNumber/Automation1-181120155306433?shipmentType=REVERSE_BAG&tenantId=4019
      /*  "shipmentId": 21221343,
                "shipmentItemCount": 1,
                "shipmentType": "REVERSE_BAG",
                "staffPhoneNumber": 1511690820,
                "status": "WFD",
                "storeDCId": 9235,
                "totalFailedDeliveriesPicked": 0,
                "totalFailedDeliveriesToPick": 1,
                "totalSuccess": 1,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,
                "totalUnAttempted": 0,
                "tripId": 59046*/

        TripShipmentAssociationResponse reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(reverseTripNumber, ShipmentType.REVERSE_BAG, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(reverseBagId));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStoreDCId().equals(storeHlPId));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getMyntraSDATripNumber().equals(reverseTripNumber));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue((reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getReverseBagExpectedShipmentTrackingNumbers().containsAll(forwardTrackingNumbersList)));

        //We have 1 failed delivery
        /* "shipmentItemCount": 1,*/

        lastmileHelper.validateReverseBagShipmentItemCnt(reverseTripNumber, LMS_CONSTANTS.TENANTID, failedUnattemptedFinalList.size());
        //Validate the amount To be collected is 0.0 and amount collected is 0.0
   /*     "amountCollected": 0,
                "amountToBeCollected": 0,*/
        lastmileHelper.validateReverseBagAmount(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0.0f, cashAmountToBeCollected);
   /*     "totalFailedDeliveriesPicked": 0,
                "totalFailedDeliveriesToPick": 1,*/
        lastmileHelper.validateReverseFailedDeliveries(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0, failedUnattemptedFinalList.size());
      /*  "totalSuccess": 1,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,*/
        lastmileHelper.validateReverseSuccess(reverseTripNumber, LMS_CONSTANTS.TENANTID, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), 0, 0);
//        "totalUnAttempted": 0,
        lastmileHelper.validateReverseUnAttempted(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0);
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the amount collected for CASH shipments + " + DlCashTrackingList.toString() + " , is : Rs . " + cashAmountToBeCollected + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the PREPAID shipments + " + DlPrepaidTrackingList.toString() + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the Unattempted and Failed  shipments are + " + failedUnattemptedFinalList.toString() + "\n\n\n ");


        //Validate the reverse MB is ADDED_TO_TRIP and there are no shipments present in ShipmentOrderMap
        //shipmentOrderMap has NO entry since we have not delivered the items
        lastmileHelper.validateForReverseMbAddedToTrip(Long.valueOf(reverseBagId));

        //Start the reverse trip

        tripClient_qa.startTrip(reverseTripId);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickip
        // Pickup the reverse bag

        TripShipmentAssociationResponse reverseShipmentResponse =  tripClient_qa.pickupReverseBagFromStore(String.valueOf(reverseBagId), failedUnattemptedFinalList, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, failedUnattemptedFinalList.size(), 0, cashAmountToBeCollected,LMS_CONSTANTS.TENANTID);


        // reverse bag should be IN_TRANSIT after pickup
        //Shipment orderMap will now have these entries ONLY which has been scanned
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateForReverseMbEntriesAfterPickup(Long.valueOf(reverseBagId), failedUnattemptedFinalList);

        //Store Summary should not have any data since everything is reconcilied
        lastmileHelper.validateStoreSummaryAfterReconcilation(originPremiseId, storeHlPId);
       /* "amountCollected": 0,
                "amountToBeCollected": 0,
                "attemptReasonCode": "PICKED_UP_SUCCESSFULLY",
                "deliveryStaffName": "LA9566 - LA_FN9566 LA_LN9566",
                "itemsPickedUp": 0,
                "myntraSDATripNumber": "AAAA-181120201411814",
                "shipmentId": 21221594,
                "shipmentItemCount": 1,
                "shipmentType": "REVERSE_BAG",
                "staffPhoneNumber": 1733995666,
                "status": "PS",
                "totalFailedDeliveriesPicked": 1,
                "totalFailedDeliveriesToPick": 1,
                "totalSuccess": 0,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,
                "totalUnAttempted": 0,*/


        reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(reverseTripNumber, ShipmentType.REVERSE_BAG, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getAttemptReasonCode().name().equals(AttemptReasonCode.PICKED_UP_SUCCESSFULLY.name()));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.PS.name()));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getMyntraSDATripNumber().equals(reverseTripNumber));
        //Validate the amount To be collected is 0.0 and amount collected is 0.0
   /*     "amountCollected": 0,
                "amountToBeCollected": 0,*/
        lastmileHelper.validateReverseBagAmount(reverseTripNumber, LMS_CONSTANTS.TENANTID, cashAmountToBeCollected, cashAmountToBeCollected);
   /*     "totalFailedDeliveriesPicked": 1,
                "totalFailedDeliveriesToPick": 1,*/
        lastmileHelper.validateReverseFailedDeliveries(reverseTripNumber, LMS_CONSTANTS.TENANTID, failedUnattemptedFinalList.size(), failedUnattemptedFinalList.size());
      /*  "totalSuccess": 0,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,*/
        //TODO : itemsPickedUp and totalSuccess are NOT used
        lastmileHelper.validateReverseSuccess(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0, 0, 0);
//        "totalUnAttempted": 0,
        lastmileHelper.validateReverseUnAttempted(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0);
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the amount collected for CASH shipments + " + DlCashTrackingList.toString() + " , is : Rs . " + cashAmountToBeCollected + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the PREPAID shipments + " + DlPrepaidTrackingList.toString() + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the Unattempted and Failed  shipments are + " + failedUnattemptedFinalList.toString() + "\n\n\n ");

        //Close trip
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, reverseTripId);
        //Validate Unattempted Shipment response has nothing (same as store summary)

        lastmileHelper.validateUnattemptedAfterReconcilation(storeHlPId);

        //TODO : Receive Shipments
        // Now close the store trip since all are marked FD/DL
        for (String track : failedUnattemptedFinalList) {
            TripOrderAssignmentResponse receiveTripOrder = tripOrderAssignmentClient_qa.receiveTripShipments(track, LMS_CONSTANTS.TENANTID);

            Assert.assertTrue(receiveTripOrder.getTripOrders().get(0).getIsReceived());
        }
        TripOrderAssignmentResponse closeTripSUcess = tripClient_qa.closeStoreTrip(storeTripId, tripOrderIdAttemptReasonMap, storeTenantId);
        //Store trip can be closed only after we receive all shipments
//Validate that trip is be closed
        Assert.assertTrue(closeTripSUcess.getStatus().getStatusType().toString().toLowerCase().contains("success"));

        //After closing the trip the failedDelivered item ,status MLResponse for store entry is RECEIVED_IN_DC
//Now even the unattemped were later marked FD
        for (String FDTrackNo : UnattemptedDLTrackingList) {

            lastmileHelper.validateStoreMLEntryFailedDeliveredAfterCloseTrip(storeTenantId, FDTrackNo);
        }
//Now even the unattemped were later marked FD
        for (String FDTrackNo : failedDlTrackingList) {

            lastmileHelper.validateStoreMLEntryFailedDeliveredAfterCloseTrip(storeTenantId, FDTrackNo);
        }

//TODO : gloria AFTER receiving it is now FAILED_DELIVERY


        for (String FDTrackNo : failedUnattemptedFinalList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.RECEIVED_IN_DC.name()));

        }


    }


    @Test(groups = {"Sanity",
    }, priority = 0, dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "eodReconAllFlows", description = " Create a Store and attach it to a DC then inscan the shipments and deliver it")
    public void EODReconExchange_AllFlows_WithScan(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                   String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws UnsupportedEncodingException, JAXBException {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        int noOfOrders = 4;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> failedDlTrackingList = new ArrayList<>();
        List<String> DlCashTrackingList = new ArrayList<>();
        List<String> UnattemptedDLTrackingList = new ArrayList<>();
        List<String> DlPrepaidTrackingList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        Map<String, Long> trackingInstakartMlIdMap = new HashMap<>();
        Map<String, Long> trackingStoreMlIdMap = new HashMap<>();
        Map<String, Float> trackingShipmentValue = new HashMap<>();
        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        Float cashAmountToBeCollected = 0.0f;
        List<String> failedUnattemptedFinalList = new ArrayList<>();

        String orderIDParent = null;
        String exchangeOrder = null;
        //
        //1. Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();

        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());


        //Create a MasterBag from pincode : LMS_PINCODE.ML_BLR (order's pincode)
//TODO : GLoria check why dc id is returned wrong for
        // originPremiseId = deliveryCenterClient.getOriginPremiseIdOfDC(LASTMILE_CONSTANTS.EOD_PINCODE, tenantId);
        originPremiseId = 1l;
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //2. Create Forward Order Till SH

        for (int i = 0; i < noOfOrders; i++) {
            try {
                orderIDParent = lmsHepler.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.EOD_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
                exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LASTMILE_CONSTANTS.EOD_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderIDParent));
                System.out.println(exchangeOrder);
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Order creation in SH state failed : " + e.toString());
            }


            //Add the order to the MB

            String packetId = omsServiceHelper.getPacketId(exchangeOrder);
            log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
            forwardTrackingNumbersList.add(trackingNo);
            trackingNoOrderIdMap.put(trackingNo, packetId);
            log.info("Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + exchangeOrder + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
            System.out.println("\n_____________Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + exchangeOrder + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

            ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
            try {
                //TODO : Try to use addShipmentToMasterBag
                //TODO : negative cases here
                String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
                if (response.toLowerCase().contains("error")) {
                    Assert.fail("Error while adding shipment to store bag");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
            System.out.println("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

            //Verify if the order is added in Bag
            OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
            Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
            System.out.println("\n______________The status of the " + i + " orderId -  " + exchangeOrder + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
            log.info("_____________The status of the " + i + " order " + exchangeOrder + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());

        }


        //Orders are created
        failedDlTrackingList.add(forwardTrackingNumbersList.get(0));
        DlCashTrackingList.add(forwardTrackingNumbersList.get(1));
        DlPrepaidTrackingList.add(forwardTrackingNumbersList.get(2));
        UnattemptedDLTrackingList.add(forwardTrackingNumbersList.get(3));


        //Creating a trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");

        //TODO : TO BE CHECKED BY DEV
      /*  Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentItems().size().equals(noOfOrders));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().name().equals(ShipmentType.FORWARD_BAG.name());
        */


        //Validate the MB status is ADDED_TO_TRIP
        //TODO : Add in validator

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, noOfOrders, code, originPremiseId);


        //TODO : to check after adding bag to trip, sipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));
        }

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        //TODO : TO BE CHECKED BY DEV
        //Validate status  of this above response is OFD
        searchParams = "tripNumber.like:" + tripNumber;
        TripResponse tripResponse1 = tripClient_qa.searchTripByParam(0, 20, "id", "ASC", Boolean.parseBoolean(null), searchParams, "");
        //TODO : to add in validation
        Assert.assertTrue(tripResponse1.getTrips().get(0).getDeliveryStaffEntry().getDeliveryStaffType().name().equals(DeliveryStaffType.MYNTRA_PAYROLL.name()));
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        lastmileHelper.validateTripStartedForMB(masterBagId);

        //Before delivering the MB validate that there is nothing in the store
        //Validate getUnattempted api in the store is empty
        List<String> forwardUnattemptedShipmentsB4DL = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        Assert.assertTrue(forwardUnattemptedShipmentsB4DL.isEmpty(), "Store has some unAttempted items before delivering the MB to the stpre itself");
        //Before delivering the bag to store, validate only one entry in ML

        //Vaidate ONLY one  ML entry created for tenant
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            // Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Saving the ml id for tenant 4019 , this will be source ref id for mlResponse with storeTenant
            trackingInstakartMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            trackingShipmentValue.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentValue());
            //Validate status of Instakart ML shipment is MLDeliveryShipmentStatus
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.name()));

        }
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            //Validate there is no ML shipment created for Store
            Assert.assertTrue(mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId).getStatus().getStatusMessage().equals("Invalid Shipment Id"));

        }

        //Deliver the MB

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);

        //Validate if shipment_order_map status is DELIVERED

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);

            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }
        //Validate getUnattempted api in the store
        List<String> forwardUnattemptedShipments = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.PU);
        Assert.assertTrue(forwardTrackingNumbersList.containsAll(forwardUnattemptedShipments), "Store does not right number of unattempted items");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Vaidate new ML entry created for tenant
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            // For 4019 no entry of getMlLastMilePartnerShipmentAssignment should return NULL
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            //   mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();

            //Validate tenant is 4019 and client is 2297
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.CLIENTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID));

            //Validate the source reference id is the orderId
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId().equals(trackingNoOrderIdMap.get(track)));

            //Validate the status for the Store Tenant ML entry as HANDED_OVER_TO_LASTMILE_PARTNER
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.name()));

            //Validate the DC id is 5

            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(originPremiseId));


        }
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.EXPECTED_IN_DC.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));

        }
        //After delivering the store bag to store , the shipment order map status should be DELIVERED
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }


        //Get delivery staff for Store by searching using dc id
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        long expectedstoreDeliveryStaffId = deliveryStaffClient_qa.findDeliveryStaffByMobNo2(mobileNumber);
        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
//TODO ------------------------ storeTrip.getTrips().get(0).getTripStatus() -> CREATED instead of OFD, sda trip : tripResponse1.getTrips().get(0).getTripStatus() is CREATED
        //Validate after creating of Store Bag it becomes OFD
        Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        //Validate Myntra SDA trip is OFD
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        //Validate the Store ML Shipment goes for OFD
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));


            //VAlidate the ORderToShip (Platform ) status is OFD
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            OrderResponse orderResponse = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(track),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.OUT_FOR_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.OUT_FOR_DELIVERY.name());

        }

        //Validate Store Trip tenant and status and code
        //TODO : Check with Azeem why storeTrip.getTrips().get(0).getTripStatus() is returning CREATED instead of OFD
        //   Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        //Get the store summary before delivering the shipments to Customer validate
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        lastmileHelper.validateStoreSummaryBeforeDeliveringExchange(originPremiseId, storeHlPId, forwardTrackingNumbersList, noOfOrders);

        //Now deliver forwardTrackingNumbersList.get(0) order to customer with PREPAID

        //TODO : ask the payment methods to dev
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        int count = 0;
        for (String DlPrepaidTrack : DlPrepaidTrackingList) {
            count++;
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(DlPrepaidTrack)).findFirst().get().getId();

            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);

            //TODO :___________________Validate this trip is in OFD
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //Assert.assertTrue(tripOrderAssignment.getTripOrders().get(0).getTripOrderStatus().name().equals("Out for delivery"));
            if (count % 2 == 0)

                tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(DlPrepaidTrack).toString(), tripOrderAssignmentId, storeTenantId, "PRE_PAID",LASTMILE_CONSTANTS.TENANT_ID);
            else
                tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(DlPrepaidTrack).toString(), tripOrderAssignmentId, storeTenantId, "DEBIT_CARD",LASTMILE_CONSTANTS.TENANT_ID);

        }

        //Delver the cash orders
        for (String DlCashTrack : DlCashTrackingList) {
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(DlCashTrack)).findFirst().get().getId();

            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);

            //TODO :___________________Validate this trip is in OFD
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //Assert.assertTrue(tripOrderAssignment.getTripOrders().get(0).getTripOrderStatus().name().equals("Out for delivery"));
            tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(DlCashTrack).toString(), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

            cashAmountToBeCollected = cashAmountToBeCollected + trackingShipmentValue.get(DlCashTrack);
        }
        // Validate                "totalAmountToBeCollected": 0,
       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 1,
                "totalUnAttempted": 1,*/

        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), noOfOrders, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), UnattemptedDLTrackingList.size() + failedDlTrackingList.size());

        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        //Validate Store ML shipment is Delivered

        for (String t : DlPrepaidTrackingList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));


            //VAlidate the ORderToShip (Platform ) status is OFD

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(t),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.DELIVERED), "The platform  orderToship status is not : " + ShipmentStatus.DELIVERED.name());

            lastmileHelper.validateStoreMLEntryDelivered(originPremiseId, storeHlPId, storeTenantId, t);

        }

        for (String t : DlCashTrackingList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));


            //VAlidate the ORderToShip (Platform ) status is OFD

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(t),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.DELIVERED), "The platform  orderToship status is not : " + ShipmentStatus.DELIVERED.name());

            lastmileHelper.validateStoreMLEntryDelivered(originPremiseId, storeHlPId, storeTenantId, t);
        }


        //Now fail the other  forwardTrackingNumbersList.get(1)/ failedDlTrackingList.get(0)
        //Get the tripOrderAssignment id for the store trip
        tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);

        for (String FDTrack : failedDlTrackingList) {
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(FDTrack)).findFirst().get().getId();
            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);

            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(FDTrack).toString(), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        }
       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 2,
                "totalUnAttempted": 0,   "totalAmountToBeCollected": 0,
*/

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, DlCashTrackingList.size() + DlPrepaidTrackingList.size() + failedDlTrackingList.size(), noOfOrders, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), UnattemptedDLTrackingList.size());
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);

        for (String FDTrack : failedDlTrackingList) {

            lastmileHelper.validateStoreMLEntryFailedDeliveredb4CloseTrip(originPremiseId, storeHlPId, storeTenantId, FDTrack);
        }
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);

        for (String FDTrackNo : failedDlTrackingList) {

            //Validate Store ML shipment is Failed Delivered
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //VAlidate the ORderToShip (Platform ) status is FAILED_DELIVERY

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(FDTrackNo),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.FAILED_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.FAILED_DELIVERY.name());
        }
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Close the Myntra SDA trip with store bag
        TripOrderAssignmentResponse closeMyntraTrip = tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
//Validate if the store trip is closed
        Assert.assertTrue(closeMyntraTrip.getStatus().getStatusType().toString().toLowerCase().contains("success"));

//Try to  close the store  trip, Since there are undelivered items we should not be able to close


        TripOrderAssignmentResponse closeTrip = tripClient_qa.closeStoreTrip(storeTripId, tripOrderIdAttemptReasonMap, storeTenantId);

//Validate that trip cannot be closed since there are Unattempted items
            Assert.assertTrue(closeTrip.getStatus().getStatusType().toString().toLowerCase().contains("error"));



        tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);

        //Mark the unattempted orders are Failed and then close
        for (String FDTrack : forwardUnattemptedShipments) {
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(FDTrack)).findFirst().get().getId();
            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);

            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(FDTrack).toString(), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        }


        //Now validate that the shipments that were unattempted are now FD


        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);

        for (String FDTrackNo : UnattemptedDLTrackingList) {

            //Validate Store ML shipment is Failed Delivered
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //VAlidate the ORderToShip (Platform ) status is FAILED_DELIVERY

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(FDTrackNo),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.FAILED_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.FAILED_DELIVERY.name());
        }

//Now the total failed and Unattempted are marked as Failed so the total failed is failedUnattemptedFinalList
     /*   failedUnattemptedFinalList.addAll(failedDlTrackingList);
        failedUnattemptedFinalList.addAll(failedUnattemptedFinalList);*/

        failedUnattemptedFinalList = ListUtils.union(failedDlTrackingList, UnattemptedDLTrackingList);


        //We cannot close the trip without receiving shipments
        if (closeTrip.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.assertTrue(closeTrip.getStatus().getStatusType().toString().toLowerCase().contains("error"));
            //   Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().contains("Shipments are in (WFD, OFD) state for this trip"));
        }

        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");

//Validate store summary before creating reverse bag
        //Now total attemped will have the earlier unattemped ones which were later marked FD
        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, DlCashTrackingList.size() + DlPrepaidTrackingList.size() + failedUnattemptedFinalList.size(), noOfOrders, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), 0);

        // lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, 2, noOfOrders, 1, 0);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);

//TODO : gloria even before receiving it is now FAILED_DELIVERY


        for (String FDTrackNo : failedUnattemptedFinalList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

        }


//Create Trip
        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + failedDlTrackingList.get(0) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + failedDlTrackingList.get(0) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the amount collected for CASH shipments + " + DlCashTrackingList.toString() + " , is : Rs . " + cashAmountToBeCollected + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the PREPAID shipments + " + DlPrepaidTrackingList.toString() + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the Unattempted and Failed  shipments are + " + failedUnattemptedFinalList.toString() + "\n\n\n ");

        //Validate the reverse bag contents displayed in love :http://lastmile.scmqa.myntra.com/lastmile-service/trip/v2/findShipmentsByTripNumber/Automation1-181120155306433?shipmentType=REVERSE_BAG&tenantId=4019
      /*  "shipmentId": 21221343,
                "shipmentItemCount": 1,
                "shipmentType": "REVERSE_BAG",
                "staffPhoneNumber": 1511690820,
                "status": "WFD",
                "storeDCId": 9235,
                "totalFailedDeliveriesPicked": 0,
                "totalFailedDeliveriesToPick": 1,
                "totalSuccess": 1,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,
                "totalUnAttempted": 0,
                "tripId": 59046*/

        TripShipmentAssociationResponse reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(reverseTripNumber, ShipmentType.REVERSE_BAG, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(reverseBagId));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStoreDCId().equals(storeHlPId));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getMyntraSDATripNumber().equals(reverseTripNumber));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue((reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getReverseBagExpectedShipmentTrackingNumbers().containsAll(forwardTrackingNumbersList)));

        //We have 1 failed delivery
        /* "shipmentItemCount": 1,*/

        lastmileHelper.validateReverseBagShipmentItemCnt(reverseTripNumber, LMS_CONSTANTS.TENANTID, failedUnattemptedFinalList.size());
        //Validate the amount To be collected is 0.0 and amount collected is 0.0
   /*     "amountCollected": 0,
                "amountToBeCollected": 0,*/
        lastmileHelper.validateReverseBagAmount(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0.0f, cashAmountToBeCollected);
   /*     "totalFailedDeliveriesPicked": 0,
                "totalFailedDeliveriesToPick": 1,*/
        lastmileHelper.validateReverseFailedDeliveries(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0, failedUnattemptedFinalList.size());
      /*  "totalSuccess": 1,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,*/
        lastmileHelper.validateReverseSuccess(reverseTripNumber, LMS_CONSTANTS.TENANTID, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), 0, 0);
//        "totalUnAttempted": 0,
        lastmileHelper.validateReverseUnAttempted(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0);
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the amount collected for CASH shipments + " + DlCashTrackingList.toString() + " , is : Rs . " + cashAmountToBeCollected + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the PREPAID shipments + " + DlPrepaidTrackingList.toString() + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the Unattempted and Failed  shipments are + " + failedUnattemptedFinalList.toString() + "\n\n\n ");


        //Validate the reverse MB is ADDED_TO_TRIP and there are no shipments present in ShipmentOrderMap
        //shipmentOrderMap has NO entry since we have not delivered the items
        lastmileHelper.validateForReverseMbAddedToTrip(Long.valueOf(reverseBagId));

        //Start the reverse trip

        tripClient_qa.startTrip(reverseTripId);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickip
        // Pickup the reverse bag

        TripShipmentAssociationResponse reverseShipmentResponse =  tripClient_qa.pickupReverseBagFromStore(String.valueOf(reverseBagId), failedUnattemptedFinalList, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, failedUnattemptedFinalList.size(), 0, cashAmountToBeCollected,LMS_CONSTANTS.TENANTID);


        // reverse bag should be IN_TRANSIT after pickup
        //Shipment orderMap will now have these entries ONLY which has been scanned
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateForReverseMbEntriesAfterPickup(Long.valueOf(reverseBagId), failedUnattemptedFinalList);

        //Store Summary should not have any data since everything is reconcilied
        lastmileHelper.validateStoreSummaryAfterReconcilation(originPremiseId, storeHlPId);
       /* "amountCollected": 0,
                "amountToBeCollected": 0,
                "attemptReasonCode": "PICKED_UP_SUCCESSFULLY",
                "deliveryStaffName": "LA9566 - LA_FN9566 LA_LN9566",
                "itemsPickedUp": 0,
                "myntraSDATripNumber": "AAAA-181120201411814",
                "shipmentId": 21221594,
                "shipmentItemCount": 1,
                "shipmentType": "REVERSE_BAG",
                "staffPhoneNumber": 1733995666,
                "status": "PS",
                "totalFailedDeliveriesPicked": 1,
                "totalFailedDeliveriesToPick": 1,
                "totalSuccess": 0,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,
                "totalUnAttempted": 0,*/


        reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(reverseTripNumber, ShipmentType.REVERSE_BAG, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getAttemptReasonCode().name().equals(AttemptReasonCode.PICKED_UP_SUCCESSFULLY.name()));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.PS.name()));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getMyntraSDATripNumber().equals(reverseTripNumber));
        //Validate the amount To be collected is 0.0 and amount collected is 0.0
   /*     "amountCollected": 0,
                "amountToBeCollected": 0,*/
        lastmileHelper.validateReverseBagAmount(reverseTripNumber, LMS_CONSTANTS.TENANTID, cashAmountToBeCollected, cashAmountToBeCollected);
   /*     "totalFailedDeliveriesPicked": 1,
                "totalFailedDeliveriesToPick": 1,*/
        lastmileHelper.validateReverseFailedDeliveries(reverseTripNumber, LMS_CONSTANTS.TENANTID, failedUnattemptedFinalList.size(), failedUnattemptedFinalList.size());
      /*  "totalSuccess": 0,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,*/
        //TODO : itemsPickedUp and totalSuccess are NOT used
        lastmileHelper.validateReverseSuccess(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0, 0, 0);
//        "totalUnAttempted": 0,
        lastmileHelper.validateReverseUnAttempted(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0);
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the amount collected for CASH shipments + " + DlCashTrackingList.toString() + " , is : Rs . " + cashAmountToBeCollected + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the PREPAID shipments + " + DlPrepaidTrackingList.toString() + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the Unattempted and Failed  shipments are + " + failedUnattemptedFinalList.toString() + "\n\n\n ");

        //Close trip
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, reverseTripId);
        //Validate Unattempted Shipment response has nothing (same as store summary)

        lastmileHelper.validateUnattemptedAfterReconcilation(storeHlPId);

        //TODO : Receive Shipments
        // Now close the store trip since all are marked FD/DL
        for (String track : failedUnattemptedFinalList) {
            TripOrderAssignmentResponse receiveTripOrder = tripOrderAssignmentClient_qa.receiveTripShipments(track, LMS_CONSTANTS.TENANTID);

            Assert.assertTrue(receiveTripOrder.getTripOrders().get(0).getIsReceived());
        }
        TripOrderAssignmentResponse closeTripSUcess = tripClient_qa.closeStoreTrip(storeTripId, tripOrderIdAttemptReasonMap, storeTenantId);
        //Store trip can be closed only after we receive all shipments
//Validate that trip is be closed
        Assert.assertTrue(closeTripSUcess.getStatus().getStatusType().toString().toLowerCase().contains("success"));

        //After closing the trip the failedDelivered item ,status MLResponse for store entry is RECEIVED_IN_DC
//Now even the unattemped were later marked FD
        for (String FDTrackNo : UnattemptedDLTrackingList) {

            lastmileHelper.validateStoreMLEntryFailedDeliveredAfterCloseTrip(storeTenantId, FDTrackNo);
        }
//Now even the unattemped were later marked FD
        for (String FDTrackNo : failedDlTrackingList) {

            lastmileHelper.validateStoreMLEntryFailedDeliveredAfterCloseTrip(storeTenantId, FDTrackNo);
        }

//TODO : gloria AFTER receiving it is now RECEIVED_IN_DC and Store tenant


        for (String FDTrackNo : failedUnattemptedFinalList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.RECEIVED_IN_DC.name()));

        }

//TODO : gloria AFTER receiving it is now RTO_CONFIRMED for both Instakart tenant


        for (String FDTrackNo : failedUnattemptedFinalList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.RECEIVED_IN_DC.name()));

        }


    }



    //Using old deliver to store bag without having tracking number list

    @Test(groups = {"Sanity",
    }, priority = 0, dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "eodReconAllFlows", description = "Using old deliver to store bag without having tracking number list ")
    public void eodRecon_NotScanningForwardBag(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                               String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        int noOfOrders = 1;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> failedDlTrackingList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        Map<String, Long> trackingInstakartMlIdMap = new HashMap<>();
        Map<String, Long> trackingStoreMlIdMap = new HashMap<>();
        Map<String, Float> trackingShipmentValue = new HashMap<>();

        //  tripOrderAssignmentClient_qa.receiveTripShipments("ML0001599587",LMS_CONSTANTS.TENANTID);

        //1. Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();

        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());


        //Create a MasterBag from pincode : LMS_PINCODE.ML_BLR (order's pincode)

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //2. Create Forward Order Till SH

        for (int i = 0; i < noOfOrders; i++) {
            try {
                orderID = lmsHepler.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
                log.info("The orderId is : +" + orderID);
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Order creation in SH state failed : " + e.toString());
            }


            //Add the order to the MB

            String packetId = omsServiceHelper.getPacketId(orderID);
            log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
            forwardTrackingNumbersList.add(trackingNo);
            trackingNoOrderIdMap.put(trackingNo, packetId);
            log.info("Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
            System.out.println("\n_____________Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

            ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
            try {
                //TODO : Try to use addShipmentToMasterBag
                //TODO : negative cases here
                String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
                if (response.toLowerCase().contains("error")) {
                    Assert.fail("Error while adding shipment to store bag");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
            System.out.println("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

            //Verify if the order is added in Bag
            OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
            Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
            System.out.println("\n______________The status of the " + i + " orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
            log.info("_____________The status of the " + i + " order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());

        }

        //Creating a trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");

        //TODO : TO BE CHECKED BY DEV
      /*  Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentItems().size().equals(noOfOrders));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().name().equals(ShipmentType.FORWARD_BAG.name());
        */


        //Validate the MB status is ADDED_TO_TRIP
        //TODO : Add in validator
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, noOfOrders, code, originPremiseId);


        //TODO : to check after adding bag to trip, sipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));
        }

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        //TODO : TO BE CHECKED BY DEV
        //Validate status  of this above response is OFD
        searchParams = "tripNumber.like:" + tripNumber;
        TripResponse tripResponse1 = tripClient_qa.searchTripByParam(0, 20, "id", "ASC", Boolean.parseBoolean(null), searchParams, "");
        //TODO : to add in validation
        Assert.assertTrue(tripResponse1.getTrips().get(0).getDeliveryStaffEntry().getDeliveryStaffType().name().equals(DeliveryStaffType.MYNTRA_PAYROLL.name()));
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        lastmileHelper.validateTripStartedForMB(masterBagId);

        //Before delivering the MB validate that there is nothing in the store  It is to get any shipments lying in dc. Non attempted ones.
        //Validate getUnattempted api in the store is empty
        List<String> forwardUnattemptedShipmentsB4DL = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        Assert.assertTrue(forwardUnattemptedShipmentsB4DL.isEmpty(), "Store has some unAttempted items before delivering the MB to the stpre itself");
        //Before delivering the bag to store, validate only one entry in ML

        //Vaidate ONLY one  ML entry created for tenant
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            // Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Saving the ml id for tenant 4019 , this will be source ref id for mlResponse with storeTenant
            trackingInstakartMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            trackingShipmentValue.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentValue());
            //Validate status of Instakart ML shipment is MLDeliveryShipmentStatus
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.name()));

        }
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            //Validate there is no ML shipment created for Store
            Assert.assertTrue(mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId).getStatus().getStatusMessage().equals("Invalid Shipment Id"));

        }


        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverMensaOldStoreBagToStore(String.valueOf(masterBagId), String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().toString().equals("ERROR"));
        Assert.assertEquals("Cannot deliver empty forward bag", shipmentResponse.getStatus().getStatusMessage(), "");

    }


    @Test(groups = {"Sanity",
    }, priority = 0, dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "eodReconAllFlows", description = "Using old pickup reverse bag without having tracking number list ")
    public void eodRecon_NotScanningReverseBag(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                               String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws UnsupportedEncodingException, JAXBException {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        int noOfOrders = 4;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> failedDlTrackingList = new ArrayList<>();
        List<String> DlCashTrackingList = new ArrayList<>();
        List<String> UnattemptedDLTrackingList = new ArrayList<>();
        List<String> DlPrepaidTrackingList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        Map<String, Long> trackingInstakartMlIdMap = new HashMap<>();
        Map<String, Long> trackingStoreMlIdMap = new HashMap<>();
        Map<String, Float> trackingShipmentValue = new HashMap<>();
        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        Float cashAmountToBeCollected = 0.0f;
        List<String> failedUnattemptedFinalList = new ArrayList<>();

        //
        //1. Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();

        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());


        //Create a MasterBag from pincode : LMS_PINCODE.ML_BLR (order's pincode)
//TODO : GLoria check why dc id is returned wrong for
        // originPremiseId = deliveryCenterClient.getOriginPremiseIdOfDC(LASTMILE_CONSTANTS.EOD_PINCODE, tenantId);
        originPremiseId = 1l;
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //2. Create Forward Order Till SH

        for (int i = 0; i < noOfOrders; i++) {
            try {
                orderID = lmsHepler.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.EOD_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
                log.info("The orderId is : +" + orderID);
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Order creation in SH state failed : " + e.toString());
            }


            //Add the order to the MB

            String packetId = omsServiceHelper.getPacketId(orderID);
            log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
            forwardTrackingNumbersList.add(trackingNo);
            trackingNoOrderIdMap.put(trackingNo, packetId);
            log.info("Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
            System.out.println("\n_____________Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

            ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
            try {
                //TODO : Try to use addShipmentToMasterBag
                //TODO : negative cases here
                String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
                if (response.toLowerCase().contains("error")) {
                    Assert.fail("Error while adding shipment to store bag");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
            System.out.println("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

            //Verify if the order is added in Bag
            OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
            Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
            System.out.println("\n______________The status of the " + i + " orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
            log.info("_____________The status of the " + i + " order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());

        }


        //Orders are created
        failedDlTrackingList.add(forwardTrackingNumbersList.get(0));
        DlCashTrackingList.add(forwardTrackingNumbersList.get(1));
        DlPrepaidTrackingList.add(forwardTrackingNumbersList.get(2));
        UnattemptedDLTrackingList.add(forwardTrackingNumbersList.get(3));


        //Creating a trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");

        //TODO : TO BE CHECKED BY DEV
      /*  Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentItems().size().equals(noOfOrders));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().name().equals(ShipmentType.FORWARD_BAG.name());
        */


        //Validate the MB status is ADDED_TO_TRIP
        //TODO : Add in validator

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, noOfOrders, code, originPremiseId);


        //TODO : to check after adding bag to trip, sipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));
        }

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        //TODO : TO BE CHECKED BY DEV
        //Validate status  of this above response is OFD
        searchParams = "tripNumber.like:" + tripNumber;
        TripResponse tripResponse1 = tripClient_qa.searchTripByParam(0, 20, "id", "ASC", Boolean.parseBoolean(null), searchParams, "");
        //TODO : to add in validation
        Assert.assertTrue(tripResponse1.getTrips().get(0).getDeliveryStaffEntry().getDeliveryStaffType().name().equals(DeliveryStaffType.MYNTRA_PAYROLL.name()));
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        lastmileHelper.validateTripStartedForMB(masterBagId);

        //Before delivering the MB validate that there is nothing in the store
        //Validate getUnattempted api in the store is empty
        List<String> forwardUnattemptedShipmentsB4DL = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        Assert.assertTrue(forwardUnattemptedShipmentsB4DL.isEmpty(), "Store has some unAttempted items before delivering the MB to the stpre itself");
        //Before delivering the bag to store, validate only one entry in ML

        //Vaidate ONLY one  ML entry created for tenant
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            // Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Saving the ml id for tenant 4019 , this will be source ref id for mlResponse with storeTenant
            trackingInstakartMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            trackingShipmentValue.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentValue());
            //Validate status of Instakart ML shipment is MLDeliveryShipmentStatus
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.name()));

        }
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            //Validate there is no ML shipment created for Store
            Assert.assertTrue(mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId).getStatus().getStatusMessage().equals("Invalid Shipment Id"));

        }

        //Deliver the MB

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);

        //Validate if shipment_order_map status is DELIVERED

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);

            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }
        //Validate getUnattempted api in the store
        List<String> forwardUnattemptedShipments = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        System.out.println("\n\n\n\n\n\n  ***************************************************** The list of forward Unattempted Shipments is         : " + forwardUnattemptedShipments + "  \n\n\n\n*****************************************************");
        Assert.assertTrue(forwardTrackingNumbersList.containsAll(forwardUnattemptedShipments), "Store does not right number of unattempted items");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Vaidate new ML entry created for tenant
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            // For 4019 no entry of getMlLastMilePartnerShipmentAssignment should return NULL
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            //   mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();

            //Validate tenant is 4019 and client is 2297
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.CLIENTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID));

            //Validate the source reference id is the orderId
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId().equals(trackingNoOrderIdMap.get(track)));

            //Validate the status for the Store Tenant ML entry as HANDED_OVER_TO_LASTMILE_PARTNER
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.name()));

            //Validate the DC id is 5

            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(originPremiseId));


        }
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.EXPECTED_IN_DC.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));

        }
        //After delivering the store bag to store , the shipment order map status should be DELIVERED
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }


        //Get delivery staff for Store by searching using dc id
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        long expectedstoreDeliveryStaffId = deliveryStaffClient_qa.findDeliveryStaffByMobNo2(mobileNumber);
        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
//TODO ------------------------ storeTrip.getTrips().get(0).getTripStatus() -> CREATED instead of OFD, sda trip : tripResponse1.getTrips().get(0).getTripStatus() is CREATED
        //Validate after creating of Store Bag it becomes OFD
        Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        //Validate Myntra SDA trip is OFD
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        //Validate the Store ML Shipment goes for OFD
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));


            //VAlidate the ORderToShip (Platform ) status is OFD
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            OrderResponse orderResponse = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(track),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.OUT_FOR_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.OUT_FOR_DELIVERY.name());

        }

        //Validate Store Trip tenant and status and code
        //TODO : Check with Azeem why storeTrip.getTrips().get(0).getTripStatus() is returning CREATED instead of OFD
        //   Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        //Get the store summary before delivering the shipments to Customer validate
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateStoreSummaryBeforeDelivering(originPremiseId, storeHlPId, forwardTrackingNumbersList, noOfOrders);

        //Now deliver forwardTrackingNumbersList.get(0) order to customer with PREPAID

        //TODO : ask the payment methods to dev
        //Get the tripOrderId for Store trip
        int count = 0;
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        for (String DlPrepaidTrack : DlPrepaidTrackingList) {
            count++;
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(DlPrepaidTrack)).findFirst().get().getId();

            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);

            //TODO :___________________Validate this trip is in OFD
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (count % 2 == 0)
                //Assert.assertTrue(tripOrderAssignment.getTripOrders().get(0).getTripOrderStatus().name().equals("Out for delivery"));
                tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(DlPrepaidTrack).toString(), tripOrderAssignmentId, storeTenantId, "PRE_PAID",LASTMILE_CONSTANTS.TENANT_ID);
            else
                tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(DlPrepaidTrack).toString(), tripOrderAssignmentId, storeTenantId, "DEBIT_CARD",LASTMILE_CONSTANTS.TENANT_ID);

        }

        //Delver the cash orders
        for (String DlCashTrack : DlCashTrackingList) {
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(DlCashTrack)).findFirst().get().getId();

            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);

            //TODO :___________________Validate this trip is in OFD
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //Assert.assertTrue(tripOrderAssignment.getTripOrders().get(0).getTripOrderStatus().name().equals("Out for delivery"));
            tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(DlCashTrack).toString(), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

            cashAmountToBeCollected = cashAmountToBeCollected + trackingShipmentValue.get(DlCashTrack);
        }
        // Validate                "totalAmountToBeCollected": 0,
       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 1,
                "totalUnAttempted": 1,*/

        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), noOfOrders, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), UnattemptedDLTrackingList.size() + failedDlTrackingList.size());

        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        //Validate Store ML shipment is Delivered

        for (String t : DlPrepaidTrackingList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));


            //VAlidate the ORderToShip (Platform ) status is OFD

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(t),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.DELIVERED), "The platform  orderToship status is not : " + ShipmentStatus.DELIVERED.name());

            lastmileHelper.validateStoreMLEntryDelivered(originPremiseId, storeHlPId, storeTenantId, t);

        }

        for (String t : DlCashTrackingList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(t, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));


            //VAlidate the ORderToShip (Platform ) status is OFD

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(t),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.DELIVERED), "The platform  orderToship status is not : " + ShipmentStatus.DELIVERED.name());

            lastmileHelper.validateStoreMLEntryDelivered(originPremiseId, storeHlPId, storeTenantId, t);
        }


        //Now fail the other  forwardTrackingNumbersList.get(1)/ failedDlTrackingList.get(0)
        //Get the tripOrderAssignment id for the store trip
        tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);

        for (String FDTrack : failedDlTrackingList) {
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(FDTrack)).findFirst().get().getId();
            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);

            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(FDTrack).toString(), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        }
       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 2,
                "totalUnAttempted": 0,   "totalAmountToBeCollected": 0,
*/

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, DlCashTrackingList.size() + DlPrepaidTrackingList.size() + failedDlTrackingList.size(), noOfOrders, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), UnattemptedDLTrackingList.size());
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);

        for (String FDTrack : failedDlTrackingList) {

            lastmileHelper.validateStoreMLEntryFailedDeliveredb4CloseTrip(originPremiseId, storeHlPId, storeTenantId, FDTrack);
        }
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);

        for (String FDTrackNo : failedDlTrackingList) {

            //Validate Store ML shipment is Failed Delivered
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //VAlidate the ORderToShip (Platform ) status is FAILED_DELIVERY

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(FDTrackNo),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.FAILED_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.FAILED_DELIVERY.name());
        }
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Close the Myntra SDA trip with store bag
        TripOrderAssignmentResponse closeMyntraTrip = tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
//Validate if the store trip is closed
        Assert.assertTrue(closeMyntraTrip.getStatus().getStatusType().toString().toLowerCase().contains("success"));

//Try to  close the store  trip, Since there are undelivered items we should not be able to close


        TripOrderAssignmentResponse closeTrip = tripClient_qa.closeStoreTrip(storeTripId, tripOrderIdAttemptReasonMap, storeTenantId);

//Validate that trip cannot be closed since there are Unattempted items
            Assert.assertTrue(closeTrip.getStatus().getStatusType().toString().toLowerCase().contains("error"));
            //   Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().contains("Shipments are in (WFD, OFD) state for this trip"));


        tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);

        //Mark the unattempted orders are Failed and then close
        for (String FDTrack : UnattemptedDLTrackingList) {
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(FDTrack)).findFirst().get().getId();
            tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);

            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(FDTrack).toString(), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        }


        //Now validate that the shipments that were unattempted are now FD


        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);

        for (String FDTrackNo : UnattemptedDLTrackingList) {

            //Validate Store ML shipment is Failed Delivered
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //Validate INstakart ML shipment is Delivered
            mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //VAlidate the ORderToShip (Platform ) status is FAILED_DELIVERY

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(FDTrackNo),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.FAILED_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.FAILED_DELIVERY.name());
        }

//Now the total failed and Unattempted are marked as Failed so the total failed is failedUnattemptedFinalList
     /*   failedUnattemptedFinalList.addAll(failedDlTrackingList);
        failedUnattemptedFinalList.addAll(failedUnattemptedFinalList);*/

        failedUnattemptedFinalList = ListUtils.union(failedDlTrackingList, UnattemptedDLTrackingList);


        //We cannot close the trip without receiving shipments
        if (closeTrip.getStatus().getStatusType().toString().toLowerCase().contains("error")) {
            Assert.assertTrue(closeTrip.getStatus().getStatusType().toString().toLowerCase().contains("error"));
            //   Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().contains("Shipments are in (WFD, OFD) state for this trip"));
        }

        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");

//Validate store summary before creating reverse bag
        //Now total attemped will have the earlier unattemped ones which were later marked FD
        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, DlCashTrackingList.size() + DlPrepaidTrackingList.size() + failedUnattemptedFinalList.size(), noOfOrders, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), 0);

        // lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, 2, noOfOrders, 1, 0);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, cashAmountToBeCollected);

//TODO : gloria even before receiving it is now FAILED_DELIVERY


        for (String FDTrackNo : failedUnattemptedFinalList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

        }


//Create Trip
        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + failedDlTrackingList.get(0) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + failedDlTrackingList.get(0) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the amount collected for CASH shipments + " + DlCashTrackingList.toString() + " , is : Rs . " + cashAmountToBeCollected + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the PREPAID shipments + " + DlPrepaidTrackingList.toString() + " \n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the Unattempted and Failed  shipments are + " + failedUnattemptedFinalList.toString() + "\n\n\n ");
        //Validate the reverse bag contents displayed in love :http://lastmile.scmqa.myntra.com/lastmile-service/trip/v2/findShipmentsByTripNumber/Automation1-181120155306433?shipmentType=REVERSE_BAG&tenantId=4019
      /*  "shipmentId": 21221343,
                "shipmentItemCount": 1,
                "shipmentType": "REVERSE_BAG",
                "staffPhoneNumber": 1511690820,
                "status": "WFD",
                "storeDCId": 9235,
                "totalFailedDeliveriesPicked": 0,
                "totalFailedDeliveriesToPick": 1,
                "totalSuccess": 1,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,
                "totalUnAttempted": 0,
                "tripId": 59046*/

        TripShipmentAssociationResponse reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(reverseTripNumber, ShipmentType.REVERSE_BAG, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(reverseBagId));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStoreDCId().equals(storeHlPId));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getMyntraSDATripNumber().equals(reverseTripNumber));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue((reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getReverseBagExpectedShipmentTrackingNumbers().containsAll(forwardTrackingNumbersList)));

        //We have 1 failed delivery
        /* "shipmentItemCount": 1,*/

        lastmileHelper.validateReverseBagShipmentItemCnt(reverseTripNumber, LMS_CONSTANTS.TENANTID, failedUnattemptedFinalList.size());
        //Validate the amount To be collected is 0.0 and amount collected is 0.0
   /*     "amountCollected": 0,
                "amountToBeCollected": 0,*/
        lastmileHelper.validateReverseBagAmount(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0.0f, cashAmountToBeCollected);
   /*     "totalFailedDeliveriesPicked": 0,
                "totalFailedDeliveriesToPick": 1,*/
        lastmileHelper.validateReverseFailedDeliveries(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0, failedUnattemptedFinalList.size());
      /*  "totalSuccess": 1,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,*/
        lastmileHelper.validateReverseSuccess(reverseTripNumber, LMS_CONSTANTS.TENANTID, DlCashTrackingList.size() + DlPrepaidTrackingList.size(), 0, 0);
//        "totalUnAttempted": 0,
        lastmileHelper.validateReverseUnAttempted(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0);
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the amount collected for CASH shipments + " + DlCashTrackingList.toString() + " , is : Rs . " + cashAmountToBeCollected + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the PREPAID shipments + " + DlPrepaidTrackingList.toString() + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the Unattempted and Failed  shipments are + " + failedUnattemptedFinalList.toString() + "\n\n\n ");


        //Validate the reverse MB is ADDED_TO_TRIP and there are no shipments present in ShipmentOrderMap
        //shipmentOrderMap has NO entry since we have not delivered the items
        lastmileHelper.validateForReverseMbAddedToTrip(Long.valueOf(reverseBagId));

        //Start the reverse trip

        tripClient_qa.startTrip(reverseTripId);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickip
        // Pickup the reverse bag

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupMensaOldReverseBagFromStore(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, failedUnattemptedFinalList.size(), 0, cashAmountToBeCollected,LMS_CONSTANTS.TENANTID);

        Assert.assertTrue(reverseShipmentResponse.getStatus().getStatusType().toString().equals("ERROR"));
        Assert.fail("Error while delivering the Store bag to the Store . Reason : " + reverseShipmentResponse.getStatus().getStatusMessage());

        // reverse bag should be IN_TRANSIT after pickup
        //Shipment orderMap will now have these entries ONLY which has been scanned
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateForReverseMbEntriesAfterPickup(Long.valueOf(reverseBagId), failedUnattemptedFinalList);

        //Store Summary should not have any data since everything is reconcilied
        lastmileHelper.validateStoreSummaryAfterReconcilation(originPremiseId, storeHlPId);
       /* "amountCollected": 0,
                "amountToBeCollected": 0,
                "attemptReasonCode": "PICKED_UP_SUCCESSFULLY",
                "deliveryStaffName": "LA9566 - LA_FN9566 LA_LN9566",
                "itemsPickedUp": 0,
                "myntraSDATripNumber": "AAAA-181120201411814",
                "shipmentId": 21221594,
                "shipmentItemCount": 1,
                "shipmentType": "REVERSE_BAG",
                "staffPhoneNumber": 1733995666,
                "status": "PS",
                "totalFailedDeliveriesPicked": 1,
                "totalFailedDeliveriesToPick": 1,
                "totalSuccess": 0,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,
                "totalUnAttempted": 0,*/


        reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(reverseTripNumber, ShipmentType.REVERSE_BAG, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getAttemptReasonCode().name().equals(AttemptReasonCode.PICKED_UP_SUCCESSFULLY.name()));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.PS.name()));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getMyntraSDATripNumber().equals(reverseTripNumber));
        //Validate the amount To be collected is 0.0 and amount collected is 0.0
   /*     "amountCollected": 0,
                "amountToBeCollected": 0,*/
        lastmileHelper.validateReverseBagAmount(reverseTripNumber, LMS_CONSTANTS.TENANTID, cashAmountToBeCollected, cashAmountToBeCollected);
   /*     "totalFailedDeliveriesPicked": 1,
                "totalFailedDeliveriesToPick": 1,*/
        lastmileHelper.validateReverseFailedDeliveries(reverseTripNumber, LMS_CONSTANTS.TENANTID, failedUnattemptedFinalList.size(), failedUnattemptedFinalList.size());
      /*  "totalSuccess": 0,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,*/
        //TODO : itemsPickedUp and totalSuccess are NOT used
        lastmileHelper.validateReverseSuccess(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0, 0, 0);
//        "totalUnAttempted": 0,
        lastmileHelper.validateReverseUnAttempted(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0);
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the amount collected for CASH shipments + " + DlCashTrackingList.toString() + " , is : Rs . " + cashAmountToBeCollected + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the PREPAID shipments + " + DlPrepaidTrackingList.toString() + "\n\n\n ");
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + " and the Unattempted and Failed  shipments are + " + failedUnattemptedFinalList.toString() + "\n\n\n ");

        //Close trip
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, reverseTripId);
        //Validate Unattempted Shipment response has nothing (same as store summary)

        lastmileHelper.validateUnattemptedAfterReconcilation(storeHlPId);

        //TODO : Receive Shipments
        // Now close the store trip since all are marked FD/DL
        for (String track : failedUnattemptedFinalList) {
            TripOrderAssignmentResponse receiveTripOrder = tripOrderAssignmentClient_qa.receiveTripShipments(track, LMS_CONSTANTS.TENANTID);

            Assert.assertTrue(receiveTripOrder.getTripOrders().get(0).getIsReceived());
        }
        TripOrderAssignmentResponse closeTripSUcess = tripClient_qa.closeStoreTrip(storeTripId, tripOrderIdAttemptReasonMap, storeTenantId);
        //Store trip can be closed only after we receive all shipments
//Validate that trip is be closed
        Assert.assertTrue(closeTripSUcess.getStatus().getStatusType().toString().toLowerCase().contains("success"));

        //After closing the trip the failedDelivered item ,status MLResponse for store entry is RECEIVED_IN_DC
//Now even the unattemped were later marked FD
        for (String FDTrackNo : UnattemptedDLTrackingList) {

            lastmileHelper.validateStoreMLEntryFailedDeliveredAfterCloseTrip(storeTenantId, FDTrackNo);
        }
//Now even the unattemped were later marked FD
        for (String FDTrackNo : failedDlTrackingList) {

            lastmileHelper.validateStoreMLEntryFailedDeliveredAfterCloseTrip(storeTenantId, FDTrackNo);
        }

//TODO : gloria AFTER receiving it is now FAILED_DELIVERY


        for (String FDTrackNo : failedUnattemptedFinalList) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(FDTrackNo, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.RECEIVED_IN_DC.name()));

        }


    }


}
