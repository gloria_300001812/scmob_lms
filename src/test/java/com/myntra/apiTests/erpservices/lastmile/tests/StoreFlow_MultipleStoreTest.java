package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.CommonUtils;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_StoreFlowsDP;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.response.TransporterResponse;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import com.myntra.logistics.platform.domain.TryAndBuyItemStatus;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.tms.container.ContainerEntry;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;
import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class StoreFlow_MultipleStoreTest {

    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LastmileClient lastmileClient = new LastmileClient();
    private LMS_CreateOrder lms_createOrder=new LMS_CreateOrder();
    TripClient_QA tripClient_qa=new TripClient_QA();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    private LastmileHelper lastmileHelper;
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa=new MLShipmentClientV2_QA();
    TMSServiceHelper tmsServiceHelper=new TMSServiceHelper();
    MasterBagServiceHelper masterBagServiceHelper=new MasterBagServiceHelper();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    static String searchParams;
    static Long storeHlPId;
    static Long storeSDAId;
    static String storeTenantId;
    static String tenantId;
    static String pincode;
    static String mobileNumber;
    static String storeCourierCode;
    static String code;

    @BeforeTest
    public void createStore(){
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        String value = CommonUtils.generateString(3);
        String name=value + String.valueOf(contactNumber).substring(5,9);
        String ownerFirstName= value + String.valueOf(contactNumber).substring(5,9);
        String ownerLastName= value + String.valueOf(contactNumber).substring(5,9);
        code=value + String.valueOf(contactNumber).substring(5,9);
        String latLong="LA_latlong" + String.valueOf(contactNumber).substring(5,9);
        String emailId="test@lastmileAutomation.com";
        mobileNumber=String.valueOf(contactNumber);
        String  address="Automation Address";
        pincode= LASTMILE_CONSTANTS.STORE_PINCODE;
        String city="Bangalore";
        String state="Karnataka";
        String mappedDcCode= LASTMILE_CONSTANTS.ROLLING_DC_CODE;
        String gstin="LA_gstin" + String.valueOf(contactNumber).substring(5,9);
        tenantId=LMS_CONSTANTS.TENANTID;

        searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId = deliveryStaffEntry.getId();
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

    }

    @Test(priority = 1,enabled = true,dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows",description = "If there are pending shipments, next day we should not be able to add store bag for Ex , then create a reverse bag and pick them up and " +
            "then try to reassign these pending shipments again to the same store and then again mark as FP and Unattempted,  next day we should not be able to add store bag and then again create a reverse bag and pick them up and then try to reassign these pending " +
            "shipments again to the SAME store and mark then DL")
    public void process_AssignEXOrderToStoreWithoutReconcile_DeliverEXOrderThroughSameStore_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                                                                       String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        String searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId = deliveryStaffEntry.getId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        String storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(exchangeTrackingNumber, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(exchangeTrackingNumber, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse1, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        MLShipmentResponse mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(exchangeTrackingNumber, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse2, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), com.myntra.lastmile.client.code.AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED

        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId, MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId,com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId, ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //update the trip order status as FD
        String sourceReturnId=null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(storeTripId, storeTenantId);
        if (pickupTripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getId();
            String exchangeOrderId1 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getExchangeOrderId();

            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId, EnumSCM.FAILED, EnumSCM.UPDATE, exchangeOrderId1, storeTripId, orderEntry,storeTenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
            sourceReturnId=tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(storeTripId, storeTenantId, com.myntra.lastmile.client.status.TripOrderStatus.FP.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);

        //validate return status in rms
        ReturnResponse returnResponse=rmsServiceHelper.getReturnDetailsNew(sourceReturnId);
        storeValidator.validateReturnOrderStatusInRMS(returnResponse, ReturnStatus.LPI);
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId1 = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID1));
        String exchangePacketId1 = omsServiceHelper.getPacketId(exchangeOrderId1);
        String exchangeTrackingNumber1 = lmsHelper.getTrackingNumber(exchangePacketId1);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create MasterBag
        ShipmentResponse shipmentResponse01 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = shipmentResponse01.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, exchangeTrackingNumber1, null, null, null, null, null, null, false).build();
        String response1 = storeHelper.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        Assert.assertTrue(response1.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse2 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry = tripResponse2.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId), ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());
        statusPollingValidator.validateTripStatus(reverseTripId,EnumSCM.OUT_FOR_DELIVERY);
        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(exchangeTrackingNumber);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), com.myntra.lastmile.client.code.AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(exchangeTrackingNumber);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4=tripClient_qa.closeMyntraTripWithStoreBag(tenantId,reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lmsServiceHelper.requeueOrder(exchangePacketId);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //Create MasterBag
        ShipmentResponse shipmentResponse6 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId2 = shipmentResponse6.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId2));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo2 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId2), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response2 = masterBagClient_qa.addShipmentToStoreBag(masterBagId2, shipmentUpdateInfo2);
        if (response2.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status
        MLShipmentResponse mlShipmentResponse21 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(exchangeTrackingNumber, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse21, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId2, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse6 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse6.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse6.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse6, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse3 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId1), String.valueOf(masterBagId2),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse3.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus3 = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId2);
        Assert.assertTrue(shipmentOrderMapStatus3.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse7 = tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //validate ML shipment status
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        statusPollingValidator.validateTripStatus(tripId1,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers1 = new ArrayList();
        listTrackingNumbers1.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse4 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId2), listTrackingNumbers1, String.valueOf(tripId1), com.myntra.lastmile.client.code.AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse4.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId2)));
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, ShipmentStatus.DELIVERED.toString());

        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId1);
        statusPollingValidator.validateTripStatus(tripId1,EnumSCM.COMPLETED);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse7 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers1, storeSDAId);
        Assert.assertTrue(tripResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
        Long storeTripId1 = tripResponse7.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId1.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId1,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());
        //update the trip order status as FD
        String sourceReturnId1=null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse1 = tripClient_qa.findExchangesByTrip(storeTripId1, storeTenantId);
        if (pickupTripOrderAssignmentResponse1.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId1 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getId();
            String exchangeOrderId2 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getExchangeOrderId();


            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId1, EnumSCM.FAILED, EnumSCM.UPDATE, exchangeOrderId2, storeTripId1, orderEntry,storeTenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            sourceReturnId1=tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(storeTripId1, storeTenantId, com.myntra.lastmile.client.status.TripOrderStatus.FP.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);

        //validate return status in rms
        ReturnResponse returnResponse4=rmsServiceHelper.getReturnDetailsNew(sourceReturnId1);
        storeValidator.validateReturnOrderStatusInRMS(returnResponse4,ReturnStatus.LPI);
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId1.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create MasterBag
        ShipmentResponse shipmentResponse11 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId3 = shipmentResponse11.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId3));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId3, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo3 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId3), ShipmentType.DL, exchangeTrackingNumber1, null, null, null, null, null, null, false).build();
        String response3 = storeHelper.addShipmentToStoreBag(masterBagId3, shipmentUpdateInfo3);
        Assert.assertTrue(response3.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //Create Reverse Trip
        Long dcSDAId1 = null;
        DeliveryStaffResponse deliveryStaffResponse1 = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry1 = deliveryStaffResponse1.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry1.size() == 0)) {
            dcSDAId1 = deliveryStaffResponse1.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId1));

        TripResponse tripResponse9 = tripClient_qa.createTrip(originPremiseId, dcSDAId1);
        Assert.assertTrue(tripResponse9.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry1 = tripResponse9.getTrips();
        Long reverseTripId1 = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId1 = lstTripEntry1.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId1));
        statusPollingValidator.validateTripStatus(reverseTripId1, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId1 = tripClient_qa.assignReverseBagToTrip(reverseTripId1, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId1));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId1), ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = tripClient_qa.startTrip(reverseTripId1);
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber1 = new ArrayList<>();
        lstreturnTrackingNumber1.add(exchangeTrackingNumber);
        TripShipmentAssociationResponse tripShipmentAssociationResponse5 = storeHelper.pickupReverseBagFromStore(reverseShipmentId1, lstreturnTrackingNumber1, String.valueOf(reverseTripId1), com.myntra.lastmile.client.code.AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(exchangeTrackingNumber);
        String receiveInDc2 = storeHelper.receiveShipmentInDC(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()), tenantId);
        Assert.assertTrue(receiveInDc2.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,EnumSCM.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse8=tripClient_qa.closeMyntraTripWithStoreBag(tenantId,reverseTripId1);
        Assert.assertTrue(tripOrderAssignmentResponse8.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse9 = lmsServiceHelper.requeueOrder(exchangePacketId);
        Assert.assertTrue(tripOrderAssignmentResponse9.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //Create MasterBag
        ShipmentResponse shipmentResponse13 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse13.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId4 = shipmentResponse13.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId4));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo4 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId4), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response4 = masterBagClient_qa.addShipmentToStoreBag(masterBagId4, shipmentUpdateInfo4);
        if (response4.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus5 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId4, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus5.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID2 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse12 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID2));
        Assert.assertTrue(tripResponse12.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId3 = tripResponse12.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId3));
        Assert.assertTrue(tripResponse12.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse12, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse6 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId3), String.valueOf(masterBagId4),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse6.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId4, ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus4 = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId4);
        Assert.assertTrue(shipmentOrderMapStatus4.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse11 = tripClient_qa.startTrip(tripId3);
        Assert.assertTrue(tripOrderAssignmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        MLShipmentResponse mlShipmentResponse30 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(exchangeTrackingNumber, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse30, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        statusPollingValidator.validateTripStatus(tripId3,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers2 = new ArrayList();
        listTrackingNumbers2.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse7 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId4), listTrackingNumbers2, String.valueOf(tripId3), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse7.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId4)));
        Assert.assertTrue(tripShipmentAssociationResponse7.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse7.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId4, ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4, ShipmentStatus.DELIVERED.toString());
        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId3);
        statusPollingValidator.validateTripStatus(tripId3,EnumSCM.COMPLETED);
        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse13 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers2, storeSDAId);
        Assert.assertTrue(tripResponse13.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
        Long storeTripId2 = tripResponse13.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId2.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId2,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //update the trip order status as deliveres and get the pickup exchange order details
        String sourceReturnId2=null;
        String pickupTrackingNumber=null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse3 = tripClient_qa.findExchangesByTrip(storeTripId2, storeTenantId);
        if (pickupTripOrderAssignmentResponse3.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId = pickupTripOrderAssignmentResponse3.getTripOrders().get(0).getId();
            String exchangeOrderId2 = pickupTripOrderAssignmentResponse3.getTripOrders().get(0).getExchangeOrderId();

            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, exchangeOrderId2, storeTripId2, orderEntry,storeTenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
            pickupTrackingNumber=tripOrderAssignmentResponse0.getTripOrders().get(0).getTrackingNumber();
            sourceReturnId2=tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(storeTripId2, storeTenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId2, ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId2, tenantId, Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());

        //CreateMB from DC To WH
        String deliveryCenterName="ELC";
        String returnHub= "RT-BLR";
        MasterbagDomain shipmentResponse7 = masterBagServiceHelper.createMasterBag(deliveryCenterName,returnHub, ShippingMethod.NORMAL,"ML",tenantId);
        Long reverseMBId=shipmentResponse7.getId();
        storeValidator.isNull(String.valueOf(reverseMBId));
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.NEW.toString());
        //Add order to MB
        masterBagServiceHelper.addShipmentsToMasterBag(reverseMBId,pickupTrackingNumber,ShipmentType.PU,tenantId);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.NEW.toString());
        //close Reverse MB
        masterBagServiceHelper.closeMasterBag(reverseMBId,tenantId);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());

        //Receive MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(deliveryCenterName, reverseMBId, tenantId);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse=storeHelper.getMappedTransportHubDetails(tenantId,deliveryCenterName);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub=hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterName,transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(deliveryCenterName, transporterHub, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, reverseMBId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());
        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());

        //Receive Container In Transport Hub
        ContainerResponse containerResponse3 = (ContainerResponse) storeHelper.receiveContainerInTransportHub.apply(containerId, transporterHub);
        Assert.assertTrue(containerResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Container is not received in TRansport hub");

        //MasterBag inscan
        String mbInscanResponse = null;
        storeHelper.masterBagInscanProcess(pickupTrackingNumber, reverseMBId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);

        //TODO calling this API twice, since it fails 1st time.
        mbInscanResponse = storeHelper.masterBagInscanProcess(pickupTrackingNumber, reverseMBId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
        Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success)," Master bag inscan failed in WH");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId2, ReturnStatus.RRC.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId2,tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.DELIVERED_TO_SELLER.toString());
        //validate pickup shipment staus
        statusPollingValidator.validatePickupShipmentStatus(sourceReturnId2,LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),EnumSCM.PICKUP_SUCCESSFUL);

    }


    @Test(priority = 2,enabled = true, dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows2",description = "If there are pending shipments, next day we should not be able to add store bag for Ex then create a reverse bag and pick them up and then try to reassign these pending shipments again to the same store and then again mark as FP and Unattempted,  next day we should not be able to add store bag and then again create a reverse bag and pick them up and then try to reassign these pending shipments again to the DIFFERENT store and mark then DL")
    public void process_AssignEXOrderToStoreWithoutReconcile_DeliverEXOrderThroughDIFFERENTStore_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                                                                            String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code,
                                                                                                            String name1, String ownerFirstName1, String ownerLastName1, String mobileNumber1, String code1) throws Exception {
        String searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId = deliveryStaffEntry.getId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        String storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), com.myntra.lastmile.client.code.AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,EnumSCM.EXPECTED_IN_DC);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());

        //complete DC trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
         Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId,com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId, ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //update the trip order status as FD
        String sourceReturnId=null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(storeTripId, storeTenantId);
        if (pickupTripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getId();
            String exchangeOrderId1 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getExchangeOrderId();


            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId, EnumSCM.FAILED, EnumSCM.UPDATE, exchangeOrderId1, storeTripId, orderEntry,storeTenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            sourceReturnId=tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(storeTripId,storeTenantId,com.myntra.lastmile.client.status.TripOrderStatus.FP.toString());

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        ReturnResponse returnResponse=rmsServiceHelper.getReturnDetailsNew(sourceReturnId);
        storeValidator.validateReturnOrderStatusInRMS(returnResponse, ReturnStatus.LPI);
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId,LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId1 = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID1));
        String exchangePacketId1 = omsServiceHelper.getPacketId(exchangeOrderId1);
        String exchangeTrackingNumber1 = lmsHelper.getTrackingNumber(exchangePacketId1);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create MasterBag
        ShipmentResponse shipmentResponse01 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = shipmentResponse01.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, exchangeTrackingNumber1, null, null, null, null, null, null, false).build();
        String response1 = storeHelper.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        Assert.assertTrue(response1.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse2 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry = tripResponse2.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId), ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        TripResponse tripResponse4=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId));
        storeValidator.validateTripStatus(tripResponse4, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(exchangeTrackingNumber);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), com.myntra.lastmile.client.code.AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(exchangeTrackingNumber);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4=tripClient_qa.closeMyntraTripWithStoreBag(tenantId,reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        TripResponse tripResponse5=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId));
        storeValidator.validateTripStatus(tripResponse5, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED);

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lmsServiceHelper.requeueOrder(exchangePacketId);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //validate OrderToShip status
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //Create MasterBag
        ShipmentResponse shipmentResponse6 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId2 = shipmentResponse6.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId2));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo2 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId2), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response2 = masterBagClient_qa.addShipmentToStoreBag(masterBagId2, shipmentUpdateInfo2);
        if (response2.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId2, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse6 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse6.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse6.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse6, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse3 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId1), String.valueOf(masterBagId2),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse3.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status
        MLShipmentResponse mlShipmentResponse22 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(exchangeTrackingNumber, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse22, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus3 = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId2);
        Assert.assertTrue(shipmentOrderMapStatus3.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse7 = tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Deliver the MB
        List<String> listTrackingNumbers1 = new ArrayList();
        listTrackingNumbers1.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse4 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId2), listTrackingNumbers1, String.valueOf(tripId1), com.myntra.lastmile.client.code.AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse4.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId2)));
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, ShipmentStatus.DELIVERED.toString());

        //Assign order to store SDA
        TripResponse tripResponse7 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers1, storeSDAId);
        Assert.assertTrue(tripResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId1 = tripResponse7.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId1.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId1,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //update the trip order status as FD
        String sourceReturnId1=null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse1 = tripClient_qa.findExchangesByTrip(storeTripId1, storeTenantId);
        if (pickupTripOrderAssignmentResponse1.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId1 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getId();
            String exchangeOrderId2 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getExchangeOrderId();


            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId1, EnumSCM.FAILED, EnumSCM.UPDATE, exchangeOrderId2, storeTripId1, orderEntry,storeTenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            sourceReturnId1=tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(storeTripId1,storeTenantId,com.myntra.lastmile.client.status.TripOrderStatus.FP.toString());

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //validate return status in rms
        ReturnResponse returnResponse4=rmsServiceHelper.getReturnDetailsNew(sourceReturnId1);
        storeValidator.validateReturnOrderStatusInRMS(returnResponse4,ReturnStatus.LPI);
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId1,LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create MasterBag
        ShipmentResponse shipmentResponse11 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId3 = shipmentResponse11.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId3));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId3, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo3 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId3), ShipmentType.DL, exchangeTrackingNumber1, null, null, null, null, null, null, false).build();
        String response3 = storeHelper.addShipmentToStoreBag(masterBagId3, shipmentUpdateInfo3);
        Assert.assertTrue(response3.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //Create Reverse Trip
        Long dcSDAId1 = null;
        DeliveryStaffResponse deliveryStaffResponse1 = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry1 = deliveryStaffResponse1.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry1.size() == 0)) {
            dcSDAId1 = deliveryStaffResponse1.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId1));

        TripResponse tripResponse9 = tripClient_qa.createTrip(originPremiseId, dcSDAId1);
        Assert.assertTrue(tripResponse9.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry1 = tripResponse9.getTrips();
        Long reverseTripId1 = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId1 = lstTripEntry1.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId1));
        TripResponse tripResponse10=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId1));
        storeValidator.validateTripStatus(tripResponse10, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //create reverse MB
        String reverseShipmentId1 = tripClient_qa.assignReverseBagToTrip(reverseTripId1, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId1));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId1), ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = tripClient_qa.startTrip(reverseTripId1);
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        TripResponse tripResponse8=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId1));
        storeValidator.validateTripStatus(tripResponse8, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber1 = new ArrayList<>();
        lstreturnTrackingNumber1.add(exchangeTrackingNumber);
        TripShipmentAssociationResponse tripShipmentAssociationResponse5 = storeHelper.pickupReverseBagFromStore(reverseShipmentId1, lstreturnTrackingNumber1, String.valueOf(reverseTripId1), com.myntra.lastmile.client.code.AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(exchangeTrackingNumber);
        String receiveInDc2 = storeHelper.receiveShipmentInDC(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()), tenantId);
        Assert.assertTrue(receiveInDc2.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse8=tripClient_qa.closeMyntraTripWithStoreBag(tenantId,reverseTripId1);
        Assert.assertTrue(tripOrderAssignmentResponse8.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        TripResponse tripResponse11=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId1));
        storeValidator.validateTripStatus(tripResponse11, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED);

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse9 = lmsServiceHelper.requeueOrder(exchangePacketId);
        Assert.assertTrue(tripOrderAssignmentResponse9.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //TODO assign the ex order to different store
        String searchParams1 = "code.like:" + code1;
        //Create Store
        StoreResponse storeResponse1 = lastmileHelper.createStore(name1, ownerFirstName1, ownerLastName1, latLong, emailId, mobileNumber1, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code1);
        //validate Store created successfully
        Assert.assertTrue(storeResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        Long storeHlPId1 = storeResponse1.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry1 = storeResponse1.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId1 = deliveryStaffEntry1.getId();
        String storeTenantId1 = storeResponse1.getStoreEntries().get(0).getTenantId();

        //Create MasterBag
        String originDCCity1 = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse3 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams1, "");
        String destinationStoreCity1 = storeResponse3.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse13 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity1, destinationStoreCity1);
        Assert.assertTrue(shipmentResponse13.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId4 = shipmentResponse13.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId4));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo4 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId4), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response4 = masterBagClient_qa.addShipmentToStoreBag(masterBagId4, shipmentUpdateInfo4);
        if (response4.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus5 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId4, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus5.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID2 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse12 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID2));
        Assert.assertTrue(tripResponse12.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId3 = tripResponse12.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId3));
        Assert.assertTrue(tripResponse12.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse12, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse6 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId3), String.valueOf(masterBagId4),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse6.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus4 = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId4);
        Assert.assertTrue(shipmentOrderMapStatus4.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse11 = tripClient_qa.startTrip(tripId3);
        Assert.assertTrue(tripOrderAssignmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Deliver the MB
        List<String> listTrackingNumbers2 = new ArrayList();
        listTrackingNumbers2.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse7 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId4), listTrackingNumbers2, String.valueOf(tripId3), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse7.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId4)));
        Assert.assertTrue(tripShipmentAssociationResponse7.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse7.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId1,EnumSCM.EXPECTED_IN_DC);

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4, ShipmentStatus.DELIVERED.toString());

        //Assign order to store SDA
        TripResponse tripResponse13 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers2, storeSDAId1);
        Assert.assertTrue(tripResponse13.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
       Long storeTripId2 = tripResponse13.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId2.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId2,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId1,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //update the trip order status as deliveres and get the pickup exchange order details
        String sourceReturnId2=null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse3 = tripClient_qa.findExchangesByTrip(storeTripId2, storeTenantId1);
        if (pickupTripOrderAssignmentResponse3.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId = pickupTripOrderAssignmentResponse3.getTripOrders().get(0).getId();
            String exchangeOrderId2 = pickupTripOrderAssignmentResponse3.getTripOrders().get(0).getExchangeOrderId();

            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, exchangeOrderId2, storeTripId2, orderEntry,storeTenantId1,Long.parseLong(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            sourceReturnId2=tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(storeTripId2, storeTenantId1, com.myntra.lastmile.client.status.TripOrderStatus.PS.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId2,ReturnStatus.RL.toString());

        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId2,tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());

    }

    @Test( priority = 3, enabled = true, description = "Create a Store and create and assign order to store,make order status as Failed_Delivered ,Reque the order from DC and delivered order with item status as NotTriedBought")
    public void processNotTriedBoughtOrderSuccessful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.EXPECTED_IN_DC);
        //complete DC trip
        storeHelper.closeMyntraTripWithStoreBag(tenantId,tripId);
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);
        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId,com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.FAILED_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //receive the failed delivery order in DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(storeTenantId,storeTripId);
        statusPollingValidator.validateTripStatus(storeTripId,EnumSCM.COMPLETED);

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = lmsServiceHelper.requeueOrder(packetId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.UNASSIGNED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //create mastre bag
        ShipmentResponse shipmentResponse5 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");

        Long masterBagId1 = shipmentResponse5.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response1 = masterBagClient_qa.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        if (response1.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId1, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse10 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Long tripId1 = tripResponse10.getTrips().get(0).getId();
        Assert.assertTrue(tripResponse10.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId1), String.valueOf(masterBagId1),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Store bag is not added to a trip");
        Assert.assertTrue(tripShipmentAssociationResponse2.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId1)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse2.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId1)), " The trip Id is not matching ");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        statusPollingValidator.validateTripStatus(tripId1,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse5 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId1), listTrackingNumbers, String.valueOf(tripId1), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse5.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId1)));
        Assert.assertTrue(tripShipmentAssociationResponse5.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse5.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.EXPECTED_IN_DC);
        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(tenantId,tripId1);
        statusPollingValidator.validateTripStatus(tripId1,EnumSCM.COMPLETED);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse3 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate after creating of Store Bag it becomes OFD
        Long storeTripId1 = tripResponse3.getTrips().get(0).getId();
        statusPollingValidator.validateTripStatusWithVNM(storeTripId1,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        MLShipmentResponse mlShipmentResponse25 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse25,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        Long dcId = mlShipmentResponse25.getMlShipmentEntries().get(0).getDeliveryCenterId();
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //update the trip with status as Tried_And_Bought
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId1 = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));
        OrderResponse orderResponse6 = mlShipmentClientV2_qa.getTryAndBuyItems(storeTenantId, trackingNo);
        Long itemEntriesId = orderResponse6.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        storeValidator.isNull(String.valueOf(itemEntriesId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = storeHelper.updateTryAndBuyOrders(tripOrderAssignmentId1, AttemptReasonCode.DELIVERED, com.myntra.lastmile.client.code.utils.TripAction.UPDATE, packetId, dcId, amount,
                itemEntriesId, storeTenantId, TryAndBuyItemStatus.NOT_TRIED, storeTripId1);
        Assert.assertEquals(tripOrderAssignmentResponse6.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.DELIVERED);

    }

}
