package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.CommonUtils;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import com.myntra.logistics.platform.domain.TryAndBuyItemStatus;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class StoreFlowSanity {

    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    TripClient_QA tripClient_qa = new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa = new DeliveryCenterClient_QA();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa = new LMSOrderDetailsClient_QA();
    StoreClient_QA storeClient_qa = new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa = new MasterBagClient_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa = new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa = new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa = new MLShipmentClientV2_QA();
    ReturnHelper returnHelper = new ReturnHelper();
    LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private LastmileHelper lastmileHelper;
    LastmileClient lastmileClient=new LastmileClient();

    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    static String searchParams;
    static Long storeHlPId;
    static Long storeSDAId;
    static String storeTenantId;
    static String tenantId;
    static String pincode;
    static String mobileNumber;
    static String storeCourierCode;
    static String code;


    @BeforeTest
    public void createStore() {
        Random r = new Random(System.currentTimeMillis());
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        String value = CommonUtils.generateString(3);

        String name = value + String.valueOf(contactNumber1).substring(5, 9);
        String ownerFirstName = value + String.valueOf(contactNumber1).substring(5, 9);
        String ownerLastName = value + String.valueOf(contactNumber1).substring(5, 9);
        code = value + String.valueOf(contactNumber1).substring(5, 9);
        String latLong = "LA_latlong" + String.valueOf(contactNumber1).substring(5, 9);
        String emailId = "test@lastmileAutomation.com";
        mobileNumber = String.valueOf(contactNumber1);
        String address = "Automation Address";
        pincode = LASTMILE_CONSTANTS.STORE_PINCODE;
        String city = "Bangalore";
        String state = "Karnataka";
        String mappedDcCode = LASTMILE_CONSTANTS.ROLLING_DC_CODE;
        String gstin = "LA_gstin" + String.valueOf(contactNumber1).substring(5, 9);
        tenantId = LMS_CONSTANTS.TENANTID;


        searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId = deliveryStaffEntry.getId();
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeCourierCode = storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

    }


    @Test(enabled = true, description = "TC ID:C28089 ,create tryAndBuy shipment till SH and deliver it to customer through EOD store flow")
    public void process_TriedAndBoughtOrders_Successful() throws Exception {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));
        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.EXPECTED_IN_DC);
        //validate mb status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());

        //close myntra SDA trip
        TripOrderAssignmentResponse closeMyntraSDATrip = storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        Assert.assertTrue(closeMyntraSDATrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlshipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.OUT_FOR_DELIVERY);

        //update the trip with status as Tried_And_Bought
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));
        OrderResponse orderResponse6 = mlShipmentClientV2_qa.getTryAndBuyItems(storeTenantId, trackingNo);
        Long itemEntriesId = orderResponse6.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        storeValidator.isNull(String.valueOf(itemEntriesId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = storeHelper.updateTryAndBuyOrders(tripOrderAssignmentId, AttemptReasonCode.DELIVERED, com.myntra.lastmile.client.code.utils.TripAction.UPDATE, packetId, originPremiseId, amount,
                itemEntriesId, storeTenantId, TryAndBuyItemStatus.TRIED_AND_BOUGHT, storeTripId);
        Assert.assertEquals(tripOrderAssignmentResponse6.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.DELIVERED);
        //close myntra SDA trip
        TripOrderAssignmentResponse closeMyntraStoreTrip = storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,storeTripId);
        Assert.assertTrue(closeMyntraStoreTrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(storeTripId,EnumSCM.COMPLETED);
    }

    @Test(enabled = true, description = "TC ID: C28088 & C28362,create forward shipment till SH and deliver it to customer through EOD store flow")
    public void process_DeliverShipmentThroughStore_Successsful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.EXPECTED_IN_DC);

        TripShipmentAssociationResponse tripShipmentDetails = tripClient_qa.getReverseShipmentBagDetails(tripNumber, ShipmentType.FORWARD_BAG, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripShipmentDetails.getStatus().getStatusMessage(), "Success", "trip shipment details is not retrieved");
        Assert.assertEquals(tripShipmentDetails.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getMasterbagShipmentAssociationStatus().toString(), ShipmentStatus.DELIVERED.toString(), "Shipment item status is not expected");
        Assert.assertEquals(tripShipmentDetails.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getShipmentType().toString(), ShipmentType.DL.toString(), "Shipment Type for items is not expected");
        Assert.assertEquals(tripShipmentDetails.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.FORWARD_BAG.toString(), "Shipment type is not expected");
        Assert.assertEquals(tripShipmentDetails.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), ShipmentType.DL.toString(), "Shipment status is not expected");

        //close myntra SDA trip
        TripOrderAssignmentResponse closeMyntraSDATrip = storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        Assert.assertTrue(closeMyntraSDATrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment2 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId3 = tripOrderAssignment2.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId3));
        MLShipmentResponse mlShipmentResponse45 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId2 = String.valueOf(mlShipmentResponse45.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId2);
        TripOrderAssignmentResponse tripOrderAssignmentResponse9 = tripClient_qa.deliverStoreOrders(storeTripId, mlShipmentEntriesId2, tripOrderAssignmentId3, storeTenantId, "CASH", LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse9.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, ShipmentUpdateEvent.DELIVERED.toString());
        //close myntra SDA trip
        TripOrderAssignmentResponse closeMyntraStoreTrip = storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,storeTripId);
        Assert.assertTrue(closeMyntraStoreTrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(storeTripId,EnumSCM.COMPLETED);
    }

    @Test(enabled = true, description = "TC ID: C28090, Create forward shipment till SH and delivered it through EOD store flow and create return and do Pickup_Successful through same store and return it back to warehouse")
    public void process_DeliverShipmentThroughStore_CreateReturn_SendItToWH_Successsful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);

        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.EXPECTED_IN_DC);

        //close myntra SDA trip
        TripOrderAssignmentResponse closeMyntraSDATrip = storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        Assert.assertTrue(closeMyntraSDATrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment2 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId3 = tripOrderAssignment2.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId3));
        MLShipmentResponse mlShipmentResponse45 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId2 = String.valueOf(mlShipmentResponse45.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId2);
        TripOrderAssignmentResponse tripOrderAssignmentResponse9 = tripClient_qa.deliverStoreOrders(storeTripId, mlShipmentEntriesId2, tripOrderAssignmentId3, storeTenantId, "CASH", LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse9.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, ShipmentUpdateEvent.DELIVERED.toString());
        //close myntra SDA trip
        TripOrderAssignmentResponse closeMyntraStoreTrip = storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,storeTripId);
        Assert.assertTrue(closeMyntraStoreTrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(storeTripId,EnumSCM.COMPLETED);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), tenantId, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse2, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnHub = returnResponse2.getDomainReturnShipments().get(0).getReturnHubCode();

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.UNASSIGNED);

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse createStoreTrip = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(createStoreTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        Long returnStoreTripId = createStoreTrip.getTrips().get(0).getId();
        storeValidator.isNull(returnStoreTripId.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.OUT_FOR_PICKUP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse findOrdersByTrip = lmsServiceHelper.findOrdersByTripId(String.valueOf(returnStoreTripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = findOrdersByTrip.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successfull in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE, tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.PICKUP_SUCCESSFUL);

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff), "Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)&&deliveryStaffResponse.getDeliveryStaffs().get(0).getIsMobileVerified()) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse createTrip = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(createTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry = createTrip.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId), ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.PICKUP_SUCCESSFUL);

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(returnTrackingNumber);
        TripShipmentAssociationResponse pickupOrdersFromStore = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS, ShipmentType.PU);
        Assert.assertTrue(pickupOrdersFromStore.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.PICKUP_SUCCESSFUL);

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(returnTrackingNumber.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.PICKUP_SUCCESSFUL);

        //Qc pass in DC
        String mlOpenBoxQCPass = storeHelper.mlOpenBoxQCPass(String.valueOf(returnId), ShipmentUpdateEvent.PICKUP_SUCCESSFUL, returnTrackingNumber);
        Assert.assertTrue(mlOpenBoxQCPass.contains(ResponseMessageConstants.qcUpdate));

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, EnumSCM.PICKUP_SUCCESSFUL);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, EnumSCM.PICKUP_SUCCESSFUL);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = storeHelper.closeMyntraTripWithStoreBag(tenantId, reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());
        //return the return order to warehouse
        returnHelper.processReturnOrderFromDCToWareHouse(returnId, returnTrackingNumber, "ELC", "RT-BLR");
    }

    @Test(enabled = true, description = "TC ID:C28091 , create exchange shipment and deliver the shipment through EOD store flow and exchanged order return back to warehouse")
    public void processExchangeOrdersFailedDeliveredAndRequeSuccesssful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse01 = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse01, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse01.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId, EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId, EnumSCM.EXPECTED_IN_DC);

        //close myntra SDA trip
        TripOrderAssignmentResponse closeMyntraSDATrip = storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        Assert.assertTrue(closeMyntraSDATrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId, EnumSCM.OUT_FOR_DELIVERY);

        //update the trip order status as delivered and get the pickup exchange order details
        String pickupTrackingNumber = null;
        String sourceReturnId = null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(storeTripId, storeTenantId);
        if (pickupTripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getId();
            String exchangeOrderId1 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getExchangeOrderId();


            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, exchangeOrderId1, storeTripId, orderEntry, storeTenantId, 2297l);
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            pickupTrackingNumber = tripOrderAssignmentResponse0.getTripOrders().get(0).getTrackingNumber();
            sourceReturnId = tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }else{
            Assert.fail("pickup tracking number is not matching exchange tracking number, pickup tracking number is -"+pickupTripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber()
            +" and founded exchange tracking number -"+exchangeTrackingNumber);
        }

        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(storeTripId, storeTenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId, ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId, tenantId, Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        //return the return order to warehouse
        returnHelper.processReturnOrderFromDCToWareHouse(Long.parseLong(sourceReturnId), pickupTrackingNumber, "ELC", "RT-BLR");
    }

}





