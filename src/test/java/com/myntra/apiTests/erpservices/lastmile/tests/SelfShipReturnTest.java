package com.myntra.apiTests.erpservices.lastmile.tests;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS.CLIENTID;
import static com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS.TENANTID;

import java.io.IOException;
import com.myntra.apiTests.erpservices.lastmile.service.LMSOrderDetailsClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.cms.pim.dto.StatusType;
import com.myntra.lms.client.status.*;
import com.myntra.serviceability.client.util.ServiceType;
import edu.emory.mathcs.backport.java.util.Arrays;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.Premise;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.testbase.BaseTest;

public class SelfShipReturnTest extends BaseTest {

    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper;
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private MasterBagServiceHelper masterBagServiceHelper=new MasterBagServiceHelper();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    StatusPollingValidator statusPollingValidator=new StatusPollingValidator();
    private MasterBagClient masterBagClient;
    ServiceabilityHelper serviceabilityHelper=new ServiceabilityHelper();
    ReturnHelper returnHelper=new ReturnHelper();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        storeHelper=  new StoreHelper(getEnvironment(),LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    }


    @Test(enabled = true,description = "TC ID:C9875, Create self ship return and do QC pass, return back to warehouse")
    public void  process_SelfShipReturnSuccessful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String deliveryCenterCode=orderResponse.getOrders().get(0).getDeliveryCenterCode();
        String returnsHub=orderResponse.getOrders().get(0).getRtoHubCode();
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071",LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnStatusInLMS, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(),"Tracking number is generated before completing self ship QC update");
        //update QC pass for self ship orders
        String qcUpdateResponse=storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_SUCCESSFUL,ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics,"5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), TENANTID,Long.parseLong(CLIENTID), com.myntra.logistics.platform.domain.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusDetails=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber=returnStatusDetails.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber,"Tracking number is not generated after completing self ship QC update");

        //process self ship return
        returnHelper.processReturnOrderFromDCToWareHouse(returnId,returnTrackingNumber,deliveryCenterCode,returnsHub);
    }

    @Test(enabled = true,description = "TC ID: C9876, Create self ship return and do QC passs, return back to warehouse(QC_ONHOLD)")
    public void process_SelfShipReturnQC_ONHOLD_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC ONHOLD for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMSOnHold=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNotNull(returnStatusInLMSOnHold.getDomainReturnShipments().get(0).getTrackingNumber(),"Tracking number is not generated after completing self ship QC update");

    }

    @Test(enabled = true,description = "TC ID:C9886, Create self ship return and do QC passs, return back to warehouse(QC_ONHOLD_APPROVED_BYCC)")
    public void process_SelfShipReturnQC_ONHOLD_APPROVED_BYCC_ReturnWH_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        String returnsHub =  orderResponse.getOrders().get(0).getRtoHubCode();
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC OnHOLD for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMSOnHold=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber=returnStatusInLMSOnHold.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber,"Tracking number is not generated after completing self ship QC update");

        //cc approve
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.APPROVED);

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.APPROVED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMSCCApprove=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(returnStatusInLMSCCApprove.getDomainReturnShipments().get(0).getStatus().toString().equalsIgnoreCase("RT"),"return shipment status is not matchingas RT");

        //Ack pickup QC
        String ackPickupQCByCC=storeHelper.selfShipPickUpApproveByCC(String.valueOf(returnId),ShipmentUpdateEvent.ACKNOWLEDGE_APPROVE_ONHOLD_WITH_RETURN_PROCESSING_CENTER);
        Assert.assertTrue(ackPickupQCByCC.contains("1/1 updated successfully"),"QC approved by cc was not successful");

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());

        //process return order DC to wh
        returnHelper.processReturnOrderFromDCToWareHouse(returnId,returnTrackingNumber,deliveryCenterCode,returnsHub);
    }

    //start here
    @Test(enabled = true,description = "TC ID:C28369, Create self ship return and do QC OnHold, CC Reject ,return back to warehouse(QC_ONHOLD_REJECT_BYCC)")
    public void process_SelfShipReturnQC_ONHOLD_REJECT_BYCC_ReshipToCustomer_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse getTrackingNumber = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(getTrackingNumber.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC OnHold for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnTracking=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber=returnTracking.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber,"Tracking number is not generated after completing self ship QC update");

        //cc reject
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.REJECTED);

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.RRSH.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.REJECTED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse7=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(returnResponse7.getDomainReturnShipments().get(0).getStatus().toString().equalsIgnoreCase("REJ"),"return shipment status is not matchingas REJ");

        //Reship to customer
        String filePath=storeHelper.createCSVFile(returnId,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,returnTrackingNumber);
        String reshipToCustomerResponse = null;
        int i=0;
        while(i<2){
            reshipToCustomerResponse=storeHelper.reshipToCustomer(filePath);
            i++;
        }
        Assert.assertTrue(reshipToCustomerResponse.contains(ResponseMessageConstants.reshipToCustomer),"Return order is not reshipped to customer");
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.SHC.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RESHIPPED_TO_CUSTOMER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(returnStatusInLMS.getDomainReturnShipments().get(0).getStatus().toString().equalsIgnoreCase(com.myntra.lastmile.client.status.ShipmentStatus.RESHIPPED_TO_CUSTOMER.toString()),"return shipment status is not matchingas SHC");
    }

    @Test(enabled = true,description = "TC ID:C9868, Positive Case : Check for availability of self shipment for return id")
    public void checkSelfShipAvailabilityForSelfShipReturn() throws IOException {
        String orderServiceabilityDetailResponse = serviceabilityHelper.checkPincodeServiceability(ServiceType.RETURN, com.myntra.serviceability.client.util.ShippingMethod.ALL, com.myntra.serviceability.client.util.PaymentMode.ALL,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode,"36", CLIENTID);
        Assert.assertEquals(orderServiceabilityDetailResponse,EnumSCM.SELF_SHIP,"serviceability is not found the given pincode and serviceType");
    }


    @Test(enabled = true, description = "TC ID:C9871, Positive Case : check for Self Ship - Approve QC on hold")
    public void process_SelfShipReturnQC_ONHOLD_APPROVED_BYCC_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC ONHOLD for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse lmsReturnStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = lmsReturnStatus.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        //cc approve
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.APPROVED);

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.APPROVED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse retrunStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(retrunStatus.getDomainReturnShipments().get(0).getStatus().toString().equalsIgnoreCase("RT"), "return shipment status is not matchingas RT");

        //Ack pickup QC
        String ackPickupQCByCC = storeHelper.selfShipPickUpApproveByCC(String.valueOf(returnId), ShipmentUpdateEvent.ACKNOWLEDGE_APPROVE_ONHOLD_WITH_RETURN_PROCESSING_CENTER);
        Assert.assertTrue(ackPickupQCByCC.contains("1/1 updated successfully"), "QC approved by cc was not successful");

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RL.toString());
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_SUCCESSFUL.toString());

    }

    @Test(enabled = true, description = "TC ID:C9872, Positive Case : Check for Self ship - Reject QC - On hold")
    public void process_SelfShipReturnQC_ONHOLD_REJECT_BYCC_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC ONHOLD for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse lmsReturnStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = lmsReturnStatus.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        //cc reject
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.REJECTED);

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RRSH.toString());

        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.REJECTED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(returStatus.getDomainReturnShipments().get(0).getStatus().toString().equalsIgnoreCase("REJ"), "return shipment status is not matchingas REJ");
    }

    @Test(enabled = true, description = "TC ID:C9877, Positive Scenario : once QC - ONHOLD is Approved, then pick up status will be \"RT\" : APPROVED")
    public void process_SelfShipReturnQC_ONHOLD_APPROVED_PickupStatusShouldBeRT_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC ONHOLD for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse lmsReturnStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = lmsReturnStatus.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        //cc approve
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.APPROVED);

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.APPROVED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(returnStatus.getDomainReturnShipments().get(0).getStatus().toString().equalsIgnoreCase("RT"), "pickup status is not RT");

    }

    @Test(enabled = true, description = "TC ID:C9878 - Positive Scenario : Once QC - ONHOLD is  Approved , then return status will be \"RL\"(Received By Logistics)")
    public void process_SelfShipReturnQC_ONHOLD_APPROVED_ReturnStatusRL_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC ONHOLD for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse lmsReturnStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = lmsReturnStatus.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        //cc approve
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.APPROVED);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RL.toString());
    }

    @Test(enabled = true, description = "TC ID:C9879, Positive Scenario : Once QC - ONHOLD is rejected , then pick up status - \"REJ\": Rejected and shipment status is REJECTED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER")
    public void process_SelfShipReturnQC_ONHOLD_REJECT_PickupStatusShouldBeREJ_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC OnHold for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnTrackingDetails = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = returnTrackingDetails.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        //cc reject
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.REJECTED);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RRSH.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.REJECTED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(returnStatus.getDomainReturnShipments().get(0).getStatus().toString().equalsIgnoreCase("REJ"), "pickup status is not in REJ");
    }

    @Test(enabled = true, description = "TC ID:C9880,Positive Scenario : Check Reshipment for the QC- Rejected")
    public void process_SelfShipReturn_Reshipment_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC OnHold for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnTrackingDetails = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = returnTrackingDetails.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        //cc reject
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.REJECTED);

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RRSH.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.REJECTED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(returnStatus.getDomainReturnShipments().get(0).getStatus().toString().equalsIgnoreCase("REJ"), "return shipment status is not matchingas REJ");

        //Reship to customer
        String filePath = storeHelper.createCSVFile(returnId, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, returnTrackingNumber);
        String reshipToCustomerResponse = null;
        int i = 0;
        while (i < 2) {
            reshipToCustomerResponse = storeHelper.reshipToCustomer(filePath);
            i++;
        }
        Assert.assertTrue(reshipToCustomerResponse.contains(ResponseMessageConstants.reshipToCustomer), "Return order is not reshipped to customer");
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.SHC.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RESHIPPED_TO_CUSTOMER.toString());
    }

    @Test(enabled = true,description = "TC ID:C9881, Negative Scenario : check for the return having no availability of Self shipment i.e open box / closed box")
    public void checkNoAvailabilityForSelfShipReturn() throws IOException {
        String orderServiceabilityDetailResponse = serviceabilityHelper.checkPincodeServiceability(ServiceType.RETURN, com.myntra.serviceability.client.util.ShippingMethod.ALL, com.myntra.serviceability.client.util.PaymentMode.ALL,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode,"36", CLIENTID);
        Assert.assertNotEquals(orderServiceabilityDetailResponse,EnumSCM.SELF_SHIP,"serviceability is not found the given pincode and serviceType");
    }

    @Test(enabled = true, description = "TC ID:C9882,Negative Scenario : QC PASS order ->Reship the return order customer is not allowed")
    public void process_SelfShipReturn_QCPass_ReshipmentNotAllowed_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC pass for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_SUCCESSFUL, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnTrackingDetails = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = returnTrackingDetails.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        //Reship to customer
        String filePath = storeHelper.createCSVFile(returnId, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, returnTrackingNumber);
        String reshipToCustomerResponse = null;
        int i = 0;
        while (i < 2) {
            reshipToCustomerResponse = storeHelper.reshipToCustomer(filePath);
            i++;
        }
        Assert.assertTrue(reshipToCustomerResponse.contains(ResponseMessageConstants.reshipToCustomer), "Return order is not reshipped to customer");
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_SUCCESSFUL.toString());
    }

    @Test(enabled = true, description = "TC ID:C9883,Negative scenario : QC Rejected / Failed Order -> shipping to warehouse is not allowed")
    public void process_SelfShipReturn_QCReject_ShipToWHNotAllowed_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC OnHold for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnTrackingDetails = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = returnTrackingDetails.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        //cc reject
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.REJECTED);

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RRSH.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.REJECTED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(returnStatus.getDomainReturnShipments().get(0).getStatus().toString().equalsIgnoreCase("REJ"), "return shipment status is not matchingas REJ");

        //return back to wh is not allowed
        //create master bag
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag(deliveryCenterCode, "RT-BLR", ShippingMethod.NORMAL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LMS_CONSTANTS.TENANTID);
        Long masterBagId = masterbagDomain.getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        ShipmentResponse shipmentResponse = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse.getStatus().getStatusMessage(), ResponseMessageConstants.retrievedShipment, "Not able to retrive Master bag details");
        storeValidator.validateMasterBagStatus(shipmentResponse, com.myntra.lms.client.status.ShipmentStatus.NEW);

        //assign order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, returnTrackingNumber, ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusType().toString().equalsIgnoreCase(StatusType.ERROR.toString()),
                "Order is in REJECTED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER state, is added to master bag.");
    }

    @Test(enabled = true, description = "TC ID:C9884,Positive scenario: Approve QC ONHOLD , Acknowledge Approve Pickup , shipment_status should be RETURN_SUCCESSFUL")
    public void process_SelfShipReturnQC_ONHOLD_ACkApprovePickup_ReturnStatusInRL_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC pass for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse trackingDetails = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = trackingDetails.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        //cc approve
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.APPROVED);

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.APPROVED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(returnStatus.getDomainReturnShipments().get(0).getStatus().toString().equalsIgnoreCase("RT"), "return shipment status is not matchingas RT");

        //Ack pickup QC
        String ackPickupQCByCC = storeHelper.selfShipPickUpApproveByCC(String.valueOf(returnId), ShipmentUpdateEvent.ACKNOWLEDGE_APPROVE_ONHOLD_WITH_RETURN_PROCESSING_CENTER);
        Assert.assertTrue(ackPickupQCByCC.contains("1/1 updated successfully"), "QC approved by cc was not successful");

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
    }

    @Test(enabled = true, description = "TC ID:C9885,Negative scenario : Reship is not allowed for RETURN_SUCCESSFUL")
    public void process_SelfShipReturn_ReshipmentNotAllowed_ForRLOrders_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC pass for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_SUCCESSFUL, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        com.myntra.lms.client.domain.response.ReturnResponse trackingDetails = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = trackingDetails.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        //Reship to customer
        String filePath = storeHelper.createCSVFile(returnId, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, returnTrackingNumber);
        String reshipToCustomerResponse = null;
        int i = 0;
        while (i < 2) {
            reshipToCustomerResponse = storeHelper.reshipToCustomer(filePath);
            i++;
        }
        Assert.assertTrue(reshipToCustomerResponse.contains(ResponseMessageConstants.reshipToCustomer), "Return order is not reshipped to customer");
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_SUCCESSFUL.toString());
    }

    @Test(enabled = true, description = "TC ID:C9887, Negative scenario : Pre-approve a return and then mark on hold then reject")
    public void process_PreApproveSelfShipReturn_Mark_ON_HOLD_CCReject_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //cc approve
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.APPROVED);
        //validate RMS status
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(returnStatus.getDomainReturnShipments().get(0).getApprovalFlag().toString().equalsIgnoreCase("APPROVED"), "return shipment status is not matchingas RT");

        //update QC ONHLOD for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_ON_HOLD, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
    }

    @Test(enabled = true, description = "TC ID:C9888,Negative Scenario : Green Channel approve for self shipment which is in Return Successful order")
    public void process_GreenChannelApprovalFor_SelfShipReturn_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnStatusInLMS, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC pass for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_SUCCESSFUL, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        com.myntra.lms.client.domain.response.ReturnResponse trackingNumberResponse = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = trackingNumberResponse.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        //TODO Green channel approval is not upload successfully when you hit first time
        String filePath1 = storeHelper.createCSVFileForGreenChannel(Arrays.asList(new String[]{returnTrackingNumber}), LMS_CONSTANTS.TENANTID);
        String greenChannelResponse1 = storeHelper.uploadGreenChannel(filePath1);
        if (!(greenChannelResponse1.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse1 = storeHelper.uploadGreenChannel(filePath1);
        }
        Assert.assertTrue(!(greenChannelResponse1.contains(ResponseMessageConstants.reshipToCustomer)), "Green channel update is completed successfully for the self ship returns which is in RT state");
    }

    @Test(enabled = true, description = "TC ID:C9889, Pre-approval Flow for Self Ship")
    public void process_PreApproveFlow_SelfShipReturn_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        String returnsHub = orderResponse.getOrders().get(0).getRtoHubCode();
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //cc approve
        lms_returnHelper.selfShipApproveOrReject(String.valueOf(returnId), EnumSCM.APPROVED);
        //validate RMS status
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatus = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(returnStatus.getDomainReturnShipments().get(0).getApprovalFlag().toString().equalsIgnoreCase("APPROVED"), "return shipment status is not matching as RT");

        //update QC pass for self ship orders
        String qcUpdateResponse=storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_SUCCESSFUL,ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics,"5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), TENANTID,Long.parseLong(CLIENTID), com.myntra.logistics.platform.domain.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusDetails=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber=returnStatusDetails.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber,"Tracking number is not generated after completing self ship QC update");

        //process self ship return
        returnHelper.processReturnOrderFromDCToWareHouse(returnId,returnTrackingNumber,deliveryCenterCode,returnsHub);
    }

    @Test(enabled = true, description = "TC ID:C9869,Positive Case : check for Selfship - QC pass , verify DC is stamped once return is received")
    public void process_SelfShipReturnQCPass_DCIDShouldbePopulate_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC pass for self ship orders
        String qcUpdateResponse=storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_SUCCESSFUL,ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics,"5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), TENANTID,Long.parseLong(CLIENTID), com.myntra.logistics.platform.domain.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusAfetrQCPass=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber=returnStatusAfetrQCPass.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber,"Tracking number is not generated after completing self ship QC update");
        Assert.assertNotNull(returnStatusAfetrQCPass.getDomainReturnShipments().get(0).getDeliveryCenterId(),"delivery center id is not populated after completing self ship QC pass");
    }

    @Test(enabled = true, description = "TC ID:C9874, Positive Case : Validate QC actions in warehouse")
    public void process_SelfShipReturnQCPass_QCActionsInWH_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNull(returnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC pass for self ship orders
        String qcUpdateResponse=storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_SUCCESSFUL,ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics,"5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), TENANTID,Long.parseLong(CLIENTID), com.myntra.logistics.platform.domain.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        com.myntra.lms.client.domain.response.ReturnResponse returnStatusAfetrQCPass=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber=returnStatusAfetrQCPass.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber,"Tracking number is not generated after completing self ship QC update");
    }

}
