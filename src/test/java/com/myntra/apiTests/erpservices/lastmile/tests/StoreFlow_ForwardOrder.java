package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.CommonUtils;
import com.myntra.apiTests.erpservices.lastmile.client.DeliveryCenterClient;
import com.myntra.apiTests.erpservices.lastmile.client.StoreClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_StoreFlowsDP;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Helper.HubToTransportHubConfigResponse;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.erpservices.utility.RejoyServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.commons.response.EmptyResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.paymentplan.exception.ErrorResponse;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.tms.container.ContainerEntry;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class StoreFlow_ForwardOrder {
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private LastmileClient lastmileClient = new LastmileClient();
    private TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa=new MLShipmentClientV2_QA();

    private LastmileHelper lastmileHelper;
    private MasterBagClient masterBagClient;
    private PPSServiceHelper ppsServiceHelper=new PPSServiceHelper();
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    static String searchParams;
    static Long storeHlPId;
    static Long storeSDAId;
    static String storeTenantId;
    static String tenantId;
    static String pincode;
    static String mobileNumber;
    static String storeCourierCode;
    static String code;

    static String code1;
    static String storeCourierCode1;
    static String searchParams1;
    static Long storeHlPId1;
    static Long storeSDAId1;
    static String storeTenantId1;
    static String mobileNumber1;

    @BeforeTest
    public void createStore(){
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");

        Random r = new Random(System.currentTimeMillis());
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        int contactNumber2 = Math.abs(2000000000 + r.nextInt(300000000));
        String value = CommonUtils.generateString(3);
        String value1 = CommonUtils.generateString(3);

        String name=value+ String.valueOf(contactNumber1).substring(5,9);
        String ownerFirstName= value+ String.valueOf(contactNumber1).substring(5,9);
        String ownerLastName= value + String.valueOf(contactNumber1).substring(5,9);
        code=value+ String.valueOf(contactNumber1).substring(5,9);
        code1=value1+ String.valueOf(contactNumber2).substring(5,9);
        String latLong="LA_latlong" + String.valueOf(contactNumber1).substring(5,9);
        String emailId="test@lastmileAutomation.com";
        mobileNumber=String.valueOf(contactNumber1);
        String  address="Automation Address";
        pincode= LASTMILE_CONSTANTS.STORE_PINCODE;
        String city="Bangalore";
        String state="Karnataka";
        String mappedDcCode= LASTMILE_CONSTANTS.ROLLING_DC_CODE;
        String gstin="LA_gstin" + String.valueOf(contactNumber1).substring(5,9);
        tenantId=LMS_CONSTANTS.TENANTID;

        String name1=value1 + String.valueOf(contactNumber2).substring(5,9);
        String ownerFirstName1= value1 + String.valueOf(contactNumber2).substring(5,9);
        String ownerLastName1= value1+ String.valueOf(contactNumber2).substring(5,9);
        String latLong1="LA_latlong" + String.valueOf(contactNumber2).substring(5,9);
        mobileNumber1=String.valueOf(contactNumber2);
        String gstin1="LA_gstin" + String.valueOf(contactNumber2).substring(5,9);


        searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId = deliveryStaffEntry.getId();
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //create 2nd store
        searchParams1 = "code.like:" + code1;
        //Create Store
        StoreResponse storeResponse1 = lastmileHelper.createStore(name1, ownerFirstName1, ownerLastName1, latLong1, "test1@lastmileAutomation.com", mobileNumber1, address, pincode, city, state, mappedDcCode,
                gstin1, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code1);
        //validate Store created successfully
        Assert.assertTrue(storeResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId1 = storeResponse1.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry1 = storeResponse1.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId1 = deliveryStaffEntry1.getId();
        storeTenantId1 = storeResponse1.getStoreEntries().get(0).getTenantId();
        storeCourierCode1=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

    }

    @AfterMethod
    public void modifiedMLLastmilePartnerShipmentDate(){
        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDateAsCurrentDate(storeCourierCode);
    }

    @Test( priority = 1, enabled = true,description = "TC ID: C24692, Check shortage for forward orders")
    public void process_MarkForwardOrder_Shortage_Successful() throws Exception {

        //Create 1st Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create 2st Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);


        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add 1st order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Add 2st order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String response1 = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        if (response1.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);

       /* //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);
*/
        ShipmentResponse shipmentResponse4 = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse4.getStatus().getStatusMessage(), ResponseMessageConstants.retrievedShipment, "Not able to retrive Master bag details");
        storeValidator.validateMasterBagStatus(shipmentResponse4, ShipmentStatus.DELIVERED);
        List<OrderShipmentAssociationEntry> shipmentEntries=shipmentResponse4.getEntries().get(0).getOrderShipmentAssociationEntries();
        for(int i=0;i<shipmentEntries.size();i++){
            if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(trackingNo)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),ShipmentStatus.DELIVERED.toString(),"The tracking number status is not expected, TrackingNUmber- "+trackingNo+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }else if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(trackingNo1)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),EnumSCM.SHORTAGE,"The tracking number status is not expected, TrackingNUmber- "+trackingNo1+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }
        }

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.FAILED_TO_HANDOVER_TO_LAST_MILE_PARTNER);

    }

    @Test( priority = 2, enabled = true,description = "TC:C24693, Check shortage for Excess orders")
    public void process_MarkForwardOrder_Excess_Successful() throws Exception {

        //Create 1st Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create 2st Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);


        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add 1st order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        listTrackingNumbers.add(trackingNo1);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        ShipmentResponse shipmentResponse4 = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse4.getStatus().getStatusMessage(), ResponseMessageConstants.retrievedShipment, "Not able to retrive Master bag details");
        storeValidator.validateMasterBagStatus(shipmentResponse4, ShipmentStatus.DELIVERED);
        List<OrderShipmentAssociationEntry> shipmentEntries=shipmentResponse4.getEntries().get(0).getOrderShipmentAssociationEntries();
        for(int i=0;i<shipmentEntries.size();i++){
            if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(trackingNo)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),ShipmentStatus.DELIVERED.toString(),"The tracking number status is not expected, TrackingNUmber- "+trackingNo+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }else if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(trackingNo1)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),"EXCESS","The tracking number status is not expected, TrackingNUmber- "+trackingNo1+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }
        }

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, storeTenantId,EnumSCM.EXPECTED_IN_DC);
    }

    @Test( priority = 3, enabled = true, description = "TC ID: C24694, Process the shortage item with normal SDA to customer flow and mark as FD and then reattempt with another Store flow,MakeFD_DeliveredItToAnotherStore")
    public void process_MarkForwardOrder_Shortage_MakeFD_DeliveredItToAnotherStore_Successful() throws Exception {

        //Create 1st Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create 2st Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);


        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add 1st order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Add 2st order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String response1 = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        if (response1.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        ShipmentResponse shipmentResponse4 = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse4.getStatus().getStatusMessage(), ResponseMessageConstants.retrievedShipment, "Not able to retrive Master bag details");
        storeValidator.validateMasterBagStatus(shipmentResponse4, ShipmentStatus.DELIVERED);
        List<OrderShipmentAssociationEntry> shipmentEntries=shipmentResponse4.getEntries().get(0).getOrderShipmentAssociationEntries();
        for(int i=0;i<shipmentEntries.size();i++){
            if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(trackingNo)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),ShipmentStatus.DELIVERED.toString(),"The tracking number status is not expected, TrackingNUmber- "+trackingNo+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }else if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(trackingNo1)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),EnumSCM.SHORTAGE,"The tracking number status is not expected, TrackingNUmber- "+trackingNo1+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }
        }

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.FAILED_TO_HANDOVER_TO_LAST_MILE_PARTNER);

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo1);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo1.substring(2, trackingNo1.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.RECEIVED_IN_DC);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4=tripClient_qa.closeMyntraTripWithStoreBag(tenantId,tripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lmsServiceHelper.requeueOrder(packetId1);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.UNASSIGNED);

        //mark FD through normal SDA flow
        //Create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse1 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse1, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse7=lastmileClient.assignOrderToTrip(tripId1,trackingNo1);
        Assert.assertTrue(tripOrderAssignmentResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Order is not assigned to Dc trip");

        //start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6=lastmileClient.startTrip(tripId1.toString(),"10");
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripUpdate),"trip is not started for the DC");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(tripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Update trip order status as Failed Delivered using DC SDA
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.FAILED_DELIVERY);

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo1);
        String receiveInDc2 = storeHelper.receiveShipmentInDC(trackingNo1.substring(2, trackingNo1.length()), tenantId);
        Assert.assertTrue(receiveInDc2.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.FAILED_DELIVERY);


        //Complete DC trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.RECEIVED_IN_DC);

        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1,com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());
        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse12 = lmsServiceHelper.requeueOrder(packetId1);
        Assert.assertTrue(tripOrderAssignmentResponse12.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.UNASSIGNED);

        //Create MasterBag
        String originDCCity1 = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse01 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams1, "");
        String destinationStoreCity1 = storeResponse01.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse10 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity1, destinationStoreCity1);
        Assert.assertTrue(shipmentResponse10.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId1 = shipmentResponse10.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo2 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String response2 = masterBagClient_qa.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo2);
        if (response2.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus7 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId1, trackingNo1);
        Assert.assertEquals(orderShipmentAssociationStatus7.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID2 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse9 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID2));
        Assert.assertTrue(tripResponse9.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId2 = tripResponse9.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId2));
        Assert.assertTrue(tripResponse9.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse9, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse7 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId2), String.valueOf(masterBagId1),LMS_CONSTANTS.TENANTID);;
        Assert.assertTrue(tripShipmentAssociationResponse7.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus1 = lastmileHelper.getShipmentOrderMapStatus(trackingNo1, masterBagId1);
        Assert.assertTrue(shipmentOrderMapStatus1.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse9 = tripClient_qa.startTrip(tripId2);
        Assert.assertTrue(tripOrderAssignmentResponse9.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers1 = new ArrayList();
        listTrackingNumbers1.add(trackingNo1);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId1), listTrackingNumbers1, String.valueOf(tripId2), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse2.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId1)));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getTotalCount() == 1);

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, storeTenantId1,EnumSCM.EXPECTED_IN_DC);

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.DELIVERED.toString());
        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse2 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers1, storeSDAId1);
        Assert.assertTrue(tripResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse2.getTrips().get(0).getId();
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        storeValidator.isNull(storeTripId.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.OUT_FOR_DELIVERY);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, storeTenantId1,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as  Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber1, storeTenantId1);
        Long tripOrderAssignmentId1 = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId1));

        MLShipmentResponse mlShipmentResponse27 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo1, tenantId);
        String mlShipmentEntriesId1 = String.valueOf(mlShipmentResponse27.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId1);
        TripOrderAssignmentResponse tripOrderAssignmentResponse10=storeHelper.deliveredStoreOrders(tripOrderAssignmentId1,AttemptReasonCode.DELIVERED, com.myntra.lastmile.client.code.utils.TripAction.UPDATE,
                "CASH",tenantId);
        Assert.assertEquals(tripOrderAssignmentResponse10.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.DELIVERED);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, storeTenantId1,EnumSCM.DELIVERED);

    }
    @Test( priority = 4, enabled = true,  description = "TC ID;C24695,Process the shortage item with normal SDA to customer flow and mark as FD and then reattempt with another Store flow,DeliveredThroughSDACustomerFlow")
    public void process_MarkForwardOrder_Shortage_DeliveredThroughSDACustomerFlow_Successful() throws Exception {

        //Create 1st Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create 2st Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);


        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add 1st order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Add 2st order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String response1 = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        if (response1.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);

        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ShipmentResponse shipmentResponse4 = masterBagClient.getShipmentOrderMap(masterBagId);
        List<OrderShipmentAssociationEntry> shipmentEntries=shipmentResponse4.getEntries().get(0).getOrderShipmentAssociationEntries();
        for(int i=0;i<shipmentEntries.size();i++){
            if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(trackingNo)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),ShipmentStatus.DELIVERED.toString(),"The tracking number status is not expected, TrackingNUmber- "+trackingNo+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }else if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(trackingNo1)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),EnumSCM.SHORTAGE,"The tracking number status is not expected, TrackingNUmber- "+trackingNo1+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }
        }

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.FAILED_TO_HANDOVER_TO_LAST_MILE_PARTNER);

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo1);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo1.substring(2, trackingNo1.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.RECEIVED_IN_DC);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4=tripClient_qa.closeMyntraTripWithStoreBag(tenantId,tripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lmsServiceHelper.requeueOrder(packetId1);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.UNASSIGNED);

        //mark FD through normal SDA flow
        //Create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse1 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse1, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse7=lastmileClient.assignOrderToTrip(tripId1,trackingNo1);
        Assert.assertTrue(tripOrderAssignmentResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Order is not assigned to Dc trip");

        //start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6=lastmileClient.startTrip(tripId1.toString(),"10");
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripUpdate),"trip is not started for the DC");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(tripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Update trip order status as Delivered using DC SDA
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1,tenantId,MLDeliveryShipmentStatus.DELIVERED.toString());
        //completeTrip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1,com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

    }

    @Test(priority = 5,enabled = true, description = "TC ID:C24696,Process the shortage item with normal SDA to customer flow and mark as FD and then reattempt with another Store flow,Shortage_MakeFD_MarkRTO")
    public void process_MarkForwardOrder_Shortage_MakeFD_MarkRTO_Successful() throws Exception {

        //Create 1st Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create 2st Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        OrderResponse orderResponse0 = lmsClient.getOrderByOrderId(packetId1);
        Assert.assertTrue(orderResponse0.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse0, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        String deliveryCenterName = orderResponse0.getOrders().get(0).getDeliveryCenterCode();
        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add 1st order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Add 2st order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String response1 = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        if (response1.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        ShipmentResponse shipmentResponse4 = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse4.getStatus().getStatusMessage(), ResponseMessageConstants.retrievedShipment, "Not able to retrive Master bag details");
        storeValidator.validateMasterBagStatus(shipmentResponse4, ShipmentStatus.DELIVERED);
        List<OrderShipmentAssociationEntry> shipmentEntries = shipmentResponse4.getEntries().get(0).getOrderShipmentAssociationEntries();
        for (int i = 0; i < shipmentEntries.size(); i++) {
            if (shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(trackingNo)) {
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(), ShipmentStatus.DELIVERED.toString(), "The tracking number status is not expected, TrackingNUmber- " + trackingNo + " and status is - " + shipmentEntries.get(i).getStatus().toString());
            } else if (shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(trackingNo1)) {
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(), EnumSCM.SHORTAGE, "The tracking number status is not expected, TrackingNUmber- " + trackingNo1 + " and status is - " + shipmentEntries.get(i).getStatus().toString());
            }
        }

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.FAILED_TO_HANDOVER_TO_LAST_MILE_PARTNER);

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo1);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo1.substring(2, trackingNo1.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.RECEIVED_IN_DC);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = tripClient_qa.closeMyntraTripWithStoreBag(tenantId, tripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lmsServiceHelper.requeueOrder(packetId1);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.UNASSIGNED);

        //mark FD through normal SDA flow
        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse1, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse7 = lastmileClient.assignOrderToTrip(tripId1, trackingNo1);
        Assert.assertTrue(tripOrderAssignmentResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Order is not assigned to Dc trip");

        //start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = lastmileClient.startTrip(tripId1.toString(), "10");
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripUpdate), "trip is not started for the DC");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(tripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Update trip order status as Failed Delivered using DC SDA
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.FAILED_DELIVERY);

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo1);
        String receiveInDc2 = storeHelper.receiveShipmentInDC(trackingNo1.substring(2, trackingNo1.length()), tenantId);
        Assert.assertTrue(receiveInDc2.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.FAILED_DELIVERY);

        //Complete DC trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.RECEIVED_IN_DC);
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1,com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //Update order as RTO
        String rtoResponse=storeHelper.createRTO(trackingNo1,originPremiseId, MLShipmentUpdateEvent.RTO_CONFIRMED,ShipmentType.DL,
                ShipmentUpdateActivityTypeSource.MyntraLogistics,tenantId);
        Assert.assertTrue(rtoResponse.contains(ResponseMessageConstants.updateOrder),"Order is not updated successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.RTO_CONFIRMED);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.RTO_CONFIRMED);

        //CreateMB from DC To WH
        ShipmentResponse shipmentResponse7 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, 20l, PremisesType.HUB, ShippingMethod.NORMAL,
                tenantId, "Bangalore", "Bangalore");
        Assert.assertTrue(shipmentResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Master bag from Dc to WH is not created");
        List<ShipmentEntry> lstShipmentEntry = shipmentResponse7.getEntries();
        Long reverseMBId = null;
        if (!(lstShipmentEntry.size() == 0)) {
            reverseMBId = lstShipmentEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(reverseMBId));
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.NEW.toString());
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo2 = new ShipmentUpdateInfo.Builder(Long.toString(reverseMBId), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String response2 = masterBagClient_qa.addShipmentToStoreBag(reverseMBId, shipmentUpdateInfo2);
        if (response2.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        Assert.assertTrue(response2.contains(ResponseMessageConstants.success1), "Order is not assigned to reverse MB");

        //close Reverse MB
        ShipmentResponse shipmentResponse8 = storeHelper.closeShipment(reverseMBId);
        Assert.assertTrue(shipmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.closeShipment), "Master Bag cannot be closed");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse=storeHelper.getMappedTransportHubDetails(tenantId,deliveryCenterName);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub=hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterName,transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(deliveryCenterName, transporterHub, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstShipmentEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, reverseMBId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseMBId), ShipmentStatus.CLOSED.toString());

        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success1), "Container is not shipped");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.RTO_DISPATCHED);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.RTO_IN_TRANSIT);

        //Receive Container In Transport Hub
        ContainerResponse containerResponse3 = (ContainerResponse) storeHelper.receiveContainerInTransportHub.apply(containerId, transporterHub);
        Assert.assertTrue(containerResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Container is not received in TRansport hub");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.RTO_DISPATCHED);

        //receive MB in hub
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(transporterHub,reverseMBId,containerId);

        //MasterBag inscan in WH
        String mbInscanResponse = null;
        mbInscanResponse =storeHelper.masterBagInscanProcess(trackingNo1, reverseMBId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
        if(!(mbInscanResponse.contains(ResponseMessageConstants.success))){
            //TODO  calling this API twice, since it fails 1st time.
            mbInscanResponse = storeHelper.masterBagInscanProcess(trackingNo, reverseMBId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED,
                    ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
        }
        Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success)," Master bag inscan failed in WH");
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.RTO_IN_TRANSIT);


        //receive in Wh
        RejoyServiceHelper.recieveShipmentInRejoy(packetId1);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo1, tenantId,EnumSCM.RTO_DISPATCHED);

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED_TO_SELLER);
    }

    /* TODO:-------------- RTO Flow ----------- */

    @Test( priority = 6, enabled = true,  description = "TC ID: C24697, Create a Store , create and assign order to store,make order status as Failed_Delivered ,update RTO order and return to warehouse")
    public void processRTOForFDOrdersSuccesssful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String deliveryCenterName=orderResponse.getOrders().get(0).getDeliveryCenterCode();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlshipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.FAILED_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse3 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry = tripResponse3.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId),ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");

        // check trip shipment association table PS status
        TripShipmentAssociationResponse tripShipmentAssociationResponse3=storeHelper.findShipmentByTrip(reverseTripId,tenantId);
        Assert.assertTrue(tripShipmentAssociationResponse3.getTripShipmentAssociationEntryList().get(0).getStatus().toString().equalsIgnoreCase(com.myntra.lastmile.client.status.TripOrderStatus.PS.toString()));

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4=storeHelper.closeMyntraTripWithStoreBag(tenantId,reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //Update order as RTO
        String rtoResponse=storeHelper.createRTO(trackingNo,originPremiseId, MLShipmentUpdateEvent.RTO_CONFIRMED,ShipmentType.DL,
                ShipmentUpdateActivityTypeSource.MyntraLogistics,tenantId);
        Assert.assertTrue(rtoResponse.contains(ResponseMessageConstants.updateOrder),"Order is not updated successfully,Reason : "+rtoResponse);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RTO_CONFIRMED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.RTO_CONFIRMED);

        //CreateMB from DC To WH
        ShipmentResponse shipmentResponse7 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, 20l, PremisesType.HUB, ShippingMethod.NORMAL,
                tenantId, "Bangalore", "Bangalore");
        Assert.assertTrue(shipmentResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Master bag from Dc to WH is not created");
        List<ShipmentEntry> lstShipmentEntry = shipmentResponse7.getEntries();
        Long reverseMBId = null;
        if (!(lstShipmentEntry.size() == 0)) {
            reverseMBId = lstShipmentEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(reverseMBId));
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.NEW.toString());
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo2 = new ShipmentUpdateInfo.Builder(Long.toString(reverseMBId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response2 = masterBagClient_qa.addShipmentToStoreBag(reverseMBId, shipmentUpdateInfo2);
        if (response2.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        Assert.assertTrue(response2.contains(ResponseMessageConstants.success1), "Order is not assigned to reverse MB");

        //close Reverse MB
        ShipmentResponse shipmentResponse8 = storeHelper.closeShipment(reverseMBId);
        Assert.assertTrue(shipmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.closeShipment), "Master Bag cannot be closed.Because " + shipmentResponse8.getStatus().getStatusMessage());
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse=storeHelper.getMappedTransportHubDetails(tenantId,deliveryCenterName);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub=hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterName,transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(deliveryCenterName, transporterHub, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstShipmentEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, reverseMBId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());

        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success1), "Container is not shipped");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RTO_DISPATCHED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.RTO_IN_TRANSIT);

        //Receive Container In Transport Hub
        ContainerResponse containerResponse3 = (ContainerResponse) storeHelper.receiveContainerInTransportHub.apply(containerId, transporterHub);
        Assert.assertTrue(containerResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Container is not received in TRansport hub");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RTO_DISPATCHED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //receive mb in hub
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(transporterHub,masterBagId,containerId);

        //MasterBag inscan in WH
        String mbInscanResponse = null;
        mbInscanResponse =storeHelper.masterBagInscanProcess(trackingNo, reverseMBId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
        if(!(mbInscanResponse.contains(ResponseMessageConstants.success))){
            //TODO calling this API twice, since it fails 1st time.
            mbInscanResponse = storeHelper.masterBagInscanProcess(trackingNo, reverseMBId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED,
                    ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
            Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success)," Master bag inscan failed in WH");
        }
        //receive in Wh
        RejoyServiceHelper.recieveShipmentInRejoy(packetId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RTO_DISPATCHED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED_TO_SELLER);


    }

    @Test( priority = 7, enabled = true,  description = "TC ID: C24698, Create a Store , create and assign order to store,make order status as Failed_Delivered ,reque FD order in DC delivered it to customer through Store SDA")
    public void processFDOrderAndRequeInDCSuccesssful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.FAILED_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff), "Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse3 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry = tripResponse3.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId),ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS, ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        // check trip shipment association table PS status
        TripShipmentAssociationResponse tripShipmentAssociationResponse03=storeHelper.findShipmentByTrip(reverseTripId,tenantId);
        Assert.assertTrue(tripShipmentAssociationResponse03.getTripShipmentAssociationEntryList().get(0).getStatus().toString().equalsIgnoreCase(com.myntra.lastmile.client.status.TripOrderStatus.PS.toString()));

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = storeHelper.closeMyntraTripWithStoreBag(tenantId, reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //reque the FD order in DC
        TripOrderAssignmentResponse tripOrderAssignmentResponse2=tripClient_qa.autoAssignmentOfOrderToTrip(packetId,tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Reque of the FD order is not successfull");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.UNASSIGNED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        ShipmentResponse shipmentResponse6 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = shipmentResponse6.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response1 = masterBagClient_qa.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        if (response1.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId1, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        TripResponse tripResponse6 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse6.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse6.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse6, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse3 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId1), String.valueOf(masterBagId1),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse3.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus1 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId1);
        Assert.assertTrue(shipmentOrderMapStatus1.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5= tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers1 = new ArrayList();
        listTrackingNumbers1.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse4 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId1), listTrackingNumbers1, String.valueOf(tripId1), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse4.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId1)));
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse7 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
        Long storeTripId1 = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId1.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId1 = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId1));

        MLShipmentResponse mlShipmentResponse27 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId1 = String.valueOf(mlShipmentResponse27.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId1);
        TripOrderAssignmentResponse tripOrderAssignmentResponse6=storeHelper.deliveredStoreOrders(tripOrderAssignmentId1,AttemptReasonCode.DELIVERED, com.myntra.lastmile.client.code.utils.TripAction.UPDATE,
                "CASH",tenantId);
        Assert.assertEquals(tripOrderAssignmentResponse6.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.DELIVERED);

    }

        @Test( priority = 8, enabled = true, description = "TC ID: C24699, Create a Store , create and assign order to store,make order status as Failed_Delivered ,reque FD order in DC delivered it to customer through DC SDA")
    public void processFDOrderAndRequeDeliverUsingDCSuccesssful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.FAILED_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff), "Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse3 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry = tripResponse3.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId),ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS, ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        // check trip shipment association table PS status
        TripShipmentAssociationResponse tripShipmentAssociationResponse03=storeHelper.findShipmentByTrip(reverseTripId,tenantId);
        Assert.assertTrue(tripShipmentAssociationResponse03.getTripShipmentAssociationEntryList().get(0).getStatus().toString().equalsIgnoreCase(com.myntra.lastmile.client.status.TripOrderStatus.PS.toString()));

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = storeHelper.closeMyntraTripWithStoreBag(tenantId, reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //reque the FD order in DC
        TripOrderAssignmentResponse tripOrderAssignmentResponse2=tripClient_qa.autoAssignmentOfOrderToTrip(packetId,tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Reque of the FD order is not successfull");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.UNASSIGNED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse6 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId1 = tripResponse6.getTrips().get(0).getId();

        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse6.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse6, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId1,trackingNo);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Order is not assigned to Dc trip");

        //start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6=lastmileClient.startTrip(tripId1.toString(),"10");
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripUpdate),"trip is not started for the DC");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(tripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //Update trip order status as Delivered using DC SDA
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Complete DS SDA Trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

    }

    @Test( priority = 9, enabled = true, description = "TC ID: C24700,Create a Store , create and assign order to store,make order status as Failed_Delivered ,check the NDR status and update RTO order successful")
    public void processFDOrderAndCheckNDRStatusCreateRTOSuccesssful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,
                LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.FAILED_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff), "Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse3 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry = tripResponse3.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());
        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId),ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS, ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        // check trip shipment association table PS status
        TripShipmentAssociationResponse tripShipmentAssociationResponse03=storeHelper.findShipmentByTrip(reverseTripId,tenantId);
        Assert.assertTrue(tripShipmentAssociationResponse03.getTripShipmentAssociationEntryList().get(0).getStatus().toString().equalsIgnoreCase(com.myntra.lastmile.client.status.TripOrderStatus.PS.toString()));

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = storeHelper.closeMyntraTripWithStoreBag(tenantId, reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //check NDR status
        Map<String, Object> ndrStatus=storeHelper.getNDRStatus(AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS.toString(),"DELIVERY",LMS_CONSTANTS.CLIENTID,tenantId);
        if(ndrStatus.get("is_enabled").toString().equalsIgnoreCase("false")&&ndrStatus.get("is_rto_blocked").toString().equalsIgnoreCase("0")){
            //Update order as RTO
            String rtoResponse=storeHelper.createRTO(trackingNo,originPremiseId,MLShipmentUpdateEvent.RTO_CONFIRMED,ShipmentType.DL,ShipmentUpdateActivityTypeSource.MyntraLogistics,tenantId);
            Assert.assertTrue(rtoResponse.contains(ResponseMessageConstants.updateOrder),"Order is not updated successfully");
            //Validate ML shipment status in DC
            statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RTO_CONFIRMED);
            statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

            //validate OrderToShip status
            statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.RTO_CONFIRMED);

        }
    }

        @Test(priority = 10,  enabled = true, description = "TC ID: C24701,Create a Store , create and assign order to store,make order status as Failed_Delivered ,check the NDR status and update RTO order Unsuccessful")
    public void processFDOrderAndCheckNDRStatusInvalidCreateRTOSuccesssful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
          Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,
                LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.FAILED_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff), "Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse3 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry = tripResponse3.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId),ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS, ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        // check trip shipment association table PS status
        TripShipmentAssociationResponse tripShipmentAssociationResponse03=storeHelper.findShipmentByTrip(reverseTripId,tenantId);
        Assert.assertTrue(tripShipmentAssociationResponse03.getTripShipmentAssociationEntryList().get(0).getStatus().toString().equalsIgnoreCase(com.myntra.lastmile.client.status.TripOrderStatus.PS.toString()));

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = storeHelper.closeMyntraTripWithStoreBag(tenantId, reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //check NDR status
        Map<String, Object> ndrStatus=storeHelper.getNDRStatus(AttemptReasonCode.CASH_CARD_NOT_READY.toString(),"DELIVERY",LMS_CONSTANTS.CLIENTID,tenantId);
        if(!(ndrStatus.get("is_enabled").toString().equalsIgnoreCase("false")&&ndrStatus.get("is_rto_blocked").toString().equalsIgnoreCase("0"))){
            //Update order as RTO
            String rtoResponse=storeHelper.createRTO(trackingNo,originPremiseId,MLShipmentUpdateEvent.RTO_CONFIRMED,ShipmentType.DL,ShipmentUpdateActivityTypeSource.MyntraLogistics,tenantId);
            Assert.assertFalse(rtoResponse.contains(ResponseMessageConstants.updateOrder),"RTO is created successfully when the NDR status are not matching");
            //Validate ML shipment status in DC
            statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
            statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.RTO_CONFIRMED);
            //validate OrderToShip status
            statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);

        }
    }

    @Test( priority = 11, enabled = true,  description = "TC ID:C24702,Create a Store , Create order and assign it store,Create one more order and try to assign store bag to same store on same day, it should allow")
    public void processAssignMultipleStoreBagInSameDayToStore() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Create Mock order Till SH 2nd order to store on same day
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails01 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo01 = lmsOrderDetails01.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo01);

        //Create MasterBag
        ShipmentResponse shipmentResponse5 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = shipmentResponse5.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNo01, null, null, null, null, null, null, false).build();
        String response1 = masterBagClient_qa.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        if (response1.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo01,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId1, trackingNo01);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse01 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse01.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse01.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse01, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId1), String.valueOf(masterBagId1),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo01,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus1 = lastmileHelper.getShipmentOrderMapStatus(trackingNo01, masterBagId1);
        Assert.assertTrue(shipmentOrderMapStatus1.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers1 = new ArrayList();
        listTrackingNumbers1.add(trackingNo01);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse3 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId1), listTrackingNumbers1, String.valueOf(tripId1), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse3.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId1)));
        Assert.assertTrue(tripShipmentAssociationResponse3.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse3.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.DELIVERED.toString());
    }

    @Test(  priority = 12,enabled = true,  description = "TC ID: C24703, Create a Store , Create order and assign it store, make it as Delivered in store,Create one more order and try to assign store bag to same store on next day, it should allow")
    public void processDeliveredOrderAndAssignStoreBagToStoreOnNextDayWhichAreNotReconciled() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

            //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams1, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());


        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId1,EnumSCM.EXPECTED_IN_DC);

        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = storeHelper.closeMyntraTripWithStoreBag(tenantId, tripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));

        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId1);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
       Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId1,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber1, storeTenantId1);
        Long tripOrderAssignmentId1 = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId1));

        MLShipmentResponse mlShipmentResponse27 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId1 = String.valueOf(mlShipmentResponse27.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId1);
        TripOrderAssignmentResponse tripOrderAssignmentResponse6=storeHelper.deliveredStoreOrders(tripOrderAssignmentId1,AttemptReasonCode.DELIVERED, com.myntra.lastmile.client.code.utils.TripAction.UPDATE,
                "CASH",tenantId);
        Assert.assertEquals(tripOrderAssignmentResponse6.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        OrderResponse orderResponse4 = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse4, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Double codAmount=orderResponse4.getOrders().get(0).getCodAmount();

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId1,EnumSCM.DELIVERED);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponseo6=storeHelper.deliveredStoreOrders(tripOrderAssignmentId1,AttemptReasonCode.DELIVERED, com.myntra.lastmile.client.code.utils.TripAction.TRIP_COMPLETE,
                "CASH",tenantId);
        Assert.assertTrue(tripOrderAssignmentResponseo6.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip), "Updating Order status response message is not matching");

        //validate trip status
        statusPollingValidator.validateTripStatus(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff), "Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse3 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry = tripResponse3.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId1, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId),ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());

        //receive cod amount
        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseShipmentId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,Float.valueOf(codAmount.toString()),LMS_CONSTANTS.TENANTID);

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode1);

        //Create Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails01 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo01 = lmsOrderDetails01.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo01);

        //Create MasterBag
        ShipmentResponse shipmentResponse11 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId0 = shipmentResponse11.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId0));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId0,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId0), ShipmentType.DL, trackingNo01, null, null, null, null, null, null, false).build();
        String response1 = storeHelper.addShipmentToStoreBag(masterBagId0, shipmentUpdateInfo1);
        Assert.assertTrue(response1.contains(ResponseMessageConstants.assignOrderToStoreBag),"shipment was not added to store Bag for same store on next day when the store doesn't have any pending orders");
    }

    @Test( priority = 13, enabled = true, description = "TC ID: C24704, Create a Store , Create order and assign it store, make it as Failed_delivered in store,Receive it in DC on same day ,Create one more order and try to assign store bag to same store on next day, it should allow ")
    public void processReceiveFDOrderInDCAndAssignStoreBagToStoreOnNextDaySuccesssful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams1, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId1,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId1);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
      Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId1,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber1, storeTenantId1);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId1,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.FAILED_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId1,EnumSCM.FAILED_DELIVERY);

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff), "Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse3 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry = tripResponse3.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId1, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId),ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS, ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        // check trip shipment association table PS status
        TripShipmentAssociationResponse tripShipmentAssociationResponse03 = storeHelper.findShipmentByTrip(reverseTripId,tenantId);
        Assert.assertTrue(tripShipmentAssociationResponse03.getTripShipmentAssociationEntryList().get(0).getStatus().toString().equalsIgnoreCase(com.myntra.lastmile.client.status.TripOrderStatus.PS.toString()));

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId1,EnumSCM.FAILED_DELIVERY);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = storeHelper.closeMyntraTripWithStoreBag(tenantId, reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId1,EnumSCM.RTO_CONFIRMED);

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode1);

        //Create Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails01 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo01 = lmsOrderDetails01.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo01);

        //Create MasterBag
        ShipmentResponse shipmentResponse11 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId0 = shipmentResponse11.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId0));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId0,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId0), ShipmentType.DL, trackingNo01, null, null, null, null, null, null, false).build();
        String response1 = storeHelper.addShipmentToStoreBag(masterBagId0, shipmentUpdateInfo1);
        Assert.assertTrue(response1.contains(ResponseMessageConstants.assignOrderToStoreBag),"Shipment was assigned to Store which was not reconciled");
    }



    @Test(priority = 14,enabled = true,  dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows2",description = "TC ID: C24705,If there are pending shipments, next day we should not be able to add store bag for Forward, then create a reverse bag and pick them up and then try to reassign these pending shipments again to the same store and then again mark as FP and Unattempted,  next day we should not be able to add store bag and then again create a reverse bag and pick them up and then try to reassign these pending shipments again to the DIFFERENT store and mark then DL")
    public void process_AssignForwardOrderToStoreWithoutReconcile_DeliverForwardOrderThroughDIFFERENTStore_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                                                                                      String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code,
                                                                                                                      String name1, String ownerFirstName1, String ownerLastName1,String mobileNumber1,String code1) throws Exception {
        String searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated),"Store is not created");
        //get Store required values
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId = deliveryStaffEntry.getId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        String storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Shipments not added Successfully to trip");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());


        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        String forwardTrackingNumber = lmsHelper.getTrackingNumber(packetId1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //Create MasterBag
        ShipmentResponse shipmentResponse01 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = shipmentResponse01.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, forwardTrackingNumber, null, null, null, null, null, null, false).build();
        String response1 = storeHelper.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        Assert.assertTrue(response1.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse2 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry = tripResponse2.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        TripResponse tripResponse3=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId));
        storeValidator.validateTripStatus(tripResponse3, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId), ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //receive the failed delivery order in DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());
        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.closeMyntraTripWithStoreBag(tenantId, reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lmsServiceHelper.requeueOrder(packetId);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //Create MasterBag
        ShipmentResponse shipmentResponse6 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId2 = shipmentResponse6.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId2));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, ShipmentStatus.NEW.toString());
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo2 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId2), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response2 = masterBagClient_qa.addShipmentToStoreBag(masterBagId2, shipmentUpdateInfo2);
        if (response2.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId2, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse6 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse6.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse6.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse6, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse3 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId1), String.valueOf(masterBagId2),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse3.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus3 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId2);
        Assert.assertTrue(shipmentOrderMapStatus3.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse7 = tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers1 = new ArrayList();
        listTrackingNumbers1.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse4 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId2), listTrackingNumbers1, String.valueOf(tripId1), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse4.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId2)));
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        TripResponse tripResponse7 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers1, storeSDAId);
        Assert.assertTrue(tripResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
         Long storeTripId1 = tripResponse7.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId1.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId1 = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId1));
        MLShipmentResponse mlShipmentResponse19 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId1 = String.valueOf(mlShipmentResponse19.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId1);
        TripOrderAssignmentResponse tripOrderAssignmentResponse21 = tripClient_qa.failedDeliverStoreOrders(storeTripId1, mlShipmentEntriesId1, tripOrderAssignmentId1, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse21.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create MasterBag
        ShipmentResponse shipmentResponse8 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId3 = shipmentResponse8.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId3));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId3, ShipmentStatus.NEW.toString());
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo3 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId3), ShipmentType.DL, forwardTrackingNumber, null, null, null, null, null, null, false).build();
        String response4 = storeHelper.addShipmentToStoreBag(masterBagId3, shipmentUpdateInfo3);
        Assert.assertTrue(response4.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //Create Reverse Trip
        Long dcSDAId1 = null;
        DeliveryStaffResponse deliveryStaffResponse1 = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry1 = deliveryStaffResponse1.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry1.size() == 0)) {
            dcSDAId1 = deliveryStaffResponse1.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId1));

        TripResponse tripResponse5 = tripClient_qa.createTrip(originPremiseId, dcSDAId1);
        Assert.assertTrue(tripResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry1 = tripResponse5.getTrips();
        Long reverseTripId1 = null;
        if (!(lstTripEntry1.size() == 0)) {
            reverseTripId1 = lstTripEntry1.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId1));
        TripResponse tripResponse8=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId1));
        storeValidator.validateTripStatus(tripResponse8, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //create reverse MB
        String reverseShipmentId1 = tripClient_qa.assignReverseBagToTrip(reverseTripId1, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId1));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId1), ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = tripClient_qa.startTrip(reverseTripId1);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
      statusPollingValidator.validateTripStatusWithVNM(reverseTripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());


        //Collect orders from store
        List<String> lstreturnTrackingNumber1 = new ArrayList<>();
        lstreturnTrackingNumber1.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse5 = storeHelper.pickupReverseBagFromStore(reverseShipmentId1, lstreturnTrackingNumber1, String.valueOf(reverseTripId1), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());
        //receive the failed delivery order in DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());
        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse23 = tripClient_qa.closeMyntraTripWithStoreBag(tenantId, reverseTripId1);
        Assert.assertTrue(tripOrderAssignmentResponse23.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId1, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = lmsServiceHelper.requeueOrder(packetId);
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //TODO Assign the forward order to different store
        String searchParams1 = "code.like:" + code1;
        //Create Store
        StoreResponse storeResponse1 = lastmileHelper.createStore(name1, ownerFirstName1, ownerLastName1, latLong, emailId, mobileNumber1, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code1);
        //validate Store created successfully
        Assert.assertTrue(storeResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        Long storeHlPId1 = storeResponse1.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry1 = storeResponse1.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId1 = deliveryStaffEntry1.getId();
        String storeTenantId1 = storeResponse1.getStoreEntries().get(0).getTenantId();

        //Create MasterBag
        String originDCCity1 = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse3 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams1, "");
        String destinationStoreCity1 = storeResponse3.getStoreEntries().get(0).getCity();
        //Create MasterBag
        ShipmentResponse shipmentResponse11 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity1, destinationStoreCity1);
        Assert.assertTrue(shipmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId4 = shipmentResponse11.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId4));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo4 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId4), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response5 = masterBagClient_qa.addShipmentToStoreBag(masterBagId4, shipmentUpdateInfo4);
        if (response5.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus4 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId4, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus4.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID2 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse11 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID2));
        Assert.assertTrue(tripResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId2 = tripResponse11.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId2));
        Assert.assertTrue(tripResponse11.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse11, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse6 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId2), String.valueOf(masterBagId4),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse6.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus6 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId4);
        Assert.assertTrue(shipmentOrderMapStatus6.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse8 = tripClient_qa.startTrip(tripId2);
        Assert.assertTrue(tripOrderAssignmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers2 = new ArrayList();
        listTrackingNumbers2.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse8 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId4), listTrackingNumbers2, String.valueOf(tripId2), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse8.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId4)));
        Assert.assertTrue(tripShipmentAssociationResponse8.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse8.getStatus().getTotalCount() == 1);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.RTO_CONFIRMED);
        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse24 = tripClient_qa.closeMyntraTripWithStoreBag(tenantId, tripId2);
        Assert.assertTrue(tripOrderAssignmentResponse24.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId2, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId1,EnumSCM.EXPECTED_IN_DC);

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4, ShipmentStatus.DELIVERED.toString());

        //Assign order to store SDA
        TripResponse tripResponse12 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers2, storeSDAId1);
        Assert.assertTrue(tripResponse12.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successful");
        //Validate Myntra SDA trip is OFD
        Long storeTripId2 = tripResponse12.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId2.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId2, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId1,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment2 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber1, storeTenantId1);
        Long tripOrderAssignmentId3 = tripOrderAssignment2.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId3));
        MLShipmentResponse mlShipmentResponse45 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId2 = String.valueOf(mlShipmentResponse45.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId2);
        TripOrderAssignmentResponse tripOrderAssignmentResponse9 = tripClient_qa.deliverStoreOrders(storeTripId2, mlShipmentEntriesId2, tripOrderAssignmentId3, storeTenantId1,"CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse9.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId1,ShipmentUpdateEvent.DELIVERED.toString());
    }

    @Test( priority = 15, enabled = true, description = " TC ID: C24706, Do admin correction of the below : Failed DL in store : Mark LOST on the same day")
    public void process_AdminCorrection_FDInStore_MakeLOSSInSameDay_Successful() throws Exception {


        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status
       Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
       Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId,com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.OUT_FOR_DELIVERY);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.FAILED_DELIVERY);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Make ordr as LOST
        PickupResponse pickupResponse=lmsServiceHelper.forceUpdateForForward(packetId,EnumSCM.LOST,EnumSCM.LOST_IN_TRANSIT,tenantId,LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(pickupResponse.getStatus().getStatusType().toString(), ResponseMessageConstants.success1,"Order status is not updated");
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.LOST);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.LOST);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.FAILED_DELIVERY);

    }

    @Test(  priority = 16,enabled = true,  description = "TC ID:C24707, Do admin correction of the below : Failed DL in store :Failed DL in store : Mark LOST on the next day and then for EOD try to create new store bag and then try to process")
    public void process_AdminCorrection_FDInStore_MakeLOSSInNextDay_Successful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams1, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate the MB status
       Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId1,EnumSCM.EXPECTED_IN_DC);

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId1);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
          Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.OUT_FOR_DELIVERY);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId1,EnumSCM.OUT_FOR_DELIVERY);


        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber1, storeTenantId1);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.FAILED_DELIVERY);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId1,EnumSCM.FAILED_DELIVERY);


        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode1);

        //Make ordr as LOST
        PickupResponse pickupResponse=lmsServiceHelper.forceUpdateForForward(packetId,EnumSCM.LOST,EnumSCM.LOST_IN_TRANSIT,tenantId,LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(pickupResponse.getStatus().getStatusType().toString(), ResponseMessageConstants.success1,"Order status is not updated");
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.LOST);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,EnumSCM.LOST);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId1,EnumSCM.FAILED_DELIVERY);


        //Create Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);


        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);
        //Create MasterBag
        ShipmentResponse shipmentResponse11 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId3 = shipmentResponse11.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId3));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId3, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo3 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId3), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String response3 = storeHelper.addShipmentToStoreBag(masterBagId3, shipmentUpdateInfo3);
        Assert.assertTrue(response3.contains(ResponseMessageConstants.assignOrderToStoreBag),"order is not assigned to store bag when the old order as lost");

    }

    @Test( priority = 17, enabled = true, description = "TC ID:C24708,Live Return is there; try creating a return for same line ID")
    public void process_CreateReturnForSameLineId_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), com.myntra.returns.common.enums.ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());

        //Create return for Active Line ID
        ReturnResponse returnResponse3 = rmsServiceHelper.createReturn(lineEntry.getId(), com.myntra.returns.common.enums.ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse3.getStatus().getStatusType().toString().toLowerCase(),ResponseMessageConstants.errorStatus,"Return order is created with the return line Id - "+lineEntry.getId()+"- has the Live return order");

    }

    @Test( priority = 18, enabled = true, description = "TC ID: C24709,Live Return is there; try creating a return for same line ID for Exchange")
    public void process_CreateExchangeForLineIdWhichHasLiveReturn_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), com.myntra.returns.common.enums.ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());

        //Create Exchange
        ErrorResponse exchangeErrorResponse = (ErrorResponse) ppsServiceHelper.createExchange("" + orderID, "" + lineEntry.getId(), "DNL", 1, "" + lineEntry.getSkuId());
        Assert.assertEquals(exchangeErrorResponse.getErrorReason().contains("EXCHANGE_ORDER_CALL_FAILED"), true, "Exchange order is created for the line id which has active return ");

    }

    @Test( priority = 19, enabled = true, description = "TC ID:C24710, Live return is there; change state of forward shipment from DL to RTO/Lost/FD")
    public void process_ChangeForwardShipmentStatusfromDLToRTO_LOST_FD_WhenOrderHasLiveReturn_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), com.myntra.returns.common.enums.ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());

        //Change order status from DL to LOST
        PickupResponse pickupResponse=lmsServiceHelper.forceUpdateForForward(packetId,EnumSCM.LOST,EnumSCM.LOST_IN_TRANSIT,LASTMILE_CONSTANTS.TENANT_ID,LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(pickupResponse.getStatus().getStatusType().toString().toLowerCase(), ResponseMessageConstants.errorStatus,"Order status is updated as LOST when it has Live return");

        //Change order status from DL to FD
        PickupResponse pickupResponse1=lmsServiceHelper.forceUpdateForForward(packetId,EnumSCM.FD,AttemptReasonCode.REQUESTED_RE_SCHEDULE.toString(),LASTMILE_CONSTANTS.TENANT_ID,LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(pickupResponse1.getStatus().getStatusType().toString().toLowerCase(), ResponseMessageConstants.errorStatus,"Order status is updated as FD when it has Live return");

        //Change order status from DL to RTO
        PickupResponse pickupResponse3=lmsServiceHelper.forceUpdateForForward(packetId,EnumSCM.RTO,EnumSCM.RTO_FOUND_ORDER,LASTMILE_CONSTANTS.TENANT_ID,LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(pickupResponse3.getStatus().getStatusType().toString().toLowerCase(), ResponseMessageConstants.errorStatus,"Order status is updated As RTO when it has Live return");

    }

    @Test(  priority = 20,enabled = true,description = "TC ID:C24711, signature upload for DL status")
    public void process_OFD_Signature_DL_Successful()throws Exception{
        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.OFD, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse=tripClient_qa.getTripDetails(trackingNumber,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusMessage(),"Trip details retrieved successfully","Trip details not retrieved successfully");
        Long tripId=tripOrderAssignmentResponse.getTripOrders().get(0).getTripId();
        storeValidator.isNull(tripId.toString());

        EmptyResponse emptyResponse = lmsServiceHelper.uploadSignature(lmsHelper.getTrackingNumber(packetId));
        Assert.assertEquals(emptyResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Signature not Uploaded properly");
        String signatureUrl=lmsHelper.getSignatureUrl(trackingNumber,15);

        //Format of signature URL in ml_shipment:- lms/signature_trackingNumber_TenantId
        Assert.assertEquals(signatureUrl,"lms/signature_"+trackingNumber+"_"+ LMS_CONSTANTS.TENANTID+"");
        SignatureResponse signatureResponse=lmsServiceHelper.signaturePath(trackingNumber);
        Assert.assertEquals(signatureResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Signature path not retrieved Properly");
        Assert.assertEquals(signatureResponse.getTrackingNumber().toString(),trackingNumber,"Tracking number not matching the signature path response");
        Assert.assertNotNull(signatureResponse.getS3FileUrl(),"Signature Amazon s3 is NULL");
        SignatureResponse signatureResponse1=lmsServiceHelper.signatureDownload(trackingNumber);
        Assert.assertEquals(signatureResponse1.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Signature retrieval failed");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long)lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId,tripId), EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

    }

    @Test(  priority = 21,enabled = true, dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows", description = "TC ID:C24712,Create a Store and create the return order and assign pickup order to store, return the order to warehouse ")
    public void process_AdminCorrectionStatus_From_REJ_To_TR_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                                        String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created successfully");

        //get Store required values
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId = deliveryStaffEntry.getId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        Long skuId = orderReleaseEntry.getOrderLines().get(0).getSkuId();
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.UNASSIGNED);

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        Long storetripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storetripId.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.OUT_FOR_PICKUP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successfull in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE, tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKED_UP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff), "Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse1 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry = tripResponse1.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId), ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKED_UP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(returnTrackingNumber);
        TripShipmentAssociationResponse tripShipmentAssociationResponse = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS, ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKED_UP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(returnTrackingNumber.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.RECEIVED_IN_DC);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //complete DC trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,reverseTripId);
        statusPollingValidator.validateTripStatus(reverseTripId,EnumSCM.COMPLETED);

        //Qc pass in DC
        String mlOpenBoxQCPass = storeHelper.mlOpenBoxQCPass(String.valueOf(returnId), ShipmentUpdateEvent.PICKUP_SUCCESSFUL, returnTrackingNumber);
        Assert.assertTrue(mlOpenBoxQCPass.contains(ResponseMessageConstants.qcUpdate));
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKUP_SUCCESSFUL);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
    /*
     * TODO : For open box return decline not available
     * //reject the return
        ReturnResponse returnResponse4=storeHelper.declineReturn(returnId, ReturnActionCode.DECLINE_RETURN,"ML",returnTrackingNumber,skuId);
        Assert.assertTrue(returnResponse4.getStatus().getStatusMessage().contains(""), "Return decline is happend successfully after completing the Green channel process");
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse5=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse5, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_REJECTED);

        //change status from REJ to RT
        PickupResponse pickupResponse=lmsServiceHelper.forceUpdateForReturn(returnId.toString(),EnumSCM.RT,EnumSCM.RETURNED,"",tenantId,LMS_CONSTANTS.CLIENTID);
        Assert.assertEquals(pickupResponse.getStatus().getStatusMessage(),"SUCCESS","Admin correction status is not successful");
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse6=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse6, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL);*/

    }




}
