package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.TransporterResponse;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.tms.container.ContainerEntry;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.List;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class NearestWarehouseRestock {
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private LastmileClient lastmileClient = new LastmileClient();
    private TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMSHelper lmsHepler = new LMSHelper();
    private MasterBagServiceHelper masterBagServiceHelper=new MasterBagServiceHelper();
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    TripClient_QA tripClient_qa=new TripClient_QA();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
    }
    @Test(priority = 1, dataProviderClass = LMSTestsDP.class,enabled = true, description=" TC ID: C24690, create return for 28 warehouse and do restock in 36 wh")
    public void process_NearestWHRestock_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID1 = lmsHepler.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LMSConstants.IN_HOUSE_COURIER, "28", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId1);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        String deliveryCenterName=orderResponse.getOrders().get(0).getDeliveryCenterCode();
        String whID=orderResponse.getOrders().get(0).getWarehouseId();
        String courierCode=orderResponse.getOrders().get(0).getCourierOperator();
        String pinCode=orderResponse.getOrders().get(0).getZipcode();
        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LMS_PINCODE.ML_BLR, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2=storeHelper.getReturnStatusInLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse2, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnHub=returnResponse2.getDomainReturnShipments().get(0).getReturnHubCode();
        // Validate the respective RMS and LMS return fields
        String nearestWH=storeHelper.getNearestWHId(whID, courierCode, pinCode.substring(0, 2));
        String expectedReturnHub = lmsHelper.getReturnHubCodeForWarehouse.apply(Long.parseLong(nearestWH)).toString();
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2ForNearestWHRestock(returnId.toString(), nearestWH, expectedReturnHub);
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        String mobileNumber=tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId,returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"TnB Order is assigned to old Dc to make status as FD");

        //Start trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.OUT_FOR_PICKUP);

        //update order as Pickup Successfull
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripId), ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Long tripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(tripOrderId.toString());
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 =lastmileClient.updatePickupInTrip(tripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.PICKUP_SUCCESSFUL);

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(returnTrackingNumber.substring(2, returnTrackingNumber.length()), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.PICKUP_SUCCESSFUL);

        //Complete DS SDA Trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 =lastmileClient.updatePickupInTrip(tripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusType().toString().toLowerCase().equalsIgnoreCase("success"),"Trip is not completed");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, TripStatus.COMPLETED.toString());

        //CreateMB from DC To WH
        MasterbagDomain shipmentResponse7 = masterBagServiceHelper.createMasterBag(deliveryCenterName,returnHub, ShippingMethod.NORMAL,"ML",LMS_CONSTANTS.TENANTID);
        Long reverseMBId=shipmentResponse7.getId();
        storeValidator.isNull(String.valueOf(reverseMBId));
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId, ShipmentStatus.NEW.toString());
        //Add order to MB
        masterBagServiceHelper.addShipmentsToMasterBag(reverseMBId,returnTrackingNumber,ShipmentType.PU,LMS_CONSTANTS.TENANTID);
        //close Reverse MB
        MasterbagUpdateResponse shipmentResponse8 = masterBagServiceHelper.closeMasterBag(reverseMBId,LMS_CONSTANTS.TENANTID);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());
        //Receive MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(deliveryCenterName, reverseMBId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseMBId), ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse=storeHelper.getMappedTransportHubDetails(LMS_CONSTANTS.TENANTID,deliveryCenterName);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub=hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterName,transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(deliveryCenterName,transporterHub, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, reverseMBId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseMBId), ShipmentStatus.CLOSED.toString());

        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success1), "Container is not shipped");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());

        //Receive Container In Transport Hub
        ContainerResponse containerResponse3 = (ContainerResponse) storeHelper.receiveContainerInTransportHub.apply(containerId,transporterHub);
        Assert.assertTrue(containerResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Container is not received in TRansport hub");

        //MasterBag inscan in WH
        String mbInscanResponse = null;
        mbInscanResponse=storeHelper.masterBagInscanProcess(returnTrackingNumber, reverseMBId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);

        if(!(mbInscanResponse.contains(ResponseMessageConstants.success))){
            //TODO calling this API twice, since it fails 1st time.
            mbInscanResponse = storeHelper.masterBagInscanProcess(returnTrackingNumber, reverseMBId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                    ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
            Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success)," Master bag inscan failed in WH");
        }
        Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success));
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.RETURN_IN_TRANSIT);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RRC.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.DELIVERED_TO_SELLER.toString());

    }

}
