package com.myntra.apiTests.erpservices.lastmile.tests;

import com.amazonaws.util.StringUtils;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagType;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.scm.enums.ShipmentType;
import com.myntra.test.commons.testbase.BaseTest;
import lombok.SneakyThrows;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class EKReturn extends BaseTest {

    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private LmsServiceHelper lmsServiceHelper=new LmsServiceHelper();
    private StatusPollingValidator statusPollingValidator=new StatusPollingValidator();
    MasterBagServiceHelper masterBagServiceHelper=new MasterBagServiceHelper();
    LMS_3plHelper lms_3plHelper=new LMS_3plHelper();
    private String pincode="400053";

    /**
     * Serviceability for receivereturnInWHAfterPickupSuccessful TestCase
     *
     * Group Id, Pincode, Hub Code, Courier, Service Type, Shipping Method, City Name, City Code, Area Name, Area Code, Cod limit, Item value limit, COD, Online, Card on delivery, Fragile, Hazmat, Large, Jewellery, Normal
     * 4,400053,RT-BLR,EK,OPEN_BOX_PICKUP,NORMAL,Dummy,Dummy,Dummy,EKART,49999,49999,0,1,0,1,1,1,1,1
     * 4,400053,RT-BLR,EK,CLOSED_BOX_PICKUP,NORMAL,Dummy,Dummy,Dummy,EKART,49999,49999,0,1,0,1,1,1,1,1
     */

    @Test(enabled = true,priority = 1,description = "receive the return in Wh after pickup successful")
    public void receivereturnInWHAfterPickupSuccessful() throws Exception {
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, "400053", "EK", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", pincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");
        String returnWHId=returnResponse1.getDomainReturnShipments().get(0).getReturnFacilityId();
        //manifest
        lms_returnHelper.manifestPickups(returnId.toString(),ReturnMode.OPEN_BOX_PICKUP.toString(), CourierCode.EK.toString());
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        //update as OUT_FOR_PICKUP
        lmsServiceHelper.updateStatusForLMS3PL(returnTrackingNumber,EnumSCM.OUT_FOR_PICKUP,ShipmentType.PU,LMS_CONSTANTS.TENANTID);
        String pickupResponse1= storeHelper.findPickupFromSourceReturnId(returnId.toString(),LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1,ShipmentStatus.OUT_FOR_PICKUP);

        //update as PICKUP_DONE
        lmsServiceHelper.updateStatusForLMS3PL(returnTrackingNumber,ShipmentStatus.PICKUP_DONE.toString(),ShipmentType.PU,LMS_CONSTANTS.TENANTID);
        String pickupResponse2= storeHelper.findPickupFromSourceReturnId(returnId.toString(),LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2,ShipmentStatus.PICKUP_DONE);

        //update as PICKUP_SUCCESSFUL
        lmsServiceHelper.updateStatusForLMS3PL(returnTrackingNumber,ShipmentStatus.PICKUP_SUCCESSFUL.toString(),ShipmentType.PU,LMS_CONSTANTS.TENANTID);
        String pickupResponse3= storeHelper.findPickupFromSourceReturnId(returnId.toString(),LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3,ShipmentStatus.PICKUP_SUCCESSFUL);
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID),EnumSCM.PICKUP_SUCCESSFUL );
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),EnumSCM.RL);
        //Receive order in warehouse
        String receiveReturnStatusMsg=lmsServiceHelper.receiveReturnInWH(returnWHId,CourierCode.EK.toString(),returnId.toString(),returnTrackingNumber,LASTMILE_CONSTANTS.TENANT_ID,LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(receiveReturnStatusMsg,"Shipment has been updated successfully","Return order is not received in WH");
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID),ShipmentStatus.DELIVERED_TO_SELLER.toString());
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),EnumSCM.RRC);
    }

    @Test(enabled = true,description = "TC ID - C28829 : Ecom large forward & Ecom return")
    @SneakyThrows
    public void ecomForwardAndEcomReturn(){

       //ECOM forward flow
        String orderId = lmsHelper.createMockOrder(EnumSCM.IS , "560018" , "EEL" , "36" , EnumSCM.NORMAL , "cod" , false , true);
        String packetId=omsServiceHelper.getPacketId(orderId);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.INSCANNED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        String originPremisesCode = orderResponse.getOrders().get(0).getDispatchHubCode();
        StringUtils.isNullOrEmpty(trackingNo);

        // Create master bag && add to master bag && close mb
        MasterbagDomain masterbagDomain= masterBagServiceHelper.createMasterBag(originPremisesCode , "ELCECOM" , ShippingMethod.NORMAL ,"EEL" , LMS_CONSTANTS.TENANTID );
        Long masterBagId = masterbagDomain.getId();
        StringUtils.isNullOrEmpty(masterBagId.toString());
        masterBagServiceHelper.addShipmentsToMasterBag(masterBagId,trackingNo, com.myntra.lms.client.status.ShipmentType.DL,LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,EnumSCM.NEW);

        masterBagServiceHelper.closeMasterBag(masterBagId,LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,EnumSCM.CLOSED);
        statusPollingValidator.validateOrderStatus(packetId,EnumSCM.ADDED_TO_MB);

        //Ship MB
        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to ship masterBag");

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                com.myntra.lms.client.status.ShipmentType.DL, "EEL");
        statusPollingValidator.validateOrderStatus(packetId,EnumSCM.OUT_FOR_DELIVERY);
        // Deliver the 3pl Order
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.DELIVERED , com.myntra.lms.client.status.ShipmentType.DL, "EEK");
        statusPollingValidator.validateOrderStatus(packetId,EnumSCM.DELIVERED);

        // return flow with EEOB courier code
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT,
                "418", "test Open box address ", "6135071", "560018", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");
        String returnWHId=returnResponse1.getDomainReturnShipments().get(0).getReturnFacilityId();
        //manifest
        lms_returnHelper.manifestPickups(returnId.toString(),ReturnMode.OPEN_BOX_PICKUP.toString(), CourierCode.EK.toString());
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        //update as OUT_FOR_PICKUP
        lmsServiceHelper.updateStatusForLMS3PL(returnTrackingNumber,EnumSCM.OUT_FOR_PICKUP,ShipmentType.PU,LMS_CONSTANTS.TENANTID);
        String pickupResponse1= storeHelper.findPickupFromSourceReturnId(returnId.toString(),LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1,ShipmentStatus.OUT_FOR_PICKUP);

        //update as PICKUP_DONE
        lmsServiceHelper.updateStatusForLMS3PL(returnTrackingNumber,ShipmentStatus.PICKUP_DONE.toString(),ShipmentType.PU,LMS_CONSTANTS.TENANTID);
        String pickupResponse2= storeHelper.findPickupFromSourceReturnId(returnId.toString(),LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2,ShipmentStatus.PICKUP_DONE);

        //update as PICKUP_SUCCESSFUL
        lmsServiceHelper.updateStatusForLMS3PL(returnTrackingNumber,ShipmentStatus.PICKUP_SUCCESSFUL.toString(),ShipmentType.PU,LMS_CONSTANTS.TENANTID);
        String pickupResponse3= storeHelper.findPickupFromSourceReturnId(returnId.toString(),LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3,ShipmentStatus.PICKUP_SUCCESSFUL);
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID),EnumSCM.PICKUP_SUCCESSFUL );
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),EnumSCM.RL);
        //Receive order in warehouse
        String receiveReturnStatusMsg=lmsServiceHelper.receiveReturnInWH(returnWHId,CourierCode.EK.toString(),returnId.toString(),returnTrackingNumber,LASTMILE_CONSTANTS.TENANT_ID,LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(receiveReturnStatusMsg,"Shipment has been updated successfully","Return order is not received in WH");
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID),ShipmentStatus.DELIVERED_TO_SELLER.toString());
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),EnumSCM.RRC);
    }
}



