package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.otp.OTPManager;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.codes.LMSConstants;
import org.apache.commons.lang.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Bharath.MC
 * @since Aug-2019
 */
public class OTPGeneration {
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TripClient_QA tripClient_qa=new TripClient_QA();

    @Test(description = "Unit test for OTP Generation")
    public void testGoogleOTPMock() throws Exception {
        String tripId = "1234";
        Integer otp = OTPManager.generateOTP(tripId);
        Assert.assertTrue(OTPManager.validateOTP(tripId, otp), "OPT Generation Mock failed");
        Assert.assertFalse(OTPManager.validateOTP(tripId, 0), "Validation failed for invalid OTP");
        Assert.assertFalse(OTPManager.validateOTP(StringUtils.reverse(tripId), 0), "Validation failed for invalid TripID");
    }

    @Test(description = "Sanity case for OTP generation at Lastmile")
    public void testOTPGenerationSanity() throws Exception {
        String orderId = null, packetId = null, trackingNumber = null, deliveryStaffID = null;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        Long tripId = 0l;
        Integer generatedOtp = 0;
        try {
            orderId = lmsHelper.createMockOrder(EnumSCM.SH, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                    "cod", false, true);
            packetId = omsServiceHelper.getPacketId(orderId);
            trackingNumber = lmsHelper.getTrackingNumber(packetId);
            System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
            //Lastmile operations
            deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
            TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
            Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
            tripId = tripResponse.getTrips().get(0).getId();
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
            //OTP Generation
            generatedOtp = tripClient_qa.generateOTP(tripId);
            Assert.assertTrue(OTPManager.validateOTP(String.valueOf(tripId), generatedOtp), "Generated OTP is invalid");
            lastmileOperations.startTrip(tripId, trackingNumber);
            lastmileOperations.updateDeliveryTripByTrackingNumber(trackingNumber);
            lastmileOperations.completeTrip(trackingNumber);
        } finally {
            System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
            System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        }
    }
}