package com.myntra.apiTests.erpservices.lastmile.dp;

import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.lastmile.client.code.utils.DeliveryCenterRegion;
import com.myntra.lastmile.client.code.utils.DeliveryCenterType;
import com.myntra.lastmile.client.code.utils.ReconType;

import com.myntra.lordoftherings.Toolbox;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class Lastmile_DeliveryCenterTestsDP {

    String pattern = "YYYY-MM-DD HH:MM:SS";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "IN"));
    String date = simpleDateFormat.format(new Date());


    @DataProvider
    public static Object[][] createDC(ITestContext testContext) {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");

        Integer masterBagCapacity = new Integer(100);
        Long store = new Long(1l);
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));

        Object[] arr1 = {DeliveryCenterType.ML, "LA" + String.valueOf(contactNumber).substring(5, 9), "" + String.valueOf(contactNumber).substring(5, 9), "ML", store, "Manager", String.valueOf(contactNumber), "Automation Address", "Bangalore",
                "BLR", "Karnataka", LASTMILE_CONSTANTS.DC_PINCODE, masterBagCapacity, "T-SEC", "LastmileAutomationAddressLine1", "LastmileAutomationAddressLine2", "Bangalore", LASTMILE_CONSTANTS.DC_PINCODE, "KARNATAKA", DeliveryCenterRegion.NORTH, "Instakart", "123", true, true, true, true, true, ReconType.ROLLING_RECON};
//

        Object[] arr2 = {DeliveryCenterType.ML, "LA" + String.valueOf(contactNumber).substring(4, 8), "" + String.valueOf(contactNumber).substring(5, 9), "ML", store, "Manager", String.valueOf(contactNumber), "Automation Address", "Bangalore",
                "BLR", "Karnataka", LASTMILE_CONSTANTS.DC_PINCODE, masterBagCapacity, "T-SEC", "LastmileAutomationAddressLine1", "LastmileAutomationAddressLine2", "Bangalore", LASTMILE_CONSTANTS.DC_PINCODE, "KARNATAKA", DeliveryCenterRegion.NORTH, "Instakart", "123", true, true, true, true, true, ReconType.EOD_RECON};


        //  Object[] arr1 = {DeliveryCenterType.ML,"LastmileAutomation","LastmileAutomation", "Myntra Logistics", 1l, "Manager", "9999999999", "Automation Address", "Bangalore",  "BLR", "Karnataka", "560068", i, "2", "LastmileAutomationAddressLine1", "LastmileAutomationAddressLine2", "Bangalore", "560068", "KARNATAKA"};


       /* Object[] arr2 = {"MADC", "myntra_auto_dc_1", "auto_test_manger_1", "1", "auto_test_test_123", "Banaglore", "YLH_12", "KA",
                "989898", "0", "0", "1", "1", "ML", "1200002213", "FRANCHISE", "801", "DELIVERY_CENTER added successfully", EnumSCM.SUCCESS};
        Object[] arr3 = {"MADC", "myntra_auto_dc_1", "auto_test_manger_1", "1", "auto_test_test_123", "Banaglore", "YLH_12", "KA",
                "989897", "0", "0", "1", "1", "ML", "1200002213", "OTHER_LOGISTICS", "801", "DELIVERY_CENTER added successfully", EnumSCM.SUCCESS};
        Object[] arr4 = {"MADC", "myntra_auto_dc_1", "auto_test_manger_1", "1", "auto_test_test_123", "Banaglore", "YLH_12", "KA",
                "919191", "0", "0", "1", "1", "ML", "1200002213", "WHPL", "801", "DELIVERY_CENTER added successfully", EnumSCM.SUCCESS};
        Object[] arr5 = {"MADC", "myntra_auto_dc_1", "auto_test_manger_1", "3", "auto_test_test_123", "Banaglore", "YLH_12", "KA",
                "929292", "0", "0", "1", "1", "3PL", "1200002213", "WHPL", "801", "DELIVERY_CENTER added successfully", EnumSCM.SUCCESS};
       */
        Object[][] dataSet = new Object[][]{arr1, arr2};
        //Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }

}
