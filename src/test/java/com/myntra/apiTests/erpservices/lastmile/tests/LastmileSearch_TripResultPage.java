package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileTripPlanningSearchHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileTripResultSearchHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lastmile.validator.TripOrderAssignmentResponseValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.DB.IQueries;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import lombok.SneakyThrows;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class LastmileSearch_TripResultPage implements StatusPoller {
    HashMap<String, String> pincodeDCMap;

    //creating HashMap to maintain pincode and DcId
    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
    }

    @Test(description = "TC ID:28038,Trip Result Restrict Only 30 Days Default Loading and giveTrip Result Restrict date as 31 Days and check its allowing ")
    @SneakyThrows
    public void validateDateErrorMessage() {
        String validDate = MessageFormat.format(LASTMILE_CONSTANTS.Trip_Result_End_URL.TRIP_RESULT_INVALID_DATE_RANGE_END_URL, LMS_CONSTANTS.TENANTID, ShipmentType.DL.toString(),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -30).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0).replace(" ", "%20"));
        OrderResponse orderResponseForValidDate = lastmileTripResultSearchHelper.tripResultSerchGetAllFailedFilterSearch(validDate);
        Assert.assertEquals(orderResponseForValidDate.getStatus().getStatusMessage(), "Success", "data is not retrieved successfully when the date range given as 30 days");

        String InvalidDate = MessageFormat.format(LASTMILE_CONSTANTS.Trip_Result_End_URL.TRIP_RESULT_INVALID_DATE_RANGE_END_URL, LMS_CONSTANTS.TENANTID, ShipmentType.DL.toString(),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -31).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0).replace(" ", "%20"));
        OrderResponse orderResponseForInValidDate = lastmileTripResultSearchHelper.tripResultSerchGetAllFailedFilterSearch(InvalidDate);
        Assert.assertEquals(orderResponseForInValidDate.getStatus().getStatusMessage(), "Please provide the valid date range.", "data retrieved successfully when the date range given as 31 days");
    }

    @Test(description = "TC ID:C28037,validate Onhold Pickups orders on the trip results page based on the Given date range, validate in DB it should have same record")
    @SneakyThrows
    public void testSearchTripResultBasedOnConditionOnholdPickup() {
        String DcId = "1";
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560102", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        statusPollingValidator.validateReturnStatusRMS(returnID.toString(), EnumSCM.LPI);
        //reconcile fail
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));
        //reconcile fail
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();
        //reconcile fail
        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");
        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");
        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");
        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);
        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ONHOLD_PICKUP_WITH_DC, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
        OrderResponse orderResponse = lastmileTripResultSearchHelper.tripResultSerchgetAllOnHoldOrdersSearch(MessageFormat.format(LASTMILE_CONSTANTS.Trip_Result_End_URL.DCID_CREATED_DATE_ONHOLD_PICKUP_API_END_URL,
                LMS_CONSTANTS.TENANTID, ShipmentType.TRY_AND_BUY, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0).replace(" ", "%20")));
        Assert.assertEquals(orderResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Trip search failed");
        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Result_Search.DCID_CREATED_DATE_ONHOLD_PICKUP_SEARCH_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID, LMS_CONSTANTS.TENANTID, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0), EnumSCM.ONHOLD_PICKUP_WITH_DC));
        //validate the recent created recodes should come first
        Assert.assertEquals(trackingNo, (String) dbQuery.get(0).get("tracking_number"), "first data not matched DB to api");
        lastmileTripPlanningSearchHelper.compareAPIDBValues_Order_Response(orderResponse, dbQuery);
    }

    @Test(description = "TC ID:C28035 - validate Failed Pick-up orders on the trip results page based on the Given date range, validate in DB it should have same record")
    @SneakyThrows
    public void testSearchTripResultBasedOnConditionFailedPickup() {
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418",
                "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        statusPollingValidator.validateReturnStatusRMS(returnID.toString(), EnumSCM.LPI);
        OrderResponse getReturnTrackingNumber = lmsServiceHelper.getReturnsInLMS(returnID.toString());
        String returnTrackingNumber = getReturnTrackingNumber.getOrders().get(0).getTrackingNumber();
        lms_returnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.FAILED_PICKUP);
        OrderResponse orderResponse = lastmileTripResultSearchHelper.tripResultSerchGetAllFailedFilterSearch(MessageFormat.format(LASTMILE_CONSTANTS.Trip_Result_End_URL.DCID_AND_CREATED_DATE_FAILED_PICKUP_API_END_URL,
                LMS_CONSTANTS.TENANTID, ShipmentType.PU.toString(), DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0).replace(" ", "%20")));

        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Result_Search.DCID_CREATED_DATE_FAILED_PICKUP_SEARCH_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID, LMS_CONSTANTS.TENANTID, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0), ShipmentType.PU.toString(), EnumSCM.ASSIGNED_TO_DC));
        //validate the recent created recodes should come first
        Assert.assertEquals(returnTrackingNumber, (String) dbQuery.get(0).get("tracking_number"), "first data not matched DB to api");
        lastmileTripPlanningSearchHelper.compareAPIDBValues_Order_Response(orderResponse, dbQuery);
    }

    @Test(description = "TC ID -:C28033,validate Failed delivery orders on the trip results page based on the given date range, validate in DB it should have same record")
    @SneakyThrows
    public void testSearchTripResultBasedOnConditionFailedDelivery() {
        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.FD, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String tracking_Number = lmsHelper.getTrackingNumber(packetId);
        OrderResponse orderResponse = lastmileTripResultSearchHelper.tripResultSerchGetAllFailedFilterSearch(MessageFormat.format(LASTMILE_CONSTANTS.Trip_Result_End_URL.DCID_AND_CREATED_DATE_FAILED_DELIVERY_API_END_URL,
                LMS_CONSTANTS.TENANTID, ShipmentType.DL.toString(), DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0).replace(" ", "%20")));

        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Result_Search.DCID_AND_CREATED_DATE_FAILED_DELIVERY_SEARCG_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID, LMS_CONSTANTS.TENANTID, EnumSCM.RECEIVED_IN_DC, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0), ShipmentType.DL.toString()));
        //validate the recent created recodes should come first
        Assert.assertEquals(tracking_Number, (String) dbQuery.get(0).get("tracking_number"), "first data not matched DB to api");
        lastmileTripPlanningSearchHelper.compareAPIDBValues_Order_Response(orderResponse, dbQuery);
    }

    @Test(description = "tc:C28036, Get the Failed Try_And_Buy data for given date range on trip results page")
    @SneakyThrows
    public void testSearchTripResultBasedOnConditionFailedTryAndBuy() {
        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.FD, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true));
        String tracking_Number = lmsHelper.getTrackingNumber(packetId);
        OrderResponse orderResponse = lastmileTripResultSearchHelper.tripResultSerchGetAllFailedFilterSearch(MessageFormat.format(LASTMILE_CONSTANTS.Trip_Result_End_URL.DCID_AND_CREATED_DATE_TRY_AND_BUY_API_END_URL,
                LMS_CONSTANTS.TENANTID, ShipmentType.TRY_AND_BUY.toString(), DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0).replace(" ", "%20")));

        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Result_Search.DCID_AND_CREATED_DATE_TRY_AND_BUY_SEARCH_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID, LMS_CONSTANTS.TENANTID, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0), ShipmentType.TRY_AND_BUY.toString(), EnumSCM.RECEIVED_IN_DC));
        //validate the recent created recodes should come first
        Assert.assertEquals(tracking_Number, (String) dbQuery.get(0).get("tracking_number"), "first data not matched DB to api");
        lastmileTripPlanningSearchHelper.compareAPIDBValues_Order_Response(orderResponse, dbQuery);
    }

    @Test(description = "tc:C28034, Get the Failed Exchange data for given date range on trip results page")
    @SneakyThrows
    public void testSearchTripResultBasedOnConditionFailedExchange() {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripId, LMS_CONSTANTS.TENANTID);
        com.myntra.apiTests.erpservices.lastmile.validator.TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateTripOrderStatus(EnumSCM.OFD);
        tripOrderAssignmentResponseValidator.validateTripId(tripId);
        tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNumber);
        tripOrderAssignmentResponseValidator.validateExchangeOrderId(packetId);
        Assert.assertEquals(tripOrderAssignmentResponse.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant Id in response");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        String exchangeTrackingNumber = tripOrderAssignmentResponse1.getTripOrders().get(0).getTrackingNumber();
        OrderResponse orderResponse = lastmileTripResultSearchHelper.tripResultSerchGetAllFailedFilterSearch(MessageFormat.format(LASTMILE_CONSTANTS.Trip_Result_End_URL.DCID_AND_CREATED_DATE_FAILED_EXCHANGE_API_END_URL,
                LMS_CONSTANTS.TENANTID, ShipmentType.EXCHANGE.toString(), DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1).replace(" ", "%20"),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0).replace(" ", "%20")));


        List<Map<String, Object>> dbQuery = lastmileTripPlanningSearchHelper.getDbData(IQueries.formatQuery(IQueries.Lastmile_Trip_Result_Search.DCID_AND_CREATED_DATE_FAILED_EXCHANGE_SEARCH_QUERY,
                LASTMILE_CONSTANTS.ML_BLR_DCID, LMS_CONSTANTS.TENANTID, DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, -1),
                DateTimeHelper.generateDate(LASTMILE_CONSTANTS.dateFormat, 0), ShipmentType.EXCHANGE.toString(), EnumSCM.RECEIVED_IN_DC));

        //validate the recent created recodes should come first
        Assert.assertEquals(exchangeTrackingNumber, (String) dbQuery.get(0).get("tracking_number"), "first data not matched DB to api");
        lastmileTripPlanningSearchHelper.compareAPIDBValues_Order_Response(orderResponse, dbQuery);
    }
}
