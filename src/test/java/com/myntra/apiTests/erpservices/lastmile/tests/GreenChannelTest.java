package com.myntra.apiTests.erpservices.lastmile.tests;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

import com.myntra.apiTests.erpservices.lastmile.service.LMSOrderDetailsClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripOrderAssignmentClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.Premise;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnActionCode;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.testbase.BaseTest;

import java.util.ArrayList;
import java.util.List;

public class GreenChannelTest extends BaseTest {

    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();

    private LMSOrderDetailsClient lmsOrderDetailsClient;
    private MasterBagClient masterBagClient;
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa = new LMSOrderDetailsClient_QA();
    StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    TripClient_QA tripClient_qa = new TripClient_QA();
    LastmileClient lastmileClient = new LastmileClient();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    ReturnHelper returnHelper = new ReturnHelper();
    LMS_OpenBoxReturnHelper lms_openBoxReturnHelper = new LMS_OpenBoxReturnHelper();
    int noOfOrders = 2;

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);

    }

    @Test(enabled = true, description = "C9836:Positive CLOSED BOX :Approve GreenChannel for return status RETURN_CREATED")
    public void processApproveGreenChannelForReturnStatusRETURN_CREATEDSuccessful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        lms_returnHelper.processClosedBoxPickup(String.valueOf(returnId), EnumSCM.PICKED_UP_SUCCESSFULLY);

        //receive return order in wh
        String response = storeHelper.receiveReturn(returnId.toString(), returnTrackingNumber, LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertTrue(response.contains("Shipment has been updated successfully"), "Return order is not received in WH");
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse4 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse4, com.myntra.lastmile.client.status.ShipmentStatus.DELIVERED_TO_SELLER);
    }

    @Test(enabled = true, description = "C9837:Positive ClOSED BOX : Approve GreenChannel for return status \"PICKUP_DONE\"")
    public void processApproveGreenChannelForReturnStatusPICKUP_DONESuccessful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //  storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3,ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        String activity_type2 = "OUT_FOR_PICKUP";
        String activity_sub_type2 = "";
        String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        String pickupResponse2 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, ShipmentStatus.OUT_FOR_PICKUP);

        String activity_type3 = "PICKUP_DONE";
        String activity_sub_type3 = "";
        String file_path3 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        String pickupResponse1 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1, ShipmentStatus.PICKUP_DONE);

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

    }

    @Test(enabled = true, description = "C9838:Positive Closed Box : Approve GreenChannel for return status \"RECEIVED_AT_RETURNS_PROCESSING_CENTER\"")
    public void processApproveGreenChannelForReturnStatus_RECEIVED_AT_RETURNS_PROCESSING_CENTERSuccessful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //  storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3,ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        String activity_type2 = "OUT_FOR_PICKUP";
        String activity_sub_type2 = "";
        String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        String pickupResponse2 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, ShipmentStatus.OUT_FOR_PICKUP);

        String activity_type3 = "PICKUP_DONE";
        String activity_sub_type3 = "";
        String file_path3 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        String pickupResponse1 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1, ShipmentStatus.PICKUP_DONE);

        lms_returnHelper.updateReturnReceiveEvents(returnId.toString(), "DE");
        String pickupResponse = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse, ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER);

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

    }

    @Test(enabled = true, description = "C9839:Positive Closed box : Approve GreenChannel for return status \"OUT_FOR_PICKUP\"")
    public void processApproveGreenChannelForReturnStatus_OUT_FOR_PICKUP_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //   storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3,ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        String activity_type2 = "OUT_FOR_PICKUP";
        String activity_sub_type2 = "";
        String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        String pickupResponse2 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, ShipmentStatus.OUT_FOR_PICKUP);

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");


        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

    }

    @Test(enabled = true, description = "C9840:Positive Scenario : declining return should not allowed when green channel is approved")
    public void processDecliningReturnNotAllowedWhenGreenChannelIsApprovedSuccessful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        Long skuId = orderReleaseEntry.getOrderLines().get(0).getSkuId();
        storeValidator.isNull(skuId.toString());
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        // check declining return
        ReturnResponse returnResponse4 = storeHelper.declineReturn(returnId, ReturnActionCode.DECLINE_RETURN, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, returnTrackingNumber, skuId);
        Assert.assertTrue(returnResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.createreturnAfterGreenchannelApproved), "Return decline is happend successfully after completing the Green channel process");

    }

    @Test(enabled = true, description = "C9841:Positive scenario : Declining return is allowed when green channel is not approved")
    public void process_DecliningReturnAllowed_WhenGreenChannel_IsNotApproved_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        Long skuId = orderReleaseEntry.getOrderLines().get(0).getSkuId();
        storeValidator.isNull(skuId.toString());
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

    }

    @Test(enabled = true, description = "C9842:Negative Scenario Closed Box : Green channel approve is not allowed in state \"PICKUP_DETAIL_UPDATED\"")
    public void processGreenChannelApproveNotAllowedState_PICKUP_DETAIL_UPDATED_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //    storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3,ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully when the order status is PICKUP_DETAILS_UPDATED ");

    }

    @Test(enabled = true, description = "C9843:Positive Scenario : verify the green_channel_approved in lms and is_refunded in rms is set true in db when green channel approved via api / ui")
    public void process_ApproveGreenChannelForReturnCheckLMSRMSStatus_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

    }

    @Test(enabled = true, description = "C9844:Negative Scenario Closed Box : QC fail is not allowed after green channel is approved")
    public void process_QCFailNotAllowed_AfterApproveGreenChannel_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");


        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //    storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3,ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        String activity_type2 = "OUT_FOR_PICKUP";
        String activity_sub_type2 = "";
        String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        String pickupResponse2 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, ShipmentStatus.OUT_FOR_PICKUP);

        String activity_type3 = "PICKUP_DONE";
        String activity_sub_type3 = "";
        String file_path3 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        String pickupResponse1 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1, ShipmentStatus.PICKUP_DONE);

        lms_returnHelper.updateReturnReceiveEvents(returnId.toString(), "DE");
        String pickupResponse = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse, ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER);

        //mark QC fail
        String closeBoxQCUpdate = storeHelper.performClosedBoxReturnQC(returnId.toString(), ItemQCStatus.ON_HOLD, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType);
        Assert.assertTrue(closeBoxQCUpdate.contains("is already in approval status: APPROVED"), "After approved the green channel, Return order is allowing to make status as ON_HOLD");
    }

    @Test(enabled = true, description = "C9845:Negative scenario Closed Box : Approve Green Channel is not allowed for RETURN_REJECTED -> APPROVE_GREEN_CHANNEL")
    public void process_ApproveGreenChannelNotAllowedForRETURN_REJECTED_Status_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //    storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3,ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        String activity_type2 = "OUT_FOR_PICKUP";
        String activity_sub_type2 = "";
        String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        String pickupResponse2 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, ShipmentStatus.OUT_FOR_PICKUP);

        String activity_type3 = "PICKUP_DONE";
        String activity_sub_type3 = "";
        String file_path3 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        String pickupResponse1 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1, ShipmentStatus.PICKUP_DONE);

        lms_returnHelper.updateReturnReceiveEvents(returnId.toString(), "DE");
        String pickupResponse = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse, ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER);

        //mark QC fail
        String closeBoxQCUpdate = storeHelper.performClosedBoxReturnQC(returnId.toString(), ItemQCStatus.ON_HOLD, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType);
        Assert.assertEquals(closeBoxQCUpdate, ResponseMessageConstants.qcUpdate, "before approved the green channel, Return order is not allowing to make order status as ON_HOLD");
        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse2 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse2, ReturnStatus.RL);
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse3 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse3, com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER);

        //Return reject from CC
        lms_returnHelper.returnApproveOrRejectClosedBox(returnId.toString(), "REJECTED");
        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse4 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse4, ReturnStatus.RRSH);
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse5 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse5, com.myntra.lastmile.client.status.ShipmentStatus.REJECTED_ONHOLD_RETURN_WITH_RETURNS_PROCESSING_CENTER);

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.returnRejectedGreenChannelApprove))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.returnRejectedGreenChannelApprove), "Green channel update is completed successfully when the Return status is in REJECTED status");
    }

    @Test(enabled = true, description = "C9848:Negative scenario Closed Box : Approve Green Channel is not allowed for RETURN_REJECTED -> APPROVE_GREEN_CHANNEL")
    public void process_AfterApproveGreenChannel_QC_PASS_AllowedForRETURN_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //     storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3,ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        String activity_type2 = "OUT_FOR_PICKUP";
        String activity_sub_type2 = "";
        String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        String pickupResponse2 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, ShipmentStatus.OUT_FOR_PICKUP);

        String activity_type3 = "PICKUP_DONE";
        String activity_sub_type3 = "";
        String file_path3 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        String pickupResponse1 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1, ShipmentStatus.PICKUP_DONE);

        lms_returnHelper.updateReturnReceiveEvents(returnId.toString(), "DE");
        String pickupResponse = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse, ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER);

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //mark QC PASS
        String closeBoxQCUpdate=storeHelper.performClosedBoxReturnQC(returnId.toString(), ItemQCStatus.PASSED,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType);
        Assert.assertEquals(closeBoxQCUpdate,ResponseMessageConstants.qcUpdate,"before approved the green channel, Return order is not allowing to make order status as ON_HOLD");
        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse2 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse2, ReturnStatus.RL);
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse3 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse3, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL);

    }

    @Test(enabled = true, description = "C9850:Verify Greenchannel for open box and closed box return")
    public void process_GreenChannelApproveFunctionality_Open_Close_BoxReturn_Successful() throws Exception {

        //Green channel approve functionality for Close box return
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        //green channel approve functionality for open box return
        //Create Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        OrderResponse orderResponse1 = lmsClient.getOrderByOrderId(packetId1);
        Assert.assertTrue(orderResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse1, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create Return
        OrderReleaseEntry orderReleaseEntry1 = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry1 = omsServiceHelper.getOrderLineEntry(orderReleaseEntry1.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse01 = rmsServiceHelper.createReturn(lineEntry1.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse01.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId1 = returnResponse01.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId1));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId1));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse03 = storeHelper.getReturnStatusInLMS(returnId1.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse03, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber1 = returnResponse03.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber1);
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber1 = new ArrayList<>();
        lstTrackingNumber1.add(returnTrackingNumber1);
        String filePath1 = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber1, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse1 = storeHelper.uploadGreenChannel(filePath1);
        if (!(greenChannelResponse1.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse1 = storeHelper.uploadGreenChannel(filePath1);
        }
        Assert.assertTrue(greenChannelResponse1.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse04 = storeHelper.getReturnStatusInLMS(returnId1.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse04.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse04.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse05 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId1));
        Assert.assertEquals(returnResponse05.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse05.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

    }

    @Test(enabled = true, description = "C9851:Negative scenario : Decline green channel approved returns in prism / QC fail is not allowed")
    public void process_DeclineGreenChannelApprovedReturns_Successful() throws Exception {

        //Green channel approve functionality for Close box return
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        Long skuId = orderReleaseEntry.getOrderLines().get(0).getSkuId();
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        // check declining return
        ReturnResponse returnResponse4 = storeHelper.declineReturn(returnId, ReturnActionCode.DECLINE_RETURN, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, returnTrackingNumber, skuId);
        Assert.assertTrue(returnResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.createreturnAfterGreenchannelApproved), "Return decline is happend successfully after completing the Green channel process");

    }

    @Test(enabled = true, description = "C9852:Closed box Negative cases upload status : After approve Greenchannel --> PICKUP_REJECTED. is not allowed")
    public void process_AfterApproveGreenChannel_PICKUP_REJECTED_NotAllowedForRETURN_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT,
                "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore",
                "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //   storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3, ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        String activity_type2 = "OUT_FOR_PICKUP";
        String activity_sub_type2 = "";
        String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        String pickupResponse2 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, ShipmentStatus.OUT_FOR_PICKUP);

        String activity_type3 = "PICKUP_REJECTED";
        String activity_sub_type3 = "";
        String file_path3 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        //TODO status should remain in OUT_FOR_PICKUP, PICKUP_REJECTED not allowed when green channel approved
        String pickupResponse1 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1, ShipmentStatus.OUT_FOR_PICKUP);

    }

    @Test(enabled = true, description = "C9853:Negative Scenario Closed Box : Green channel approve is not allowed in state \"PICKUP_REJECTED\"")
    public void process_GreenChannelApproval_NotAllowedWhenRETURN_Status_PICKUP_REJECTED_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT,
                "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore",
                "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //     storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3, ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        String activity_type2 = "OUT_FOR_PICKUP";
        String activity_sub_type2 = "";
        String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        String pickupResponse2 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, ShipmentStatus.OUT_FOR_PICKUP);

        String activity_type3 = "PICKUP_REJECTED";
        String activity_sub_type3 = "";
        String file_path3 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        String pickupResponse1 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1, ShipmentStatus.PICKUP_REJECTED);

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains("TRANSITION_NOT_CONFIGURED from RETURN_REJECTED -> APPROVE_GREEN_CHANNEL."))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("TRANSITION_NOT_CONFIGURED from RETURN_REJECTED -> APPROVE_GREEN_CHANNEL."), "Green channel update is completed successful when return status is in PICKUP_REJECTED ");

    }

    @Test(enabled = true, description = "C9854 : Negative Scenario Closed Box : Green channel approve is not allowed in state \"FAILED_PICKUP\"")
    public void process_GreenChannelApproval_NotAllowedForRETURN_Status_FAILED_PICKUP_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT,
                "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore",
                "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //     storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3, ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        String activity_type2 = "OUT_FOR_PICKUP";
        String activity_sub_type2 = "";
        String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        String pickupResponse2 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, ShipmentStatus.OUT_FOR_PICKUP);

        String activity_type3 = "FAILED_PICKUP";
        String activity_sub_type3 = "";
        String file_path3 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        String pickupResponse1 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1, ShipmentStatus.FAILED_PICKUP);

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

    }

    @Test(enabled = true, description = "C9855 : Negative Scenario Closed Box : Green channel approve is not allowed in state \"ON_HOLD_WITH_RETURN_PROCESSING_CENTER\"")
    public void process_ApproveGreenChannelNotAllowedForONHOLD_WITHRETURN_PROCESSING_CENTER_Status_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //     storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3,ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        String activity_type2 = "OUT_FOR_PICKUP";
        String activity_sub_type2 = "";
        String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        String pickupResponse2 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, ShipmentStatus.OUT_FOR_PICKUP);

        String activity_type3 = "PICKUP_DONE";
        String activity_sub_type3 = "";
        String file_path3 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        String pickupResponse1 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1, ShipmentStatus.PICKUP_DONE);

        lms_returnHelper.updateReturnReceiveEvents(returnId.toString(), "DE");
        String pickupResponse = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse, ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER);

        //mark QC fail
        String closeBoxQCUpdate = storeHelper.performClosedBoxReturnQC(returnId.toString(), ItemQCStatus.ON_HOLD, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType);
        Assert.assertEquals(closeBoxQCUpdate, ResponseMessageConstants.qcUpdate, "before approved the green channel, Return order is not allowing to make order status as ON_HOLD");
        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse2 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse2, ReturnStatus.RL);
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse3 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse3, com.myntra.lastmile.client.status.ShipmentStatus.ON_HOLD_WITH_RETURN_PROCESSING_CENTER);

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains("TRANSITION_NOT_CONFIGURED from ON_HOLD_WITH_RETURN_PROCESSING_CENTER -> APPROVE_GREEN_CHANNEL."))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }

        Assert.assertTrue(greenChannelResponse.contains("TRANSITION_NOT_CONFIGURED from ON_HOLD_WITH_RETURN_PROCESSING_CENTER -> APPROVE_GREEN_CHANNEL."), "Green channel update is completed successfully when the Return status is in ONHOLD_WITH_RETURNS_PROCESSING_CENTER status");
    }

    @Test(enabled = true, description = "C9856 : Negative scenario Closed Box : Approve Green Channel is not allowed for PICKUP_REJECTED -> APPROVE_GREEN_CHANNEL")
    public void process_GreenChannelApproval_NotAllowed_For_ReturnStatus_PICKUP_REJECTED_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT,
                "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore",
                "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest close box pickup
        lms_returnHelper.manifestClosedBoxPickups();
        Assert.assertTrue(lms_returnHelper.isPickupManifested(String.valueOf(returnId)), "Pickup associated with return - " + returnId + " is NOT manifested");

        String tracking_number = lms_returnHelper.getPickupTrackingNoOfReturn(returnId.toString());
        String courier_code = "DE";
        String shipment_type = "PU";
        String activity_type1 = "PICKUP_DETAILS_UPDATED";
        String activity_sub_type1 = "SHIPMENT_ACCEPTED_BY_COURIER";

        String file_path = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type1, activity_sub_type1);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path);
        String pickupResponse3 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        //  storeValidator.validatePickupShipmentStatusInLMS(pickupResponse3, ShipmentStatus.PICKUP_CREATED);//TODO need to check the status

        String activity_type2 = "OUT_FOR_PICKUP";
        String activity_sub_type2 = "";
        String file_path2 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type2, activity_sub_type2);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path2);
        String pickupResponse2 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse2, ShipmentStatus.OUT_FOR_PICKUP);

        String activity_type3 = "PICKUP_REJECTED";
        String activity_sub_type3 = "";
        String file_path3 = lms_returnHelper.generateClosedBoxStatusCSV(tracking_number, courier_code, shipment_type, activity_type3, activity_sub_type3);
        lms_returnHelper.uploadClosedBoxStatusFile(file_path3);
        String pickupResponse1 = storeHelper.findPickupFromSourceReturnId(returnId.toString(), LASTMILE_CONSTANTS.TENANT_ID, Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));
        storeValidator.validatePickupShipmentStatusInLMS(pickupResponse1, ShipmentStatus.PICKUP_REJECTED);

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains("TRANSITION_NOT_CONFIGURED from RETURN_REJECTED -> APPROVE_GREEN_CHANNEL."))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("TRANSITION_NOT_CONFIGURED from RETURN_REJECTED -> APPROVE_GREEN_CHANNEL."), "Green channel update is completed successfully when the return status as PICKUP_REJECTED");

    }

    @Test(enabled = true, description = "C9857 : Negative Bulk update : upload invalid return id")
    public void process_GreenChannelApproval_WITH_INVALID_RETURNID_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT,
                "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore",
                "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnId.toString()+"0");
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains("Return " + returnId.toString() + "0" + " not found."))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }

        Assert.assertTrue(greenChannelResponse.contains("Return " + returnId.toString() + "0" + " not found."), "Green channel update is completed successfully for invalid return id ");
    }

    @Test(enabled = true, description = "C9858 : Negative Bulk update : upload with pickup id")
    public void process_AfterApproveGreenChannel_WITH_PICKUPID_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 9L, RefundMode.NEFT,
                "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.closeBoxPincode, "Bangalore",
                "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse0 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse0, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse0.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //get pickup id for return
        String pickupId = lms_returnHelper.getPickupIdOfReturn(returnId.toString());

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(pickupId);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains("Return " + pickupId + " not found."))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }

        Assert.assertTrue(greenChannelResponse.contains("Return " + pickupId + " not found."), "Green channel update is completed successfully for return id ");
    }

    /**
     * Whenever green channel is triggered, we cannot mark pickup status as PQCP.
     * It will directly go to Return_Successful and PICKUP_SUCCESSFUL
     *
     * @throws Exception
     */
    @Test(description = "TC ID - C9846 - Negative Scenario : After Green channel approved QC on hold - reject is not allowed for ML orders")
    public void process_ApproveGreenChannel_ForQCOnHoldReject_NotAllowed() throws Exception {
        //Create Mock order Till DL
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        //validate OrderToShip status
        OrderResponse orderResponse1 = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse1, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create Return
        OrderReleaseEntry orderReleaseEntry1 = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry1 = omsServiceHelper.getOrderLineEntry(orderReleaseEntry1.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse01 = rmsServiceHelper.createReturn(lineEntry1.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse01.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse01.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse03 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse03, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse03.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        String mobileNumber = tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not " + LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "TnB Order is assigned to old Dc to make status as FD");

        //Start trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase("Trip Started Successfully"), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.OUT_FOR_PICKUP);

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse0 = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse0.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse0 = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse0.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse04 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse04.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse04.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse05 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse05.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse05.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        //update order as Pickup Successfull QC Pending
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lastmileClient.findOrdersByTrip(tripId, ShipmentType.PU);
        Long tripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(tripOrderId.toString());
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = storeHelper.updatePickupInTrip(tripOrderId, EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.PICKED_UP);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_SUCCESSFUL.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.PICKUP_SUCCESSFUL.toString());
    }

    @Test(description = "TC ID - C9847 - Negative Scenario : After Green channel approved , QC hold approve is allowed For ML orders")
    public void process_ApproveGreenChannel_ForQCOnHoldApprove_NotAllowed() throws Exception {
        //Create Mock order Till DL
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        //validate OrderToShip status
        OrderResponse orderResponse1 = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse1, ShipmentStatus.DELIVERED);
        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create Return
        OrderReleaseEntry orderReleaseEntry1 = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry1 = omsServiceHelper.getOrderLineEntry(orderReleaseEntry1.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse01 = rmsServiceHelper.createReturn(lineEntry1.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse01.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse01.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse03 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse03, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse03.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        String mobileNumber = tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not " + LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "TnB Order is assigned to old Dc to make status as FD");

        //Start trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase("Trip Started Successfully"), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.OUT_FOR_PICKUP);

        //TODO Green channel approval is not upload successfully when you hit first time
        List<String> lstTrackingNumber = new ArrayList<>();
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse04 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse04.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse04.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse05 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse05.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse05.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        //update order as Pickup Successfull QC Pending
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lastmileClient.findOrdersByTrip(tripId, ShipmentType.PU);
        Long tripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(tripOrderId.toString());
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = storeHelper.updatePickupInTrip(tripOrderId, EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.PICKED_UP);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_SUCCESSFUL.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.PICKUP_SUCCESSFUL.toString());
    }

    @Test(description = "TC ID - C9849 - Positive Bulk update - Green Channel Approve functionality")
    public void process_BulkUpdate_GreenChannel_Functionality() throws Exception {

        List<String> lstTrackingNumber = new ArrayList<>();
        List<String> lstReturnId = new ArrayList<>();
        for (int i = 0; i <= noOfOrders - 1; i++) {
            //Create Mock order Till DL
            String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
            String packetId = omsServiceHelper.getPacketId(orderId);
            //validate OrderToShip status
            OrderResponse orderResponse1 = lmsClient.getOrderByOrderId(packetId);
            Assert.assertTrue(orderResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
            storeValidator.validateOrderStatus(orderResponse1, ShipmentStatus.DELIVERED);
            //get order tracking number
            OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
            String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
            storeValidator.isNull(trackingNo1);

            //Create Return
            OrderReleaseEntry orderReleaseEntry1 = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
            OrderLineEntry lineEntry1 = omsServiceHelper.getOrderLineEntry(orderReleaseEntry1.getOrderLines().get(0).getId().toString());
            ReturnResponse returnResponse01 = rmsServiceHelper.createReturn(lineEntry1.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
            Assert.assertEquals(returnResponse01.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
            Long returnId = returnResponse01.getData().get(0).getId();
            storeValidator.isNull(String.valueOf(returnId));
            lstReturnId.add(returnId.toString());
            //validate return status in rms
            Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
            ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
            storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
            Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

            //validate return status in LMS
            com.myntra.lms.client.domain.response.ReturnResponse returnResponse03 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
            storeValidator.validateReturnOrderStatusInLMS(returnResponse03, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
            String returnTrackingNumber = returnResponse03.getDomainReturnShipments().get(0).getTrackingNumber();
            storeValidator.isNull(returnTrackingNumber);
            Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
            Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");
            lstTrackingNumber.add(returnTrackingNumber);
        }

        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("2/2 updated successfully"), "Green channel update is not completed successfully");
        for (String returnId : lstReturnId) {
            //check the return shipment status
            com.myntra.lms.client.domain.response.ReturnResponse returnResponse04 = storeHelper.getReturnStatusInLMS(returnId, LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(returnResponse04.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
            Assert.assertEquals(returnResponse04.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
            //check the refund status in RMS
            Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
            ReturnResponse returnResponse05 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
            Assert.assertEquals(returnResponse05.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
            Assert.assertEquals(returnResponse05.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");
        }
    }

    @Test(description = "TC ID - C9859 - Positive Scenario Open Box: Approve Green Channel is allowed after CC pre-approves from prism after manifestation")
    public void process_GreenChannelUploadAllowed_CCPreaproveAfterManifestation() throws Exception {
        List<String> lstTrackingNumber = new ArrayList<>();

        //Create Mock order Till DL
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));
        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest the return
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //cc pre aprove
        returnHelper.approveAtCC(returnId.toString());
        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("1/1 updated successfully"), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

    }

    @Test(description = "TC ID - C9860 - Positive Scenario Open Box: Approve Green Channel is allowed on trip start, mark PQCP")
    public void process_GreenChannelUploadNotAllowed_CCPreaproveOnPQCP() throws Exception {
        List<String> lstTrackingNumber = new ArrayList<>();

        //Create Mock order Till DL
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));
        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest the return
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        String mobileNumber = tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not " + LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "TnB Order is assigned to old Dc to make status as FD");

        //Start trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase("Trip Started Successfully"), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.OUT_FOR_PICKUP);

        //update order as Pickup Successfull QC Pending
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lastmileClient.findOrdersByTrip(tripId, ShipmentType.PU);
        Long tripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(tripOrderId.toString());
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = storeHelper.updatePickupInTrip(tripOrderId, EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate return status
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.PICKED_UP);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.PICKUP_DONE.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.PICKUP_DONE.toString());

        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("1/1 updated successfully"), "Green channel update is not completed successfully");
        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

    }

    @Test(description = "TC ID - C9861 - Negative Scenario Open Box :Approve Green Channel is allowed after CC preapproves from prism on trip start, mark Pickup On Hold")
    public void process_GreenChannelUploadNotAllowed_CCPreaproveOnPickupOnHOLD() throws Exception {
        List<String> lstTrackingNumber = new ArrayList<>();

        //Create Mock order Till DL
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));
        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //manifest the return
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        String mobileNumber = tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not " + LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "TnB Order is assigned to old Dc to make status as FD");

        //Start trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase("Trip Started Successfully"), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.OUT_FOR_PICKUP);

        //update order as Pickup Successfull QC Pending
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lastmileClient.findOrdersByTrip(tripId, ShipmentType.PU);
        Long tripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(tripOrderId.toString());
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = storeHelper.updatePickupInTrip(tripOrderId, EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate return status
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.PICKED_UP);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.PICKUP_DONE.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.PICKUP_DONE.toString());
        //mark it as onhold
        returnHelper.tripUpdateAtSDA(tripOrderId.toString(), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, returnTrackingNumber);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.ONHOLD_PICKUP_WITH_DC);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.ONHOLD_RETURN_WITH_COURIER.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.ONHOLD_PICKUP_WITH_COURIER.toString());

        //cc pre aprove
        returnHelper.approveAtCC(returnId.toString());
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.ONHOLD_PICKUP_WITH_DC);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_SUCCESSFUL.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.PICKUP_SUCCESSFUL.toString());

        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("0/1 updated successfully"), "Green channel update is not completed successfully");
    }

    /**
     * After green channel approval we cannot mark pickup status as PQCP
     *
     * @throws Exception
     */
    @Test(enabled = true, description = "TC ID - C9862 - Negative Scenario Open Box : Approve Green Channel . DA marks `Requested re-schedule`, close trip, DC Manager re-queues, PQCP -> On-hold -CC Reject is not allowed")
    public void process_GreenChannelUpload_SDAMarkReschedule_DCManagerReques_PQCP() throws Exception {
        List<String> lstTrackingNumber = new ArrayList<>();

        //Create Mock order Till DL
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));
        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("1/1 updated successfully"), "Green channel update is not completed successfully");
        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        //manifest the return
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        String mobileNumber = tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not " + LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "TnB Order is assigned to old Dc to make status as FD");

        //Start trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase("Trip Started Successfully"), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.OUT_FOR_PICKUP);

        //update order as Pickup Successfull QC Pending
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lastmileClient.findOrdersByTrip(tripId, ShipmentType.PU);
        Long tripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(tripOrderId.toString());
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = storeHelper.updatePickupInTrip(tripOrderId, EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate return status
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.FAILED_PICKUP);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_CREATED.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.FAILED_PICKUP.toString());

        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = storeHelper.updatePickupInTrip(tripOrderId, EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE, EnumSCM.TRIP_COMPLETE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip), "Status message is not matching");

        //reque the FP order
        storeHelper.createRTO(returnTrackingNumber, 5l, MLShipmentUpdateEvent.UNASSIGN, ShipmentType.PU, ShipmentUpdateActivityTypeSource.MyntraLogistics, LMS_CONSTANTS.TENANTID);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.UNASSIGNED);

        //Create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse1 = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        String mobileNumber1 = tripResponse1.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber1);
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not " + LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse1, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = lastmileClient.assignOrderToTrip(tripId1, returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "TnB Order is assigned to old Dc to make status as FD");

        //Start trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase("Trip Started Successfully"), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.OUT_FOR_PICKUP);

        //update order as Pickup Successfull QC Pending
        TripOrderAssignmentResponse tripOrderAssignmentResponse0 = lastmileClient.findOrdersByTrip(tripId1, ShipmentType.PU);
        Long tripOrderId1 = tripOrderAssignmentResponse0.getTripOrders().get(0).getId();
        storeValidator.isNull(tripOrderId1.toString());
        TripOrderAssignmentResponse tripOrderAssignmentResponse01 = storeHelper.updatePickupInTrip(tripOrderId1, EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate return status
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.PICKED_UP);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_SUCCESSFUL.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.PICKUP_SUCCESSFUL.toString());
    }

    /**
     * After green channel approval we cannot mark pickup status as PQCP
     *
     * @throws Exception
     */
    @Test(enabled = true, description = "TC ID - C9863 - Negative Scenario Open Box : Approve Green Channel . DA marks `Requested re-schedule`, close trip, DC Manager re-queues, PQCP -> On-hold -CC Approve is allowed")
    public void process_GreenChannelUpload_SDAMarkReschedule_DCManagerReques_PQCP_ONHoldApproveAllowed() throws Exception {
        List<String> lstTrackingNumber = new ArrayList<>();

        //Create Mock order Till DL
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));
        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("1/1 updated successfully"), "Green channel update is not completed successfully");
        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        //manifest the return
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        String mobileNumber = tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not " + LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "TnB Order is assigned to old Dc to make status as FD");

        //Start trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase("Trip Started Successfully"), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.OUT_FOR_PICKUP);

        //update order as Pickup Successfull QC Pending
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lastmileClient.findOrdersByTrip(tripId, ShipmentType.PU);
        Long tripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(tripOrderId.toString());
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = storeHelper.updatePickupInTrip(tripOrderId, EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate return status
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.FAILED_PICKUP);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_CREATED.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.FAILED_PICKUP.toString());

        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = storeHelper.updatePickupInTrip(tripOrderId, EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE, EnumSCM.TRIP_COMPLETE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip), "Status message is not matching");

        //reque the FP order
        storeHelper.createRTO(returnTrackingNumber, 5l, MLShipmentUpdateEvent.UNASSIGN, ShipmentType.PU, ShipmentUpdateActivityTypeSource.MyntraLogistics, LMS_CONSTANTS.TENANTID);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.UNASSIGNED);

        //Create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse1 = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        String mobileNumber1 = tripResponse1.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber1);
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not " + LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse1, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = lastmileClient.assignOrderToTrip(tripId1, returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "TnB Order is assigned to old Dc to make status as FD");

        //Start trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase("Trip Started Successfully"), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.OUT_FOR_PICKUP);

        //update order as Pickup Successfull QC Pending
        TripOrderAssignmentResponse tripOrderAssignmentResponse0 = lastmileClient.findOrdersByTrip(tripId1, ShipmentType.PU);
        Long tripOrderId1 = tripOrderAssignmentResponse0.getTripOrders().get(0).getId();
        storeValidator.isNull(tripOrderId1.toString());
        TripOrderAssignmentResponse tripOrderAssignmentResponse01 = storeHelper.updatePickupInTrip(tripOrderId1, EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate return status
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.PICKED_UP);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_SUCCESSFUL.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.PICKUP_SUCCESSFUL.toString());
    }

    /**
     * After green channel approval SDA cannot mark pickup status as PQCP
     *
     * @throws Exception
     */
    @Test(enabled = true, description = "TC ID - C9864 - Negative scenario Open Box Alpha seller :Approve Green channel SDA marks PQCP,After trip complete DC Manager marks On-Hold and CC Rejects is not allowed")
    public void process_GreenChannelUpload_SDAMarkPQCP_DCManagerMarkONHOLD_CCRejectNotAllowed() throws Exception {
        List<String> lstTrackingNumber = new ArrayList<>();

        //Create Mock order Till DL
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));
        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("1/1 updated successfully"), "Green channel update is not completed successfully");
        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        //manifest the return
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        String mobileNumber = tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not " + LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "TnB Order is assigned to old Dc to make status as FD");

        //Start trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase("Trip Started Successfully"), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.OUT_FOR_PICKUP);

        //update order as Pickup Successfull QC Pending
        TripOrderAssignmentResponse tripOrderAssignmentResponse0 = lastmileClient.findOrdersByTrip(tripId, ShipmentType.PU);
        Long tripOrderId = tripOrderAssignmentResponse0.getTripOrders().get(0).getId();
        storeValidator.isNull(tripOrderId.toString());
        TripOrderAssignmentResponse tripOrderAssignmentResponse01 = storeHelper.updatePickupInTrip(tripOrderId, EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate return status
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.PICKED_UP);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_SUCCESSFUL.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.PICKUP_SUCCESSFUL.toString());
    }

    /**
     * After green channel approval SDA cannot mark pickup status as PQCP
     *
     * @throws Exception
     */
    @Test(enabled = true, description = "TC ID - C9865 - Negative scenario Open Box Alpha seller :Approve Green channel SDA marks PQCP,After trip complete DC Manager marks On-Hold and CC Rejects is not allowed")
    public void process_GreenChannelUpload_SDAMarkPQCP_DCManagerRejectNotAllowed() throws Exception {
        List<String> lstTrackingNumber = new ArrayList<>();

        //Create Mock order Till DL
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        //get order tracking number
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(String.valueOf(returnId));
        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse1, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber = returnResponse1.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse1.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        lstTrackingNumber.add(returnTrackingNumber);
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("1/1 updated successfully"), "Green channel update is not completed successfully");
        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse2.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        Assert.assertEquals(returnResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse3.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        //manifest the return
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        String mobileNumber = tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not " + LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "TnB Order is assigned to old Dc to make status as FD");

        //Start trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase("Trip Started Successfully"), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.OUT_FOR_PICKUP);

        //update order as Pickup Successfull QC Pending
        TripOrderAssignmentResponse tripOrderAssignmentResponse0 = lastmileClient.findOrdersByTrip(tripId, ShipmentType.PU);
        Long tripOrderId = tripOrderAssignmentResponse0.getTripOrders().get(0).getId();
        storeValidator.isNull(tripOrderId.toString());
        TripOrderAssignmentResponse tripOrderAssignmentResponse01 = storeHelper.updatePickupInTrip(tripOrderId, EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");

        //Validate return status
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.PICKED_UP);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.RETURN_SUCCESSFUL.toString());
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), ShipmentStatus.PICKUP_SUCCESSFUL.toString());
    }

    @Test(enabled = true, description = "TC - ID : Green Channel For Self Ship : Before performing QC not allowed")
    public void process_GreenchannelApproval_SelfShipReturn_BeforeQCOperation() throws Exception {
        List<String> lstTrackingNumber = new ArrayList<>();

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase("ORDER(s) retrieved successfully"), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse3, ReturnStatus.LPI);
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse03 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse03, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");
        lstTrackingNumber.add(returnResponse03.getDomainReturnShipments().get(0).getTrackingNumber());
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("0/1 updated successfully"), "Green channel update is not completed successfully");

        //update QC pass for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_SUCCESSFUL, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        com.myntra.returns.response.ReturnResponse returnResponse4 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse4, ReturnStatus.RL);
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse5 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse5, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL);
        String returnTrackingNumber = returnResponse5.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

    }

    @Test(enabled = true, description = "TC - ID : Green Channel For Self Ship : Before performing QC not allowed")
    public void process_GreenchannelApproval_SelfShipReturn_AfterQCOperation() throws Exception {
        List<String> lstTrackingNumber = new ArrayList<>();
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase("ORDER(s) retrieved successfully"), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.SELF_SHIP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.selfShipPincode, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();

        //validate return status in rms
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse3, ReturnStatus.LPI);
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse03 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse03, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getTrackingNumber(), "Tracking number is generated before completing self ship QC update");

        //update QC pass for self ship orders
        String qcUpdateResponse = storeHelper.selfShipPickupQCUpdates(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID),
                ShipmentUpdateEvent.RETURN_SUCCESSFUL, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.MyntraLogistics, "5", Premise.PremiseType.DELIVERY_CENTER);
        Assert.assertTrue(qcUpdateResponse.equalsIgnoreCase("Shipment Updation Successful"));

        //validate return status in rms
        com.myntra.returns.response.ReturnResponse returnResponse4 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse4, ReturnStatus.RL);
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse5 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse5, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL);
        String returnTrackingNumber = returnResponse5.getDomainReturnShipments().get(0).getTrackingNumber();
        Assert.assertNotNull(returnTrackingNumber, "Tracking number is not generated after completing self ship QC update");

        com.myntra.lms.client.domain.response.ReturnResponse returnResponse6 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse6, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL);
        lstTrackingNumber.add(returnResponse6.getDomainReturnShipments().get(0).getTrackingNumber());
        String filePath = storeHelper.createCSVFileForGreenChannel(lstTrackingNumber, LMS_CONSTANTS.TENANTID);
        String greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        if (!(greenChannelResponse.contains(ResponseMessageConstants.reshipToCustomer))) {
            greenChannelResponse = storeHelper.uploadGreenChannel(filePath);
        }
        Assert.assertTrue(greenChannelResponse.contains("0/1 updated successfully"), "Green channel update is not completed successfully");
    }
}
