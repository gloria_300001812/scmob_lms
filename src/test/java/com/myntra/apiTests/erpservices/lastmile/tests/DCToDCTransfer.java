package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.LMSOrderDetailsClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClientV2_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.response.TransporterResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.TryAndBuyItemStatus;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class DCToDCTransfer {
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    private LastmileClient lastmileClient = new LastmileClient();
    private TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_CreateOrder lms_createOrder=new LMS_CreateOrder();
    TripClient_QA tripClient_qa=new TripClient_QA();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa=new MLShipmentClientV2_QA();

    private MLShipmentClientV2 mlShipmentServiceV2Client;

    private LMSOrderDetailsClient lmsOrderDetailsClient;
    private MLShipmentClientV2 mlShipmentClientV2;

    private LMSHelper lmsHepler = new LMSHelper();
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    @Test( priority = 1, enabled = true,dataProviderClass = LMSTestsDP.class, dataProvider = "DCtoDC_usingTMS",description="TC ID:C24681, DC to DC transfer Forward orders through new DC")
    public void DCtoDC_ForDLOrders(String pincode , long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {

        String packetId = omsServiceHelper.getPacketId(lmsHepler.createMockOrder(EnumSCM.SH, pincode, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        System.out.println("packet id "+packetId);
        String tracking_num = lmsHepler.getTrackingNumber(packetId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC
        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.ASSIGNED_TO_OTHER_DC);

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId+","+transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.EXPECTED_IN_DC);

        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 42, "DC");
        lmsServiceHelper.masterBagInScanUpdate( masterBagId,  packetId,  "Bangalore",
                42, "DC",  42);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(42));
        TripResponse tripResponse = tripClient_qa.createTrip(42, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId1,tracking_num);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Order is not assigned to Dc trip");

        //start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6=lastmileClient.startTrip(tripId1.toString(),"10");
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripUpdate),"trip is not started for the DC");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.OUT_FOR_DELIVERY);

        //Update trip order status as Delivered using DC SDA
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Complete DS SDA Trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.DELIVERED);

        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1,com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
    }

    @Test(priority = 2,enabled = true, dataProviderClass = LMSTestsDP.class, dataProvider = "DCtoDC_usingTMS",description="TC ID: C24682, DC to DC transfer for exchange orders")
    public void DCtoDC_ForEXOrders(String pincode , long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {

        //Create Mock order Till DL
        String orderID1 = lmsHepler.createMockOrder(EnumSCM.DL, pincode, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID1));
        String packetId = omsServiceHelper.getPacketId(exchangeOrderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC
        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.ASSIGNED_TO_OTHER_DC);

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId+","+transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.EXPECTED_IN_DC);

        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 42, "DC");
        lmsServiceHelper.masterBagInScanUpdate( masterBagId,  packetId,  "Bangalore",
                42, "DC",  42);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(42));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(42, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId1,tracking_num);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Order is not assigned to Dc trip");

        //start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6=lastmileClient.startTrip(tripId1.toString(),"10");
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripUpdate),"trip is not started for the DC");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.OUT_FOR_DELIVERY);

        String sourceReturnId2=null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse3 = tripClient_qa.findExchangesByTrip(tripId1, LMS_CONSTANTS.TENANTID);
        if (pickupTripOrderAssignmentResponse3.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(tracking_num)) {
            Long pickupTripOrderAssignmentId = pickupTripOrderAssignmentResponse3.getTripOrders().get(0).getId();
            String exchangeOrderId2 = pickupTripOrderAssignmentResponse3.getTripOrders().get(0).getExchangeOrderId();


            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(tracking_num.substring(2, tracking_num.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, exchangeOrderId2, tripId1, orderEntry,LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            sourceReturnId2=tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.DELIVERED);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId2, ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId2.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
    }

    @Test(priority = 3,enabled = true,dataProviderClass = LMSTestsDP.class, dataProvider = "DCtoDC_usingTMS",description="TC ID: C24683,DC to DC transfer for TnB orders")
    public void DCtoDC_ForTnBOrders(String pincode , long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {

        String orderId=lmsHepler.createMockOrder(EnumSCM.SH, pincode, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHepler.getTrackingNumber(packetId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC
        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.ASSIGNED_TO_OTHER_DC);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId+","+transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.EXPECTED_IN_DC);

        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 42, "DC");
        lmsServiceHelper.masterBagInScanUpdate( masterBagId,  packetId,  "Bangalore",
                42, "DC",  42);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);
        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(42));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(42, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        String mobileNumber=tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId1,tracking_num);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Order is not assigned to Dc trip");

        //start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6=lastmileClient.startTrip(tripId1.toString(),"10");
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripUpdate),"trip is not started for the DC");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1, TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        MLShipmentResponse mlShipmentResponse0 = mlShipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse0, ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());
        Long dcId = mlShipmentResponse0.getMlShipmentEntries().get(0).getDeliveryCenterId();

        //update the trip with status as Tried_And_Bought
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderId);
        Double amount = orderEntryDetails.getFinalAmount();
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, LMS_CONSTANTS.TENANTID);
        Long tripOrderAssignmentId = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));
        OrderResponse orderResponse6 = mlShipmentClientV2_qa.getTryAndBuyItems(LMS_CONSTANTS.TENANTID, tracking_num);
        Long itemEntriesId = orderResponse6.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        storeValidator.isNull(String.valueOf(itemEntriesId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = storeHelper.updateTryAndBuyOrders(tripOrderAssignmentId, AttemptReasonCode.DELIVERED, com.myntra.lastmile.client.code.utils.TripAction.UPDATE, packetId, dcId, amount,
                itemEntriesId, LMS_CONSTANTS.TENANTID, TryAndBuyItemStatus.TRIED_AND_BOUGHT, tripId1);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.DELIVERED);

        //Complete DS SDA Trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1,com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

    }

    @Test(priority = 4,enabled = true,dataProviderClass = LMSTestsDP.class, dataProvider = "DCtoDC_usingTMS",description="C24684 : DC to DC transfer for forward orders and delivered that order using old DC")
    public void DCtoDC_ForDLOrders_WithOldDC(String pincode , long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {

        String packetId = omsServiceHelper.getPacketId(lmsHepler.createMockOrder(EnumSCM.SH, pincode, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String tracking_num = lmsHepler.getTrackingNumber(packetId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC
        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.ASSIGNED_TO_OTHER_DC);

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId+","+transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.EXPECTED_IN_DC);

        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 42, "DC");
        lmsServiceHelper.masterBagInScanUpdate( masterBagId,  packetId,  "Bangalore",
                42, "DC",  42);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId1,tracking_num);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusType().toString().toLowerCase().equalsIgnoreCase(ResponseMessageConstants.errorStatus),"Order is assigned to old Dc make DL ");

    }

    @Test(priority = 5,enabled = true,dataProviderClass = LMSTestsDP.class, dataProvider = "DCtoDC_usingTMS",description="C24685 : DC to DC transfer Failed Delivered orders through old DC")
    public void DCtoDC_ForFDOrders_WithOldDC(String pincode , long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {

        String packetId = omsServiceHelper.getPacketId(lmsHepler.createMockOrder(EnumSCM.SH, pincode, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String tracking_num = lmsHepler.getTrackingNumber(packetId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC
        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.ASSIGNED_TO_OTHER_DC);

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId+","+transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.EXPECTED_IN_DC);

        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 42, "DC");
        lmsServiceHelper.masterBagInScanUpdate( masterBagId,  packetId,  "Bangalore",
                42, "DC",  42);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId1,tracking_num);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusType().toString().toLowerCase().equalsIgnoreCase(ResponseMessageConstants.errorStatus),"Order is assigned to old Dc to make FD ");

    }

    @Test(priority = 6,enabled = true,dataProviderClass = LMSTestsDP.class, dataProvider = "DCtoDC_usingTMS",description="TC ID: C24686, DC to DC transfer for Exchange orders with OLD DC")
    public void DCtoDC_ForEXOrders_DL_WithOldDC(String pincode , long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {

        //Create Mock order Till DL
        String orderID1 = lmsHepler.createMockOrder(EnumSCM.DL, pincode, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID1));
        String packetId = omsServiceHelper.getPacketId(exchangeOrderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);
        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC

        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.ASSIGNED_TO_OTHER_DC);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC

        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId+","+transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.EXPECTED_IN_DC);

        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 42, "DC");
        lmsServiceHelper.masterBagInScanUpdate( masterBagId,  packetId,  "Bangalore",
                42, "DC",  42);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId1,tracking_num);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusType().toString().toLowerCase().equalsIgnoreCase(ResponseMessageConstants.errorStatus),"Order is assigned to old Dc trip to make DL");

    }

    @Test(priority = 7,enabled = true,dataProviderClass = LMSTestsDP.class, dataProvider = "DCtoDC_usingTMS",description="TC ID : C24687, DC to DC transfer for exchange orders and try to mark order status as FD and delivered it through old DC")
    public void DCtoDC_ForEXOrders_FD_WithOldDC(String pincode , long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {

        //Create Mock order Till DL
        String orderID1 = lmsHepler.createMockOrder(EnumSCM.DL, pincode, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID1));
        String packetId = omsServiceHelper.getPacketId(exchangeOrderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC

        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.ASSIGNED_TO_OTHER_DC);

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId+","+transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.EXPECTED_IN_DC);
        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 42, "DC");
        lmsServiceHelper.masterBagInScanUpdate( masterBagId,  packetId,  "Bangalore",
                42, "DC",  42);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId1,tracking_num);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusType().toString().toLowerCase().equalsIgnoreCase(ResponseMessageConstants.errorStatus),"Order is assigned to old Dc trip to make FD");

    }

    @Test(priority = 8,enabled = true,dataProviderClass = LMSTestsDP.class, dataProvider = "DCtoDC_usingTMS",description="TC ID:C24688,DC to DC transfer for TnB orders and make it as DL through old DC")
    public void DCtoDC_ForTnBOrders_MakeDL_withOLDDC(String pincode , long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {

        String orderId=lmsHepler.createMockOrder(EnumSCM.SH, pincode, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHepler.getTrackingNumber(packetId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);
        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC
        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.ASSIGNED_TO_OTHER_DC);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId+","+transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.EXPECTED_IN_DC);

        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 42, "DC");
        lmsServiceHelper.masterBagInScanUpdate( masterBagId,  packetId,  "Bangalore",
                42, "DC",  42);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        String mobileNumber=tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId1,tracking_num);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusType().toString().toLowerCase().equalsIgnoreCase(ResponseMessageConstants.errorStatus),"TnB Order is assigned to old Dc to make status as DL");
    }

    @Test(priority = 9,enabled = true,dataProviderClass = LMSTestsDP.class, dataProvider = "DCtoDC_usingTMS",description="TC ID : C24689 : DC to DC transfer for Tnb  orders and mark it as FD through old DC")
    public void DCtoDC_ForTnBOrders_MakeFD_withOLDDC(String pincode , long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {

        String orderId=lmsHepler.createMockOrder(EnumSCM.SH, pincode, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHepler.getTrackingNumber(packetId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC
        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.ASSIGNED_TO_OTHER_DC);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId+","+transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.EXPECTED_IN_DC);

        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 42, "DC");
        lmsServiceHelper.masterBagInScanUpdate( masterBagId,  packetId,  "Bangalore",
                42, "DC",  42);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.UNASSIGNED);

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(5));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(5, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        String mobileNumber=tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        storeValidator.isNull(mobileNumber);
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not "+LMS_CONSTANTS.TENANTID);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId1,tracking_num);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusType().toString().toLowerCase().equalsIgnoreCase(ResponseMessageConstants.errorStatus),"TnB Order is assigned to old Dc to make status as FD");
    }

}
