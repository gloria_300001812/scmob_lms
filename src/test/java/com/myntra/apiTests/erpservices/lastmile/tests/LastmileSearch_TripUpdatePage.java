package com.myntra.apiTests.erpservices.lastmile.tests;

import com.amazonaws.util.StringUtils;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.common.Utils.RamdomGenerators;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastMileTripUpdateSearchHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.DB.IQueries;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import com.myntra.logistics.platform.domain.TryAndBuyItemStatus;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import lombok.SneakyThrows;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class LastmileSearch_TripUpdatePage {
    LMSHelper lmsHelper = new LMSHelper();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMSClient lmsClient = new LMSClient();
    StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    TripClient_QA tripClient_qa = new TripClient_QA();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    LastMileTripUpdateSearchHelper lastMileTripUpdateSearchHelper = new LastMileTripUpdateSearchHelper();
    LastmileOperations lastmileOperations = new LastmileOperations();
    RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    StoreValidator storeValidator = new StoreValidator();
    LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LastmileClient lastmileClient = new LastmileClient();
    ReturnHelper returnHelper = new ReturnHelper();
    MLShipmentClientV2_QA mlShipmentClientV2_qa = new MLShipmentClientV2_QA();
    LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa = new LMSOrderDetailsClient_QA();
    StoreClient_QA storeClient_qa = new StoreClient_QA();
    LastmileHelper lastmileHelper = new LastmileHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient_QA masterBagClient_qa = new MasterBagClient_QA();

    static String searchParams;
    static Long storeHlPId;
    static Long storeSDAId;
    static String storeTenantId;
    static String tenantId;
    static String pincode;
    static String mobileNumber;
    static String storeCourierCode;
    static String code;


    @BeforeTest
    public void createStore() {

        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));

        RamdomGenerators ramdomGenerators = new RamdomGenerators();
        String generateString = ramdomGenerators.generateRandomString(3);

        String name = generateString + String.valueOf(contactNumber).substring(5, 9);
        String ownerFirstName = generateString + String.valueOf(contactNumber).substring(5, 9);
        String ownerLastName = generateString + String.valueOf(contactNumber).substring(5, 9);
        code = generateString + String.valueOf(contactNumber).substring(5, 9);
        String latLong = "LA_latlong" + String.valueOf(contactNumber).substring(5, 9);
        String emailId = "test@lastmileAutomation.com";
        mobileNumber = String.valueOf(contactNumber);
        String address = "Automation Address";
        pincode = LASTMILE_CONSTANTS.STORE_PINCODE;
        String city = "Bangalore";
        String state = "Karnataka";
        String mappedDcCode = LASTMILE_CONSTANTS.ROLLING_DC_CODE;
        String gstin = "LA_gstin" + String.valueOf(contactNumber).substring(5, 9);
        tenantId = LMS_CONSTANTS.TENANTID;


        searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId = deliveryStaffEntry.getId();
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeCourierCode = storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

    }

    @Test(enabled = true, description = "TC ID - C28294 - Search trip update page based on tripId and trip status as Created for forward orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsCREATEDForDLOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripId), ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId, String.valueOf(tripId), ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C28295 - Search trip update page based on tripNumber and trip status as Created for forward orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsCREATEDForDLOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //search api
        TripOrderAssignmentResponse findShipmentsByTripNumberResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(findShipmentsByTripNumberResponse.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripNumber = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber, tripNumber, ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(findShipmentsByTripNumberResponse, getTripDetailsUsingTripNumber);
    }

    @Test(enabled = true, description = "TC ID - C 28296 - Search trip update page based on tripId and trip status as OFD for forward orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsOFDForDLOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        //create trip
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        //add and out scan trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);
        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripId), ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId, String.valueOf(tripId), ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID -C28297 - Search trip update page based on tripNumber and trip status as OFD for forward orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsOFDForDLOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        //create trip
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        //add and out scan trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);
        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);

        //search api
        TripOrderAssignmentResponse findShipmentsByTripNumberResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(findShipmentsByTripNumberResponse.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripNumber = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber, tripNumber, ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(findShipmentsByTripNumberResponse, getTripDetailsUsingTripNumber);
    }

    @Test(enabled = true, description = "TC ID - C28298 - Search trip update page based on tripId and trip status as COMPLETED for forward orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsCOMPLETEDForDLOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        //create trip
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        //add and out scan trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);
        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);
        //update trip ad DELIVERED and complete the trip
        lastmileOperations.updateDeliveryTripByTrackingNumber(trackingNo);
        lastmileOperations.completeTrip(trackingNo);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.COMPLETED);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.DELIVERED);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripId), ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId, String.valueOf(tripId), ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C28299 - Search trip update page based on tripNumber and trip status as COMPLETED for forward orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsCOMPLETEDForDLOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        //create trip
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        //add and out scan trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);
        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);
        //update trip ad DELIVERED and complete the trip
        lastmileOperations.updateDeliveryTripByTrackingNumber(trackingNo);
        lastmileOperations.completeTrip(trackingNo);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.COMPLETED);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.DELIVERED);

        //search api
        TripOrderAssignmentResponse findShipmentsByTripNumberResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(findShipmentsByTripNumberResponse.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripNumber = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber, tripNumber, ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(findShipmentsByTripNumberResponse, getTripDetailsUsingTripNumber);
    }

    @Test(enabled = true, description = "TC ID - C283307 - Search trip update page based on tripId and trip status as Created for TnB orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsCREATEDForTnBOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripId), ShipmentType.TRY_AND_BUY, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId, String.valueOf(tripId), ShipmentType.TRY_AND_BUY.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C283308 - Search trip update page based on tripNumber and trip status as Created for TnB orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsCREATEDForTnBOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //search api
        TripOrderAssignmentResponse findShipmentsByTripNumberResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.TRY_AND_BUY, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(findShipmentsByTripNumberResponse.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripNumber = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber, tripNumber, ShipmentType.TRY_AND_BUY.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(findShipmentsByTripNumberResponse, getTripDetailsUsingTripNumber);
    }

    @Test(enabled = true, description = "TC IC - C283309 - Search trip update page based on tripId and trip status as OFD for TnB orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsOFDForTnBOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        //create trip
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        //add and out scan trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);
        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripId), ShipmentType.TRY_AND_BUY, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId, String.valueOf(tripId), ShipmentType.TRY_AND_BUY.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C28311 - Search trip update page based on tripNumber and trip status as OFD for TnB orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsOFDForTnBOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        //create trip
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        //add and out scan trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);
        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);

        //search api
        TripOrderAssignmentResponse findShipmentsByTripNumberResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.TRY_AND_BUY, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(findShipmentsByTripNumberResponse.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripNumber = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber, tripNumber, ShipmentType.TRY_AND_BUY.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(findShipmentsByTripNumberResponse, getTripDetailsUsingTripNumber);
    }

    @Test(enabled = true, description = "TC ID - C28313 - Search trip update page based on tripId and trip status as COMPLETED for TnB orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsCOMPLETEDForTnBOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        //create trip
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String mobileNumber = tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        //add and out scan trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);
        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);

        //update the trip with status as Tried_And_Bought
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, LMS_CONSTANTS.TENANTID);
        Long tripOrderAssignmentId = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));
        OrderResponse orderResponse6 = mlShipmentClientV2_qa.getTryAndBuyItems(LMS_CONSTANTS.TENANTID, trackingNo);
        Long itemEntriesId = orderResponse6.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        storeValidator.isNull(String.valueOf(itemEntriesId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = storeHelper.updateTryAndBuyOrders(tripOrderAssignmentId, AttemptReasonCode.DELIVERED,
                com.myntra.lastmile.client.code.utils.TripAction.UPDATE, packetId, dcId, amount,
                itemEntriesId, LMS_CONSTANTS.TENANTID, TryAndBuyItemStatus.TRIED_AND_BOUGHT, tripId);
        Assert.assertEquals(tripOrderAssignmentResponse6.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.DELIVERED);

        //complete trip
        lastmileOperations.completeTrip(trackingNo);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.COMPLETED);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripId), ShipmentType.TRY_AND_BUY, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId,
                String.valueOf(tripId), ShipmentType.TRY_AND_BUY.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C28314 - Search trip update page based on tripNumber and trip status as COMPLETED for TnB orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsCOMPLETEDForTnBOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(dcId);
        //create trip
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String mobileNumber = tripResponse.getTrips().get(0).getDeliveryStaffEntry().getMobile();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        //add and out scan trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);
        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);

        //update the trip with status as Tried_And_Bought
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, LMS_CONSTANTS.TENANTID);
        Long tripOrderAssignmentId = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));
        OrderResponse orderResponse6 = mlShipmentClientV2_qa.getTryAndBuyItems(LMS_CONSTANTS.TENANTID, trackingNo);
        Long itemEntriesId = orderResponse6.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        storeValidator.isNull(String.valueOf(itemEntriesId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = storeHelper.updateTryAndBuyOrders(tripOrderAssignmentId, AttemptReasonCode.DELIVERED, com.myntra.lastmile.client.code.utils.TripAction.UPDATE, packetId, dcId, amount,
                itemEntriesId, LMS_CONSTANTS.TENANTID, TryAndBuyItemStatus.TRIED_AND_BOUGHT, tripId);
        Assert.assertEquals(tripOrderAssignmentResponse6.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LMS_CONSTANTS.TENANTID, EnumSCM.DELIVERED);

        //complete trip
        lastmileOperations.completeTrip(trackingNo);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.COMPLETED);
        //search api
        TripOrderAssignmentResponse findShipmentsByTripNumberResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.TRY_AND_BUY, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(findShipmentsByTripNumberResponse.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripNumber = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber,
                tripNumber, ShipmentType.TRY_AND_BUY.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(findShipmentsByTripNumberResponse, getTripDetailsUsingTripNumber);
    }

    @Test(enabled = true, description = "TC ID - C29588 - Search trip update page based on tripId and trip status as Created for Return orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsCREATEDForPUOrders() {
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse getReturnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(getReturnStatusInLMS, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        //getReturnTrackingNumber
        String returnTrackingNumber = getReturnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);

        //create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse createReverseTripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(createReverseTripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = createReverseTripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));

        //add return order to a trip
        TripOrderAssignmentResponse assignOrderToTrip = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(assignOrderToTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Order is not assigned to Dc trip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.CREATED);

        //search api
        //TODO need to change all the TC in this class
        TripOrderAssignmentResponse tripUpdateFilterSearch = lmsServiceHelper.findOrdersByTripId(tripId.toString(), ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId, String.valueOf(tripId), ShipmentType.PU.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29589 - Search trip update page based on trip Number and trip status as Created for Return orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsCREATEDForPUOrders() {
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse getReturnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(getReturnStatusInLMS, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        //getReturnTrackingNumber
        String returnTrackingNumber = getReturnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);

        //create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse createReverseTripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(createReverseTripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = createReverseTripResponse.getTrips().get(0).getId();
        String tripNumber = createReverseTripResponse.getTrips().get(0).getTripNumber();
        storeValidator.isNull(String.valueOf(tripId));

        //add return order to a trip
        TripOrderAssignmentResponse assignOrderToTrip = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(assignOrderToTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Order is not assigned to Dc trip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.CREATED);

        //search api
        TripOrderAssignmentResponse findShipmentsByTripNumberResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(findShipmentsByTripNumberResponse.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripNumber = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber, tripNumber, ShipmentType.PU.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(findShipmentsByTripNumberResponse, getTripDetailsUsingTripNumber);
    }

    @Test(enabled = true, description = "TC ID - C29590 - Search trip update page based on tripId and trip status as OFD for Return orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsOFDForPUOrders() {
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse getReturnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(getReturnStatusInLMS, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        //getReturnTrackingNumber
        String returnTrackingNumber = getReturnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);

        //create Trip
        String deliveryStaffId = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse createReverseTripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffId));
        Assert.assertTrue(createReverseTripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = createReverseTripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));

        //add return order to a trip
        TripOrderAssignmentResponse assignOrderToTrip = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(assignOrderToTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Order is not assigned to Dc trip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.CREATED);

        //start trip
        lastmileOperations.startTrip(tripId, returnTrackingNumber);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.valueOf(LMS_CONSTANTS.CLIENTID), EnumSCM.RETURN_CREATED);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_PICKUP);

        //search api
        //TODO need to change all the TC in this class
        TripOrderAssignmentResponse tripUpdateFilterSearch = lmsServiceHelper.findOrdersByTripId(tripId.toString(), ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId, String.valueOf(tripId), ShipmentType.PU.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29591 - Search trip update page based on trip Number and trip status as OFD for Return orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsOFPForPUOrders() {
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse getReturnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(getReturnStatusInLMS, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        //getReturnTrackingNumber
        String returnTrackingNumber = getReturnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);

        //create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse createReverseTripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(createReverseTripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = createReverseTripResponse.getTrips().get(0).getId();
        String tripNumber = createReverseTripResponse.getTrips().get(0).getTripNumber();
        storeValidator.isNull(String.valueOf(tripId));

        //add return order to a trip
        TripOrderAssignmentResponse assignOrderToTrip = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(assignOrderToTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Order is not assigned to Dc trip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.CREATED);

        //start trip
        lastmileOperations.startTrip(tripId, returnTrackingNumber);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.valueOf(LMS_CONSTANTS.CLIENTID), EnumSCM.RETURN_CREATED);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_PICKUP);

        //search api
        TripOrderAssignmentResponse findShipmentsByTripNumberResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(findShipmentsByTripNumberResponse.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripNumber = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber, tripNumber, ShipmentType.PU.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(findShipmentsByTripNumberResponse, getTripDetailsUsingTripNumber);
    }

    @Test(enabled = true, description = "TC ID - C29592 - Search trip update page based on tripId and trip status as COMPLETED for Return orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsCOMPLETEDForPUOrders() {
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse getReturnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(getReturnStatusInLMS, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        //getReturnTrackingNumber
        String returnTrackingNumber = getReturnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);

        //create Trip
        String deliveryStaffId = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse createReverseTripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffId));
        Assert.assertTrue(createReverseTripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = createReverseTripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));

        //add return order to a trip
        TripOrderAssignmentResponse assignOrderToTrip = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(assignOrderToTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Order is not assigned to Dc trip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.CREATED);

        //start trip
        lastmileOperations.startTrip(tripId, returnTrackingNumber);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.valueOf(LMS_CONSTANTS.CLIENTID), EnumSCM.RETURN_CREATED);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_PICKUP);

        //update trip as Pickup successful
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripId), ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKUP_SUCCESSFUL, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.PICKUP_SUCCESSFUL);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.RETURN_SUCCESSFUL);
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.PICKUP_SUCCESSFUL);

        //receive in DC
        returnHelper.receiveOrderAtDC(returnTrackingNumber, tripId);
        //complete trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.COMPLETED);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.PICKUP_SUCCESSFUL);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.RETURN_SUCCESSFUL);
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.PICKUP_SUCCESSFUL);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = lmsServiceHelper.findOrdersByTripId(tripId.toString(), ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId, String.valueOf(tripId), ShipmentType.PU.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29593 - Search trip update page based on trip Number and trip status as COMPLETED for Return orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsCOMPLETEDForPUOrders() {
        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse getReturnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(getReturnStatusInLMS, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        //getReturnTrackingNumber
        String returnTrackingNumber = getReturnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);

        //create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse createReverseTripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(createReverseTripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = createReverseTripResponse.getTrips().get(0).getId();
        String tripNumber = createReverseTripResponse.getTrips().get(0).getTripNumber();
        storeValidator.isNull(String.valueOf(tripId));

        //add return order to a trip
        TripOrderAssignmentResponse assignOrderToTrip = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(assignOrderToTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Order is not assigned to Dc trip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.CREATED);

        //start trip
        lastmileOperations.startTrip(tripId, returnTrackingNumber);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.valueOf(LMS_CONSTANTS.CLIENTID), EnumSCM.RETURN_CREATED);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_PICKUP);

        //update trip as Pickup successful
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripId), ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKUP_SUCCESSFUL, EnumSCM.UPDATE, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.PICKUP_SUCCESSFUL);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.RETURN_SUCCESSFUL);
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.PICKUP_SUCCESSFUL);

        //receive in DC
        returnHelper.receiveOrderAtDC(returnTrackingNumber, tripId);
        //complete trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.COMPLETED);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.PICKUP_SUCCESSFUL);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.RETURN_SUCCESSFUL);
        statusPollingValidator.validatePickupShipmentStatus(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.PICKUP_SUCCESSFUL);

        //search api
        TripOrderAssignmentResponse findShipmentsByTripNumberResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(findShipmentsByTripNumberResponse.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");

        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripNumber = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber, tripNumber, ShipmentType.PU.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(findShipmentsByTripNumberResponse, getTripDetailsUsingTripNumber);
    }

    @Test(enabled = true, description = "TC ID - C29594 - Search trip update page based on trip Id and trip status as CREATED for Exchange orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsCREATEDForExchangeOrders() {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId,
                EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL,
                "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        statusPollingValidator.validateOrderStatus(exchangePacketId, EnumSCM.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //create trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        //add shipment to trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(exchangeTrackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = tripClient_qa.findExchangesByTrip(tripId, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId, String.valueOf(tripId), ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29595 - Search trip update page based on trip number and trip status as CREATED for Exchange orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsCREATEDForExchangeOrders() {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId,
                EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL,
                "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        statusPollingValidator.validateOrderStatus(exchangePacketId, EnumSCM.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //create trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        //add shipment to trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(exchangeTrackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = tripClient_qa.findExchangesByTrip(tripNumber, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber, tripNumber, ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29596 - Search trip update page based on trip Id and trip status as OFD for Exchange orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsOFDForExchangeOrders() {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId,
                EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL,
                "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        statusPollingValidator.validateOrderStatus(exchangePacketId, EnumSCM.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //create trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        //add shipment to trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(exchangeTrackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //start trip
        lastmileOperations.startTrip(tripId, exchangeTrackingNumber);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = tripClient_qa.findExchangesByTrip(tripId, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId, String.valueOf(tripId), ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29597 - Search trip update page based on trip number and trip status as OFD for Exchange orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsOFDForExchangeOrders() {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId,
                EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL,
                "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        statusPollingValidator.validateOrderStatus(exchangePacketId, EnumSCM.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //create trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        //add shipment to trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(exchangeTrackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //start trip
        lastmileOperations.startTrip(tripId, exchangeTrackingNumber);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = tripClient_qa.findExchangesByTrip(tripNumber, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber, tripNumber, ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID -C29598 - Search trip update page based on trip Id and trip status as COMPLETED for Exchange orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsCOMPLETEDForExchangeOrders() {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId,
                EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL,
                "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        statusPollingValidator.validateOrderStatus(exchangePacketId, EnumSCM.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //create trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        //add shipment to trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(exchangeTrackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //start trip
        lastmileOperations.startTrip(tripId, exchangeTrackingNumber);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);

        //update the trip order status as deliveres and get the pickup exchange order details
        String pickupTrackingNumber = null;
        String sourceReturnId = null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripId, LMS_CONSTANTS.TENANTID);
        if (pickupTripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getId();
            String exchangeOrderId1 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getExchangeOrderId();

            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, exchangeOrderId1, tripId, orderEntry, LMS_CONSTANTS.TENANTID, Long.valueOf(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            pickupTrackingNumber = tripOrderAssignmentResponse0.getTripOrders().get(0).getTrackingNumber();
            sourceReturnId = tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(tripId, LMS_CONSTANTS.TENANTID, com.myntra.lastmile.client.status.TripOrderStatus.PS.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId, ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId, LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        //validate pickup shipment staus
        statusPollingValidator.validatePickupShipmentStatus(sourceReturnId, LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.PICKUP_SUCCESSFUL);

        //receive in DC
        returnHelper.receiveOrderAtDC(exchangeTrackingNumber, tripId, LMS_CONSTANTS.TENANTID);
        //complete trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.COMPLETED);
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId, LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.RETURN_SUCCESSFUL);
        statusPollingValidator.validatePickupShipmentStatus(sourceReturnId, LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.PICKUP_SUCCESSFUL);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = tripClient_qa.findExchangesByTrip(tripId, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripId, String.valueOf(tripId), ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29599 - Search trip update page based on trip number and trip status as COMPLETED for Exchange orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsCOMPLETEDForExchangeOrders() {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId,
                EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL,
                "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        statusPollingValidator.validateOrderStatus(exchangePacketId, EnumSCM.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //create trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        //add shipment to trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(exchangeTrackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //start trip
        lastmileOperations.startTrip(tripId, exchangeTrackingNumber);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.OUT_FOR_DELIVERY);

        //update the trip order status as deliveres and get the pickup exchange order details
        String pickupTrackingNumber = null;
        String sourceReturnId = null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripId, LMS_CONSTANTS.TENANTID);
        if (pickupTripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getId();
            String exchangeOrderId1 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getExchangeOrderId();

            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, exchangeOrderId1, tripId, orderEntry, LMS_CONSTANTS.TENANTID, Long.valueOf(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            pickupTrackingNumber = tripOrderAssignmentResponse0.getTripOrders().get(0).getTrackingNumber();
            sourceReturnId = tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(tripId, LMS_CONSTANTS.TENANTID, com.myntra.lastmile.client.status.TripOrderStatus.PS.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId, ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId, LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        //validate pickup shipment staus
        statusPollingValidator.validatePickupShipmentStatus(sourceReturnId, LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.PICKUP_SUCCESSFUL);

        //receive in DC
        returnHelper.receiveOrderAtDC(exchangeTrackingNumber, tripId, LMS_CONSTANTS.TENANTID);

        //complete trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.COMPLETED);
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId, LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.RETURN_SUCCESSFUL);
        statusPollingValidator.validatePickupShipmentStatus(sourceReturnId, LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.PICKUP_SUCCESSFUL);

        //search api
        TripOrderAssignmentResponse tripUpdateFilterSearch = tripClient_qa.findExchangesByTrip(tripNumber, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripUpdateFilterSearch.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        //get db data for same combination
        Map<String, Object> getTripDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getDBDetailsUsingTripNumber, tripNumber, ShipmentType.DL.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageData(tripUpdateFilterSearch, getTripDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29600 - Search trip update page based on trip Id and trip status as CREATED for StoreShipments orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsCREATEDForStoreShipmentsOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String originDCCity = orderResponse.getOrders().get(0).getCity();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(dcId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse addStoreBagToTrip = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(addStoreBagToTrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.ADDED_TO_TRIP.toString());

        //search lastmile update
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.getShipmentIdUsingTripId(tripId, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripShipmentAssociationResponse.getStatus().getStatusMessage(), "Success", "trip shipment is not retrieved successfully");

        //get db data for same combination
        Map<String, Object> getTripShipmentDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getTripShipmentDetailsByTripId, tripId.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageStoreShipmentsData(tripShipmentAssociationResponse, getTripShipmentDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - 29601 - Search trip update page based on trip number and trip status as CREATED for StoreShipments orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsCREATEDForStoreShipmentsOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String originDCCity = orderResponse.getOrders().get(0).getCity();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(dcId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse addStoreBagToTrip = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(addStoreBagToTrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.ADDED_TO_TRIP.toString());

        //search lastmile update
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse.getStatus().getStatusMessage(), "Success", "trip shipment is not retrieved successfully");

        //get db data for same combination
        Map<String, Object> getTripShipmentDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getTripShipmentDetailsByTripId, tripId.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageStoreShipmentsData(tripShipmentAssociationResponse, getTripShipmentDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29602 - Search trip update page based on trip Id and trip status as OFD for StoreShipments orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsOFDForStoreShipmentsOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String originDCCity = orderResponse.getOrders().get(0).getCity();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(dcId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse addStoreBagToTrip = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(addStoreBagToTrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);

        //search lastmile update
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.getShipmentIdUsingTripId(tripId, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripShipmentAssociationResponse.getStatus().getStatusMessage(), "Success", "trip shipment is not retrieved successfully");

        //get db data for same combination
        Map<String, Object> getTripShipmentDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getTripShipmentDetailsByTripId, tripId.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageStoreShipmentsData(tripShipmentAssociationResponse, getTripShipmentDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29603 - Search trip update page based on trip number and trip status as OFD for StoreShipments orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsOFDForStoreShipmentsOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String originDCCity = orderResponse.getOrders().get(0).getCity();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(dcId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse addStoreBagToTrip = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(addStoreBagToTrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);

        //search lastmile update
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse.getStatus().getStatusMessage(), "Success", "trip shipment is not retrieved successfully");

        //get db data for same combination
        Map<String, Object> getTripShipmentDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getTripShipmentDetailsByTripId, tripId.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageStoreShipmentsData(tripShipmentAssociationResponse, getTripShipmentDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29604 - Search trip update page based on trip Id and trip status as COMPLETED for StoreShipments orders")
    @SneakyThrows
    public void searchBasedOnTripIdAndTripStatusAsCOMPLETEDForStoreShipmentsOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String originDCCity = orderResponse.getOrders().get(0).getCity();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(dcId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse addStoreBagToTrip = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(addStoreBagToTrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());
        //complete trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.COMPLETED);

        //search lastmile update
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.getShipmentIdUsingTripId(tripId, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripShipmentAssociationResponse.getStatus().getStatusMessage(), "Success", "trip shipment is not retrieved successfully");

        //get db data for same combination
        Map<String, Object> getTripShipmentDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getTripShipmentDetailsByTripId, tripId.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageStoreShipmentsData(tripShipmentAssociationResponse, getTripShipmentDetailsUsingTripId);
    }

    @Test(enabled = true, description = "TC ID - C29605 - Search trip update page based on trip number and trip status as COMPLETED for StoreShipments orders")
    @SneakyThrows
    public void searchBasedOnTripNumberAndTripStatusAsCOMPLETEDForStoreShipmentsOrders() {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long dcId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String originDCCity = orderResponse.getOrders().get(0).getCity();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(dcId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse addStoreBagToTrip = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(addStoreBagToTrip.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.DELIVERED.toString());
        //complete trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.COMPLETED);

        //search lastmile update
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse.getStatus().getStatusMessage(), "Success", "trip shipment is not retrieved successfully");

        //get db data for same combination
        Map<String, Object> getTripShipmentDetailsUsingTripId = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.Lastmile_TripUpdateSearch.getTripShipmentDetailsByTripId, tripId.toString()), "myntra_lms");

        //validate API data with DB data
        lastMileTripUpdateSearchHelper.validateTripUpdatePageStoreShipmentsData(tripShipmentAssociationResponse, getTripShipmentDetailsUsingTripId);
    }

    @Test(enabled = true,description = "TC ID - C29606 - Create trips for same SDA add diff shipment type(PU,EXC,DL,TnB) in diff trip and validate if all are coming in list or not using tripNumber")
    @SneakyThrows
    public void testSearchTripDetailsUsingTripNumber(){

        Long dcId =5l;
        //search orderUpdate tab with trip status as CREATED
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        StringUtils.isNullOrEmpty(String.valueOf(tripId));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        StringUtils.isNullOrEmpty(tripNumber);

        // assign DL order for a Trip
        String orderIdForDLOrders = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetIdForDLOrders = omsServiceHelper.getPacketId(orderIdForDLOrders);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetIdForDLOrders);
        statusPollingValidator.validateOrderStatus(packetIdForDLOrders, ShipmentStatus.SHIPPED);
        String trackingNoForDLOrders = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNoForDLOrders);
        //assign order to a trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNoForDLOrders, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNoForDLOrders, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        // assign TnB order for a Trip
        String orderIdForTnBOrders = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetIdForTnBOrders = omsServiceHelper.getPacketId(orderIdForTnBOrders);
        //validate OrderToShip status
        OrderResponse orderResponseForTnBOrders = lmsClient.getOrderByOrderId(packetIdForTnBOrders);
        statusPollingValidator.validateOrderStatus(packetIdForTnBOrders, ShipmentStatus.SHIPPED);
        String trackingNoForTnBOrders = orderResponseForTnBOrders.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNoForTnBOrders);
        //assign order to a trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNoForTnBOrders, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNoForTnBOrders, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //create PU order and assign to the same trip
        String orderIdForPUOrder = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetIdForPUOrder = omsServiceHelper.getPacketId(orderIdForPUOrder);
        //validate OrderToShip status
        OrderResponse orderResponseForPU = lmsClient.getOrderByOrderId(packetIdForPUOrder);
        statusPollingValidator.validateOrderStatus(packetIdForPUOrder, ShipmentStatus.DELIVERED);
        String trackingNo = orderResponseForPU.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderIdForPUOrder));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse getReturnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(getReturnStatusInLMS, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        //getReturnTrackingNumber
        String returnTrackingNumber = getReturnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);
        //add return order to a trip
        TripOrderAssignmentResponse assignOrderToTrip = lastmileClient.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertTrue(assignOrderToTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Order is not assigned to Dc trip");
        statusPollingValidator.validateTripStatus(tripId, EnumSCM.CREATED);

       //create and assign EX order to same trip
        String orderIdForExchangeOrder = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId,
                EnumSCM.NORMAL, "cod", false, true);
        String packetIdForExchangeOrder = omsServiceHelper.getPacketId(orderIdForExchangeOrder);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetIdForExchangeOrder, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL,
                "cod", false, true, omsServiceHelper.getReleaseId(orderIdForExchangeOrder));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, EnumSCM.SHIPPED);
        //add shipment to trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(exchangeTrackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //validations
        TripOrderAssignmentResponse responseForForwardOrder = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(responseForForwardOrder.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        Assert.assertEquals(responseForForwardOrder.getStatus().getTotalCount(),1l,"DL order is not retrieved ");

        TripOrderAssignmentResponse responseTnBOrder = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.TRY_AND_BUY, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(responseTnBOrder.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        Assert.assertEquals(responseTnBOrder.getStatus().getTotalCount(),1l,"TnB order is not retrieved ");

        TripOrderAssignmentResponse responsePUOrder = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(responsePUOrder.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        Assert.assertEquals(responsePUOrder.getStatus().getTotalCount(),1l,"Return order is not retrieved ");

        TripOrderAssignmentResponse responseExchangeOrder = tripClient_qa.findExchangesByTrip(tripId, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(responseExchangeOrder.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        Assert.assertEquals(responseExchangeOrder.getStatus().getTotalCount(),1l,"Exchange order is not retrieved ");

    }

    @Test(enabled = true,description = "TC ID - C29611 - Create trips for same SDA add diff shipment type(PU,EXC,DL,TnB) in diff trip and validate if all are coming in trip list dropdown or not using trip Id")
    @SneakyThrows
    public void testSearchTripDetailsUsingTripId(){

        Long dcId =5l;
        long totalNoOfTrips=4;
        List lstOfTripNumber=new ArrayList();
        List lstOfTripNumbersFromAPI=new ArrayList();

        //Create trip for Dl order
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(dcId));
        TripResponse tripResponse = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripIdForDLOrder = tripResponse.getTrips().get(0).getId();
        StringUtils.isNullOrEmpty(String.valueOf(tripIdForDLOrder));
        String tripNumberForDLOrder = tripResponse.getTrips().get(0).getTripNumber();
        StringUtils.isNullOrEmpty(tripNumberForDLOrder);

        // assign DL order for a Trip
        String orderIdForDLOrders = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetIdForDLOrders = omsServiceHelper.getPacketId(orderIdForDLOrders);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetIdForDLOrders);
        statusPollingValidator.validateOrderStatus(packetIdForDLOrders, ShipmentStatus.SHIPPED);
        String trackingNoForDLOrders = orderResponse.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNoForDLOrders);
        //assign order to a trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNoForDLOrders, tripIdForDLOrder, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNoForDLOrders, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        // assign TnB order for a Trip
        String orderIdForTnBOrders = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetIdForTnBOrders = omsServiceHelper.getPacketId(orderIdForTnBOrders);
        //validate OrderToShip status
        OrderResponse orderResponseForTnBOrders = lmsClient.getOrderByOrderId(packetIdForTnBOrders);
        statusPollingValidator.validateOrderStatus(packetIdForTnBOrders, ShipmentStatus.SHIPPED);
        String trackingNoForTnBOrders = orderResponseForTnBOrders.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNoForTnBOrders);

        //Create trip for TnB order
        TripResponse tripResponseForTnB = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponseForTnB.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripIdForTnBOrder = tripResponseForTnB.getTrips().get(0).getId();
        StringUtils.isNullOrEmpty(String.valueOf(tripIdForTnBOrder));
        String tripNumberForTnBOrder = tripResponseForTnB.getTrips().get(0).getTripNumber();
        StringUtils.isNullOrEmpty(tripNumberForTnBOrder);
        //assign order to a trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNoForTnBOrders, tripIdForTnBOrder, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(trackingNoForTnBOrders, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //create PU order and assign to the same trip
        String orderIdForPUOrder = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,
                LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetIdForPUOrder = omsServiceHelper.getPacketId(orderIdForPUOrder);
        //validate OrderToShip status
        OrderResponse orderResponseForPU = lmsClient.getOrderByOrderId(packetIdForPUOrder);
        statusPollingValidator.validateOrderStatus(packetIdForPUOrder, ShipmentStatus.DELIVERED);
        String trackingNoForPU = orderResponseForPU.getOrders().get(0).getTrackingNumber();
        StringUtils.isNullOrEmpty(trackingNoForPU);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderIdForPUOrder));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse getReturnStatusInLMS = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(getReturnStatusInLMS, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        //getReturnTrackingNumber
        String returnTrackingNumber = getReturnStatusInLMS.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber);

        //Create trip for PU order
        TripResponse tripResponseForPU = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponseForPU.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripIdForPUOrder = tripResponseForPU.getTrips().get(0).getId();
        StringUtils.isNullOrEmpty(String.valueOf(tripIdForPUOrder));
        String tripNumberForPUOrder = tripResponseForPU.getTrips().get(0).getTripNumber();
        StringUtils.isNullOrEmpty(tripNumberForPUOrder);
        //add return order to a trip
        TripOrderAssignmentResponse assignOrderToTrip = lastmileClient.assignOrderToTrip(tripIdForPUOrder, returnTrackingNumber);
        Assert.assertTrue(assignOrderToTrip.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Order is not assigned to Dc trip");
        statusPollingValidator.validateTripStatus(tripIdForPUOrder, EnumSCM.CREATED);

        //create and assign EX order to same trip
        String orderIdForExchangeOrder = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId,
                EnumSCM.NORMAL, "cod", false, true);
        String packetIdForExchangeOrder = omsServiceHelper.getPacketId(orderIdForExchangeOrder);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetIdForExchangeOrder, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL,
                "cod", false, true, omsServiceHelper.getReleaseId(orderIdForExchangeOrder));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId, EnumSCM.SHIPPED);

        //Create trip for Exchange order
        TripResponse tripResponseForEX = tripClient_qa.createTrip(dcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponseForEX.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripIdForExOrder = tripResponseForEX.getTrips().get(0).getId();
        StringUtils.isNullOrEmpty(String.valueOf(tripIdForExOrder));
        String tripNumberForExOrder = tripResponseForEX.getTrips().get(0).getTripNumber();
        StringUtils.isNullOrEmpty(tripNumberForExOrder);
        //add shipment to trip
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(exchangeTrackingNumber, tripIdForExOrder, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to add And Out scan NewOrderToTrip");
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.ASSIGNED_TO_SDA);

        //validations
        Date date =new SimpleDateFormat("yyyy-MM-dd").parse(DateTimeHelper.generateDate("yyyy-MM-dd",0));
        TripResponse tripResponseForSearchTripDetails = tripClient_qa.searchTripDetails(Long.parseLong(deliveryStaffID),date,date ,TripStatus.CREATED,Long.parseLong(deliveryStaffID),LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripResponseForSearchTripDetails.getStatus().getStatusMessage(),"Trip(s) retrieved successfully","Trip(s) not retrieved successfully");
        Assert.assertEquals(tripResponseForSearchTripDetails.getStatus().getTotalCount(),totalNoOfTrips,"Total no of trip created to SDA is not matching");
        //compare all trip numbers are same in dropdown
        lstOfTripNumber.add(tripNumberForDLOrder);
        lstOfTripNumber.add(tripNumberForPUOrder);
        lstOfTripNumber.add(tripNumberForTnBOrder);
        lstOfTripNumber.add(tripNumberForExOrder);

        List<TripEntry> tripNumbers = tripResponseForSearchTripDetails.getTrips();
        for(int i=0;i<tripNumbers.size();i++){
            lstOfTripNumbersFromAPI.add(tripNumbers.get(i).getTripNumber());
        }
        Assert.assertTrue(lstOfTripNumber.equals(lstOfTripNumbersFromAPI),"Trip numbers are not matching in Trip dropdown tripUpdate page");

        // validation for DL orders
        TripOrderAssignmentResponse tripSearchForDLOrders = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripIdForDLOrder), ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripSearchForDLOrders.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        Assert.assertEquals(tripSearchForDLOrders.getStatus().getTotalCount(),1l,"trip doesn't contain any orders");
        Assert.assertEquals(tripSearchForDLOrders.getTripOrders().get(0).getTrackingNumber(),trackingNoForDLOrders,"DL order tracking number is not matching with DL trip "+tripIdForDLOrder);

        // validation for TnB orders
        TripOrderAssignmentResponse tripSearchForTnBOrders = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripIdForTnBOrder), ShipmentType.TRY_AND_BUY, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripSearchForTnBOrders.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        Assert.assertEquals(tripSearchForTnBOrders.getStatus().getTotalCount(),1l,"trip doesn't contain any orders");
        Assert.assertEquals(tripSearchForTnBOrders.getTripOrders().get(0).getTrackingNumber(),trackingNoForTnBOrders,"TnB orders tracking number is not matching with Tnb trip "+tripIdForTnBOrder);

        // validation for PU orders
        TripOrderAssignmentResponse tripSearchForPUOrders = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripIdForPUOrder), ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripSearchForPUOrders.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        Assert.assertEquals(tripSearchForPUOrders.getStatus().getTotalCount(),1l,"trip doesn't contain any orders");
        Assert.assertEquals(tripSearchForPUOrders.getTripOrders().get(0).getTrackingNumber(),returnTrackingNumber,"return order tracking number is not matching with Return trip "+tripIdForPUOrder);

        // validation for Exchange orders
        TripOrderAssignmentResponse tripSearchForExOrders = lmsServiceHelper.findOrdersByTripId(String.valueOf(tripIdForExOrder), ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(tripSearchForExOrders.getStatus().getStatusMessage(), "Success", "Trips not retrieved successfully ");
        Assert.assertEquals(tripSearchForExOrders.getStatus().getTotalCount(),1l,"trip doesn't contain any orders");
        Assert.assertEquals(tripSearchForExOrders.getTripOrders().get(0).getTrackingNumber(),exchangeTrackingNumber,"Exchange order tracking number is not matching with Exchange trip "+tripIdForExOrder);
    }



}
