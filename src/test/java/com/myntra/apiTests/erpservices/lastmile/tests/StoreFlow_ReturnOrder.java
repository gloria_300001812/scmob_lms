package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.CommonUtils;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_StoreFlowsDP;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.response.TransporterResponse;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.tms.container.ContainerEntry;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class StoreFlow_ReturnOrder {
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private LastmileClient lastmileClient = new LastmileClient();
    private TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    private LastmileHelper lastmileHelper;
    private MasterBagServiceHelper masterBagServiceHelper=new MasterBagServiceHelper();
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa=new MLShipmentClientV2_QA();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    static String searchParams;
    static Long storeHlPId;
    static Long storeSDAId;
    static String storeTenantId;
    static String tenantId;
    static String pincode;
    static String mobileNumber;
    static String storeCourierCode;
    static String code;

    static String code1;
    static String storeCourierCode1;
    static String searchParams1;
    static Long storeHlPId1;
    static Long storeSDAId1;
    static String storeTenantId1;
    static String mobileNumber1;

    @BeforeTest
    public void createStore(){

        Random r = new Random(System.currentTimeMillis());
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        int contactNumber2 = Math.abs(2000000000 + r.nextInt(300000000));
        String value = CommonUtils.generateString(3);
        String value1 = CommonUtils.generateString(3);
        String name=value + String.valueOf(contactNumber1).substring(5,9);
        String ownerFirstName= value + String.valueOf(contactNumber1).substring(5,9);
        String ownerLastName= value + String.valueOf(contactNumber1).substring(5,9);
        code=value + String.valueOf(contactNumber1).substring(5,9);
        code1=value1 + String.valueOf(contactNumber2).substring(5,9);
        String latLong="LA_latlong" + String.valueOf(contactNumber1).substring(5,9);
        String emailId="test@lastmileAutomation.com";
        mobileNumber=String.valueOf(contactNumber1);
        String  address="Automation Address";
        pincode= LASTMILE_CONSTANTS.STORE_PINCODE;
        String city="Bangalore";
        String state="Karnataka";
        String mappedDcCode= LASTMILE_CONSTANTS.ROLLING_DC_CODE;
        String gstin="LA_gstin" + String.valueOf(contactNumber1).substring(5,9);
        tenantId=LMS_CONSTANTS.TENANTID;

        String name1=value1 + String.valueOf(contactNumber2).substring(5,9);
        String ownerFirstName1= value1 + String.valueOf(contactNumber2).substring(5,9);
        String ownerLastName1= value1 + String.valueOf(contactNumber2).substring(5,9);
        String latLong1="LA_latlong" + String.valueOf(contactNumber2).substring(5,9);
        mobileNumber1=String.valueOf(contactNumber2);
        String gstin1="LA_gstin" + String.valueOf(contactNumber2).substring(5,9);

        searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId = deliveryStaffEntry.getId();
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //create 2nd store
        searchParams1 = "code.like:" + code1;
        //Create Store
        StoreResponse storeResponse1 = lastmileHelper.createStore(name1, ownerFirstName1, ownerLastName1, latLong1, "test1@lastmileAutomation.com", mobileNumber1, address, pincode, city, state, mappedDcCode,
                gstin1, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code1);
        //validate Store created successfully
        Assert.assertTrue(storeResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId1 = storeResponse1.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry1 = storeResponse1.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId1 = deliveryStaffEntry1.getId();
        storeTenantId1 = storeResponse1.getStoreEntries().get(0).getTenantId();
        storeCourierCode1=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

    }

    @AfterMethod
    public void modifiedMLLastmilePartnerShipmentDate(){
        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDateAsCurrentDate(storeCourierCode);
    }

    /* TODO:-------------- Return Flow ----------- */

    @Test( priority = 1, enabled = true, description = "TC iD: C24720, Create a Store and create the return order and assign pickup order to store, return the order to warehouse ")
    public void processReturnPickupSuccessfulForStoreOrders() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String deliveryCenterName=orderResponse.getOrders().get(0).getDeliveryCenterCode();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2=storeHelper.getReturnStatusInLMS(returnId.toString(),tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse2, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnHub=returnResponse2.getDomainReturnShipments().get(0).getReturnHubCode();

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.UNASSIGNED);

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        Long storetripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storetripId.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.OUT_FOR_PICKUP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successfull in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE,tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse1 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry = tripResponse1.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId), ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId,TripStatus.OUT_FOR_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(returnTrackingNumber);
        TripShipmentAssociationResponse tripShipmentAssociationResponse = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(returnTrackingNumber.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(storeTenantId,reverseTripId);
        statusPollingValidator.validateTripStatus(reverseTripId,EnumSCM.COMPLETED);

        //Qc pass in DC
        String mlOpenBoxQCPass = storeHelper.mlOpenBoxQCPass(String.valueOf(returnId), ShipmentUpdateEvent.PICKUP_SUCCESSFUL, returnTrackingNumber);
        Assert.assertTrue(mlOpenBoxQCPass.contains(ResponseMessageConstants.qcUpdate));

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.PICKUP_SUCCESSFUL);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //CreateMB from DC To WH
        MasterbagDomain shipmentResponse7 = masterBagServiceHelper.createMasterBag(deliveryCenterName,returnHub, ShippingMethod.NORMAL,"ML",tenantId);
        Long reverseMBId=shipmentResponse7.getId();
        storeValidator.isNull(String.valueOf(reverseMBId));
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.NEW.toString());
        //Add order to MB
        masterBagServiceHelper.addShipmentsToMasterBag(reverseMBId,returnTrackingNumber,ShipmentType.PU,tenantId);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.NEW.toString());
        //close Reverse MB
        masterBagServiceHelper.closeMasterBag(reverseMBId,tenantId);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());

        //Receive MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(deliveryCenterName, reverseMBId, tenantId);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse=storeHelper.getMappedTransportHubDetails(tenantId,deliveryCenterName);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub=hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterName,transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(deliveryCenterName, transporterHub, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, reverseMBId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());
        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());

        //Receive Container In Transport Hub
        ContainerResponse containerResponse3 = (ContainerResponse) storeHelper.receiveContainerInTransportHub.apply(containerId, transporterHub);
        Assert.assertTrue(containerResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Container is not received in TRansport hub");

        //MasterBag inscan
        String mbInscanResponse = null;
        storeHelper.masterBagInscanProcess(returnTrackingNumber, reverseMBId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);

        //TODO calling this API twice, since it fails 1st time.
        mbInscanResponse = storeHelper.masterBagInscanProcess(returnTrackingNumber, reverseMBId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
        Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success)," Master bag inscan failed in WH");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.RETURN_IN_TRANSIT);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(), ReturnStatus.RRC.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.DELIVERED_TO_SELLER.toString());
    }

    @Test(  priority = 2, enabled = true,description = "TC ID:C24721,Create a Store and create the return order and assign pickup order to store, make the order status as Failed_Pickup")
    public void processReturnFailedPickupForStoreToCustomerOrdersSuccessful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.UNASSIGNED);

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        Long storetripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storetripId.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.OUT_FOR_PICKUP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup unSuccessfull in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE, EnumSCM.UPDATE,storeTenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.FAILED_PICKUP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.FAILED_PICKUP);

        //Complete Trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE, EnumSCM.TRIP_COMPLETE,storeTenantId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip),"Trip not able to complete");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.FAILED_PICKUP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.ASSIGNED_TO_DC);

    }

    @Test( priority = 3, enabled =true,  description = "TC ID:C24722,Create a Store and create the return order, assign pickup order to store, make the order status as Pickup_Successful and make the reverse shipment status as Failed_Pickup(FP) from DC to Store")
    public void processFailedPickupReverseBagFromDCToStoreOrdersSuccessful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.UNASSIGNED);

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        Long storetripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storetripId.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.OUT_FOR_PICKUP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successfull in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE,tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse1 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry = tripResponse1.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId,TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId),ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId,TripStatus.OUT_FOR_DELIVERY.toString());

        //update MB as FailedPickup reverse bag in store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(returnTrackingNumber);
        TripShipmentAssociationResponse tripShipmentAssociationResponse = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.FP,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        // validate trip shipment association status is FP or not
        TripShipmentAssociationResponse tripShipmentAssociationResponse03=storeHelper.findShipmentByTrip(reverseTripId,tenantId);
        Assert.assertTrue(tripShipmentAssociationResponse03.getTripShipmentAssociationEntryList().get(0).getStatus().toString().equalsIgnoreCase(com.myntra.lastmile.client.status.TripOrderStatus.FP.toString()));

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(returnTrackingNumber.substring(2, returnTrackingNumber.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);
        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(storeTenantId,reverseTripId);
        statusPollingValidator.validateTripStatus(reverseTripId,EnumSCM.COMPLETED);

    }

    @Test( priority = 4,  enabled = true,description = "TC ID:C24723,Create a store and create and assign order to store, make the store bag as Failed_Delivered to store")
    public void processFDStoreBagSuccessful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse,TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Failed Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = storeHelper.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS,
                com.myntra.lastmile.client.status.TripOrderStatus.FD,ShipmentType.DL,tenantId);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.FAILED_TO_HANDOVER_TO_LAST_MILE_PARTNER);

        //Complete Trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4=storeHelper.closeMyntraTripWithStoreBag(tenantId,tripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,TripStatus.COMPLETED.toString());
    }


    @Test(priority = 5,enabled = true,  description = "TC ID:C24724, Create a Store , Create return order and make it Pickup_successful in store,Create one more order and try to assign the store bag to same store on next day, it should not allow")
    public void process_AssignStoreBagWithoutReceivingPSOrders_FromStoreWhichAreNotReconciledForReturnOrders() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.UNASSIGNED);

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        Long storetripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storetripId.toString());
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.OUT_FOR_PICKUP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successfull in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE, tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails01 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo01 = lmsOrderDetails01.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo01);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo01, null, null, null, null, null, null, false).build();
        String response = storeHelper.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        Assert.assertTrue(response.contains(ResponseMessageConstants.shipmentReconcile),"Shipment was assigned to Store which was not reconciled");
    }

    @Test( priority = 6,enabled = true, description = "TC ID:C24725,Create a Store , Create order and assign it store, make it as Failed_delivered in store,Create one more order and try to assign store bag to same store on next day without receiving FD order, it should not allow ")
    public void processAssignStoreBagWithoutReceivingOldOrderstoStoreWhichAreNotReconciledForFDOrders() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);

        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.EXPECTED_IN_DC);
        //complete DC trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);
        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlshipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,tenantId,EnumSCM.FAILED_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails01 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo01 = lmsOrderDetails01.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo01);

        //create master bag
        ShipmentResponse shipmentResponse11 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId0 = shipmentResponse11.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId0));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId0,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId0), ShipmentType.DL, trackingNo01, null, null, null, null, null, null, false).build();
        String addShipmentSBResponse = storeHelper.addShipmentToStoreBag(masterBagId0, shipmentUpdateInfo1);
        Assert.assertTrue(addShipmentSBResponse.contains(ResponseMessageConstants.shipmentReconcile),"Shipment was assigned to Store which was not reconciled");
    }

    @Test(priority = 7, enabled=true, description = "TC ID:C24726, Create a return and with out pickup the return order from store try to assign another return to store next day, it should not allowed.")
    public void process_AssignReturOrderToStoreWhenFDOrderNotReconcile_Successful() throws Exception {

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Shipments not added Successfully to trip");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
       //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);

        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //complete DC trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlshipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        storeValidator.isNull(packetId1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLP.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.contains("Store "+storeCourierCode+" has backlog."), "Return Order is not assigned to store");

    }

    @Test( priority = 8, enabled = true, dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows",description = "TC ID:C24727,Create a return ,create reverse bag and pick them up and then try to reassign these pending shipments again to the same store and then again mark as FD and Unattempted,  next day we should not be able to add store bag and then deliver through SDA-CUSTOMER FLOW ")
    public void process_AssignReturOrderToStoreWhenFDOrderNotReconcile_DELIVER_ThroughSDACUSTOMER_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                                                                             String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        String searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created successfully");

        //get Store required values
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId = deliveryStaffEntry.getId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        String storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Shipments not added Successfully to trip");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);
        //complete DC trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);
        //Assign order to store SDA
        TripResponse tripResponse1 = mlshipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        storeValidator.isNull(packetId1);
        //validate OrderToShip
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusMessage = (String) storeHelper.assignPickupToHLP.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusMessage.contains("Store "+storeCourierCode+" has backlog."), "Return Order is assigned to store when the store has blacklogs");

        //Deliver the FD order through SDA-CUSTOMER
        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse5 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry1 = tripResponse5.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry1.size() == 0)) {
            reverseTripId = lstTripEntry1.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        TripResponse tripResponse8=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId));
        storeValidator.validateTripStatus(tripResponse8, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId), ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());  //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse5 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.closeMyntraTripWithStoreBag(tenantId, reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //reque the FD order in DC
        TripOrderAssignmentResponse tripOrderAssignmentResponse3=tripClient_qa.autoAssignmentOfOrderToTrip(packetId,tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Reque of the FD order is not successfull");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //create Trip
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse6 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId1 = tripResponse6.getTrips().get(0).getId();

        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse6.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse6, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5=lastmileClient.assignOrderToTrip(tripId1,trackingNo);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Order is not assigned to Dc trip");

        //start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6=lastmileClient.startTrip(tripId1.toString(),"10");
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripUpdate),"trip is not started for the DC");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(tripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString()); //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //Update trip order status as Delivered using DC SDA
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        //Complete DC SDA Trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //validate trip status
        TripResponse tripResponse3=lastmileClient.getTripDetailsByTripId(String.valueOf(tripId1));
        storeValidator.validateTripStatus(tripResponse3, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED);

        //After Delivering FD order assign return order to store
        //AssignPickupToHLP
        String statusType1 = (String) storeHelper.assignPickupToHLP.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType1.contains("1/1 Shipment assigned/UnAssigned successfully"), "Return Order is not assigned to store after receiving all pending order into DC");

    }

    @Test( priority = 9, enabled = true, description = "TC ID:C24728, Try to assign the return order to store and mark it as PS  and now do NOT bring it back to the DC and then next day also we should not be able to add any new order to store (since we have one pending return which has been picked up and not yet bought to the DC),")
    public void process_AssignReturOrderToStore_MakeStatusAsPS_WithoutReceivingInDC_TryToAssignOneMoreOrder_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        storeValidator.isNull(packetId1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code1, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId1,MLDeliveryShipmentStatus.UNASSIGNED.toString());

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId1);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        Long storetripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storetripId.toString());

        //Validate ML shipment status in DC
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,ShipmentUpdateEvent.OUT_FOR_PICKUP.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId1,ShipmentUpdateEvent.OUT_FOR_PICKUP.toString());

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId), ShipmentType.PU, storeTenantId1);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successfull in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE,tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKED_UP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId1,ShipmentUpdateEvent.PICKUP_SUCCESSFUL.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode1);

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry1 = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry1 = omsServiceHelper.getOrderLineEntry(orderReleaseEntry1.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse2 = rmsServiceHelper.createReturn(lineEntry1.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId1 = returnResponse2.getData().get(0).getId();
        storeValidator.isNull(returnId1.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId1));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId1));

        //getReturnTrackingNumber
        ReturnResponse returnResponse3 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId1));
        String returnTrackingNumber1 = returnResponse3.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber1);

        //AssignPickupToHLP
        String statusType1 = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber1, code1, tenantId);
        Assert.assertTrue(statusType1.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is assigned to store when the PS order is not received in DC");

    }

    @Test( priority =10, enabled= true, description = "TC ID:C24729, Create return and reverse bag  and pick the return and receive it and DON'T close the trip and try creating a store bag and add some orders to the bag (should not work) ,")
    public void process_AssignReturOrderToStore_MakeStatusAsPS_ReceivingInDC_TryToAssignOneMoreOrderToStore_WithoutCompletingTrip_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        storeValidator.isNull(packetId1);
        //validate OrderToShip status
        OrderResponse orderResponse01 = lmsClient.getOrderByOrderId(packetId1);
        Assert.assertTrue(orderResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse01, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse01.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        Long storetripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storetripId.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,ShipmentUpdateEvent.OUT_FOR_PICKUP.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.OUT_FOR_PICKUP.toString());

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successfull in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE,tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKED_UP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.PICKUP_SUCCESSFUL.toString());

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse1 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry = tripResponse1.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        TripResponse tripResponse2=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId));
        storeValidator.validateTripStatus(tripResponse2, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId), ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString()); //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKED_UP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.PICKUP_SUCCESSFUL.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(returnTrackingNumber);
        TripShipmentAssociationResponse tripShipmentAssociationResponse = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKED_UP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.PICKUP_SUCCESSFUL.toString());

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(returnTrackingNumber.substring(2, returnTrackingNumber.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.RECEIVED_IN_DC);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.PICKUP_SUCCESSFUL.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse8 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId3 = shipmentResponse8.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId3));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId3, ShipmentStatus.NEW.toString());
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo3 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId3), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response4 = masterBagClient_qa.addShipmentToStoreBag(masterBagId3, shipmentUpdateInfo3);
        Assert.assertTrue(response4.toLowerCase().contains(ResponseMessageConstants.errorStatus),"Order is assigned when the trip is not completed");

    }

    @Test( priority = 12, enabled = true,  description = " TC ID:C24730, Mark a return as Failed Pickup and next day try for EOD store bag creation and adding  and then re-process this return by attempting direct SDA-Customer Pickup and then mark as \"Happy with return \"")
    public void process_MarkOrderStatusAsFP_AttemptingDirectSDA_CustomerPickupAndMarkAsHappyWithReturn_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        storeValidator.isNull(packetId1);
        //validate OrderToShip status
        OrderResponse orderResponse01 = lmsClient.getOrderByOrderId(packetId1);
        Assert.assertTrue(orderResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse01, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse01.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        Long storetripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storetripId.toString());
        //Validate ML shipment status in DC
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.OUT_FOR_PICKUP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Failed in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.FAILED, EnumSCM.UPDATE, tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,ShipmentUpdateEvent.FAILED_PICKUP.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.FAILED_PICKUP.toString());
        //complete trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, storeTenantId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip), "Trip is not completed");
        statusPollingValidator.validateTripStatus(storetripId,EnumSCM.COMPLETED);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.FAILED_PICKUP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.ASSIGNED_TO_DC);

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);
        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse11 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId3 = shipmentResponse11.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId3));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId3, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo3 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId3), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response3 = storeHelper.addShipmentToStoreBag(masterBagId3, shipmentUpdateInfo3);
        Assert.assertTrue(response3.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //process the return through SDA-Customer pickup and then mark as "Happy with return "
        //reque the FP order
        storeHelper.createRTO(returnTrackingNumber,storeHlPId, MLShipmentUpdateEvent.UNASSIGN,ShipmentType.PU, ShipmentUpdateActivityTypeSource.MyntraLogistics,storeTenantId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.FAILED_PICKUP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());

        //create Trip From DC to Customer
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse4 = lastmileClient.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse4.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse4.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse4, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //assign return order to trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3=lastmileClient.assignOrderToTrip(tripId,returnTrackingNumber);
        Assert.assertEquals(tripOrderAssignmentResponse3.getStatus().getStatusMessage(),ResponseMessageConstants.assignOrderToStoreBag,"Return order is not assigned to trip");

        //start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4=lastmileClient.startTrip(tripId.toString(),"30");
        Assert.assertEquals(tripOrderAssignmentResponse4.getStatus().getStatusMessage(),ResponseMessageConstants.tripUpdate,"Trip not started");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(tripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());  //Validate ML shipment status in DC
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.PICKUP_SUCCESSFUL.toString());

    }

    @Test(  priority =13,enabled = true, description = "TC ID:C24731,Mark a return as Failed Pickup and next day try for EOD store bag creation and adding  and then re-process this return by attempting via SAME STORE flow and then mark as \"Happy with return \"")
    public void process_MarkOrderStatusAsFP_AttemptingSameStoreSDA_CustomerPickupAndMarkAsHappyWithReturn_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        storeValidator.isNull(packetId1);
        //validate OrderToShip status
        OrderResponse orderResponse01 = lmsClient.getOrderByOrderId(packetId1);
        Assert.assertTrue(orderResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse01, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse01.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        Long storetripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storetripId.toString());
        //Validate ML shipment status in DC
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.OUT_FOR_PICKUP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup FAILED in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.FAILED, EnumSCM.UPDATE, tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.FAILED_PICKUP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.FAILED_PICKUP);

        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip), "Trip is not completed");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.FAILED_PICKUP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.ASSIGNED_TO_DC);

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);
        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse11 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId3 = shipmentResponse11.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId3));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId3, ShipmentStatus.NEW.toString());

        //Add order to MB
        //TODO check why its not allowing
        ShipmentUpdateInfo shipmentUpdateInfo3 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId3), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response3 = storeHelper.addShipmentToStoreBag(masterBagId3, shipmentUpdateInfo3);
        Assert.assertTrue(response3.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //process the return through SDA-Customer pickup and then mark as "Happy with return "
        //reque the FP order
        storeHelper.createRTO(returnTrackingNumber,storeHlPId, MLShipmentUpdateEvent.UNASSIGN,ShipmentType.PU, ShipmentUpdateActivityTypeSource.MyntraLogistics,storeTenantId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.FAILED_PICKUP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers1 = new ArrayList<>();
        lstTrackingNumbers1.add(returnTrackingNumber);
        TripResponse tripResponse1 = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers1, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        Long storetripId1 = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storetripId1.toString());
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.OUT_FOR_PICKUP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId1), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId1 = tripOrderAssignmentResponse3.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId1.toString());
        //update order as Pickup Successful in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = lastmileClient.updatePickupInTrip(storeTripOrderId1, EnumSCM.HAPPY_WITH_PRODUCT, EnumSCM.UPDATE, tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.FAILED_PICKUP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,EnumSCM.FAILED_PICKUP);

        //complete trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lastmileClient.updatePickupInTrip(storeTripOrderId1, EnumSCM.HAPPY_WITH_PRODUCT, EnumSCM.TRIP_COMPLETE, tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip), "Trip is not completed");

    }
}
