package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_RollingTripSheetDP;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.DeliveryStaffType;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.OrderShipmentAssociationStatus;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class Lastmile_RollingTripSheet {

    static Logger log = Logger.getLogger(Lastmile_RollingTripSheet.class);
    StoreClient storeClient;
    DeliveryCenterClient deliveryCenterClient;

    LMSOrderDetailsClient lmsOrderDetailsClient;
    MasterBagClient masterBagClient;
    MLShipmentClientV2 mlShipmentServiceV2Client;
    DeliveryStaffClient deliveryStaffClient;
    TripOrderAssignmentClient tripOrderAssignmentClient;
    private LMSHelper lmsHepler = new LMSHelper();
    private LastmileHelper lastmileHelper;
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa=new DeliveryStaffClient_QA();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa=new TripOrderAssignmentClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        deliveryCenterClient = new DeliveryCenterClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        deliveryStaffClient = new DeliveryStaffClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    @Test(groups = {"Sanity",
    }, priority = 0, dataProviderClass = Lastmile_RollingTripSheetDP.class, dataProvider = "rollingTripFGOn", description = " Create a Store and attach it to a DC then inscan the shipments and deliver it")
    public void EODTest(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                        String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws UnsupportedEncodingException, JAXBException {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        int noOfOrders = 2;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> failedDlTrackingList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        Map<String, Long> trackingInstakartMlIdMap = new HashMap<>();
        Map<String, Long> trackingStoreMlIdMap = new HashMap<>();
        Map<String, Float> trackingShipmentValue = new HashMap<>();

        //  tripOrderAssignmentClient.receiveTripShipments("ML0001599587",LMS_CONSTANTS.TENANTID);

        //1. Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();

        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());


        //Create a MasterBag from pincode : LMS_PINCODE.ML_BLR (order's pincode)

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //2. Create Forward Order Till SH

        for (int i = 0; i < noOfOrders; i++) {
            try {
                orderID = lmsHepler.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
                log.info("The orderId is : +" + orderID);
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Order creation in SH state failed : " + e.toString());
            }


            //Add the order to the MB

            String packetId = omsServiceHelper.getPacketId(orderID);
            log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
            forwardTrackingNumbersList.add(trackingNo);
            trackingNoOrderIdMap.put(trackingNo, packetId);
            log.info("Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
            System.out.println("\n_____________Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

            ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
            try {
                //TODO : Try to use addShipmentToMasterBag
                //TODO : negative cases here
                masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
            System.out.println("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

            //Verify if the order is added in Bag
            OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
            Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
            System.out.println("\n______________The status of the " + i + " orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
            log.info("_____________The status of the " + i + " order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());

        }
        //We will mark the second tracking number as Failed Delivery
        failedDlTrackingList.add(forwardTrackingNumbersList.get(1));


        //Creating a trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");

        //TODO : TO BE CHECKED BY DEV
      /*  Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentItems().size().equals(noOfOrders));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().name().equals(ShipmentType.FORWARD_BAG.name());
        */


        //Validate the MB status is ADDED_TO_TRIP
        //TODO : Add in validator
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, noOfOrders, code, originPremiseId);


        //TODO : to check after adding bag to trip, sipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));
        }

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        //TODO : TO BE CHECKED BY DEV
        //Validate status  of this above response is OFD
        searchParams = "tripNumber.like:" + tripNumber;
        TripResponse tripResponse1 = tripClient_qa.searchTripByParam(0, 20, "id", "ASC", Boolean.parseBoolean(null), searchParams, "");
        //TODO : to add in validation
        Assert.assertTrue(tripResponse1.getTrips().get(0).getDeliveryStaffEntry().getDeliveryStaffType().name().equals(DeliveryStaffType.MYNTRA_PAYROLL.name()));
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        lastmileHelper.validateTripStartedForMB(masterBagId);

        //Before delivering the MB validate that there is nothing in the store
        //Validate getUnattempted api in the store is empty
        List<String> forwardUnattemptedShipmentsB4DL = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        Assert.assertTrue(forwardUnattemptedShipmentsB4DL.isEmpty(), "Store has some unAttempted items before delivering the MB to the stpre itself");
        //Before delivering the bag to store, validate only one entry in ML

        //Vaidate ONLY one  ML entry created for tenant
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            // Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Saving the ml id for tenant 4019 , this will be source ref id for mlResponse with storeTenant
            trackingInstakartMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            trackingShipmentValue.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentValue());
            //Validate status of Instakart ML shipment is MLDeliveryShipmentStatus
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.name()));

        }
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            //Validate there is no ML shipment created for Store
            Assert.assertTrue(mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId).getStatus().getStatusMessage().equals("Invalid Shipment Id"));

        }

        //Deliver the MB

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);

        //Validate if shipment_order_map status is DELIVERED

        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }
        //Validate getUnattempted api in the store
        List<String> forwardUnattemptedShipments = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);

        //TODO - GLORIAAAAA - CHECK THE forwardUnattemptedShipments is empty
        Assert.assertTrue(forwardTrackingNumbersList.containsAll(forwardUnattemptedShipments), "Store does not right number of unattempted items");
        try {
            Thread.sleep(25000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Vaidate new ML entry created for tenant
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            // For 4019 no entry of getMlLastMilePartnerShipmentAssignment should return NULL
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            //   mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();

            //Validate tenant is 4019 and client is 2297
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.CLIENTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID));

            //Validate the source reference id is the orderId
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId().equals(trackingNoOrderIdMap.get(track)));


            //With lms.enable_mda_scan_at_the_store not enabled for the store the status will be because automatically a trip is created
            //TODO : Rolling Validate the status for the Store Tenant ML entry as HANDED_OVER_TO_LASTMILE_PARTNER (in mensa since the trip is created it will be OFD
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.name()));

            //Validate the DC id is 5

            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(originPremiseId));


        }
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.EXPECTED_IN_DC.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));

        }
        //After delivering the store bag to store , the shipment order map status should be DELIVERED
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }


        //Get delivery staff for Store by searching using dc id
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        long expectedstoreDeliveryStaffId = deliveryStaffClient_qa.findDeliveryStaffByMobNo2(mobileNumber);
        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
//TODO ------------------------ storeTrip.getTrips().get(0).getTripStatus() -> CREATED instead of OFD, sda trip : tripResponse1.getTrips().get(0).getTripStatus() is CREATED
        //Validate after creating of Store Bag it becomes OFD
        Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        //Validate Myntra SDA trip is OFD
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        //Validate the Store ML Shipment goes for OFD
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));


            //VAlidate the ORderToShip (Platform ) status is OFD
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            OrderResponse orderResponse = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(track),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.OUT_FOR_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.OUT_FOR_DELIVERY.name());

        }

        //Validate Store Trip tenant and status and code
        //TODO : Check with Azeem why storeTrip.getTrips().get(0).getTripStatus() is returning CREATED instead of OFD
        //   Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        //Get the store summary before delivering the shipments to Customer validate
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateStoreSummaryBeforeDelivering(originPremiseId, storeHlPId, forwardTrackingNumbersList, noOfOrders);

        //Now deliver forwardTrackingNumbersList.get(0) order to customer with PREPAID

        //TODO : ask the payment methods to dev
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);

        //TODO :___________________Validate this trip is in OFD
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Assert.assertTrue(tripOrderAssignment.getTripOrders().get(0).getTripOrderStatus().name().equals("Out for delivery"));
        tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(forwardTrackingNumbersList.get(0)).toString(), tripOrderAssignmentId, storeTenantId, "PRE_PAID",LASTMILE_CONSTANTS.TENANT_ID);

        // Validate                "totalAmountToBeCollected": 0,
       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 1,
                "totalUnAttempted": 1,*/

        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, 1, noOfOrders, 1, 1);
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, 0.0f);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        //Validate Store ML shipment is Delivered
        MLShipmentResponse mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(forwardTrackingNumbersList.get(0), storeTenantId);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

        //Validate INstakart ML shipment is Delivered
        mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(forwardTrackingNumbersList.get(0), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

        //VAlidate the ORderToShip (Platform ) status is OFD

        OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(forwardTrackingNumbersList.get(0)),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.DELIVERED), "The platform  orderToship status is not : " + ShipmentStatus.DELIVERED.name());

        lastmileHelper.validateStoreMLEntryDelivered(originPremiseId, storeHlPId, storeTenantId, forwardTrackingNumbersList.get(0));


        //Now fail the other  forwardTrackingNumbersList.get(1)/ failedDlTrackingList.get(0)
        //Get the tripOrderAssignment id for the store trip
        tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(failedDlTrackingList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);

        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(failedDlTrackingList.get(0)).toString(), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);

       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 2,
                "totalUnAttempted": 0,   "totalAmountToBeCollected": 0,
*/

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, 2, noOfOrders, 1, 0);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        lastmileHelper.validateStoreMLEntryFailedDeliveredb4CloseTrip(originPremiseId, storeHlPId, storeTenantId, failedDlTrackingList.get(0));
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, 0.0f);


        //Validate Store ML shipment is Failed Delivered
        mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(failedDlTrackingList.get(0), storeTenantId);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Sleeping since this is async
        //Validate INstakart ML shipment is Failed Delivered
        mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(failedDlTrackingList.get(0), LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus(), (MLDeliveryShipmentStatus.FAILED_DELIVERY.name()), "The status is not matching");

        //VAlidate the ORderToShip (Platform ) status is FAILED_DELIVERY

        orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(failedDlTrackingList.get(0)),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.FAILED_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.FAILED_DELIVERY.name());

        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());

//Close all the store  trip - should fail since we did not receive the shipments


        TripOrderAssignmentResponse closeTrip = tripClient_qa.closeStoreTrip(storeTripId, tripOrderIdAttemptReasonMap, storeTenantId);
        Assert.assertTrue(closeTrip.getStatus().getStatusType().toString().toLowerCase().contains("error"));
        //Validate Store ML shipment is STILL Failed Delivered and NOT RECeiVED_IN_DC
        mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(failedDlTrackingList.get(0), storeTenantId);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));


        //Now Receive the shipment

        tripOrderAssignmentClient_qa.receiveTripShipments(failedDlTrackingList.get(0), LMS_CONSTANTS.TENANTID);

        //Close trip
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);

        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");

//Validate store summary before creating reverse bag

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, 2, noOfOrders, 1, 0);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, 0.0f);
        //After closing the trip the failedDelivered item ,status MLResponse for store entry is RECEIVED_IN_DC

        //
        lastmileHelper.validateStoreMLEntryFailedDeliveredAfterCloseTrip(storeTenantId, failedDlTrackingList.get(0));


//Create Trip
        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + failedDlTrackingList.get(0) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + failedDlTrackingList.get(0) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");
        //Validate the reverse bag contents displayed in love :http://lastmile.scmqa.myntra.com/lastmile-service/trip/v2/findShipmentsByTripNumber/Automation1-181120155306433?shipmentType=REVERSE_BAG&tenantId=4019
      /*  "shipmentId": 21221343,
                "shipmentItemCount": 1,
                "shipmentType": "REVERSE_BAG",
                "staffPhoneNumber": 1511690820,
                "status": "WFD",
                "storeDCId": 9235,
                "totalFailedDeliveriesPicked": 0,
                "totalFailedDeliveriesToPick": 1,
                "totalSuccess": 1,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,
                "totalUnAttempted": 0,
                "tripId": 59046*/

        TripShipmentAssociationResponse reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(reverseTripNumber, ShipmentType.REVERSE_BAG, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(reverseBagId));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStoreDCId().equals(storeHlPId));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getMyntraSDATripNumber().equals(reverseTripNumber));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue((reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getReverseBagExpectedShipmentTrackingNumbers().containsAll(forwardTrackingNumbersList)));

        //We have 1 failed delivery
        /* "shipmentItemCount": 1,*/

        lastmileHelper.validateReverseBagShipmentItemCnt(reverseTripNumber, LMS_CONSTANTS.TENANTID, failedDlTrackingList.size());
        //Validate the amount To be collected is 0.0 and amount collected is 0.0
   /*     "amountCollected": 0,
                "amountToBeCollected": 0,*/
        lastmileHelper.validateReverseBagAmount(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0.0f, 0.0f);
   /*     "totalFailedDeliveriesPicked": 0,
                "totalFailedDeliveriesToPick": 1,*/
        lastmileHelper.validateReverseFailedDeliveries(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0, 1);
      /*  "totalSuccess": 1,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,*/
        lastmileHelper.validateReverseSuccess(reverseTripNumber, LMS_CONSTANTS.TENANTID, 1, 0, 0);
//        "totalUnAttempted": 0,
        lastmileHelper.validateReverseUnAttempted(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0);
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");


        //Validate the reverse MB is ADDED_TO_TRIP and there are no shipments present in ShipmentOrderMap
        //shipmentOrderMap has NO entry since we have not delivered the items
        lastmileHelper.validateForReverseMbAddedToTrip(Long.valueOf(reverseBagId));

        //Start the reverse trip

        tripClient_qa.startTrip(reverseTripId);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickip
        // Pickup the reverse bag
        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStore(String.valueOf(reverseBagId), failedDlTrackingList, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, failedDlTrackingList.size(), 0, 0.0f,LMS_CONSTANTS.TENANTID);


        // reverse bag should be IN_TRANSIT after pickup
        //Shipment orderMap will now have these entries ONLY which has been scanned
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateForReverseMbEntriesAfterPickup(Long.valueOf(reverseBagId), failedDlTrackingList);

        //Store Summary should not have any data since everything is reconcilied
        lastmileHelper.validateStoreSummaryAfterReconcilation(originPremiseId, storeHlPId);
       /* "amountCollected": 0,
                "amountToBeCollected": 0,
                "attemptReasonCode": "PICKED_UP_SUCCESSFULLY",
                "deliveryStaffName": "LA9566 - LA_FN9566 LA_LN9566",
                "itemsPickedUp": 0,
                "myntraSDATripNumber": "AAAA-181120201411814",
                "shipmentId": 21221594,
                "shipmentItemCount": 1,
                "shipmentType": "REVERSE_BAG",
                "staffPhoneNumber": 1733995666,
                "status": "PS",
                "totalFailedDeliveriesPicked": 1,
                "totalFailedDeliveriesToPick": 1,
                "totalSuccess": 0,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,
                "totalUnAttempted": 0,*/


        reverseBagTripShipment = tripClient_qa.getReverseShipmentBagDetails(reverseTripNumber, ShipmentType.REVERSE_BAG, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getAttemptReasonCode().name().equals(AttemptReasonCode.PICKED_UP_SUCCESSFULLY.name()));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.PS.name()));
        Assert.assertTrue(reverseBagTripShipment.getTripShipmentAssociationEntryList().get(0).getMyntraSDATripNumber().equals(reverseTripNumber));
        //Validate the amount To be collected is 0.0 and amount collected is 0.0
   /*     "amountCollected": 0,
                "amountToBeCollected": 0,*/
        lastmileHelper.validateReverseBagAmount(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0.0f, 0.0f);
   /*     "totalFailedDeliveriesPicked": 1,
                "totalFailedDeliveriesToPick": 1,*/
        lastmileHelper.validateReverseFailedDeliveries(reverseTripNumber, LMS_CONSTANTS.TENANTID, 1, 1);
      /*  "totalSuccess": 0,
                "totalSuccessfulReturnsPicked": 0,
                "totalSuccessfulReturnsToPick": 0,*/
        //TODO : itemsPickedUp and totalSuccess are NOT used
        lastmileHelper.validateReverseSuccess(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0, 0, 0);
//        "totalUnAttempted": 0,
        lastmileHelper.validateReverseUnAttempted(reverseTripNumber, LMS_CONSTANTS.TENANTID, 0);
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");

        //Close trip
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, reverseTripId);
        //Validate Unattempted Shipment response has nothing (same as store summary)

        lastmileHelper.validateUnattemptedAfterReconcilation(storeHlPId);

        //TODO : Receive Shipments


    }

//TODO : Gloria : Error when FG is OFF

    @Test(groups = {"Sanity",
    }, priority = 0, dataProviderClass = Lastmile_RollingTripSheetDP.class, dataProvider = "EODTripFGOn", description = " Create a Store and attach it to a DC then inscan the shipments and deliver it")
    public void EODTripFGOn(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                            String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        int noOfOrders = 2;

        //1. Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();

        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());


        //Create a MasterBag from pincode : LMS_PINCODE.ML_BLR (order's pincode)

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        lastmileHelper.validateMasterBagCreation(masterBagId, code);
        //2. Create Order Till SH

        for (int i = 0; i < noOfOrders; i++) {
            try {
                orderID = lmsHepler.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
                log.info("The orderId is : +" + orderID);
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Order creation in SH state failed : " + e.toString());
            }


            //Add the order to the MB

            String packetId = omsServiceHelper.getPacketId(orderID);
            log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
            log.info("Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
            System.out.println("\n_____________Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

            ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
            try {
                //TODO : Try to use addShipmentToMasterBag
                //TODO : negative cases here
                masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
            System.out.println("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

            //Verify if the order is added in Bag
            OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
            Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
            System.out.println("\n______________The status of the " + i + " orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
            log.info("_____________The status of the " + i + " order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());

        }
        //Creating a trip
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + 5);

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");

        //TODO : TO BE CHECKED BY DEV
      /*  Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentItems().size().equals(noOfOrders));
*/

        //Validate the MB status is ADDED_TO_TRIP
        //TODO : Add in validator
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, noOfOrders, code, originPremiseId);

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        //TODO : TO BE CHECKED BY DEV
        //Validate status  of this above response is OFD
        searchParams = "tripNumber.like:" + tripNumber;
        TripResponse tripResponse1 = tripClient_qa.searchTripByParam(0, 20, "id", "ASC", Boolean.parseBoolean(null), searchParams, "");
        //TODO : to add in validation
        Assert.assertTrue(tripResponse1.getTrips().get(0).getDeliveryStaffEntry().getDeliveryStaffType().name().equals(DeliveryStaffType.MYNTRA_PAYROLL.name()));
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        lastmileHelper.validateTripStartedForMB(masterBagId);


    }


    @Test(groups = {"Sanity",
    }, priority = 0, dataProviderClass = Lastmile_RollingTripSheetDP.class, dataProvider = "rollingTripFGOn", description = " Create a Store and attach it to a DC then inscan the shipments and deliver it")
    public void createReverseBagWithoutTripClose(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                 String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        int noOfOrders = 2;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        Map<String, Long> trackingInstakartMlIdMap = new HashMap<>();
        Map<String, Long> trackingStoreMlIdMap = new HashMap<>();
        Map<String, Float> trackingShipmentValue = new HashMap<>();

        //1. Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();

        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());


        //Create a MasterBag from pincode : LMS_PINCODE.ML_BLR (order's pincode)

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //2. Create Forward Order Till SH

        for (int i = 0; i < noOfOrders; i++) {
            try {
                orderID = lmsHepler.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
                log.info("The orderId is : +" + orderID);
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Order creation in SH state failed : " + e.toString());
            }


            //Add the order to the MB

            String packetId = omsServiceHelper.getPacketId(orderID);
            log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
            forwardTrackingNumbersList.add(trackingNo);
            trackingNoOrderIdMap.put(trackingNo, packetId);
            log.info("Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
            System.out.println("\n_____________Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

            ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
            try {
                //TODO : Try to use addShipmentToMasterBag
                //TODO : negative cases here
                masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
            System.out.println("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

            //Verify if the order is added in Bag
            OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
            Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
            System.out.println("\n______________The status of the " + i + " orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
            log.info("_____________The status of the " + i + " order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());

        }
        //Creating a trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");

        //TODO : TO BE CHECKED BY DEV
      /*  Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentItems().size().equals(noOfOrders));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().name().equals(ShipmentType.FORWARD_BAG.name());
        */


        //Validate the MB status is ADDED_TO_TRIP
        //TODO : Add in validator
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, noOfOrders, code, originPremiseId);


        //TODO : to check after adding bag to trip, sipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));
        }

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        //TODO : TO BE CHECKED BY DEV
        //Validate status  of this above response is OFD
        searchParams = "tripNumber.like:" + tripNumber;
        TripResponse tripResponse1 = tripClient_qa.searchTripByParam(0, 20, "id", "ASC", Boolean.parseBoolean(null), searchParams, "");
        //TODO : to add in validation
        Assert.assertTrue(tripResponse1.getTrips().get(0).getDeliveryStaffEntry().getDeliveryStaffType().name().equals(DeliveryStaffType.MYNTRA_PAYROLL.name()));
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        lastmileHelper.validateTripStartedForMB(masterBagId);

        //Before delivering the MB validate that there is nothing in the store
        //Validate getUnattempted api in the store is empty
        List<String> forwardUnattemptedShipmentsB4DL = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        Assert.assertTrue(forwardUnattemptedShipmentsB4DL.isEmpty(), "Store has some unAttempted items before delivering the MB to the stpre itself");
        //Before delivering the bag to store, validate only one entry in ML

        //Vaidate ONLY one  ML entry created for tenant
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            // Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Saving the ml id for tenant 4019 , this will be source ref id for mlResponse with storeTenant
            trackingInstakartMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            trackingShipmentValue.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentValue());
            //Validate status of Instakart ML shipment is MLDeliveryShipmentStatus
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.name()));

        }
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            //Validate there is no ML shipment created for Store
            Assert.assertTrue(mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId).getStatus().getStatusMessage().equals("Invalid Shipment Id"));

        }

        //Deliver the MB

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);

        //Validate if shipment_order_map status is DELIVERED

        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }
        //Validate getUnattempted api in the store
        List<String> forwardUnattemptedShipments = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        Assert.assertTrue(forwardTrackingNumbersList.containsAll(forwardUnattemptedShipments), "Store does not right number of unattempted items");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Vaidate new ML entry created for tenant
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            // For 4019 no entry of getMlLastMilePartnerShipmentAssignment should return NULL
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            //   mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();

            //Validate tenant is 4019 and client is 2297
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.CLIENTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID));

            //Validate the source reference id is the orderId
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId().equals(trackingNoOrderIdMap.get(track)));

            //Validate the status for the Store Tenant ML entry as HANDED_OVER_TO_LASTMILE_PARTNER
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.name()));

            //Validate the DC id is 5

            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(originPremiseId));


        }
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.EXPECTED_IN_DC.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));

        }
        //After delivering the store bag to store , the shipment order map status should be DELIVERED
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }


        //Get delivery staff for Store by searching using dc id
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        long expectedstoreDeliveryStaffId = deliveryStaffClient_qa.findDeliveryStaffByMobNo2(mobileNumber);
        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
//TODO ------------------------ storeTrip.getTrips().get(0).getTripStatus() -> CREATED instead of OFD, sda trip : tripResponse1.getTrips().get(0).getTripStatus() is CREATED
        //Validate after creating of Store Bag it becomes OFD
        Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        //Validate Myntra SDA trip is OFD
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        //Validate the Store ML Shipment goes for OFD
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));


            //VAlidate the ORderToShip (Platform ) status is OFD
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            OrderResponse orderResponse = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(track),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.OUT_FOR_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.OUT_FOR_DELIVERY.name());

        }

        //Validate Store Trip tenant and status and code
        //TODO : Check with Azeem why storeTrip.getTrips().get(0).getTripStatus() is returning CREATED instead of OFD
        //   Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        //Get the store summary before delivering the shipments to Customer validate
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateStoreSummaryBeforeDelivering(originPremiseId, storeHlPId, forwardTrackingNumbersList, noOfOrders);

        //Now deliver forwardTrackingNumbersList.get(0) order to customer with PREPAID

        //TODO : ask the payment methods to dev
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);

        //TODO :___________________Validate this trip is in OFD
        //Assert.assertTrue(tripOrderAssignment.getTripOrders().get(0).getTripOrderStatus().name().equals("Out for delivery"));
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(forwardTrackingNumbersList.get(0)).toString(), tripOrderAssignmentId, storeTenantId, "PRE_PAID",LASTMILE_CONSTANTS.TENANT_ID);

        // Validate                "totalAmountToBeCollected": 0,
       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 1,
                "totalUnAttempted": 1,*/

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, 1, noOfOrders, 1, 1);
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, 0.0f);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        //Validate Store ML shipment is Delivered
        MLShipmentResponse mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(forwardTrackingNumbersList.get(0), storeTenantId);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

        //Validate INstakart ML shipment is Delivered
        mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(forwardTrackingNumbersList.get(0), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

        //VAlidate the ORderToShip (Platform ) status is OFD

        OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(forwardTrackingNumbersList.get(0)),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.DELIVERED), "The platform  orderToship status is not : " + ShipmentStatus.DELIVERED.name());

        lastmileHelper.validateStoreMLEntryDelivered(originPremiseId, storeHlPId, storeTenantId, forwardTrackingNumbersList.get(0));


        //Now fail the other  forwardTrackingNumbersList.get(1)
        //Get the tripOrderAssignment id for the store trip
        tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(forwardTrackingNumbersList.get(1)).toString(), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);

       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 2,
                "totalUnAttempted": 0,   "totalAmountToBeCollected": 0,
*/

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, 2, noOfOrders, 1, 0);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        lastmileHelper.validateStoreMLEntryFailedDeliveredb4CloseTrip(originPremiseId, storeHlPId, storeTenantId, forwardTrackingNumbersList.get(1));
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, 0.0f);


        //Validate Store ML shipment is Failed Delivered
        mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(forwardTrackingNumbersList.get(1), storeTenantId);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

        //Validate INstakart ML shipment is Delivered
        mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(forwardTrackingNumbersList.get(1), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

        //VAlidate the ORderToShip (Platform ) status is FAILED_DELIVERY

        orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(forwardTrackingNumbersList.get(1)),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.FAILED_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.FAILED_DELIVERY.name());

        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());

//WITHOUT CLOSING TRIP


        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
    }


    @Test(groups = {"Sanity",
    }, priority = 0, dataProviderClass = Lastmile_RollingTripSheetDP.class, dataProvider = "rollingTripFGOn", description = " Create a Store and attach it to a DC then inscan the shipments and deliver it")
    public void rollingTripFGOn_CASH(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                     String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws UnsupportedEncodingException, JAXBException {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        int noOfOrders = 2;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        Map<String, Long> trackingInstakartMlIdMap = new HashMap<>();
        Map<String, Long> trackingStoreMlIdMap = new HashMap<>();
        Map<String, Float> trackingShipmentValue = new HashMap<>();

        //1. Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();

        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());


        //Create a MasterBag from pincode : LMS_PINCODE.ML_BLR (order's pincode)

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //2. Create Forward Order Till SH

        for (int i = 0; i < noOfOrders; i++) {
            try {
                orderID = lmsHepler.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
                log.info("The orderId is : +" + orderID);
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Order creation in SH state failed : " + e.toString());
            }


            //Add the order to the MB

            String packetId = omsServiceHelper.getPacketId(orderID);
            log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
            forwardTrackingNumbersList.add(trackingNo);
            trackingNoOrderIdMap.put(trackingNo, packetId);
            log.info("Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
            System.out.println("\n_____________Adding trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

            ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
            try {
                //TODO : Try to use addShipmentToMasterBag
                //TODO : negative cases here
                masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
            System.out.println("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

            //Verify if the order is added in Bag
            OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
            Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
            System.out.println("\n______________The status of the " + i + " orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
            log.info("_____________The status of the " + i + " order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());

        }
        //Creating a trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");

        //TODO : TO BE CHECKED BY DEV
      /*  Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().name().equals(TripOrderStatus.WFD.name()));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentItems().size().equals(noOfOrders));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().name().equals(ShipmentType.FORWARD_BAG.name());
        */


        //Validate the MB status is ADDED_TO_TRIP
        //TODO : Add in validator
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, noOfOrders, code, originPremiseId);


        //TODO : to check after adding bag to trip, sipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));
        }

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        //TODO : TO BE CHECKED BY DEV
        //Validate status  of this above response is OFD
        searchParams = "tripNumber.like:" + tripNumber;
        TripResponse tripResponse1 = tripClient_qa.searchTripByParam(0, 20, "id", "ASC", Boolean.parseBoolean(null), searchParams, "");
        //TODO : to add in validation
        Assert.assertTrue(tripResponse1.getTrips().get(0).getDeliveryStaffEntry().getDeliveryStaffType().name().equals(DeliveryStaffType.MYNTRA_PAYROLL.name()));
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        lastmileHelper.validateTripStartedForMB(masterBagId);

        //Before delivering the MB validate that there is nothing in the store
        //Validate getUnattempted api in the store is empty
        List<String> forwardUnattemptedShipmentsB4DL = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        Assert.assertTrue(forwardUnattemptedShipmentsB4DL.isEmpty(), "Store has some unAttempted items before delivering the MB to the stpre itself");
        //Before delivering the bag to store, validate only one entry in ML

        //Vaidate ONLY one  ML entry created for tenant
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            // Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Saving the ml id for tenant 4019 , this will be source ref id for mlResponse with storeTenant
            trackingInstakartMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            trackingShipmentValue.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentValue());
            //Validate status of Instakart ML shipment is MLDeliveryShipmentStatus
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.name()));

        }
        for (String track : forwardTrackingNumbersList) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            //Validate there is no ML shipment created for Store
            Assert.assertTrue(mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId).getStatus().getStatusMessage().equals("Invalid Shipment Id"));

        }

        //Deliver the MB

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);

        //Validate if shipment_order_map status is DELIVERED

        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }
        //Validate getUnattempted api in the store
        List<String> forwardUnattemptedShipments = tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        Assert.assertTrue(forwardTrackingNumbersList.containsAll(forwardUnattemptedShipments), "Store does not right number of unattempted items");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Vaidate new ML entry created for tenant
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, LMS_CONSTANTS.TENANTID);
            // For 4019 no entry of getMlLastMilePartnerShipmentAssignment should return NULL
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries
            //   mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();

            //Validate tenant is 4019 and client is 2297
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.CLIENTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID));

            //Validate the source reference id is the orderId
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId().equals(trackingNoOrderIdMap.get(track)));

            //Validate the status for the Store Tenant ML entry as HANDED_OVER_TO_LASTMILE_PARTNER
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.name()));

            //Validate the DC id is 5

            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(originPremiseId));


        }
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.EXPECTED_IN_DC.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));

        }
        //After delivering the store bag to store , the shipment order map status should be DELIVERED
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }


        //Get delivery staff for Store by searching using dc id
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        long expectedstoreDeliveryStaffId = deliveryStaffClient_qa.findDeliveryStaffByMobNo2(mobileNumber);
        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
//TODO ------------------------ storeTrip.getTrips().get(0).getTripStatus() -> CREATED instead of OFD, sda trip : tripResponse1.getTrips().get(0).getTripStatus() is CREATED
        //Validate after creating of Store Bag it becomes OFD
        Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        //Validate Myntra SDA trip is OFD
        Assert.assertTrue(tripResponse1.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));

        //Validate the Store ML Shipment goes for OFD
        for (String track : forwardUnattemptedShipments) {
            MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(track, storeTenantId);
            // For store Tenant getMlLastMilePartnerShipmentAssignment should return something
            //TODO : to check with Vivek for ml_last_mile_partner_shipment_assignment entries

            //   Long mlId = mlShipmentResponse.getMlShipmentEntries().get(0).getMlLastMilePartnerShipmentAssignment().getId();
            //Validate the client id is 4019 and tenantId is storeTenant id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));

            //Validate the trackingMlIdMap is is the mlResponse id with tenant 4019
            Assert.assertTrue(String.valueOf(trackingInstakartMlIdMap.get(track)).equals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId()));

            //Saving the Store ML id
            trackingStoreMlIdMap.put(track, mlShipmentResponse.getMlShipmentEntries().get(0).getId());
            //Validate the status for the Store Tenant ML entry as EXPECTED_IN_DC
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));


            //VAlidate the ORderToShip (Platform ) status is OFD
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            OrderResponse orderResponse = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(track),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.OUT_FOR_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.OUT_FOR_DELIVERY.name());

        }

        //Validate Store Trip tenant and status and code
        //TODO : Check with Azeem why storeTrip.getTrips().get(0).getTripStatus() is returning CREATED instead of OFD
        //   Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(TripStatus.OUT_FOR_DELIVERY.name()));
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        //Get the store summary before delivering the shipments to Customer validate
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastmileHelper.validateStoreSummaryBeforeDelivering(originPremiseId, storeHlPId, forwardTrackingNumbersList, noOfOrders);

        //Now deliver forwardTrackingNumbersList.get(0) order to customer with PREPAID

        //TODO : ask the payment methods to dev
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);

        //TODO :___________________Validate this trip is in OFD
        //Assert.assertTrue(tripOrderAssignment.getTripOrders().get(0).getTripOrderStatus().name().equals("Out for delivery"));
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.deliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(forwardTrackingNumbersList.get(0)).toString(), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        // Validate                "totalAmountToBeCollected": 0,
       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 1,
                "totalUnAttempted": 1,*/

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, 1, noOfOrders, 1, 1);
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, trackingShipmentValue.get(forwardTrackingNumbersList.get(0)));
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        //Validate Store ML shipment is Delivered
        MLShipmentResponse mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(forwardTrackingNumbersList.get(0), storeTenantId);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

        //Validate INstakart ML shipment is Delivered
        mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(forwardTrackingNumbersList.get(0), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

        //VAlidate the ORderToShip (Platform ) status is OFD

        OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(forwardTrackingNumbersList.get(0)),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.DELIVERED), "The platform  orderToship status is not : " + ShipmentStatus.DELIVERED.name());

        lastmileHelper.validateStoreMLEntryDelivered(originPremiseId, storeHlPId, storeTenantId, forwardTrackingNumbersList.get(0));


        //Now fail the other  forwardTrackingNumbersList.get(1)
        //Get the tripOrderAssignment id for the store trip
        tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, trackingStoreMlIdMap.get(forwardTrackingNumbersList.get(1)).toString(), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);

       /* "totalCount": 2,
                "totalSuccess": 1,
                "totalAttempted": 2,
                "totalUnAttempted": 0,   "totalAmountToBeCollected": 0,
*/

        lastmileHelper.validStoreSummaryCount(originPremiseId, storeHlPId, 2, noOfOrders, 1, 0);
        tripClient_qa.getListOfForwardUnattemptedOrders(storeHlPId, ShipmentType.DL);
        lastmileHelper.validateStoreMLEntryFailedDeliveredb4CloseTrip(originPremiseId, storeHlPId, storeTenantId, forwardTrackingNumbersList.get(1));
        lastmileHelper.validStoreSummaryAmtToBeCollected(originPremiseId, storeHlPId, trackingShipmentValue.get(forwardTrackingNumbersList.get(0)));

        //Validate Store ML shipment is Failed Delivered
        mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(forwardTrackingNumbersList.get(1), storeTenantId);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

        //Validate INstakart ML shipment is Delivered
        mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(forwardTrackingNumbersList.get(1), LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

        //VAlidate the ORderToShip (Platform ) status is FAILED_DELIVERY

        orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(forwardTrackingNumbersList.get(1)),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.FAILED_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.FAILED_DELIVERY.name());

        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());

//Close all the store  trip

        tripClient_qa.closeStoreTrip(storeTripId, tripOrderIdAttemptReasonMap, storeTenantId);
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);

        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + forwardTrackingNumbersList.get(1) + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");

        //Close the trips

        //Get Reverse pendency
        //Create Reverse Bag

        //Receive FD
        //For cash validate 1099
    }




}