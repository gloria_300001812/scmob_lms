package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_StoreFlowsDP;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import com.myntra.logistics.platform.domain.TryAndBuyItemStatus;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class StoreFlow_ReconcileTest {
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LastmileClient lastmileClient = new LastmileClient();
    private TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa=new MLShipmentClientV2_QA();

    private LastmileHelper lastmileHelper;
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    private LMS_ReturnHelper lms_returnHelper=new LMS_ReturnHelper();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    @Test(priority = 2,enabled = true, dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows", description = "TC ID:C24713 - If there are pending shipments, next day we should not be able to add store bag for Forward, then create a reverse bag and pick them up and then try to reassign these pending shipments again to the same store and then again mark as FP and Unattempted,  next day we should not be able to add store bag and then again create a reverse bag and pick them up and then try to reassign these pending shipments again to the SAME store and mark then DL")
    public void process_AssignForwardOrderToStoreWithoutReconcile_DeliverForwardOrderThroughSametore_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                                                                                String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        String searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated),"Store is not created");
        //get Store required values
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId = deliveryStaffEntry.getId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        String storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);
        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId, ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        String forwardTrackingNumber = lmsHelper.getTrackingNumber(packetId1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //Create MasterBag
        ShipmentResponse shipmentResponse01 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = shipmentResponse01.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, forwardTrackingNumber, null, null, null, null, null, null, false).build();
        String response1 = storeHelper.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        Assert.assertTrue(response1.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse2 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry = tripResponse2.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        TripResponse tripResponse3=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId));
        storeValidator.validateTripStatus(tripResponse3, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId), ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
         statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //receive the failed delivery order in DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //complete reverse trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse2=storeHelper.closeMyntraTripWithStoreBag(tenantId,reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip),"Reverse trip is not completed");
        //validate trip status
        TripResponse tripResponse02 = lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId));
        storeValidator.validateTripStatus(tripResponse02, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED);

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = lmsServiceHelper.requeueOrder(packetId);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //Create MasterBag
        ShipmentResponse shipmentResponse6 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId2 = shipmentResponse6.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId2));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, ShipmentStatus.NEW.toString());
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo2 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId2), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response2 = masterBagClient_qa.addShipmentToStoreBag(masterBagId2, shipmentUpdateInfo2);
        if (response2.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId2, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse6 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Assert.assertTrue(tripResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse6.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse6.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse6, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse3 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId1), String.valueOf(masterBagId2),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse3.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus3 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId2);
        Assert.assertTrue(shipmentOrderMapStatus3.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse7 = tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId1,EnumSCM.OUT_FOR_DELIVERY);

        //Deliver the MB
        List<String> listTrackingNumbers1 = new ArrayList();
        listTrackingNumbers1.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse4 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId2), listTrackingNumbers1, String.valueOf(tripId1), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse4.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId2)));
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2,ShipmentStatus.DELIVERED.toString());
        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse02 = tripClient_qa.closeMyntraTripWithStoreBag(tenantId, tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse02.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        TripResponse tripResponse7 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers1, storeSDAId);
        Assert.assertTrue(tripResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId1 = tripResponse7.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId1.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId1 = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId1));
        MLShipmentResponse mlShipmentResponse19 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId1 = String.valueOf(mlShipmentResponse19.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId1);
        TripOrderAssignmentResponse tripOrderAssignmentResponse021 = tripClient_qa.failedDeliverStoreOrders(storeTripId1, mlShipmentEntriesId1, tripOrderAssignmentId1, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse021.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create MasterBag
        ShipmentResponse shipmentResponse8 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId3 = shipmentResponse8.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId3));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId3, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo3 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId3), ShipmentType.DL, forwardTrackingNumber, null, null, null, null, null, null, false).build();
        String response4 = storeHelper.addShipmentToStoreBag(masterBagId3, shipmentUpdateInfo3);
        Assert.assertTrue(response4.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //Create Reverse Trip
        Long dcSDAId1 = null;
        DeliveryStaffResponse deliveryStaffResponse1 = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry1 = deliveryStaffResponse1.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry1.size() == 0)) {
            dcSDAId1 = deliveryStaffResponse1.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId1));

        TripResponse tripResponse5 = tripClient_qa.createTrip(originPremiseId, dcSDAId1);
        Assert.assertTrue(tripResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<com.myntra.lastmile.client.entry.TripEntry> lstTripEntry1 = tripResponse5.getTrips();
        Long reverseTripId1 = null;
        if (!(lstTripEntry1.size() == 0)) {
            reverseTripId1 = lstTripEntry1.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId1));
        TripResponse tripResponse8=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId1));
        storeValidator.validateTripStatus(tripResponse8, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //create reverse MB
        String reverseShipmentId1 = tripClient_qa.assignReverseBagToTrip(reverseTripId1, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId1));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId1), ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = tripClient_qa.startTrip(reverseTripId1);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber1 = new ArrayList<>();
        lstreturnTrackingNumber1.add(trackingNo);
        TripShipmentAssociationResponse tripShipmentAssociationResponse5 = storeHelper.pickupReverseBagFromStore(reverseShipmentId1, lstreturnTrackingNumber1, String.valueOf(reverseTripId1), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //receive the failed delivery order in DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //complete reverse trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse21=storeHelper.closeMyntraTripWithStoreBag(tenantId,reverseTripId1);
        Assert.assertTrue(tripOrderAssignmentResponse21.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip),"Reverse trip is not completed");
        //validate trip status
        TripResponse tripResponse03 = lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId1));
        storeValidator.validateTripStatus(tripResponse03, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED);

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = lmsServiceHelper.requeueOrder(packetId);
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //Create MasterBag
        ShipmentResponse shipmentResponse11 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId4 = shipmentResponse11.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId4));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo4 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId4), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response5 = masterBagClient_qa.addShipmentToStoreBag(masterBagId4, shipmentUpdateInfo4);
        if (response5.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus4 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId4, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus4.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID2 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse11 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID2));
        Assert.assertTrue(tripResponse11.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId2 = tripResponse11.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId2));
        Assert.assertTrue(tripResponse11.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse11, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse6 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId2), String.valueOf(masterBagId4),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse6.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus6 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId4);
        Assert.assertTrue(shipmentOrderMapStatus6.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse8 = tripClient_qa.startTrip(tripId2);
        Assert.assertTrue(tripOrderAssignmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId2,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers2 = new ArrayList();
        listTrackingNumbers2.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse8 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId4), listTrackingNumbers2, String.valueOf(tripId2), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse8.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId4)));
        Assert.assertTrue(tripShipmentAssociationResponse8.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse8.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4, ShipmentStatus.DELIVERED.toString());
        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId2);
        statusPollingValidator.validateTripStatus(tripId2,EnumSCM.COMPLETED);

        //Assign order to store SDA
        TripResponse tripResponse12 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers2, storeSDAId);
        Assert.assertTrue(tripResponse12.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successful");
        //Validate Myntra SDA trip is OFD
        Long storeTripId2 = tripResponse12.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId2.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId2, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment2 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId3 = tripOrderAssignment2.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId3));
        MLShipmentResponse mlShipmentResponse45 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId2 = String.valueOf(mlShipmentResponse45.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId2);
        TripOrderAssignmentResponse tripOrderAssignmentResponse9 = tripClient_qa.deliverStoreOrders(storeTripId2, mlShipmentEntriesId2, tripOrderAssignmentId3, storeTenantId,"CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse9.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.DELIVERED.toString());
        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(storeTenantId,storeTripId2);
        statusPollingValidator.validateTripStatus(storeTripId2,EnumSCM.COMPLETED);
    }

    @Test(priority =3,enabled = true,  dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows", description = "If there are pending shipments, next day we should not be able to add store bag for Tnb , then create a reverse bag and pick them up and then try to reassign these pending shipments again to the same store and then again mark as FP and Unattempted,  next day we should not be able to add store bag and then again create a reverse bag and pick them up and then try to reassign these pending shipments again to the SAME store and mark then DL")
    public void process_AssignTnBOrderToStoreWithoutReconcile_DeliverTnBOrderThroughSameStore_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                                                                         String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        String searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated),"Store is not created");
        //get Store required values
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId = deliveryStaffEntry.getId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        String storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);
        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);

        //Assign order to store SDA
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
           Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create MasterBag
        ShipmentResponse shipmentResponse01 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId1 = shipmentResponse01.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String response1 = storeHelper.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        Assert.assertTrue(response1.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //receive the failed delivery order in DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc.contains(ResponseMessageConstants.success1), "Order is not received in DC");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        MLShipmentResponse mlShipmentResponse11 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse11,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        MLShipmentResponse mlShipmentResponse12 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, storeTenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse12,MLDeliveryShipmentStatus.RTO_CONFIRMED.toString());
        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(storeTenantId,storeTripId);
        statusPollingValidator.validateTripStatus(storeTripId,EnumSCM.COMPLETED);
        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = lmsServiceHelper.requeueOrder(packetId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //create mastre bag
        ShipmentResponse shipmentResponse5 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");

        Long masterBagId2 = shipmentResponse5.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId2));
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo2 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId2), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response2 = masterBagClient_qa.addShipmentToStoreBag(masterBagId2, shipmentUpdateInfo2);
        if (response2.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId2, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse10 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Long tripId1 = tripResponse10.getTrips().get(0).getId();
        Assert.assertTrue(tripResponse10.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId1), String.valueOf(masterBagId2),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Store bag is not added to a trip");
        Assert.assertTrue(tripShipmentAssociationResponse2.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId2)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId2);
        Assert.assertTrue(tripShipmentAssociationResponse2.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId1)), " The trip Id is not matching ");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2,ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, sipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus1 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId2);
        Assert.assertTrue(shipmentOrderMapStatus1.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Deliver the MB
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse5 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId2), listTrackingNumbers, String.valueOf(tripId1), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse5.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId2)));
        Assert.assertTrue(tripShipmentAssociationResponse5.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse5.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,EnumSCM.EXPECTED_IN_DC);
        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId1);
        statusPollingValidator.validateTripStatus(tripId1,EnumSCM.COMPLETED);
        //Assign order to store SDA
        TripResponse tripResponse3 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        //Validate after creating of Store Bag it becomes OFD
        Long storeTripId1 = tripResponse3.getTrips().get(0).getId();
        statusPollingValidator.validateTripStatusWithVNM(storeTripId1,com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        MLShipmentResponse mlShipmentResponse25 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse25,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        Long dcId = mlShipmentResponse25.getMlShipmentEntries().get(0).getDeliveryCenterId();
        storeValidator.isNull(dcId.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId1 = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId1));

        MLShipmentResponse mlShipmentResponse17 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId1 = String.valueOf(mlShipmentResponse17.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId1);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.failedDeliverStoreOrders(storeTripId1, mlShipmentEntriesId1, tripOrderAssignmentId1, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create MasterBag
        ShipmentResponse shipmentResponse03 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse03.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId3 = shipmentResponse03.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId3));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId3, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo3 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId3), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String response3 = storeHelper.addShipmentToStoreBag(masterBagId3, shipmentUpdateInfo3);
        Assert.assertTrue(response3.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //receive the failed delivery order in DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = lmsServiceHelper.requeueOrder(packetId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //create MB
        ShipmentResponse shipmentResponse7 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");

        Long masterBagId4 = shipmentResponse7.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId4));
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo4 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId4), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response4 = masterBagClient_qa.addShipmentToStoreBag(masterBagId4, shipmentUpdateInfo4);
        if (response4.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus4 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId4, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus4.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID2 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse11 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID2));
        Long tripId2 = tripResponse11.getTrips().get(0).getId();
        Assert.assertTrue(tripResponse11.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse3 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId2), String.valueOf(masterBagId4),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse3.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Store bag is not added to a trip");
        Assert.assertTrue(tripShipmentAssociationResponse3.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId4)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId4);
        Assert.assertTrue(tripShipmentAssociationResponse3.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId2)), " The trip Id is not matching ");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4,ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, sipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus2 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId4);
        Assert.assertTrue(shipmentOrderMapStatus2.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = tripClient_qa.startTrip(tripId2);
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Deliver the MB
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse7 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId4), listTrackingNumbers, String.valueOf(tripId2), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse7.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId4)));
        Assert.assertTrue(tripShipmentAssociationResponse7.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse7.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4,ShipmentStatus.DELIVERED.toString());

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse4 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate after creating of Store Bag it becomes OFD
        Long storeTripId2 = tripResponse4.getTrips().get(0).getId();
        statusPollingValidator.validateTripStatusWithVNM(storeTripId2,com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        MLShipmentResponse mlShipmentResponse35 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse35,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        Long dcId1 = mlShipmentResponse35.getMlShipmentEntries().get(0).getDeliveryCenterId();
        storeValidator.isNull(dcId1.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //update the trip with status as Tried_And_Bought
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment2 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId2 = tripOrderAssignment2.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId2));
        OrderResponse orderResponse8 = mlShipmentClientV2_qa.getTryAndBuyItems(storeTenantId, trackingNo);
        Long itemEntriesId1 = orderResponse8.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        storeValidator.isNull(String.valueOf(itemEntriesId1));
        TripOrderAssignmentResponse tripOrderAssignmentResponse7 = storeHelper.updateTryAndBuyOrders(tripOrderAssignmentId2, AttemptReasonCode.DELIVERED, com.myntra.lastmile.client.code.utils.TripAction.UPDATE, packetId, dcId1, amount,
                itemEntriesId1, storeTenantId, TryAndBuyItemStatus.TRIED_AND_BOUGHT, storeTripId2);
        Assert.assertEquals(tripOrderAssignmentResponse7.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.DELIVERED.toString());

    }

    @Test( priority = 4,enabled = true, dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows2", description = "If there are pending shipments, next day we should not be able to add store bag for Tnb, then create a reverse bag and pick them up and then try to reassign these pending shipments again to the same store and then again mark as FP and Unattempted,  next day we should not be able to add store bag and then again create a reverse bag and pick them up and then try to reassign these pending shipments again to the DIFFERENT store and mark then DL.")
    public void process_AssignTnBOrderToStoreWithoutReconcile_DeliverTnBOrderThroughDIFFERENTStore_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                                                                              String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code,
                                                                                                              String name1, String ownerFirstName1, String ownerLastName1,String mobileNumber1,String code1) throws Exception {

        String searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated),"Store is not created");
        //get Store required values
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId = deliveryStaffEntry.getId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        String storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded),"Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(trackingNo);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,storeTenantId,MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());

        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);

        //Assign order to store SDA
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId));

        MLShipmentResponse mlShipmentResponse8 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId = String.valueOf(mlShipmentResponse8.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.failedDeliverStoreOrders(storeTripId, mlShipmentEntriesId, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till SH
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", true, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create MasterBag
        ShipmentResponse shipmentResponse01 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId1 = shipmentResponse01.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String response1 = storeHelper.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        Assert.assertTrue(response1.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //receive the failed delivery order in DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc.contains(ResponseMessageConstants.success1), "Order is not received in DC");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = lmsServiceHelper.requeueOrder(packetId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //validate OrderToShip status
       Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //create mastre bag
        ShipmentResponse shipmentResponse5 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");

        Long masterBagId2 = shipmentResponse5.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId2));
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo2 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId2), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response2 = masterBagClient_qa.addShipmentToStoreBag(masterBagId2, shipmentUpdateInfo2);
        if (response2.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId2, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse10 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Long tripId1 = tripResponse10.getTrips().get(0).getId();
        Assert.assertTrue(tripResponse10.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId1), String.valueOf(masterBagId2),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Store bag is not added to a trip");
        Assert.assertTrue(tripShipmentAssociationResponse2.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId2)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId2);
        Assert.assertTrue(tripShipmentAssociationResponse2.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId1)), " The trip Id is not matching ");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, sipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus1 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId2);
        Assert.assertTrue(shipmentOrderMapStatus1.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Validate trip status
        statusPollingValidator.validateTripStatus(tripId1,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse5 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId2), listTrackingNumbers, String.valueOf(tripId1), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse5.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId2)));
        Assert.assertTrue(tripShipmentAssociationResponse5.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse5.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,com.myntra.constants.EnumSCM.EXPECTED_IN_DC);

        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId1);
        statusPollingValidator.validateTripStatus(tripId1,EnumSCM.COMPLETED);

        //Assign order to store SDA
        TripResponse tripResponse3 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        //Validate after creating of Store Bag it becomes OFD
        Long storeTripId1 = tripResponse3.getTrips().get(0).getId();
        statusPollingValidator.validateTripStatusWithVNM(storeTripId1,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        MLShipmentResponse mlShipmentResponse25 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse25,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        Long dcId = mlShipmentResponse25.getMlShipmentEntries().get(0).getDeliveryCenterId();
        storeValidator.isNull(dcId.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //Mark it as Failed Delivered
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId1 = tripOrderAssignment1.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId1));

        MLShipmentResponse mlShipmentResponse17 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        String mlShipmentEntriesId1 = String.valueOf(mlShipmentResponse17.getMlShipmentEntries().get(0).getId());
        storeValidator.isNull(mlShipmentEntriesId1);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.failedDeliverStoreOrders(storeTripId1, mlShipmentEntriesId1, tripOrderAssignmentId1, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse3.getStatus().getStatusMessage(), ResponseMessageConstants.success, "Updating Order status response message is not matching");

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.FAILED_DELIVERY.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.FAILED_DELIVERY.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create MasterBag
        ShipmentResponse shipmentResponse03 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse03.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");
        Long masterBagId3 = shipmentResponse03.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId3));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId3, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo3 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId3), ShipmentType.DL, trackingNo1, null, null, null, null, null, null, false).build();
        String response3 = storeHelper.addShipmentToStoreBag(masterBagId3, shipmentUpdateInfo3);
        Assert.assertTrue(response3.contains("Reason: Store "+storeCourierCode+" has shipments that are not reconciled."));

        //receive the failed delivery order in DC
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(trackingNo.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");

        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.RECEIVED_IN_DC.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //reque the FD order
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = lmsServiceHelper.requeueOrder(packetId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

         Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId,ShipmentUpdateEvent.RTO_CONFIRMED.toString());

        //TODO assign the TnB order to different store
        String searchParams1 = "code.like:" + code1;
        //Create Store
        StoreResponse storeResponse1 = lastmileHelper.createStore(name1, ownerFirstName1, ownerLastName1, latLong, emailId, mobileNumber1, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code1);
        //validate Store created successfully
        Assert.assertTrue(storeResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        Long storeHlPId1 = storeResponse1.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry1 = storeResponse1.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId1 = deliveryStaffEntry1.getId();
        String storeTenantId1 = storeResponse1.getStoreEntries().get(0).getTenantId();

        //Create MasterBag
        String originDCCity1 = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse3 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams1, "");
        String destinationStoreCity1 = storeResponse3.getStoreEntries().get(0).getCity();
        //create mastre bag
        ShipmentResponse shipmentResponse7 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId1, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity1, destinationStoreCity1);
        Assert.assertTrue(shipmentResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment),"Status message is not matching");

        Long masterBagId4 = shipmentResponse7.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId4));
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo4 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId4), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response4 = masterBagClient_qa.addShipmentToStoreBag(masterBagId4, shipmentUpdateInfo4);
        if (response4.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus4 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId4, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus4.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID2 = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse11 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID2));
        Long tripId2 = tripResponse11.getTrips().get(0).getId();
        Assert.assertTrue(tripResponse11.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse3 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId2), String.valueOf(masterBagId4),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse3.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded),"Store bag is not added to a trip");
        Assert.assertTrue(tripShipmentAssociationResponse3.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId4)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId4);
        Assert.assertTrue(tripShipmentAssociationResponse3.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId2)), " The trip Id is not matching ");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4,ShipmentStatus.ADDED_TO_TRIP.toString());
        // to check after adding bag to trip, sipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus2 = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId4);
        Assert.assertTrue(shipmentOrderMapStatus2.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse6 = tripClient_qa.startTrip(tripId2);
        Assert.assertTrue(tripOrderAssignmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId2,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse7 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId4), listTrackingNumbers, String.valueOf(tripId2), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse7.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId4)));
        Assert.assertTrue(tripShipmentAssociationResponse7.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse7.getStatus().getTotalCount() == 1);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId4,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId1,EnumSCM.EXPECTED_IN_DC);
        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId2);
        statusPollingValidator.validateTripStatus(tripId2,EnumSCM.COMPLETED);

        //Assign order to store SDA
        TripResponse tripResponse4 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId1);
        Assert.assertTrue(tripResponse4.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        Long storeTripId2 = tripResponse4.getTrips().get(0).getId();

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);

        //Validate ML shipment status in DC
        MLShipmentResponse mlShipmentResponse35 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNo, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse35,MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString());
        Long dcId1 = mlShipmentResponse35.getMlShipmentEntries().get(0).getDeliveryCenterId();
        storeValidator.isNull(dcId1.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId1,ShipmentUpdateEvent.OUT_FOR_DELIVERY.toString());

        //update the trip with status as Tried_And_Bought
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();
        //Get the tripOrderId for Store trip
        TripOrderAssignmentResponse tripOrderAssignment2 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber1, storeTenantId1);
        Long tripOrderAssignmentId2 = tripOrderAssignment2.getTripOrders().get(0).getId();
        storeValidator.isNull(String.valueOf(tripOrderAssignmentId2));
        OrderResponse orderResponse8 = mlShipmentClientV2_qa.getTryAndBuyItems(storeTenantId1, trackingNo);
        Long itemEntriesId1 = orderResponse8.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        storeValidator.isNull(String.valueOf(itemEntriesId1));
        TripOrderAssignmentResponse tripOrderAssignmentResponse7 = storeHelper.updateTryAndBuyOrders(tripOrderAssignmentId2, AttemptReasonCode.DELIVERED, com.myntra.lastmile.client.code.utils.TripAction.UPDATE, packetId, dcId1, amount,
                itemEntriesId1, storeTenantId1, TryAndBuyItemStatus.TRIED_AND_BOUGHT, storeTripId2);
        Assert.assertEquals(tripOrderAssignmentResponse7.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, tenantId,MLDeliveryShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, storeTenantId1,ShipmentUpdateEvent.DELIVERED.toString());

    }

    @Test(  priority = 5,enabled = true,dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows",description = "Create return and reverse bag  and pick the return and receive it and DON'T close the trip and try creating a store bag and add some orders to the bag (should work) ,")
    public void process_AssignReturOrderToStore_MakeStatusAsPS_ReceivingInDC_TryToAssignOneMoreOrderToStore_WithCompletingTrip_Successful(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                                                                                                          String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        String searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created successfully");

        //get Store required values
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        Long storeSDAId = deliveryStaffEntry.getId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        String storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

        //Create Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        storeValidator.isNull(packetId1);
        //validate OrderToShip status
        OrderResponse orderResponse01 = lmsClient.getOrderByOrderId(packetId1);
        Assert.assertTrue(orderResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse01, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse01.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId, MLDeliveryShipmentStatus.UNASSIGNED.toString());

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        Long storetripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storetripId.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId, ShipmentUpdateEvent.OUT_FOR_PICKUP.toString());
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.OUT_FOR_PICKUP.toString());

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successfull in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE,tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKED_UP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.PICKUP_SUCCESSFUL.toString());

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse1 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry = tripResponse1.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        TripResponse tripResponse2=lastmileClient.getTripDetailsByTripId(String.valueOf(reverseTripId));
        storeValidator.validateTripStatus(tripResponse2, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseShipmentId), ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKED_UP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.PICKUP_SUCCESSFUL.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(returnTrackingNumber);
        TripShipmentAssociationResponse tripShipmentAssociationResponse = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.PICKED_UP);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.PICKUP_SUCCESSFUL.toString());

        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(returnTrackingNumber.substring(2, returnTrackingNumber.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, tenantId,EnumSCM.RECEIVED_IN_DC);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber, storeTenantId,ShipmentUpdateEvent.PICKUP_SUCCESSFUL.toString());

        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDate(storeCourierCode);

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);


        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse8 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse8.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId3 = shipmentResponse8.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId3));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId3, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo3 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId3), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        String response4 = storeHelper.addShipmentToStoreBag(masterBagId3, shipmentUpdateInfo3);
        Assert.assertTrue(response4.contains(ResponseMessageConstants.assignOrderToStoreBag),"Order is not assigned to store when there is no pending orders ");

    }
}
