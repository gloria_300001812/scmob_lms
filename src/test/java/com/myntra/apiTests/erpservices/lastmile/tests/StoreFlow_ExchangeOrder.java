package com.myntra.apiTests.erpservices.lastmile.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.CommonUtils;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Helper.HubToTransportHubConfigResponse;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.tms.container.ContainerEntry;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class StoreFlow_ExchangeOrder {
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    private LMS_CreateOrder lms_createOrder=new LMS_CreateOrder();
    TripClient_QA tripClient_qa=new TripClient_QA();
    private LastmileHelper lastmileHelper;
    private MasterBagClient masterBagClient;
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MasterBagServiceHelper masterBagServiceHelper=new MasterBagServiceHelper();
    TMSServiceHelper tmsServiceHelper=new TMSServiceHelper();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    static String searchParams;
    static Long storeHlPId;
    static Long storeSDAId;
    static String storeTenantId;
    static String tenantId;
    static String pincode;
    static String mobileNumber;
    static String storeCourierCode;
    static String code;

    @BeforeTest
    public void createStore(){
        Random r = new Random(System.currentTimeMillis());
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        String value = CommonUtils.generateString(3);

        String name=value+ String.valueOf(contactNumber1).substring(5,9);
        String ownerFirstName= value + String.valueOf(contactNumber1).substring(5,9);
        String ownerLastName=value + String.valueOf(contactNumber1).substring(5,9);
        code=value + String.valueOf(contactNumber1).substring(5,9);
        String latLong="LA_latlong" + String.valueOf(contactNumber1).substring(5,9);
        String emailId="test@lastmileAutomation.com";
        mobileNumber=String.valueOf(contactNumber1);
        String  address="Automation Address";
        pincode= LASTMILE_CONSTANTS.STORE_PINCODE;
        String city="Bangalore";
        String state="Karnataka";
        String mappedDcCode= LASTMILE_CONSTANTS.ROLLING_DC_CODE;
        String gstin="LA_gstin" + String.valueOf(contactNumber1).substring(5,9);
        tenantId=LMS_CONSTANTS.TENANTID;

        searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId = deliveryStaffEntry.getId();
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeCourierCode=storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

    }

    @AfterMethod
    public void modifiedMLLastmilePartnerShipmentDate(){
        //modify the ML lastmile partner last_modified column
        storeHelper.modifiedMLLastmilePartnerShipmentDateAsCurrentDate(storeCourierCode);
    }

    @Test( priority = 1, enabled = true, description = "TC ID: C24676, Shortage for exchange orders ")
    public void process_ShortageForExchangeOrders_Successful() throws Exception {

        //Create Mock order Till DL for first ex order
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse001 = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse001.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse001, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse001.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Mock order Till DL for 2nd ex order
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //create exchange order
        String exchangeOrderId1 = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID1));
        String exchangePacketId1 = omsServiceHelper.getPacketId(exchangeOrderId1);
        String exchangeTrackingNumber1 = lmsHelper.getTrackingNumber(exchangePacketId1);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status
        MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(exchangeTrackingNumber, tenantId);
        storeValidator.validateMLShipmentStatus(mlShipmentResponse, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, exchangeTrackingNumber1, null, null, null, null, null, null, false).build();
        String response1 = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        if (response1.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber1, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, exchangeTrackingNumber1);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());


        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate the MB status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, storeTenantId,EnumSCM.EXPECTED_IN_DC);

        ShipmentResponse shipmentResponse4 = masterBagClient.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse4.getStatus().getStatusMessage(), ResponseMessageConstants.retrievedShipment, "Not able to retrive Master bag details");
        storeValidator.validateMasterBagStatus(shipmentResponse4, ShipmentStatus.DELIVERED);
        List<OrderShipmentAssociationEntry> shipmentEntries=shipmentResponse4.getEntries().get(0).getOrderShipmentAssociationEntries();
        for(int i=0;i<shipmentEntries.size();i++){
            if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),ShipmentStatus.DELIVERED.toString(),"The tracking number status is not expected, TrackingNUmber- "+exchangeTrackingNumber+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }else if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber1)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),EnumSCM.SHORTAGE,"The tracking number status is not expected, TrackingNUmber- "+exchangeTrackingNumber1+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }
        }

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber1, tenantId,EnumSCM.FAILED_TO_HANDOVER_TO_LAST_MILE_PARTNER.toString());

    }

    @Test( priority = 2, enabled = true,  description = "TC ID:C24677, Excess for exchange orders ")
    public void process_ExcessForExchangeOrders_Successful() throws Exception {

        //Create Mock order Till DL for first ex order
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse001 = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse001.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse001, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse001.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Mock order Till DL for 2nd ex order
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId1, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);

        //create exchange order
        String exchangeOrderId1 = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID1));
        String exchangePacketId1 = omsServiceHelper.getPacketId(exchangeOrderId1);
        String exchangeTrackingNumber1 = lmsHelper.getTrackingNumber(exchangePacketId1);

        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId1, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);

        //get order tracking number
        OrderResponse lmsOrderDetails1 = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId1,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo1 = lmsOrderDetails1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not " + tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber, tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(exchangeTrackingNumber);
        listTrackingNumbers.add(exchangeTrackingNumber1);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        ShipmentResponse shipmentResponse4 = masterBagClient.getShipmentOrderMap(masterBagId);
        List<OrderShipmentAssociationEntry> shipmentEntries=shipmentResponse4.getEntries().get(0).getOrderShipmentAssociationEntries();
        for(int i=0;i<shipmentEntries.size();i++){
            if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),ShipmentStatus.DELIVERED.toString(),"The tracking number status is not expected, TrackingNUmber- "+exchangeTrackingNumber+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }else if(shipmentEntries.get(i).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber1)){
                Assert.assertEquals(shipmentEntries.get(i).getStatus().toString(),"EXCESS","The tracking number status is not expected, TrackingNUmber- "+exchangeTrackingNumber1+" and status is - "+shipmentEntries.get(i).getStatus().toString());
            }
        }

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber1, tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        //Validate ML shipment status in store
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber1, storeTenantId,EnumSCM.EXPECTED_IN_DC);

    }

    /* TODO:-------------- Exchange Flow ----------- */

    @Test( priority = 3, enabled = true,description = "TC ID:C24678, Create a Store , create and assign order to store,make order status as Delivered ,Create exchange order and process it successfully")
    public void processExchangeOrdersDeliveredSuccesssful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId,
                EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL,
                "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse001 = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse001.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse001,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse001.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());
        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //validate trip status
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.OUT_FOR_DELIVERY);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //complete store trip
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");

        //Validate Myntra SDA trip is OFD
         Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId,com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //update the trip order status as deliveres and get the pickup exchange order details
        String pickupTrackingNumber = null;
        String sourceReturnId=null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(storeTripId, storeTenantId);
        if (pickupTripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getId();
            String exchangeOrderId1 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getExchangeOrderId();


            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, exchangeOrderId1, storeTripId, orderEntry,storeTenantId,Long.valueOf(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            pickupTrackingNumber=tripOrderAssignmentResponse0.getTripOrders().get(0).getTrackingNumber();
            sourceReturnId=tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(storeTripId,storeTenantId,com.myntra.lastmile.client.status.TripOrderStatus.PS.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId, ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId,tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        //validate pickup shipment staus
        statusPollingValidator.validatePickupShipmentStatus(sourceReturnId,LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),EnumSCM.PICKUP_SUCCESSFUL);

        //CreateMB from DC To WH
        String deliveryCenterName="ELC";
        String returnHub= "RT-BLR";
        MasterbagDomain shipmentResponse7 = masterBagServiceHelper.createMasterBag(deliveryCenterName,returnHub, ShippingMethod.NORMAL,"ML",tenantId);
        Long reverseMBId=shipmentResponse7.getId();
        storeValidator.isNull(String.valueOf(reverseMBId));
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.NEW.toString());
        //Add order to MB
        masterBagServiceHelper.addShipmentsToMasterBag(reverseMBId,pickupTrackingNumber,ShipmentType.PU,tenantId);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.NEW.toString());
        //close Reverse MB
        masterBagServiceHelper.closeMasterBag(reverseMBId,tenantId);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());

        //Receive MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(deliveryCenterName, reverseMBId, tenantId);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse=storeHelper.getMappedTransportHubDetails(tenantId,deliveryCenterName);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub=hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterName,transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(deliveryCenterName, transporterHub, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, reverseMBId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());
        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());

        //Receive Container In Transport Hub
        ContainerResponse containerResponse3 = (ContainerResponse) storeHelper.receiveContainerInTransportHub.apply(containerId, transporterHub);
        Assert.assertTrue(containerResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Container is not received in TRansport hub");

        //MasterBag inscan
        String mbInscanResponse = null;
        storeHelper.masterBagInscanProcess(pickupTrackingNumber, reverseMBId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);

        //TODO calling this API twice, since it fails 1st time.
        mbInscanResponse = storeHelper.masterBagInscanProcess(pickupTrackingNumber, reverseMBId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
        Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success)," Master bag inscan failed in WH");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId, ReturnStatus.RRC.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId,tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.DELIVERED_TO_SELLER.toString());

    }

    @Test( priority = 4, enabled = true, description = "TC ID: C24679, Create a Store , create and assign order to store,make order status as Delivered ,Create exchange order and make the exchange status as Failed_Delivered , check the pickup order status as Failed_pickup")
    public void processExchangeOrdersFailedDeliveredSuccesssful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse01 = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse01,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse01.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId,com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //update the trip order status as failed deliveries and get the pickup exchange order details
        String pickupTrackingNumber = null;
        String sourceReturnId=null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(storeTripId, storeTenantId);
        if (pickupTripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getId();
            String exchangeOrderId1 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getExchangeOrderId();


            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId, EnumSCM.FAILED, EnumSCM.UPDATE, exchangeOrderId1, storeTripId, orderEntry,storeTenantId,Long.valueOf(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            pickupTrackingNumber=tripOrderAssignmentResponse0.getTripOrders().get(0).getTrackingNumber();
            sourceReturnId=tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(storeTripId,storeTenantId,com.myntra.lastmile.client.status.TripOrderStatus.FP.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId,com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId,ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId,tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());
    }

    @Test( priority = 5, enabled = true,  description = "TC ID:C24680 , Create a Store , create and assign order to store,make order status as Delivered ,Create exchange order and make the exchange order status as Failed_Delivered , reque it in DC and make the exchange order status as Delivered and check the Pickup order status as Pickup_Successful")
    public void processExchangeOrdersFailedDeliveredAndRequeSuccesssful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //create exchange order
        String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        //validate OrderToShip status
        OrderResponse orderResponse01 = lmsClient.getOrderByOrderId(exchangePacketId);
        Assert.assertTrue(orderResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse01,com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        Long originPremiseId = orderResponse01.getOrders().get(0).getDeliveryCenterId();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create MasterBag
        String originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        String destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();

        ShipmentResponse shipmentResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        if (response.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId));
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        Assert.assertTrue(tripOrderAssignmentResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Deliver the MB
        List<String> listTrackingNumbers = new ArrayList();
        listTrackingNumbers.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), listTrackingNumbers, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse1.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(masterBagId,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse1 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");
        Long storeTripId = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId,com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //update the trip order status as deliveres and get the pickup exchange order details
        String pickupTrackingNumber = null;
        String sourceReturnId=null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(storeTripId, storeTenantId);
        if (pickupTripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getId();
            String exchangeOrderId1 = pickupTripOrderAssignmentResponse.getTripOrders().get(0).getExchangeOrderId();


            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId, EnumSCM.FAILED, EnumSCM.UPDATE, exchangeOrderId1, storeTripId, orderEntry,storeTenantId,Long.valueOf(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            pickupTrackingNumber=tripOrderAssignmentResponse0.getTripOrders().get(0).getTrackingNumber();
            sourceReturnId=tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(storeTripId,storeTenantId,com.myntra.lastmile.client.status.TripOrderStatus.FP.toString());

        //validate OrderToShip status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateOrderStatus(exchangePacketId,com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId,ReturnStatus.LPI.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId,tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED.toString());

        //Create Reverse Trip from Dc to store
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff), "Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse3 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry = tripResponse3.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId),ShipmentStatus.ADDED_TO_TRIP.toString());

        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatusWithVNM(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(exchangeTrackingNumber);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, com.myntra.lastmile.client.status.TripOrderStatus.PS, ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");
        // check trip shipment association table PS status
        TripShipmentAssociationResponse tripShipmentAssociationResponse03=storeHelper.findShipmentByTrip(reverseTripId,tenantId);
        Assert.assertTrue(tripShipmentAssociationResponse03.getTripShipmentAssociationEntryList().get(0).getStatus().toString().equalsIgnoreCase(com.myntra.lastmile.client.status.TripOrderStatus.PS.toString()));


        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(exchangeTrackingNumber);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,EnumSCM.FAILED_DELIVERY);

        //Complete DC trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse4 = storeHelper.closeMyntraTripWithStoreBag(tenantId, reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse4.getStatus().getStatusMessage().contains(ResponseMessageConstants.completeTrip));
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId, com.myntra.lastmile.client.code.utils.TripStatus.COMPLETED.toString());
        //Validate ML shipment status in DC
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,EnumSCM.RTO_CONFIRMED);

        //reque the FD order in DC
        TripOrderAssignmentResponse tripOrderAssignmentResponse2=tripClient_qa.autoAssignmentOfOrderToTrip(exchangePacketId,tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse2.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Reque of the FD order is not successfull");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.UNASSIGNED);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,EnumSCM.RTO_CONFIRMED);

        ShipmentResponse shipmentResponse6 = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        Assert.assertTrue(shipmentResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.createShipment), "Status message is not matching");
        Long masterBagId1 = shipmentResponse6.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.NEW.toString());

        //Add order to MB
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, exchangeTrackingNumber, null, null, null, null, null, null, false).build();
        String response1 = masterBagClient_qa.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        if (response1.toLowerCase().contains(ResponseMessageConstants.errorStatus)) {
            Assert.fail("Error while adding shipment to store bag");
        }
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus1 = lastmileHelper.validateOrderAddedInStoreBag(masterBagId1, exchangeTrackingNumber);
        Assert.assertEquals(orderShipmentAssociationStatus1.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());

        //Create Trip
        TripResponse tripResponse6 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Assert.assertTrue(tripResponse6.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip not added successfully");
        Long tripId1 = tripResponse6.getTrips().get(0).getId();
        storeValidator.isNull(String.valueOf(tripId1));
        Assert.assertTrue(tripResponse6.getTrips().get(0).getTenantId().equals(tenantId), " The trip tenantId is not "+tenantId);
        storeValidator.validateTripStatus(tripResponse6, com.myntra.lastmile.client.code.utils.TripStatus.CREATED);

        //Add the storeBag to this trip
        TripShipmentAssociationResponse tripShipmentAssociationResponse3 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId1), String.valueOf(masterBagId1),LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse3.getStatus().getStatusMessage().contains(ResponseMessageConstants.shipmentAdded), "Shipments not added Successfully to trip");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);

        //Validate the MB status is ADDED_TO_TRIP
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.ADDED_TO_TRIP.toString());

        // to check after adding bag to trip, shipment status is ADDED_TO_TRIP, but shipmentOrderMap is NEW , Validate shipmentOrderMap status is NEW
        OrderShipmentAssociationStatus shipmentOrderMapStatus1 = lastmileHelper.getShipmentOrderMapStatus(exchangeTrackingNumber, masterBagId1);
        Assert.assertTrue(shipmentOrderMapStatus1.name().equals(OrderShipmentAssociationStatus.NEW.name()));

        //Start the trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse5= tripClient_qa.startTrip(tripId1);
        Assert.assertTrue(tripOrderAssignmentResponse5.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.startTrip), "Trip has not started successfully");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER);
        //Deliver the MB
        List<String> listTrackingNumbers1 = new ArrayList();
        listTrackingNumbers1.add(exchangeTrackingNumber);
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripShipmentAssociationResponse tripShipmentAssociationResponse4 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId1), listTrackingNumbers1, String.valueOf(tripId1), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tripShipmentAssociationResponse4.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId1)));
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getStatusType().name().equals(ResponseMessageConstants.success1), "Delivering MasterBag to store failed ");
        Assert.assertTrue(tripShipmentAssociationResponse4.getStatus().getTotalCount() == 1);
        //Validate the MB status is DELIVERED
        statusPollingValidator.validateShipmentBagStatus(masterBagId1,ShipmentStatus.DELIVERED.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,EnumSCM.EXPECTED_IN_DC);

        //Assign order to store SDA
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        TripResponse tripResponse7 = mlShipmentServiceV2Client_qa.createStoreTrip(listTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse7.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Assign order to store SDA ia not successfull");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        //Validate Myntra SDA trip is OFD
        Long storeTripId1 = tripResponse7.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId1.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId1,com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId,com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,tenantId,EnumSCM.OUT_FOR_DELIVERY);
        statusPollingValidator.validateML_ShipmentStatus(exchangeTrackingNumber,storeTenantId,EnumSCM.OUT_FOR_DELIVERY);

        //update the trip order status as deliveres and get the pickup exchange order details
        String pickupTrackingNumber1 = null;
        String sourceReturnId1=null;
        TripOrderAssignmentResponse pickupTripOrderAssignmentResponse04 = tripClient_qa.findExchangesByTrip(storeTripId1, storeTenantId);
        if (pickupTripOrderAssignmentResponse04.getTripOrders().get(0).getTrackingNumber().equalsIgnoreCase(exchangeTrackingNumber)) {
            Long pickupTripOrderAssignmentId1 = pickupTripOrderAssignmentResponse04.getTripOrders().get(0).getId();
            String exchangeOrderId1 = pickupTripOrderAssignmentResponse04.getTripOrders().get(0).getExchangeOrderId();


            OrderEntry orderEntry1 = new OrderEntry();
            orderEntry1.setOperationalTrackingId(exchangeTrackingNumber.substring(2, exchangeTrackingNumber.length()));
            TripOrderAssignmentResponse tripOrderAssignmentResponse0 = storeHelper.updateOrderInTrip(pickupTripOrderAssignmentId1, EnumSCM.DELIVERED, EnumSCM.UPDATE, exchangeOrderId1, storeTripId1, orderEntry1,storeTenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID));
            Assert.assertEquals(tripOrderAssignmentResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");

            pickupTrackingNumber1=tripOrderAssignmentResponse0.getTripOrders().get(0).getTrackingNumber();
            sourceReturnId1=tripOrderAssignmentResponse0.getTripOrders().get(0).getSourceReturnId();
        }

        //validate TripOrder status
        statusPollingValidator.validateTripOrderStatus(storeTripId1,storeTenantId,com.myntra.lastmile.client.status.TripOrderStatus.PS.toString());
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(exchangePacketId,com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId1,ReturnStatus.RL.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId1,tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.RETURN_SUCCESSFUL.toString());
        //CreateMB from DC To WH
        String deliveryCenterName="ELC";
        String returnHub= "RT-BLR";
        MasterbagDomain shipmentResponse7 = masterBagServiceHelper.createMasterBag(deliveryCenterName,returnHub, ShippingMethod.NORMAL,"ML",tenantId);
        Long reverseMBId=shipmentResponse7.getId();
        storeValidator.isNull(String.valueOf(reverseMBId));
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.NEW.toString());
        //Add order to MB
        masterBagServiceHelper.addShipmentsToMasterBag(reverseMBId,pickupTrackingNumber,ShipmentType.PU,tenantId);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.NEW.toString());
        //close Reverse MB
        masterBagServiceHelper.closeMasterBag(reverseMBId,tenantId);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());

        //Receive MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(deliveryCenterName, reverseMBId, tenantId);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse=storeHelper.getMappedTransportHubDetails(tenantId,deliveryCenterName);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub=hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterName,transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(deliveryCenterName, transporterHub, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, reverseMBId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.CLOSED.toString());
        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());

        //Receive Container In Transport Hub
        ContainerResponse containerResponse3 = (ContainerResponse) storeHelper.receiveContainerInTransportHub.apply(containerId, transporterHub);
        Assert.assertTrue(containerResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Container is not received in TRansport hub");

        //MasterBag inscan
        String mbInscanResponse = null;
        storeHelper.masterBagInscanProcess(pickupTrackingNumber, reverseMBId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);

        //TODO calling this API twice, since it fails 1st time.
        mbInscanResponse = storeHelper.masterBagInscanProcess(pickupTrackingNumber, reverseMBId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED,
                ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
        Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success)," Master bag inscan failed in WH");

        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId,ShipmentStatus.IN_TRANSIT.toString());
        //validate return status in rms
        statusPollingValidator.validateReturnStatusRMS(sourceReturnId, ReturnStatus.RRC.toString());
        //validate return status in LMS
        statusPollingValidator.validateReturnStatusLMS(sourceReturnId,tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID),com.myntra.lastmile.client.status.ShipmentStatus.DELIVERED_TO_SELLER.toString());
        //validate pickup shipment staus
        statusPollingValidator.validatePickupShipmentStatus(sourceReturnId,LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),EnumSCM.PICKUP_SUCCESSFUL);

    }

}
