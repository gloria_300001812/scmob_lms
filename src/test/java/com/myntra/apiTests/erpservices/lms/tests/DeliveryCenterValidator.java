/**
 * Created by Abhishek Jaiswal on 15/11/18.
 */

package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lastmile.client.response.TripResponse;
import org.testng.Assert;

public class DeliveryCenterValidator {
    DeliveryCenterResponse lmsTripResponse;

    public DeliveryCenterValidator(DeliveryCenterResponse lmsTripResponse) {
        this.lmsTripResponse = lmsTripResponse;
    }

    /*
     * BASE MODULAR FUNCTIONS START For Trip Response*/

    public void validateStatusType(StatusResponse.Type statusType)
    {
        Assert.assertEquals(lmsTripResponse.getStatus().getStatusType(), statusType, "Unexpected Status");
    }

    public void validateStatusCode(int statusCode)
    {
        Assert.assertEquals(lmsTripResponse.getStatus().getStatusCode(), statusCode, "Wrong status Code");
    }

    public void validateTotalCount(long totalCount)
    {
        Assert.assertEquals(lmsTripResponse.getStatus().getTotalCount(), totalCount, "Count differs");
    }

    public void validateDeliveryHubCode(String deliveryHubCode)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(0).getDeliveryHubCode(), deliveryHubCode, "Wrong Delivery Hub Code");
    }
    public void validateDCID(String DcID)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(0).getId().toString(), DcID, "Wrong DCID details returned");
    }
    public void validateDCID(int index , String DcID)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getId().toString(), DcID, "Wrong DCID details returned");
    }

    public void validateID(String DcID)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(0).getId().toString(), DcID, "Wrong DCID details returned");
    }

    public void validateID(int index, String Id)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getId().toString(), Id, "Unexpected Status");

    }

    public void validateIsActive(int index, Boolean status)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getActive(), status, "Wrong Active value for index" + index);
    }

    public void validateCourierCode(int index, String code)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getCourierCode(), code, "Wrong courier code");
    }

    public void validateIsStrictServiceable(int index , Boolean isStrictServiceable)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getIsStrictServiceable(), isStrictServiceable, "Wrong boolean value");
    }


    public void validateState(int index, String state)
    {
        Assert.assertTrue(lmsTripResponse.getDeliveryCenters().get(index).getState().equalsIgnoreCase(state), "Wrong state");
    }

    public void validateContactNum(int index, String num)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getContactNumber(), num, "Wrong code");
    }

    public void validateIsSelfShipped(int index, Boolean status)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getSelfShipSupported(), status, "Wrong Self Shipped value for index" + index);
    }

    public void validateCity(int index, String city)
    {
        Assert.assertTrue(lmsTripResponse.getDeliveryCenters().get(index).getCity().equalsIgnoreCase(city),"Wrong courier code");
    }

    public void validateManagerName(int index, String manager)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getManager(), manager, "Wrong courier code");
    }
    public void validateCourierCode(String code)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(0).getCourierCode(), code, "Wrong courier code");
    }
    public void validateCode(String code)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(0).getCode(), code, "Wrong code");
    }
    public void validateCode(int index, String code)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getCode(), code, "Wrong code");
    }

    public void validatePincode(int index, String pincode)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getPincode(), pincode, "Wrong code");
    }


    public void validateStoreReconType(int index, String reconType)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getReconType().toString(), reconType, "Wrong type");
    }


    public void validateType(int index, String type)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getType().toString(), type, "Invalid type" );
    }

    public void validateTenantId(int index, String tenantId)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getTenantId(), tenantId, "Wrong Tenant ID");
    }

    public void validateTenantId(String tenantId)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(0).getTenantId(), tenantId, "Wrong Tenant ID");
    }

    public void validateName(int index, String name)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryCenters().get(index).getName(), name, "Name Mismatch");
    }
}