/**
 * Created by Abhishek Jaiswal on 15/11/18.
 */
package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.DeliveryCenterClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.client.tools.response.ApplicationPropertiesResponse;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.response.EmptyResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.DeliveryStaffCommute;
import com.myntra.lastmile.client.code.utils.DeliveryStaffType;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.ScanAndSortShipmentRequest;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.FinanceReportResponse;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.response.StoreTripResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.OrderShipmentAssociationStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.platform.domain.*;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.testbase.BaseTest;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.LAST_MILE_PARTNER;
import static com.myntra.lastmile.client.code.utils.ReconType.EOD_RECON;


public class MDA_App extends BaseTest {

    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);


    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private MDA_Helper mda_helper = new MDA_Helper();
    private OrderEntry orderEntry = new OrderEntry();
    private ItemEntry itemEntry = new ItemEntry();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private ProcessRelease processRelease = new ProcessRelease();
    MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(LMS_Trip.class);
    HashMap<String, String> pincodeDCMap;
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();


    //creating HashMap to maintain pincode and DcId
    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19327", enabled = true)
    public void authenticate() throws Exception {

        String response = mda_helper.authenticatePartnerAPI();
        String token = mda_helper.tokenExtraction(response);
        System.out.println(token);
        Assert.assertTrue(response.contains(EnumSCM.SUCCESS));
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19328, activeTripOrder", enabled = true)
    public void activeTripOrder() throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = mda_helper.activeTripOrders(LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "No response received");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19329, findSDAByMobNum", enabled = true)
    public void findSDAByMobNum() throws Exception {
        DeliveryStaffResponse deliveryStaffResponse = mda_helper.findSDAByMobNo("8861786572");
        Assert.assertEquals(deliveryStaffResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Unable to receive SDA data");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19330, updateTripOrder", enabled = true)
    public void updateTripOrder() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        TripOrderAssignmentResponse tripAssignmentResponse2 = tripClient_qa.getTripDetails(lmsHelper.getTrackingNumber(packetId), LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(tripAssignmentResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(tripAssignmentResponse2.getTripOrders().get(0).getId());
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator2 = new TripOrderAssignmentResponseValidator(tripAssignmentResponse2);
        Assert.assertEquals(tripAssignmentResponse2.getStatus().getTotalCount(), 1, "Invalid :Multiple Orders from the same tracking num");
        tripOrderAssignmentResponseValidator2.validateOrderId(packetId);
        tripOrderAssignmentResponseValidator2.validateTripId(tripId);
        Assert.assertEquals(tripAssignmentResponse2.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid tenant id received from response");
        //------------------ complete trip -----------------------
        Assert.assertEquals(mda_helper.updateOrderInTrip(packetId, EnumSCM.DELIVERED,EnumSCM.UPDATE,tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip."); //check if API is latest
        Thread.sleep(3000);
        Assert.assertEquals(mda_helper.updateOrderInTrip(packetId, EnumSCM.DELIVERED,EnumSCM.TRIP_COMPLETE,tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip."); //check if API is latest

    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19331, mdaConfig", enabled = true)
    public void findByDeliveryCenter() throws Exception {
        DeliveryStaffResponse deliveryStaffResponse = mda_helper.deliveryStaffForDeliveryCenter("5446");//DCID
        Assert.assertEquals(deliveryStaffResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Unable to find SDA ");

    }

    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2)
    public void dd() throws JSONException, JAXBException, WebClientException, XMLStreamException, IOException {
        mda_helper.authenticatePartnerAPI();
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19332, mdaConfig", enabled = true)
    public void getAllUnattemptedShipments() throws Exception {
        TripOrderAssignmentResponse tripOrderAssignmentResponse = mda_helper.getAllUnattemptedShipments("5446");//DCID
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Unable to find unattempted shipments");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19333, mdaConfig", enabled = true)
    public void scanAndSort() throws Exception {
        ScanAndSortShipmentResponse scanAndSortShipmentRequest = mda_helper.scanAndSort("Banglore", LASTMILE_CONSTANTS.TENANT_ID, "ML0001338957");
        Assert.assertEquals(scanAndSortShipmentRequest.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Unable to find unattempted shipments");

    }
    @Deprecated
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19334 , mdaConfig", enabled = false)
    public void findShipmentsByTrip() throws Exception {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = mda_helper.findShipmentsByTrip("93160", ShipmentType.REVERSE_BAG.toString(), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Unable to find unattempted shipments");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19335 , mdaConfig", enabled = true)
    public void mdaConfig() throws Exception {
        ApplicationPropertiesResponse applicationPropertiesResponse = mda_helper.mdaConfig();
        Assert.assertEquals(applicationPropertiesResponse.getStatus().getStatusType().toString(),EnumSCM.ERROR, "Unable to find mda config");

    }

}