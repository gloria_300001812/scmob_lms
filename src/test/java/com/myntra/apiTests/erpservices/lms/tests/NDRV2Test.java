package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.service.DeliveryCenterClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.client.MLShipmentClient;
import com.myntra.apiTests.erpservices.lms.validators.NDRValidator;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.lmsUploader.LMSDownloadHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.MLPickupShipmentStatus;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.BulkJobResponse;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.S3FileResponse;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.platform.domain.NDRStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateAdditionalInfo;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.test.commons.testbase.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class NDRV2Test extends BaseTest {
    String env = getEnvironment();
    LMSHelper lmsHelper = new LMSHelper();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    HashMap<String, String> pincodeDCMap;
    public static String DcId;
    private OrderEntry orderEntry = new OrderEntry();
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NDRTest.class);
    DeliveryCenterClient_QA deliveryCenterClient_qa = new DeliveryCenterClient_QA();
    NDRServiceHelper ndrServiceHelper = new NDRServiceHelper();
    StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    TripClient_QA tripClient_qa=new TripClient_QA();

    //creating HashMap to maintain pincode and DcId
    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
    }

    /**
     * ***************************FORWARD FLOW***************************
     * if ndr_config table, is_enable=1 and is_rto_blocked=0,then All tranction will be gone through the Customer Update.
     * if ndr_config table, is_enable=1 and is_rto_blocked=1,then RTO will be work as per Customer Update.
     * if ndr_config table, is_enable=0 and is_rto_blocked=1,then NDR update will not go & RTO will be blocked Bas
     */
    @Test(groups={"Sanity"}, priority=1, enabled=true,description="Config:-is_enabled`=0,`is_rto_blocked`=1,Create Delivery Orders till SH status,Mark the order as FD," +
            "NDR status should be Null,Status Validation:- NULL To IN_PROGRESS is Possible -> IN_PROGRESS to REQueue is allowed -> IN_PROGRESS to REATTEMPT is allowed, " +
            "IN_PROGRESS to RTO is blocked,"+
            "To check and verify the NDR Report validation.")
    public void updateShipmentinDifferentStatus() throws Exception {
        String trackingNumber1 = null;
        String trackingNumber2 = null;
        String trackingNumber3 = null;
        String packetId1 = null;
        String packetId2 = null;
        String packetId3 = null;
        packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        log.debug("packetId 1:-" + packetId1 + ",PacketId 2" + packetId2 + "PacketId 3" + packetId3);

        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);
        trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + " trackingNumber 3:-" + trackingNumber3);

        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId2)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.toString(), 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString(), 2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);

        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);

        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), (AttemptReasonCode) dataMap1.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId1, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), (AttemptReasonCode) dataMap2.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId2, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), (AttemptReasonCode) dataMap3.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId3, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");

        //NDR Config DB Update:- `is_enabled`=0,`is_rto_blocked`=1 for RTO validation
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=1 where `service_type`='DELIVERY'and `attempt_reason_code`='INCOMPLETE_ADDRESS' and `client_id`=2297;", "lms");
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=1 where `service_type`='DELIVERY'and `attempt_reason_code`='CUSTOMER_UNREACHABLE' and `client_id`=2297;", "lms");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //validation: After Delivery NDR status should be NULL
        NDRValidator.ValidateNDRStatusForShipment(packetId1, "After Delivery NDR status should be NULL");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet2), NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId2, "After FD packet2, NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet3) NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId3, "checking After FD packet3 NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //Received FD and Complete the trip
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.completeTrip(tripData);

        //Update Order NDR status NULL To IN_PROGRESS for tracking Number2
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.IN_PROGRESS, trackingNumber2, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber2,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber2, NDRStatus.IN_PROGRESS, ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());


        //NDR Bulk Download
        BulkJobResponse bulkJobResponse = NDRServiceHelper.ndrCreateBulkJob(LMS_CONSTANTS.NDR.NAME, LMS_CONSTANTS.NDR.PAYLOAD_FORWARD + "," + DateTimeHelper.generateDate("YYYY-MM-dd",-2) + "," + DateTimeHelper.generateDate("YYYY-MM-dd",0), BulkJobType.BULK_NDR_3PL_REPORT_DOWNLOAD, LMS_CONSTANTS.TENANTID);
        Long bulkJobId = bulkJobResponse.getBulkJobEntryList().get(0).getId();
        Assert.assertEquals(bulkJobResponse.getStatus().getStatusMessage(), "Bulk Job Created");
        BulkJobResponse responseStatus=NDRServiceHelper.validate_BulkJob_Status(bulkJobId);
        Assert.assertEquals(responseStatus.getStatus().getStatusMessage().toString(),"ORDER(s) retrieved successfully");
        statusPollingValidator.validate_BulkJob_Status(bulkJobId,"COMPLETE");
        S3FileResponse s3FileResponse = NDRServiceHelper.getNDRDownloadURL(bulkJobId);
        Assert.assertEquals(s3FileResponse.getStatus().getStatusCode(), 3);
        String url = s3FileResponse.getData().getUrl();
        List<String> lstValue = LMSDownloadHelper.downloadFileFromAmazonurl(url);


        //Update Order NDR status Null To IN_PROGRESS for tracking Number3
        shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.IN_PROGRESS, trackingNumber3, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber3,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber3, NDRStatus.IN_PROGRESS, ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());


        //Verify that, IN_PROGRESS to RTO is blocked for Tracking Number2
        MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.DL, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber2);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber2, null);


        //IN_PROGRESS to REQueue is allowed at this point.
        TripOrderAssignmentResponse response = lmsServiceHelper.requeueOrder(packetId2);
        Assert.assertEquals(response.getStatus().getStatusMessage(), "Success");
        Assert.assertEquals(response.getStatus().getStatusType().toString(), "SUCCESS");
        MLShipmentResponse mlShipmentResponse1 = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(trackingNumber2);
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        Assert.assertEquals(mlShipmentResponse1.getMlShipmentEntries().get(0).getNdrStatus().name(), NDRStatus.NDR_NO_RESPONSE.toString(), "After REQueue NDR status should be NDR_NO_RESPONSE");


        //Verify IN_PROGRESS to REATTEMPT is allowed for FD Tracking Number3.
        shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.REATTEMPT, trackingNumber3, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber3,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.REATTEMPT.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber3, NDRStatus.REATTEMPT, ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());


    }


    /**
     * *********************REVERSE FLOW *********************************************
     */
    @Test(groups={"Sanity"}, priority=2, enabled=true,description="Config:-is_enabled`=0,`is_rto_blocked`=1\n" +
            "Create return,Mark the order as failed PickUp,NDR status should be Null,\n" +
            "Status Validation:- Verify NDR status NUll to In_Progress update shipment status & validate NDR Config,\n" +
            "                    Verify NDR status In_Progress to REATTEMPT is Possible.\n" +
            "                    Reattempt to Requeue is Possible.\n" +
            "                    Verify NDR status In_Progress to RTO should be blocked.\n" +
            "                    Validate NDRDashBoard validation")
    public void updateShipmentinDifferentStatusForPickUp() throws Exception {
        String trackingNumberForPickup = null;
        String forwardOrderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.toString(), "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(forwardOrderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        com.myntra.returns.response.ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135078", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String returnOrderID = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(returnOrderID);

        //menifest Open Box PickUP
        String pickup_trackingNo = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().equals(ReturnMode.OPEN_BOX_PICKUP), "");
        lmsReturnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lmsReturnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        //assignOrderToTrip for pickup
        trackingNumberForPickup = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();
        LastmileClient client = new LastmileClient();
        client.assignOrderToTrip(tripId, trackingNumberForPickup);

        //Start trip
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        // Update trip with reason
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID), AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE.name(), TripAction.UPDATE.name()).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        tripData.add(dataMap1);
        //Received FP
        lmsServiceHelper.receiveShipment(trackingNumberForPickup);
        //Complete the trip
        lmsServiceHelper.completeTrip(tripData);

        //NDR Config DB Update:- `is_enabled`=0,`is_rto_blocked`=1 for RTO validation
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=1 where `service_type`='PICKUP'and `attempt_reason_code`='CUSTOMER_UNREACHABLE' and `client_id`=2297;", "lms");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //VALIDATION AFTER FAILED PICKUP
        MLShipmentResponse mlShipmentResponse = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(lmsHelper.getReturnsTrackingNumber.apply(returnID).toString());
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "SUCCESS", "Success", "3");

        //Verify NDR status NUll to In_Progress update shipment status & validate NDR Config
        ShipmentUpdateResponse shipmentUpdateResponse1 = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.IN_PROGRESS, trackingNumberForPickup, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumberForPickup,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse1, trackingNumberForPickup, NDRStatus.IN_PROGRESS, ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLPickupShipmentStatus.FAILED_PICKUP.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());

        //Verify NDR status In_Progress to RTO should be blocked
        com.myntra.lastmile.client.entry.MLShipmentResponse ml_ShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.PU, LASTMILE_CONSTANTS.TENANT_ID, trackingNumberForPickup);
        NDRValidator.ValidateMLShipmentResponse(ml_ShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumberForPickup, null);


        //Verify NDR status In_Progress to REATTEMPT is Possible
        ShipmentUpdateResponse shipmentUpdateResponse2 = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.REATTEMPT, trackingNumberForPickup, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumberForPickup,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.REATTEMPT.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse2, trackingNumberForPickup, NDRStatus.REATTEMPT, ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLPickupShipmentStatus.FAILED_PICKUP.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());

        //NDR Bulk Download
        BulkJobResponse bulkJobResponse = NDRServiceHelper.ndrCreateBulkJob(LMS_CONSTANTS.NDR.NAME, LMS_CONSTANTS.NDR.PAYLOAD_REVERSE + "," + DateTimeHelper.generateDate("YYYY-MM-dd",-2) + "," + DateTimeHelper.generateDate("YYYY-MM-dd",0), BulkJobType.BULK_NDR_3PL_REPORT_DOWNLOAD, LMS_CONSTANTS.TENANTID);
        Long bulkJobId = bulkJobResponse.getBulkJobEntryList().get(0).getId();
        Assert.assertEquals(bulkJobResponse.getStatus().getStatusMessage(), "Bulk Job Created");
        BulkJobResponse responseStatus=NDRServiceHelper.validate_BulkJob_Status(bulkJobId);
        Assert.assertEquals(responseStatus.getStatus().getStatusMessage().toString(),"ORDER(s) retrieved successfully");
        statusPollingValidator.validate_BulkJob_Status(bulkJobId,"COMPLETE");
        S3FileResponse s3FileResponse = NDRServiceHelper.getNDRDownloadURL(bulkJobId);
        Assert.assertEquals(s3FileResponse.getStatus().getStatusCode(), 3);
        String url = s3FileResponse.getData().getUrl();
        List<String> lstValue = LMSDownloadHelper.downloadFileFromAmazonurl(url);


        //Reattempt to Requeue is Possible
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormat.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumberForPickup);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("failed pickup requeued for another attempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponseBody = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponseBody.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");
        Assert.assertTrue(mlShipmentResponseBody.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");
        lmsReturnHelper.processReturnWithoutNewValidation(returnID.toString(), AttemptReasonCode.PICKED_UP_SUCCESSFULLY.name());
    }

    /**
     * Try and Buy Flow
     */
    @Test(groups={"Sanity"}, priority=3, enabled=true,description="Config:-is_enabled`=0,`is_rto_blocked`=1,Create T & B Orders till SH status,Mark the order as FD," +
            "NDR status should be Null,Status Validation:- NULL To IN_PROGRESS is Possible -> IN_PROGRESS to REQueue is allowed -> IN_PROGRESS to REATTEMPT is allowed, " +
            "IN_PROGRESS to RTO is blocked"+
            "To check and verify the NDR Report validation.")
    public void updateShipmentinDifferentStatusTandB() throws Exception {
        String trackingNumber1 = null;
        String trackingNumber2 = null;
        String trackingNumber3 = null;
        String packetId1 = null;
        String packetId2 = null;
        String packetId3 = null;
        packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        log.debug("packetId 1:-" + packetId1 + ",PacketId 2" + packetId2 + "PacketId 3" + packetId3);

        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);
        trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        //log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + " trackingNumber 3:-" + trackingNumber3);

        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId2)).getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to assignOrderToTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.name(), 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name());
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name(), 2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.REQUESTED_RE_SCHEDULE);

        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);

        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), (AttemptReasonCode) dataMap1.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId1, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), (AttemptReasonCode) dataMap2.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId2, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), (AttemptReasonCode) dataMap3.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId3, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");

        log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + ",trackingNumber3:- " + trackingNumber3);


        //NDR Config DB Update:- `is_enabled`=0,`is_rto_blocked`=1 for RTO validation
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=1 where `service_type`='TRY_AND_BUY'and `attempt_reason_code`='CUSTOMER_UNREACHABLE' and `client_id`=2297;", "lms");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //checking After FD (packet1), NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId1, "After Delivery NDR status should be NULL");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet2), NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId2, "After FD packet2, NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet3) NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId3, "checking After FD packet3 NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //Received FAILED T & B and Complete the trip
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId1));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.completeTrip(tripData);

        //Update Order NDR status NULL To IN_PROGRESS for tracking Number1
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.IN_PROGRESS, trackingNumber1, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber1,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber1, NDRStatus.IN_PROGRESS, ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());

        //NDR Bulk Download
        BulkJobResponse bulkJobResponse = NDRServiceHelper.ndrCreateBulkJob(LMS_CONSTANTS.NDR.NAME, LMS_CONSTANTS.NDR.PAYLOAD_FORWARD + "," + DateTimeHelper.generateDate("YYYY-MM-dd",-2) + "," + DateTimeHelper.generateDate("YYYY-MM-dd",0), BulkJobType.BULK_NDR_3PL_REPORT_DOWNLOAD, LMS_CONSTANTS.TENANTID);
        Long bulkJobId = bulkJobResponse.getBulkJobEntryList().get(0).getId();
        Assert.assertEquals(bulkJobResponse.getStatus().getStatusMessage(), "Bulk Job Created");
        BulkJobResponse responseStatus=NDRServiceHelper.validate_BulkJob_Status(bulkJobId);
        Assert.assertEquals(responseStatus.getStatus().getStatusMessage().toString(),"ORDER(s) retrieved successfully");
        statusPollingValidator.validate_BulkJob_Status(bulkJobId,"COMPLETE");
        S3FileResponse s3FileResponse = NDRServiceHelper.getNDRDownloadURL(bulkJobId);
        Assert.assertEquals(s3FileResponse.getStatus().getStatusCode(), 3);
        String url = s3FileResponse.getData().getUrl();
        List<String> lstValue = LMSDownloadHelper.downloadFileFromAmazonurl(url);


        //Update Order NDR status Null To IN_PROGRESS for tracking Number3
        shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.IN_PROGRESS, trackingNumber3, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber3,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber3, NDRStatus.IN_PROGRESS, ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());

        //Update Order NDR status Null To IN_PROGRESS for tracking Number2
        shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.IN_PROGRESS, trackingNumber2, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber2,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber2, NDRStatus.IN_PROGRESS, ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());

        //Verify that, IN_PROGRESS to RTO is blocked for Tracking Number2
        MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber2);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber2, null);

        //IN_PROGRESS to REQueue is allowed at this point for packetId2.
        TripOrderAssignmentResponse response = lmsServiceHelper.requeueOrder(packetId2);
        Assert.assertEquals(response.getStatus().getStatusMessage(), "Success");
        Assert.assertEquals(response.getStatus().getStatusType().toString(), "SUCCESS");
        MLShipmentResponse mlShipmentResponse1 = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(trackingNumber2);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber2,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.NDR_NO_RESPONSE.toString());
        Assert.assertEquals(mlShipmentResponse1.getMlShipmentEntries().get(0).getNdrStatus().name(), NDRStatus.NDR_NO_RESPONSE.toString(), "After REQueue NDR status should be NDR_NO_RESPONSE");

        //Verify IN_PROGRESS to REATTEMPT is allowed for FD packet3.
        shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.REATTEMPT, trackingNumber3, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber3,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.REATTEMPT.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber3, NDRStatus.REATTEMPT, ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());
    }


    /**
     * Create Exchage till SH status Mark all Shipment as Failed Shipment and validate NDR status
     */
    @Test(groups={"Sanity"}, priority=0, enabled=true,description="Config:-is_enabled`=0,`is_rto_blocked`=1,Create Exchange Orders till OFD status,Mark the order as failed Exchange," +
            "NDR status should be Null,Status Validation:- NULL To IN_PROGRESS is Possible -> IN_PROGRESS to REQueue is allowed -> IN_PROGRESS to REATTEMPT is allowed, " +
            "IN_PROGRESS to RTO is blocked"+
            "To check and verify the NDR Report validation.")
    public void updateShipmentinDifferentStatusExchange() throws Exception {
        List<String> al = new ArrayList();
        List<String> trackingNumberList = new ArrayList<>();
        List<AttemptReasonCode> attemptReasonCodes = Arrays.asList(AttemptReasonCode.REQUESTED_RE_SCHEDULE, AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS, AttemptReasonCode.REFUSED_TO_ACCEPT, AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        for (int i = 0; i<=4; i++) {
            LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
            String orderIDParent = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true);
            String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true, omsServiceHelper.getReleaseId(orderIDParent));
            String packetId = omsServiceHelper.getPacketId(exchangeOrder);
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);
            al.add(packetId);
            trackingNumberList.add(trackingNumber);
        }
        //create trip
        Long tripId = ndrServiceHelper.createTrip();
        List<Map<String, Object>> tripData = new ArrayList<>();

        //addShipmentToTrip
        Iterator<String> trackingNoItr = trackingNumberList.iterator();
        while (trackingNoItr.hasNext()) {
            ndrServiceHelper.addShipmentToTrip(tripId, trackingNoItr.next());
        }

        //startTrip
        ndrServiceHelper.startTrip(tripId);

        // Add the attempt reson code
        Iterator<String> packetItr = al.iterator();
        trackingNoItr = trackingNumberList.iterator();
        Iterator<AttemptReasonCode> reasonsItr = attemptReasonCodes.iterator();
        while (packetItr.hasNext()) {
            String packetId = packetItr.next();
            String trackingNumber = trackingNoItr.next();
            AttemptReasonCode reasonCode = reasonsItr.next();
            Map<String, Object> tripOrder = ndrServiceHelper.processFailedExchange(packetId, trackingNumber, reasonCode, tripId);
            tripData.add(tripOrder);

        }

        //RECEIVE Shipment
        trackingNoItr = al.iterator();
        while (trackingNoItr.hasNext()) {
            lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(trackingNoItr.next()));
        }

        //Complete trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.completeTrip(tripData);
        log.info("TripOrderAssignmentResponse=" + tripOrderAssignmentResponse.getStatus().getStatusMessage());

        //NDR Config DB Update:- `is_enabled`=0,`is_rto_blocked`=1 for RTO validation
        DBUtilities.exUpdateQuery(
                "update ndr_config set is_enabled=0,is_rto_blocked=1 where service_type='EXCHANGE'and attempt_reason_code='REFUSED' and client_id=2297;", "lms");
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        DBUtilities.exUpdateQuery(
                "update ndr_config set is_enabled=0,is_rto_blocked=0 where service_type='EXCHANGE'and attempt_reason_code='CUSTOMER_UNREACHABLE' and client_id=2297;", "lms");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //checking After FD, NDR status should be NULL status
        trackingNoItr = trackingNumberList.iterator();
        while (trackingNoItr.hasNext()) {
            ndrServiceHelper.verifyAndUpdateNdrStatusAfterFD(trackingNoItr.next());
        }

        ndrServiceHelper.verifyInProgressToRtoBlockedAndRequeue(al.get(1), trackingNumberList.get(1));
        ndrServiceHelper.verifyInProgressToReattemptIsAllowed(trackingNumberList.get(2));
        //varify config:- IS_Enable '0' and Is_RTO Blocked '1' RTO should be blocked.
        ndrServiceHelper.varifyNDRConfig2(trackingNumberList.get(3));
        //varify config:- IS_Enable '0' and Is_RTO Blocked '0' RTO should not be blocked.
        ndrServiceHelper.varifyNDRConfig1(trackingNumberList.get(4));
    }


    /**
     * NDR Config TestCase
     * IS_Enable '0' and Is_RTO Blocked '1' RTO should be blocked.
     * IS_Enable '1' and Is_RTO Blocked '1' Check NDR status.
     * IS_Enable '0' and Is_RTO Blocked '0' RTO should not be blocked.
     */
    @Test(groups={"Sanity"}, priority=5, enabled=true,description="RTO validation based on the NDR Flag,Create Delivery Order till OFD ,Mark the order as failed Delivery,NDR status should be Null,"+
            "Config:-is_enabled`=0,`is_rto_blocked`=1,NDR status should be NULL & RTO should be blocked." +
            "Config:-IS_Enable =1 and Is_RTO Blocked=1,After FD NDR status should be IN_PROGRESS." +
            "Validate NDR DashBoard")
    public void rtoConfigForDeliveryOrder() throws Exception {
        String trackingNumber1 = null;
        String trackingNumber2 = null;
        String trackingNumber3 = null;
        String packetId1 = null;
        String packetId2 = null;
        String packetId3 = null;
        packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        log.debug("packetId 1:-" + packetId1 + ",PacketId 2" + packetId2 + "PacketId 3" + packetId3);

        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);
        trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + " trackingNumber 3:-" + trackingNumber3);

        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = Long.parseLong(lmsServiceHelper.getDeliveryStaffID("" + DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId2)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.name(), 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name(), 2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.REFUSED_TO_ACCEPT);

        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.CASH_CARD_NOT_READY);

        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), (AttemptReasonCode) dataMap1.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId1, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), (AttemptReasonCode) dataMap2.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId2, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), (AttemptReasonCode) dataMap3.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId3, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        //Received FD and Complete the trip
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId1));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.completeTrip(tripData);

        //NDR Config DB Update:- `is_enabled`=0,`is_rto_blocked`=1 for RTO validation
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=1 where `service_type`='DELIVERY'and `attempt_reason_code`='COD_NOT_READY' and `client_id`=2297;", "lms");
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=0 where `service_type`='DELIVERY'and `attempt_reason_code`='CUSTOMER_UNREACHABLE' and `client_id`=2297;", "lms");
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=1,`is_rto_blocked`=1 where `service_type`='DELIVERY'and `attempt_reason_code`='REFUSED' and `client_id`=2297;", "lms");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //RTO config validation:-IS_Enable '0' and Is_RTO Blocked '1' RTO(trackingNumber2) should be blocked.
        com.myntra.lastmile.client.entry.MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.DL, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber2);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber2, null);

        //RTO config validation:-IS_Enable '0' and Is_RTO Blocked '0' RTO(trackingNumber3) should not be blocked.
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber3);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.DL);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");

        //RTO config validation:-IS_Enable '1' and Is_RTO Blocked '1' Check NDR status.
        //After FD NDR status should be IN_PROGRESS for tracking Number1
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.IN_PROGRESS, trackingNumber1, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber1,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber1, NDRStatus.IN_PROGRESS, ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());
        //NDR Bulk Download
        BulkJobResponse bulkJobResponse = NDRServiceHelper.ndrCreateBulkJob(LMS_CONSTANTS.NDR.NAME, LMS_CONSTANTS.NDR.PAYLOAD_REVERSE + "," + DateTimeHelper.generateDate("YYYY-MM-dd",-2) + "," + DateTimeHelper.generateDate("YYYY-MM-dd",0), BulkJobType.BULK_NDR_3PL_REPORT_DOWNLOAD, LMS_CONSTANTS.TENANTID);
        Long bulkJobId = bulkJobResponse.getBulkJobEntryList().get(0).getId();
        Assert.assertEquals(bulkJobResponse.getStatus().getStatusMessage(), "Bulk Job Created");
        BulkJobResponse responseStatus=NDRServiceHelper.validate_BulkJob_Status(bulkJobId);
        Assert.assertEquals(responseStatus.getStatus().getStatusMessage().toString(),"ORDER(s) retrieved successfully");
        statusPollingValidator.validate_BulkJob_Status(bulkJobId,"COMPLETE");
        S3FileResponse s3FileResponse = NDRServiceHelper.getNDRDownloadURL(bulkJobId);
        Assert.assertEquals(s3FileResponse.getStatus().getStatusCode(), 3);
        String url = s3FileResponse.getData().getUrl();
        List<String> lstValue = LMSDownloadHelper.downloadFileFromAmazonurl(url);

    }


    @Test(groups={"Sanity"}, priority=6, enabled=true,description="RTO validation based on the NDR Flag,Create Return,Mark the order as failed pickup,NDR status should be Null,"+
            "Config:-is_enabled`=0,`is_rto_blocked`=1,NDR status should be NULL & RTO should be blocked." +
            "Config:-IS_Enable =1 and Is_RTO Blocked=1,After FD NDR status should be IN_PROGRESS." +
            "Validate NDR DashBoard")
    public void rtoConfigForPickUp() throws Exception {
        String trackingNumberForPickup = null;
        String forwardOrderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(forwardOrderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        com.myntra.returns.response.ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135078", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String returnOrderID = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(returnOrderID);
        //menifest Open Box PickUP
        String pickup_trackingNo = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().equals(ReturnMode.OPEN_BOX_PICKUP), "");
        lmsReturnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lmsReturnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = Long.parseLong(lmsServiceHelper.getDeliveryStaffID("" + DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        //assignOrderToTrip for pickup
        trackingNumberForPickup = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();
        LastmileClient client = new LastmileClient();
        client.assignOrderToTrip(tripId, trackingNumberForPickup);

        //Start trip
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        // Update trip with reason
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID), EnumSCM.FAILED, TripAction.UPDATE.name()).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.HAPPY_WITH_PRODUCT);
        tripData.add(dataMap1);
        //Received FP
        lmsServiceHelper.receiveShipment(trackingNumberForPickup);
        //Complete the trip
        lmsServiceHelper.completeTrip(tripData);

        //NDR Config DB Update:- `is_enabled`=0,`is_rto_blocked`=1 for RTO validation
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=1 where `service_type`='PICKUP'and `attempt_reason_code`='CUSTOMER_SATISFIED_WITH_PRODUCT' and `client_id`=2297;", "lms");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //NDR config Validation:-IS_Enable '0' and Is_RTO Blocked '1' RTO should be blocked.
        com.myntra.lastmile.client.entry.MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.DL, LASTMILE_CONSTANTS.TENANT_ID, trackingNumberForPickup);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumberForPickup, null);
        //NDR Bulk Download
        BulkJobResponse bulkJobResponse = NDRServiceHelper.ndrCreateBulkJob(LMS_CONSTANTS.NDR.NAME, LMS_CONSTANTS.NDR.PAYLOAD_REVERSE + "," + DateTimeHelper.generateDate("YYYY-MM-dd",-2) + "," + DateTimeHelper.generateDate("YYYY-MM-dd",0), BulkJobType.BULK_NDR_3PL_REPORT_DOWNLOAD, LMS_CONSTANTS.TENANTID);
        Long bulkJobId = bulkJobResponse.getBulkJobEntryList().get(0).getId();
        Assert.assertEquals(bulkJobResponse.getStatus().getStatusMessage(), "Bulk Job Created");
        BulkJobResponse responseStatus=NDRServiceHelper.validate_BulkJob_Status(bulkJobId);
        Assert.assertEquals(responseStatus.getStatus().getStatusMessage().toString(),"ORDER(s) retrieved successfully");
        statusPollingValidator.validate_BulkJob_Status(bulkJobId,"COMPLETE");
        S3FileResponse s3FileResponse = NDRServiceHelper.getNDRDownloadURL(bulkJobId);
        Assert.assertEquals(s3FileResponse.getStatus().getStatusCode(), 3);
        String url = s3FileResponse.getData().getUrl();
        List<String> lstValue = LMSDownloadHelper.downloadFileFromAmazonurl(url);

    }


    @Test(groups={"Sanity"}, priority=7, enabled=true,description="RTO validation based on the NDR Flag,Create T & B Order till OFD ,Mark the order as failed Delivery,NDR status should be Null,\n" +
            "Config:-is_enabled`=0,`is_rto_blocked`=1,NDR status should be NULL & RTO should be blocked." +
            "Config:-is_enabled`=0,`is_rto_blocked`=0,NDR status should be NULL & RTO should not be blocked." +
            "Config:-IS_Enable =1 and Is_RTO Blocked=1,After FD NDR status should be IN_PROGRESS." +
            "Validate NDR DashBoard")
    public void rtoConfigForTryandBuy() throws Exception {
        String trackingNumber1 = null;
        String trackingNumber2 = null;
        String trackingNumber3 = null;
        String packetId1 = null;
        String packetId2 = null;
        String packetId3 = null;
        packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        log.debug("packetId 1:-" + packetId1 + ",PacketId 2" + packetId2 + "PacketId 3" + packetId3);

        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);
        trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        //log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + " trackingNumber 3:-" + trackingNumber3);

        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = Long.parseLong(lmsServiceHelper.getDeliveryStaffID("" + DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId2)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.name(), 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name(), 2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.REFUSED_TO_ACCEPT);

        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.CASH_CARD_NOT_READY);

        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), (AttemptReasonCode) dataMap1.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId1, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), (AttemptReasonCode) dataMap2.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId2, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), (AttemptReasonCode) dataMap3.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId3, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");

        //Received FAILED T & B and Complete the trip
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId1));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.completeTrip(tripData);

        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=1,`is_rto_blocked`=1 where `service_type`='TRY_AND_BUY'and `attempt_reason_code`='REFUSED' and `client_id`=2297;", "lms");
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=1 where `service_type`='TRY_AND_BUY'and `attempt_reason_code`='COD_NOT_READY' and `client_id`=2297;", "lms");
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=0 where `service_type`='TRY_AND_BUY'and `attempt_reason_code`='CUSTOMER_UNREACHABLE' and `client_id`=2297;", "lms");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //RTO config validation:-IS_Enable '0' and Is_RTO Blocked '1' RTO(trackingNumber2) should be blocked.
        com.myntra.lastmile.client.entry.MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber2);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber2, null);

        //RTO config validation:-IS_Enable '0' and Is_RTO Blocked '0' RTO(trackingNumber3) should not be blocked.
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber3);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.TRY_AND_BUY);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");

        //RTO config validation:-IS_Enable '1' and Is_RTO Blocked '1' Check NDR status.
        //After FD NDR status should be IN_PROGRESS for tracking Number1
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.IN_PROGRESS, trackingNumber1, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber1,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber1, NDRStatus.IN_PROGRESS, ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());
        //NDR Bulk Download
        BulkJobResponse bulkJobResponse = NDRServiceHelper.ndrCreateBulkJob(LMS_CONSTANTS.NDR.NAME, LMS_CONSTANTS.NDR.PAYLOAD_FORWARD + "," + DateTimeHelper.generateDate("YYYY-MM-dd",-2) + "," + DateTimeHelper.generateDate("YYYY-MM-dd",0), BulkJobType.BULK_NDR_3PL_REPORT_DOWNLOAD, LMS_CONSTANTS.TENANTID);
        Long bulkJobId = bulkJobResponse.getBulkJobEntryList().get(0).getId();
        Assert.assertEquals(bulkJobResponse.getStatus().getStatusMessage(), "Bulk Job Created");
        BulkJobResponse responseStatus=NDRServiceHelper.validate_BulkJob_Status(bulkJobId);
        Assert.assertEquals(responseStatus.getStatus().getStatusMessage().toString(),"ORDER(s) retrieved successfully");
        statusPollingValidator.validate_BulkJob_Status(bulkJobId,"COMPLETE");
        S3FileResponse s3FileResponse = NDRServiceHelper.getNDRDownloadURL(bulkJobId);
        Assert.assertEquals(s3FileResponse.getStatus().getStatusCode(), 3);
        String url = s3FileResponse.getData().getUrl();
        List<String> lstValue = LMSDownloadHelper.downloadFileFromAmazonurl(url);

    }


    /*
      NULL- All transitions will be allowed
    */
    @Test(groups={"Sanity"}, priority=8, enabled=true,description="RTO validation based on the NDR Flag,Create Exchange Order till OFD ,Mark the order as failed Exchange,NDR status should be Null,\n" +
            "Config:-is_enabled`=0,`is_rto_blocked`=1,NDR status should be NULL & RTO should be blocked." +
            "Config:-is_enabled`=0,`is_rto_blocked`=0,NDR status should be NULL & RTO should not be blocked." +
            "Config:-IS_Enable =1 and Is_RTO Blocked=1,After FD NDR status should be IN_PROGRESS." +
            "Validate NDR DashBoard")
    public void UpdateNDRStatus() throws Exception {
        String trackingNumber1 = null;
        String trackingNumber2 = null;
        String trackingNumber3 = null;
        String packetId1 = null;
        String packetId2 = null;
        String packetId3 = null;
        packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        log.debug("packetId 1:-" + packetId1 + ",PacketId 2" + packetId2 + "PacketId 3" + packetId3);

        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);
        trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + " trackingNumber 3:-" + trackingNumber3);

        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId2)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.toString(), 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString(), 2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);

        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);

        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), (AttemptReasonCode) dataMap1.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId1, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), (AttemptReasonCode) dataMap2.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId2, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), (AttemptReasonCode) dataMap3.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId3, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");

        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=0 where `service_type`='DELIVERY'and `attempt_reason_code`='INCOMPLETE_ADDRESS' and `client_id`=2297;", "lms");
        DBUtilities.exUpdateQuery(
                "update `ndr_config` set `is_enabled`=0,`is_rto_blocked`=0 where `service_type`='DELIVERY'and `attempt_reason_code`='CUSTOMER_UNREACHABLE' and `client_id`=2297;", "lms");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //validation: After Delivery NDR status should be NULL
        NDRValidator.ValidateNDRStatusForShipment(packetId1, "After Delivery NDR status should be NULL");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet2), NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId2, "After FD packet2, NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet3) NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId3, "checking After FD packet3 NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //Received FD and Complete the trip
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.completeTrip(tripData);

        //Update Order NDR status NULL To requeue is allowed for tracking Number2
        TripOrderAssignmentResponse response = lmsServiceHelper.requeueOrder(packetId2);
        Assert.assertEquals(response.getStatus().getStatusMessage(), "Success");
        Assert.assertEquals(response.getStatus().getStatusType().toString(), "SUCCESS");
        MLShipmentResponse mlShipmentResponse1 = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(trackingNumber2);
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //Update Order NDR status NULL To RTO is allowed for tracking Number3
        SimpleDateFormat f = new SimpleDateFormat();
        String date = DateTimeHelper.referenceDateTimeConv();
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber3);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.DL);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");
        //NDR Bulk Download
        BulkJobResponse bulkJobResponse = NDRServiceHelper.ndrCreateBulkJob(LMS_CONSTANTS.NDR.NAME, LMS_CONSTANTS.NDR.PAYLOAD_REVERSE + "," + DateTimeHelper.generateDate("YYYY-MM-dd",-2) + "," + DateTimeHelper.generateDate("YYYY-MM-dd",0), BulkJobType.BULK_NDR_3PL_REPORT_DOWNLOAD, LMS_CONSTANTS.TENANTID);
        Long bulkJobId = bulkJobResponse.getBulkJobEntryList().get(0).getId();
        Assert.assertEquals(bulkJobResponse.getStatus().getStatusMessage(), "Bulk Job Created");
        BulkJobResponse responseStatus=NDRServiceHelper.validate_BulkJob_Status(bulkJobId);
        Assert.assertEquals(responseStatus.getStatus().getStatusMessage().toString(),"ORDER(s) retrieved successfully");
        statusPollingValidator.validate_BulkJob_Status(bulkJobId,"COMPLETE");
        S3FileResponse s3FileResponse = NDRServiceHelper.getNDRDownloadURL(bulkJobId);
        Assert.assertEquals(s3FileResponse.getStatus().getStatusCode(), 3);
        String url = s3FileResponse.getData().getUrl();
        List<String> lstValue = LMSDownloadHelper.downloadFileFromAmazonurl(url);
    }

    /*
        NDR_NO_RESPONSE- All transitions will be allowed
    */
    @Test(groups={"Sanity"}, priority=3, enabled=true,description="NDR_NO_RESPONSE validation based on the NDR Response,Create T & B Order till OFD ,Mark the order as failed Delivery," +
            " NDR status should be NULL & RTO should be blocked" +
            " Update Order NDR status NULL To IN_PROGRESS" +
            " Update Order NDR status NDR_NO_RESPONSE To RTO is allowed" +
            " NDR_NO_RESPONSE to REQueue is allowed." +
            " Validate NDR DashBoard")
    public void UpdateNDRStatus2() throws Exception {
        String trackingNumber1 = null;
        String trackingNumber2 = null;
        String trackingNumber3 = null;
        String packetId1 = null;
        String packetId2 = null;
        String packetId3 = null;
        packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        log.debug("packetId 1:-" + packetId1 + ",PacketId 2" + packetId2 + "PacketId 3" + packetId3);

        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);
        trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        //log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + " trackingNumber 3:-" + trackingNumber3);

        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId2)).getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to assignOrderToTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.name(), 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name());
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name(), 2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.REQUESTED_RE_SCHEDULE);

        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);

        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), (AttemptReasonCode) dataMap1.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId1, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), (AttemptReasonCode) dataMap2.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId2, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), (AttemptReasonCode) dataMap3.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId3, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");

        log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + ",trackingNumber3:- " + trackingNumber3);

        //checking After FD (packet1), NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId1, "After Delivery NDR status should be NULL");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet2), NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId2, "After FD packet2, NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet3) NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId3, "checking After FD packet3 NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //Received FAILED T & B and Complete the trip
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId1));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.completeTrip(tripData);

        //Update Order NDR status NULL To IN_PROGRESS for tracking Number1
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.NDR_NO_RESPONSE, trackingNumber1, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber1,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.NDR_NO_RESPONSE.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber1, NDRStatus.NDR_NO_RESPONSE, ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());
        //NDR Bulk update
        BulkJobResponse bulkJobResponse = NDRServiceHelper.ndrCreateBulkJob(LMS_CONSTANTS.NDR.NAME, LMS_CONSTANTS.NDR.PAYLOAD_FORWARD + "," + DateTimeHelper.generateDate("YYYY-MM-dd",-2) + "," + DateTimeHelper.generateDate("YYYY-MM-dd",0), BulkJobType.BULK_NDR_3PL_REPORT_DOWNLOAD, LMS_CONSTANTS.TENANTID);
        Long bulkJobId = bulkJobResponse.getBulkJobEntryList().get(0).getId();
        Assert.assertEquals(bulkJobResponse.getStatus().getStatusMessage(), "Bulk Job Created");
        BulkJobResponse responseStatus=NDRServiceHelper.validate_BulkJob_Status(bulkJobId);
        Assert.assertEquals(responseStatus.getStatus().getStatusMessage().toString(),"ORDER(s) retrieved successfully");
        statusPollingValidator.validate_BulkJob_Status(bulkJobId,"COMPLETE");
        S3FileResponse s3FileResponse = NDRServiceHelper.getNDRDownloadURL(bulkJobId);
        Assert.assertEquals(s3FileResponse.getStatus().getStatusCode(), 3);
        String url = s3FileResponse.getData().getUrl();
        List<String> lstValue = LMSDownloadHelper.downloadFileFromAmazonurl(url);


        //Update Order NDR status Null To IN_PROGRESS for tracking Number2
        shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.NDR_NO_RESPONSE, trackingNumber2, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber2,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.NDR_NO_RESPONSE.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber2, NDRStatus.NDR_NO_RESPONSE, ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());


        //Update Order NDR status NDR_NO_RESPONSE To RTO is allowed for tracking Number2
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber2);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.TRY_AND_BUY);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");

        //NDR_NO_RESPONSE to REQueue is allowed at this point for packetId2.
        TripOrderAssignmentResponse response = lmsServiceHelper.requeueOrder(packetId1);
        Assert.assertEquals(response.getStatus().getStatusMessage(), "Success");
        Assert.assertEquals(response.getStatus().getStatusType().toString(), "SUCCESS");
        MLShipmentResponse mlShipmentResponse1 = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(trackingNumber1);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber1,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.NDR_NO_RESPONSE.toString());
        Assert.assertEquals(mlShipmentResponse1.getMlShipmentEntries().get(0).getNdrStatus().name(), NDRStatus.NDR_NO_RESPONSE.toString(), "After REQueue NDR status should be NDR_NO_RESPONSE");

    }


    /*
       CUST_NO_RESPONSE- All transitions will be allowed
    */
    @Test(groups={"Sanity"}, priority=2, enabled=true,description="TC :-CUST_NO_RESPONSE- All transitions will be allowed\n" +
            "CUST_NO_RESPONSE validation based on the NDR Response,Create Return ,Mark the order as failed PickUp,NDR status should be Null" +
            " Update Order NDR status NULL To IN_PROGRESS." +
            " Verify NDR status In_Progress to CUST_NO_RESPONSE is Possible" +
            " CUST_NO_RESPONSE to Requeue is Possible")
    public void updateNDRStatusForPickUp() throws Exception {
        String trackingNumberForPickup = null;
        String forwardOrderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.toString(), "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(forwardOrderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        com.myntra.returns.response.ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135078", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String returnOrderID = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(returnOrderID);

        //menifest Open Box PickUP
        String pickup_trackingNo = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().equals(ReturnMode.OPEN_BOX_PICKUP), "");
        lmsReturnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lmsReturnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        //assignOrderToTrip for pickup
        trackingNumberForPickup = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();
        LastmileClient client = new LastmileClient();
        client.assignOrderToTrip(tripId, trackingNumberForPickup);

        //Start trip
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        // Update trip with reason
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID), AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE.name(), TripAction.UPDATE.name()).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        tripData.add(dataMap1);
        //Received FP
        lmsServiceHelper.receiveShipment(trackingNumberForPickup);
        //Complete the trip
        lmsServiceHelper.completeTrip(tripData);
        //VALIDATION AFTER FAILED PICKUP
        MLShipmentResponse mlShipmentResponse = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(lmsHelper.getReturnsTrackingNumber.apply(returnID).toString());
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "SUCCESS", "Success", "3");

        //Verify NDR status NUll to In_Progress update shipment status & validate NDR Config
        ShipmentUpdateResponse shipmentUpdateResponse1 = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.IN_PROGRESS, trackingNumberForPickup, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumberForPickup,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse1, trackingNumberForPickup, NDRStatus.IN_PROGRESS, ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLPickupShipmentStatus.FAILED_PICKUP.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());

        //Verify NDR status In_Progress to CUST_NO_RESPONSE is Possible
        ShipmentUpdateResponse shipmentUpdateResponse2 = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.CUST_NO_RESPONSE, trackingNumberForPickup, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumberForPickup,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.CUST_NO_RESPONSE.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse2, trackingNumberForPickup, NDRStatus.CUST_NO_RESPONSE, ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLPickupShipmentStatus.FAILED_PICKUP.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());

        //CUST_NO_RESPONSE to Requeue is Possible
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormat.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumberForPickup);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("failed pickup requeued for another attempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponseBody = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponseBody.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");
        Assert.assertTrue(mlShipmentResponseBody.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");
        lmsReturnHelper.processReturnWithoutNewValidation(returnID.toString(), AttemptReasonCode.PICKED_UP_SUCCESSFULLY.name());

    }


    /*
     *PRE CONDITIONS:- GROOT SHOULD BE UP IN TEST ENVIRONMENT
     * if ndr_config table, is_enable=1 then All tranction will be gone through the Customer Update.
     * if ndr_config table, is_enable=1 and is_rto_blocked=1,then RTO will be work as per Customer Update.
     * if ndr_config table, is_enable=0 and is_rto_blocked=1,then NDR update will not go & RTO will be blocked Based on the attempted reason code
     *Case 1:-After FD,IN_PROGRESS to RTO is Blocked.
     *Case 2:-After FD,IN_PROGRESS to Requeue is possible.
     *Case 3:-After FD,IN_PROGRESS To REATTEMPT is Possible,REATTEMPT to RTO will be Blocked
     *Case 4:-After FD,IN_PROGRESS To REATTEMPT is possible,REATTEMPT to Requeue will be Allowed.
     *Case 5:-After FD,IN_PROGRESS To RTO is Possible,RTO to all transitions will be allowed(Requeue and RTO).
     */

    @Test(groups={"Sanity"}, priority=1, enabled=true,description="TC :-Config Validation" +
            "is_enabled=1,is_rto_blocked=0:-After FD, NDR status should be IN-Progress" +
            "                               NDR status IN_PROGRESS to Requeue is possible" +
            "is_enabled=1,is_rto_blocked=1:-After FD, NDR status should be IN-Progress" +
            "                               NDR status IN_PROGRESS to RTO is Blocked" +
            "                               NDR status IN_PROGRESS To REATTEMPT" +
            "                               REATTEMPT to RTO will be Blocked" +
            "                               REATTEMPT to Requeue will be Allowed" +
            "is_enabled=1,is_rto_blocked=0:-After FD, NDR status should be IN-Progress" +
            "                               Update Order NDR status IN_PROGRESS To RTO is allowed")
    public void UpdateNDRStatusConfig() throws Exception {
        String trackingNumber1 = null;
        String trackingNumber2 = null;
        String trackingNumber3 = null;
        String packetId1 = null;
        String packetId2 = null;
        String packetId3 = null;
        packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        log.debug("packetId 1:-" + packetId1 + ",PacketId 2:-" + packetId2 + "PacketId 3:-" + packetId3);

        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);
        trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + " trackingNumber 3:-" + trackingNumber3);

        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId2)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.toString(), 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString(), 2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.REFUSED_TO_ACCEPT);

        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);

        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), (AttemptReasonCode) dataMap1.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId1, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), (AttemptReasonCode) dataMap2.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId2, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), (AttemptReasonCode) dataMap3.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId3, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");

        DBUtilities.exUpdateQuery(
                "update ndr_config set is_enabled=1,is_rto_blocked=0 where service_type='DELIVERY'and attempt_reason_code='REFUSED' and client_id=2297;", "lms");
        DBUtilities.exUpdateQuery(
                "update ndr_config set is_enabled=1,is_rto_blocked=1 where service_type='DELIVERY'and attempt_reason_code='CUSTOMER_UNREACHABLE' and client_id=2297;", "lms");
        DBUtilities.exUpdateQuery(
                "update ndr_config set is_enabled=1,is_rto_blocked=0 where service_type='DELIVERY'and attempt_reason_code='INCOMPLETE_ADDRESS' and client_id=2297;", "lms");

        //validation: After failed Delivery NDR status should be IN-Progress status
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber1,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateShipmentThroughNDRConfig(packetId1, "After FD packet1, NDR status should be In-Progress");

        // checking After FD (packet2), NDR status should be IN-Progress status
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber2,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateShipmentThroughNDRConfig(packetId2, "After FD packet2, NDR status should be In-Progress");

        // checking After FD (packet3) NDR status should be IN-Progress status
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber3,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.IN_PROGRESS.toString());
        NDRValidator.ValidateShipmentThroughNDRConfig(packetId3, "After FD packet3, NDR status should be In-Progress");

        //Received FD and Complete the trip
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId1));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.completeTrip(tripData);

        //TCs:- Update Order NDR status IN_PROGRESS to RTO is Blocked for tracking Number1
        //Ndr_config:- is_enable=1,is_rto_blocked=0,attempt_reason_code=REFUSED
        MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.DL, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber2);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber2, null);

        //TCs:- Update Order NDR status IN_PROGRESS to Requeue is possible for tracking Number1
        TripOrderAssignmentResponse response = lmsServiceHelper.requeueOrder(packetId1);
        Assert.assertEquals(response.getStatus().getStatusMessage(), "Success");
        Assert.assertEquals(response.getStatus().getStatusType().toString(), "SUCCESS");
        MLShipmentResponse mlShipmentResponse1 = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(trackingNumber1);
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //Update Order NDR status IN_PROGRESS To REATTEMPT for tracking Number2
        //Ndr_config:- is_enable=1,is_rto_blocked=0,attempt_reason_code=CUSTOMER_UNREACHABLE
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.REATTEMPT, trackingNumber2, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber2,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.REATTEMPT.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber2, NDRStatus.REATTEMPT, ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());

        //REATTEMPT to RTO will be Blocked
        MLShipmentResponse mlShipmentResponse2 = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.DL, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber2);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse2, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber2, null);

        //REATTEMPT to Requeue will be Allowed
        TripOrderAssignmentResponse response1 = lmsServiceHelper.requeueOrder(packetId2);
        Assert.assertEquals(response1.getStatus().getStatusMessage(), "Success");
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), "SUCCESS");
        MLShipmentResponse mlShipmentDetailsByTrackingNumber = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(trackingNumber2);
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        Assert.assertEquals(mlShipmentDetailsByTrackingNumber.getMlShipmentEntries().get(0).getNdrStatus().name(), NDRStatus.REATTEMPT.toString(), "After REQueue NDR status should be REATTEMPT");

        //Update Order NDR status IN_PROGRESS To RTO for tracking Number3
        //Ndr_config:- is_enable=1,is_rto_blocked=0,attempt_reason_code=INCOMPLETE_INCORRECT_ADDRESS
        shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.RTO, trackingNumber3, LASTMILE_CONSTANTS.TENANT_ID);
        statusPollingValidator.validateML_ShipmentNDRStatus(trackingNumber3,LASTMILE_CONSTANTS.TENANT_ID, NDRStatus.RTO.toString());
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumber3, NDRStatus.RTO, ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());

        //RTO:- All transitions will be allowed(Requeue is possible)
        TripOrderAssignmentResponse response2 = lmsServiceHelper.requeueOrder(packetId3);
        Assert.assertEquals(response2.getStatus().getStatusMessage(), "Success");
        Assert.assertEquals(response2.getStatus().getStatusType().toString(), "SUCCESS");
        MLShipmentResponse mlShipmentDetailsByTrackingNumber1 = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(trackingNumber3);
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        Assert.assertEquals(mlShipmentDetailsByTrackingNumber1.getMlShipmentEntries().get(0).getNdrStatus().name(), NDRStatus.RTO.toString(), "After REQueue NDR status should be RTO");
        //NDR Bulk Download
        BulkJobResponse bulkJobResponse = NDRServiceHelper.ndrCreateBulkJob(LMS_CONSTANTS.NDR.NAME, LMS_CONSTANTS.NDR.PAYLOAD_FORWARD + "," + DateTimeHelper.generateDate("YYYY-MM-dd",-2) + "," + DateTimeHelper.generateDate("YYYY-MM-dd",0), BulkJobType.BULK_NDR_3PL_REPORT_DOWNLOAD, LMS_CONSTANTS.TENANTID);
        Long bulkJobId = bulkJobResponse.getBulkJobEntryList().get(0).getId();
        Assert.assertEquals(bulkJobResponse.getStatus().getStatusMessage(), "Bulk Job Created");
        BulkJobResponse responseStatus=NDRServiceHelper.validate_BulkJob_Status(bulkJobId);
        Assert.assertEquals(responseStatus.getStatus().getStatusMessage().toString(),"ORDER(s) retrieved successfully");
        statusPollingValidator.validate_BulkJob_Status(bulkJobId,"COMPLETE");
        S3FileResponse s3FileResponse = NDRServiceHelper.getNDRDownloadURL(bulkJobId);
        Assert.assertEquals(s3FileResponse.getStatus().getStatusCode(), 3);
        String url = s3FileResponse.getData().getUrl();
        List<String> lstValue = LMSDownloadHelper.downloadFileFromAmazonurl(url);

    }


}

