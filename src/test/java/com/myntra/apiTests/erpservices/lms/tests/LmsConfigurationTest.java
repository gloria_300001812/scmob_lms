package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.erpservices.lms.DataHelper.ConfigurationHelper;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lms.client.response.HubResponse;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

/*
Written by Bairagi Muduli
 */

@Slf4j
public class LmsConfigurationTest {
    ConfigurationHelper configurationHelper=new ConfigurationHelper();

    String tenantId="4019";
//    String hubCode=RandomStringUtils.randomAlphabetic(3);
    String hubCode="BAM";
    long pincode=110015;

    @Test
    public void testConfig() throws IOException {
        configurationHelper.addShipment("4019","26851727");
    }

    @Test
    public
    void testLmsConfig() throws IOException, JAXBException, JSONException, XMLStreamException {
        final HubResponse hubResponse = configurationHelper.transportHub(tenantId, hubCode, pincode);
        final String thCode = hubResponse.getHub().get(0).getCode();

        final HubResponse hubResponse1 = configurationHelper.dispatchHub(tenantId, hubCode, pincode);
        final String dhCode = hubResponse1.getHub().get(0).getCode();

        final HubResponse hubResponse2 = configurationHelper.returnHub(tenantId, hubCode, pincode);
        final String rhCode = hubResponse2.getHub().get(0).getCode();

        final DeliveryCenterResponse deliveryCenterResponse = configurationHelper.deliveryCenter(tenantId, hubCode, pincode);
        final String statusMessage = deliveryCenterResponse.getStatus().getStatusMessage();
        Assert.assertEquals(statusMessage,"DELIVERY_CENTER added successfully","Might be Duplicate entry 'pincode-tenantId' for key 'uk_delivery_center_pincode_tenant'");
        configurationHelper.addLane("BLR-"+hubCode,hubCode,thCode,tenantId);
    }


    @Test
    public void testName() throws IOException, JAXBException, JSONException, XMLStreamException {
//        configurationHelper.deliveryStaff("CRN",8613);
        configurationHelper.addLane("kuch bhi","TH-BLR","THCRN",tenantId);
    }
}
