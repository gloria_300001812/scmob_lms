package com.myntra.apiTests.erpservices.lms.tests;
/**
 * Author:-Shuvendu Kumar Dash
 * Script Description:-NDR AUTOMATION
 */

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.client.TripOrderAssignmentClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.service.DeliveryCenterClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.client.MLShipmentClient;
import com.myntra.apiTests.erpservices.lms.validators.NDRValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.MLPickupShipmentStatus;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShipmentSourceEnum;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lms.client.status.TripAction;
import com.myntra.logistics.platform.domain.NDRStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateAdditionalInfo;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.test.commons.testbase.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lms.tests.LastMileTestDP.deliveryCenterClient;

public class NDRTest extends BaseTest {
    String env = getEnvironment();
    LMSHelper lmsHelper = new LMSHelper();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    HashMap<String, String> pincodeDCMap;
    public static String DcId;
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private OrderEntry orderEntry = new OrderEntry();
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NDRTest.class);
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();

    //creating HashMap to maintain pincode and DcId
    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
    }

    /**
     * ***************************FORWARD FLOW***************************
     */
    @Test(groups = {"NDR", "Smoke", "Regression"}, priority = 1, description = "ID: TC1,updateTripWithMultiOrderNmultiStatus - create order till SH,Mark the order FD, Check the status In Progress, FD order Received and Complete the trip",
            enabled = true)
    public void updateShipmentinDifferentStatus() throws Exception {
        String trackingNumber1 = null;
        String trackingNumber2 = null;
        String trackingNumber3 = null;
        String packetId1 = null;
        String packetId2 = null;
        String packetId3 = null;
        packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        log.debug("packetId 1:-" + packetId1 + ",PacketId 2" + packetId2 + "PacketId 3" + packetId3);

        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);
        trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + " trackingNumber 3:-" + trackingNumber3);

        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = Long.parseLong(lmsServiceHelper.getDeliveryStaffID("" + DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId2)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.toString(), 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.toString(), 2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);

        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);

        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), (AttemptReasonCode) dataMap1.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId1, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), (AttemptReasonCode) dataMap2.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId2, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), (AttemptReasonCode) dataMap3.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId3, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");

        //validation: After Delivery NDR status should be NULL
        NDRValidator.ValidateNDRStatusForShipment(packetId1, "After Delivery NDR status should be NULL");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet2), NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId2, "After FD packet2, NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet3) NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId3, "checking After FD packet3 NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //Received FD and Complete the trip
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.completeTrip(tripData);

        //Update Order NDR status NULL To IN_PROGRESS for tracking Number2
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.IN_PROGRESS, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber2);
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        NDRValidator.ValidateShipmentUpdateResponse(shipmentUpdateResponse, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumber2, NDRStatus.IN_PROGRESS, ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name());

        //Update Order NDR status Null To IN_PROGRESS for tracking Number3
        shipmentUpdateResponse = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.IN_PROGRESS, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber3);
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        NDRValidator.ValidateShipmentUpdateResponse(shipmentUpdateResponse, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumber3, NDRStatus.IN_PROGRESS, ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name());

        //Verify that, IN_PROGRESS to RTO is blocked for Tracking Number2
        MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.DL, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber2);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber2, null);

        //IN_PROGRESS to REQueue is allowed at this point.
        TripOrderAssignmentResponse response = lmsServiceHelper.requeueOrder(packetId2);
        Assert.assertEquals(response.getStatus().getStatusMessage(), "Success");
        Assert.assertEquals(response.getStatus().getStatusType().toString(), "SUCCESS");

        //Verify IN_PROGRESS to REATTEMPT is allowed for FD packet.
        shipmentUpdateResponse = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.REATTEMPT, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber3);
        NDRValidator.ValidateShipmentUpdateResponse(shipmentUpdateResponse, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumber3, NDRStatus.REATTEMPT, ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name());
    }

    /**
     * *********************REVERSE FLOW *********************************************
     */
    @Test(groups = {"NDR", "P0", "Smoke", "Regression"}, priority = 2, description = "ID: C2 , FP Scenario", enabled = true)
    public void updateShipmentinDifferentStatusForPickUp() throws Exception {
        String trackingNumberForPickup = null;
        String forwardOrderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.toString(), "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(forwardOrderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        com.myntra.returns.response.ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135078", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String returnOrderID = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(returnOrderID);

        //menifest Open Box PickUP
        String pickup_trackingNo = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().equals(ReturnMode.OPEN_BOX_PICKUP), "");
        lmsReturnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lmsReturnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = Long.parseLong(lmsServiceHelper.getDeliveryStaffID("" + DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        //assignOrderToTrip for pickup
        trackingNumberForPickup = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();
        LastmileClient client = new LastmileClient();
        client.assignOrderToTrip(tripId, trackingNumberForPickup);

        //Start trip
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        // Update trip with reason
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID), AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE.name(), TripAction.UPDATE.name()).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        tripData.add(dataMap1);
        //Received FP
        lmsServiceHelper.receiveShipment(trackingNumberForPickup);
        //Complete the trip
        lmsServiceHelper.completeTrip(tripData);
        //VALIDATION AFTER FAILED PICKUP
        MLShipmentResponse mlShipmentResponse = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(lmsHelper.getReturnsTrackingNumber.apply(returnID).toString());
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "SUCCESS", "Success", "3");

        //Verify NDR status NUll to In_Progress update shipment status & validate NDR Config
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.IN_PROGRESS,
                LASTMILE_CONSTANTS.TENANT_ID, trackingNumberForPickup);
        NDRValidator.ValidateShipmentUpdateResponse(shipmentUpdateResponse, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumberForPickup, NDRStatus.IN_PROGRESS, ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLPickupShipmentStatus.FAILED_PICKUP.name());

        //Verify NDR status In_Progress to RTO should be blocked
        com.myntra.lastmile.client.entry.MLShipmentResponse ml_ShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.PU, LASTMILE_CONSTANTS.TENANT_ID, trackingNumberForPickup);
        NDRValidator.ValidateMLShipmentResponse(ml_ShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumberForPickup, null);

        //Verify NDR status In_Progress to REATTEMPT is Possible
        ShipmentUpdateResponse ShipmentUpdateResponse1 = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.REATTEMPT,
                LASTMILE_CONSTANTS.TENANT_ID, trackingNumberForPickup);
        NDRValidator.ValidateShipmentUpdateResponse(ShipmentUpdateResponse1, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumberForPickup, NDRStatus.REATTEMPT, ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLPickupShipmentStatus.FAILED_PICKUP.name());
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //Reattempt to Requeue is Possible
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormat.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumberForPickup);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("failed pickup requeued for another attempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponseBody = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponseBody.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");
        Assert.assertTrue(mlShipmentResponseBody.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");
        lmsReturnHelper.processReturnWithoutNewValidation(returnID.toString(), AttemptReasonCode.PICKED_UP_SUCCESSFULLY.name());
    }


    /**
     * Try and Buy Flow
     */
    @Test(groups = {"NDR", "P0", "Smoke", "Regression"}, priority = 3, description = "ID: C3 ,Try&Buy Scenario", enabled = true)
    public void updateShipmentinDifferentStatusTandB() throws Exception {
        String trackingNumber1 = null;
        String trackingNumber2 = null;
        String trackingNumber3 = null;
        String packetId1 = null;
        String packetId2 = null;
        String packetId3 = null;
        packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        log.debug("packetId 1:-" + packetId1 + ",PacketId 2" + packetId2 + "PacketId 3" + packetId3);

        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);
        trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        //log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + " trackingNumber 3:-" + trackingNumber3);

        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = Long.parseLong(lmsServiceHelper.getDeliveryStaffID("" + DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId2)).getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name(), "Unable to assignOrderToTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.name(), 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.name());
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name(), 2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.REQUESTED_RE_SCHEDULE);

        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);

        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), (AttemptReasonCode) dataMap1.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId1, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), (AttemptReasonCode) dataMap2.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId2, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), (AttemptReasonCode) dataMap3.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId3, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");

        log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + ",trackingNumber3:- " + trackingNumber3);

        //checking After FD (packet1), NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId1, "After Delivery NDR status should be NULL");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet2), NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId2, "After FD packet2, NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        // checking After FD (packet3) NDR status should be NULL status
        NDRValidator.ValidateNDRStatusForShipment(packetId3, "checking After FD packet3 NDR status should be NULL status");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //Received FAILED T & B and Complete the trip
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId1));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.completeTrip(tripData);

        //Update Order NDR status NULL To IN_PROGRESS for tracking Number1
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.IN_PROGRESS, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber1);
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        NDRValidator.ValidateShipmentUpdateResponse(shipmentUpdateResponse, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumber1, NDRStatus.IN_PROGRESS, ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name());

        //Update Order NDR status Null To IN_PROGRESS for tracking Number3
        shipmentUpdateResponse = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.IN_PROGRESS, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber3);
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        NDRValidator.ValidateShipmentUpdateResponse(shipmentUpdateResponse, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumber3, NDRStatus.IN_PROGRESS, ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name());

        //Verify that, IN_PROGRESS to RTO is blocked for Tracking Number2
        MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber2);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber2, null);

        //IN_PROGRESS to REQueue is allowed at this point.
        TripOrderAssignmentResponse response = lmsServiceHelper.requeueOrder(packetId2);
        Assert.assertEquals(response.getStatus().getStatusMessage(), "Success");
        Assert.assertEquals(response.getStatus().getStatusType().toString(), "SUCCESS");

        //Verify IN_PROGRESS to REATTEMPT is allowed for FD packet.
        shipmentUpdateResponse = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.REATTEMPT, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber3);
        NDRValidator.ValidateShipmentUpdateResponse(shipmentUpdateResponse, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumber3, NDRStatus.REATTEMPT, ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name());
    }

    /**
     * Create Exchage till SH status Mark all Shipment as Failed Shipment and validate NDR status
     */
    @Test(groups = {"NDR", "Smoke", "Regression"}, priority = 4, description = "ID: TC4 ,updateShipmentinDifferentStatusExchange", enabled = true)
    public void updateShipmentinDifferentStatusExchange() throws Exception {
        List<String> al = new ArrayList();
        List<String> trackingNumberList = new ArrayList<>();
        List<AttemptReasonCode> attemptReasonCodes = Arrays.asList(AttemptReasonCode.REQUESTED_RE_SCHEDULE, AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS, AttemptReasonCode.REFUSED_TO_ACCEPT, AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        for (int i = 0; i <= 4; i++) {
            LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
            String orderIDParent = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true);
            String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true, omsServiceHelper.getReleaseId(orderIDParent));
            String packetId = omsServiceHelper.getPacketId(exchangeOrder);
            String trackingNumber = lmsHelper.getTrackingNumber(packetId);
            al.add(packetId);
            trackingNumberList.add(trackingNumber);
        }
        //create trip
        Long tripId = createTrip();
        List<Map<String, Object>> tripData = new ArrayList<>();

        //addShipmentToTrip
        Iterator<String> trackingNoItr = trackingNumberList.iterator();
        while (trackingNoItr.hasNext()) {
            addShipmentToTrip(tripId, trackingNoItr.next());
        }

        //startTrip
        startTrip(tripId);

        // Add the attempt reson code
        Iterator<String> packetItr = al.iterator();
        trackingNoItr = trackingNumberList.iterator();
        Iterator<AttemptReasonCode> reasonsItr = attemptReasonCodes.iterator();
        while (packetItr.hasNext()) {
            String packetId = packetItr.next();
            String trackingNumber = trackingNoItr.next();
            AttemptReasonCode reasonCode = reasonsItr.next();
            Map<String, Object> tripOrder = process(packetId, trackingNumber, reasonCode, tripId);
            tripData.add(tripOrder);

        }

        //RECEIVE Shipment
        trackingNoItr = al.iterator();
        while (trackingNoItr.hasNext()) {
            lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(trackingNoItr.next()));
        }

        //Complete trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.completeTrip(tripData);
        log.info("TripOrderAssignmentResponse=" + tripOrderAssignmentResponse.getStatus().getStatusMessage());

        //checking After FD, NDR status should be NULL status
        trackingNoItr = trackingNumberList.iterator();
        while (trackingNoItr.hasNext()) {
            verifyAndUpdateNdrStatusAfterFD(trackingNoItr.next());
        }

        verifyInProgressToRtoBlockedAndRequeue(al.get(1), trackingNumberList.get(1));
        verifyInProgressToReattemptIsAllowed(trackingNumberList.get(2));
        //varify config:- IS_Enable '0' and Is_RTO Blocked '1' RTO should be blocked.
        varifyNDRConfig2(trackingNumberList.get(3));
        //varify config:- IS_Enable '0' and Is_RTO Blocked '0' RTO should not be blocked.
        varifyNDRConfig1(trackingNumberList.get(4));
    }

    private Long createTrip() {
        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);

        //Create Trip
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("Trip Number is:- " + tripNumber);
        return tripId;
    }

    private void startTrip(Long tripId) {
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to startTrip");
    }

    private void addShipmentToTrip(Long tripId, String trackingNumber) throws WebClientException {
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
    }

    private Map<String, Object> process(String packetId, String trackingNumber, AttemptReasonCode attemptReasonCode, Long tripId) throws WebClientException, IOException, JAXBException {

        //Start trip
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name(), 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, attemptReasonCode, TripAction.UPDATE.name(), packetId, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");

        //MarK orers as Failed Exchange
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        dataMap.put("AttemptReasonCode", attemptReasonCode);
        return dataMap;
    }

    private void verifyAndUpdateNdrStatusAfterFD(String trackingNumber) throws Exception {
        MLShipmentResponse mlShipmentResponse1 = new MLShipmentClient_QA().getMLShipmentDetailsByTrackingNumber(trackingNumber);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse1, "SUCCESS", "Success", "3");
        Assert.assertNull(mlShipmentResponse1.getMlShipmentEntries().get(0).getNdrStatus(), "After Delivery NDR status should be NULL");
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        //Update Order NDR status NULL To IN_PROGRESS for tracking Number
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.EXCHANGE, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.IN_PROGRESS, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        NDRValidator.ValidateShipmentUpdateResponse(shipmentUpdateResponse, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumber, NDRStatus.IN_PROGRESS, ShipmentType.EXCHANGE, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name());
        Thread.sleep(Constants.LMS_PATH.sleepTime);
    }

    private void verifyInProgressToRtoBlockedAndRequeue(String packetId, String trackingNumber) throws Exception {
        log.debug("RTO Blocked for Tracking Number As per Config :- " + trackingNumber);
        //Verify that, IN_PROGRESS to RTO is blocked
        com.myntra.lastmile.client.entry.MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.EXCHANGE, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber, null);

        //IN_PROGRESS to REQueue is allowed at this point.
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponses = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponses.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");
    }

    private void verifyInProgressToReattemptIsAllowed(String trackingNumber) throws Exception {
        //Verify IN_PROGRESS to REATTEMPT is allowed for FD packet.
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.EXCHANGE, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.REATTEMPT, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        NDRValidator.ValidateShipmentUpdateResponse(shipmentUpdateResponse, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumber, NDRStatus.REATTEMPT, ShipmentType.EXCHANGE, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name());
    }

    private void varifyNDRConfig1(String trackingNumber) throws Exception {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("initiating RTO for shipment");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");
    }

    private void varifyNDRConfig2(String trackingNumber) throws Exception {
        com.myntra.lastmile.client.entry.MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.EXCHANGE, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber, null);

    }

    /**
     * NDR Config TestCase
     * IS_Enable '0' and Is_RTO Blocked '1' RTO should be blocked.
     * IS_Enable '1' and Is_RTO Blocked '1' Check NDR status.
     * IS_Enable '0' and Is_RTO Blocked '0' RTO should not be blocked.
     */
    @Test(groups = {"NDR", "Smoke", "Regression"}, priority = 5, description = "ID: TC5 ,NDR Config TestCase", enabled = true)
    public void rtoConfigForDeliveryOrder() throws Exception {
        String trackingNumber1 = null;
        String trackingNumber2 = null;
        String trackingNumber3 = null;
        String packetId1 = null;
        String packetId2 = null;
        String packetId3 = null;
        packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true));
        log.debug("packetId 1:-" + packetId1 + ",PacketId 2" + packetId2 + "PacketId 3" + packetId3);

        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);
        trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + " trackingNumber 3:-" + trackingNumber3);

        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = Long.parseLong(lmsServiceHelper.getDeliveryStaffID("" + DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId2)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.name(), 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name(), 2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.REFUSED_TO_ACCEPT);

        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.CASH_CARD_NOT_READY);

        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), (AttemptReasonCode) dataMap1.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId1, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), (AttemptReasonCode) dataMap2.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId2, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), (AttemptReasonCode) dataMap3.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId3, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        //Received FD and Complete the trip
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId1));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.completeTrip(tripData);

        //RTO config validation:-IS_Enable '0' and Is_RTO Blocked '1' RTO(trackingNumber2) should be blocked.
        com.myntra.lastmile.client.entry.MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.DL, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber2);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber2, null);

        //RTO config validation:-IS_Enable '0' and Is_RTO Blocked '0' RTO(trackingNumber3) should not be blocked.
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber3);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.DL);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");

        //RTO config validation:-IS_Enable '1' and Is_RTO Blocked '1' Check NDR status.
        //After FD NDR status should be IN_PROGRESS for tracking Number1
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.IN_PROGRESS, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber1);
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        NDRValidator.ValidateShipmentUpdateResponse(shipmentUpdateResponse, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumber1, NDRStatus.IN_PROGRESS, ShipmentType.DL, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name());
    }

    @Test(groups = {"NDR", "P0", "Smoke", "Regression"}, priority = 6, description = "ID: C6 ,rtoConfigForPickUp", enabled = true)
    public void rtoConfigForPickUp() throws Exception {
        String trackingNumberForPickup = null;
        String forwardOrderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(forwardOrderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        com.myntra.returns.response.ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135078", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String returnOrderID = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(returnOrderID);
        //menifest Open Box PickUP
        String pickup_trackingNo = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().equals(ReturnMode.OPEN_BOX_PICKUP), "");
        lmsReturnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lmsReturnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = Long.parseLong(lmsServiceHelper.getDeliveryStaffID("" + DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        //assignOrderToTrip for pickup
        trackingNumberForPickup = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();
        LastmileClient client = new LastmileClient();
        client.assignOrderToTrip(tripId, trackingNumberForPickup);

        //Start trip
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        // Update trip with reason
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID), EnumSCM.FAILED, TripAction.UPDATE.name()).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.HAPPY_WITH_PRODUCT);
        tripData.add(dataMap1);
        //Received FP
        lmsServiceHelper.receiveShipment(trackingNumberForPickup);
        //Complete the trip
        lmsServiceHelper.completeTrip(tripData);

        //NDR config Validation:-IS_Enable '0' and Is_RTO Blocked '1' RTO should be blocked.
        com.myntra.lastmile.client.entry.MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.DL, LASTMILE_CONSTANTS.TENANT_ID, trackingNumberForPickup);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumberForPickup, null);
    }

    @Test(groups = {"NDR", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C7 ,rtoConfigForTryandBuy Scenario", enabled = true)
    public void rtoConfigForTryandBuy() throws Exception {
        String trackingNumber1 = null;
        String trackingNumber2 = null;
        String trackingNumber3 = null;
        String packetId1 = null;
        String packetId2 = null;
        String packetId3 = null;
        packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.COURIER_CODE_ML, LASTMILE_CONSTANTS.WAREHOUSE_36, ShippingMethod.NORMAL.name(), "cod", true, true));
        log.debug("packetId 1:-" + packetId1 + ",PacketId 2" + packetId2 + "PacketId 3" + packetId3);

        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);
        trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        //log.debug("trackingNumber1:- " + trackingNumber1 + ",trackingNumber2:- " + trackingNumber2 + " trackingNumber 3:-" + trackingNumber3);

        //Get SDA
        DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = Long.parseLong(lmsServiceHelper.getDeliveryStaffID("" + DcId));

        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        log.debug("trip number is :- " + tripNumber);
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId2)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.name(), 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType(), StatusResponse.Type.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name(), 2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.REFUSED_TO_ACCEPT);

        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.CASH_CARD_NOT_READY);

        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), (AttemptReasonCode) dataMap1.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId1, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), (AttemptReasonCode) dataMap2.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId2, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), (AttemptReasonCode) dataMap3.get("AttemptReasonCode"), TripAction.UPDATE.name(), packetId3, tripId, orderEntry).getStatus().getStatusType(), StatusResponse.Type.SUCCESS, "Unable to update orders in Trip.");

        //Received FAILED T & B and Complete the trip
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId1));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId2));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3));
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        lmsServiceHelper.completeTrip(tripData);

        //RTO config validation:-IS_Enable '0' and Is_RTO Blocked '1' RTO(trackingNumber2) should be blocked.
        com.myntra.lastmile.client.entry.MLShipmentResponse mlShipmentResponse = lmsServiceHelper.updateShipmentRTOTripResult(ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber2);
        NDRValidator.ValidateMLShipmentResponse(mlShipmentResponse, "ERROR", "Delivery center in shipmentUpdate: null does not match with delivery center: " + DcId + " associated with shipment trackingNumber: " + trackingNumber2, null);

        //RTO config validation:-IS_Enable '0' and Is_RTO Blocked '0' RTO(trackingNumber3) should not be blocked.
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber3);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.TRY_AND_BUY);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(StatusResponse.Type.SUCCESS.name()), "Status not updated successfully");

        //RTO config validation:-IS_Enable '1' and Is_RTO Blocked '1' Check NDR status.
        //After FD NDR status should be IN_PROGRESS for tracking Number1
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateShipmentStatusNDR(ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, ShipmentSourceEnum.MYNTRA.name(), NDRStatus.IN_PROGRESS, LASTMILE_CONSTANTS.TENANT_ID, trackingNumber1);
        Thread.sleep(Constants.LMS_PATH.sleepTime);
        NDRValidator.ValidateShipmentUpdateResponse(shipmentUpdateResponse, MLShipmentUpdateEvent.NDR_UPDATE.name(), ShipmentSourceEnum.MYNTRA.name(), trackingNumber1, NDRStatus.IN_PROGRESS, ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLDeliveryShipmentStatus.FAILED_DELIVERY.name());
    }
}