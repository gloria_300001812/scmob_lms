package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.logistics.platform.domain.Premise;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnActionCode;
import com.myntra.returns.response.ReturnResponse;
import lombok.SneakyThrows;
import org.apache.maven.shared.utils.StringUtils;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.logistics.platform.domain.Premise.PremiseType.DELIVERY_CENTER;
import static com.myntra.logistics.platform.domain.Premise.PremiseType.HUB;

public class CloseBoxReturnTest {
    LMS_ReturnHelper lmsReturnHelper=new LMS_ReturnHelper();
    private static org.slf4j.Logger log=LoggerFactory.getLogger(LMS_Reverse_Exchange.class);
    private OMSServiceHelper omsServiceHelper=new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper=new RMSServiceHelper();
    private LMSHelper lmsHepler=new LMSHelper();
    LMS_CloseBoxReturnHelper lms_closeBoxReturnHelper=new LMS_CloseBoxReturnHelper();
    LMS_ReturnHelper lms_returnHelper=new LMS_ReturnHelper();
    StatusPollingValidator statusPollingValidator=new StatusPollingValidator();
    StoreHelper storeHelper=new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);

    private ReturnHelper returnHelper=new ReturnHelper();
    LMS_OpenBoxReturnHelper lms_openBoxReturnHelper=new LMS_OpenBoxReturnHelper();
    @BeforeSuite
    public void setEnv( ) {
        String env=getEnvironment();
    }


    @Test( retryAnalyzer=Retry.class,enabled=true,
            description="C9801 Closed Box Flow : One Pickup 3 returns -1 QC Pass, 1 On Hold - Approval, 1 Shortage, Received at DC")
    public void onePickup_3Return_1QCPass_1OnHoldApproval_1Shortage_AllReceivedAtDC( )
            throws Exception {

        //Before creating any returns, manifest any old Closed Box return so that it does not go into this same pickup

        lmsReturnHelper.manifestClosedBoxPickups();
        Thread.sleep(5000);

        final List <String> returnIds=placeOrderCreateReturn(3);
        final String returnId1=returnIds.get(0);
        final String returnId2=returnIds.get(1);
        final String returnId3=returnIds.get(2);
        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(returnId1,"DE");
        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(returnId2,"DE");
        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(returnId3,"DE");

        lmsReturnHelper.manifestClosedBoxPickups();
        Thread.sleep(5000);


        Assert.assertTrue(lmsReturnHelper.isPickupManifested(returnId1),
                "Pickup associated with return - "+returnId1+" is NOT manifested");
        Assert.assertTrue(lmsReturnHelper.isPickupManifested(returnId2),
                "Pickup associated with return - "+returnId2+" is NOT manifested");
        Assert.assertTrue(lmsReturnHelper.isPickupManifested(returnId3),
                "Pickup associated with return - "+returnId3+" is NOT manifested");

        Assert.assertTrue(lmsReturnHelper.verifyClosedBoxConsolidation(returnIds),
                "The returns are not consolidated into ONE pickup");

        lms_closeBoxReturnHelper.pickupSuccessFullyClosedBox(returnId1,ItemQCStatus.PASSED);

    }

    @Test( retryAnalyzer=Retry.class,enabled=true,
            description="C9802 Closed Box Flow : One Pickup 3 returns -1 QC Pass, 1 On Hold - Reject, 1 Shortage receive at Returns Hub")
    public void onePickup_3Return_1QCPass_1OnHoldReject_1Shortage_AllReceivedAtDC( )
            throws Exception {

        //Before creating any returns, manifest any old Closed Box return so that it does not go into this same pickup
        for (int i=0; i < 10; i++)
             lmsReturnHelper.manifestClosedBoxPickups();
        Thread.sleep(5000);

        final List <String> returnIds=placeOrderCreateReturn(3);
        final String returnId1=returnIds.get(0);
        final String returnId2=returnIds.get(1);
        final String returnId3=returnIds.get(2);

        lmsReturnHelper.manifestClosedBoxPickups();
        Thread.sleep(5000);

        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(String.valueOf(returnId1),"DE");
        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(String.valueOf(returnId2),"DE");
        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(String.valueOf(returnId3),"DE");

        lmsReturnHelper.manifestClosedBoxPickups();
        Thread.sleep(5000);

        Assert.assertTrue(lmsReturnHelper.isPickupManifested(String.valueOf(returnId1)),
                "Pickup associated with return - "+returnId1+" is NOT manifested");
        Assert.assertTrue(lmsReturnHelper.isPickupManifested(String.valueOf(returnId2)),
                "Pickup associated with return - "+returnId2+" is NOT manifested");
        Assert.assertTrue(lmsReturnHelper.isPickupManifested(String.valueOf(returnId3)),
                "Pickup associated with return - "+returnId3+" is NOT manifested");

        Assert.assertTrue(lmsReturnHelper.verifyClosedBoxConsolidation(returnIds),
                "The returns are not consolidated into ONE pickup");

        lms_closeBoxReturnHelper.pickupSuccessFullyClosedBox(returnId1,ItemQCStatus.PASSED);
    }


    @Test(retryAnalyzer=Retry.class,  enabled=false,
            description="C9803 Closed Box Flow : Verify Shortage Flow , return should be retried until customer wants")
    public void verifyShortageFlow( ) throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);
        lmsReturnHelper.manifestClosedBoxPickups();

        Assert.assertTrue(lmsReturnHelper.isPickupManifested(String.valueOf(returnId)),
                "Pickup associated with return - "+returnId+" is NOT manifested");

        Assert.assertTrue(lmsReturnHelper.verifyClosedBoxConsolidation(returnIds),
                "The returns are not consolidated into ONE pickup");

        lms_closeBoxReturnHelper.processClosedBoxPickup(returnId,EnumSCM.PICKED_UP_SHORTAGE,
                EnumSCM.RECEIVED_IN_RETURNS_HUB);
    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9804 Closed Box Flow : Manifest api validation")
    public void manifestApiValidation( ) throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();

        Assert.assertTrue(lmsReturnHelper.isPickupManifested(returnId),
                "Pickup associated with return - "+returnId+" is NOT manifested");
        Thread.sleep(5000);

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9805 Closed Box :Failed Pickup Flow")
    public void failedPickup( ) throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP,"");
        Assert.assertTrue(lms_returnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP_STATUS,15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9806 Upload Negative Scenario : PICKUP_CREATED -> PICKUP_IN_TRANSIT not allowed")
    public void pickupCreatedPickupInTransitNotAllowed( ) throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        Assert.assertTrue(
                lms_returnHelper.validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.OUT_FOR_PICKUP.toString(),
                        15));
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,"");
        Assert.assertFalse(
                lms_returnHelper.validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.PICKUP_CREATED.toString(),
                        15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9807 Upload Negative Scenario : PICKUP_CREATED -> PICKUP_SUCCESSFUL not allowed")
    public void pickupCreatedPickupSuccessfulNotAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        Assert.assertTrue(
                lms_returnHelper.validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.OUT_FOR_PICKUP.toString(),
                        15));
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,"");
        Assert.assertFalse(lms_returnHelper.validatePickupShipmentStatusInLMS(returnId,
                ShipmentStatus.PICKUP_SUCCESSFUL.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9808 Positive Upload Scenarios: PICKUP_CREATED to PICKUP_DONE & OUT_FOR_PICKUP allowed")
    public void pickupCreatedPickupDoneOutForPickup( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9809 Receive Pickup : Should take PICKUP tracking no and not return's tracking number (similarly for pickupId not returnId)")
    public void receivePickupAcceptTrackingNoCheck( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatusSuccessfully(returnId);

        final String returnTrackingNo=lms_closeBoxReturnHelper.getReturnTrackingNo(returnId);

        final String statusMessage=lms_closeBoxReturnHelper.updateReturnReceiveEventsByTrackingNo(returnTrackingNo,
                "DE");
        Assert.assertNotEquals(statusMessage,"Shipment Updation Successful");

        final String pickupTrackingNo=lmsReturnHelper.getPickupTrackingNoOfReturn(returnId);

        final String statusMessage1=lms_closeBoxReturnHelper.updateReturnReceiveEventsByTrackingNo(pickupTrackingNo,
                "DE");
        Assert.assertEquals(statusMessage1,"Shipment Updation Successful");


    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9810 Receive Pickup : Pickup should be allowed to be received in DC or Returns Hub")
    public void pickReceiveAtDCorReturnHub( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatusSuccessfully(returnId);

        final String returnTrackingNo=lms_closeBoxReturnHelper.getReturnTrackingNo(returnId);

        final Premise premiseDC=new Premise("5",DELIVERY_CENTER);

        final String statusMessage=lms_closeBoxReturnHelper.updateReturnReceiveEventsByPremise(returnId,"DE",
                premiseDC);
        Assert.assertEquals(statusMessage,"Shipment Updation Successful");

        final Premise premiseHub=new Premise("5",HUB);
        final String statusMessage1=lms_closeBoxReturnHelper.updateReturnReceiveEventsByPremise(returnId,"DE",
                premiseDC);
        Assert.assertEquals(statusMessage1,"Shipment Updation Successful");


    }


    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9811 Receive Pickup : Pickup's tracking number search and Pickup Id search should work fine")
    public void pickupSearch( )
            throws Exception {

        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatusSuccessfully(returnId);

        String pickupFromSourceReturnId=LMS_CloseBoxReturnHelper.findPickupFromSourceReturnId(returnId,
                LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LASTMILE_CONSTANTS.CLIENT_ID));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9812 Receive Pickup : pickups in PICKUP_CREATED state can be received directly without any prior bulk uploads")
    public void pickupReceiveInPickup_CreatedState( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

//        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertTrue(
                lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,ShipmentStatus.PICKUP_CREATED.toString(),
                        15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9813 Receive Pickup : pickups in PICKUP_CREATED state can be received directly after uploading - PICKUP_DETAILS_UPDATED,SHIPMENT_ACCEPTED_BY")
    public void pickupCreatedAfterShipmentAccept( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertTrue(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9814 Receive Pickup : pickups in OUT_FOR_PICKUP can be received")
    public void pickupAfterOutForPickup( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertTrue(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9815 Receive Pickup : pickups in PICKUP_DONE can be received")
    public void pickupAfterPickupDone( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertTrue(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9816 Receive Pickup Negative Scenario :RECEIVED_AT_RETURNS_PROCESSING_CENTER to PICKUP_DETAILS_UPDATED,OUT_FOR_PICKUP,PICKUP_DONE,PICKUP_SUCCESSFUL,PICKUP_IN_TRANSIT not allowed")
    public void pickupInTransitAfterPickupSuccessfulNotAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();

        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_SUCCESSFUL,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertFalse(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                ShipmentStatus.PICKUP_IN_TRANSIT.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9817 Negative Upload Scenario :OUT_FOR_PICKUP to PICKUP_SUCCESSFUL not allowed")
    public void outForPickupToPickupSuccessfulNotAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_SUCCESSFUL,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertFalse(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                ShipmentStatus.PICKUP_SUCCESSFUL.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9818 Negative Upload Scenario : OUT_FOR_PICKUP -> PICKUP_DETAILS_UPDATED not allowed")
    public void outForPickupToPickupDetailsUpdatedNotAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");

//        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertFalse(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED_STATUS,1));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9819 Negative Upload Scenario : OUT_FOR_PICKUP -> PICKUP_IN_TRANSIT not allowed ")
    public void outForPickupToPickupTransitNotAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT,"");


        Assert.assertFalse(
                lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT,
                        15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9820 Positive Upload scenario : OUT_FOR_PICKUP to PICKUP_DONE allowed ")
    public void outForPickupToPickupDoneAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertTrue(
                lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE_STATUS,
                        15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9821 Positive Upload Scenario : PICKUP_DONE to PICKUP_IN_TRANSIT is allowed ")
    public void pickupDoneToPickupTransit( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT,"");

        Assert.assertTrue(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT.toString(),5));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9822 FAILED_PICKUP : Can be marked from PICKUP_CREATED")
    public void failedPickupFromPickupCreated( )
            throws Exception {

        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertTrue(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP_STATUS,5));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9823 Receive Pickup negative scenario : FAILED_PICKUP not allowed to be received ")
    public void failedPickupNotAllowedToReceive( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP,"");
        final Premise premiseDC=new Premise("5",DELIVERY_CENTER);
        final String statusMessage=lms_closeBoxReturnHelper.updateReturnReceiveEventsByPremise(returnId,"DE",
                premiseDC);
        Assert.assertNotEquals(statusMessage,"Shipment Updation Successful");

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9824 Receive Return Warehouse : Negative scenario :After marking Failed PIckup, should not be able to receive the returns (RETURN_CREATED status)")
    public void failedPickupNotAllowedToReceiveReturn( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_CREATED,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP,"");


        final Premise premiseDC=new Premise("5",DELIVERY_CENTER);
        final String statusMessage=lms_closeBoxReturnHelper.updateReturnReceiveEventsByPremise(returnId,"DE",
                premiseDC);
        Assert.assertEquals(statusMessage,"Shipment Updation Successful");

    }


    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9825 Negative Upload Scenario : FAILED_PICKUP -> PICKUP_IN_TRANSIT. not allowed")
    public void failedPickupToPickupTransitNotAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_CREATED,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertFalse(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9826 Negative Upload Scenario : FAILED_PICKUP -> PICKUP_SUCCESSFUL is not allowed ")
    public void failedPickupToPickupSuccessfulNotAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_CREATED,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_SUCCESSFUL,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertFalse(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_PICKUP_SUCCESSFUL.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9827 Positive Upload Scenario : FAILED_PICKUP -> PICKUP_DONE is allowed  ")
    public void failedPickupToPickupDoneNotAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_CREATED,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertFalse(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9828 Positive Upload Scenario : OUT_FOR_PICKUP -> PICKUP_DONE is allowed  ")
    public void outForPickupToPickupDoneAllowed1( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertTrue(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_PICKUP_DONE_STATUS.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9829 Positive Upload Scenario : OUT_FOR_PICKUP -> FAILED_PICKUP is allowed")
    public void outForPickupToFailedPickupAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertTrue(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_FAILED_PICKUP.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9830 Negative Upload scenario : OUT_FOR_PICKUP -> PICKUP_IN_TRANSIT is not allowed  ")
    public void outForPickupToPickupInTransitNotAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertFalse(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9831 Positive Upload Scenario : PICKUP_DONE ->RECEIVED_AT_RETURNS_PROCESSING_CENTER is allowed ")
    public void outForPickupToPickupDoneNotAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertFalse(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT.toString(),15));

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9832 Return QC should not be allowed for returns in RETURN_CREATED state.  ")
    public void returnQCNotAllowedForReturnCreated( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();

        lms_closeBoxReturnHelper.unableToPerformClosedBoxReturnQC(returnId,ItemQCStatus.PASSED,
                "DE");

    }

    @Test(retryAnalyzer=Retry.class,enabled=true,
            description="C9833 Return QC only should operated on Pickup Id from Closed Box returns ")
    public void returnQCOnPickupIdOnly( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        String pickupIdOfReturn=lms_returnHelper.getPickupIdOfReturn(returnId);
        lms_closeBoxReturnHelper.performClosedBoxReturnQCByReturnTrackingNo(pickupIdOfReturn,
                ItemQCStatus.PASSED,"DE");


    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9834 Return QC not allowed in return- PICKUP_DONE status")
    public void returnQCNotAllowedInPickupDoneState( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);
        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");

        lmsReturnHelper.manifestClosedBoxPickups();


        lms_closeBoxReturnHelper.unableToPerformClosedBoxReturnQC(returnId,ItemQCStatus.PASSED,
                "DE");

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9835 Preapproval Flow Closed Box ")
    public void preApprovalCloseBox( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);
        lmsReturnHelper.manifestClosedBoxPickups();

        lms_closeBoxReturnHelper.approveAtCC(returnId);

        Assert.assertTrue(lmsReturnHelper.isPickupManifested(String.valueOf(returnId)),
                "Pickup associated with return - "+returnId+" is NOT manifested");

        lms_closeBoxReturnHelper.processClosedBoxPickup(returnId,EnumSCM.PICKED_UP_SUCCESSFULLY,
                EnumSCM.RECEIVED_IN_RETURNS_HUB);

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9890 Before Manifesting, return can be declined by CC, verify that return is not manifested into pickup ")
    public void returnDeclineByCCBeforeManifest( )
            throws Exception {
        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);
        rmsServiceHelper.updateReturn(Long.valueOf(returnId),ReturnActionCode.DECLINE_RETURN,3879l,1);
        lmsReturnHelper.manifestClosedBoxPickups();
    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9897 Download Manifest ")
    public void downloadManifest( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();
    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C9898 Only after updating PICKUP_DETAILS_UPDATED,SHIPMENT_ACCEPTED_BY_COURIER , we can process the pickups further ")
    public void processPickupWithoutUpdatingPICKUP_DETAILS_UPDATED( )
            throws Exception {

        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);
        lmsReturnHelper.manifestClosedBoxPickups();

        //skipping PICKUP_DETAILS_UPDATED,SHIPMENT_ACCEPTED_BY_COURIER and trying return qc
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");

        lmsReturnHelper.manifestClosedBoxPickups();


        lms_closeBoxReturnHelper.unableToPerformClosedBoxReturnQC(returnId,ItemQCStatus.PASSED,
                "DE");

    }

    @Test(retryAnalyzer=Retry.class,  enabled=false,
            description="C9899 Return Label ")
    public void returnLabel( )
            throws Exception {
        // TODO not required removed from test cases
    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C10159 Verify Signature Upload for Closed Box ")
    public void verifySignatureUpload( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);
        lmsReturnHelper.manifestClosedBoxPickups();
        //processing return

        lms_closeBoxReturnHelper.uploadSignature(returnId);

    }

    @Test(retryAnalyzer=Retry.class,  enabled=true,
            description="C10159 pickup Created to PICKUP_IN_TRANSIT is not allowed")
    public void pickupCreatedToPickupTransitNotAllowed( )
            throws Exception {
        lmsReturnHelper.manifestClosedBoxPickups();

        final List <String> returnIds=placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        lmsReturnHelper.manifestClosedBoxPickups();
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DETAILS_UPDATED,
                "SHIPMENT_ACCEPTED_BY_COURIER");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_OUT_FOR_PICKUP,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_DONE,"");
        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_CREATED,"");

        lms_closeBoxReturnHelper.uploadPickupStatus(returnId,EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT,"");

        lms_returnHelper.updateReturnReceiveEvents(returnId,"DE");

        Assert.assertFalse(lmsReturnHelper.validatePickupShipmentStatusInLMS(returnId,
                EnumSCM.ACTIVITY_TYPE_PICKUP_IN_TRANSIT.toString(),5));

    }


    public List <String> placeOrderCreateReturn(int noOfOrders) throws Exception {
        List <String> returnIds=new ArrayList <>();

        for (int i=0; i < noOfOrders; i++) {
            for (int j=0; j < 10; j++) {
                lmsReturnHelper.insertTrackingNumberClosedBox("DE");
            }

            //place order
            String orderId=lmsHepler.createMockOrder(EnumSCM.DL,LMS_PINCODE.ML_BLR,"ML","36",EnumSCM.NORMAL,
                    "cod",false,true);
            OrderReleaseEntry orderReleaseEntry1=omsServiceHelper.getOrderReleaseEntry(
                    omsServiceHelper.getReleaseId(orderId));
            OrderLineEntry lineEntry=omsServiceHelper.getOrderLineEntry(
                    orderReleaseEntry1.getOrderLines().get(0).getId().toString());

            //create return
            ReturnResponse returnResponse=rmsServiceHelper.createReturn(lineEntry.getId(),ReturnType.NORMAL,
                    ReturnMode.CLOSED_BOX_PICKUP,1,49L,RefundMode.NEFT,"418","testDEaddress","6135071",
                    LMS_PINCODE.NORTH_DE,"Bangalore","Karnataka","India",LMS_CONSTANTS.MOBILE_NO);
            Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                    "Return creation failed");
            Long returnID=returnResponse.getData().get(0).getId();
            returnIds.add(String.valueOf(returnID));
        }
        return returnIds;
    }

    @Test(enabled = true,description = "TC_ID : C28823,Return Report fixes for both open and closed box.")
    @SneakyThrows
    public void uploadPickupPreAlertFunctionalityCloseBoxPickup(){

        List<String> returnIds = placeOrderCreateReturn(1);
        final String returnId=returnIds.get(0);

        statusPollingValidator.validateReturnStatusRMS(returnId,EnumSCM.LPI);
        statusPollingValidator.validateReturnStatusLMS(returnId,LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),EnumSCM.RETURN_CREATED);

        com.myntra.lms.client.domain.response.ReturnResponse returnResponse = storeHelper.getReturnStatusInLMS(returnId, LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = returnResponse.getDomainReturnShipments().get(0).getTrackingNumber();
        String warehouseId = returnResponse.getDomainReturnShipments().get(0).getReturnFacilityId();

        //process return order till PS state
       returnHelper.processReturnOrderTillPSState(Long.parseLong(returnId),ReturnMode.CLOSED_BOX_PICKUP,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        statusPollingValidator.validateReturnStatusRMS(returnId,EnumSCM.RL);
        statusPollingValidator.validateReturnStatusLMS(returnId,LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),EnumSCM.RETURN_SUCCESSFUL);

        String filePath = lms_openBoxReturnHelper.createPreAlertReportCSV(returnTrackingNumber, CourierCode.ML.toString(), DateTimeHelper.generateDate("dd-MM-yyy",0),warehouseId, LMS_CONSTANTS.TENANTID);
        String uploadPreAlertReport =lms_openBoxReturnHelper.uploadPreAlertReport(filePath);
        Assert.assertTrue(uploadPreAlertReport.contains("1/1 updated successfully"),"Pre-Alert Report is not uploaded successfully");

        // validate for return shipment table
        com.myntra.lms.client.domain.response.ReturnResponse returnShipmentResponse = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNotNull(returnShipmentResponse.getDomainReturnShipments().get(0).getExpectedReceivedDateInHub(),"Return received date is not stamped in return shipment table");
    }
}