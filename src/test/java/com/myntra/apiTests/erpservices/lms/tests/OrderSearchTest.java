package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.erpservices.lastmile.service.OrderTrackingClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.OrderTabSearch;
import com.myntra.apiTests.erpservices.lms.DB.IQueries;
import com.myntra.apiTests.erpservices.lms.Helper.OrderTabSearchHelper;
import com.myntra.apiTests.erpservices.lms.Helper.ReturnHelper;
import com.myntra.apiTests.erpservices.lms.dp.OrderSearchDP;
import com.myntra.apiTests.erpservices.lms.validators.OrderTabSearchValidator;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagV2SearchHelper;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

/**
 * @author Shanmugam Yuvaraj
 * @since Sep-2019
 */
public class OrderSearchTest implements StatusPoller {

    OrderTabSearchHelper orderTabSearchHelper = new OrderTabSearchHelper();
    MasterBagV2SearchHelper masterBagV2SearchHelper = new MasterBagV2SearchHelper();
    String endQueryParam = "&start=0&fetchSize=20&sortBy=id&sortOrder=ASC";
    OrderTrackingClient_QA orderTrackingClient_qa = new OrderTrackingClient_QA();
    int noOfOrders = 2;

    @Test(priority = 1, enabled = true, dataProviderClass = OrderSearchDP.class, dataProvider = "SearchByOrderId", description = "TC ID-C28025 : search by using order_id is in orders tab and validate using order_to_Ship api")
    public void test_SearchBasedOn_OrderID(String shipmentStatus, String shipmentType) {
        List<Map<String, Object>> dbData = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getOrderId, shipmentStatus, shipmentType, LMS_CONSTANTS.CLIENTID));
        String finalQueryParam = masterBagV2SearchHelper.buildQueryParam(MessageFormat.format(OrderTabSearch.ORDER_ID.getFieldValue(), dbData.get(0).get("order_id").toString(), dbData.get(1).get("order_id").toString()));
        OrderResponse orderResponseDashBoard = orderTabSearchHelper.getOrdersTabDashBoardSearch(finalQueryParam + endQueryParam);
        orderTabSearchValidator.validate_DashBoardSearch_OrderToShip(orderResponseDashBoard, dbData, noOfOrders);
    }

    @Test(priority = 2, enabled = true, dataProviderClass = OrderSearchDP.class, dataProvider = "SearchByOrderId", description = "search by using trackingNumber is in orders tab and validate using order_to_Ship api")
    public void test_SearchBasedOn_TrackingNumber(String shipmentStatus, String shipmentType) {
        List<Map<String, Object>> getOrderToShipData = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getTrackingNumber, shipmentStatus, shipmentType, LMS_CONSTANTS.CLIENTID));
        String finalQueryParam = masterBagV2SearchHelper.buildQueryParam(MessageFormat.format(OrderTabSearch.TRACKING_NUMBER.getFieldValue(), getOrderToShipData.get(0).get("tracking_number").toString(), getOrderToShipData.get(1).get("tracking_number").toString()));
        OrderResponse orderResponseDashBoard = orderTabSearchHelper.getOrdersTabDashBoardSearch(finalQueryParam + endQueryParam);

        orderTabSearchValidator.validate_DashBoardOrdersTabSearch(orderResponseDashBoard, getOrderToShipData, noOfOrders);
    }

    @Test(priority = 3, enabled = true, dataProviderClass = OrderSearchDP.class, dataProvider = "SearchByOrderId", description = "search by using shipment_id is in orders tab and validate using master bag search api api")
    public void test_SearchBasedOn_ShipmentId(String shipmentStatus, String shipmentType) {
        List<Map<String, Object>> getTrackingNumber = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getTrackingNumber, shipmentStatus, shipmentType, LMS_CONSTANTS.CLIENTID));
        List<Map<String, Object>> getMasterbagId = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getMasterBagId, getTrackingNumber.get(0).get("tracking_number").toString()));
        List<Map<String, Object>> getMasterbagId1 = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getMasterBagId, getTrackingNumber.get(1).get("tracking_number").toString()));

        String finalQueryParam = orderTabSearchHelper.buildQueryParam(MessageFormat.format(OrderTabSearch.MASTER_BAG_ID.getFieldValue(), getMasterbagId.get(0).get("shipment_id").toString(), getMasterbagId1.get(0).get("shipment_id").toString()));
        OrderResponse orderResponseDashBoard = orderTabSearchHelper.getOrdersTabDashBoardSearchForMasterBagField(finalQueryParam + endQueryParam);
        orderTabSearchValidator.validate_DashBoardOrdersTabSearch(orderResponseDashBoard, getTrackingNumber, noOfOrders);
    }

    @Test(priority = 4, enabled = true, description = "search by using Pickup_id is in orders tab and validate using Pickup_shipment api")
    public void test_SearchBasedOn_PickupId() throws Exception {
        List<Map<String, Object>> getPickUpShipmentData = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getPickupId, ShipmentStatus.PICKUP_SUCCESSFUL.toString()));
        String finalQueryParam = masterBagV2SearchHelper.buildQueryParam(MessageFormat.format(OrderTabSearch.PICKUP_ID.getFieldValue(), getPickUpShipmentData.get(0).get("id").toString(), getPickUpShipmentData.get(1).get("id").toString()));
        OrderResponse orderResponse = orderTabSearchHelper.getOrdersTabDashBoardSearch(finalQueryParam + endQueryParam);
        orderTabSearchValidator.validate_pickupShipment_OrderSearchTab_Details(orderResponse, getPickUpShipmentData, noOfOrders);
    }

    @Test(priority = 5, enabled = true, description = "TC ID-C28025 : search by using order_id and tracking number in orders tab and validate using order_to_Ship api")
    public void test_SearchBasedOn_OrderID_TrackingNumber() {
        List<Map<String, Object>> dbData = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getOrderId, ShipmentStatus.DELIVERED.toString(), ShipmentType.DL.toString(), LMS_CONSTANTS.CLIENTID));
        String finalQueryParam = masterBagV2SearchHelper.buildQueryParam(MessageFormat.format(OrderTabSearch.ORDER_ID.getFieldValue(), dbData.get(0).get("order_id").toString(), dbData.get(1).get("order_id").toString()),
                MessageFormat.format(OrderTabSearch.TRACKING_NUMBER.getFieldValue(), dbData.get(0).get("tracking_number").toString(), dbData.get(1).get("tracking_number").toString()));
        OrderResponse orderResponseDashBoard = orderTabSearchHelper.getOrdersTabDashBoardSearch(finalQueryParam + endQueryParam);
        orderTabSearchValidator.validate_DashBoardSearch_OrderToShip(orderResponseDashBoard, dbData, noOfOrders);
    }

    @Test(priority = 6, enabled = true, description = "search by using shipment_id and tracking number in orders tab and validate using master bag search api api")
    public void test_SearchBasedOn_ShipmentId_TrackingNumber() {
        List<Map<String, Object>> getTrackingNumber = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getTrackingNumber, ShipmentStatus.DELIVERED.toString(), ShipmentType.DL.toString(), LMS_CONSTANTS.CLIENTID));
        List<Map<String, Object>> getMasterBagId = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getMasterBagId, getTrackingNumber.get(0).get("tracking_number").toString()));
        List<Map<String, Object>> getMasterBagId1 = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getMasterBagId, getTrackingNumber.get(1).get("tracking_number").toString()));
        String finalQueryParam = orderTabSearchHelper.buildQueryParam(MessageFormat.format(OrderTabSearch.MASTER_BAG_ID.getFieldValue(), getMasterBagId.get(0).get("shipment_id").toString(), getMasterBagId1.get(0).get("shipment_id").toString()),
                MessageFormat.format(OrderTabSearch.TRACKING_NUMBER_WITHMB.getFieldValue(), getMasterBagId.get(0).get("tracking_number").toString(), getMasterBagId1.get(0).get("tracking_number").toString()));
        OrderResponse orderResponseDashBoard = orderTabSearchHelper.getOrdersTabDashBoardSearchForMasterBagField(finalQueryParam + endQueryParam);
        orderTabSearchValidator.validate_DashBoardOrdersTabSearch(orderResponseDashBoard, getTrackingNumber, noOfOrders);
    }

    @Test(priority = 7, enabled = true, dataProviderClass = OrderSearchDP.class, dataProvider = "SearchByOrderId", description = "search by using shipment_id and orderId in orders tab and validate using master bag search api api")
    public void test_SearchBasedOn_ShipmentId_OrderId(String shipmentStatus, String shipmentType) {
        List<Map<String, Object>> getTrackingNumber = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getTrackingNumber, shipmentStatus, shipmentType, LMS_CONSTANTS.CLIENTID));
        List<Map<String, Object>> getMasterbagID = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getMasterBagId, getTrackingNumber.get(0).get("tracking_number").toString()));
        List<Map<String, Object>> getMasterbagID1 = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getMasterBagId, getTrackingNumber.get(1).get("tracking_number").toString()));

        String finalQueryParam = orderTabSearchHelper.buildQueryParam(MessageFormat.format(OrderTabSearch.MASTER_BAG_ID.getFieldValue(), getMasterbagID.get(0).get("shipment_id").toString(), getMasterbagID1.get(0).get("shipment_id").toString()),
                MessageFormat.format(OrderTabSearch.ORDER_ID_WITHMB.getFieldValue(), getTrackingNumber.get(0).get("order_id").toString(), getTrackingNumber.get(1).get("order_id").toString()));
        OrderResponse orderResponseDashBoard = orderTabSearchHelper.getOrdersTabDashBoardSearchForMasterBagField(finalQueryParam + endQueryParam);
        orderTabSearchValidator.validate_DashBoardOrdersTabSearch(orderResponseDashBoard, getTrackingNumber, noOfOrders);
    }

    @Test(priority = 8, enabled = true, dataProviderClass = OrderSearchDP.class, dataProvider = "SearchByOrderId", description = "search by using shipment_id and orderId in orders tab and validate using master bag search api api")
    public void test_SearchBasedOn_ShipmentId_PickupId(String shipmentStatus, String shipmentType) {
        List<Map<String, Object>> getPickUpShipmentData = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getPickupId, ShipmentStatus.PICKUP_SUCCESSFUL.toString()));
        List<Map<String, Object>> getMasterbagID = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getMasterBagId, getPickUpShipmentData.get(0).get("tracking_number").toString()));
        List<Map<String, Object>> getMasterbagID1 = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getMasterBagId, getPickUpShipmentData.get(1).get("tracking_number").toString()));

        String finalQueryParam = orderTabSearchHelper.buildQueryParam(MessageFormat.format(OrderTabSearch.MASTER_BAG_ID.getFieldValue(), getMasterbagID.get(0).get("shipment_id").toString(), getMasterbagID1.get(0).get("shipment_id").toString()),
                MessageFormat.format(OrderTabSearch.Pickup_ID_WITHMB.getFieldValue(), getPickUpShipmentData.get(0).get("id").toString(), getPickUpShipmentData.get(1).get("id").toString()));
        OrderResponse orderResponse = orderTabSearchHelper.getOrdersTabDashBoardSearchForMasterBagField(finalQueryParam + endQueryParam);
        orderTabSearchValidator.validate_pickupShipment_OrderSearchTab_Details(orderResponse, getPickUpShipmentData, noOfOrders);
    }

    @Test(priority = 9, enabled = true, dataProviderClass = OrderSearchDP.class, dataProvider = "SearchByOrderId", description = "TC ID-C28025 : search by using order_id and tracking number and master bag in orders tab and validate using order_to_Ship api")
    public void test_SearchBasedOn_OrderID_TrackingNumber_MasterbagID(String shipmentStatus, String shipmentType) {
        List<Map<String, Object>> getOrder_To_Ship_Data = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getOrderId, shipmentStatus, shipmentType, LMS_CONSTANTS.CLIENTID));
        Map<String, Object> getShipmentOrderMapData = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.get_ShipmentOrderMap_Data, getOrder_To_Ship_Data.get(0).get("tracking_number").toString()), "myntra_lms");
        Map<String, Object> getShipmentOrderMapData1 = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.get_ShipmentOrderMap_Data, getOrder_To_Ship_Data.get(1).get("tracking_number").toString()), "myntra_lms");

        String finalQueryParam = orderTabSearchHelper.buildQueryParam(MessageFormat.format(OrderTabSearch.MASTER_BAG_ID.getFieldValue(), getShipmentOrderMapData.get("shipment_id").toString(), getShipmentOrderMapData1.get("shipment_id").toString()),
                MessageFormat.format(OrderTabSearch.TRACKING_NUMBER_WITHMB.getFieldValue(), getOrder_To_Ship_Data.get(0).get("tracking_number").toString(), getOrder_To_Ship_Data.get(1).get("tracking_number").toString()));
        OrderResponse orderResponseDashBoard = orderTabSearchHelper.getOrdersTabDashBoardSearchForMasterBagField(finalQueryParam + endQueryParam);
        orderTabSearchValidator.validate_DashBoardSearch_OrderToShip(orderResponseDashBoard, getOrder_To_Ship_Data, noOfOrders);
    }

    @Test(priority = 10, enabled = true, description = "TC ID-C28025 : search by using pickup_id and tracking number and master bag in orders tab and validate using order_to_Ship api")
    public void test_SearchBasedOn_PickupID_TrackingNumber_MasterbagID() {
        List<Map<String, Object>> get_PickupShipment_Data = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.getPickupId, ShipmentStatus.PICKUP_SUCCESSFUL.toString()));
        Map<String, Object> getShipmentOrderMapData = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.get_ShipmentOrderMap_Data, get_PickupShipment_Data.get(0).get("tracking_number").toString()), "myntra_lms");
        Map<String, Object> getShipmentOrderMapData1 = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.get_ShipmentOrderMap_Data, get_PickupShipment_Data.get(1).get("tracking_number").toString()), "myntra_lms");

        String finalQueryParam = orderTabSearchHelper.buildQueryParam(MessageFormat.format(OrderTabSearch.MASTER_BAG_ID.getFieldValue(), getShipmentOrderMapData.get("shipment_id").toString(), getShipmentOrderMapData1.get("shipment_id").toString()),
                MessageFormat.format(OrderTabSearch.TRACKING_NUMBER_WITHMB.getFieldValue(), get_PickupShipment_Data.get(0).get("tracking_number").toString(), get_PickupShipment_Data.get(1).get("tracking_number").toString()));
        OrderResponse orderResponse = orderTabSearchHelper.getOrdersTabDashBoardSearchForMasterBagField(finalQueryParam + endQueryParam);
        orderTabSearchValidator.validate_pickupShipment_OrderSearchTab_Details(orderResponse, get_PickupShipment_Data, noOfOrders);
    }

    @Test(priority = 11, enabled = true, description = "TC ID-C28025 validate track details in Orders tab using getOrderTrackingDetail api")
    public void test_OrderTrackingDetails_InOrdersTab() throws IOException, JAXBException, JSONException, XMLStreamException {
        List<Map<String, Object>> get_Order_To_Ship_Data = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.OrdersTabSearchQuery.get_OrderToShip_data1, ShipmentStatus.DELIVERED.toString()));
        String trackingNumber = get_Order_To_Ship_Data.get(0).get("tracking_number").toString();
        OrderTrackingResponseV2 orderTrackingResponseOrdersTab = lmsServiceHelper.getOrderTrackingDetail(CourierCode.ML.toString(), trackingNumber, OrderTrackingDetailLevel.LEVEL2.toString());
        OrderTrackingResponseV2 orderTrackingResponseApi = orderTrackingClient_qa.getOrderTrackingDetailV2(trackingNumber, CourierCode.ML.toString(), OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        try {
            Assert.assertEquals(orderTrackingResponseOrdersTab.getOrderTrackingEntry().getTrackingNumber(), orderTrackingResponseApi.getOrderTrackingEntry().getTrackingNumber(), "tracking number is not matching from order tab data and api data");
            List<OrderTrackingDetailEntryV2> lstOrderTrackingDetailEntryV2Api = orderTrackingResponseApi.getOrderTrackingEntry().getOrderTrackingDetails();
            List<OrderTrackingDetailEntryV2> lstOrderTrackingDetailEntryV2 = orderTrackingResponseOrdersTab.getOrderTrackingEntry().getOrderTrackingDetails();
            Assert.assertEquals(lstOrderTrackingDetailEntryV2Api.size(), lstOrderTrackingDetailEntryV2.size(), "size of the order entries is not matching");
            orderTabSearchValidator.validateOrdersTrackDetails(lstOrderTrackingDetailEntryV2Api, lstOrderTrackingDetailEntryV2);
        } catch (NullPointerException n) {
            n.printStackTrace();
            Assert.fail("Invalid data received on tracking Number - "+ trackingNumber);
        }
    }
}

