package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.erpservices.LMSCancellation.validators.OrderValidator;
import com.myntra.apiTests.erpservices.cancellation.dp.CancellationDataProvider;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_ReturnHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.apiTests.erpservices.oms.OMSHelpersEnums;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.oms.client.response.PacketResponse;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import lombok.SneakyThrows;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bharath.MC
 * @since Oct-2019
 * This class will be single point for LMS operations. Can be used during manual testing.
 */
public class LMSOperationsHelper {

    private TripClient_QA tripClient_qa = new TripClient_QA();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    OrderValidator orderValidator = new OrderValidator();

    @Test
    @SneakyThrows
    public void createTripForDeliveryCenter() {
        Long deliveryCenterId = 5l;
        TripResponse tripResponse = tripClient_qa.createTrip(deliveryCenterId, Long.parseLong(String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(deliveryCenterId))));
        System.out.println(String.format("DeliveryCenter Id:[%s] , Trip Number:[%s], Trip Id:[%s]",
                tripResponse.getTrips().get(0).getDeliveryCenterId(),
                tripResponse.getTrips().get(0).getTripNumber(),
                tripResponse.getTrips().get(0).getId())
        );
    }

    @Test(description = "Process Order from X to Y State (Only Forward till DL)")
    public void setOrderStatus() throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        ReleaseStatus releaseStatus = ReleaseStatus.SH;
        String packetId = "3864127471754246";
        String courierCode = "ML";
        String query = "SELECT order_release_id_fk FROM order_line WHERE packet_id_fk = '%s';";
        query = String.format(query, packetId);
        Map<String, Object> resultSet = DBUtilities.exSelectQueryForSingleRecord(query,"myntra_oms");
        String releaseId = resultSet.get("order_release_id_fk").toString();
        lmsHelper.processOrderInSCM(releaseStatus.toString(), courierCode, releaseId, packetId);
    }

    @Test(description = "Create Mock Order - COD")
    public void createMockOrderCOD() throws Exception {
        final Integer NumberOfOrders = 1;
        Map<String, String> orderMap = new HashMap<>();
        String orderId = null;
        String trackingNumber, packetId;
        for (int i = 0; i < NumberOfOrders; i++) {
            orderId = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            packetId = omsServiceHelper.getPacketId(orderId);
            trackingNumber = lmsHelper.getTrackingNumber(packetId);
            orderMap.put(trackingNumber, "[" + packetId + " , " + orderId + "]");
        }
        System.out.println("orderMap=" + orderMap);
    }

    @Test(description = "Create Mock Order - ONLINE")
    public void createMockOrderOnline() throws Exception {
        final Integer NumberOfOrders = 1;
        Map<String, String> orderMap = new HashMap<>();
        String orderId = null;
        String trackingNumber, packetId;
        for (int i = 0; i < NumberOfOrders; i++) {
            orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "on", false, true);
            packetId = omsServiceHelper.getPacketId(orderId);
            trackingNumber = lmsHelper.getTrackingNumber(packetId);
            orderMap.put(trackingNumber, "[" + packetId + " , " + orderId + "]");
        }
        System.out.println("orderMap=" + orderMap);
    }

    @Test
    public void createExchangeOrder() throws Exception {
        final Integer NumberOfOrders = 1;
        List<String> exchangeOrderIds = new ArrayList<>();
        List<String> exchangeTrackingNumbers = new ArrayList<>();
        List<String> exchangePacketIds = new ArrayList<>();
        Map<String, String> exchangePacketTRMap = new HashMap<>();
        for (int i = 0; i < NumberOfOrders; i++) {
            String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            String exchangeOrderId = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
            String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrderId);
            String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);
            exchangeOrderIds.add(exchangeOrderId);
            exchangeTrackingNumbers.add(exchangeTrackingNumber);
            exchangePacketIds.add(exchangePacketId);
            exchangePacketTRMap.put(exchangePacketId, exchangeTrackingNumber);
        }
        System.out.println("exchangeOrderIds:" + exchangeOrderIds);
        System.out.println("exchangePacketIds:" + exchangePacketIds);
        System.out.println("exchangeTrackingNumbers:" + exchangeTrackingNumbers);
        System.out.println(exchangePacketTRMap);
    }

    @Test(description = "Create mock order and deliver it to seller")
    public void deliverAndReturnFlow() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        System.out.println("________ReturnId: " + returnId);
        lmsReturnHelper.processOpenBoxReturn(returnId.toString(), EnumSCM.PICKED_UP_SUCCESSFULLY);
    }

    @Test(description = "create return for a given order")
    public void createReturn(){
        String orderID = "";
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        System.out.println("________ReturnId: " + returnId);
    }

    @Test(description = "Process OpenBox return till Deliver to Seller")
    @SneakyThrows
    public void processOpenBoxReturn(){
        String returnId = "4001558373";
        lmsReturnHelper.processOpenBoxReturn(returnId, EnumSCM.PICKED_UP_SUCCESSFULLY);
    }

    @Test(description = "Create Return and Mark OnHold")
    public void createReturnAndMarkItOnHold(){

    }

    @Test(description = "Create RTO")
    public void createRTO(){

    }

    /**
     * Process order to X State and Cancell it.
     * Note: order must be atleast in Packed State
     */
    public void processOrderStatus(String packetId, ReleaseStatus releaseStatus) throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        String courierCode = "ML";
        String query = "SELECT order_release_id_fk FROM order_line WHERE packet_id_fk = '%s';";
        query = String.format(query, packetId);
        Map<String, Object> resultSet = DBUtilities.exSelectQueryForSingleRecord(query,"myntra_oms");
        String releaseId = resultSet.get("order_release_id_fk").toString();
        lmsHelper.processOrderInSCM(releaseStatus.toString(), courierCode, releaseId, packetId);
    }
    @Test(description = "Testing Cancellation in OMS & LMS for a given packet and cancellation on particular ReleaseStatus.")
    public void testCancellation() throws Exception {
        String packetId = "21476140071511";
        ReleaseStatus releaseStatus = ReleaseStatus.ADDED_TO_MB;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        processOrderStatus(packetId, releaseStatus);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        orderValidator.validateOrderIsCancellable(packetId, releaseStatus);
        if (releaseStatus == ReleaseStatus.SH) {
            orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        }
        //Cancel API
        PacketResponse packetResponse = omsServiceHelper.cancelPacket(packetId, OMSHelpersEnums.PacketEvents.CANCEL_PACKET);
        //orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        if(!(releaseStatus== ReleaseStatus.OFD || releaseStatus== ReleaseStatus.DL)) {
            orderValidator.validatePacketStatusInOMS(packetId, "IC");
            orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, releaseStatus);
        }
        if(releaseStatus== ReleaseStatus.OFD) {
            orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.OUT_FOR_DELIVERY, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        }
        if(releaseStatus== ReleaseStatus.DL) {
            orderValidator.validatePacketStatusInOMS(packetId, "D");
            orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        }
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
    }

}
