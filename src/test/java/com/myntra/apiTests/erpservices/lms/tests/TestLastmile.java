package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.client.LastmileClient;
import com.myntra.apiTests.erpservices.lms.validators.AssignOrderToTripValidator;
import com.myntra.apiTests.erpservices.lms.validators.CreateTripValidator;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.testng.annotations.Test;

public class TestLastmile {
    LMSHelper lmsHelper =new LMSHelper();
    LastmileClient lastmileClient=new LastmileClient();
    LmsServiceHelper lmsServiceHelper=new LmsServiceHelper();


    @Test
    public void testLastmile()throws Exception{
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID(""+42);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse =lastmileClient.createTrip(5L,Long.valueOf(deliveryStaffID));
        CreateTripValidator createTripValidator=new CreateTripValidator(tripResponse);
        createTripValidator.validateCreateTripStatus(1001,"Trip added successfully",EnumSCM.SUCCESS);
        createTripValidator.validateTenantId("4019");
        LMSClient lmsClient=new LMSClient();
        OrderResponse orderResponse =lmsClient.getOrderByOrderId(orderId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse =lastmileClient.assignOrderToTrip(tripResponse.getTrips().get(0).getId(),orderResponse.getOrders().get(0).getTrackingNumber());
        AssignOrderToTripValidator assignOrderToTripValidator =new AssignOrderToTripValidator(tripOrderAssignmentResponse);
        assignOrderToTripValidator.validateIsTripStarted(false);
    }
}
