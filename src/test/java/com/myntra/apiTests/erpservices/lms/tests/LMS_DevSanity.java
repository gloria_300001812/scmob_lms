package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_ReturnHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.testbase.BaseTest;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import static com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS.WAREHOUSE_36;

/**
 * Created by Gloria R on 12/16/16.
 */
public class LMS_DevSanity extends BaseTest {
	
	private static org.slf4j.Logger log = LoggerFactory.getLogger(LMS_Reverse_Exchange.class);
	private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
	private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
	private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
	private LMSHelper lmsHepler = new LMSHelper();
	private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
	private ProcessRelease processRelease = new ProcessRelease();
	LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
	
	
	@Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, description = "ID: C496 , LMS E2E flow from order placement to DL and than create return and process return till receive back in WH", invocationCount = 1)
	public void LMS_Mock_OpenBoxPickUpSuccessful() throws Exception {
		String orderID=null;
		try {
			 orderID = lmsHepler.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", WAREHOUSE_36, EnumSCM.NORMAL, "cod", false, true);
		}catch(SkipException e){
			org.testng.Assert.fail("Order creation failed");
		}
		OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
		OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
		ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
		Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
		Long returnId = returnResponse.getData().get(0).getId();
		log.info("________ReturnId: " + returnId);
		lmsReturnHelper.processOpenBoxReturn(returnId.toString(), EnumSCM.PICKED_UP_SUCCESSFULLY);
		
		
	}
		
	}