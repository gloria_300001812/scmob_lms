/**
 * Created by Abhishek Jaiswal on 15/11/18.
 */

package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import org.testng.Assert;

public class DeliveryStaffValidator {
    DeliveryStaffResponse lmsTripResponse;

    public DeliveryStaffValidator(DeliveryStaffResponse lmsTripResponse) {
        this.lmsTripResponse = lmsTripResponse;
    }

    /*
     * BASE MODULAR FUNCTIONS START For Trip Response*/

    public void validateStatusType(StatusResponse.Type statusType)
    {
        Assert.assertEquals(lmsTripResponse.getStatus().getStatusType(), statusType, "Unexpected Status");
    }

    public void validateStatusCode(int statusCode)
    {
        Assert.assertEquals(lmsTripResponse.getStatus().getStatusCode(), statusCode, "Wrong status Code");
    }

    public void validateTotalCount(long totalCount)
    {
        Assert.assertEquals(lmsTripResponse.getStatus().getTotalCount(), totalCount, "Count differs");
    }

    public void isAvailable(int index, Boolean status)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryStaffs().get(index).getAvailable(), status, "Expected"+status);
    }

    public void isDeleted(int index, Boolean status)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryStaffs().get(index).getDeleted(), status, "Expected"+status);
    }

    public void validateTenantId(int index, String tenantId)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryStaffs().get(index).getTenantId(), tenantId, "Wrong Tenant ID");
    }

    public void validateTenantId( String tenantId)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryStaffs().get(0).getTenantId(), tenantId, "Wrong Tenant ID");
    }

    public void validateFirstName(String firstName)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryStaffs().get(0).getFirstName(),firstName, "First name is incorrect" );
    }
    public void validateFirstName(int index , String firstName)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryStaffs().get(index).getFirstName(),firstName, "First name is incorrect" );
    }


    public void validateLastName(String lastName)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryStaffs().get(0).getLastName(),lastName, "Last name is incorrect" );
    }
    public void validateLastName(int index , String lastName)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryStaffs().get(index).getLastName(),lastName, "Last name is incorrect" );
    }


    public void validateMobileNum(String mobnum)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryStaffs().get(0).getMobile(),mobnum, "Mob num is incorrect" );

    }

    public void validateDcId(String DcID)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryStaffs().get(0).getDeliveryCenterId().toString(),DcID, "DcId is incorrect" );
    }

    public void validateDcId(int index , String DcID)
    {
        Assert.assertEquals(lmsTripResponse.getDeliveryStaffs().get(index).getDeliveryCenterId().toString(),DcID, "DcId is incorrect" );
    }


    public void validateIsAvailable(Integer val)
    {
        if(val == 1)
            Assert.assertTrue(lmsTripResponse.getDeliveryStaffs().get(0).getAvailable());
        else
            Assert.assertFalse(lmsTripResponse.getDeliveryStaffs().get(0).getAvailable());

    }

    public void validateIsDeleted(Integer val)
    {
        if(val == 1)
            Assert.assertTrue(lmsTripResponse.getDeliveryStaffs().get(0).getDeleted());
        else
            Assert.assertFalse(lmsTripResponse.getDeliveryStaffs().get(0).getDeleted());
    }

}