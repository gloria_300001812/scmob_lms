/**
 * Created by Abhishek Jaiswal on 15/11/18.
 */
package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.apiTests.erpservices.khata.validator.KhataAccountValidator;
import com.myntra.apiTests.erpservices.khata.validator.LedgerValidator;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.erpservices.utility.RejoyServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.response.EmptyResponse;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;
import com.myntra.khata.response.PaymentResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.*;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.FinanceReportResponse;
import com.myntra.lastmile.client.response.StoreTripResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.response.TripUpdateDashboardInfoResponse;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.lms.client.status.OrderShipmentAssociationStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lms.khata.domain.EventType;
import com.myntra.lms.khata.domain.PaymentMode;
import com.myntra.logistics.platform.domain.*;
import com.myntra.logistics.platform.response.MasterbagResponse;
import com.myntra.lordoftherings.SlackMessenger;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.oms.client.entry.PacketEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.testbase.BaseTest;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_ADDED_TO_REVERSE_BAG;
import static com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.LAST_MILE_PARTNER;
import static com.myntra.lastmile.client.code.utils.ReconType.EOD_RECON;
import static com.myntra.lms.client.constants.HttpParamConstants.TENANT_ID;


public class LastmileTests extends BaseTest {

    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    TripClient_QA tripClient_qa = new TripClient_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa = new MLShipmentClientV2_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa = new TripOrderAssignmentClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa = new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa = new DeliveryStaffClient_QA();
    StoreClient_QA storeClient_qa = new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa = new MasterBagClient_QA();

    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private OrderEntry orderEntry = new OrderEntry();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private ProcessRelease processRelease = new ProcessRelease();
    private ItemEntry itemEntry = new ItemEntry();
    MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(LMS_Trip.class);
    HashMap<String, String> pincodeDCMap;

    //creating HashMap to maintain pincode and DcId
    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
        lmsHelper.setNdrConfigTo0();
    }

    public void checkReconStatus(String orderId, String tripOrderAssignmentId, String status, String isReceived) {
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(orderId, status, 4), "Wrong status in ml_shipment");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), isReceived, "Wrong status in trip_order_assignment");
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 6, description = "ID: C16999 , deleteAllTripForDC 42, For all the trip scenarios we are using DC 42 (CAR). So before running any test cases from this class we are just closing all the open trips for this Delivery center", enabled = false)
    public void completeAllTripForDC42() {

        List<Map<String, Object>> tripIds = DBUtilities.exSelectQuery("select tp.id from delivery_staff ds, trip tp where ds.id = tp.`delivery_staff_id` and " +
                "tp.`delivery_center_id` in (5,42) and tp.`trip_status` <> 'COMPLETED'", "lms");
        if (tripIds == null) {
            return;
        } else {
            String ids = "";
            for (Map<String, Object> tripId : tripIds)
                ids = ids + "," + tripId.get("id");// we can put comma after the first id so no need to put regex in the sql query written to mark the delivery completed.
            DBUtilities.exUpdateQuery("update trip set trip_status = 'COMPLETED' where id in(" + ids.replaceFirst(",", "") + ")", "lms");

        }
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripWithParamForDeliveryCenter", description = "ID: C16998 , getTrip", enabled = true)
    public void getTripWithParamForDeliveryCenter(String queryParam, int start, int fetchSize, String sortBy, String sortOrder, String statusCode, String statusMessage, String tenantId) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        queryParam = queryParam.replace("ToBeReplacedDCId", DcId);
        TripResponse response = tripClient_qa.searchTripByParam(start, fetchSize, sortBy, sortOrder, Boolean.parseBoolean(null), queryParam, "");
        TripResponseValidator lmsTripValidator = new TripResponseValidator(response);
        lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);
        int totalCount = (int) response.getStatus().getTotalCount();
        if (totalCount < fetchSize)
            fetchSize = totalCount;

        if (fetchSize > 0) {
            List resultSet = DBUtilities.exSelectQuery("select `id`,`delivery_staff_id` from trip where `delivery_center_id`=" + DcId + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY `trip_date` desc limit " + fetchSize, "myntra_lms");
            HashMap<String, Object> row = null;
            lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);
            for (int index = 0; index < fetchSize; index++) {
                row = (HashMap<String, Object>) resultSet.get(index);
                lmsTripValidator.validateId(Long.toString((Long) row.get("id")), index);
                lmsTripValidator.validateDeliveryStaffId(Long.toString((Long) row.get("delivery_staff_id")), index);
                Assert.assertEquals(response.getTrips().get(index).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant ID");
            }
        }
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripWithParamForWrongDC", description = "ID: C16998 , getTrip", enabled = true)
    public void getTripWithParamForWrongDC(String queryParam, int start, int fetchSize, String sortBy, String sortOrder, String statusCode, String statusMessage, String tenantId) throws Exception {
        String WrongDcId = "1234";
        queryParam = queryParam.replace("ToBeReplacedDCId", WrongDcId);
        TripResponse response = tripClient_qa.searchTripByParam(start, fetchSize, sortBy, sortOrder, Boolean.parseBoolean(null), queryParam, "");
        Assert.assertEquals(response.getStatus().getTotalCount(), 0L, "Expected total count as 0 as wrong DcId");
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "searchTripForOnlyDC", description = "ID: C16998 , getTrip", enabled = true)
    public void searchTripForOnlyDC(String queryParam, int start, int fetchSize, String sortBy, String sortOrder, String statusCode, String statusMessage, String tenantId) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        queryParam = queryParam.replace("ToBeReplacedDCId", DcId);
        TripResponse response = tripClient_qa.searchTripByParam(start, fetchSize, sortBy, sortOrder, Boolean.parseBoolean(null), queryParam, "");
        TripResponseValidator lmsTripValidator = new TripResponseValidator(response);
        lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);

    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripWithParamForDeliveryStaff", description = "ID: C17000 , getTrip", enabled = true)
    public void getTripWithParamForDeliveryStaff(String queryParam, int start, int fetchSize, String sortBy, String sortOrder, String statusCode, String statusMessage) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        queryParam = queryParam.replace("ToBeReplacedDCId", DcId);
        queryParam = queryParam.replace("ToBeReplacedStaffId", deliveryStaffID);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to reate trip");
        Thread.sleep(4000);
        TripResponse response = tripClient_qa.searchTripByParam(start, fetchSize, sortBy, sortOrder, Boolean.parseBoolean(null), queryParam, "");
        TripResponseValidator lmsTripValidator = new TripResponseValidator(response);
        lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);
        int totalCount = (int) response.getStatus().getTotalCount();
        if (totalCount < fetchSize)
            fetchSize = totalCount;
        if (fetchSize > 0) {
            lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);
            List resultSet = DBUtilities.exSelectQuery("select * from trip where  `delivery_center_id` =" + DcId + " AND `delivery_staff_id` =" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY `trip_date` desc limit " + fetchSize, "myntra_lms");
            HashMap<String, Object> row = null;
            HashMap<String, Object> deliveryStaffData = (HashMap<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select * from delivery_staff where `id` =" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "myntra_lms");
            String firstName = (String) deliveryStaffData.get("first_name");
            String lastName = (String) deliveryStaffData.get("last_name");
            String mobileNum = (String) deliveryStaffData.get("mobile");
            for (int index = 0; index < fetchSize; index++) {
                row = (HashMap<String, Object>) resultSet.get(index);
                lmsTripValidator.validateId(Long.toString((Long) row.get("id")), index);
                lmsTripValidator.validateDeliveryStaffId(deliveryStaffID, index);
                lmsTripValidator.validateDeliveryCenterId(Integer.parseInt(DcId), index);
                lmsTripValidator.validateNameAndMobileNum(firstName, lastName, mobileNum, index);
                Assert.assertEquals(response.getTrips().get(index).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant ID");

            }
        }
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripWithParamForCardEnabled", description = "ID: C17001 , getTrip", enabled = true)
    public void getTripWithParamForCardEnabled(String queryParam, int start, int fetchSize, String sortBy, String sortOrder, String statusCode, String statusMessage) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        queryParam = queryParam.replace("ToBeReplacedDCId", DcId);
        TripResponse response = tripClient_qa.searchTripByParam(start, fetchSize, sortBy, sortOrder, Boolean.parseBoolean(null), queryParam, "");
        TripResponseValidator lmsTripValidator = new TripResponseValidator(response);
        lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);
        List resultSet = DBUtilities.exSelectQuery("select * from trip where `delivery_center_id`=" + DcId + " AND `is_card_enabled`= true AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY `trip_date` desc limit " + fetchSize, "myntra_lms");
        HashMap<String, Object> row = null;
        lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);
        int totalCount = (int) response.getStatus().getTotalCount();
        if (totalCount < fetchSize)
            fetchSize = totalCount;
        if (fetchSize > 0) {

            for (int index = 0; index < fetchSize; index++) {
                row = (HashMap<String, Object>) resultSet.get(index);
                lmsTripValidator.validateId(Long.toString((Long) row.get("id")), index);
                lmsTripValidator.validateDeliveryStaffId(Long.toString((Long) row.get("delivery_staff_id")), index);
                lmsTripValidator.validateCardEnabled(true, index);
                Assert.assertEquals(response.getTrips().get(index).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant ID");

            }
        }
    }

    //TODO: 2 trip status Outforpickup and added to sda trip not added
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripWithParamForTripStatus", description = "ID: C17002 , getTrip", enabled = true)
    public void getTripWithParamForTripStatus(String queryParam, int start, int fetchSize, String sortBy, String sortOrder, String statusCode, String statusMessage, String tripStatus) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        queryParam = queryParam.replace("ToBeReplacedDCId", DcId);
        TripResponse response = tripClient_qa.searchTripByParam(start, fetchSize, sortBy, sortOrder, Boolean.parseBoolean(null), queryParam, "");
        TripResponseValidator lmsTripValidator = new TripResponseValidator(response);
        lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);

        int totalCount = (int) response.getStatus().getTotalCount();
        if (totalCount < fetchSize)
            fetchSize = totalCount;
        if (fetchSize > 0) {
            List resultSet = DBUtilities.exSelectQuery("select * from trip where `delivery_center_id` =" + DcId + " AND `trip_status`=\"" + tripStatus + "\" AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY `trip_date` desc limit " + fetchSize, "myntra_lms");

            HashMap<String, Object> row = null;
            lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);
            for (int index = 0; index < fetchSize; index++) {
                row = (HashMap<String, Object>) resultSet.get(index);
                lmsTripValidator.validateId(Long.toString((Long) row.get("id")), index);
                lmsTripValidator.validateDeliveryStaffId(Long.toString((Long) row.get("delivery_staff_id")), index);
                lmsTripValidator.validateTripStatus(tripStatus, index);
                lmsTripValidator.validateDeliveryCenterId(Integer.parseInt(DcId), index);
                Assert.assertEquals(response.getTrips().get(index).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant ID");

            }
        }
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getAllAvailableTripsForDc", description = "ID: C17003 , getTrip", enabled = true)
    public void getAllAvailableTripsForDc(String statusCode, String statusMessage) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        TripResponse tripResponse = tripClient_qa.getAllAvailableTripsForDC(Long.parseLong(DcId));
        int totalCount = (int) tripResponse.getStatus().getTotalCount();
        if (totalCount > 0) {
            TripResponseValidator lmsTripValidator = new TripResponseValidator(tripResponse);
            lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);
//            List resultSet = DBUtilities.exSelectQuery("select * from trip where  `delivery_center_id` =" + DcId + " AND `trip_status`= 'CREATED' AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "myntra_lms");
//            HashMap<String, Object> row = null;
//            for (int index = 0; index < totalCount; index++) {
//                row = (HashMap<String, Object>) resultSet.get(index);
//                lmsTripValidator.validateId(Long.toString((Long) row.get("id")), index);
//                lmsTripValidator.validateDeliveryStaffId(Long.toString((Long) row.get("delivery_staff_id")), index);
//                lmsTripValidator.validateTripStatus(EnumSCM.CREATED, index);
//                lmsTripValidator.validateDeliveryCenterId(Integer.parseInt(DcId), index);
//
//            }

        }
    }

    //TODO: Unable to use client for this Api problem with passing parameter
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getAllSearchResultOfTrip", description = "ID: C17004 , getTrip", enabled = true)
    public void getAllSearchResultOfTrip(String queryParam, String statusCode, String statusMessage) throws Exception {
        TripResponse tripResponse = (TripResponse) lmsServiceHelper.getTrip.apply(queryParam);
        int totalCount = (int) tripResponse.getStatus().getTotalCount();
        if (totalCount > 0) {
            List resultSet = DBUtilities.exSelectQuery("select * from trip ", "myntra_lms");
            TripResponseValidator lmsTripValidator = new TripResponseValidator(tripResponse);
            lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);
            HashMap<String, Object> row = null;
            for (int index = 0; index < 5; index++) {
                row = (HashMap<String, Object>) resultSet.get(index);
                lmsTripValidator.validateId(Long.toString((Long) row.get("id")), index);
                lmsTripValidator.validateDeliveryStaffId(Long.toString((Long) row.get("delivery_staff_id")), index);
                lmsTripValidator.validateDeliveryCenterId(((Long) row.get("delivery_center_id")).intValue(), index);

            }
        }
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripByTripNumber", description = "ID: C17005 , getTrip", enabled = true)
    public void getTripByTripNumber(String queryParam, int start, int fetchSize, String sortBy, String sortOrder, String statusCode, String statusMessage) throws Exception {
        List tripData = DBUtilities.exSelectQuery("select `id`,`trip_number`,`delivery_staff_id` from trip WHERE `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY RAND() LIMIT 1; ", "myntra_lms");
        HashMap<String, Object> row = null;
        row = (HashMap<String, Object>) tripData.get(0);
        String tripNumber = row.get("trip_number").toString();
        String tripId = row.get("id").toString();
        String deliveryStaffId = row.get("delivery_staff_id").toString();
        queryParam = queryParam.replace("ToBeReplacedTripNumber", tripNumber);
        TripResponse response = tripClient_qa.searchTripByParam(start, fetchSize, sortBy, sortOrder, Boolean.parseBoolean(null), queryParam, "");
        TripResponseValidator lmsTripValidator = new TripResponseValidator(response);

        lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);
        lmsTripValidator.validateTripNumber(tripNumber);
        lmsTripValidator.validateId(tripId);
        HashMap<String, Object> deliveryStaffData = (HashMap<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select * from delivery_staff where `id` =" + deliveryStaffId + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "myntra_lms");
        lmsTripValidator.validateNameAndMobileNum((String) deliveryStaffData.get("first_name"), (String) deliveryStaffData.get("last_name"), (String) deliveryStaffData.get("mobile"));
    }

    //TODO: cannot use client
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripByTripId", description = "ID: C17006 , getTrip", enabled = true)
    public void getTripPlanningByTripId(String queryParam, int start, int fetchSize, String sortBy, String sortOrder, String statusCode, String statusMessage) throws Exception {
        List tripData = DBUtilities.exSelectQuery("select `id`,`trip_number` from trip WHERE `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY RAND() LIMIT 1; ", "myntra_lms");
        HashMap<String, Object> row = null;
        row = (HashMap<String, Object>) tripData.get(0);
        String tripNumber = row.get("trip_number").toString();
        String tripId = row.get("id").toString();
        queryParam = queryParam.replace("ToBeReplacedTripId", tripId);// trip ID as per test case
        //TripResponse response = tripClient_qa.searchTripByParam(start, fetchSize, sortBy, sortOrder, Boolean.parseBoolean(null), queryParam, "");
        TripResponse response = (TripResponse) lmsServiceHelper.getTrip.apply(tripId);// cannot use client
        TripResponseValidator lmsTripValidator = new TripResponseValidator(response);
        lmsTripValidator.validateStatus(Integer.parseInt(statusCode), statusMessage);
        lmsTripValidator.validateTripNumber(tripNumber);
        lmsTripValidator.validateId(tripId);

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 7, description = "ID: C17007 , downloadStoreTrip", enabled = true)
    public void downloadStoreTrip() throws IOException, NumberFormatException, JAXBException, WebClientException {
        List tripData = DBUtilities.exSelectQuery("select `id` from trip ORDER BY RAND() LIMIT 1; ", "myntra_lms");
        HashMap<String, Object> row = null;
        row = (HashMap<String, Object>) tripData.get(0);
        Long tripId = (Long) row.get("id");
        StoreTripResponse shipmentResponse = tripClient_qa.getStoreTripDetails(tripId);
        Assert.assertEquals(shipmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not downloaded");
    }


    //TODO:Validations Pending
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripOrderByTripId", description = "ID: C17008 , getTrip", enabled = true)
    public void getTripOrderByTripId(String queryParam, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        TripOrderAssignmentResponse tripAssignmentResponse1 = tripClient_qa.getTripDetails(lmsHelper.getTrackingNumber(packetId), LASTMILE_CONSTANTS.TENANT_ID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripAssignmentResponse1);
        tripOrderAssignmentResponseValidator.validateGetTripOrderByTripId(TripOrderStatus.WFD.toString(), packetId);
        Assert.assertEquals(tripAssignmentResponse1.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant Id");
        MLShipmentResponse mlShipmentResponse = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator = new MLShipmentResponseValidator(mlShipmentResponse);
        mlShipmentResponseValidator.validateShipmentStatus(EnumSCM.ASSIGNED_TO_SDA);
        mlShipmentResponseValidator.validateTrackingNumber(tracking_num);
        mlShipmentResponseValidator.validateSourceReferenceId(packetId);
        //TODO : validation of ml_shipment tracking detail table data cannot be done as their is no client available for it
    }

    //TODO: Need to add Try and buy
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17009 , getTrip", enabled = true)
    public void getTripOrderByTripNumberForDL() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Thread.sleep(3000);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateGetTripOrderByTripNumber(lmsHelper.getTrackingNumber(packetId), packetId, ShippingMethod.NORMAL.toString(), "Delivered");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    //TODO : DL order for try n BUY complete it
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17009 , getTrip", enabled = false)
    public void getTripOrderByTripNumberForTryNBuy() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Thread.sleep(3000);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.TRY_AND_BUY, LASTMILE_CONSTANTS.TENANT_ID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateGetTripOrderByTripNumber(lmsHelper.getTrackingNumber(packetId), packetId, ShippingMethod.NORMAL.toString(), "Delivered");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    //TODO: Add in test rail
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17009 , getTrip", enabled = true)
    public void getTripOrderByTripNumberForReturn() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnId);
        lms_returnHelper.processOpenBoxReturn(returnId.toString(), EnumSCM.PICKED_UP_SUCCESSFULLY);
        String trackingNo = (String) lmsHelper.getReturnsTrackingNumber.apply(returnId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.getTripDetails(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        Long tripID = tripOrderAssignmentResponse.getTripOrders().get(0).getTripId();
        String tripNumber = (String) lmsHelper.getTripNumberByTripId.apply(tripID);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNo);
        tripOrderAssignmentResponseValidator.validateTripId(tripID);
        tripOrderAssignmentResponseValidator.validateTripOrderStatus(String.valueOf(TripOrderStatus.PS));
        tripOrderAssignmentResponseValidator.validateShipmentType(ShipmentType.PU.toString());
        tripOrderAssignmentResponseValidator.validateSourceReturnId(String.valueOf(returnId));
        Assert.assertEquals(tripOrderAssignmentResponse1.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant ID");

    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17009 , getTrip", enabled = true)
    public void getTripOrderByTripNumberForExchange() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripNumber, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateTripOrderStatus(EnumSCM.OFD);
        tripOrderAssignmentResponseValidator.validateTripId(tripId);
        tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNumber);
        tripOrderAssignmentResponseValidator.validateExchangeOrderId(packetId);
        Assert.assertEquals(tripOrderAssignmentResponse.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant Id in response");
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17233 , getTrip", enabled = true)
    public void getTripOrderByTripIDForExchange() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        //String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripId, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateTripOrderStatus(EnumSCM.OFD);
        tripOrderAssignmentResponseValidator.validateTripId(tripId);
        tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNumber);
        tripOrderAssignmentResponseValidator.validateExchangeOrderId(packetId);
        Assert.assertEquals(tripOrderAssignmentResponse.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant Id in response");
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    //Failing exchange order
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripResult", description = "ID: C17010 , getTripResult", enabled = true)
    public void getTripResult(int start, int limit, String sortBy, String sortOrder, String statusCode, String statusMessage, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        //
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        OrderResponse orderResponse = tripClient_qa.getAllFailedExchanges(Long.parseLong(DcId), start, limit, sortBy, sortOrder, LASTMILE_CONSTANTS.TENANT_ID);
        OrderResponseValidator orderResponseValidator = new OrderResponseValidator(orderResponse);
        orderResponseValidator.validateStatusCodeAndMessage(statusCode, statusMessage);

        int totalCount = (int) orderResponse.getStatus().getTotalCount();
        if (totalCount > 0) {
            List<String> trackingNum = new ArrayList<>();
            for (int index = 0; index < 20; index++) {
                Assert.assertEquals(orderResponse.getOrders().get(index).getStatus().toString(), EnumSCM.PK, "As it's a failed exchange order should not be delivered");
//                Assert.assertTrue(orderResponse.getOrders().get(index).getFailDeliveryCount() > 0);
                trackingNum.add(orderResponse.getOrders().get(index).getTrackingNumber());
            }
            trackingNum.contains(trackingNumber);
            List trackingNumDB = DBUtilities.exSelectQuery("select * from `ml_shipment` WHERE `delivery_center_id` =" + DcId + " and `shipment_type` = \"" + EnumSCM.EXCHANGE + "\" and shipment_status = \"" + EnumSCM.RECEIVED_IN_DC + "\" and `failed_attempt_count` > 0 and `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY promise_date DESC;", "myntra_lms");
            trackingNum.containsAll(trackingNumDB);
        }
    }

    //TODO: BUG- AS FOR LAST TEST DATA EXPECTED SHIPMENT TYPE IS DL BUT RESPONSE ONLY CONTAINS PU
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripResultPost1", description = "ID: C17012 , getTripResultPost", enabled = true)
    public void getTripResultPost(ShipmentType shipmentType, int start, int fetchSize, boolean isInbound, boolean isCardEnabled, String statusCode, String statusMessage, String statusType) throws IOException, NumberFormatException, JAXBException, WebClientException {//DONE
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        TripEntry tripEntry = new TripEntry();
        tripEntry.setDeliveryCenterId(Long.parseLong(DcId));
        tripEntry.setShipmentType(shipmentType);
        tripEntry.setStart(start);
        tripEntry.setLimit(fetchSize);
        tripEntry.setIsInbound(isInbound);
        tripEntry.setIsCardEnabled(isCardEnabled);
        OrderResponse orderResponse = tripClient_qa.getAllFailedOrdersForDCBaedOnTripDate(tripEntry);
        OrderResponseValidator orderResponseValidator = new OrderResponseValidator(orderResponse);
        int totalCount = (int) orderResponse.getStatus().getTotalCount();
        orderResponseValidator.validateStatusCodeAndMessage(statusCode, statusMessage);
        if (totalCount < fetchSize)
            fetchSize = totalCount;
        if (fetchSize > 0) {
            for (int index = 0; index < fetchSize; index++) {
                HashMap<String, Object> shipment_details = (HashMap<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select  shipment_status, shipment_type from order_tracking where `tenant_id` =" + LASTMILE_CONSTANTS.TENANT_ID + " and `tracking_no` = \"" + orderResponse.getOrders().get(index).getTrackingNumber() + "\";", "myntra_lms");
                orderResponseValidator.validateShipmentType(shipmentType.toString(), index);
                Assert.assertEquals(shipment_details.get("shipment_type"), shipmentType.toString(), "DB value differs for shipment_type");
            }
        }
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripResultPostPickUp", description = "ID: C17012 , getTripResultPost", enabled = true)
    public void getTripResultPostPickUp(ShipmentType shipmentType, int start, int fetchSize, String sortBy, String sortOrder, String statusCode, String statusMessage, String statusType) throws IOException, NumberFormatException, JAXBException, WebClientException {//DONE
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        TripEntry tripEntry = new TripEntry();
        tripEntry.setDeliveryCenterId(Long.parseLong(DcId));
        tripEntry.setShipmentType(shipmentType);
        tripEntry.setStart(start);
        tripEntry.setLimit(fetchSize);
        tripEntry.setSortBy(sortBy);
        tripEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        tripEntry.setDir(sortOrder);
        OrderResponse orderResponse = tripClient_qa.getAllFailedOrdersForDCBaedOnTripDate(tripEntry);
        OrderResponseValidator orderResponseValidator = new OrderResponseValidator(orderResponse);
        int totalCount = (int) orderResponse.getStatus().getTotalCount();
        orderResponseValidator.validateStatusCodeAndMessage(statusCode, statusMessage);
        if (totalCount < fetchSize)
            fetchSize = totalCount;
        if (fetchSize > 0) {
            for (int index = 0; index < fetchSize; index++) {
                HashMap<String, Object> shipment_details = (HashMap<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select  shipment_status, shipment_type from order_tracking where `tenant_id` =" + LASTMILE_CONSTANTS.TENANT_ID + " and `tracking_no` = \"" + orderResponse.getOrders().get(index).getTrackingNumber() + "\";", "myntra_lms");
                orderResponseValidator.validateShipmentType(shipmentType.toString(), index);
                Assert.assertEquals(shipment_details.get("shipment_type"), shipmentType.toString(), "DB value differs for shipment_type");
            }
        }
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getAllOnHoldOrdersForDCBasedOnFilter", description = "ID: C17012 , getTripResultPost", enabled = false)
    public void getAllOnHoldOrdersForDCBasedOnFilter(ShipmentType shipmentType, int start, int fetchSize, String sortBy, String sortOrder, String statusCode, String statusMessage, String statusType) throws IOException, NumberFormatException, JAXBException, WebClientException {//DONE
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        TripEntry tripEntry = new TripEntry();
        tripEntry.setDeliveryCenterId(Long.parseLong(DcId));
        tripEntry.setShipmentType(shipmentType);
        tripEntry.setStart(start);
        tripEntry.setLimit(fetchSize);
        tripEntry.setSortBy(sortBy);
        tripEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        tripEntry.setDir(sortOrder);
        OrderResponse orderResponse = tripClient_qa.getAllOnHoldOrdersForDCBasedOnFilter(tripEntry);
        OrderResponseValidator orderResponseValidator = new OrderResponseValidator(orderResponse);
        int totalCount = (int) orderResponse.getStatus().getTotalCount();
        orderResponseValidator.validateStatusCodeAndMessage(statusCode, statusMessage);
        if (totalCount < fetchSize)
            fetchSize = totalCount;
        if (fetchSize > 0) {
            for (int index = 0; index < fetchSize; index++) {
                HashMap<String, Object> shipment_details = (HashMap<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select  shipment_status, shipment_type from order_tracking where `tenant_id` =" + LASTMILE_CONSTANTS.TENANT_ID + " and `tracking_no` = \"" + orderResponse.getOrders().get(index).getTrackingNumber() + "\";", "myntra_lms");
                orderResponseValidator.validateShipmentType(shipmentType.toString(), index);
                Assert.assertEquals(shipment_details.get("shipment_type"), shipmentType.toString(), "DB value differs for shipment_type");
            }
        }
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "createTrip", description = "ID: C17013 , createTrip", enabled = true)
    public void createTrip(String statusMessage, String tripStatus) throws IOException, NumberFormatException, JAXBException, InterruptedException {//DONE
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        TripResponse response = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(response.getStatus().getStatusMessage(), statusMessage);
        TripResponseValidator lmsTripValidator = new TripResponseValidator(response);
        lmsTripValidator.validateCreateTrip(deliveryStaffID, tripStatus);
    }

    // TODO: isAnyTripOpenedOnStaff isn't returning the proper result
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17014, getOpenTripForStaff", enabled = true)
    public void getOpenTripForStaff() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        //TripResponse tripResponse1 = (TripResponse) lmsServiceHelper.getTrip.apply(queryParam + tripId);
        TripResponse tripResponse1;
        for (int count = 0; count < 2; count++) {
            if (count == 0)// for isAnyTripOpenedForToday
                tripResponse1 = tripClient_qa.isAnyTripOpenedForToday(tripId);
            else
                tripResponse1 = tripClient_qa.getOpenedTripsOnStaff(tripId);

            TripResponseValidator tripResponseValidator = new TripResponseValidator(tripResponse1);
            Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
            tripResponseValidator.validateGetOpenTripForStaff(1L, EnumSCM.OUT_FOR_DELIVERY);
        }
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    //------ TODO: API's from this point are saved in the collection
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "isAutoCardEnabledForDCnDF", description = "ID: C17015 , getOpenTripForStaff", enabled = true)
    public void isAutoCardEnabledForDCnDF(String dfId, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        TripResponse tripResponse = tripClient_qa.autoCardEnabled(Long.parseLong(DcId), Long.parseLong(dfId));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), statusType, "Status mismatch");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getAllAvailableTripsForDC", description = "ID: C17016 , getAllAvailableTripsForDC", enabled = true)
    public void getAllAvailableTripsForDC(long dcId, boolean dataCheck) throws Exception {
        TripResponse response = (TripResponse) tripClient_qa.getAllAvailableTripsForDC(dcId);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        if (dataCheck && response.getTrips() != null && !response.getTrips().isEmpty()) {
            Assert.assertNotNull(response.getTrips().get(0).getId());
            Assert.assertTrue(response.getStatus().getTotalCount() > 0);
        } else Assert.assertTrue(response.getStatus().getTotalCount() == 0);
    }

    //TODO: trip detail differs from db
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 7, dataProviderClass = LastMileTestDP.class, dataProvider = "getTripDetailForDate", description = "ID: C17018 , getDSRouteNameForDC", enabled = true)
    public void getTripDetailForDate(String dcId, boolean dataCheck) throws Exception {
        Date date = new Date();
        SimpleDateFormat f = new SimpleDateFormat("MMM");
        String mmm = "";
        HashMap<String, Object> row = null;
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        String day = String.valueOf(localDate.getDayOfMonth());
        String year = String.valueOf(localDate.getYear());
        if (dcId == "ToBeReplacedDcId")
            dcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        TripOrderAssignmentResponse response = tripClient_qa.getTripsDetail(Long.valueOf(dcId), lmsServiceHelper.getDateOnly.get().toString());
        if (dataCheck) {
            Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
//            List resultList = DBUtilities.exSelectQuery("select trip_number from trip where created_on >\"" + localDate.toString() + ": 00:00:00\" AND `delivery_center_id`=" + dcId + " AND `tenant_id`= " + LASTMILE_CONSTANTS.TENANT_ID + ";", "myntra_lms");
//            int totalCount = Math.toIntExact(response.getStatus().getTotalCount());
//            TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(response);
//            Assert.assertEquals(resultList.size(), totalCount, "Size of response and DB result is Mismatched");
//            for (int index = 0; index < totalCount; index++) {
//                row = (HashMap<String, Object>) resultList.get(0);
//                String time = response.getTripOrders().get(0).getDeliveryTime().toString();
//                time.contains(day);
//                time.contains(year);
//                mmm = f.format(new Date());
//                time.contains(mmm);
//                tripOrderAssignmentResponseValidator.validateTripNumber(row.get("trip_number").toString());
//            }
        } else {
            Assert.assertTrue(response.getStatus().getTotalCount() == 0);
        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 7, description = "ID: C17019 , getDeliveryStaffByMobileNumber", enabled = true)
    public void getDeliveryStaffByMobileNumber() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        DeliveryStaffResponse deliveryStaffResponse1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo(LASTMILE_CONSTANTS.MOBILE_NUM);
        Assert.assertEquals(deliveryStaffResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to find delivery staff");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17020 , getTripDetailsByTrackingNumber", enabled = true)
    public void getTripDetailsByTrackingNumber() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));

        TripOrderAssignmentResponse tripAssignmentResponse1 = tripClient_qa.getTripDetails(lmsHelper.getTrackingNumber(packetId), LASTMILE_CONSTANTS.TENANT_ID);
        TripOrderAssignmentResponseValidator lmsTripValidator = new TripOrderAssignmentResponseValidator(tripAssignmentResponse1);
        lmsTripValidator.validateGetTripDetailsByTrackingNumber(TripOrderStatus.WFD.toString(), EnumSCM.DL, packetId);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripAssignmentResponse1);
        tripOrderAssignmentResponseValidator.validateOrderId(packetId);
        tripOrderAssignmentResponseValidator.validateTripId(tripId);
        //contains tracking number TriporderAssignment

        Assert.assertEquals(tripAssignmentResponse1.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid tenant id received from response");
        Assert.assertNotNull(tripAssignmentResponse1.getTripOrders().get(0).getId());
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        TripOrderAssignmentResponse tripAssignmentResponse2 = tripClient_qa.getTripDetails(lmsHelper.getTrackingNumber(packetId), LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(tripAssignmentResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(tripAssignmentResponse2.getTripOrders().get(0).getId());
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator2 = new TripOrderAssignmentResponseValidator(tripAssignmentResponse2);
        Assert.assertEquals(tripAssignmentResponse2.getStatus().getTotalCount(), 1, "Invalid :Multiple Orders from the same tracking num");
        tripOrderAssignmentResponseValidator2.validateOrderId(packetId);
        tripOrderAssignmentResponseValidator2.validateTripId(tripId);
        Assert.assertEquals(tripAssignmentResponse2.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid tenant id received from response");
        //------------------ complete trip -----------------------
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip."); //check if API is latest
        Thread.sleep(3000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17021 , getActiveTripsForOrder", enabled = true)
    public void getActiveTripsForOrder() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripAssignmentResponse = tripClient_qa.getActiveTripForOrder(packetId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripAssignmentResponse.getStatus().getTotalCount(), 0, "Trip is active without assignment");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        TripResponse tripAssignmentResponse1 = tripClient_qa.getActiveTripForOrder(packetId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(tripAssignmentResponse1.getTrips().get(0).getId());
        //Assert.assertEquals(((TripOrderAssignmentResponse) lmsServiceHelper.unassignOrderFronTrip.apply(lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId))).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        String tripOrder = Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.unassignOrderFromTrip(tripOrder);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateTripId(tripId);
        tripOrderAssignmentResponseValidator.validateId(tripOrder);
        TripResponse tripAssignmentResponseunassigned = tripClient_qa.getActiveTripForOrder(packetId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripAssignmentResponseunassigned.getStatus().getTotalCount(), 0, "Trip is active without assignment");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        TripResponse tripAssignmentResponse2 = tripClient_qa.getActiveTripForOrder(packetId, LASTMILE_CONSTANTS.TENANT_ID);
        TripResponseValidator lmsTripValidator = new TripResponseValidator(tripAssignmentResponse2);
        lmsTripValidator.validateTripStatus(EnumSCM.OUT_FOR_DELIVERY);
        Assert.assertEquals(tripAssignmentResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(tripAssignmentResponse2.getTrips().get(0).getId());
        //------------------ complete trip -----------------------
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(3000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "startTripWithValidTripId", description = "ID C17022:  ,Start Trip Just check that api is returning 200 or not with wrong data set", enabled = true)
    public void startTripWithValidTripId(boolean is_complete, String trip_status) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        TripResponseValidator lmsValidator = new TripResponseValidator(tripResponse);
        lmsValidator.validateStartTripWithValidTripId(is_complete, trip_status);
        String shipmentStatus = lmsServiceHelper.getOrderStatusFromML(packetId);// packetid = source ref id
        Assert.assertEquals(shipmentStatus, EnumSCM.UNASSIGNED, "Invalid Shipment Status");
        String trackingNo = lmsHelper.getTrackingNumber(packetId);
        String masterBagId = lmsHelper.getMasterBagId(packetId);
        long masterBagID = Long.parseLong(masterBagId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        TripOrderAssignmentResponse tripAssignmentResponse1 = tripClient_qa.getTripDetails(lmsHelper.getTrackingNumber(packetId), LASTMILE_CONSTANTS.TENANT_ID);
        TripOrderAssignmentResponseValidator lmsTripValidator = new TripOrderAssignmentResponseValidator(tripAssignmentResponse1);
        lmsTripValidator.validateGetTripDetailsByTrackingNumber(TripOrderStatus.WFD.toString(), EnumSCM.DL, packetId);
        lmsTripValidator.validateTripOrderStatus(TripOrderStatus.WFD.toString());
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagID);
        Assert.assertEquals(shipmentOrderMapStatus.toString(), EnumSCM.RECEIVED);
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        TripOrderAssignmentResponse tripAssignmentResponse2 = tripClient_qa.getTripDetails(lmsHelper.getTrackingNumber(packetId), LASTMILE_CONSTANTS.TENANT_ID);
        TripOrderAssignmentResponseValidator lmsTripValidator1 = new TripOrderAssignmentResponseValidator(tripAssignmentResponse2);
        lmsTripValidator1.validateTripOrderStatus(EnumSCM.OFD);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusFromML(packetId), EnumSCM.OUT_FOR_DELIVERY, "Invalid Shipment Status");
        Assert.assertEquals(lmsHelper.getOrderToShipStatus(packetId), EnumSCM.OUT_FOR_DELIVERY, "Invalid Shipment Status");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "startTrip", description = "ID: C17023 ,Start Trip Just check that api is returning 200 or not with wrong data set", enabled = true)
    public void startTripWithInValidTripId(String tripId, String odometerReading, String statusType) throws Exception {
        Assert.assertEquals(lmsServiceHelper.startTrip(tripId, odometerReading).getStatus().getStatusType().toString(), statusType);
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deleteTrip", description = "ID: C17024 , deleteTrip", enabled = true)
    public void deleteTrip(String status, String statusType) throws Exception {
        String tripId = "";
        String trackingNo = "";
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        if (status.equals(EnumSCM.CREATED)) {
            TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
            tripId = tripResponse.getTrips().get(0).getId().toString();
        } else {
            String toStatus = "";
            if (status.equals((EnumSCM.OUT_FOR_DELIVERY))) {
                toStatus = EnumSCM.OFD;
            } else {
                toStatus = EnumSCM.DL;
            }
            String orderId = lmsHelper.createMockOrder(toStatus, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
            String packetId = omsServiceHelper.getPacketId(orderId);
            trackingNo = lmsHelper.getTrackingNumber(packetId);
            TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.getTripDetails(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
            TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
            tripOrderAssignmentResponseValidator.validateTripOrderStatus(toStatus);
            tripId = tripOrderAssignmentResponse.getTripOrders().get(0).getTripId().toString();
        }
        TripResponse response = lmsServiceHelper.deleteTrip(tripId);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), statusType);

    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17234 , assignReturnTODC", enabled = true)
    public void assignPickupToDC() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));
        Thread.sleep(1000);
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        List<String> trackingNum = new ArrayList<>();
        trackingNum.add(returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo());
        Assert.assertEquals(lmsServiceHelper.assignOrderToDC.apply(DcId, 1, returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo()), EnumSCM.SUCCESS);

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17026 , unAssignOrderFromTrip", enabled = true)
    public void unAssignOrderFromTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String trackingNo = lmsHelper.getTrackingNumber(packetId);
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        String tripOrder = Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.unassignOrderFromTrip(tripOrder);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateTripId(tripId);
        tripOrderAssignmentResponseValidator.validateId(tripOrder);
        tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNo);
        Assert.assertEquals(tripOrderAssignmentResponse.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Wrong tenat ID in response");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.UNASSIGNED, 2));
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        String masterBagId = lmsHelper.getMasterBagId(packetId);
        long masterBagID = Long.parseLong(masterBagId);
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagID);
        Assert.assertEquals(shipmentOrderMapStatus.toString(), EnumSCM.RECEIVED);
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(((TripOrderAssignmentResponse) lmsServiceHelper.unassignOrderFronTrip.apply(lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId))).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.unassignOrderFromTrip(Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId)));
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "As no order in trip unassign should result in error");
        //------------------ complete trip -----------------------
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(3000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17027 , unAssignOrderFromTripThroughTripId", enabled = true)
    public void unAssignOrderFromTripThroughTripId() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals((tripClient_qa.unassignOrderFromTripThroughTripId(packetId, lmsHelper.getTrackingNumber(packetId), tripId, ShipmentType.DL)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.UNASSIGNED, 2));
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals((tripClient_qa.unassignOrderFromTripThroughTripId(packetId, lmsHelper.getTrackingNumber(packetId), tripId, ShipmentType.DL)).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        //------------------ complete trip -----------------------
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    //TODO: Add in test rail
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17235 , unAssignReturnFromTrip", enabled = true)
    public void unAssignReturnFromTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));
        Thread.sleep(1000);
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        Assert.assertEquals(returnResponse1.getData().get(0).getReturnType().toString(), EnumSCM.NORMAL, "Return Mismatch");
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getReturnsTrackingNumber.apply(returnID).toString()).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + returnID, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(((TripOrderAssignmentResponse) lmsServiceHelper.unassignOrderFronTrip.apply(lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID))).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + returnID, EnumSCM.UNASSIGNED, 2));
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getReturnsTrackingNumber.apply(returnID).toString()).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + returnID, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + returnID, EnumSCM.OUT_FOR_PICKUP, 2));
        Assert.assertEquals(((TripOrderAssignmentResponse) lmsServiceHelper.unassignOrderFronTrip.apply(lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID))).getStatus().getStatusType().toString(), EnumSCM.ERROR);
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17236 , unAssignExchangeFromTrip", enabled = true)
    public void unAssignExchangeFromTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        String returnId = lmsHelper.getSourceReturnId(packetId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.unassignExchangeFromTrip(packetId, returnId, tripId, LASTMILE_CONSTANTS.TENANT_ID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        Assert.assertEquals(lmsHelper.getMLShipmentStatus(packetId), EnumSCM.UNASSIGNED, "After trip start DB status of order in ml_shipment is not `OUT_FOR_DELIVERY`");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17028 , assignAndOutScanOrderToTrip", enabled = true)
    public void assignAndOutScanOrderToTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals((tripClient_qa.unassignOrderFromTripThroughTripId(packetId, lmsHelper.getTrackingNumber(packetId), tripId, ShipmentType.DL)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.UNASSIGNED, 2));
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        //------------------ complete trip -----------------------
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    //TODO:Try and Buy case will fail on update
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17237 , assignAndOutScanDLOrderToTripTOD", enabled = true)
    public void assignAndOutScanTODOrderToTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        //reconcile

        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17029 , assignAndOutScanDLOrderToTwoTrips (multiTripAssignment)", enabled = true)
    public void assignAndOutScanDLOrderToTwoTrips() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String deliveryStaffID2 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID2 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        TripResponse trip2Response = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID2));
        Assert.assertEquals(trip2Response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId2 = trip2Response.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId2, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "100").getStatus().getStatusType().toString(), EnumSCM.ERROR);
        Assert.assertEquals(tripClient_qa.startTrip(tripId2).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        //------------------ complete trip -----------------------
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId2),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId2),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    //TODO: Add test rail
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17238 , assignAndOutScanReturnToTwoTrips (multiTripAssignment)", enabled = true)
    public void assignReturnToTwoTrips() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));
        Thread.sleep(1000);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String deliveryStaffID2 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID2, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getReturnsTrackingNumber.apply(returnID).toString()).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + returnID, EnumSCM.ASSIGNED_TO_SDA, 2));
        TripResponse trip2Response = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID2));
        Assert.assertEquals(trip2Response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId2 = trip2Response.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId2, lmsHelper.getReturnsTrackingNumber.apply(returnID).toString()).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to assignOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId2, "100").getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + returnID, EnumSCM.OUT_FOR_PICKUP, 2));
        //------------------ complete trip -----------------------
        Assert.assertEquals(lmsServiceHelper.deleteTrip("" + tripId2).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to delete trip: Post processing");
        //  DBUtilities.exUpdateQuery("update trip set trip_status ='COMPLETED' where id =" + tripId, "lms");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17030 , assignAndOutScanDLOrderOFDAndAddToSecondTrip", enabled = true)
    public void assignAndOutScanDLOrderOFDAndAddToSecondTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String deliveryStaffID2 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String trackingNo = lmsHelper.getTrackingNumber(packetId);
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID2 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        TripResponse trip2Response = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID2));
        Assert.assertEquals(trip2Response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId2 = trip2Response.getTrips().get(0).getId();
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId2, lmsHelper.getTrackingNumber(packetId));
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusMessage(), "Shipment with trackingNumber: " + trackingNo + " not in valid state. Current Status: OUT_FOR_DELIVERY");
        //------------------ complete trip -----------------------
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17031 , assignAndOutScanDeliveredOrderToTrip", enabled = true)
    public void assignAndOutScanDeliveredOrderToTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String deliveryStaffID2 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String trackingNo = lmsHelper.getTrackingNumber(packetId);
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID2 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripResponse trip2Response = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID2));
        Assert.assertEquals(trip2Response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId2 = trip2Response.getTrips().get(0).getId();
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId2, lmsHelper.getTrackingNumber(packetId));
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusMessage(), "Shipment with trackingNumber: " + trackingNo + " not in valid state. Current Status: DELIVERED");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    // TODO: Add in test rail check on addition of other scenario
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17239 , assignReturntoTripPickupSuccessFul", enabled = true)
    public void assignReturntoTripNegative() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));
        Thread.sleep(1000);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getReturnsTrackingNumber.apply(returnID).toString()).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getReturnsTrackingNumber.apply(returnID).toString()).getStatus().getStatusType().toString(), EnumSCM.ERROR);
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17032 , startTripWithNoOrders", enabled = true)
    public void startTripWithNoOrders() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType().toString(), EnumSCM.ERROR);
        Assert.assertEquals(lmsServiceHelper.deleteTrip("" + tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to delete trip: Post processing");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17033 , startTripWithSelfMarkDLOrder", enabled = true)
    public void startTripWithSelfMarkDLOrder() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String trackingNo = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, trackingNo).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to assignOrderToTrip");
        //Assigining a DL order to trip will result in an error So stopping the test case here.

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17034 , startTripWithSelfMarkDLOrder", dataProviderClass = LastMileTestDP.class, dataProvider = "markOrderDeliveredForDifferentOrders", enabled = true)
    public void markOrderDeliveredForDifferentOrders(String status, String response) throws Exception {
        String orderId = lmsHelper.createMockOrder(status, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNo = lmsHelper.getTrackingNumber(packetId);
        if (response.equalsIgnoreCase("SUCCESS"))
            Assert.assertTrue(lmsServiceHelper.updateShipmentStatusV3(trackingNo, ShipmentUpdateEvent.DELIVERED, ShipmentType.DL, ShipmentUpdateActivityTypeSource.Customer).contains("SUCCESS"));
        else
            Assert.assertFalse(lmsServiceHelper.updateShipmentStatusV3(trackingNo, ShipmentUpdateEvent.DELIVERED, ShipmentType.DL, ShipmentUpdateActivityTypeSource.Customer).contains("SUCCESS"));
    }

    //TODO: issue with test case not written properly and enabled = false
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C532 , startTripWithMultiDayOFDTrips", enabled = false)
    // The case need to be discussed further
    public void startTripWithMultiDayOFDTrips() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);

        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse0 = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse0.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        DBUtilities.exUpdateQuery("update trip set trip_date = DATE(CURRENT_DATE() - INTERVAL 1 DAY), trip_start_time = DATE(CURRENT_DATE() - INTERVAL 1 DAY), created_on = DATE(CURRENT_DATE() - INTERVAL 1 DAY) where id = " + tripId, "lms");
        DBUtilities.exUpdateQuery("update trip_order_assignment set created_on = DATE(CURRENT_DATE() - INTERVAL 1 DAY), assignment_date = DATE(CURRENT_DATE() - INTERVAL 1 DAY) where trip_id = " + tripId, "lms");
        TripResponse tripResponse = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId1 = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId1, lmsHelper.getTrackingNumber(packetId1)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId1, "10").getStatus().getStatusType().toString(), EnumSCM.ERROR);
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17035 , startTripWithSameDayOFDTrips", enabled = true)
    public void startTripWithSameDayOFDTrips() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse0 = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse0.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId1 = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId1), tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17036 , updateTripWithMultiOrderNmultiStatus", enabled = true)
    public void updateTripWithMultiOrderAndMultiStatus() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String packetId1 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String packetId2 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse0 = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse0.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId1), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId2), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId3), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId), EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId), EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);

        Assert.assertEquals(lmsServiceHelper.receiveShipment(lmsHelper.getTrackingNumber(packetId3)), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.DELIVERED, 4));
        Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(packetId1, EnumSCM.D, 4));
    }

    //TODO: Test case fails as xpress mock order is not working
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17037 , updateTripWithMultiOrderNmultiStatus", enabled = false)
    public void updateTripWithMultiShipmentTypeToMultiStatus() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String packetId3 = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry1 = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry1 = omsServiceHelper.getOrderLineEntry(orderReleaseEntry1.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse1 = rmsServiceHelper.createReturn(lineEntry1.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID1 = returnResponse1.getData().get(0).getId();
        log.info("________ReturnId1: " + returnID1);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID1)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID1);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID1));
        Thread.sleep(1000);
        String orderID2 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry2 = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID2));
        OrderLineEntry lineEntry2 = omsServiceHelper.getOrderLineEntry(orderReleaseEntry2.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse2 = rmsServiceHelper.createReturn(lineEntry2.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID2 = returnResponse2.getData().get(0).getId();
        log.info("________ReturnId2: " + returnID2);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID2)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID2);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID2));
        Thread.sleep(1000);
        ////
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getReturnsTrackingNumber.apply(returnID1).toString()).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getReturnsTrackingNumber.apply(returnID2).toString()).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId3)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(packetId3, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.updatePickupInTrip((long) lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID1), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updatePickupInTrip((long) lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID1), EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId), EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID1));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.PICKED_UP_SUCCESSFULLY);
        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID2));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.REQUESTED_RE_SCHEDULE);
        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(packetId3, EnumSCM.DELIVERED, 4));
        Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(packetId3, EnumSCM.D, 4));
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17038 , autoAssignmentOfOrderToTrip which is used in requeue orders to trips and being used in Failed delivery cases", enabled = true)
    public void autoAssignmentOfOrderToTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        tripOrderAssignmentResponse = tripClient_qa.autoAssignmentOfOrderToTrip(packetId, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        long deliveryStaffID1 = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");

        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID1);
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId1, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        TripEntry entry = new TripEntry();
        entry.setId(tripId1);
        entry.setCodAmountCollected(70.0);
        tripClient_qa.update(entry, tripId1);//updating cod amount
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17039 , closeTrip", enabled = false)
    public void completeTripWithoutTripUpdate() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.completeTripWithEmptyFields((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId)).getStatus().getStatusType().toString(), EnumSCM.ERROR, "As no field is pass it should result in error");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17040 , closeTrip", enabled = true)
    public void completeTripWithWrongStatusAndNoUpdate() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId), null).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId), AttemptReasonCode.CANNOT_PICKUP).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId), AttemptReasonCode.PICKUP_ON_HOLD_INCORRECT_PRODUCT).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY).getStatus().getStatusType().toString(), EnumSCM.ERROR);
    }

    //TODO : No test case number available
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C17041 , Not Available , signature upload ", enabled = true)
    public void OFD_Signature_DL() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        EmptyResponse emptyResponse = lmsServiceHelper.uploadSignature(lmsHelper.getTrackingNumber(packetId));
        Assert.assertEquals(emptyResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Signature not Uploaded properly");
        String signatureUrl = lmsHelper.getSignatureUrl(trackingNumber, 15);
        //Format of signature URL in ml_shipment:- lms/signature_trackingNumber_TenantId
        Assert.assertEquals(signatureUrl, "lms/signature_" + trackingNumber + "_" + LMS_CONSTANTS.TENANTID + "");
        SignatureResponse signatureResponse = lmsServiceHelper.signaturePath(trackingNumber);
        Assert.assertEquals(signatureResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Signature path not retrieved Properly");
        Assert.assertEquals(signatureResponse.getTrackingNumber().toString(), trackingNumber, "Tracking number not matching the signature path response");
        Assert.assertNotNull(signatureResponse.getS3FileUrl(), "Signature Amazon s3 is NULL");
        SignatureResponse signatureResponse1 = lmsServiceHelper.signatureDownload(trackingNumber);
        Assert.assertEquals(signatureResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Signature retrieval failed");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId), EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

    }

    // TODO : generalize code
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C19260 , getTripsDetail", enabled = true)
    public void getTripsDetail() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        String orderIdDL = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetIdDL = omsServiceHelper.getPacketId(orderIdDL);
        String trackingNoDL = lmsHelper.getTrackingNumber(packetIdDL);

        String orderIdFD = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetIdFD = omsServiceHelper.getPacketId(orderIdFD);
        String trackingNoFD = lmsHelper.getTrackingNumber(packetIdFD);

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.getTripsDetail(Long.parseLong(DcId), lmsServiceHelper.getDateOnly.get().toString());
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        int totalOrders = (int) Integer.parseInt(String.valueOf(tripOrderAssignmentResponse.getStatus().getTotalCount()));

        TripResponse tripResponse0 = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse0.getTrips().get(0).getId();
        String tripNumber = tripResponse0.getTrips().get(0).getTripNumber();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetIdDL), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetIdFD), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetIdDL, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetIdFD, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse tripOrderAssignmentResponse12 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNoFD, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetIdDL, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.getTripsDetail(Long.parseLong(DcId), lmsServiceHelper.getDateOnly.get().toString());
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse1);
        tripOrderAssignmentResponseValidator.validateOrderId(packetIdDL, totalOrders);
        tripOrderAssignmentResponseValidator.validateTripNumber(tripNumber, totalOrders);
        tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNoDL, totalOrders);
        tripOrderAssignmentResponseValidator.validateAttemptReasonCode(AttemptReasonCode.DELIVERED.toString(), totalOrders);
        // tripOrderAssignmentResponseValidator.validateCorrespondingTripStatus(TripStatus.COMPLETED.toString(), totalOrders);
        tripOrderAssignmentResponseValidator.validateOrderId(packetIdFD, totalOrders + 1);
        tripOrderAssignmentResponseValidator.validateTripNumber(tripNumber, totalOrders + 1);
        tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNoFD, totalOrders + 1);
        tripOrderAssignmentResponseValidator.validateAttemptReasonCode(AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE.toString(), totalOrders + 1);
        //   tripOrderAssignmentResponseValidator.validateCorrespondingTripStatus(TripStatus.COMPLETED.toString(), totalOrders + 1);
    }

    //TODO : In logs the response has come correctly in code its not able to parse the object properly
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17042 , getTripUpdateDashboardInfoByTripNumber", enabled = false)
    public void getTripUpdateDashboardInfoByTripNumber() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        String orderIdDL = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetIdDL = omsServiceHelper.getPacketId(orderIdDL);
        String trackingNoDL = lmsHelper.getTrackingNumber(packetIdDL);

        String orderIdFD = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetIdFD = omsServiceHelper.getPacketId(orderIdFD);
        String trackingNoFD = lmsHelper.getTrackingNumber(packetIdFD);

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.getTripsDetail(Long.valueOf(DcId), lmsServiceHelper.getDateOnly.get().toString());
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        int totalOrders = (int) Integer.parseInt(String.valueOf(tripOrderAssignmentResponse.getStatus().getTotalCount()));

        TripResponse tripResponse0 = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse0.getTrips().get(0).getId();
        String tripNumber = tripResponse0.getTrips().get(0).getTripNumber();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetIdDL)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetIdFD)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        //TripUpdateDashboardInfoResponse tripUpdateDashboardInfoResponse = tripClient.getTripUpdateDashboardInfoByTripNumber(tripNumber);
    }

    //TODO : In logs the response has come correctly in code its not able to parse the object properly
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17042 , getTripUpdateDashboardInfoV2", enabled = true)
    public void getTripUpdateDashboardInfoV2() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        String orderIdDL = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetIdDL = omsServiceHelper.getPacketId(orderIdDL);

        String orderIdFD = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetIdFD = omsServiceHelper.getPacketId(orderIdFD);

        //TripUpdateDashboardInfoResponse tripUpdateDashboardInfoResponse = tripClient.getTripUpdateDashboardInfoV2(Long.valueOf(DcId), lmsServiceHelper.getDateOnly.get().toString());

        TripResponse tripResponse0 = lmsServiceHelper.createTrip(Long.parseLong(DcId), deliveryStaffID);
        Assert.assertEquals(tripResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse0.getTrips().get(0).getId();
        String tripNumber = tripResponse0.getTrips().get(0).getTripNumber();

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetIdDL)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetIdFD)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");

        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetIdDL, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetIdFD, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
//        Thread.sleep(2000);
//        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetIdDL, tripId),
//                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    //TODO : In logs the response has come correctly in code its not able to parse the object properly
    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17042 , getTripUpdateDashboardInfoByTripId", enabled = true)
    public void getTripUpdateDashboardInfoByTripId() throws Exception {
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong("1"));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        String orderIdDL = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetIdDL = omsServiceHelper.getPacketId(orderIdDL);

        String orderIdFD = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetIdFD = omsServiceHelper.getPacketId(orderIdFD);
        TripResponse tripResponse0 = lmsServiceHelper.createTrip(1L, deliveryStaffID);
        Assert.assertEquals(tripResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        String tripNumber = tripResponse0.getTrips().get(0).getTripNumber();
        long tripId = tripResponse0.getTrips().get(0).getId();
        //TripUpdateDashboardInfoResponse tripUpdateDashboardInfoResponse = tripClient.getTripUpdateDashboardInfo(tripId);

        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetIdDL)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertEquals(lmsServiceHelper.addAndOutscanNewOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetIdFD)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");

        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetIdDL, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetIdFD, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
//        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetIdDL, tripId),
//                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
//        TripUpdateDashboardInfoResponse tripUpdateDashboardInfoResponse1 = tripClient.getTripUpdateDashboardInfo(tripId);

    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17043 , createTripWithInvalidDcId", enabled = true)
    public void createTripWithInvalidDcId() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(0L, deliveryStaffID);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Trip created with invalid DcId");
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17044 , createTripWithInvalidDcId", enabled = true)
    public void createTripWithInvalidDeliveryStaff() throws Exception {
        String deliveryStaffID = "0"; // Invalid delivery staff iID
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Trip created with invalid Delivery staff");
    }

    @Test(groups = {"Trip", "P0", "Smoke", "Regression"}, priority = 7, description = "ID: C17045 , createTripWithInvalidDcId", enabled = true)
    public void createTripWithInvalidDsAndDc() throws Exception {
        String deliveryStaffID = "0"; // Invalid delivery staff iID
        TripResponse tripResponse = tripClient_qa.createTrip(0L, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Trip created with invalid DC and DS");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17020 , getTripDetailsByTrackingNumberNeg", dataProviderClass = LastMileTestDP.class, dataProvider = "getTripDetailsByTrackingNumberNeg", enabled = true)
    public void getTripDetailsByTrackingNumberNeg(String trackingNum, String statusCode, String statusMessage, String statusType) throws Exception {
        TripOrderAssignmentResponse tripAssignmentResponse = tripClient_qa.getTripDetails(trackingNum, LASTMILE_CONSTANTS.TENANT_ID);
        if (statusType == EnumSCM.SUCCESS) {
            Assert.assertEquals(tripAssignmentResponse.getStatus().getStatusType().toString(), statusType, "Invalid Status Type Received");
        } else {
            Assert.assertEquals(tripAssignmentResponse.getStatus().getStatusMessage(), statusMessage, "Invalid Status Message Received");
            Assert.assertEquals(String.valueOf(tripAssignmentResponse.getStatus().getStatusCode()), statusCode, "Invalid Status Code Received");
            Assert.assertEquals(tripAssignmentResponse.getStatus().getTotalCount(), 0L, "Total Count Must be 0");
        }
    }

    //TODO: add this case in Test rail
    //
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C17240, addingOutScanningOrderToTrip", enabled = true)
    public void addingOutScanningOrderToTrip() throws Exception {
        {
            long wrongTripId = 5L;
            String wrongTrackingNum = "ML0000900000";
            String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
            long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
            //    DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
            String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
            String packetId = omsServiceHelper.getPacketId(orderId);
            TripResponse tripResponse0 = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
            Assert.assertEquals(tripResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
            long tripId = tripResponse0.getTrips().get(0).getId();
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), wrongTripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Able to assignOrderToTrip");
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(wrongTrackingNum, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Able to assignOrderToTrip");
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        }
    }

    //TODO: add this case in Test rail
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17241, addingOutScanningOrderToTrip", enabled = true)
    public void addingOutScanningOrderToTripForVariousStatus() throws Exception {
        {
            String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
            long deliveryStaffID = lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
            //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
            String orderIdIS = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
            String orderIdPK = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
            String orderIdAddedToMB = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
            String orderIdRCID = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
            String orderIdSH = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
            String orderIdDL = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
            String orderIdOFD = lmsHelper.createMockOrder(EnumSCM.OFD, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);

            String packetIdIS = omsServiceHelper.getPacketId(orderIdIS);
            String packetIdPK = omsServiceHelper.getPacketId(orderIdPK);
            String packetIdAddedToMB = omsServiceHelper.getPacketId(orderIdAddedToMB);
            String packetIdRCID = omsServiceHelper.getPacketId(orderIdRCID);
            String packetIdSH = omsServiceHelper.getPacketId(orderIdSH);
            String packetIdDL = omsServiceHelper.getPacketId(orderIdDL);
            String packetIdFD = omsServiceHelper.getPacketId(orderIdOFD);

            TripResponse tripResponse0 = tripClient_qa.createTrip(Long.parseLong(DcId), deliveryStaffID);
            Assert.assertEquals(tripResponse0.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
            long tripId = tripResponse0.getTrips().get(0).getId();
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetIdIS), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to assignOrderToTrip");
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetIdPK), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to assignOrderToTrip");
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetIdAddedToMB), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to assignOrderToTrip");
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetIdRCID), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetIdSH), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetIdDL), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to assignOrderToTrip");
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetIdFD), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to assignOrderToTrip");

        }
    }

    //TODO: add this case in Test rail
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17242, unassignOrderFromTrip", dataProviderClass = LastMileTestDP.class, dataProvider = "unassignOrderFromTrip", enabled = true)
    public void unassignOrderFromTrip(String status, String response) throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(status, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNo = lmsHelper.getTrackingNumber(packetId);
        long tripId;

        if (status.equalsIgnoreCase(EnumSCM.OFD) || status.equalsIgnoreCase(EnumSCM.DL)) {
            TripOrderAssignmentResponse tripAssignmentResponse = tripClient_qa.getTripDetails(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
            tripId = tripAssignmentResponse.getTripOrders().get(0).getTripId();
            Assert.assertEquals(((TripOrderAssignmentResponse) tripClient_qa.unassignOrderFromTripThroughTripId(packetId, lmsHelper.getTrackingNumber(packetId), tripId, ShipmentType.DL)).getStatus().getStatusType().toString(), EnumSCM.ERROR);

        } else {
            String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
            //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
            TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            tripId = tripResponse.getTrips().get(0).getId();
            Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNo, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Able to assignOrderToTrip");
            Assert.assertEquals(((TripOrderAssignmentResponse) tripClient_qa.unassignOrderFromTripThroughTripId(packetId, lmsHelper.getTrackingNumber(packetId), tripId, ShipmentType.DL)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        }
    }

    //TODO: Add this test case to Test Rail
    //TODO: Is ADM and Store Used ?
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17243, getAllAvailableTripsForDCByStaffType", dataProviderClass = LastMileTestDP.class, dataProvider = "getAllAvailableTripsForDCByStaffType", enabled = true)
    public void getAllAvailableTripsForDCByStaffType(String deliveryStaffType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID;
        DeliveryStaffType deliveryStaffType1;
        if (deliveryStaffType.equals(DeliveryStaffType.MYNTRA_PAYROLL.toString())) {
            deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
            deliveryStaffType1 = DeliveryStaffType.MYNTRA_PAYROLL;
        } else if (deliveryStaffType.equals(DeliveryStaffType.STORE.toString())) {
            deliveryStaffID = lmsServiceHelper.getDeliveryStaffIDForStore("" + DcId);
            deliveryStaffType1 = DeliveryStaffType.STORE;
        } else {
            deliveryStaffID = lmsServiceHelper.getDeliveryStaffIDForADM("" + DcId);
            deliveryStaffType1 = DeliveryStaffType.ADM;
        }
        if (deliveryStaffID != null) {

            //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
            String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
            String packetId = omsServiceHelper.getPacketId(orderId);
            TripResponse tripResponse0 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
            TripResponse tripResponse = tripClient_qa.getAllAvailableTripsForDCByStaffType(Long.parseLong(DcId), deliveryStaffType1);
            String tripNumber = tripResponse0.getTrips().get(0).getTripNumber();
            List<String> tripNumbers = new ArrayList<>();
            int totalCount = (int) tripResponse.getStatus().getTotalCount();
            if (totalCount > 0) {
                TripResponseValidator tripResponseValidator = new TripResponseValidator(tripResponse);
                for (int index = 0; index < totalCount; index++) {
                    tripResponseValidator.validateTripStatus(EnumSCM.CREATED, index);
                    tripResponseValidator.validateDeliveryCenterId(Integer.parseInt(DcId), index);
                    tripResponseValidator.validateDeliveryStaffType(deliveryStaffType, index);
                    tripNumbers.add(tripResponse.getTrips().get(index).getTripNumber());
                }
                Assert.assertTrue(tripNumbers.contains(tripNumber));
            }
        }
    }

//    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17244, getAllIncompleteOrdersForDCForDLOrders", dataProviderClass = LastMileTestDP.class, dataProvider = "getAllIncompleteOrdersForDCForDLOrders", enabled = true)
//    public void getAllIncompleteOrdersForDCForDLOrders(ShipmentType shipmentType, int start, int limit, String sortBy, String sortOrder, String tenantId, String status, String statusCode, String statusMessage) throws Exception {
//        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
//        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
//        String packetId = omsServiceHelper.getPacketId(orderId);
//        OrderResponse orderResponse = tripClient.getAllIncompleteOrdersForDC(Long.parseLong(DcId), shipmentType, start, limit, sortBy, sortOrder, tenantId);
//        String tracking_num = lmsHelper.getTrackingNumber(packetId);
//        int totalCount = (int) orderResponse.getStatus().getTotalCount();
//        if (totalCount < limit)
//            limit = totalCount;
//        if (limit > 0) {
//            OrderResponseValidator orderResponseValidator = new OrderResponseValidator(orderResponse);
//            orderResponseValidator.validateTrackingNumber(tracking_num);
//
//            List resultSet = DBUtilities.exSelectQuery("select * from `ml_shipment` where `shipment_type`=\"" + shipmentType.toString() + "\" and `shipment_status` = \"UNASSIGNED\" and `delivery_center_id` =" + DcId + " and `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY created_on desc;", "myntra_lms");
//            Assert.assertEquals(totalCount, resultSet.size(), "Size differs");
//            HashMap<String, Object> row = null;
//            orderResponseValidator.validateStatusCodeAndMessage(statusCode, statusMessage);
//            for (int index = 0; index < limit; index++) {
//                row = (HashMap<String, Object>) resultSet.get(index);
//                orderResponseValidator.validateDeliveryCenterID(Long.parseLong(DcId));
//                orderResponseValidator.validateTrackingNumber((String) row.get("tracking_number"), index);
//                orderResponseValidator.validateOrderStatus(EnumSCM.SH);
//                orderResponseValidator.validateShipmentType(EnumSCM.DL, index);
//            }
//        }
//    }

    //TODO : Check Sort isn't working
//    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17244, getAllIncompleteOrdersForDCForPUOrders", dataProviderClass = LastMileTestDP.class, dataProvider = "getAllIncompleteOrdersForDCForPUOrders", enabled = true)
//    public void getAllIncompleteOrdersForDCForPUOrders(ShipmentType shipmentType, int start, int limit, String sortBy, String sortOrder, String tenantId, String status, String statusCode, String statusMessage) throws Exception {
//        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
//        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
//        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
//        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
//        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
//        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
//        Long returnID = returnResponse.getData().get(0).getId();
//        log.info("________ReturnId: " + returnID);
//        Thread.sleep(3000);
//        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
//        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
//        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));
//        String trackingNo = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();
//        OrderResponse orderResponse = tripClient.getAllIncompleteOrdersForDC(Long.parseLong(DcId), shipmentType, start, limit, sortBy, sortOrder, tenantId);
//        int totalCount = (int) orderResponse.getStatus().getTotalCount();
//        if (totalCount < limit)
//            limit = totalCount;
//        if (limit > 0) {
//            OrderResponseValidator orderResponseValidator = new OrderResponseValidator(orderResponse);
//            orderResponseValidator.validateTrackingNumber(trackingNo);
//
//            List resultSet = DBUtilities.exSelectQuery("select * from `ml_shipment` where `shipment_type`=\"" + shipmentType.toString() + "\" and `shipment_status` = \"UNASSIGNED\" and `delivery_center_id` =" + DcId + " and `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY created_on desc;", "myntra_lms");
//            Assert.assertEquals(totalCount, resultSet.size(), "Size differs");
//            HashMap<String, Object> row = null;
//            orderResponseValidator.validateStatusCodeAndMessage(statusCode, statusMessage);
//            for (int index = 0; index < limit; index++) {
//                row = (HashMap<String, Object>) resultSet.get(index);
//                orderResponseValidator.validateDeliveryCenterID(Long.parseLong(DcId));
//                orderResponseValidator.validateTrackingNumber((String) row.get("tracking_number"), index);
//                orderResponseValidator.validateOrderStatus(EnumSCM.RIT);
//                orderResponseValidator.validateShipmentType(EnumSCM.PU, index);
//            }
//        }
//    }

//    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17244, getAllIncompleteOrdersForDCForExchangeOrders", dataProviderClass = LastMileTestDP.class, dataProvider = "getAllIncompleteOrdersForDCForExchangeOrders", enabled = true)
//    public void getAllIncompleteOrdersForDCForExchangeOrders(ShipmentType shipmentType, int start, int limit, String sortBy, String sortOrder, String tenantId, String status, String statusCode, String statusMessage) throws Exception {
//
//        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
//        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
//        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
//        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
//        OrderResponse orderResponse = tripClient.getAllIncompleteExchangesForDC(Long.parseLong(DcId), start, limit, sortBy, sortOrder, tenantId);
//        String tracking_num = lmsHelper.getTrackingNumber(packetId);
//        int totalCount = (int) orderResponse.getStatus().getTotalCount();
//        if (totalCount < limit)
//            limit = totalCount;
//        if (limit > 0) {
//            OrderResponseValidator orderResponseValidator = new OrderResponseValidator(orderResponse);
//            orderResponseValidator.validateTrackingNumber(tracking_num);
//
//            List resultSet = DBUtilities.exSelectQuery("select * from `ml_shipment` where `shipment_type`=\"" + shipmentType.toString() + "\" and `shipment_status` = \"UNASSIGNED\" and `delivery_center_id` =" + DcId + " and `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY created_on desc;", "myntra_lms");
//            Assert.assertEquals(totalCount, resultSet.size(), "Size differs");
//            HashMap<String, Object> row = null;
//            orderResponseValidator.validateStatusCodeAndMessage(statusCode, statusMessage);
//            for (int index = 0; index < limit; index++) {
//                row = (HashMap<String, Object>) resultSet.get(index);
//                orderResponseValidator.validateDeliveryCenterID(Long.parseLong(DcId));
//                orderResponseValidator.validateTrackingNumber((String) row.get("tracking_number"), index);
//                orderResponseValidator.validateOrderStatus(EnumSCM.PK);
//                orderResponseValidator.validateShipmentType(EnumSCM.EXCHANGE, index);
//            }
//        }
//    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17046, getAllIncompleteOrdersForDCForExchangeOrders", dataProviderClass = LastMileTestDP.class, dataProvider = "getAllIncompleteOrdersForDCForExchangeOrders", enabled = true)
    public void requeueAndDeliverExchangeOrder(ShipmentType shipmentType, int start, int limit, String sortBy, String sortOrder, String tenantId, String status, String statusCode, String statusMessage) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        //
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");


        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        // Re- queuing the tracking to get it delivered

        //MLShipmentUpdate
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");


        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID1 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        orderEntry.setOperationalTrackingId(trackingNumber1.substring(2, trackingNumber1.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber1.substring(2, trackingNumber1.length()), LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }


    //Adding 6 flows

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17245, failRequeueExchangeAndDLFlow", enabled = true)
    public void failRequeueExchangeAndDLFlow() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        //
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        //change it to failed
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        //getAllFailedExchanges
        //TODO: Use Get ALl FAIled Exchange to check status not DL.it wont come

        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        // Re- queuing the tracking to get it delievered
        //MLShipmentUpdate
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        //TODO: Use Get ALl FAIled Exchange to check status not DL.it will come check status . ml_shipment_exchnage check status

        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID1 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //2 fails done now requeue and deliver the order
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator1 = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse1);
        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        String deliveryStaffID2 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //    DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID2 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse2 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID2));
        long tripId2 = tripResponse2.getTrips().get(0).getId();
        String trackingNumber2 = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber2, tripId2, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId2).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId2 = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId2,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId2, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId2,
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, tripId2, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        // DB check ml_shipment

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C17246 , failRequeueRTOExchangeFlow", enabled = true)
    public void failRequeueRTOExchangeFlow() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //    DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        //
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        Thread.sleep(2000L);

        checkReconStatus(packetId, String.valueOf(lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(trackingNumber)), EnumSCM.RECEIVED_IN_DC, "true");

        // Re- queuing the tracking to get it delivered

        //MLShipmentUpdate
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID1 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        System.out.println("RTO");
        Thread.sleep(10000);
        date = f.format(new Date());
        String returnId = lmsHelper.getSourceReturnId(packetId);
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber1);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("initiating RTO for shipment");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        Thread.sleep(10000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.RTO_CONFIRMED, 2));

        TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
        long masterBagId = (long) tmsServiceHelper.createcloseMBforRTO(20L, packetId, DcId);
        tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);

        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WH");

        Assert.assertEquals(
                lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive shipment in DC");

        ExceptionHandler.handleTrue(lmsServiceHelper.validateOrderStatusInLMS(packetId, ShipmentStatus.RTO_IN_TRANSIT.toString(), 10));

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17247, failRequeueReturnAndDLFlow", enabled = true)
    public void failRequeueReturnAndDLFlow() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        lms_returnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.FAILED_PICKUP);
        String trackingNo = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();
        Map<String, Object> shipment_status = (Map<String, Object>) lmsHelper.getShipmentStatusForPickup.apply(trackingNo);
        Assert.assertEquals(shipment_status.get("shipment_status"), EnumSCM.FAILED_PICKUP, "Expected failed pickup");
        //requeue
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNo);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("failed pickup requeued for another attempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        lms_returnHelper.processReturnWithoutNewValidation(returnID.toString(), EnumSCM.FAILED_PICKUP);
        //requeue
        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        lms_returnHelper.processReturnWithoutNewValidation(returnID.toString(), EnumSCM.PICKED_UP_SUCCESSFULLY);
        //assign dc change

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17248, failRequeueReturnAndDLFlow", enabled = true)
    public void failRequeueRTOReturnFlow() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        lms_returnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.FAILED_PICKUP);
        String trackingNo = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();
        Map<String, Object> shipment_status = (Map<String, Object>) lmsHelper.getShipmentStatusForPickup.apply(trackingNo);
        Assert.assertEquals(shipment_status.get("shipment_status"), EnumSCM.FAILED_PICKUP, "Expected failed pickup");
        //requeue
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNo);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("failed pickup requeued for another attempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        lms_returnHelper.processReturnWithoutNewValidation(returnID.toString(), EnumSCM.FAILED_PICKUP);
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNo);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("failed pickup rejected");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RETURN_REJECTED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        Thread.sleep(7000);
        Map<String, Object> shipment_status1 = (Map<String, Object>) lmsHelper.getShipmentStatusForPickup.apply(trackingNo);
        Assert.assertEquals(shipment_status1.get("shipment_status"), EnumSCM.PICKUP_REJECTED, "Expected failed pickup");
    }

    //for DL orders
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C17249 , failRequeueReturnAndDLFlow", enabled = true)
    public void failRequeueAndDLFlow() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        // String deliveryStaffID =  "" +lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String deliveryStaffID = "3396";
        String tracking_num = lmsHelper.getTrackingNumber(packetId);

        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //requeue
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);


        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.autoAssignmentOfOrderToTrip(packetId, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator1 = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse1);
        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID1 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");

        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId1, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //requeue
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator2 = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse2);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.autoAssignmentOfOrderToTrip(packetId, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator3 = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse3);

        String deliveryStaffID2 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID2 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");

        TripResponse tripResponse2 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID2));
        Assert.assertEquals(tripResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId2 = tripResponse2.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId2, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId2).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId2),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId2),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C17250, failRequeueReturnAndDLFlow", enabled = true)
    public void failRequeueRTODLFlow() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String tracking_num = lmsHelper.getTrackingNumber(packetId);

        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //requeue
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);


        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.autoAssignmentOfOrderToTrip(packetId, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator1 = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse1);
        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID1 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");

        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId1, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(tracking_num);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.DL);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        long masterBagId = (long) tmsServiceHelper.createcloseMBforRTO(20L, packetId, DcId);
        tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);

        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WH");

        Assert.assertEquals(
                lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive shipment in DC");

        ExceptionHandler.handleTrue(lmsServiceHelper.validateOrderStatusInLMS(packetId, ShipmentStatus.RTO_IN_TRANSIT.toString(), 10));


    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19261, failRequeueReturnAndDLFlow", enabled = true)
    public void processRTOTillReturnHub() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", LASTMILE_CONSTANTS.WAREHOUSE_36, EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
        String releaseId = omsServiceHelper.getReleaseId(orderId);
        Long destPremiseId = 20L;

        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(tracking_num);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.DL);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        long masterBagId = (long) tmsServiceHelper.createcloseMBforRTO(destPremiseId, packetId, DcId);
        tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);

    }

    //TODO : date range needs to be included
    //Adding cases for API's which is not called validation to be added and Add in Test Rail
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , getAllIncompleteScheduledShipmentsForDC", dataProviderClass = LastMileTestDP.class, dataProvider = "getAllIncompleteScheduledShipmentsForDC", enabled = false)
    public void getAllIncompleteScheduledShipmentsForDC(String queryParam) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        queryParam = queryParam.replace("ToBeReplacedDCID", DcId);
        OrderResponse orderResponse = lmsServiceHelper.getAllIncompleteScheduledShipmentsForDC(queryParam);
        OrderResponseValidator orderResponseValidator = new OrderResponseValidator(orderResponse);
        orderResponseValidator.validateStatusType(EnumSCM.SUCCESS);

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , bulkCreateTrip", dataProviderClass = LastMileTestDP.class, dataProvider = "bulkCreateTrip", enabled = true)
    public void bulkCreateTrip(String queryParam) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.HSR_ML);
        queryParam = queryParam.replace("ToBeReplacedDCID", DcId);
        TripResponse tripResponse = lmsServiceHelper.createTripsForAvailableDeliveryStaff(queryParam);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Expected success");
        Assert.assertTrue(tripResponse.getStatus().getTotalCount() > 0, "Total trip created should be greater than 1");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , bulkCreateTrip", dataProviderClass = LastMileTestDP.class, dataProvider = "bulkCreateTrip", enabled = true)
    public void bulkDeleteTrip(String queryParam) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.HSR_ML);
        queryParam = queryParam.replace("ToBeReplacedDCID", DcId);
        TripResponse tripResponse = lmsServiceHelper.bulkDelete(queryParam);
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Expected success");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , assignOrder", dataProviderClass = LastMileTestDP.class, dataProvider = "assignOrder", enabled = true)
    public void assignOrder(String toStatus, String status) throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(toStatus, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), status, "Unable to assignOrderToTrip");

    }

//    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , getAllIncompleteOrdersForDCForTrynBuy", dataProviderClass = LastMileTestDP.class, dataProvider = "getAllIncompleteOrdersForDCForTrynBuy", enabled = true)
//    public void getAllIncompleteOrdersForDCForTrynBuy(ShipmentType shipmentType, int start, int limit, String sortBy, String sortOrder, String tenantId, String status, String statusCode, String statusMessage) throws Exception {
//
//        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
//        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
//        String packetId = omsServiceHelper.getPacketId(orderId);
//        OrderResponse orderResponse = tripClient.getAllIncompleteOrdersForDC(Long.parseLong(DcId), shipmentType, start, limit, sortBy, sortOrder, tenantId);
//        String tracking_num = lmsHelper.getTrackingNumber(packetId);
//        int totalCount = (int) orderResponse.getStatus().getTotalCount();
//        if (totalCount < limit)
//            limit = totalCount;
//        if (limit > 0) {
//            OrderResponseValidator orderResponseValidator = new OrderResponseValidator(orderResponse);
//            orderResponseValidator.validateTrackingNumber(tracking_num);
//
//            List resultSet = DBUtilities.exSelectQuery("select * from `ml_shipment` where `shipment_type`=\"" + shipmentType.toString() + "\" and `shipment_status` = \"UNASSIGNED\" and `delivery_center_id` =" + DcId + " and `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY created_on desc;", "myntra_lms");
//            Assert.assertEquals(totalCount, resultSet.size(), "Size differs");
//            HashMap<String, Object> row = null;
//            orderResponseValidator.validateStatusCodeAndMessage(statusCode, statusMessage);
//            for (int index = 0; index < limit; index++) {
//                row = (HashMap<String, Object>) resultSet.get(index);
//                orderResponseValidator.validateDeliveryCenterID(Long.parseLong(DcId));
//                orderResponseValidator.validateTrackingNumber((String) row.get("tracking_number"), index);
//                orderResponseValidator.validateOrderStatus(EnumSCM.SH);
//                orderResponseValidator.validateShipmentType(EnumSCM.TRY_AND_BUY, index);
//            }
//        }
//    }

    //TODO: Create a store bag and add to trip and check if data is populating
    //NOT USING CLIENT AS WE HAVE TO SHIPMENT TYPE WHICH IS PASSED AS A PARAMETER MAKING THE RESPONSE RETURNING NULL
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , findShipmentsByTripNumber", dataProviderClass = LastMileTestDP.class, dataProvider = "findShipmentsByTripNumber", enabled = false)
    public void findShipmentsByTripNumber(String param) throws Exception {
        TripShipmentAssociationResponse tripShipmentAssociationResponse = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Expected DL");
    }
    //TODO: Create a store bag and add to trip and check if data is populating

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , findShipmentsByTripNumberReverseBag", enabled = true)
    public void findShipmentsByTripNumberReverseBag1() throws Exception {
        String tripNumber = "Automation1-190312004936369";
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(tripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected REVERSE_BAG");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , startTripAtDifferentStatus", enabled = true)
    public void startTripAtDifferentStatus() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String tracking_num = lmsHelper.getTrackingNumber(packetId);

        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.startTrip("" + tripId, "10").getStatus().getStatusType().toString(), EnumSCM.ERROR, "Trip Started at wrong status");

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , printTab", enabled = true)
    public void printTabForTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String tracking_num = lmsHelper.getTrackingNumber(packetId);

        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        TripResponse tripResponse1 = tripClient_qa.findById(tripId);
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Print tab not working");
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripResponse1.getTrips().get(0).getTripNumber(), tripNumber, "Wrong trip details");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , printTab", enabled = true)
    public void printTabForSDA() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient_qa.findById(Long.valueOf(deliveryStaffID));
        Map<String, Object> staffDetails = DBUtilities.exSelectQueryForSingleRecord("select * from delivery_staff where id = " + deliveryStaffID, "lms");
        DeliveryStaffValidator deliveryStaffValidator = new DeliveryStaffValidator(deliveryStaffResponse);
        deliveryStaffValidator.validateTenantId(staffDetails.get("tenant_id").toString());
        deliveryStaffValidator.validateFirstName(staffDetails.get("first_name").toString());
        deliveryStaffValidator.validateLastName(staffDetails.get("last_name").toString());
        deliveryStaffValidator.validateMobileNum(staffDetails.get("mobile").toString());
        deliveryStaffValidator.validateIsAvailable((Integer) staffDetails.get("is_available"));
        deliveryStaffValidator.validateIsDeleted((Integer) staffDetails.get("is_deleted"));


    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19262, failRequeueReturnAndDLFlow", enabled = true)
    public void getDcDetails() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCDetails(DcId);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateDeliveryHubCode(LASTMILE_CONSTANTS.DELIVERY_HUB_CODE_ELC);
        deliveryCenterValidator.validateDCID(DcId);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateTotalCount(1L);
        List resultSet = DBUtilities.exSelectQuery("select * from delivery_center where `id`=" + DcId + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "myntra_lms");
        HashMap<String, Object> row = null;
        row = (HashMap<String, Object>) resultSet.get(0);
        deliveryCenterValidator.validateDCID(Long.toString((Long) row.get("id")));
        deliveryCenterValidator.validateCourierCode((String) row.get("courier_code"));
        deliveryCenterValidator.validateCode((String) row.get("code"));
        deliveryCenterValidator.validateTenantId((String) row.get("tenant_id"));

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch", description = "ID: C19262, deliveryCenterSearch", enabled = true)
    public void deliveryCenterSearch(String queryParam) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where tenant_id =" + LASTMILE_CONSTANTS.TENANT_ID + " and is_active = 1 and type != \"" + LAST_MILE_PARTNER + "\"", "myntra_lms");
        HashMap<String, Object> row = null;
        Long totalCount = deliveryCenterResponse.getStatus().getTotalCount();
        deliveryCenterValidator.validateTotalCount(deliveryCenterResponse.getStatus().getTotalCount());
        Assert.assertEquals(resultSet.size(), Integer.parseInt(String.valueOf(totalCount)), "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateDCID(index, String.valueOf((Long) row.get("id")));// ID
            deliveryCenterValidator.validateIsActive(index, true);
            Assert.assertEquals(deliveryCenterResponse.getDeliveryCenters().get(index).getTenantId(), (String) row.get("tenant_id"), "Wrong Tenant ID");
            deliveryCenterValidator.validateCode(index, String.valueOf(row.get("code")));
            deliveryCenterValidator.validateType(index, String.valueOf(row.get("type")));

        }
    }


    //TODO : 63 data via query select * from `delivery_center` where is_active = 1 and is_card_enabled = 1 and  `id` in ( select `hlp_dc_id` from `dc_hlp_config` where `delivery_center_id` = 5 and is_active = 1 and is_card_enabled = 1)
    // 61 via response
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19263 , failRequeueReturnAndDLFlow", enabled = true)
    public void findAllStoreForDC() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.findAllStoreForDC(DcId);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_3));
    }

    //TODO : count mismatch
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "getActiveDeliveryStaff", description = "ID: C19264, getActiveDeliveryStaff", enabled = true)
    public void getActiveDeliveryStaff(String queryParam) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        DeliveryStaffResponse deliveryStaffResponse = lmsServiceHelper.deliveryStaff(queryParam);
        DeliveryStaffValidator deliveryStaffValidator = new DeliveryStaffValidator(deliveryStaffResponse);
        deliveryStaffValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_3));
        deliveryStaffValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        int totalCount = (int) deliveryStaffResponse.getStatus().getTotalCount();
        if (totalCount > 0)
            Assert.assertTrue(true);

        // TODO : commenting due to data inconsistency
        //        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_staff` where tenant_id = " + LASTMILE_CONSTANTS.TENANT_ID + " and is_available = 1 and is_deleted = 0", "myntra_lms");
//        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
//        HashMap<String, Object> row = null;
//        for (int index = 0; index < totalCount; index++) {
//            row = (HashMap<String, Object>) resultSet.get(index);
//            deliveryStaffValidator.isAvailable(index, true);
//            deliveryStaffValidator.isDeleted(index, false);
//            deliveryStaffValidator.validateFirstName(index, row.get("first_name").toString());
//            deliveryStaffValidator.validateLastName(index, row.get("last_name").toString());
//            deliveryStaffValidator.validateTenantId(index, (String) row.get("tenant_id"));

    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch2", description = "ID: C19262, deliveryCenterSearch2", enabled = true)
    public void deliveryCenterSearch2(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from delivery_center where `courier_code` LIKE \"" + LASTMILE_CONSTANTS.COURIER_CODE_ML + "%\" and is_active = 1 and tenant_id = " + LASTMILE_CONSTANTS.TENANT_ID, "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateName(index, row.get("name").toString());
            deliveryCenterValidator.validateIsActive(index, true);
        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch3", description = "ID:C19262 , deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch3(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from delivery_center where `courier_code` LIKE \"" + LASTMILE_CONSTANTS.COURIER_CODE_ML + "%\" and is_active = 1 and tenant_id =" + LASTMILE_CONSTANTS.TENANT_ID, "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateName(index, row.get("name").toString());
            deliveryCenterValidator.validateIsActive(index, true);
        }
    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "getDeliveryStaffSearch", description = "ID:19265 , getActiveDeliveryStaff", enabled = true)
    public void getDeliveryStaffSearch(String queryParam) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        DeliveryStaffResponse deliveryStaffResponse = lmsServiceHelper.deliveryStaff(queryParam);
        DeliveryStaffValidator deliveryStaffValidator = new DeliveryStaffValidator(deliveryStaffResponse);
        deliveryStaffValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_813));
        deliveryStaffValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        int totalCount = (int) deliveryStaffResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_staff` where tenant_id =" + LASTMILE_CONSTANTS.TENANT_ID, "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");

        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryStaffValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryStaffValidator.validateFirstName(index, row.get("first_name").toString());
            deliveryStaffValidator.validateLastName(index, row.get("last_name").toString());
            deliveryStaffValidator.validateDcId(index, row.get("delivery_center_id").toString());

        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch4", description = "ID: C19262, deliveryCenterSearch4", enabled = false)
    public void deliveryCenterSearch4(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where code = \"" + LASTMILE_CONSTANTS.DELIVERY_HUB_CODE_ELC + "\"" + " and hlp_recon_type=\"" + EOD_RECON.toString() + "\" and `is_card_enabled` = 1 ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateName(index, row.get("name").toString());
            deliveryCenterValidator.validateIsActive(index, true);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());

        }
    }

    //Searching by Manager name for Elc DC
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch5", description = "ID: C19262, deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch5(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where manager = \"Mohan Kumar\"  and `is_card_enabled` = 1 and `tenant_id`= " + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateManagerName(index, "Mohan Kumar");
            deliveryCenterValidator.validateIsActive(index, true);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());

        }

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch6", description = "ID: , deliveryCenterSearch3", enabled = false)
    public void deliveryCenterSearch6(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where city = \"Bangalore\" and  tenant_id=" + LASTMILE_CONSTANTS.TENANT_ID + " and `is_card_enabled` = 1 ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateCity(index, "bangalore");
            deliveryCenterValidator.validateIsActive(index, true);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());

        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch7", description = "ID:C19262 , deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch7(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `self_ship_supported` = true and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " and `is_card_enabled` = 1  ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateIsSelfShipped(index, true);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());

        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch8", description = "ID: C19262, deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch8(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `self_ship_supported` = false and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " and `is_card_enabled` = 1  ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateIsSelfShipped(index, false);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());

        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch9", description = "ID: C19262 , deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch9(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `is_active` = 1 and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " and `is_card_enabled` = 1  ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateIsActive(index, true);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());

        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch10", description = "ID:C19262 , deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch10(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `name` =\"ANNA NAGAR (ABT)\"  and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " and `is_card_enabled` = 1  ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateIsActive(index, true);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());


        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch11", description = "ID: C19262, deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch11(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `contact_no` = 9840644767  and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " and `is_card_enabled` = 1  ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateContactNum(index, "9840644767");
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());


        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch12", description = "ID: C19262, deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch12(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `state` = \"Tamil Nadu\"  and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " and `is_card_enabled` = 1  ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateState(index, "Tamil Nadu");
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());

        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch13", description = "ID: C19262, deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch13(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `is_strict_serviceable` = true  and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " and `is_card_enabled` = 1  ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateIsStrictServiceable(index, true);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());


        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch14", description = "ID:C19262 , deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch14(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `is_strict_serviceable` = false  and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " and `is_card_enabled` = 1  ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateIsStrictServiceable(index, false);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());


        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch15", description = "ID: C19262, deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch15(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `is_active` = 0 and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " and `is_card_enabled` = 1  ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateIsActive(index, false);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());


        }
    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch16", description = "ID: C19262, deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch16(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `courier_code` = \"DE\" and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCourierCode(index, "DE");
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());


        }
    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch17", description = "ID: C19262, deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch17(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `hlp_recon_type` = \"EOD_RECON\" and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateStoreReconType(index, "EOD_RECON");
            deliveryCenterValidator.validateID(index, row.get("id").toString());
        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch18", description = "ID: C19262, deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch18(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where `pincode` = 560068 and  tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY " + LASTMILE_CONSTANTS.SORT_BY_CODE + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validatePincode(index, "560068");
            deliveryCenterValidator.validateID(index, row.get("id").toString());
        }
    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch19", description = "ID: C1926, deliveryCenterSearch3", enabled = true)
    public void deliveryCenterSearch19(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where tenant_id= " + LASTMILE_CONSTANTS.TENANT_ID + " ORDER BY " + LASTMILE_CONSTANTS.CONTACT_NO + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateContactNum(index, row.get("contact_no").toString());

        }
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "deliveryCenterSearch20", description = "ID: C19262, deliveryCenterSearch3", enabled = false)
    public void deliveryCenterSearch20(String queryParam) throws Exception {
        DeliveryCenterResponse deliveryCenterResponse = lmsServiceHelper.getDCSearch(queryParam);
        DeliveryCenterValidator deliveryCenterValidator = new DeliveryCenterValidator(deliveryCenterResponse);
        deliveryCenterValidator.validateStatusType(StatusResponse.Type.SUCCESS);
        deliveryCenterValidator.validateStatusCode(Integer.parseInt(LASTMILE_CONSTANTS.STATUS_CODE_803));
        int totalCount = (int) deliveryCenterResponse.getStatus().getTotalCount();
        List resultSet = DBUtilities.exSelectQuery("select * from `delivery_center` where tenant_id= 4019 and code = \"" + LASTMILE_CONSTANTS.DELIVERY_HUB_CODE_ELC + "\" ORDER BY " + LASTMILE_CONSTANTS.CONTACT_NO + " " + LASTMILE_CONSTANTS.SORT_ORDER_ASC + " limit 20", "myntra_lms");
        HashMap<String, Object> row = null;
        Assert.assertEquals(resultSet.size(), totalCount, "Count Mismatch");
        for (int index = 0; index < totalCount; index++) {
            row = (HashMap<String, Object>) resultSet.get(index);
            deliveryCenterValidator.validateTenantId(index, LASTMILE_CONSTANTS.TENANT_ID);
            deliveryCenterValidator.validateCode(index, row.get("code").toString());
            deliveryCenterValidator.validateID(index, row.get("id").toString());
            deliveryCenterValidator.validateCourierCode(index, row.get("courier_code").toString());
            deliveryCenterValidator.validateContactNum(index, row.get("contact_no").toString());

        }
    }

    //TODO: Create a store bag and add to trip and check if data is populating
    //NOT USING CLIENT AS WE HAVE TO SHIPMENT TYPE WHICH IS PASSED AS A PARAMETER MAKING THE RESPONSE RETURNING NULL
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19266 , findShipmentsByTripNumber", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void findShipmentsByTripNumber(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                          String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {


        //Store creation Experiment

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);


        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);
        //Delivering
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");


    }


    //TODO: Create a store bag and add to trip and check if data is populating
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = false)
    public void findShipmentsByTripNumberReverseBag(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                    String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);


        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.getAndAddDeliveryStaffID(originPremiseId));

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId), LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);
        //Delivering
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        // Assert.assertEquals(tripShipmentAssociationResponse.get);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        // DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient.findDeliveryStaffByMobNo(mobileNumber);
//        long expectedstoreDeliveryStaffId = Long.parseLong(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile());
//        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentClientV2_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);

        //tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(tripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, LASTMILE_CONSTANTS.TENANT_ID);

        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickip
        // Pickup the reverse bag

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStore(String.valueOf(reverseBagId), forwardTrackingNumbersList, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, forwardTrackingNumbersList.size(), 0, 0.0f, LASTMILE_CONSTANTS.TENANT_ID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");

        //validateDuringReconciliation(deliveryStaffID, DcId, tracking_num, 1L, EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");

    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19268 , updateCODAmount", enabled = true)
    public void updateCODAmount() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Thread.sleep(3000);
        TripEntry tripEntry = new TripEntry();
        tripEntry.setCodAmountCollected(234D);
        tripEntry.setId(Long.valueOf(DcId));
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateGetTripOrderByTripNumber(lmsHelper.getTrackingNumber(packetId), packetId, ShippingMethod.NORMAL.toString(), "Delivered");
        TripResponse tripResponse1 = (TripResponse) lmsServiceHelper.updateCodAmount(tripEntry, String.valueOf(tripId));
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "cod amount not updated");
        TripResponseValidator tripResponseValidator = new TripResponseValidator(tripResponse1);
        tripResponseValidator.validateDeliveryCenterId(Integer.parseInt(DcId));
        tripResponseValidator.validateDeliveryStaffId(deliveryStaffID);
        tripResponseValidator.validateTripNumber(tripNumber);
        tripResponseValidator.validateCODAmount("234.0");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    //TODO: not working
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19269, dcToDcTransferForDLOrders", dataProviderClass = LastMileTestDP.class, dataProvider = "dcToDcTransferForDLOrders", enabled = false)
    public void dcToDcTransferForDLOrders(long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.NORTH_CGH, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        String trackingNumber = lmsServiceHelper.getTrackingNumber(courierCode, "36", "", LMS_PINCODE.NORTH_CGH, "").getTrackingNumberEntry().getTrackingNumber();
        ShipmentResponse shipmentResponse = tmsServiceHelper.autoMasterBag(trackingNumber);
        try {
            Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply("CAR", masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
            Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        } catch (InterruptedException | IOException | JAXBException | ManagerException | JSONException | XMLStreamException e) {
            log.error("ERROR:: while receiving master bag in TMS", e.getCause());
        }
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply("ELC", masterBagId, containerId);
        tmsServiceHelper.intercityTransfer.accept(masterBagId);
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 5, "DC");


        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");


    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19270 , dcToDcTransferForDLOrders", dataProviderClass = LastMileTestDP.class, dataProvider = "dcToDcTransferForExchangeOrders", enabled = false)
    public void dcToDcTransferForExchangeOrders(long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.NORTH_CGH, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "on", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);

        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        MLShipmentResponse mlShipmentResponse = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator = new MLShipmentResponseValidator(mlShipmentResponse);
        mlShipmentResponseValidator.validateShipmentStatus(EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC

        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        MLShipmentResponse mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.ASSIGNED_TO_OTHER_DC);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC

        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(1).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId + "," + transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        MLShipmentResponse mlShipmentResponse3 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator3 = new MLShipmentResponseValidator(mlShipmentResponse3);
        mlShipmentResponseValidator3.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);

        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 5, "DC");
        lmsServiceHelper.masterBagInScanUpdate(masterBagId, packetId, "Bangalore",
                5, "DC", 5);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        MLShipmentResponse mlShipmentResponse4 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator4 = new MLShipmentResponseValidator(mlShipmentResponse4);
        mlShipmentResponseValidator4.validateShipmentStatus(EnumSCM.UNASSIGNED);

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        orderEntry.setOperationalTrackingId(tracking_num.substring(2, tracking_num.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        //validateDuringReconciliation(deliveryStaffID, DcId, tracking_num, 1L, EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num.substring(2, tracking_num.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");


    }


    //Add 2 orders to trip remove 1 order from trip and add to another complete both the trip
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19271 , assignOrderTOAnotherTrip", enabled = true)
    public void assignOrderTOAnotherTrip() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);


        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();

        //second trip
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        String tripNumber1 = tripResponse1.getTrips().get(0).getTripNumber();


        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId1), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.ASSIGNED_TO_SDA, 2));

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId1), tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19272, multipleTripsForOneSDAOnSameDay", enabled = true)
    public void multipleTripsForOneSDAOnSameDay() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);


        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();

        //second trip
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        String tripNumber1 = tripResponse1.getTrips().get(0).getTripNumber();


        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId1), tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");


    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19273, createTripForTwoDiffDC", enabled = true)
    public void createTripForTwoDiffDC() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String DcId1 = pincodeDCMap.get(LMS_PINCODE.HSR_ML);

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();

        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId1));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID1 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId1), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");


    }

    //neg case
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19274, createTripForTwoDiffDC", enabled = true)
    public void addingMasterBagToTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);

        long masterBagId = lmsServiceHelper.createMasterBag(Long.valueOf(DcId), com.myntra.lms.client.status.PremisesType.DC.toString(), Long.valueOf(DcId), com.myntra.lms.client.status.PremisesType.DC.toString(), EnumSCM.NORMAL, EnumSCM.COURIER_CODE_ML).getEntries().get(0).getId();

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, String.valueOf(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to assignOrderToTrip");

    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19275, addOrdersInTripsWithDiffMlShipmentStatus", dataProviderClass = LastMileTestDP.class, dataProvider = "addOrdersInTripsWithDiffMlShipmentStatus", enabled = true)
    public void addOrdersInTripsWithDiffMlShipmentStatus(String toStatus, String res, String shipmentType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String tracking_num = lmsHelper.getTrackingNumber(packetId);

        //Db update
        lmsHelper.updateShipmentStatus(tracking_num, toStatus);
        lmsHelper.updateShipmentType(tracking_num, shipmentType);
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), res, "Unable to assignOrderToTrip");
        if (res.equalsIgnoreCase(EnumSCM.SUCCESS)) {
            Assert.assertEquals(((TripOrderAssignmentResponse) tripClient_qa.unassignOrderFromTripThroughTripId(packetId, lmsHelper.getTrackingNumber(packetId), tripId, ShipmentType.DL)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        }
    }

    //should fail
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19276, addOrdersInTripsWithDiffMlShipmentStatus", enabled = false)
    public void shToRTO() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.UNRTO, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19278 , addOneOrderInTripOfAnotherDC", enabled = true)
    public void addOneOrderInTripOfAnotherDC() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String DcId1 = pincodeDCMap.get(LMS_PINCODE.HSR_ML);

        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId1));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId1), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to assignOrderToTrip");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19279 , dcToDcTransferForDLOrders", dataProviderClass = LastMileTestDP.class, dataProvider = "dcToDcTransferForTryBuyOrders", enabled = false)
    public void dcToDcTransferForTOADOrders(long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.NORTH_CGH, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        MLShipmentResponse mlShipmentResponse = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator = new MLShipmentResponseValidator(mlShipmentResponse);
        mlShipmentResponseValidator.validateShipmentStatus(EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC

        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        MLShipmentResponse mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.ASSIGNED_TO_OTHER_DC);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC

        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(1).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId + "," + transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        MLShipmentResponse mlShipmentResponse3 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator3 = new MLShipmentResponseValidator(mlShipmentResponse3);
        mlShipmentResponseValidator3.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);

        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 5, "DC");
        lmsServiceHelper.masterBagInScanUpdate(masterBagId, packetId, "Bangalore",
                5, "DC", 5);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        MLShipmentResponse mlShipmentResponse4 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator4 = new MLShipmentResponseValidator(mlShipmentResponse4);
        mlShipmentResponseValidator4.validateShipmentStatus(EnumSCM.UNASSIGNED);

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");


        //deliver order
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, tracking_num);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(tracking_num.substring(2, tracking_num.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Map<String, Object> dataMap1 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", tripOrderAssignmentId);
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        List<Map<String, Object>> tripData = new ArrayList<>();
        tripData.add(dataMap1);
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);


    }


}