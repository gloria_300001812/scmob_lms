package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.DeliveryCenterClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClientV2_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.client.MLShipmentServiceV2Client;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.ItemEntry;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.*;
import com.myntra.lordoftherings.SlackMessenger;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class ShipmentRecon {

    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);


    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private OrderEntry orderEntry = new OrderEntry();
    private ProcessRelease processRelease = new ProcessRelease();
    MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(LMS_Trip.class);
    HashMap<String, String> pincodeDCMap;
    ItemEntry itemEntry = new ItemEntry();
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa=new MLShipmentClientV2_QA();

    //creating HashMap to maintain pincode and DcId
    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
    }

    public void checkReconStatus(String orderId,String tripOrderAssignmentId, String status, String isReceived)
    {
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(orderId, status, 4), "Wrong status in ml_shipment");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),isReceived,"Wrong status in trip_order_assignment");
    }


    public void validateDuringReconciliation(String deliveryStaffID,String DcId,String trackingNumber, long expected, String expectedReconcileStatus)
    {
        StatusResponse res; TripOrderAssignmentResponse tripOrderAssignmentResponse;
        long first,second;
        if (expected==0L)
        {
            first=0L;second=0L;
        }
        else{
            first = expected;
            second = expected-1;
        }
        tripOrderAssignmentResponse = lmsServiceHelper.getExpectedShipmentMessage(DcId, deliveryStaffID);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getTotalCount(), first, "Expected count mismatch");

//        res = lmsServiceHelper.getPendingShipmentMessage(DcId, deliveryStaffID);
//        Assert.assertEquals(res.getTotalCount(), first, "Expected Count mismatch");

        tripOrderAssignmentResponse = lmsServiceHelper.receiveShipmentAtDC(trackingNumber, DcId, deliveryStaffID);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), expectedReconcileStatus, "unable to receive in DC");

        tripOrderAssignmentResponse = lmsServiceHelper.getExpectedShipmentMessage(DcId, deliveryStaffID);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getTotalCount(), second, "Expected count mismatch");

//        res = lmsServiceHelper.getPendingShipmentMessage(DcId, deliveryStaffID);
//        Assert.assertEquals(res.getTotalCount(), second, "Expected Count mismatch");

    }
    //Full Forward Flow testing:
    /*Reconciling at every step in this flow
    Create 3 orders -> Fail 2 -> Deliver 1 -> Complete trip should Fail -> Reconcile -> Complete Trip -> assign new order to same DS Fail -> recocile -> complete trip-> requeue
    Last Order Create new Trip For same DL staff Dl it -> complete trip -> requeue another failed order assign to a new DS and Deliver it-> Reconcile should not be expected. -> Reconcile
    multiple times reconciled order. Validation trip order assignment :is_received and ml shipment Received in dc.
    */

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19309, Forward all flow test", enabled = true)
    public void failDlReconFullFlow() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);

        String orderId2 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId2 = omsServiceHelper.getPacketId(orderId2);

        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        String trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);//DL

        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();

        //reconcile fail

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L, EnumSCM.ERROR);

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId1), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId2), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L, EnumSCM.ERROR);

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId2, EnumSCM.ASSIGNED_TO_SDA, 2));

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L, EnumSCM.ERROR);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId), EnumSCM.FAILED_DELIVERY, "false");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,2L, EnumSCM.SUCCESS);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,1L,EnumSCM.SUCCESS);

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.RECEIVED_IN_DC, "true");

        String orderId3 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId3 = omsServiceHelper.getPacketId(orderId3);

        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        String trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber3, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber3,0L,EnumSCM.ERROR);
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber3,0L,EnumSCM.ERROR);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId1),
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId3, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId1),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId3, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete trip.");
       tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId1);
       //reconcile
        checkReconStatus(packetId3, String.valueOf(tripOrderAssignmentId), EnumSCM.FAILED_DELIVERY, "false");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber3,1L,EnumSCM.SUCCESS);
        checkReconStatus(packetId3, String.valueOf(tripOrderAssignmentId), EnumSCM.RECEIVED_IN_DC, "true");
        //requeue
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber3);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        TripResponse tripResponse2 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId2 = tripResponse2.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId3), tripId2, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber3,0L, EnumSCM.ERROR);
        Assert.assertEquals(tripClient_qa.startTrip(tripId2).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber3,0L,EnumSCM.ERROR);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId2),
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId3, tripId2,orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        //shipment count expected should be 0
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber3,0L,EnumSCM.ERROR);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId2),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId3, tripId2,orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        //shipment count expected should be 0 in reconcilition
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber3,0L,EnumSCM.ERROR);
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber);
        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        String deliveryStaffID2 = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        TripResponse tripResponse3 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID2));
        Assert.assertEquals(tripResponse3.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId3 = tripResponse3.getTrips().get(0).getId();
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId3, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        Assert.assertEquals(tripClient_qa.startTrip(tripId3).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId3),
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId3,orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        //shipment count expected should be 0
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId3),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, tripId3,orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        System.out.println("Done");
        //shipment count expected should be 0 in reconcilition
        tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId3);
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
    }


  /*Create 2 exchange orders-> fail 1 -> exchange 1 -> requeue fail order -> New DS assign requeue order and Deliver -> assign new order to same DS Fail */

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19310, Forward all flow test", enabled = true)
    public void failExchangeReconFullFlow() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);//exchange

        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder1 = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID1));
        String packetId1 = omsServiceHelper.getPacketId(exchangeOrder1);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);//Fail

        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,0L,EnumSCM.ERROR);

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,0L,EnumSCM.ERROR);

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,0L,EnumSCM.ERROR);
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);//DL
        long tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId1);//FAILED

        orderEntry.setOperationalTrackingId(trackingNumber.substring(2,trackingNumber.length()));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId,orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Trip not updated");
        String pickup_tracking_number = tripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber();

        orderEntry.setOperationalTrackingId(trackingNumber1.substring(2,trackingNumber1.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId1, tripId,orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Thread.sleep(2000);

        //complete trip error
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", tripOrderAssignmentId);
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        dataMap1.put("tripId",tripId);
        dataMap1.put("exchangeOrderId", packetId);
        dataMap2.put("trip_order_assignment_id",tripOrderAssignmentId1);
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        dataMap2.put("tripId",tripId);
        dataMap2.put("exchangeOrderId",packetId1);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        Thread.sleep(2000);
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
        checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentId1), EnumSCM.FAILED_DELIVERY, "false");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2,trackingNumber.length()),2L,EnumSCM.SUCCESS);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,1L,EnumSCM.SUCCESS);

        long tripOrderAssignmentIdForPickup = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(pickup_tracking_number);
        long tripOrderAssignmentIdOrderForFailedExchange = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(trackingNumber1);
        checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentIdOrderForFailedExchange), EnumSCM.RECEIVED_IN_DC, "true");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForPickup)),"true","Wrong status in trip_order_assignment");

        //trip complete
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //requeue

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber1);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        //reconcile error
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,0L,EnumSCM.ERROR);


        String deliveryStaffID1 = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID1, DcId, trackingNumber1,0L,EnumSCM.ERROR);

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID1, DcId, trackingNumber1,0L,EnumSCM.ERROR);

        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId1, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.OUT_FOR_DELIVERY, 2));
        Long tripOrderAssignmentId2 = (Long) lmsHelper.getTripOrderAssignmentIdByTrackingNumOFD.apply(trackingNumber1);// requed failed order

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID1, DcId, trackingNumber1,0L,EnumSCM.ERROR);
        String tripNumber = tripResponse1.getTrips().get(0).getTripNumber();

        //TODO: get correct ID from db by query
        orderEntry.setOperationalTrackingId(trackingNumber1.substring(2,trackingNumber1.length()));
        tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId2+1L,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId1, tripId1,orderEntry);

        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Trip not updated");
        pickup_tracking_number = tripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber();

        //reconcile exchanged order
        validateDuringReconciliation(deliveryStaffID1, DcId, trackingNumber1.substring(2,trackingNumber1.length()),1L,EnumSCM.SUCCESS);

        tripOrderAssignmentIdForPickup = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNumPS.apply(pickup_tracking_number);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForPickup)),"true","Wrong status in trip_order_assignment");


        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId2,
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        // new order to same delivery staff


        orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        packetId = omsServiceHelper.getPacketId(exchangeOrder);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);//exchange

        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        tripId = tripResponse.getTrips().get(0).getId();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);//DL

        orderEntry.setOperationalTrackingId(trackingNumber.substring(2,trackingNumber.length()));
        tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId,orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Trip not updated");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete trip.");
        pickup_tracking_number = tripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber();
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2,trackingNumber.length()),1L,EnumSCM.SUCCESS);

        tripOrderAssignmentIdForPickup = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(pickup_tracking_number);

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForPickup)),"true","Wrong status in trip_order_assignment");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19311 , Forward all flow test", enabled = true)
    public void exchangeReconcileFlow() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);//exchange

        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);//DL

        orderEntry.setOperationalTrackingId(trackingNumber.substring(2,trackingNumber.length()));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId,orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Trip not updated");
       Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete trip.");
        String pickup_tracking_number = tripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber();
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2,trackingNumber.length()),1L,EnumSCM.SUCCESS);

        long tripOrderAssignmentIdForPickup = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(pickup_tracking_number);

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForPickup)),"true","Wrong status in trip_order_assignment");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    /*
    Create 4 orders (Tried bought, not tried and bought, tried and not bought , Failed Try and buy) ->
    Should not receive (Tried and bought) receive (Not tried and bought, tried and not bought (2)) -> Requeue 2 received in dc order -> and deliver it -> Should not be expected in DC.
    * 1-Fail 2- Tried And not Bought 3-not tried and bought 4- Tried And bought
    * */
//Try And Buy

    //fail 2 try buy order requeue and deliver
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19312, Forward all flow test", enabled = true)
    public void failTrynBuyRequeAndDlRecon() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);

        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);


        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,0L,EnumSCM.ERROR);


        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,0L,EnumSCM.ERROR);


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        long tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId);

        orderEntry.setOperationalTrackingId(trackingNumber.substring(2,trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId,orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        orderEntry.setOperationalTrackingId(trackingNumber1.substring(2,trackingNumber1.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId,orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", tripOrderAssignmentId);
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);

        dataMap2.put("trip_order_assignment_id",tripOrderAssignmentId1);
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);

        tripData.add(dataMap1);
        tripData.add(dataMap2);

        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        //reconcile
        checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId), EnumSCM.FAILED_DELIVERY, "false");
        checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId1), EnumSCM.FAILED_DELIVERY, "false");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,2L, EnumSCM.SUCCESS);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,1L,EnumSCM.SUCCESS);

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.RECEIVED_IN_DC, "true");
        checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId1), EnumSCM.RECEIVED_IN_DC, "true");

        //trip complete
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //requeue 1
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        //requeue 2
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber1);
        mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");


        //create new trip
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber1 = tripResponse1.getTrips().get(0).getTripNumber();
        long tripId1 = tripResponse1.getTrips().get(0).getId();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,0L,EnumSCM.ERROR);

        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNumOFD.apply(trackingNumber);
        tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNumOFD.apply(trackingNumber1);

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,0L,EnumSCM.ERROR);

        //deliver order
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2,trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId1,orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        orderEntryDetails = omsServiceHelper.getOrderEntry(orderID1);
        amount = orderEntryDetails.getFinalAmount();
        orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber1);
        Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber1.substring(2,trackingNumber1.length()));
        orderEntry.setOrderId(packetId1);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries1 = new ArrayList<>();
        entries1.add(itemEntry);
        orderEntry.setItemEntries(entries1);


        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId1, tripId1,orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,0L,EnumSCM.ERROR);

        dataMap1.put("trip_order_assignment_id", tripOrderAssignmentId);
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        dataMap2.put("trip_order_assignment_id",tripOrderAssignmentId1);
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        tripData.add(dataMap1);
        tripData.add(dataMap2);

        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,0L,EnumSCM.ERROR);

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,0L,EnumSCM.ERROR);
    }

    //tried and not bought order reconciliation
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19313, Forward all flow test", enabled = true)
    public void triedAndNotBoughtReconQCPass() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        Double amount = orderEntryDetails.getFinalAmount();
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2,trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);
        itemEntry.setRemarks("not bought");
        itemEntry.setQcStatus(ItemQCStatus.PASSED);
        itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
// mark delivered and reconcile shouldn't work
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId,orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Trip not updated");
        String ml_shipment_id = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
        String return_tracking_num = lmsHelper.updateOperationalTrackingNumForTryAndBuy(ml_shipment_id);
        //reconcile

        checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        validateDuringReconciliation(deliveryStaffID, DcId, return_tracking_num.substring(2,return_tracking_num.length()),1L,EnumSCM.SUCCESS);
        Long tripOrderAssignmentIdForReturn = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(return_tracking_num);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForReturn)),"true","Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(return_tracking_num), EnumSCM.PICKUP_SUCCESSFUL, "Status not updated to received in DC");
        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19314, Forward all flow test", enabled = true)
    public void triedAndNotBoughtReconQCFail() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        Double amount = orderEntryDetails.getFinalAmount();
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2,trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);
        itemEntry.setRemarks("not bought");
        itemEntry.setQcStatus(ItemQCStatus.FAILED);
        itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
// mark delivered and reconcile shouldn't work
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId,orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Trip not updated");
        String ml_shipment_id = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
        String return_tracking_num = lmsHelper.updateOperationalTrackingNumForTryAndBuy(ml_shipment_id);
        //reconcile

        checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        validateDuringReconciliation(deliveryStaffID, DcId, return_tracking_num.substring(2,return_tracking_num.length()),1L,EnumSCM.SUCCESS);
        Long tripOrderAssignmentIdForReturn = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(return_tracking_num);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForReturn)),"true","Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(return_tracking_num), EnumSCM.PICKUP_SUCCESSFUL, "Status not updated to received in DC");
        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    //tried and bought order shouldn't be expected in dc
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19315 , Forward all flow test", enabled = true)
    public void triedAndBoughtRecon() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2,trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId,orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Trip not updated");
       //reconcile
        checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2,trackingNumber.length()),0L,EnumSCM.ERROR);

        checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }
    //not tried and bought -> recon not required
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19316 , Forward all flow test", enabled = true)
    public void notTriedAndBoughtRecon() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2,trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.NOT_TRIED);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId,orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS, "Trip not updated");
        //reconcile
        checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2,trackingNumber.length()),0L,EnumSCM.ERROR);

        checkReconStatus(packetId,String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    //return flow starts here
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19317 , Forward all flow test", enabled = true)
    public void failedPickup() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID(DcId);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India",LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track =lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
       //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        SlackMessenger.send("scm_e2e_order_sanity", "Trip Creation for pickup complete");
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip( tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.FAILED, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        //reconcile not required for failed pickup
       // checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentId), EnumSCM.FAILED_PICKUP, "false");

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"false","Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.FAILED_PICKUP, "shipment status mismatch");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);
       // checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentId), EnumSCM.RECEIVED_IN_DC, "true");

        //complete trip
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19318, Forward all flow test", enabled = true)
    public void pickupSuccessful() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID(DcId);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India",LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track =lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip( tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"false","Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKUP_SUCCESSFUL, "shipment status mismatch");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"true","Wrong status in trip_order_assignment");

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19319 , Forward all flow test", enabled = true)
    public void pickupDoneQCApproved() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID(DcId);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India",LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track =lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip( tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"false","Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"true","Wrong status in trip_order_assignment");

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_DONE, trackingNo);

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),0L,EnumSCM.SUCCESS);

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),0L,EnumSCM.SUCCESS);

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19320, Forward all flow test", enabled = true)
    public void pickupDoneQCOnHold() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID(DcId);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India",LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track =lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip( tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"false","Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"true","Wrong status in trip_order_assignment");

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),0L,EnumSCM.ERROR);

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ONHOLD_PICKUP_WITH_DC, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),0L,EnumSCM.ERROR);

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19321 , Forward all flow test", enabled = true)
    public void pickupDoneQCOnHoldccApprove() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID(DcId);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track, 0L, EnumSCM.ERROR);

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track, 0L, EnumSCM.ERROR);


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo, 0L, EnumSCM.ERROR);

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo, 0L, EnumSCM.ERROR);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo, 0L, EnumSCM.ERROR);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 1L, EnumSCM.SUCCESS);

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 0L, EnumSCM.ERROR);

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ONHOLD_PICKUP_WITH_DC, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 0L, EnumSCM.ERROR);
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_APPROVE,false);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 0L, EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKUP_SUCCESSFUL," Pickup expected to be successful");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19322, Forward all flow test", enabled = true)
    public void pickupDoneQCOnHoldccReject() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID(DcId);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track, 0L, EnumSCM.ERROR);

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track, 0L, EnumSCM.ERROR);


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo, 0L, EnumSCM.ERROR);

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo, 0L, EnumSCM.ERROR);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo, 0L, EnumSCM.ERROR);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 1L, EnumSCM.SUCCESS);

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 0L, EnumSCM.ERROR);
        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ONHOLD_PICKUP_WITH_DC, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 0L, EnumSCM.ERROR);
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_REJECTED, false);
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 0L, EnumSCM.ERROR);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo),EnumSCM.REJECTED_ONHOLD_PICKUP_WITH_DC,"QC rejected by cc status mismatch");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19323 , Forward all flow test", enabled = true)
    public void returnMarkedHappyWithProduct() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID(DcId);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India",LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track =lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        SlackMessenger.send("scm_e2e_order_sanity", "Trip Creation for pickup complete");
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip( tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.HAPPY_WITH_PRODUCT, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        //reconcile not required for failed pickup
        // checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentId), EnumSCM.FAILED_PICKUP, "false");

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"false","Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.FAILED_PICKUP, "shipment status mismatch");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);
        // checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentId), EnumSCM.RECEIVED_IN_DC, "true");

        //complete trip
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.HAPPY_WITH_PRODUCT, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.ASSIGNED_TO_DC, "shipment status mismatch");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19324, Forward all flow test", enabled = true)
    public void onHoldCCReject() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID(DcId);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India",LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track =lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        SlackMessenger.send("scm_e2e_order_sanity", "Trip Creation for pickup complete");
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip( tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"false","Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER, "shipment status mismatch");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

       //trip complete
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
        //rejected by cc
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_REJECTED, true);

        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.RETURN_REJECTED, "shipment status mismatch");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19325 , Forward all flow test", enabled = true)
    public void onHoldCCApprove() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID(DcId);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India",LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track =lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        SlackMessenger.send("scm_e2e_order_sanity", "Trip Creation for pickup complete");
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip( tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"false","Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER, "shipment status mismatch");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        //trip complete
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        //rejected by cc
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_APPROVE,true);

        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.UNASSIGNED, "shipment status mismatch");

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        //pickup successful after cc approval

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, track,0L,EnumSCM.ERROR);

        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // scan tracking number in trip
        scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // Start Trip
        startTripRes = tripClient_qa.startTrip( tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"false","Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKUP_SUCCESSFUL, "shipment status mismatch");

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId),"true","Wrong status in trip_order_assignment");

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),0L,EnumSCM.SUCCESS);

    }

    //Add 1 order in Trip A and 2nd Order in Trip B , Fail both the orders, receive trip B order in trip A it should fail. Reconcile and complete the trip.
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19326, Forward all flow test", enabled = true)
    public void reconcilePendingOrderOfAnotherSDA() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);

        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);

        String deliveryStaffID = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();

        String deliveryStaffID1 = lmsServiceHelper.getDeliveryStaffID("" + DcId);
        DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID1 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        String tripNumber1 = tripResponse1.getTrips().get(0).getTripNumber();

        //reconcile fail


        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId1), tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        // Reconcile 2nd order in 1st Trip Expected ERROR

        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,1L,EnumSCM.ERROR);

        // Reconcile 1st order in 2nd Trip Expected ERROR

        validateDuringReconciliation(deliveryStaffID1, DcId, trackingNumber,1L,EnumSCM.ERROR);

        //reconcile
        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,1L,EnumSCM.SUCCESS);
        validateDuringReconciliation(deliveryStaffID1, DcId, trackingNumber1,1L,EnumSCM.SUCCESS);


        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId1),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId1, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

}

