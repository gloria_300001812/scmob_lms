package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.client.DeliveryCenterClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.Toolbox;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.lastmile.client.code.utils.ReconType.EOD_RECON;

public class CashReconTestDP {

    static Initialize init = new Initialize("/Data/configuration");
    static String FGTenantID = "";
    static String env = getEnvironment();
    static DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);

    public CashReconTestDP() {
        if (LMS_CONSTANTS.FGVersion == 3) {
            FGTenantID = "tenantId.eq:" + LMS_CONSTANTS.TENANTID + "___";
        }
        if (LMS_CONSTANTS.FGVersion == 0) {
            FGTenantID = "";
        }

    }

    @DataProvider/*(parallel = true)*/
    public static Object[][] lostRto(ITestContext testContext) {
        Object[] arr1 = {EnumSCM.LOST};
        Object[] arr2 = {EnumSCM.RTO};
        Object[][] dataSet = new Object[][]{arr1,arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object[][] StorelostRto(ITestContext testContext) {
        Object[] arr1 = {EnumSCM.LOST , EnumSCM.LOST};
        Object[] arr2 = {EnumSCM.RTO , EnumSCM.RTO_CONFIRMED};
        Object[][] dataSet = new Object[][]{arr1,arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] markingLostForDiffOrder(ITestContext testContext)
    {
        Object[] arr1 = { EnumSCM.PK, EnumSCM.SUCCESS};
        Object[] arr2 = { EnumSCM.IS, EnumSCM.SUCCESS};
        Object[] arr3 = { EnumSCM.ADDED_TO_MB, EnumSCM.SUCCESS};
        Object[] arr4 = { EnumSCM.SH, EnumSCM.SUCCESS};
        Object[] arr7 = { EnumSCM.FD, EnumSCM.SUCCESS};

        Object[][] dataSet = new Object[][] { arr1, arr2, arr3, arr4};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }
}