package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.utility.RejoyServiceHelper;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class Lms3PlTest {
    String env=getEnvironment();
    private LmsServiceHelper lmsServiceHelper=new LmsServiceHelper();
    private LMSHelper lmsHelper=new LMSHelper();
    private OMSServiceHelper omsServiceHelper=new OMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper=new LMS_ReturnHelper();
    LMS_3plHelper lms_3plHelper=new LMS_3plHelper();
    LMS_CreateOrder lms_createOrder=new LMS_CreateOrder();


    @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="Deliver3pl",groups={"Sanity"}, priority=0, enabled=true,
            description="Deliver the 3pl order")
    public void Deliver3plOrder( String pincode , String courier , String warehouseId, long originPremisesId, long destinationPremisesId ) throws Exception {

        // create Shipment till in scan
        String packetId=omsServiceHelper.getPacketId(
                lmsHelper.createMockOrder(EnumSCM.IS , pincode , courier , warehouseId , EnumSCM.NORMAL , "cod" , false , true));

        // Create master bag and add to master bag
        Long masterBagId=
                lmsServiceHelper.createMasterBag(originPremisesId , "HUB" , "Bangalore" , destinationPremisesId , "DC" , "Bangalore" ,
                        EnumSCM.SUCCESS).getEntries().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId , ""+masterBagId , ShipmentType.DL) ,
                EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to close masterBag");

        //Ship MB
        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to ship masterBag");

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
        // Deliver the 3pl Order
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.DELIVERED , ShipmentType.DL, CourierCode.EK.toString());

    }

    @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="Deliver3pl",groups={"Sanity"}, priority=0, enabled=true,
            description="EXCHANGE:- Deliver the 3pl order")
    public void Deliver3plOrderExchange(String pincode , String courier , String warehouseId, long originPremisesId, long destinationPremisesId   ) throws Exception {

        // create Shipment till in scan
        String orderID=lmsHelper.createMockOrder(EnumSCM.DL , LMS_PINCODE.ML_BLR , "ML" , warehouseId , EnumSCM.NORMAL ,
                "cod" , false , true);
        String exchangeOrder=lms_createOrder.createMockOrderExchange(EnumSCM.IS , pincode , courier , warehouseId ,
                EnumSCM.NORMAL , "cod" , false , true , omsServiceHelper.getReleaseId(orderID));
        String packetId=omsServiceHelper.getPacketId(exchangeOrder);

        // Create master bag and add to master bag
        Long masterBagId=
                lmsServiceHelper.createMasterBag(originPremisesId , "HUB" , "Bangalore" , destinationPremisesId , "DC" , "Bangalore" ,
                        EnumSCM.SUCCESS)
                        .getEntries().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId , ""+masterBagId , ShipmentType.DL) ,
                EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to close masterBag");

        //Ship MB
        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to ship masterBag");

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.EXCHANGE, CourierCode.EK.toString());
        // Deliver the 3pl Order
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.DELIVERED , ShipmentType.EXCHANGE, CourierCode.EK.toString());

    }

    /*@Test(groups = {"Sanity"}, priority = 0, enabled = true,
            description = "try n buy:- Deliver the 3pl order")
    public void Deliver3plOrderTryNBuy() throws Exception {

        // create Shipment till in scan
        String packetId = omsServiceHelper.getPacketId(
                lmsHelper.createMockOrder(EnumSCM.IS, pincode, courier, warehouseId, EnumSCM.NORMAL, "cod", true, true));

        // Create master bag and add to master bag
        Long masterBagId =
                lmsServiceHelper.createMasterBag(originPremisesId, "HUB", "Bangalore", destinationPremisesId, "DC", "Bangalore", EnumSCM.SUCCESS)
                        .getEntries().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, ShipmentType.DL), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close masterBag");

        //Ship MB
        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to ship masterBag");

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId, ShipmentUpdateEvent.OUT_FOR_DELIVERY,ShipmentType.TRY_AND_BUY);
        // Deliver the 3pl Order
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId, ShipmentUpdateEvent.DELIVERED, ShipmentType.TRY_AND_BUY);

    }
*/
    @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="Deliver3pl",groups={"Sanity"}, priority=0, enabled=true,
            description="Failed Deliver the 3pl Order")
    public void FailedDeliver3plOrder( String pincode , String courier , String warehouseId, long originPremisesId, long destinationPremisesId  ) throws Exception {

        // create Shipment till in scan
        String packetId=omsServiceHelper.getPacketId(
                lmsHelper.createMockOrder(EnumSCM.IS , pincode , courier , warehouseId , EnumSCM.NORMAL , "cod" , false , true));
        String trackingNum=lmsHelper.getTrackingNumber(packetId);

        // Create master bag and add to master bag
        Long masterBagId=
                lmsServiceHelper.createMasterBag(originPremisesId , "HUB" , "Bangalore" , destinationPremisesId , "DC" , "Bangalore" ,
                        EnumSCM.SUCCESS)
                        .getEntries().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId , ""+masterBagId , ShipmentType.DL) ,
                EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to close masterBag");

        final String shipmentStatus1=lms_returnHelper.getShipmentStatusFromOrder_to_ship(trackingNum);
        Assert.assertEquals(shipmentStatus1 , EnumSCM.ADDED_TO_MB , "order is not in ADDED_TO_MB state");

        //Ship MB
        final ShipmentResponse shipmentResponse=lmsServiceHelper.shipMasterBag(masterBagId);

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.FAILED_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
    }


        @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="Deliver3pl",groups={"Sanity"}, priority=0, enabled=true,
            description="EXCHANGE:- Failed Deliver the 3pl Order")
    public void FailedDeliver3plOrderExchange( String pincode , String courier , String warehouseId, long originPremisesId, long destinationPremisesId  ) throws Exception {

        // create Shipment till in scan
        String orderID=lmsHelper.createMockOrder(EnumSCM.DL , LMS_PINCODE.ML_BLR , "ML" , warehouseId , EnumSCM.NORMAL ,
                "cod" , false , true);
        String exchangeOrder=lms_createOrder.createMockOrderExchange(EnumSCM.IS , pincode , courier , warehouseId ,
                EnumSCM.NORMAL , "cod" , false , true , omsServiceHelper.getReleaseId(orderID));
        String packetId=omsServiceHelper.getPacketId(exchangeOrder);

        // Create master bag and add to master bag
        Long masterBagId=
                lmsServiceHelper.createMasterBag(originPremisesId , "HUB" , "Bangalore" , destinationPremisesId , "DC" , "Bangalore" ,
                        EnumSCM.SUCCESS)
                        .getEntries().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId , ""+masterBagId , ShipmentType.DL) ,
                EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to close masterBag");

        //Ship MB
        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to ship masterBag");

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
        // Deliver the 3pl Order
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.FAILED_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());

    }

    @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="Deliver3pl",groups={"Sanity"}, priority=0, enabled=true,
            description="try n buy:- Failed Deliver the 3pl Order")
    public void FailedDeliver3plOrderTryNBuy( String pincode , String courier , String warehouseId, long originPremisesId, long destinationPremisesId  ) throws Exception {

        // create Shipment till in scan
        String packetId=omsServiceHelper.getPacketId(
                lmsHelper.createMockOrder(EnumSCM.IS , pincode , courier , warehouseId , EnumSCM.NORMAL , "cod" , true , true));

        // Create master bag and add to master bag
        Long masterBagId=
                lmsServiceHelper.createMasterBag(originPremisesId , "HUB" , "Bangalore" , destinationPremisesId , "DC" , "Bangalore" ,
                        EnumSCM.SUCCESS)
                        .getEntries().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId , ""+masterBagId , ShipmentType.TRY_AND_BUY) ,
                EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to close masterBag");

        //Ship MB
        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to ship masterBag");

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.TRY_AND_BUY, CourierCode.EK.toString());
        // Deliver the 3pl Order
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.FAILED_DELIVERY ,
                ShipmentType.TRY_AND_BUY, CourierCode.EK.toString());

    }


    @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="Deliver3pl",groups={"Sanity"}, priority=0, enabled=true,
            description="Receive the failed delivered Order in Wh")
    public void ReceiveFailedDeliveredOrderInWh(String pincode , String courier , String warehouseId, long originPremisesId, long destinationPremisesId   ) throws Exception {

        // create Shipment till in scan
        lms_returnHelper.insertTrackingNumberRHD("DE-COD");
        String packetId=omsServiceHelper.getPacketId(
                lmsHelper.createMockOrder(EnumSCM.IS , pincode , courier , warehouseId , EnumSCM.NORMAL , "cod" , false , true));
        String trackingNum=lmsHelper.getTrackingNumber(packetId);

        final String shipmentStatus=lms_returnHelper.getShipmentStatusFromOrder_to_ship(trackingNum);
        Assert.assertEquals(shipmentStatus , EnumSCM.INSCANNED , "order is not in INSCANNED state");

        // Create master bag and add to master bag
        Long masterBagId=
                lmsServiceHelper.createMasterBag(originPremisesId , "HUB" , "Bangalore" , destinationPremisesId , "DC" , "Bangalore" ,
                        EnumSCM.SUCCESS)
                        .getEntries().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId , ""+masterBagId , ShipmentType.DL) ,
                EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to close masterBag");

        final String shipmentStatus1=lms_returnHelper.getShipmentStatusFromOrder_to_ship(trackingNum);
        Assert.assertEquals(shipmentStatus1 , EnumSCM.ADDED_TO_MB , "order is not in ADDED_TO_MB state");

        //Ship MB
        final ShipmentResponse shipmentResponse=lmsServiceHelper.shipMasterBag(masterBagId);

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.FAILED_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.RTO_CONFIRMED , ShipmentType.DL, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.RTO_DISPATCHED , ShipmentType.DL, CourierCode.EK.toString());

        //Receive FD order in Warehouse
        RejoyServiceHelper.recieveShipmentInRejoy(packetId);

        final String shipmentStatus7=lms_returnHelper.getShipmentStatusFromOrder_to_ship(trackingNum);
        Assert.assertEquals(shipmentStatus7 , EnumSCM.DELIVERED_TO_SELLER ,
                "order is not in DELIVERED_TO_SELLER state");

    }


    @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="Deliver3pl",groups={"Sanity"}, priority=0, enabled=true,
            description="EXCHANGE:- Receive the failed delivered Order in Wh")
    public void ReceiveFailedDeliveredOrderInWhExchange( String pincode , String courier , String warehouseId, long originPremisesId, long destinationPremisesId  ) throws Exception {

        // create Shipment till in scan
        lms_returnHelper.insertTrackingNumberRHD("DE-COD");
        String orderID=lmsHelper.createMockOrder(EnumSCM.DL , LMS_PINCODE.ML_BLR , "ML" , warehouseId , EnumSCM.NORMAL ,
                "cod" , false , true);
        String exchangeOrder=lms_createOrder.createMockOrderExchange(EnumSCM.IS , pincode , courier , warehouseId ,
                EnumSCM.NORMAL , "cod" , false , true , omsServiceHelper.getReleaseId(orderID));
        String packetId=omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNum=lmsHelper.getTrackingNumber(packetId);

        // Create master bag and add to master bag
        Long masterBagId=
                lmsServiceHelper.createMasterBag(originPremisesId , "HUB" , "Bangalore" , destinationPremisesId , "DC" , "Bangalore" ,
                        EnumSCM.SUCCESS)
                        .getEntries().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId , ""+masterBagId , ShipmentType.DL) ,
                EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to close masterBag");

        //Ship MB
        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to ship masterBag");

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.FAILED_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.RTO_CONFIRMED , ShipmentType.DL, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.RTO_DISPATCHED , ShipmentType.DL, CourierCode.EK.toString());

        //Receive FD order in Warehouse
        RejoyServiceHelper.recieveShipmentInRejoy(packetId);

        final String shipmentStatus7=lms_returnHelper.getShipmentStatusFromOrder_to_ship(trackingNum);
        Assert.assertEquals(shipmentStatus7 , EnumSCM.DELIVERED_TO_SELLER ,
                "order is not in DELIVERED_TO_SELLER state");


    }

    @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="Deliver3pl",groups={"Sanity"}, priority=0, enabled=true,
            description="try n buy:- Receive the failed delivered Order in Wh")
    public void ReceiveFailedDeliveredOrderInWhTryNBuy(String pincode , String courier , String warehouseId, long originPremisesId, long destinationPremisesId   ) throws Exception {

        // create Shipment till in scan
        lms_returnHelper.insertTrackingNumberRHD("DE-COD");
        String packetId=omsServiceHelper.getPacketId(
                lmsHelper.createMockOrder(EnumSCM.IS , pincode , courier , warehouseId , EnumSCM.NORMAL , "cod" , true , true));
        String trackingNum=lmsHelper.getTrackingNumber(packetId);

        // Create master bag and add to master bag
        Long masterBagId=
                lmsServiceHelper.createMasterBag(originPremisesId , "HUB" , "Bangalore" , destinationPremisesId , "DC" , "Bangalore" ,
                        EnumSCM.SUCCESS)
                        .getEntries().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId , ""+masterBagId , ShipmentType.TRY_AND_BUY) ,
                EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to close masterBag");

        //Ship MB
        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId).getStatus().getStatusType().toString() ,
                EnumSCM.SUCCESS , "Unable to ship masterBag");

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.TRY_AND_BUY, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.FAILED_DELIVERY ,
                ShipmentType.TRY_AND_BUY, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.RTO_CONFIRMED ,
                ShipmentType.TRY_AND_BUY, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.RTO_DISPATCHED ,
                ShipmentType.TRY_AND_BUY, CourierCode.EK.toString());

        //Receive FD order in Warehouse
        RejoyServiceHelper.recieveShipmentInRejoy(packetId);

        final String shipmentStatus7=lms_returnHelper.getShipmentStatusFromOrder_to_ship(trackingNum);
        Assert.assertEquals(shipmentStatus7 , EnumSCM.DELIVERED_TO_SELLER ,
                "order is not in DELIVERED_TO_SELLER state");

    }


    @Test(retryAnalyzer=Retry.class,dataProviderClass=LMSTestsDP.class, dataProvider="RegionalHandOver", groups={"Sanity"}, priority=0, enabled=true,
            description="Regional HandOver - Deliver the Order")
    public void RegionalHandOver_DeliverOrder(String pincode , String courier , String shippingMethod , Long originPremisesId , Long destPremisesId , String source , String destination , Long courierDC) throws Exception {

        // create Shipment till in scan
        for (int i=0; i < 10; i++) {
            lms_returnHelper.insertTrackingNumberRHD("DE-COD");
        }
        String orderId=lmsHelper.createMockOrder(EnumSCM.IS , pincode , courier , "36" , shippingMethod , "cod" ,
                false ,
                false);
        String packetId=omsServiceHelper.getPacketId(orderId);
        String trackingNum=lmsHelper.getTrackingNumber(packetId);
        final String shipmentStatus=lms_returnHelper.getShipmentStatusFromOrder_to_ship(trackingNum);
        Assert.assertEquals(shipmentStatus , EnumSCM.INSCANNED , "order is not in INSCANNED state");

        lms_3plHelper.masterBagOperationRHC(packetId , shippingMethod , source , destination , originPremisesId ,
                destPremisesId , courierDC , courier , ShipmentType.DL);

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.DELIVERED , ShipmentType.DL, CourierCode.EK.toString());

    }

    @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="RegionalHandOver", groups={"Sanity"}, priority=0, enabled=true,
            description="EXCHANGE:- Regional HandOver Deliver the Order")
    public void RegionalHandOver_DeliverOrderInWhExchange(String pincode , String courier , String shippingMethod , Long originPremisesId , Long destPremisesId , String source , String destination , Long courierDC) throws Exception {

        // create Shipment till in scan
        lms_returnHelper.insertTrackingNumberRHD("DE-COD");
        String orderID=lmsHelper.createMockOrder(EnumSCM.DL , LMS_PINCODE.ML_BLR , "ML" , "36" , EnumSCM.NORMAL ,
                "cod" , false , true);
        String exchangeOrder=lms_createOrder.createMockOrderExchange(EnumSCM.IS , pincode , courier , "36" ,
                EnumSCM.NORMAL , "cod" , false , true , omsServiceHelper.getReleaseId(orderID));
        String packetId=omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNum=lmsHelper.getTrackingNumber(packetId);

        lms_3plHelper.masterBagOperationRHC(packetId , shippingMethod , source , destination , originPremisesId ,
                destPremisesId , courierDC , courier , ShipmentType.DL);

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.DELIVERED , ShipmentType.DL, CourierCode.DE.toString());


    }

    /*@Test(dataProviderClass = LMSTestsDP.class, dataProvider = "RegionalHandOver",groups = {"Sanity"}, priority = 0, enabled = true,
            description = "try n buy:-Regional HandOver Deliver the Order")
    public void RegionalHandOver_DeliverOrderInWhTryNBuy(String pincode, String courier, String shippingMethod , Long originPremisesId , Long destPremisesId , String source , String destination , Long courierDC) throws Exception {

        // create Shipment till in scan
        lms_returnHelper.insertTrackingNumberRHD("DE-COD");
        String packetId = omsServiceHelper.getPacketId(
                lmsHelper.createMockOrder(EnumSCM.IS, pincode, courier, "36", EnumSCM.NORMAL, "cod", true, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        lms_3plHelper.masterBagOperationRHC(packetId,shippingMethod,source,destination,originPremisesId,destPremisesId,courierDC, courier, ShipmentType.TRY_AND_BUY);

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId, ShipmentUpdateEvent.OUT_FOR_DELIVERY,ShipmentType.TRY_AND_BUY);
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId, ShipmentUpdateEvent.DELIVERED, ShipmentType.TRY_AND_BUY);

    }
*/
    @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="RegionalHandOver", groups={"Sanity"}, priority=0, enabled=true,
            description="Regional HandOver - Failed Deliver the 3pl Order")
    public void RegionalHandOver_FailedDeliver3plOrder(String pincode , String courier , String shippingMethod , Long originPremisesId , Long destPremisesId , String source , String destination , Long courierDC) throws Exception {

        // create Shipment till in scan
        lms_returnHelper.insertTrackingNumberRHD("DE-COD");
        String orderId=lmsHelper.createMockOrder(EnumSCM.IS , pincode , courier , "36" , shippingMethod , "cod" ,
                false ,
                false);
        String packetId=omsServiceHelper.getPacketId(orderId);
        String trackingNum=lmsHelper.getTrackingNumber(packetId);
        final String shipmentStatus=lms_returnHelper.getShipmentStatusFromOrder_to_ship(trackingNum);
        Assert.assertEquals(shipmentStatus , EnumSCM.INSCANNED , "order is not in INSCANNED state");

        lms_3plHelper.masterBagOperationRHC(packetId , shippingMethod , source , destination , originPremisesId ,
                destPremisesId , courierDC , courier , ShipmentType.DL);

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.DL, CourierCode.DE.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.FAILED_DELIVERY ,
                ShipmentType.DL, CourierCode.DE.toString());

    }

    @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="RegionalHandOver", groups={"Sanity"}, priority=0, enabled=true,
            description="EXCHANGE:- Regional HandOver Failed Deliver the 3pl Order")
    public void RegionalHandOver_FailedDeliver3plOrderInWhExchange(String pincode , String courier , String shippingMethod , Long originPremisesId , Long destPremisesId , String source , String destination , Long courierDC) throws Exception {

        // create Shipment till in scan
        lms_returnHelper.insertTrackingNumberRHD("DE-COD");
        String orderID=lmsHelper.createMockOrder(EnumSCM.DL , LMS_PINCODE.ML_BLR , "ML" , "36" , EnumSCM.NORMAL ,
                "cod" , false , true);
        String exchangeOrder=lms_createOrder.createMockOrderExchange(EnumSCM.IS , pincode , courier , "36" ,
                EnumSCM.NORMAL , "cod" , false , true , omsServiceHelper.getReleaseId(orderID));
        String packetId=omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNum=lmsHelper.getTrackingNumber(packetId);

        lms_3plHelper.masterBagOperationRHC(packetId , shippingMethod , source , destination , originPremisesId ,
                destPremisesId , courierDC , courier , ShipmentType.DL);

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.DL, CourierCode.DE.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.FAILED_DELIVERY ,
                ShipmentType.DL, CourierCode.DE.toString());

    }

    /*@Test(retryAnalyzer = Retry.class,dataProviderClass = LMSTestsDP.class, dataProvider = "RegionalHandOver",groups = {"Sanity"}, priority = 0, enabled = true,
            description = "try n buy:-Regional HandOver Failed Deliver the 3pl Order")
    public void RegionalHandOver_FailedDeliver3plOrderInWhTryNBuy(String pincode, String courier, String shippingMethod , Long originPremisesId , Long destPremisesId , String source , String destination , Long courierDC) throws Exception {

        // create Shipment till in scan
        lms_returnHelper.insertTrackingNumberRHD("DE-COD");
        String packetId = omsServiceHelper.getPacketId(
                lmsHelper.createMockOrder(EnumSCM.IS, pincode, courier, "36", EnumSCM.NORMAL, "cod", true, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        lms_3plHelper.masterBagOperationRHC(packetId,shippingMethod,source,destination,originPremisesId,destPremisesId,courierDC, courier, ShipmentType.TRY_AND_BUY);
// Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId, ShipmentUpdateEvent.OUT_FOR_DELIVERY,ShipmentType.TRY_AND_BUY);
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId, ShipmentUpdateEvent.FAILED_DELIVERY, ShipmentType.TRY_AND_BUY);
    }
*/


    @Test(retryAnalyzer=Retry.class, dataProviderClass=LMSTestsDP.class, dataProvider="RegionalHandOver", groups={"Sanity"}, priority=0, enabled=true,
            description="EXCHANGE:- Regional HandOver Receive the failed delivered Order in Wh")
    public void RegionalHandOver_ReceiveFailedDeliveredOrderInWhExchange(String pincode , String courier , String shippingMethod , Long originPremisesId , Long destPremisesId , String source , String destination , Long courierDC) throws Exception {

        // create Shipment till in scan
        lms_returnHelper.insertTrackingNumberRHD("DE-COD");
        String orderID=lmsHelper.createMockOrder(EnumSCM.DL , LMS_PINCODE.ML_BLR , "ML" , "36" , EnumSCM.NORMAL ,
                "cod" , false , true);
        String exchangeOrder=lms_createOrder.createMockOrderExchange(EnumSCM.IS , pincode , courier , "36" ,
                EnumSCM.NORMAL , "cod" , false , true , omsServiceHelper.getReleaseId(orderID));
        String packetId=omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNum=lmsHelper.getTrackingNumber(packetId);

        lms_3plHelper.masterBagOperationRHC(packetId , shippingMethod , source , destination , originPremisesId ,
                destPremisesId , courierDC , courier , ShipmentType.DL);

        // Out for Delivery
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.OUT_FOR_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.FAILED_DELIVERY ,
                ShipmentType.DL, CourierCode.EK.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.RTO_CONFIRMED , ShipmentType.DL, CourierCode.DE.toString());
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId , ShipmentUpdateEvent.RTO_DISPATCHED , ShipmentType.DL, CourierCode.DE.toString());

        //Receive FD order in Warehouse
        RejoyServiceHelper.recieveShipmentInRejoy(packetId);

        final String shipmentStatus7=lms_returnHelper.getShipmentStatusFromOrder_to_ship(trackingNum);
        Assert.assertEquals(shipmentStatus7 , EnumSCM.DELIVERED_TO_SELLER ,
                "order is not in DELIVERED_TO_SELLER state");


    }

    /*@Test(dataProviderClass = LMSTestsDP.class, dataProvider = "RegionalHandOver",groups = {"Sanity"}, priority = 0, enabled = true,
            description = "try n buy:-Regional HandOver Receive the failed delivered Order in Wh")
    public void RegionalHandOver_ReceiveFailedDeliveredOrderInWhTryNBuy(String pincode, String courier, String shippingMethod , Long originPremisesId , Long destPremisesId , String source , String destination , Long courierDC) throws Exception {

        // create Shipment till in scan
        lms_returnHelper.insertTrackingNumberRHD("DE-COD");
        String packetId = omsServiceHelper.getPacketId(
                lmsHelper.createMockOrder(EnumSCM.IS, pincode, courier, "36", EnumSCM.NORMAL, "cod", true, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        lms_3plHelper.masterBagOperationRHC(packetId,shippingMethod,source,destination,originPremisesId,destPremisesId,courierDC, courier, ShipmentType.TRY_AND_BUY);

        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId, ShipmentUpdateEvent.OUT_FOR_DELIVERY,ShipmentType.TRY_AND_BUY);
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId, ShipmentUpdateEvent.FAILED_DELIVERY, ShipmentType.TRY_AND_BUY);
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId, ShipmentUpdateEvent.RTO_CONFIRMED, ShipmentType.TRY_AND_BUY);
        lms_3plHelper.updateAndVerifyShipmentStatus3pl(packetId, ShipmentUpdateEvent.RTO_DISPATCHED, ShipmentType.TRY_AND_BUY);

        //Receive FD order in Warehouse
        RejoyServiceHelper.recieveShipmentInRejoy(packetId);

        final String shipmentStatus7 = lms_returnHelper.getShipmentStatusFromOrder_to_ship(trackingNum);
        Assert.assertEquals(shipmentStatus7, EnumSCM.DELIVERED_TO_SELLER, "order is not in DELIVERED_TO_SELLER state");

    }
*/
}
