package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.erpservices.lastmile.service.LMSOrderDetailsClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.response.TransporterResponse;
import com.myntra.lms.client.status.OrderShipmentAssociationStatus;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.test.commons.testbase.BaseTest;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_ReturnHelper;

import javax.xml.bind.SchemaOutputResolver;
import java.util.HashMap;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class LMS_RegionalHandover {

    String env = getEnvironment();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMSOrderDetailsClient lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();



    @Test(dataProviderClass = LMSTestsDP.class, dataProvider = "RegionalHandOver")

    public void RH_with_TMS(String pincode, String courier, String shippingMethod , Long originPremisesId , Long destPremisesId , String source , String destination , Long courierDC) throws Exception {

        // Create tracking number for DE .

        DBUtilities.exUpdateQuery("update `tracking_number` set `status` = 0 where `courier_code` = 'DE-COD' and `tenant_id` = '4019' ORDER BY `last_modified_on` ASC limit 10;", "lms");
        lmsReturnHelper.insertTrackingNumberRHD("DE");

        // Create an order with PK with DE courier

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
            false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.PACKED),"Order is not moved to PACKED status , Check whether pincode have serviceability , lms and lms3pl is up  ");


        // Inscan that order at DH-BLR

        if (!courier.equals("ML"))
            DBUtilities
                    .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                        + packetId + "'", "lms");
        Assert.assertEquals(lmsServiceHelper.orderInScanNew(packetId), EnumSCM.SUCCESS);
        String hubCode = lmsServiceHelper.getHubConfig("36", "DL");
        lmsHelper.inscanValidation(omsServiceHelper.getPacketId(orderId), hubCode);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.INSCANNED),"Order is not moved to INSCANNED status , Check whether pincode have serviceability , lms and lms3pl is up  ");


        // Create a Masterbag from DH-BLR to BHU ( Myntra Regional Handover Bhubaneswar )

        Long masterBagId = masterBagServiceHelper.createMasterBag(originPremisesId , destPremisesId, ShippingMethod.valueOf(shippingMethod), "ML" ).getId();

        // Add Shipment to Masterbag.

        lmsServiceHelper.addAndSaveMasterBag("" + packetId, "" + masterBagId, ShipmentType.DL);
        System.out.println("\nShipment is added to Masterbag :: " +masterBagId);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.ADDED_TO_MB),"Order is not moved to ADDED_TO_MB status  ");

        // Close MasterBag.

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(),
            EnumSCM.SUCCESS, "Unable to close masterBag");

        // Receive Masterbag at TH-BLR transport hub

        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(source, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");

        // Create Container from TH-BLR to BHU.

        long laneId = ((LaneResponse)tmsServiceHelper.getSupportedLanes.apply(source, destination)).getLaneEntries().get(0).getId();
        long transporterId = ((TransporterResponse)tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        ContainerResponse containerResponse = (ContainerResponse)tmsServiceHelper.createContainer.apply(source, destination, laneId, transporterId);
        long containerId = containerResponse.getContainerEntries().get(0).getId();

        // Add Masterbag to Container.

        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Error occurred during state transition");
        System.out.println("\nMasterbag "+ masterBagId+" is added to Container :: " +containerId);

        // Ship the Container.

        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "CONTAINER SHIP IS FAILING :: " +containerId);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.SHIPPED),"Order is not moved to SHIPPED status  ");


        // Receive the Masterbag at BHU transport hub.

        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destination, masterBagId, containerId);

        // Masterbag Inscan at BHU DC.

        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.IN_TRANSIT, "Bangalore", destPremisesId, "DC");
        ExceptionHandler.handleEquals(lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2_RHD(masterBagId,trackingNum,"Bangalore",destPremisesId , ShipmentType.DL ,PremisesType.DC).
                getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");

        lmsServiceHelper.masterBagInScanUpdateNew(masterBagId, packetId, "Bangalore","Myntra BHU", destPremisesId, "DC",originPremisesId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT);

        ExceptionHandler.handleEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId, packetId, "Bangalore",destPremisesId
                , "DC", originPremisesId).
                getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive master bag in DC");
        ExceptionHandler.handleEquals(lmsHelper.getMasterBagStatus(masterBagId),EnumSCM.RECEIVED_AT_HANDOVER_CENTER,
                "Masterbag status is not update in DB to `RECEIVED`");


        // Create a MasterBag from BHU to Delhivery (DE) with courier DE.

        Long masterBagId1 = masterBagServiceHelper.createMasterBag(destPremisesId , courierDC, ShippingMethod.valueOf(shippingMethod), courier).getId();

        // Add Shipment to Masterbag.

        lmsServiceHelper.addAndSaveMasterBag("" + packetId, "" + masterBagId1, ShipmentType.DL);
        System.out.println("\nShipment is added to Masterbag :: " +masterBagId1);

        // Close Masterbag.

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId1).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag ::" + masterBagId1 );
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER),"Order is not moved to RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER status , Check whether FG is enabled . ( Should lms.enable_rhc_v2 = TRUE )");


        // Ship the Masterbag at Delhivery (DE)

        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId1).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Shipping masterbag is failed ::" +masterBagId1);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.SHIPPED),"Order is not moved to SHIPPED status , Check whether FG is enabled . ( Should lms.enable_rhc_v2 = TRUE )");
        System.out.println("Tracking Number With Courier DE :: " + trackingNum);


}

    @Test(dataProviderClass = LMSTestsDP.class, dataProvider = "RegionalHandOver")

    public void RH_without_TMS(String pincode, String courier, String shippingMethod ,String source , String destination ) throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
                false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        Thread.sleep(3000);
        if (!courier.equals("ML"))
            DBUtilities
                    .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                            + packetId + "'", "lms");
        Assert.assertEquals(lmsServiceHelper.orderInScanGOR(omsServiceHelper.getPacketId(orderId), "36").getStatus()
                .getStatusType().toString(), EnumSCM.SUCCESS);
        String hubCode = lmsServiceHelper.getHubConfig("36", "DL");
        lmsHelper.inscanValidation(omsServiceHelper.getPacketId(orderId), hubCode);
        ShipmentResponse shipmentResponse = lmsServiceHelper.createMasterBag(
                1, "WH",
                Long.parseLong("2281"), "DC", "Delhi", courier);
        Long ShipmentID = shipmentResponse.getEntries().get(0).getId();
        System.out.println(ShipmentID);
        lmsServiceHelper.addAndSaveMasterBag("" + packetId, "" + ShipmentID, ShipmentType.DL);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(ShipmentID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        Assert.assertEquals(
                lmsServiceHelper.masterBagInScanUpdate(ShipmentID, "" + packetId, "Bangalore", 1, "WH", 36)
                        .getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
    }

    @Test(dataProviderClass = LMSTestsDP.class, dataProvider = "RegionalHandOver")

    public void RH_shortage(String pincode, String courier, String shippingMethod ,String source , String destination ) throws Exception {
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
                false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String orderId2 = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
                false);
        String packetId2 = omsServiceHelper.getPacketId(orderId1);
        Thread.sleep(3000);
        if (!courier.equals("ML"))
            DBUtilities
                    .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                            + packetId1 + "'", "lms");
            DBUtilities
                .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                        + packetId2 + "'", "lms");
        Assert.assertEquals(lmsServiceHelper.orderInScanGOR(omsServiceHelper.getPacketId(orderId1), "36").getStatus()
                .getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.orderInScanGOR(omsServiceHelper.getPacketId(orderId2), "36").getStatus()
                .getStatusType().toString(), EnumSCM.SUCCESS);
        String hubCode = lmsServiceHelper.getHubConfig("36", "DL");
        lmsHelper.inscanValidation(omsServiceHelper.getPacketId(orderId1), hubCode);
        lmsHelper.inscanValidation(omsServiceHelper.getPacketId(orderId2), hubCode);
        ShipmentResponse shipmentResponse = lmsServiceHelper.createMasterBag(
                1, "WH",
                Long.parseLong("2281"), "DC", "Delhi", courier);
        Long ShipmentID = shipmentResponse.getEntries().get(0).getId();
        System.out.println(ShipmentID);
        lmsServiceHelper.addAndSaveMasterBag("" + packetId1, "" + ShipmentID, ShipmentType.DL);
        lmsServiceHelper.addAndSaveMasterBag("" + packetId2, "" + ShipmentID, ShipmentType.DL);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(ShipmentID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        Assert.assertEquals(
                lmsServiceHelper.masterBagInScanUpdate(ShipmentID, "" + packetId1, "Bangalore", 1, "WH", 36)
                        .getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);

    }
    @Test(dataProviderClass = LMSTestsDP.class, dataProvider = "RegionalHandOver")

    public void RH_Excess(String pincode, String courier, String shippingMethod ,String source , String destination ) throws Exception {
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
                false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String orderId2 = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
                false);
        String packetId2 = omsServiceHelper.getPacketId(orderId1);
        Thread.sleep(3000);
        if (!courier.equals("ML"))
            DBUtilities
                    .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                            + packetId1 + "'", "lms");
        DBUtilities
                .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                        + packetId2 + "'", "lms");
        Assert.assertEquals(lmsServiceHelper.orderInScanGOR(omsServiceHelper.getPacketId(orderId1), "36").getStatus()
                .getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.orderInScanGOR(omsServiceHelper.getPacketId(orderId2), "36").getStatus()
                .getStatusType().toString(), EnumSCM.SUCCESS);
        String hubCode = lmsServiceHelper.getHubConfig("36", "DL");
        lmsHelper.inscanValidation(omsServiceHelper.getPacketId(orderId1), hubCode);
        lmsHelper.inscanValidation(omsServiceHelper.getPacketId(orderId2), hubCode);
        ShipmentResponse shipmentResponse = lmsServiceHelper.createMasterBag(
                1, "WH",
                Long.parseLong("2281"), "DC", "Delhi", courier);
        Long ShipmentID = shipmentResponse.getEntries().get(0).getId();
        System.out.println(ShipmentID);
        lmsServiceHelper.addAndSaveMasterBag("" + packetId1, "" + ShipmentID, ShipmentType.DL);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(ShipmentID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        Assert.assertEquals(
                lmsServiceHelper.masterBagInScanUpdate(ShipmentID, "" + packetId1, "Bangalore", 1, "WH", 36)
                        .getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
        Assert.assertEquals(
                lmsServiceHelper.masterBagInScanUpdate(ShipmentID, "" + packetId2, "Bangalore", 1, "WH", 36)
                        .getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
    }

    @Test(dataProviderClass = LMSTestsDP.class, dataProvider = "RegionalHandOver")
    public void RH_receiveMasterbag_with_order_in_inscan(String pincode, String courier, String shippingMethod ,String source , String destination ) throws Exception {
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
                false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String orderId2 = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
                false);
        String packetId2 = omsServiceHelper.getPacketId(orderId1);
        Thread.sleep(3000);
        if (!courier.equals("ML"))
            DBUtilities
                    .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                            + packetId1 + "'", "lms");
        DBUtilities
                .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                        + packetId2 + "'", "lms");
        Assert.assertEquals(lmsServiceHelper.orderInScanGOR(omsServiceHelper.getPacketId(orderId1), "36").getStatus()
                .getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.orderInScanGOR(omsServiceHelper.getPacketId(orderId2), "36").getStatus()
                .getStatusType().toString(), EnumSCM.SUCCESS);
        String hubCode = lmsServiceHelper.getHubConfig("36", "DL");
        lmsHelper.inscanValidation(omsServiceHelper.getPacketId(orderId1), hubCode);
        lmsHelper.inscanValidation(omsServiceHelper.getPacketId(orderId2), hubCode);
        ShipmentResponse shipmentResponse = lmsServiceHelper.createMasterBag(
                1, "WH",
                Long.parseLong("2281"), "DC", "Delhi", courier);
        Long ShipmentID = shipmentResponse.getEntries().get(0).getId();
        System.out.println(ShipmentID);
        lmsServiceHelper.addAndSaveMasterBag("" + packetId1, "" + ShipmentID, ShipmentType.DL);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(ShipmentID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        Assert.assertEquals(
                lmsServiceHelper.masterBagInScanUpdate(ShipmentID, "" + packetId1, "Bangalore", 1, "WH", 36)
                        .getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
        Assert.assertEquals(
                lmsServiceHelper.masterBagInScanUpdate(ShipmentID, "" + packetId2, "Bangalore", 1, "WH", 36)
                        .getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
    }
    @Test(dataProviderClass = LMSTestsDP.class, dataProvider = "RegionalHandOver")
    public void RH_receiveMasterbag_with_order_in_packed(String pincode, String courier, String shippingMethod ,String source , String destination ) throws Exception {
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
                false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String orderId2 = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
                false);
        String packetId2 = omsServiceHelper.getPacketId(orderId1);
        Thread.sleep(3000);
        if (!courier.equals("ML"))
            DBUtilities
                    .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                            + packetId1 + "'", "lms");
        DBUtilities
                .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                        + packetId2 + "'", "lms");
        Assert.assertEquals(lmsServiceHelper.orderInScanGOR(omsServiceHelper.getPacketId(orderId1), "36").getStatus()
                .getStatusType().toString(), EnumSCM.SUCCESS);
        String hubCode = lmsServiceHelper.getHubConfig("36", "DL");
        lmsHelper.inscanValidation(omsServiceHelper.getPacketId(orderId1), hubCode);
        ShipmentResponse shipmentResponse = lmsServiceHelper.createMasterBag(
                1, "WH",
                Long.parseLong("2281"), "DC", "Delhi", courier);
        Long ShipmentID = shipmentResponse.getEntries().get(0).getId();
        System.out.println(ShipmentID);
        lmsServiceHelper.addAndSaveMasterBag("" + packetId1, "" + ShipmentID, ShipmentType.DL);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(ShipmentID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        Assert.assertEquals(
                lmsServiceHelper.masterBagInScanUpdate(ShipmentID, "" + packetId1, "Bangalore", 1, "WH", 36)
                        .getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
        Assert.assertEquals(
                lmsServiceHelper.masterBagInScanUpdate(ShipmentID, "" + packetId2, "Bangalore", 1, "WH", 36)
                        .getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
    }
}