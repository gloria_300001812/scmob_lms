package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.client.DeliveryCenterClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.lastmile.client.code.utils.DeliveryStaffType;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.Toolbox;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.lastmile.client.code.utils.ReconType.EOD_RECON;

public class LastMileTestDP {

    static Initialize init = new Initialize("/Data/configuration");
    static String FGTenantID = "";
    static String env = getEnvironment();
    static DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);

    public LastMileTestDP() {
        if (LMS_CONSTANTS.FGVersion == 3) {
            FGTenantID = "tenantId.eq:" + LMS_CONSTANTS.TENANTID + "___";
        }
        if (LMS_CONSTANTS.FGVersion == 0) {
            FGTenantID = "";
        }

    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getTripWithParamForDeliveryCenter(ITestContext testContext)
    {
        Object[] arr1 = {"deliveryCenterId.eq:"+"ToBeReplacedDCId"+"___showAllDeliveryStaff.eq:false___tenantId.eq:"+ LASTMILE_CONSTANTS.TENANT_ID, 0, 20, LASTMILE_CONSTANTS.SORT_BY_TRIPDATE, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL, LASTMILE_CONSTANTS.TENANT_ID};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getTripWithParamForWrongDC(ITestContext testContext)
    {
        Object[] arr1 = {"deliveryCenterId.eq:"+"ToBeReplacedDCId"+"___showAllDeliveryStaff.eq:false___tripStatus.eq:CREATED___isDeleted.eq:false___isInbound.eq:false___tenantId.eq:"+ LASTMILE_CONSTANTS.TENANT_ID, 0, 20, LASTMILE_CONSTANTS.SORT_BY_TRIPDATE, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.STATUS_CODE_3, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL, LASTMILE_CONSTANTS.TENANT_ID};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] searchTripForOnlyDC(ITestContext testContext)
    {
        Object[] arr1 = {"deliveryCenterId.eq:"+"ToBeReplacedDCId", 0, 20, LASTMILE_CONSTANTS.SORT_BY_TRIPDATE, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL, LASTMILE_CONSTANTS.TENANT_ID};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getTripDetailsByTrackingNumberNeg(ITestContext testContext)
    {
        Object[] arr1 = {"L0001586066",LASTMILE_CONSTANTS.STATUS_CODE_914, LASTMILE_CONSTANTS.FAILURE_STATUS_MESSAGE_NO_TRIP_FOUND,EnumSCM.ERROR};
        Object[] arr2 = {"ML0001586066",LASTMILE_CONSTANTS.STATUS_CODE_914, LASTMILE_CONSTANTS.FAILURE_STATUS_MESSAGE_NO_TRIP_FOUND,EnumSCM.ERROR};
        Object[] arr3 = {null,LASTMILE_CONSTANTS.STATUS_CODE_914, LASTMILE_CONSTANTS.FAILURE_STATUS_MESSAGE_NO_TRIP_FOUND,EnumSCM.ERROR};

        Object[][] dataSet = new Object[][] {arr1, arr2, arr3};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }


    @DataProvider/*(parallel = true)*/
    public static Object [][] getTripWithParamForDeliveryStaff(ITestContext testContext)
    {
        Object[] arr1 = {"deliveryCenterId.eq:"+"ToBeReplacedDCId"+"___deliveryStaffId.eq:ToBeReplacedStaffId___showAllDeliveryStaff.eq:false___tenantId.eq:"+ LASTMILE_CONSTANTS.TENANT_ID, 0, 20, LASTMILE_CONSTANTS.SORT_BY_TRIPDATE,LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getTripWithParamForCardEnabled(ITestContext testContext)
    {
        Object[] arr1 = {"deliveryCenterId.eq:"+"ToBeReplacedDCId___showAllDeliveryStaff.eq:false___isCardEnabled.eq:true___tenantId.eq:"+ LASTMILE_CONSTANTS.TENANT_ID, 0, 20, LASTMILE_CONSTANTS.SORT_BY_TRIPDATE, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object[][] getTripWithParamForTripStatus(ITestContext testContext) {
        Object[] arr4 = {"deliveryCenterId.eq:ToBeReplacedDCId___showAllDeliveryStaff.eq:false___tripStatus.eq:" + EnumSCM.OUT_FOR_DELIVERY + "___tenantId.eq:" + LASTMILE_CONSTANTS.TENANT_ID, 0, 20, "tripDate", "DESC", LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL, EnumSCM.OUT_FOR_DELIVERY};
        Object[] arr5 = {"deliveryCenterId.eq:ToBeReplacedDCId___showAllDeliveryStaff.eq:false___tripStatus.eq:" + EnumSCM.CREATED + "___tenantId.eq:" + LASTMILE_CONSTANTS.TENANT_ID, 0, 20, "tripDate", "DESC", LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL, EnumSCM.CREATED};
        Object[] arr6 = {"deliveryCenterId.eq:ToBeReplacedDCId___showAllDeliveryStaff.eq:false___tripStatus.eq:" + EnumSCM.COMPLETED + "___tenantId.eq:" + LASTMILE_CONSTANTS.TENANT_ID, 0, 20, "tripDate", "DESC", LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL, EnumSCM.COMPLETED};
        Object[] arr7 = {"deliveryCenterId.eq:ToBeReplacedDCId___showAllDeliveryStaff.eq:false___tripStatus.eq:" + EnumSCM.OUT_FOR_PICKUP + "___tenantId.eq:" + LASTMILE_CONSTANTS.TENANT_ID, 0, 20, "tripDate", "DESC", LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL, EnumSCM.OUT_FOR_PICKUP};
        Object[] arr8 = {"deliveryCenterId.eq:ToBeReplacedDCId___showAllDeliveryStaff.eq:false___tripStatus.eq:" + EnumSCM.ADDED_TO_SDA_TRIP + "___tenantId.eq:" + LASTMILE_CONSTANTS.TENANT_ID, 0, 20, "tripDate", "DESC", LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL, EnumSCM.ADDED_TO_SDA_TRIP};

        Object[][] dataSet = new Object[][]{arr4, arr5, arr6};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    ////////////////////
    public static Object[][] getTripPlanningCombinationSearch(ITestContext testContext) {
        Object[] arr4 = {"deliveryCenterId.eq:5___deliveryStaffId.eq:5522___tenantId.eq:4019&sortBy=id&sortOrder=ASE"};

        Object[] arr5 = {"deliveryCenterId.eq:5___deliveryStaffId.eq:5522___tenantId.eq:4019&sortBy=id&sortOrder=ASE"};
        Object[] arr6 = {"q=deliveryCenterId.eq:5___showAllDeliveryStaff.eq:false___tripStatus.eq:CREATED___tenantId.eq:4019"};


        Object[][] dataSet = new Object[][]{arr4, arr5, arr6};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getAllAvailableTripsForDc(ITestContext testContext) {
        Object[] arr7 = { LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL};
        Object[][] dataSet = new Object[][]{arr7};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getAllSearchResultOfTrip(ITestContext testContext) {
        Object[] arr8 = {"search", LASTMILE_CONSTANTS.STATUS_CODE_1003,LASTMILE_CONSTANTS.TRIP_SUCCESSFUL};
        Object[][] dataSet = new Object[][]{ arr8};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getTripByTripNumber(ITestContext testContext)
    {
        Object[] arr1 = {"tripNumber.like:ToBeReplacedTripNumber___tenantId.eq:"+ LASTMILE_CONSTANTS.TENANT_ID, 0, 1, LASTMILE_CONSTANTS.SORT_BY_ID, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL};
        Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getTripByTripId(ITestContext testContext)
    {
        Object[] arr1 = {"tripId.eq:ToBeReplacedTripId___tenantId.eq:"+ LASTMILE_CONSTANTS.TENANT_ID, 0, 1, LASTMILE_CONSTANTS.SORT_BY_ID, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.STATUS_CODE_1003, LASTMILE_CONSTANTS.TRIP_SUCCESSFUL};
        Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getTripOrderByTripId(ITestContext testContext)
    {
        Object[] arr1 = {"findOrdersByTrip/TRIP_ID",  EnumSCM.SUCCESS};
        Object[] arr2 = {"findOrdersByTrip/TRIP_ID/TRY_AND_BUY",  EnumSCM.SUCCESS};
        Object[] arr3 = {"getTripUpdateDashboardInfo/TRIP_ID", EnumSCM.SUCCESS};
        Object[] arr4 = {"findOrdersByTrip/TRIP_ID/PU", EnumSCM.SUCCESS};
        Object[] arr5 = {"findExchangesByTrip/TRIP_ID", EnumSCM.SUCCESS};
        Object[] arr6 = {"findOrdersByTrip/TRIP_ID/DL", EnumSCM.SUCCESS};
        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider /*(parallel = true)*/
    public static Object [][] getTripResult(ITestContext testContext)
    {
        Object[] arr1 = {LASTMILE_CONSTANTS.START_INDEX,LASTMILE_CONSTANTS.FETCH_SIZE, LASTMILE_CONSTANTS.SORT_BY_PROMISE_DATE, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.STATUS_CODE_3, LASTMILE_CONSTANTS.SUCCESS_STATUS_MESSAGE, EnumSCM.SUCCESS};
        Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getTripResultPost1(ITestContext testContext)
    {// param, deliveryCenterId, shipmentType
        Object[] arr1 = {ShipmentType.TRY_AND_BUY,LASTMILE_CONSTANTS.START_INDEX, LASTMILE_CONSTANTS.FETCH_SIZE, false, false, LASTMILE_CONSTANTS.STATUS_CODE_3, LASTMILE_CONSTANTS.SUCCESS_STATUS_MESSAGE, EnumSCM.SUCCESS};
        Object[] arr2 = {ShipmentType.DL,LASTMILE_CONSTANTS.START_INDEX, LASTMILE_CONSTANTS.FETCH_SIZE, false, false, LASTMILE_CONSTANTS.STATUS_CODE_3, LASTMILE_CONSTANTS.SUCCESS_STATUS_MESSAGE, EnumSCM.SUCCESS };
        Object[] arr3 = {ShipmentType.PU, LASTMILE_CONSTANTS.START_INDEX, LASTMILE_CONSTANTS.FETCH_SIZE, false, false, LASTMILE_CONSTANTS.STATUS_CODE_3, LASTMILE_CONSTANTS.SUCCESS_STATUS_MESSAGE, EnumSCM.SUCCESS};
        Object[] arr4 = {"getAllOnHoldOrdersForDCBasedOnFilter/", 5L, ShipmentType.DL,"3", EnumSCM.SUCCESS, EnumSCM.SUCCESS};
        Object[][] dataSet = new Object[][] {arr1, arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getTripResultPostPickUp(ITestContext testContext)
    {// param, deliveryCenterId, shipmentType
        Object[] arr1 = {ShipmentType.PU, LASTMILE_CONSTANTS.START_INDEX, LASTMILE_CONSTANTS.FETCH_SIZE, LASTMILE_CONSTANTS.SORT_BY_ZIPCODE, LASTMILE_CONSTANTS.SORT_ORDER_DESC,LASTMILE_CONSTANTS.STATUS_CODE_3, LASTMILE_CONSTANTS.SUCCESS_STATUS_MESSAGE, EnumSCM.SUCCESS};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getAllOnHoldOrdersForDCBasedOnFilter(ITestContext testContext)
    {// param, deliveryCenterId, shipmentType
        Object[] arr1 = {ShipmentType.PU, LASTMILE_CONSTANTS.START_INDEX, LASTMILE_CONSTANTS.FETCH_SIZE, LASTMILE_CONSTANTS.SORT_BY_ZIPCODE, LASTMILE_CONSTANTS.SORT_ORDER_DESC,LASTMILE_CONSTANTS.STATUS_CODE_3, LASTMILE_CONSTANTS.SUCCESS_STATUS_MESSAGE, EnumSCM.SUCCESS};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] createTrip(ITestContext testContext)
    {
        Object[] arr1 = { "Trip added successfully", EnumSCM.CREATED};
        Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider /*(parallel = true)*/
    public static Object [][] isAutoCardEnabledForDCnDF(ITestContext testContext)
    {
        Object[] arr1 = { "1", EnumSCM.SUCCESS};
        Object[] arr2 = { "999", EnumSCM.ERROR};
        Object[][] dataSet = new Object[][] { arr1, arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getAllAvailableTripsForDC(ITestContext testContext)
    {// param, deliveryCenterId, shipmentType
        Object[] arr1 = {5,true};
        Object[] arr2 = {999,false};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getTripDetailForDate(ITestContext testContext)
    {// param, deliveryCenterId, shipmentType
        Object[] arr1 = {"ToBeReplacedDcId",true};
        Object[] arr2 = {"999999",false};
        Object[][] dataSet = new Object[][] {arr1, arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] startTripWithValidTripId(ITestContext testContext)
    {
        Object[] arr1 = {false, EnumSCM.CREATED};
        Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] startTrip(ITestContext testContext)
    {
        Object[] arr1 = { "464453343343443","100", EnumSCM.ERROR};
        Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] deleteTrip(ITestContext testContext)
    {
        Object[] arr1 = { EnumSCM.OUT_FOR_DELIVERY, EnumSCM.ERROR};
        Object[] arr2 = { EnumSCM.COMPLETED, EnumSCM.ERROR};
        Object[] arr3 = { EnumSCM.CREATED, EnumSCM.SUCCESS};
        Object[][] dataSet = new Object[][] {  arr2, arr1, arr3};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] assignReturntoTripNegative(ITestContext testContext)
    {
        Object[] arr1 = {EnumSCM.PICKED_UP_SUCCESSFULLY};
        Object[] arr2 = {EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER};
        Object[] arr3 = {EnumSCM.ONHOLD_PICKUP_WITH_DC};
        Object[][] dataSet = new Object[][] { arr1, arr2, arr3};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object [][] markOrderDeliveredForDifferentOrders(ITestContext testContext)
    {
        Object[] arr1 = {EnumSCM.PK, EnumSCM.ERROR};
        Object[] arr2 = {EnumSCM.IS, EnumSCM.ERROR};
        Object[] arr3 = {EnumSCM.ADDED_TO_MB, EnumSCM.SUCCESS};
        Object[] arr4 = {EnumSCM.SH, EnumSCM.SUCCESS};
        Object[] arr5 = {EnumSCM.OFD, EnumSCM.SUCCESS};
        Object[] arr6 = {EnumSCM.DL, EnumSCM.SUCCESS};
        Object[][] dataSet = new Object[][] { arr1, arr2,arr3, arr4, arr5, arr6};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] unassignOrderFromTrip(ITestContext testContext)
    {

        Object[] arr3 = {EnumSCM.RECEIVE_IN_DC, EnumSCM.SUCCESS};
        Object[] arr4 = {EnumSCM.SH, EnumSCM.SUCCESS};
        Object[] arr5 = {EnumSCM.OFD, EnumSCM.ERROR};
        Object[] arr6 = {EnumSCM.DL, EnumSCM.ERROR};
        Object[][] dataSet = new Object[][] { arr3, arr4, arr5, arr6};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }


    @DataProvider
    public static Object [][] getAllAvailableTripsForDCByStaffType(ITestContext testContext)
    {

        Object[] arr3 = {DeliveryStaffType.MYNTRA_PAYROLL.toString()};
        Object[] arr4 = {DeliveryStaffType.STORE.toString()};
        Object[] arr5 = {DeliveryStaffType.ADM.toString()};
        Object[][] dataSet = new Object[][] {   arr3};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] getAllIncompleteOrdersForDCForDLOrders(ITestContext testContext)
    {
        Object[] arr1 = {ShipmentType.DL, 0,20, LASTMILE_CONSTANTS.SORT_BY_CREATED_ON, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.SH, LASTMILE_CONSTANTS.STATUS_CODE_3, LASTMILE_CONSTANTS.SUCCESS_STATUS_MESSAGE};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] getAllIncompleteOrdersForDCForPUOrders(ITestContext testContext)
    {
        Object[] arr1 = {ShipmentType.PU, 0,20, LASTMILE_CONSTANTS.SORT_BY_ZIPCODE, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.RIT, LASTMILE_CONSTANTS.STATUS_CODE_3, LASTMILE_CONSTANTS.SUCCESS_STATUS_MESSAGE};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] getAllIncompleteOrdersForDCForExchangeOrders(ITestContext testContext)
    {
        Object[] arr1 = {ShipmentType.EXCHANGE, 0,20, LASTMILE_CONSTANTS.SORT_BY_CREATED_ON, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.PK, LASTMILE_CONSTANTS.STATUS_CODE_3, LASTMILE_CONSTANTS.SUCCESS_STATUS_MESSAGE};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] getAllIncompleteOrdersForDCForTrynBuy(ITestContext testContext)
    {
        Object[] arr1 = {ShipmentType.TRY_AND_BUY, 0,20, LASTMILE_CONSTANTS.SORT_BY_CREATED_ON, LASTMILE_CONSTANTS.SORT_ORDER_DESC, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.PK, LASTMILE_CONSTANTS.STATUS_CODE_3, LASTMILE_CONSTANTS.SUCCESS_STATUS_MESSAGE};
        Object[][] dataSet = new Object[][] {arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch(ITestContext testContext)
    {
        Object[] arr1 = {"search?fetchSize=-1&q=tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"___type.ne:"+LASTMILE_CONSTANTS.LAST_MILE_PARTNER+"___active.eq:true"};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch2(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=courierCode.like:"+LASTMILE_CONSTANTS.COURIER_CODE_ML+"___active.eq:true___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=100000&sortBy=&sortOrder="};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }


    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch3(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=courierCode.like:"+LASTMILE_CONSTANTS.COURIER_CODE_ML+"___active.eq:true___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=-1&sortBy=&sortOrder=&start=0&fetchSize=-1&sortBy=&sortOrder="};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch4(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=code.in:"+LASTMILE_CONSTANTS.DELIVERY_HUB_CODE_ELC+"___reconType.in:"+EOD_RECON.toString()+"___isCardEnabled.eq:true___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC+""};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch5(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=manager.in:Mohan+Kumar___isCardEnabled.eq:true___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch6(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=city.in:Bangalore___isCardEnabled.eq:true___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch7(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=selfShipSupported.eq:true___isCardEnabled.eq:true___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }


    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch8(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=selfShipSupported.eq:false___isCardEnabled.eq:true___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch9(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=isCardEnabled.eq:true___active.eq:true___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch15(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=isCardEnabled.eq:true___active.eq:false___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch16(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=courierCode.in:DE___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch17(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=reconType.in:EOD_RECON___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch18(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=pincode.in:560068___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch19(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CONTACTNUM+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch20(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=code.in:ELC___name.in:Electronics+City+%28ELC%29___courierCode.in:ML___manager.in:Mohan+Kumar___contactNumber.in:7259023811___reconType.in:EOD_RECON___city.in:Bangalore___state.in:Karnataka___pincode.in:560068___selfShipSupported.eq:false___active.eq:true___tenantId.eq:4019&start=0&fetchSize=20&sortBy=contactNumber&sortOrder=ASC"};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch10(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=isCardEnabled.eq:true___active.eq:true___name.in:ANNA+NAGAR+%28ABT%29___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch11(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=isCardEnabled.eq:true___contactNumber.in:9840644767___active.eq:true___name.in:ANNA+NAGAR+%28ABT%29___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch12(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=isCardEnabled.eq:true___state.in:Tamil+Nadu___active.eq:true___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch13(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=isCardEnabled.eq:true___isStrictServiceable.eq:true___active.eq:true___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] deliveryCenterSearch14(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=isCardEnabled.eq:true___isStrictServiceable.eq:false___active.eq:true___tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=20&sortBy="+LASTMILE_CONSTANTS.SORT_BY_CODE+"&sortOrder="+LASTMILE_CONSTANTS.SORT_ORDER_ASC};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }


    @DataProvider/*(parallel = true)*/
    public static Object [][] getActiveDeliveryStaff(ITestContext testContext)
    {
        Object[] arr1 = {"activeDeliveryStaffs?tenantId="+LASTMILE_CONSTANTS.TENANT_ID};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getDeliveryStaffSearch(ITestContext testContext)
    {
        Object[] arr1 = {"search?q=tenantId.eq:"+LASTMILE_CONSTANTS.TENANT_ID+"&start=0&fetchSize=1000000&sortBy=&sortOrder="};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider /*(parallel = true)*/
    public static Object [][] isAutoCardEnabledForStaff(ITestContext testContext)
    {
        Object[] arr1 = { "1", EnumSCM.SUCCESS};
        Object[] arr2 = { "999", EnumSCM.ERROR};
        Object[][] dataSet = new Object[][] { arr1, arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getAllIncompleteScheduledShipmentsForDC(ITestContext testContext)
    {
        Object[] arr1 = {"ToBeReplacedDCID/"+EnumSCM.PU+"/0/20?sortBy="+LASTMILE_CONSTANTS.SORT_BY_ZIPCODE+"&dir="+LASTMILE_CONSTANTS.SORT_ORDER_DESC+"&tenantId="+LASTMILE_CONSTANTS.TENANT_ID};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] bulkCreateTrip(ITestContext testContext)
    {
        Object[] arr1 = {"ToBeReplacedDCID/?tenantId="+LASTMILE_CONSTANTS.TENANT_ID};

        Object[][] dataSet = new Object[][] {  arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] assignOrder(ITestContext testContext)
    {
        Object[] arr1 = {EnumSCM.IS, EnumSCM.ERROR};
        Object[] arr2 = {EnumSCM.PK, EnumSCM.ERROR};
        Object[] arr3 = {EnumSCM.SH, EnumSCM.SUCCESS};
        Object[] arr4 = {EnumSCM.RECEIVE_IN_DC, EnumSCM.SUCCESS};
        Object[] arr5 = {EnumSCM.OFD, EnumSCM.ERROR};
        Object[] arr6 = {EnumSCM.DL, EnumSCM.ERROR};
        Object[] arr7 = {EnumSCM.UNRTO, EnumSCM.ERROR};

        Object[][] dataSet = new Object[][] {  arr1, arr2,arr3,arr4,arr5,arr6};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20,20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] findShipmentsByTripNumber(ITestContext testContext)
    {
        Object[] arr1 = {"Raghavendra-190213162954981?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID};
        Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] findShipmentsByTripNumberReverseBag(ITestContext testContext)
    {
        Object[] arr1 = {"Raghavendra-190213162954981?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID};
        Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 20, 20);
    }

    @DataProvider
    public static Object[][] rollingTripFGOn(ITestContext testContext) {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");

        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));

        Object[] arr1 = {"SA" + String.valueOf(contactNumber).substring(5, 9), "SA_FN" + String.valueOf(contactNumber).substring(5, 9), "SA_LN" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.DELIVERY_HUB_CODE_ELC,
                "LA_gstin" + String.valueOf(contactNumber).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.ROLLING_RECON, 500, "SA" + String.valueOf(contactNumber).substring(5, 9)};

        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }


    @DataProvider
    public static Object[][] HSR_HLP(ITestContext testContext) {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");

        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));

        Object[] arr1 = {"SA" + String.valueOf(contactNumber).substring(5, 9), "SA_FN" + String.valueOf(contactNumber).substring(5, 9), "SA_LN" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", LASTMILE_CONSTANTS.EOD_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.HSR_ML,
                "LA_gstin" + String.valueOf(contactNumber).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.ROLLING_RECON, 500, "SA" + String.valueOf(contactNumber).substring(5, 9)};

        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] dcToDcTransferForDLOrders(ITestContext testContext)
    {
        Object[] arr1 = {5, "DC", 1 , "DC" , EnumSCM.NORMAL,"ML", ShipmentType.DL , EnumSCM.SUCCESS };

        Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] dcToDcTransferForTryBuyOrders(ITestContext testContext)
    {
        Object[] arr1 = {42, "DC", 5 , "DC" , EnumSCM.NORMAL,"ML", ShipmentType.TRY_AND_BUY , EnumSCM.SUCCESS };

        Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] dcToDcTransferForExchangeOrders(ITestContext testContext)
    {
        Object[] arr1 = {5, "DC", 42 , "DC" , EnumSCM.NORMAL,"ML", ShipmentType.DL , EnumSCM.SUCCESS };

        Object[][] dataSet = new Object[][] { arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] addOrdersInTripsWithDiffMlShipmentStatus(ITestContext testContext)
    { // shipment type to be added
        Object[] arr1 = {EnumSCM.ASSIGNED_TO_DC, EnumSCM.ERROR, ShipmentType.DL.toString()};
        Object[] arr2 = {EnumSCM.ASSIGNED_TO_LAST_MILE_PARTNER, EnumSCM.ERROR, ShipmentType.DL.toString()};
        Object[] arr3 = {EnumSCM.ASSIGNED_TO_OTHER_DC, EnumSCM.ERROR, ShipmentType.DL.toString()};
        Object[] arr4 = {EnumSCM.ASSIGNED_TO_SDA, EnumSCM.SUCCESS, ShipmentType.DL.toString()};
        Object[] arr5 = {EnumSCM.DELIVERED, EnumSCM.ERROR, ShipmentType.DL.toString()};
        Object[] arr6 = {EnumSCM.EXPECTED_IN_DC, EnumSCM.ERROR, ShipmentType.DL.toString()};
        Object[] arr7 = {EnumSCM.FAILED_DELIVERY, EnumSCM.ERROR, ShipmentType.DL.toString()};
        Object[] arr8 = {EnumSCM.FAILED_PICKUP, EnumSCM.ERROR, ShipmentType.PU.toString()};
        Object[] arr9 = {EnumSCM.FAILED_TO_HANDOVER_TO_LAST_MILE_PARTNER, EnumSCM.ERROR, ShipmentType.DL.toString()};
        Object[] arr10 = {EnumSCM.HANDED_TO_LAST_MILE_PARTNER, EnumSCM.ERROR, ShipmentType.DL.toString()};
        Object[] arr11 = {EnumSCM.LOST, EnumSCM.ERROR, ShipmentType.DL.toString()};
        Object[] arr12 = {EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER, EnumSCM.ERROR, ShipmentType.PU.toString()};
        Object[] arr13 = {EnumSCM.ONHOLD_PICKUP_WITH_DC, EnumSCM.ERROR, ShipmentType.PU.toString()};
        Object[] arr14 = {EnumSCM.OUT_FOR_DELIVERY, EnumSCM.ERROR, ShipmentType.DL.toString()};
        Object[] arr15 = {EnumSCM.OUT_FOR_PICKUP, EnumSCM.ERROR,ShipmentType.PU.toString()};
        Object[] arr16 = {EnumSCM.PICKED_UP, EnumSCM.ERROR,ShipmentType.PU.toString()};
        Object[] arr17 = {EnumSCM.PICKUP_SUCCESSFUL, EnumSCM.ERROR,ShipmentType.PU.toString()};
        Object[] arr18 = {EnumSCM.RECEIVED_IN_DC, EnumSCM.SUCCESS, ShipmentType.DL.toString()};
        Object[] arr19 = {EnumSCM.REJECTED_ONHOLD_PICKUP_WITH_DC, EnumSCM.ERROR, ShipmentType.PU.toString()};
        Object[] arr20 = {EnumSCM.RETURN_IN_TRANSIT, EnumSCM.ERROR, ShipmentType.PU.toString()};
        Object[] arr21 = {EnumSCM.RETURN_REJECTED, EnumSCM.ERROR, ShipmentType.PU.toString()};
        Object[] arr22 = {EnumSCM.RTO_CONFIRMED, EnumSCM.ERROR, ShipmentType.PU.toString()};
        Object[] arr23 = {EnumSCM.RTO_DISPATCHED, EnumSCM.ERROR, ShipmentType.DL.toString()};
        Object[] arr24 = {EnumSCM.UNASSIGNED, EnumSCM.SUCCESS, ShipmentType.DL.toString()};

        Object[][] dataSet = new Object[][] { arr15,arr2,arr3, arr4, arr5, arr6, arr7, arr8, arr9, arr10, arr11, arr12, arr13,arr14,arr1,arr16,arr17,arr19,arr20,arr21,arr22,arr23,arr24};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] markingLostForDiffOrder(ITestContext testContext)
    {
        Object[] arr1 = { EnumSCM.PK, EnumSCM.SUCCESS};
        Object[] arr2 = { EnumSCM.IS, EnumSCM.SUCCESS};
        Object[] arr3 = { EnumSCM.ADDED_TO_MB, EnumSCM.SUCCESS};
        Object[] arr4 = { EnumSCM.SH, EnumSCM.SUCCESS};
        Object[] arr5 = { EnumSCM.OFD, EnumSCM.ERROR};
        Object[] arr6 = { EnumSCM.DL, EnumSCM.SUCCESS};
        Object[] arr7 = { EnumSCM.FD, EnumSCM.SUCCESS};

        Object[][] dataSet = new Object[][] { arr1, arr2, arr3, arr4, arr5, arr6, arr7};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] markingLostForDiffOrderExchange(ITestContext testContext)
    {
        Object[] arr1 = { EnumSCM.PK, EnumSCM.SUCCESS};
        Object[] arr2 = { EnumSCM.IS, EnumSCM.SUCCESS};
        Object[] arr3 = { EnumSCM.ADDED_TO_MB, EnumSCM.SUCCESS};
        Object[] arr4 = { EnumSCM.SH, EnumSCM.SUCCESS};

        Object[][] dataSet = new Object[][] { arr1, arr2, arr3, arr4 };
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] markingLostForDiffOrderTrynBuy(ITestContext testContext)
    {
        Object[] arr1 = { EnumSCM.PK, EnumSCM.SUCCESS};
        Object[] arr2 = { EnumSCM.IS, EnumSCM.SUCCESS};
        Object[] arr3 = { EnumSCM.ADDED_TO_MB, EnumSCM.SUCCESS};
        Object[] arr4 = { EnumSCM.SH, EnumSCM.SUCCESS};

        Object[][] dataSet = new Object[][] { arr1, arr2, arr3, arr4 };
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object [][] checkShippingMenthod(ITestContext testContext){
        Object[] array1={ShippingMethod.NORMAL.toString()};
        Object[] array2={ShippingMethod.EXPRESS.toString()};
        Object[] array3={ShippingMethod.SDD.toString()};
        return new Object[][] { array1,array2,array3 };
    }


    @DataProvider
    public static Object [][] forwardOrdersShippingLabel(ITestContext testContext){
        Object[] array1={"Forward","Fwd",false};
        Object[] array2={"TryAndBuy","TnB",true};
        Object[] array3={"Exchange","Ex",false};
        return new Object[][] { array1,array2,array3 };
    }
}




