package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.testbase.BaseTest;
import lombok.SneakyThrows;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class OpenBoxReturnTest extends BaseTest {

    private static org.slf4j.Logger log=LoggerFactory.getLogger(OpenBoxReturnTest.class);
    private OMSServiceHelper omsServiceHelper=new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper=new RMSServiceHelper();
    private LMSHelper lmsHepler=new LMSHelper();
    private ReturnHelper returnHelper=new ReturnHelper();
    LMS_OpenBoxReturnHelper lms_openBoxReturnHelper=new LMS_OpenBoxReturnHelper();
    StatusPollingValidator statusPollingValidator=new StatusPollingValidator();
    StoreHelper storeHelper=new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27859 Myntra Open Box Return for Alpha Seller (ML): SDA marks PickupDoneQCPending, DC Manager Approves After completing trip")
    public void OpenBoxReturnForAlphaSeller_SDA_PQCP_DC_Approve( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.PQCP_APPROVED_After_trip_close);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=false,
            description="C27860 Myntra Open Box Return for Non Alpha Seller (ML): SDA marks PickupDoneQCPending, DC Manager Approve After completing trip")
    public void OpenBoxReturnForNonAlphaSeller_SDA_PQCP_DC_Approve( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.JIT);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.PQCP_APPROVED_After_trip_close);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27861 Myntra Open Box Return for Alpha Seller (ML): SDA marks PickupDoneQCPending,DC Manager Approve Before Trip close")
    public void OpenBoxReturnForAlphaSeller_SDA_PQCP_DC_Approve_BeforeTripClose( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.PQCP_APPROVED_Before_trip_close);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27862 Myntra Open Box Return for Alpha Seller (ML): SDA marks PQCP, Before trip complete DC Manager On Hold ,after trip complete CC Approves")
    public void OpenBoxReturnForAlphaSeller_SDA_PQCP_DC_OnHold_CC_Approve_AfterTripComplete( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper
                .processOpenBoxReturn(returnId.toString(),EnumSCM.PQCP_ONHOLD_APPROVED_After_trip_close);

    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27863 Myntra Open Box Return for Alpha Seller (ML): SDA marks PQCP, DC Manager marks OnHold and CC also Approves both Before trip complete")
    public void OpenBoxReturnForAlphaSeller_SDA_PQCP_DC_OnHold_CC_Approve_BeforeTripComplete( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),
                EnumSCM.PQCP_ONHOLD_BEFORE_TRIP_CLOSE_APPROVED_BEFORE_TRIP_CLOSE);

    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27864 Myntra Open Box Return for Alpha Seller (ML): SDA marks PQCP, After trip complete DC Manager marks OnHold and CC also Approves")
    public void OpenBoxReturnForAlphaSeller_SDA_PQCP_AfterTripComplete_DC_OnHold_CC_Approve( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.PQCP_TRIP_CLOSE_ONHOLD_APPROVE);

    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27865 Myntra Open Box Return for Alpha Seller (ML): SDA marks PQCP, DC Manager OnHold Before trip complete, CC Rejects after trip complete")
    public void OpenBoxReturnForAlphaSeller_SDA_PQCP_BeforeTripComplete_DC_OnHold_CC_Reject_AfterTripComplete( )
            throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),
                EnumSCM.PQCP_ONHOLD_BEFORE_TRIP_CLOSE_REJECT_AFTER_TRIP_CLOSE);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27866 Myntra Open Box Return for Alpha Seller (ML): SDA marks PQCP, DC Manager marks On-Hold - Before trip complete, CC Rejects before trip complete")
    public void OpenBoxReturnForAlphaSeller_SDA_PQCP_BeforeTripComplete_DC_OnHold_CC_Reject_BeforeTripComplete( )
            throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),
                EnumSCM.PQCP_ONHOLD_BEFORE_TRIP_CLOSE_REJECT_BEFORE_TRIP_CLOSE);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27867 Myntra Myntra Open Box Return for Alpha Seller (ML): SDA marks PQCP,After trip complete DC Manager marks On-Hold and CC Rejects")
    public void OpenBoxReturnForAlphaSeller_SDA_PQCP_AfterTripComplete_DC_OnHold_CC_Reject( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.PQCP_TRIP_CLOSE_ONHOLD_REJECT);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27868 Myntra Open Box Pickup : SDA marks `Requested re-schedule`, close trip, DC Manager re-queues, PQCP -> On-hold -CC Approve")
    public void SDA_RequestedReschedule_CloseTrip_DC_RequeuePQPCOnHold_CC_Approve( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper
                .processOpenBoxReturn(returnId.toString(),EnumSCM.RESCHEDULED_Trip_close_ONHOLD_APPROVE);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27869 Myntra Open Box Pickup : SDA marks `Requested re-schedule`, close trip, DC Manager re-queues, PQCP -> On-hold -CC Reject")
    public void SDA_RequestedReschedule_CloseTrip_DC_RequeuePQPCOnHold_CC_Reject( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.RESCHEDULED_Trip_close_ONHOLD_REJECT);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27870 Myntra Open Box Return for Alpha Seller (ML), SDA marks Happy with product- Requeue")
//            retryAnalyzer = Retry.class)
    public void SDA_MarkHappyRequeue( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.HAPPY_REQUEUE);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=false,
            description="C27871 Myntra Open Box Return for Alpha Seller (ML), SDA marks Happy with product- Reject")
    public void SDA_MarkHappyReject( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.HAPPY_REJECT);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27872 Myntra OpenBox Alpha Seller-SDA marks Pickup On Hold Damaged Product, Before closing trip CC aaproves")
    public void OpenBoxReturnForAlphaSeller_SDA_PickedUp_DC_OnHold_Damaged_CC_Approve_TripClose( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper
                .processOpenBoxReturn(returnId.toString(),EnumSCM.PICKED_UP_ONHOLD_DAMAGED_APPROVE_TripCLose_Reship);
    }

    @Test(retryAnalyzer=Retry.class,groups={"Sanity"}, priority=0, enabled=true,
            description="C27873 Myntra OpenBox Alpha Seller-SDA marks Pickup On Hold Damaged Product, Before closing trip CC rejects")
    public void OpenBoxReturnForAlphaSeller_SDA_PickedUp_DC_OnHold_Damaged_CC_Reject_TripClose( ) throws Exception {

        String orderID=lmsHepler
                .createMockOrder(EnumSCM.DL,LMS_PINCODE.ML_BLR,"ML","36",EnumSCM.NORMAL,"cod",false,true);
        OrderReleaseEntry orderReleaseEntry=
                omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry=
                omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());

        //creating return for the order
        ReturnResponse returnResponse=
                rmsServiceHelper.createReturn(lineEntry.getId(),ReturnType.NORMAL,ReturnMode.OPEN_BOX_PICKUP,1,49L,
                        RefundMode.MYNTCREDIT,"418","test Open box address ","6135071",
                        "560068","Bangalore","Karnataka","India",LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Return creation failed");
        Long returnId=returnResponse.getData().get(0).getId();
        log.info("________ReturnId: "+returnId);
        //processing return
        lms_openBoxReturnHelper
                .processOpenBoxReturn(returnId.toString(),EnumSCM.PICKED_UP_ONHOLD_DAMAGED_REJECT_TRIP_CLOSE);

        //creating return for the order
        ReturnResponse returnResponse1=
                rmsServiceHelper.createReturn(lineEntry.getId(),ReturnType.NORMAL,ReturnMode.OPEN_BOX_PICKUP,1,49L,
                        RefundMode.MYNTCREDIT,"418","test Open box address ","6135071",
                        "560068","Bangalore","Karnataka","India",LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Return creation failed");
        Long returnId1=returnResponse1.getData().get(0).getId();
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId1.toString(),EnumSCM.PICKUP_SUCCESSFUL);


    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27874 Myntra Open Box Return for Alpha Seller (ML): SDA marks Pickup On Hold Damaged Product, After closing trip CC aaproves	")
    public void OpenBoxReturnForAlphaSeller_SDA_PickedUp_DC_OnHold_Damaged_TripClose_CC_Approve( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper
                .processOpenBoxReturn(returnId.toString(),EnumSCM.PICKED_UP_ONHOLD_DAMAGED_TripCLose_APPROVE);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27875 Myntra Open Box Return for Alpha Seller (ML): After SDA marks On-Hold and CC approves, SDA should NOT be allowed to mark onHold again")
    public void OpenBoxReturnForAlphaSeller_SDA_OnHold_CC_Approve_SDA_OnHoldFailed( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper
                .processOpenBoxReturn(returnId.toString(),EnumSCM.SDA_ONHOLD_CC_APPROVE_SDA_ONHOLD_Fail);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27876 Myntra Open Box Return : CC pre-approves from prism before manifestation")
    public void CCPreApproveBeforeManifest( ) throws Exception {
        LMS_ReturnHelper lms_returnHelper=new LMS_ReturnHelper();
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        ExceptionHandler.handleTrue(
                rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId)).getData().get(0)
                        .getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP),"");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(""+returnId);
        returnHelper.approveAtCC(String.valueOf(returnId));
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));

    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27876 Myntra Open Box Return : CC pre-approves from prism after manifestation")
    public void CCPreApproveAfterManifest( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.CC_PreApprove_After_Manifest);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27877 Myntra Open Box Return : CC pre-approves from prism on trip start, mark PQCP")
    public void TripStartCCPreApprovePQCP( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.TripStart_CCPreApprove_PQCP);
    }

    @Test(retryAnalyzer=Retry.class,groups={"Sanity"}, priority=0, enabled=true,
            description="C27878 Myntra Open Box Return : CC preapproves from prism on trip start, mark Pickup On Hold")
    public void TripStartCCPreApprovePickUpOnHold( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.TripStart_CCPreApprove_PickupOnHold);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27879 Myntra Open Box Return for Alpha Seller (ML), SDA marks reason Others- Requeue")
    public void SDA_MarkOthers_Requeue( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.SDA_REASON_OTHERS_REQUEUE);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27880 Myntra Open Box Return for Alpha Seller (ML), SDA marks reason Others-Reject")
    public void OpenBoxReturnForAlphaSeller_SDA_Marks_ReasonOthers_Reject( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.SDA_REASON_OTHERS_REJECT);
    }

    @Test(retryAnalyzer=Retry.class,groups={"Sanity"}, priority=0, enabled=true,
            description="C27881 Myntra Open Box Return for Alpha Seller (ML), SDA marks reason Happy With Product-Reject")
    public void OpenBoxReturnForAlphaSeller_SDA_Marks_ReasonHappy_Reject( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.SDA_REASON_HAPPY_REJECT);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27882 Myntra Open Box Return for Alpha Seller (ML), SDA marks reason Happy With Product-Reque")
    public void OpenBoxReturnForAlphaSeller_SDA_Marks_ReasonHappy_Requeue( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.SDA_Reason_Happy_REQUEUE);
    }

    @Test(retryAnalyzer=Retry.class,groups={"Sanity"}, priority=0, enabled=true,
            description="C27883 Myntra OpenBox Alpha Seller-SDA marks Pickup On Hold Damaged Product, Before closing trip CC rejects")
    public void OpenBoxReturnForAlphaSeller_SDA_PickUpOnHoldDamagedProduct_CC_REJECT_Trip_close( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),
                EnumSCM.SDA_PickUp_ONHOLD_DamagedProduct_CC_REJECT_CloseTrip);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27884 Myntra Open Box Return for Alpha Seller (ML): SDA marks Pickup On Hold Damaged Product, After closing trip CC rejects")
    public void OpenBoxReturnForAlphaSeller_SDA_PickUpOnHoldDamagedProduct_Trip_close_CC_REJECT( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),
                EnumSCM.SDA_PickUp_ONHOLD_DamagedProduct_Trip_close_CC_REJECT);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27885 Myntra Open Box For alpha : preapproval refund triggered immediately after marking PQCP even before trip closure")
    public void TripStartCCPreApprove_PQCP_Refund_TripClose( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper
                .processOpenBoxReturn(returnId.toString(),EnumSCM.TripStart_CCPreApprove_PQCP_Refund_Trip_Close);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27886 Preapproval : DC manager should not be able to reject an PQCP return once its preapproved")
    public void PreApproval_SDA_PQCP_CC_Approve_DC_RejectFailed( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.PQCP_CC_Approve_DC_RejectFailed);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27887 Return Label print from Returns page and Orders page also")
    public void ReturnLablePrintFromReturnPageOrdersPage( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.PrintReturnLable);
    }

    @Test(retryAnalyzer=Retry.class,groups={"Sanity"}, priority=0, enabled=true,
            description="C27888 Verify Signature Upload for Open box return")
    public void VerifySignatureUpload( ) throws Exception {
        final Long returnId=placeOrderCreateReturn(EnumSCM.NORMAL);
        //processing return
        returnHelper.uploadSignature(returnId);
    }

    @Test(retryAnalyzer=Retry.class, groups={"Sanity"}, priority=0, enabled=true,
            description="C27889 After 2 failed Pickups, DC manager Rejects the return , User should be able to create return again from the same order and process it")
    public void TwoFailedPickUp_DC_Reject_UserCreateReturnProcess( ) throws Exception {
        String orderID=lmsHepler
                .createMockOrder(EnumSCM.DL,LMS_PINCODE.ML_BLR,"ML","36",EnumSCM.NORMAL,"cod",false,true);
        OrderReleaseEntry orderReleaseEntry=
                omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry=
                omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());

        //creating return for the order
        ReturnResponse returnResponse=
                rmsServiceHelper.createReturn(lineEntry.getId(),ReturnType.NORMAL,ReturnMode.OPEN_BOX_PICKUP,1,49L,
                        RefundMode.MYNTCREDIT,"418","test Open box address ","6135071",
                        "560068","Bangalore","Karnataka","India",LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Return creation failed");
        Long returnId=returnResponse.getData().get(0).getId();
        log.info("________ReturnId: "+returnId);

        lms_openBoxReturnHelper.processOpenBoxReturn(returnId.toString(),EnumSCM.FAILED_PICK_UPS_DC_REJECT);

        //creating return for the order
        ReturnResponse returnResponse1=
                rmsServiceHelper.createReturn(lineEntry.getId(),ReturnType.NORMAL,ReturnMode.OPEN_BOX_PICKUP,1,49L,
                        RefundMode.MYNTCREDIT,"418","test Open box address ","6135071",
                        "560068","Bangalore","Karnataka","India",LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Return creation failed");
        Long returnId1=returnResponse1.getData().get(0).getId();
        lms_openBoxReturnHelper.processOpenBoxReturn(returnId1.toString(),EnumSCM.PICKUP_SUCCESSFUL);

    }

    public Long placeOrderCreateReturn(String orderType) throws Exception {
        // placing order
        String orderID=lmsHepler
                .createMockOrder(EnumSCM.DL,LMS_PINCODE.ML_BLR,"ML","36",EnumSCM.NORMAL,"cod",false,true);
        OrderReleaseEntry orderReleaseEntry=
                omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry=
                omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());

        //creating return for the order
        ReturnResponse returnResponse=
                rmsServiceHelper.createReturn(lineEntry.getId(),ReturnType.NORMAL,ReturnMode.OPEN_BOX_PICKUP,1,49L,
                        RefundMode.MYNTCREDIT,"418","test Open box address ","6135071",
                        "560068","Bangalore",
                        "Karnataka","India",LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,
                "Return creation failed");
        Long returnId=returnResponse.getData().get(0).getId();
        log.info("________ReturnId: "+returnId);
        return returnId;
    }

    @Test(enabled = true,description = "TC_ID : C28823,Return Report fixes for both open and closed box.")
    @SneakyThrows
    public void uploadPickupPreAlertFunctionalityOpenBoxPickup(){

        Long returnId = placeOrderCreateReturn(EnumSCM.NORMAL);
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),EnumSCM.LPI);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),EnumSCM.RETURN_CREATED);

        com.myntra.lms.client.domain.response.ReturnResponse returnResponse = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        String returnTrackingNumber = returnResponse.getDomainReturnShipments().get(0).getTrackingNumber();
        String warehouseId = returnResponse.getDomainReturnShipments().get(0).getReturnFacilityId();
        returnHelper.processReturnOrderTillPSState(returnId,ReturnMode.OPEN_BOX_PICKUP,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        statusPollingValidator.validateReturnStatusRMS(returnId.toString(),EnumSCM.RL);
        statusPollingValidator.validateReturnStatusLMS(returnId.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID),EnumSCM.RETURN_SUCCESSFUL);

        String filePath = lms_openBoxReturnHelper.createPreAlertReportCSV(returnTrackingNumber, CourierCode.ML.toString(), DateTimeHelper.generateDate("dd-MM-yyy",0),warehouseId, LMS_CONSTANTS.TENANTID);
        String uploadPreAlertReport =lms_openBoxReturnHelper.uploadPreAlertReport(filePath);
        Assert.assertTrue(uploadPreAlertReport.contains("1/1 updated successfully"),"Pre-Alert Report is not uploaded successfully");

        // validate for return shipment table
        com.myntra.lms.client.domain.response.ReturnResponse returnShipmentResponse = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertNotNull(returnShipmentResponse.getDomainReturnShipments().get(0).getExpectedReceivedDateInHub(),"Return received date is not stamped in return shipment table");
    }

}
