package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.DB.IQueries;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;

public class CashValidationTest implements StatusPoller {

    @Test(enabled = true, description = "TC ID:C28435,To check and verify the COD amount once shipment get packed.[validation:Order_to_ship= is_cod and cod amount, order_additional_info:total ,subtotal,tax]")
    public void cashValidationsLMSandOMSDBsForCODOrders() throws Exception {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.PACKED);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        //validation cod amount from OMS and OrderToShip table in LMS
        Double codAmount = orderResponse.getOrders().get(0).getCodAmount();
        String iscod = String.valueOf(orderResponse.getOrders().get(0).getCod());
        storeHelper.cashValidation(iscod, codAmount, packetId, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        //validate tracking number from lms db and oms db
        Map<String, Object> orderLineTableData = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.CashValidationQuery.orderLineTableData), packetId), "myntra_oms");
        Map<String, Object> orderReleaseData = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.CashValidationQuery.orderReleaseTableData), orderLineTableData.get("order_release_id_fk").toString()), "myntra_oms");
        Assert.assertEquals(orderReleaseData.get("tracking_no").toString(),trackingNumber,"Tracking number is not stamped correctly in OMS DB");
    }

    @Test(enabled = true, description = "TC ID:C28817,To Verify UPI Integration Scenario")
    public void cashValidationsLMSandOMSDBsForONLINEOrders() throws Exception {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "on", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.PACKED);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        //validation cod amount from OMS and OrderToShip table in LMS
        Double codAmount = orderResponse.getOrders().get(0).getCodAmount();
        String iscod = String.valueOf(orderResponse.getOrders().get(0).getCod());
        storeHelper.cashValidation(iscod, codAmount, packetId, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        //validate tracking number from lms db and oms db
        Map<String, Object> orderLineTableData = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.CashValidationQuery.orderLineTableData), packetId), "myntra_oms");
        Map<String, Object> orderReleaseData = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.CashValidationQuery.orderReleaseTableData), orderLineTableData.get("order_release_id_fk").toString()), "myntra_oms");
        Assert.assertEquals(orderReleaseData.get("tracking_no").toString(),trackingNumber,"Tracking number is not stamped correctly in OMS DB");
    }

    @Test(enabled = true, description = "TC ID:C28817,To Verify UPI Integration Scenario")
    public void cashValidationsLMSandOMSDBsForPrepaidOrders() throws Exception {
        //Create Mock order Till SH
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.PACKED);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        String zipcode = orderResponse.getOrders().get(0).getZipcode();

        //validation cod amount from OMS and OrderToShip table in LMS
        String orderReleaseId = omsServiceHelper.getReleaseId(orderID);
        lmsHelper.processOrderInSCM(EnumSCM.SH, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        statusPollingValidator.validateOrderStatus(packetId,EnumSCM.SHIPPED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber,LMS_CONSTANTS.TENANTID,EnumSCM.UNASSIGNED);

        //last mile operations till DL
        long tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(zipcode)).get("tripId"));
        lastmileOperations.assignOrderToTripByTrackingNumber(trackingNumber, tripId);
        lastmileOperations.startTrip(tripId, trackingNumber);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_PAYMENT_TYPE_DEBIT_CARD ).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        storeHelper.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID,tripId);
        statusPollingValidator.validateTripStatus(tripId,EnumSCM.COMPLETED);
        statusPollingValidator.validateOrderStatus(packetId,EnumSCM.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber,LMS_CONSTANTS.TENANTID,EnumSCM.DELIVERED);
        OrderResponse orderResponseForPrepaidOrders = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponseForPrepaidOrders.getOrders().get(0).getCod(),"is_cod is not true for the prepaid orders");
        //TODO need to check this scenario
        Assert.assertEquals(orderResponseForPrepaidOrders.getOrders().get(0).getCodAmount()==1099.0,"true","cod amount field have when order type as Prepaid ");

    }

}
