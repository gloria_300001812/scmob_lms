/**
 * Created by Abhishek Jaiswal on 15/11/18.
 */
package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.response.EmptyResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.DeliveryStaffCommute;
import com.myntra.lastmile.client.code.utils.DeliveryStaffRole;
import com.myntra.lastmile.client.code.utils.DeliveryStaffType;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.StoreTripResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.lms.client.status.OrderShipmentAssociationStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.platform.domain.*;
import com.myntra.lordoftherings.SlackMessenger;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.testbase.BaseTest;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.LAST_MILE_PARTNER;
import static com.myntra.lastmile.client.code.utils.ReconType.EOD_RECON;


public class LastmileTests1 extends BaseTest {

    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    TripClient_QA tripClient_qa = new TripClient_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa = new MLShipmentClientV2_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa = new TripOrderAssignmentClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa = new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa = new DeliveryStaffClient_QA();
    StoreClient_QA storeClient_qa = new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa = new MasterBagClient_QA();

    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private OrderEntry orderEntry = new OrderEntry();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private ProcessRelease processRelease = new ProcessRelease();
    private ItemEntry itemEntry = new ItemEntry();
    MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(LMS_Trip.class);
    HashMap<String, String> pincodeDCMap;

    //creating HashMap to maintain pincode and DcId
    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
        lmsHelper.setNdrConfigTo0();
    }

    public void checkReconStatus(String orderId, String tripOrderAssignmentId, String status, String isReceived) {
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(orderId, status, 4), "Wrong status in ml_shipment");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), isReceived, "Wrong status in trip_order_assignment");
    }


    //shipment Reconciliation  Add here

    //Full Forward Flow testing:
    /*Reconciling at every step in this flow
    Create 3 orders -> Fail 2 -> Deliver 1 -> Complete trip should Fail -> Reconcile -> Complete Trip -> assign new order to same DS Fail -> recocile -> complete trip-> requeue
    Last Order Create new Trip For same DL staff Dl it -> complete trip -> requeue another failed order assign to a new DS and Deliver it-> Reconcile should not be expected. -> Reconcile
    multiple times reconciled order. Validation trip order assignment :is_received and ml shipment Received in dc.
    */

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19284, Forward all flow test", enabled = true)
    public void failDlReconFullFlow() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);

        String orderId2 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId2 = omsServiceHelper.getPacketId(orderId2);

        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        String trackingNumber2 = lmsHelper.getTrackingNumber(packetId2);//DL

        String deliveryStaffID = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();

        //reconcile fail

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId1), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId2), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId2, EnumSCM.ASSIGNED_TO_SDA, 2));

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        //reconcile fail
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        dataMap3.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId2, tripId));
        dataMap3.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        tripData.add(dataMap3);
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.FAILED_DELIVERY, "false");

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,2L, EnumSCM.SUCCESS);
        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,1L,EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber1, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");


        //checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.RECEIVED_IN_DC, "true");

        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.RECEIVED_IN_DC, "true");
        checkReconStatus(packetId1, String.valueOf((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId)), EnumSCM.RECEIVED_IN_DC, "true");


        String orderId3 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId3 = omsServiceHelper.getPacketId(orderId3);

        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        String trackingNumber3 = lmsHelper.getTrackingNumber(packetId3);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber3, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        //reconcile fail
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId1),
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId3, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId1),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId3, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete trip.");
        tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId1);
        //reconcile
        checkReconStatus(packetId3, String.valueOf(tripOrderAssignmentId), EnumSCM.FAILED_DELIVERY, "false");

        // validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber3,1L,EnumSCM.SUCCESS);
        tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber3, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId1),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId3, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //checkReconStatus(packetId3, String.valueOf(tripOrderAssignmentId), EnumSCM.RECEIVED_IN_DC, "true");
        //requeue
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber3);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        TripResponse tripResponse2 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId2 = tripResponse2.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId3), tripId2, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        //reconcile fail
        Assert.assertEquals(tripClient_qa.startTrip(tripId2).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        //reconcile fail
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId2),
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId3, tripId2, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        //shipment count expected should be 0
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId2),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId3, tripId2, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        //shipment count expected should be 0 in reconcilition
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber);
        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        String deliveryStaffID2 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        TripResponse tripResponse3 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID2));
        Assert.assertEquals(tripResponse3.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId3 = tripResponse3.getTrips().get(0).getId();
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId3, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail
        Assert.assertEquals(tripClient_qa.startTrip(tripId3).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        //reconcile fail
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId3),
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId3, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        //shipment count expected should be 0

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId3),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, tripId3, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        System.out.println("Done");
        //shipment count expected should be 0 in reconcilition
        tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId3);
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

    }


    /*Create 2 exchange orders-> fail 1 -> exchange 1 -> requeue fail order -> New DS assign requeue order and Deliver -> assign new order to same DS Fail */

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19286 , Forward all flow test", enabled = true)
    public void failExchangeReconFullFlow() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);//exchange

        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder1 = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID1));
        String packetId1 = omsServiceHelper.getPacketId(exchangeOrder1);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);//Fail

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();


        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        //reconcile fail
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);//DL
        long tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId1);//FAILED

        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        String pickup_tracking_number = tripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber();

        orderEntry.setOperationalTrackingId(trackingNumber1.substring(2, trackingNumber1.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId1, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Thread.sleep(2000);

        //complete trip error
        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", tripOrderAssignmentId);
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        dataMap1.put("tripId", tripId);
        dataMap1.put("exchangeOrderId", packetId);
        dataMap2.put("trip_order_assignment_id", tripOrderAssignmentId1);
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        dataMap2.put("tripId", tripId);
        dataMap2.put("exchangeOrderId", packetId1);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        Thread.sleep(2000);
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
        checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentId1), EnumSCM.FAILED_DELIVERY, "false");

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2,trackingNumber.length()),2L,EnumSCM.SUCCESS);
        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,1L,EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber1, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");


        long tripOrderAssignmentIdForPickup = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(pickup_tracking_number);
        long tripOrderAssignmentIdOrderForFailedExchange = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(trackingNumber1);

        //trip complete
        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(tripId)).equalsIgnoreCase(EnumSCM.SUCCESS));
        checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentIdOrderForFailedExchange), EnumSCM.RECEIVED_IN_DC, "true");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForPickup)), "true", "Wrong status in trip_order_assignment");

        //requeue

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber1);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");


        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        //reconcile fail

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId1, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.OUT_FOR_DELIVERY, 2));
        Long tripOrderAssignmentId2 = (Long) lmsHelper.getTripOrderAssignmentIdByTrackingNumOFD.apply(trackingNumber1);// requed failed order

        String tripNumber = tripResponse1.getTrips().get(0).getTripNumber();

        //TODO: get correct ID from db by query
        orderEntry.setOperationalTrackingId(trackingNumber1.substring(2, trackingNumber1.length()));
        tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId2 + 1L,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId1, tripId1, orderEntry);

        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        pickup_tracking_number = tripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber();

        //reconcile exchanged order
        //validateDuringReconciliation(deliveryStaffID1, DcId, trackingNumber1.substring(2,trackingNumber1.length()),1L,EnumSCM.SUCCESS);

        tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber1.substring(2, trackingNumber1.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        tripOrderAssignmentIdForPickup = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNumPS.apply(pickup_tracking_number);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForPickup)), "true", "Wrong status in trip_order_assignment");


        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId2,
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        // new order to same delivery staff


        orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        packetId = omsServiceHelper.getPacketId(exchangeOrder);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);//exchange

        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        tripId = tripResponse.getTrips().get(0).getId();

        //reconcile fail
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);//DL

        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete trip.");
        pickup_tracking_number = tripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber();
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2,trackingNumber.length()),1L,EnumSCM.SUCCESS);

        tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");


        tripOrderAssignmentIdForPickup = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(pickup_tracking_number);

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForPickup)), "true", "Wrong status in trip_order_assignment");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19287, Forward all flow test", enabled = true)
    public void exchangeReconcileFlow() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);//exchange

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();

        //reconcile fail
        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,0L,EnumSCM.ERROR);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);//DL

        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete trip.");
        String pickup_tracking_number = tripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber();
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2,trackingNumber.length()),1L,EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        long tripOrderAssignmentIdForPickup = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(pickup_tracking_number);

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForPickup)), "true", "Wrong status in trip_order_assignment");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    /*
    Create 4 orders (Tried bought, not tried and bought, tried and not bought , Failed Try and buy) ->
    Should not receive (Tried and bought) receive (Not tried and bought, tried and not bought (2)) -> Requeue 2 received in dc order -> and deliver it -> Should not be expected in DC.
    * 1-Fail 2- Tried And not Bought 3-not tried and bought 4- Tried And bought
    * */
//Try And Buy

    //fail 2 try buy order requeue and deliver
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19288 , Forward all flow test", enabled = true)
    public void failTrynBuyRequeAndDlRecon() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);

        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);


        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();


        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        long tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId);

        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        orderEntry.setOperationalTrackingId(trackingNumber1.substring(2, trackingNumber1.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", tripOrderAssignmentId);
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);

        dataMap2.put("trip_order_assignment_id", tripOrderAssignmentId1);
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);

        tripData.add(dataMap1);
        tripData.add(dataMap2);

        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.FAILED_DELIVERY, "false");
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId1), EnumSCM.FAILED_DELIVERY, "false");

//        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber,2L, EnumSCM.SUCCESS);
//        validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1,1L,EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber1, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");


        //trip complete
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.RECEIVED_IN_DC, "true");
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId1), EnumSCM.RECEIVED_IN_DC, "true");

        //requeue 1
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        //requeue 2
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber1);
        mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");


        //create new trip
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber1 = tripResponse1.getTrips().get(0).getTripNumber();
        long tripId1 = tripResponse1.getTrips().get(0).getId();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail


        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNumOFD.apply(trackingNumber);
        tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNumOFD.apply(trackingNumber1);

        //reconcile fail

        //deliver order
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        orderEntryDetails = omsServiceHelper.getOrderEntry(orderID1);
        amount = orderEntryDetails.getFinalAmount();
        orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber1);
        Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber1.substring(2, trackingNumber1.length()));
        orderEntry.setOrderId(packetId1);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries1 = new ArrayList<>();
        entries1.add(itemEntry);
        orderEntry.setItemEntries(entries1);


        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId1, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


        dataMap1.put("trip_order_assignment_id", tripOrderAssignmentId);
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        dataMap2.put("trip_order_assignment_id", tripOrderAssignmentId1);
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        tripData.add(dataMap1);
        tripData.add(dataMap2);

        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

    }

    //tried and not bought order reconciliation
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19289, Forward all flow test", enabled = true)
    public void triedAndNotBoughtReconQCPass() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        Double amount = orderEntryDetails.getFinalAmount();
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);
        itemEntry.setRemarks("not bought");
        itemEntry.setQcStatus(ItemQCStatus.PASSED);
        itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
// mark delivered and reconcile shouldn't work

        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        String ml_shipment_id = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
        String return_tracking_num = lmsHelper.updateOperationalTrackingNumForTryAndBuy(ml_shipment_id);
        //reconcile

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //validateDuringReconciliation(deliveryStaffID, DcId, return_tracking_num.substring(2,return_tracking_num.length()),1L,EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(return_tracking_num.substring(2, return_tracking_num.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        Long tripOrderAssignmentIdForReturn = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(return_tracking_num);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForReturn)), "true", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(return_tracking_num), EnumSCM.PICKUP_SUCCESSFUL, "Status not updated to received in DC");
        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        System.out.println("Returns Tracking Number  " + return_tracking_num);

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(return_tracking_num);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.TRY_AND_BUY);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(return_tracking_num, LMS_CONSTANTS.TENANTID);
        String returnId = mlShipmentResponse2.getMlShipmentEntries().get(0).getSourceReferenceId();
        System.out.println("stop");

        long masterBagId = (long) tmsServiceHelper.createNcloseMBforReturn.apply(returnId);
        tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);


        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WH");
        Assert.assertEquals(
                lmsServiceHelper.receiveReturnShipmentFromMasterbag(masterBagId, String.valueOf(returnId)).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive return from masterbag in warehouse");


        ExceptionHandler.handleTrue(lmsReturnHelper.validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.DELIVERED_TO_SELLER.toString(), 10));
        lmsServiceHelper.validateRmsStatusAndRefund(returnId, EnumSCM.RRC, true, Constants.LMS_PATH.sleepTime);

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19290, Forward all flow test", enabled = true)
    public void triedAndNotBoughtReconQCFail() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        Double amount = orderEntryDetails.getFinalAmount();
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);
        itemEntry.setRemarks("not bought");
        itemEntry.setQcStatus(ItemQCStatus.FAILED);
        itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
// mark delivered and reconcile shouldn't work

        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        String ml_shipment_id = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
        String return_tracking_num = lmsHelper.updateOperationalTrackingNumForTryAndBuy(ml_shipment_id);
        //reconcile

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //validateDuringReconciliation(deliveryStaffID, DcId, return_tracking_num.substring(2,return_tracking_num.length()),1L,EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(return_tracking_num.substring(2, return_tracking_num.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");


        Long tripOrderAssignmentIdForReturn = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(return_tracking_num);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForReturn)), "true", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(return_tracking_num), EnumSCM.PICKUP_SUCCESSFUL, "Status not updated to received in DC");
        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        System.out.println("Returns Tracking Number  " + return_tracking_num);

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(return_tracking_num);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.TRY_AND_BUY);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(return_tracking_num, LMS_CONSTANTS.TENANTID);
        String returnId = mlShipmentResponse2.getMlShipmentEntries().get(0).getSourceReferenceId();
        System.out.println("stop");

        long masterBagId = (long) tmsServiceHelper.createNcloseMBforReturn.apply(returnId);
        tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);


        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WH");
        Assert.assertEquals(
                lmsServiceHelper.receiveReturnShipmentFromMasterbag(masterBagId, String.valueOf(returnId)).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive return from masterbag in warehouse");


        ExceptionHandler.handleTrue(lmsReturnHelper.validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.DELIVERED_TO_SELLER.toString(), 10));
        lmsServiceHelper.validateRmsStatusAndRefund(returnId, EnumSCM.RRC, true, Constants.LMS_PATH.sleepTime);

    }

    //tried and bought order shouldn't be expected in dc
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19291, Forward all flow test", enabled = true)
    public void triedAndBoughtRecon() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //reconcile fail

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();

        //reconcile fail

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2,trackingNumber.length()),0L,EnumSCM.ERROR);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to receive");

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    //not tried and bought -> recon not required
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19292, Forward all flow test", enabled = true)
    public void notTriedAndBoughtRecon() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();


        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.NOT_TRIED);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
    }

    //return flow starts here
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19293, Forward all flow test", enabled = true)
    public void failedPickup() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        SlackMessenger.send("scm_e2e_order_sanity", "Trip Creation for pickup complete");
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.FAILED, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        //reconcile not required for failed pickup
        // checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentId), EnumSCM.FAILED_PICKUP, "false");

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.FAILED_PICKUP, "shipment status mismatch");
        // checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentId), EnumSCM.RECEIVED_IN_DC, "true");

        //complete trip
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19294, Forward all flow test", enabled = true)
    public void pickupSuccessful() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKUP_SUCCESSFUL, "shipment status mismatch");

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19295, Forward all flow test", enabled = true)
    public void pickupDoneQCApproved() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_DONE, trackingNo);


        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");


    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19296, Forward all flow test", enabled = true)
    public void pickupDoneQCOnHold() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);


        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ONHOLD_PICKUP_WITH_DC, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");


    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19297, Forward all flow test", enabled = true)
    public void pickupDoneQCOnHoldccApprove() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 1L, EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);


        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ONHOLD_PICKUP_WITH_DC, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_APPROVE, false);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKUP_SUCCESSFUL, " Pickup expected to be successful");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19298 , Forward all flow test", enabled = true)
    public void pickupDoneQCOnHoldccReject() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 1L, EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");


        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ONHOLD_PICKUP_WITH_DC, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_REJECTED, false);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.REJECTED_ONHOLD_PICKUP_WITH_DC, "QC rejected by cc status mismatch");
    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19299, Forward all flow test", enabled = true)
    public void returnMarkedHappyWithProduct() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        SlackMessenger.send("scm_e2e_order_sanity", "Trip Creation for pickup complete");
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.HAPPY_WITH_PRODUCT, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        //reconcile not required for failed pickup
        // checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentId), EnumSCM.FAILED_PICKUP, "false");

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.FAILED_PICKUP, "shipment status mismatch");

        // checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentId), EnumSCM.RECEIVED_IN_DC, "true");

        //complete trip
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.HAPPY_WITH_PRODUCT, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.ASSIGNED_TO_DC, "shipment status mismatch");

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19300, Forward all flow test", enabled = true)
    public void onHoldCCReject() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        SlackMessenger.send("scm_e2e_order_sanity", "Trip Creation for pickup complete");
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER, "shipment status mismatch");


        //trip complete
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
        //rejected by cc
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_REJECTED, true);

        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.RETURN_REJECTED, "shipment status mismatch");

        //reconcile fail

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19301, Forward all flow test", enabled = true)
    public void onHoldCCApprove() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        SlackMessenger.send("scm_e2e_order_sanity", "Trip Creation for pickup complete");
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER, "shipment status mismatch");


        //trip complete
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        //rejected by cc
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_APPROVE, true);

        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.UNASSIGNED, "shipment status mismatch");

        //reconcile fail

        //pickup successful after cc approval

        //reconcile fail

        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail

        // scan tracking number in trip
        scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail

        // Start Trip
        startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKUP_SUCCESSFUL, "shipment status mismatch");

        // validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");


        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

    }

    // 8 march
    //TODO : check
    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, description = "ID: C496 , LMS E2E flow from order placement to DL and than create return and process return till receive back in WH", invocationCount = 1, enabled = false)
    public void LMS_Mock_ClosedBoxDE_Consolidation() throws Exception {
        //TODO : upload DE tracking numbers , enable DE serviceability for 400053
        //TODO : manifest function should take courier and return type as a parameter
        //TODO : enable DE for another pincode, so create 2 more returns with the other pincode for DE
        //TODO : enable another pincode for IP and then create 2 more returns with same data
        //TODO : find an address ID existing in DB and then create return for the same return id - see function fetchAddressIdForLogin()
        //Before creating any returns, manifest any old Closed Box return so that it does not go into this same pickup
        lmsReturnHelper.manifestClosedBoxPickups();

        //Create 1st order, place 2 returns for both the items in the order, have same addressId, address, mobile number, return type,courier, pincode

        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());

        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "testDEaddress", "6135071", LMS_PINCODE.NORTH_DE, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();

        String orderID1A = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry1A = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1A));
        OrderLineEntry lineEntry1A = omsServiceHelper.getOrderLineEntry(orderReleaseEntry1A.getOrderLines().get(0).getId().toString());

        ReturnResponse returnResponse2 = rmsServiceHelper.createReturn(lineEntry1A.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "testDEaddress", "6135071", LMS_PINCODE.NORTH_DE, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID2 = returnResponse2.getData().get(0).getId();

        lmsReturnHelper.manifestClosedBoxPickups();

        log.info("________ReturnId: " + returnID);
        log.info("________ReturnId: " + returnID2);
        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(String.valueOf(returnID), "DE");
        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(String.valueOf(returnID2), "DE");

        // Creating next order, placing 2 returns with same addres
        String orderID2 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry2 = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID2));
        OrderLineEntry lineEntry3 = omsServiceHelper.getOrderLineEntry(orderReleaseEntry2.getOrderLines().get(0).getId().toString());


        String orderID2A = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry2A = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID2A));
        OrderLineEntry lineEntry4 = omsServiceHelper.getOrderLineEntry(orderReleaseEntry2A.getOrderLines().get(0).getId().toString());


        ReturnResponse returnResponse3 = rmsServiceHelper.createReturn(lineEntry3.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "testDEaddress", "6135072", LMS_PINCODE.NORTH_DE, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse3.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID3 = returnResponse3.getData().get(0).getId();

        ReturnResponse returnResponse4 = rmsServiceHelper.createReturn(lineEntry4.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "testDEaddress", "6135072", LMS_PINCODE.NORTH_DE, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse4.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID4 = returnResponse4.getData().get(0).getId();

        log.info("________ReturnId: " + returnID3);
        log.info("________ReturnId: " + returnID4);
        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(String.valueOf(returnID3), "DE");
        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(String.valueOf(returnID4), "DE");

        //Creating 3rd order with different mobile number
        String orderID3 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry3 = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID3));
        OrderLineEntry lineEntry5 = omsServiceHelper.getOrderLineEntry(orderReleaseEntry3.getOrderLines().get(0).getId().toString());

        String orderID3A = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry3A = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID3A));

        OrderLineEntry lineEntry6 = omsServiceHelper.getOrderLineEntry(orderReleaseEntry3A.getOrderLines().get(0).getId().toString());


        ReturnResponse returnResponse5 = rmsServiceHelper.createReturn(lineEntry5.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "testDEaddress", "6135071", LMS_PINCODE.NORTH_DE, "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse5.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID5 = returnResponse5.getData().get(0).getId();

        ReturnResponse returnResponse6 = rmsServiceHelper.createReturn(lineEntry6.getId(), ReturnType.NORMAL, ReturnMode.CLOSED_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "testDEaddress", "6135071", LMS_PINCODE.NORTH_DE, "Bangalore", "Karnataka", "India", "0123456788");
        Assert.assertEquals(returnResponse6.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID6 = returnResponse6.getData().get(0).getId();
        lmsReturnHelper.manifestClosedBoxPickups();

        log.info("________ReturnId: " + returnID5);
        log.info("________ReturnId: " + returnID6);
        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(String.valueOf(returnID5), "DE");
        lmsReturnHelper.validateClosedBoxRmsLmsReturnCreationV2(String.valueOf(returnID6), "DE");

        Thread.sleep(5000);
        //Now manifest
        lmsReturnHelper.manifestClosedBoxPickups();
        Thread.sleep(5000);


        //Verify each pickup is manifested
        Assert.assertTrue(lmsReturnHelper.isPickupManifested(String.valueOf(returnID)), "Pickup associated with return - " + returnID + " is NOT manifested");
        Assert.assertTrue(lmsReturnHelper.isPickupManifested(String.valueOf(returnID2)), "Pickup associated with return - " + returnID2 + " is NOT manifested");
        Assert.assertTrue(lmsReturnHelper.isPickupManifested(String.valueOf(returnID3)), "Pickup associated with return - " + returnID3 + " is NOT manifested");
        Assert.assertTrue(lmsReturnHelper.isPickupManifested(String.valueOf(returnID4)), "Pickup associated with return - " + returnID4 + " is NOT manifested");
        Assert.assertTrue(lmsReturnHelper.isPickupManifested(String.valueOf(returnID5)), "Pickup associated with return - " + returnID5 + " is NOT manifested");
        Assert.assertTrue(lmsReturnHelper.isPickupManifested(String.valueOf(returnID6)), "Pickup associated with return - " + returnID6 + " is NOT manifested");

        //Verify consolidation of return 1&2 , 3&4, 5&6 respectively

        Assert.assertTrue(lmsReturnHelper.verifyClosedBoxConsolidation(String.valueOf(returnID), String.valueOf(returnID2)), "The return - " + returnID + " , and return - " + returnID2 + " are not consolidated into ONE pickup");
        Assert.assertTrue(lmsReturnHelper.verifyClosedBoxConsolidation(String.valueOf(returnID3), String.valueOf(returnID4)), "The return - " + returnID3 + " , and return - " + returnID4 + " are not consolidated into ONE pickup");
        Assert.assertTrue(lmsReturnHelper.verifyClosedBoxConsolidation(String.valueOf(returnID5), String.valueOf(returnID6)), "The return - " + returnID5 + " , and return - " + returnID6 + " are not consolidated into ONE pickup");
        Assert.assertFalse(lmsReturnHelper.verifyClosedBoxConsolidation(String.valueOf(returnID5), String.valueOf(returnID)), "The return - " + returnID5 + " , and return - " + returnID + " are  consolidated into ONE pickup inspite of differences");
        Assert.assertFalse(lmsReturnHelper.verifyClosedBoxConsolidation(String.valueOf(returnID5), String.valueOf(returnID2)), "The return - " + returnID5 + " , and return - " + returnID2 + " are  consolidated into ONE pickup inspite of differences");
        Assert.assertFalse(lmsReturnHelper.verifyClosedBoxConsolidation(String.valueOf(returnID3), String.valueOf(returnID6)), "The return - " + returnID3 + " , and return - " + returnID6 + " are  consolidated into ONE pickup inspite of differences");
        lmsReturnHelper.processClosedBoxPickup(String.valueOf(returnID), EnumSCM.PICKED_UP_SUCCESSFULLY);

    }

    //Uassigned
    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, description = "ID: C19282")
    public void markingLostForDiffOrder8() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String shipmentStatus = lmsServiceHelper.getOrderStatusFromML(packetId);// packetid = source ref id
        Assert.assertEquals(shipmentStatus, EnumSCM.UNASSIGNED, "Invalid Shipment Status");
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    //ASSIGNED_TO_OTHER_DC
    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, dataProviderClass = LastMileTestDP.class, dataProvider = "dcToDcTransferForDLOrders", description = "C19283")
    public void markingLostForDiffOrder9(long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.NORTH_CGH, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        MLShipmentResponse mlShipmentResponse = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator = new MLShipmentResponseValidator(mlShipmentResponse);
        mlShipmentResponseValidator.validateShipmentStatus(EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC

        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        MLShipmentResponse mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.ASSIGNED_TO_OTHER_DC);

        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    // EXPECTED_IN_DC
    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, dataProviderClass = LastMileTestDP.class, dataProvider = "dcToDcTransferForDLOrders", description = "C19283")
    public void markingLostForDiffOrder10(long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.NORTH_CGH, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        MLShipmentResponse mlShipmentResponse = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator = new MLShipmentResponseValidator(mlShipmentResponse);
        mlShipmentResponseValidator.validateShipmentStatus(EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC

        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        MLShipmentResponse mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.ASSIGNED_TO_OTHER_DC);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC

        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(1).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId + "," + transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        MLShipmentResponse mlShipmentResponse3 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator3 = new MLShipmentResponseValidator(mlShipmentResponse3);
        mlShipmentResponseValidator3.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);

        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    //TODO: pk and is should be allowed to mark lost but it's not happening
    //Lost order marking for exchange
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "markingLostForDiffOrderExchange", description = "ID: C19302 , markingLostForDiffOrder", enabled = true)
    public void markingLostForDiffOrderExchange(String toStatus, String res) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(toStatus, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), res, "Mismatch");

        if (res == EnumSCM.SUCCESS) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
            MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
            if (toStatus == EnumSCM.PK || toStatus == EnumSCM.IS) {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);
            } else {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);
            }
            //RTO
            statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
            Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

            mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
            mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
            if (toStatus == EnumSCM.PK || toStatus == EnumSCM.IS) {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);
            } else {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);
            }
        }


    }

    //ofd dl fd for exchange
    //TODO: unable to complete trip
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19302 , markingLostForDiffOrder", enabled = true)
    public void markingLostForDiffOrderExchange1() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);//exchange

        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder1 = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID1));
        String packetId1 = omsServiceHelper.getPacketId(exchangeOrder1);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);//Fail

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();


        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);//DL
        long tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId1);//FAILED

        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        String pickup_tracking_number = tripOrderAssignmentResponse.getTripOrders().get(0).getTrackingNumber();


        orderEntry.setOperationalTrackingId(trackingNumber1.substring(2, trackingNumber1.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId1, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Thread.sleep(2000);


        List<Map<String, Object>> tripData = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        dataMap1.put("trip_order_assignment_id", tripOrderAssignmentId);
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        dataMap1.put("tripId", tripId);
        dataMap1.put("exchangeOrderId", packetId);
        dataMap2.put("trip_order_assignment_id", tripOrderAssignmentId1);
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        dataMap2.put("tripId", tripId);
        dataMap2.put("exchangeOrderId", packetId1);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        Thread.sleep(2000);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse1);
        tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber1, LASTMILE_CONSTANTS.TENANT_ID);
        tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse1);

        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(tripId)).equalsIgnoreCase(EnumSCM.SUCCESS));


        //LOST
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");


        //LOST
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(trackingNumber1, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(trackingNumber1, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);


//        //complete trip error
//        List<Map<String, Object>> tripData = new ArrayList<>();
//        Map<String, Object> dataMap1 = new HashMap<>();
//        Map<String, Object> dataMap2 = new HashMap<>();
//        dataMap1.put("trip_order_assignment_id", tripOrderAssignmentId);
//        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
//        dataMap1.put("tripId", tripId);
//        dataMap1.put("exchangeOrderId", packetId);
//        dataMap2.put("trip_order_assignment_id", tripOrderAssignmentId1);
//        dataMap2.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
//        dataMap2.put("tripId", tripId);
//        dataMap2.put("exchangeOrderId", packetId1);
//        tripData.add(dataMap1);
//        tripData.add(dataMap2);
//        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.ERROR);
//        Thread.sleep(2000);
//        //reconcile
////        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
////        checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentId1), EnumSCM.FAILED_DELIVERY, "false");
//
//        // receive
//        //   validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2, trackingNumber.length()), 2L, EnumSCM.SUCCESS);
//        //  validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber1, 1L, EnumSCM.SUCCESS);
//
//        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
//        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse1);
////
////         tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber1, LASTMILE_CONSTANTS.TENANT_ID);
////         tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse1);
////
////        long tripOrderAssignmentIdForPickup = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(pickup_tracking_number);
////        long tripOrderAssignmentIdOrderForFailedExchange = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(trackingNumber1);
////        checkReconStatus(packetId1, String.valueOf(tripOrderAssignmentIdOrderForFailedExchange), EnumSCM.RECEIVED_IN_DC, "true");
////        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForPickup)), "true", "Wrong status in trip_order_assignment");
//
//        //trip complete
//        //TODO: Unable to complete trip which should be allowed as all the order are marked RTO.
//        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);


//        //LOST
//        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
//        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
//
//        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
//        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
//
//        //RTO
//        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
//        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");//EXCHANGED
//        //RTO
//        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
//        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch"); //FD

    }

    //this
    // Assigned To SDA and Received IN DC status
    //TODO: status not going to failed delivery
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19302 , markingLostForDiffOrderExchange2", enabled = true)
    public void markingLostForDiffOrderExchange2() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);//FAILED


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Thread.sleep(1000l);
        orderEntry.setOperationalTrackingId(tracking_num.substring(2, tracking_num.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");

        //Receiving at DC if FG is on for shipment recon use validate during recon method
        //validateDuringReconciliation(deliveryStaffID, DcId, tracking_num, 1L, EnumSCM.SUCCESS);
        lmsServiceHelper.receiveShipment(tracking_num);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    // RTO LOST RTO exchange order
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:  C19302, markingLostForDiffOrderExchange3", enabled = true)
    public void markingLostForDiffOrderExchange3() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        //
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        //RECEIVE
        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber, 1L, EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);


        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        // Re- queuing the tracking to get it delievered

        //MLShipmentUpdate
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("unassigning order for reattempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID1 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        orderEntry.setOperationalTrackingId(trackingNumber1.substring(2, trackingNumber1.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        //receive

        //validateDuringReconciliation(deliveryStaffID1, DcId, trackingNumber1, 1L, EnumSCM.SUCCESS);
        tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber1, LASTMILE_CONSTANTS.TENANT_ID);
        tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        Thread.sleep(3000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId, tripId1, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        System.out.println("RTO");
        Thread.sleep(3000);

        String returnId = lmsHelper.getSourceReturnId(packetId);
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber1);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("initiating RTO for shipment");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.EXCHANGE);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.RTO_CONFIRMED, 2));

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(trackingNumber1, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);


        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(trackingNumber1, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    //Uassigned
    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, description = "C19302")
    public void markingLostForDiffOrderExchnange4() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String shipmentStatus = lmsServiceHelper.getOrderStatusFromML(packetId);// packetid = source ref id
        Assert.assertEquals(shipmentStatus, EnumSCM.UNASSIGNED, "Invalid Shipment Status");

        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(trackingNumber, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(trackingNumber, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, dataProviderClass = LastMileTestDP.class, dataProvider = "dcToDcTransferForDLOrders", description = "C19302")
    public void markingLostForDiffOrderExchange5(long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.NORTH_CGH, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);

        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        MLShipmentResponse mlShipmentResponse = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator = new MLShipmentResponseValidator(mlShipmentResponse);
        mlShipmentResponseValidator.validateShipmentStatus(EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC

        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        MLShipmentResponse mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.ASSIGNED_TO_OTHER_DC);
        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    // EXPECTED_IN_DC
    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, dataProviderClass = LastMileTestDP.class, dataProvider = "dcToDcTransferForDLOrders")
    public void markingLostForDiffOrderExchange6(long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.NORTH_CGH, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);

        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        MLShipmentResponse mlShipmentResponse = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator = new MLShipmentResponseValidator(mlShipmentResponse);
        mlShipmentResponseValidator.validateShipmentStatus(EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC

        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        MLShipmentResponse mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.ASSIGNED_TO_OTHER_DC);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC

        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(1).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId + "," + transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        MLShipmentResponse mlShipmentResponse3 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator3 = new MLShipmentResponseValidator(mlShipmentResponse3);
        mlShipmentResponseValidator3.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);

        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    // try buy lost scenarios
    //LOST scenarios
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "markingLostForDiffOrderTrynBuy", description = "ID: C19305 , markingLostForDiffOrder", enabled = true)
    public void markingLostForDiffOrderTrynBuy(String toStatus, String res) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(toStatus, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderID);
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), res, "Mismatch");

        if (res == EnumSCM.SUCCESS) {
            MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
            MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
            if (toStatus == EnumSCM.PK || toStatus == EnumSCM.IS) {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);
            } else {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);
            }

            //RTO
            statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
            Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), res, "Mismatch");

            mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
            mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
            if (toStatus == EnumSCM.PK || toStatus == EnumSCM.IS) {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);
            } else {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);
            }
        }

    }

    //OFD ,DL
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19305 , markingLostForDiffOrderTrynBuy1", enabled = true)
    public void markingLostForDiffOrderTrynBuy1() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");


        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();


        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //LOST
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    //FD , assigned to sda
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19305  , markingLostForDiffOrderTrynBuy1", enabled = true)
    public void markingLostForDiffOrderTrynBuy2() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);

        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        //LOST
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(trackingNumber, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(trackingNumber, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    // Receive in DC
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19305  , markingLostForDiffOrderTrynBuy3", enabled = true)
    public void markingLostForDiffOrderTrynBuy3() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        Double amount = orderEntryDetails.getFinalAmount();
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);
        itemEntry.setRemarks("not bought");
        itemEntry.setQcStatus(ItemQCStatus.PASSED);
        itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
// mark delivered and reconcile shouldn't work

        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        String ml_shipment_id = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
        String return_tracking_num = lmsHelper.updateOperationalTrackingNumForTryAndBuy(ml_shipment_id);
        //reconcile

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //receive
        // validateDuringReconciliation(deliveryStaffID, DcId, return_tracking_num.substring(2, return_tracking_num.length()), 1L, EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(return_tracking_num.substring(2, return_tracking_num.length()), LASTMILE_CONSTANTS.TENANT_ID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse1);


        Long tripOrderAssignmentIdForReturn = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(return_tracking_num);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForReturn)), "true", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(return_tracking_num), EnumSCM.PICKUP_SUCCESSFUL, "Status not updated to received in DC");
        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

    }

    //rto
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19305 , markingLostForDiffOrderTrynBuy4", enabled = true)
    public void markingLostForDiffOrderTrynBuy4() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);

        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        //receive
        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2, trackingNumber.length()), 1L, EnumSCM.SUCCESS);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse1);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(10000);

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("initiating RTO for shipment");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.TRY_AND_BUY);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        Thread.sleep(10000);

        //LOST
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(trackingNumber, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);
        Thread.sleep(10000);
        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");


    }

    //unassigned
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19305  , markingLostForDiffOrderTrynBuy4", enabled = true)
    public void markingLostForDiffOrderTrynBuy5() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String shipmentStatus = lmsServiceHelper.getOrderStatusFromML(packetId);// packetid = source ref id
        Assert.assertEquals(shipmentStatus, EnumSCM.UNASSIGNED, "Invalid Shipment Status");

        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(trackingNumber, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(trackingNumber, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    //ASSIGNED_TO_OTHER_DC
    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, dataProviderClass = LastMileTestDP.class, dataProvider = "dcToDcTransferForDLOrders", description = "C19305")
    public void markingLostForDiffOrderTrynBuy6(long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.NORTH_CGH, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);

        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        MLShipmentResponse mlShipmentResponse = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator = new MLShipmentResponseValidator(mlShipmentResponse);
        mlShipmentResponseValidator.validateShipmentStatus(EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC

        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        MLShipmentResponse mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.ASSIGNED_TO_OTHER_DC);
        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    // EXPECTED_IN_DC
    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, dataProviderClass = LastMileTestDP.class, dataProvider = "dcToDcTransferForDLOrders", description = "C19305")
    public void markingLostForDiffOrderTrynBuy7(long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.NORTH_CGH, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);

        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        MLShipmentResponse mlShipmentResponse = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator = new MLShipmentResponseValidator(mlShipmentResponse);
        mlShipmentResponseValidator.validateShipmentStatus(EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC

        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        MLShipmentResponse mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.ASSIGNED_TO_OTHER_DC);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC

        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(1).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId + "," + transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        MLShipmentResponse mlShipmentResponse3 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator3 = new MLShipmentResponseValidator(mlShipmentResponse3);
        mlShipmentResponseValidator3.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);

        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    //return creation for try and buy order
    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, description = "C19306")
    public void returnForTrynBuyOrder() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();


        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());

        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnId);
        lms_returnHelper.processOpenBoxReturn(returnId.toString(), EnumSCM.PICKED_UP_SUCCESSFULLY);
        String trackingNo = (String) lmsHelper.getReturnsTrackingNumber.apply(returnId);

    }

    //return creation for not tried and bought item
    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, description = "C19307")
    public void returnForNotTriedAndBought() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();


        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.NOT_TRIED);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        //reconcile
        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());

        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnId);
        lms_returnHelper.processOpenBoxReturn(returnId.toString(), EnumSCM.PICKED_UP_SUCCESSFULLY);
        String trackingNo = (String) lmsHelper.getReturnsTrackingNumber.apply(returnId);

    }

    //return creation for exchange
    @Test(groups = {"P0", "Smoke", "Regression"}, priority = 2, description = "C19308")
    public void returnForExchangeOrder() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripNumber, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateTripOrderStatus(EnumSCM.OFD);
        tripOrderAssignmentResponseValidator.validateTripId(tripId);
        tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNumber);
        tripOrderAssignmentResponseValidator.validateExchangeOrderId(packetId);
        Assert.assertEquals(tripOrderAssignmentResponse.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant Id in response");
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        // validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2, trackingNumber.length()), 1L, EnumSCM.SUCCESS);
        tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");


        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(exchangeOrder));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        Thread.sleep(4000);
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnId);
        lms_returnHelper.processOpenBoxReturn(returnId.toString(), EnumSCM.PICKED_UP_SUCCESSFULLY);
        String trackingNo = (String) lmsHelper.getReturnsTrackingNumber.apply(returnId);

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "online order", enabled = true)
    public void onlineOrder() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "on", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId);
        System.out.println("Tracking Number" + trackingNum1);
    }

    //TODO: Test 5 -> FD->Receive in DC->requeu-> DL
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "online order", enabled = true)

    public void FD_ReceiveDC_Requeue_DL_and_DL_LOST_RTO() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);

        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String deliveryStaffID2 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        //get current account pendency of IK to Myntra
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// FD

        String trackingNumber = lmsHelper.getTrackingNumber(packetId);

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order

        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);

        // OFD for FD and DL Order
        // Received_in_dc
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));


        //Failing and delivering the order

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Map<String, Object> dataMap1 = new HashMap<>();
        List<Map<String, Object>> tripData = new ArrayList<>();

        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        tripData.add(dataMap1);

        //Receiving the FD order in DC
        //after receiving the FD order at dc liability will be removed from SDA of that good

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        //complete trip
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //account pendency

        //FD order after received in dc to Requeue
        //requeue

        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.autoAssignmentOfOrderToTrip(packetId, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator1 = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse3);
        //Delivering the [FD->Receive in DC->requeued order]
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID2));
        tripId = tripResponse1.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.DELIVERED, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");


    }

    //TODO: Test 2 -> FD->Receive in DC -> rto -> dls, DL in TRIP 2
    //trackingNumber = trackingNum1 and trackingNumber1 = trackingNum2
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "online order", enabled = true)
    public void FD_ReceiveDC_RTO_and_DL() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// FD
        //String trackingNum1 = lmsHelper.getTrackingNumber(packetId);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1); // DL
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);

        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);


        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);

        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        // OFD for FD and DL Order
        // Received_in_dc
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId1, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.OUT_FOR_DELIVERY, 2));


        //Failing and delivering the order

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();

        List<Map<String, Object>> tripData = new ArrayList<>();


        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        tripData.add(dataMap1);
        tripData.add(dataMap2);

        //Receiving the FD order in DC
        //after receiving the FD order at dc liability will be removed from SDA of that good

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        //complete trip
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());


        System.out.println("RTO");
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNumber);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("initiating RTO for shipment");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.DL);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        Thread.sleep(5000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.RTO_CONFIRMED, 2));

        TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
        long masterBagId = (long) tmsServiceHelper.createcloseMBforRTO(20L, packetId, DcId);
        tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);

        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WH");

        Assert.assertEquals(
                lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive shipment in DC");

        ExceptionHandler.handleTrue(lmsServiceHelper.validateOrderStatusInLMS(packetId, ShipmentStatus.RTO_IN_TRANSIT.toString(), 10));
        System.out.println("stop");
    }


//////////////////////////////////////////////////////

    //deleting the sda
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , Forward all flow test", enabled = true)
    public void deletingSDA() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));

        Map<String, Object> sda = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select * from delivery_staff where id = " + deliveryStaffID1, "lms");

        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Thread.sleep(3000);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateGetTripOrderByTripNumber(lmsHelper.getTrackingNumber(packetId), packetId, ShippingMethod.NORMAL.toString(), "Delivered");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //DELETING the SDA
        DeliveryStaffResponse deliveryStaffResponse1 = lmsServiceHelper.updateDeliveryStaff(sda.get("id").toString(), sda.get("code").toString(), sda.get("first_name").toString(), sda.get("last_name").toString(), Long.parseLong(sda.get("delivery_center_id").toString()), sda.get("mobile").toString(), sda.get("created_by").toString(), true, true, DeliveryStaffCommute.BIKER, sda.get("emp_code").toString(), true, DeliveryStaffRole.STAFF, sda.get("tenant_id").toString());
        Assert.assertEquals(deliveryStaffResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "SDA is not marked inactive");


    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , Forward all flow test", enabled = true)
    public void inactiveSDA() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));

        Map<String, Object> sda = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select * from delivery_staff where id = " + deliveryStaffID1, "lms");

        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Thread.sleep(3000);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateGetTripOrderByTripNumber(lmsHelper.getTrackingNumber(packetId), packetId, ShippingMethod.NORMAL.toString(), "Delivered");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //inactive the SDA
        DeliveryStaffResponse deliveryStaffResponse1 = lmsServiceHelper.updateDeliveryStaff(sda.get("id").toString(), sda.get("code").toString(), sda.get("first_name").toString(), sda.get("last_name").toString(), Long.parseLong(sda.get("delivery_center_id").toString()), sda.get("mobile").toString(), sda.get("created_by").toString(), false, false, DeliveryStaffCommute.BIKER, sda.get("emp_code").toString(), true, DeliveryStaffRole.STAFF, sda.get("tenant_id").toString());
        Assert.assertEquals(deliveryStaffResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "SDA is not marked inactive");

    }


    // prepaid normal flow
    //TODO : DL a normal order and pay via prepaid and check if pendency is removed from every stake holder
    @Test
    public void prepaid_dl_order() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// FD
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId);

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //

        // OFD for FD and DL Order
        // Received_in_dc
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        //


        //Failing and delivering the order
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_PAYMENT_TYPE_DEBIT_CARD).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_PAYMENT_TYPE_DEBIT_CARD).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


    }


    @Test
    public void assignSDAToAnotherDC() throws IOException, JAXBException, InterruptedException {
        String deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong("5"));
        Map<String, Object> sda = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select * from delivery_staff where id = " + deliveryStaffID1, "lms");
        // assign SDA to another DC

        DeliveryStaffResponse deliveryStaffResponse1 = lmsServiceHelper.updateDeliveryStaff(sda.get("id").toString(), sda.get("code").toString(), sda.get("first_name").toString(), sda.get("last_name").toString(), 1l, sda.get("mobile").toString(), sda.get("created_by").toString(), true, true, DeliveryStaffCommute.BIKER, sda.get("emp_code").toString(), true, DeliveryStaffRole.STAFF, sda.get("tenant_id").toString());
        Assert.assertEquals(deliveryStaffResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "SDA is not marked inactive");


    }

    //TODO : sda cannot be marked inactive during the trip
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , Forward all flow test", enabled = true)
    public void inactiveSDADuringTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);

        String deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        Map<String, Object> sda = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select * from delivery_staff where id = " + deliveryStaffID1, "lms");

        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Thread.sleep(3000);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateGetTripOrderByTripNumber(lmsHelper.getTrackingNumber(packetId), packetId, ShippingMethod.NORMAL.toString(), "Failed delivery");
        //inactive the SDA
        DeliveryStaffResponse deliveryStaffResponse1 = lmsServiceHelper.updateDeliveryStaff(sda.get("id").toString(), sda.get("code").toString(), sda.get("first_name").toString(), sda.get("last_name").toString(), Long.parseLong(sda.get("delivery_center_id").toString()), sda.get("mobile").toString(), sda.get("created_by").toString(), false, false, DeliveryStaffCommute.BIKER, sda.get("emp_code").toString(), true, DeliveryStaffRole.STAFF, sda.get("tenant_id").toString());
        Assert.assertEquals(deliveryStaffResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "SDA is not marked inactive");

    }
//create return Mark it failed pickup Reject it -> create another return and mark the first return as successful from Admin return page

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19280, dcToDcTransferForDLOrders", enabled = true)
    public void markRejectedReturnPickupSuccessfull() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        lms_returnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.FAILED_PICKUP);
        String trackingNo = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();
        Map<String, Object> shipment_status = (Map<String, Object>) lmsHelper.getShipmentStatusForPickup.apply(trackingNo);
        Assert.assertEquals(shipment_status.get("shipment_status"), EnumSCM.FAILED_PICKUP, "Expected failed pickup");
        //requeue
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNo);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("failed pickup requeued for another attempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        lms_returnHelper.processReturnWithoutNewValidation(returnID.toString(), EnumSCM.FAILED_PICKUP);
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNo);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("failed pickup rejected");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RETURN_REJECTED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        Thread.sleep(10000);
        Map<String, Object> shipment_status1 = (Map<String, Object>) lmsHelper.getShipmentStatusForPickup.apply(trackingNo);
        Assert.assertEquals(shipment_status1.get("shipment_status"), EnumSCM.PICKUP_REJECTED, "Expected failed pickup");

        //creating 2nd return
        ReturnResponse returnResponse1 = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        System.out.println("wait");
        PickupResponse pickupResponse = lmsServiceHelper.forceUpdateForReturn(String.valueOf(returnID), EnumSCM.RT, EnumSCM.RETURNED, EnumSCM.RETURNED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(pickupResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "When another return is created for the first return cannot be marked pickedup from admin correction");
        //Assert.assertEquals(pickupResponse.getStatus().getStatusMessage(), LASTMILE_CONSTANTS.FAILURE_FORCE_UPDATE_MESSAGE + " " + returnID, "failure message mismatch");
        Assert.assertEquals(String.valueOf(pickupResponse.getStatus().getStatusCode()), LASTMILE_CONSTANTS.STATUS_CODE_6661, "Mismatch status code");
    }

    //LOST scenarios
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = LastMileTestDP.class, dataProvider = "markingLostForDiffOrder", description = "ID: C19282 , markingLostForDiffOrder", enabled = true)
    public void markingLostForDiffOrder(String toStatus, String res) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(toStatus, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderID);
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), res, "Mismatch");

        if (res == EnumSCM.SUCCESS) {
            MLShipmentResponse mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
            MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
            if (toStatus == EnumSCM.PK || toStatus == EnumSCM.IS) {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);
            } else {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);
            }
            //RTO
            statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
            Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), res, "Mismatch");

            mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
            mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
            if (toStatus == EnumSCM.PK || toStatus == EnumSCM.IS) {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);
            } else {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);
            }
        }
    }

    // Assigned To SDA and Received IN DC status
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19282 , markingLostForDiffOrder", enabled = true)
    public void markingLostForDiffOrder1() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");

        //Receiving at DC if FG is on for shipment recon use validate during recon method
        //validateDuringReconciliation(deliveryStaffID, DcId, tracking_num, 1L, EnumSCM.SUCCESS);
        lmsServiceHelper.receiveShipment(tracking_num);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    //Assigned TO DC, out_for_pickup_fail_pickup
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19283 , markingLostForDiffOrder2", enabled = true)
    public void markingLostForDiffOrder2() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        log.info("Delivery Center ID :" + deliveryStaffID + "  Delivery Center ID : " + DcId);
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        Map<String, String> data = new HashMap<>();
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        // scan tracking number in trip
        log.info("Tracking ID := " + tripId);
        String trackingNo = String.valueOf(lmsHelper.getReturnsTrackingNumber.apply(returnID));
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        String tripOrderAssignmentId = String.valueOf(lmsHelper.getTripOrderAssignemntIdForReturn.apply(returnID));
        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.NOT_ABLE_TO_PICKUP, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.NOT_ABLE_TO_PICKUP, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

    }

    // pickup successful
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19283 , markingLostForDiffOrder2", enabled = true)
    public void markingLostForDiffOrder3() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail
        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo,0L,EnumSCM.ERROR);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKUP_SUCCESSFUL, "shipment status mismatch");


        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        //Receiving at DC if FG is on for shipment recon use validate during recon method

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);
        lmsServiceHelper.receiveShipment(trackingNo.substring(2, trackingNo.length()));
//
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

    }

    //picked up ,on hold pickup with dc
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19283 , markingLostForDiffOrder4", enabled = true)
    public void markingLostForDiffOrder4() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();


        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        //Receiving at DC if FG is on for shipment recon use validate during recon method

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);
        lmsServiceHelper.receiveShipment(trackingNo.substring(2, trackingNo.length()));


        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);


        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 0L, EnumSCM.ERROR);

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ONHOLD_PICKUP_WITH_DC, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");


        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 0L, EnumSCM.ERROR);
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_APPROVE, false);
        // validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 0L, EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKUP_SUCCESSFUL, " Pickup expected to be successful");

    }

    //ONHOLD_PICKUP_WITH_CUSTOMER , RETURN REJECTED
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19283 , markingLostForDiffOrder5", enabled = true)
    public void markingLostForDiffOrder5() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();


        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER, "shipment status mismatch");

        //LOST

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");

        //trip complete
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
        //rejected by cc
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_REJECTED, true);

        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.RETURN_REJECTED, "shipment status mismatch");

        //LOST
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(String.valueOf(lmsHelper.getTrackingNumber(packetId1)), LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse1 = mlShipmentClientV2_qa.getMLShipmentDetails(String.valueOf(lmsHelper.getTrackingNumber(packetId1)), LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }

    //this
    //Rejected On HOld Pickup with DC
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19283 , markingLostForDiffOrder5", enabled = true)
    public void markingLostForDiffOrder6() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        String track = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(returnResponse.getData().get(0).getOrderId().toString()));
        //reconcile fail

        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //reconcile fail


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        //reconcile fail

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reconcile fail

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //reconcile fail

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");

        //reconcile not required for failed pickup
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        //receive
        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()), 1L, EnumSCM.SUCCESS);

        lmsServiceHelper.receiveShipment(trackingNo.substring(2, trackingNo.length()));


        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ONHOLD_PICKUP_WITH_DC, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_REJECTED, false);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.REJECTED_ONHOLD_PICKUP_WITH_DC, "QC rejected by cc status mismatch");

        //LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Mismatch");


    }

    //RTO for DL order marking LOST
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: C19282 , markingLostForDiffOrder7", enabled = true)
    public void markingLostForDiffOrder7() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        String tracking_num = lmsHelper.getTrackingNumber(packetId);

        //  DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");

        // receive shipment
        // validateDuringReconciliation(deliveryStaffID, DcId, tracking_num, 1L, EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //requeue
//        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);
//        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);


        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.autoAssignmentOfOrderToTrip(packetId, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator1 = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse1);
        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID1 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");

        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId1 = tripResponse1.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId1, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);

        //receive
        //validateDuringReconciliation(deliveryStaffID1, DcId, tracking_num, 1L, EnumSCM.SUCCESS);
        tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);


        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId1),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(tracking_num);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.DL);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        Thread.sleep(3000);
        //marking LOST
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        MLShipmentResponse mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);
        Thread.sleep(3000);

        //RTO
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        mlShipmentResponse2 = mlShipmentClientV2_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
        mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse2);
        mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.RTO_CONFIRMED);

    }


}