package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.common.Constants.ShipmentSource;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.common.entries.ReleaseEntry;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.test.commons.testbase.BaseTest;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class LMS_Exchange  extends BaseTest {

    private static org.slf4j.Logger log = LoggerFactory.getLogger(LMS_Reverse_Exchange.class);
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    TMSServiceHelper tmsServiceHelper= new TMSServiceHelper();
    private LMSHelper lmsHepler = new LMSHelper();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private ProcessRelease processRelease = new ProcessRelease();
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();

    @Test(groups = {"Reverse", "P0", "Smoke", "Regression"}, description = "", priority = 2)
    public void E2E_LMS_ProcessExchangeWithMock() throws Exception {

        String orderIDParent = lmsHepler.createMockOrder(EnumSCM.DL,"560068", "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String   exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, "560068", "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderIDParent));
        String packetId  = omsServiceHelper.getPacketId(exchangeOrder);
        System.out.println("___________________________*** "+packetId);

    }

    }
