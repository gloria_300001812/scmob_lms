package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.common.entries.ReleaseEntry;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.lastmile.client.entry.StoreEntry;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.OrderShipmentAssociationStatus;
import com.myntra.lms.client.status.ShipmentStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.PacketEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.testbase.BaseTest;
import com.myntra.tms.masterbag.TMSMasterbagEntry;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertTrue;


public class LMS_MENSA {
    private static org.slf4j.Logger log = LoggerFactory.getLogger(LMS_MENSA.class);
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMS_StoreHelper lmsStoreHelper = new LMS_StoreHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private ProcessRelease processRelease = new ProcessRelease();

    @Test()
    public void Create_store() throws Exception{
        String MobileNumber = 91 +LMSUtils.randomGenn(8);
        String Code = "MENSA" + LMSUtils.randomGenn(4);
        StoreResponse storeResponse = (StoreResponse) lmsStoreHelper.createStore("ELC" , Code , MobileNumber);
        Assert.assertEquals(storeResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        String StoreId = storeResponse.getStoreEntries().get(0).getId().toString();
        lmsStoreHelper.validateStoreCreation(StoreId, Code,MobileNumber,"ELC");
    }

    @Test(dataProviderClass = LMSTestsDP.class, dataProvider = "Mensa_Add_orders_to_Store")
    public void Adding_orders_to_store(Long source,String sourceType , String shippingMethod ) throws Exception{
        String MobileNumber = 91 +LMSUtils.randomGenn(8);
        String Code = "MENSA" + LMSUtils.randomGenn(4);
        StoreResponse storeResponse = (StoreResponse) lmsStoreHelper.createStore("ELC" , Code , MobileNumber);
        Assert.assertEquals(storeResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        String StoreId = storeResponse.getStoreEntries().get(0).getId().toString();
        lmsStoreHelper.validateStoreCreation(StoreId, Code,MobileNumber,"ELC");
//        String storeBagId = lmsServiceHelper
//                .createMasterBag(source, sourceType, StoreId, "DC", shippingMethod, Code).getEntries().get(0)
//                .getId();


    }

}

