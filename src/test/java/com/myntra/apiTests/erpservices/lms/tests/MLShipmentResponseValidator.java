/**
 * Created by Abhishek Jaiswal on 15/11/18.
 */
package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import org.testng.Assert;


public class MLShipmentResponseValidator {
    MLShipmentResponse mlShipmentResponse;

    public MLShipmentResponseValidator(MLShipmentResponse mlShipmentResponse) {
        this.mlShipmentResponse = mlShipmentResponse;
        Assert.assertEquals(mlShipmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        if (mlShipmentResponse.getMlShipmentEntries() == null || mlShipmentResponse.getMlShipmentEntries().isEmpty())
            Assert.fail("No shipment entries available in the response");
    }


    /* BASE MODULAR FUNCTIONS FOR ML Shipment Response STARTS*/

    public void validateShipmentStatus(String status)
    {
        Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getShipmentStatus(), status, "Unexpected shipment status");
    }

    public void validateTrackingNumber(String trackingNum)
    {
        Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getTrackingNumber(), trackingNum, "Invalid Tracking Number");
    }

    public void validateSourceReferenceId(String Id)
    {
        Assert.assertEquals(mlShipmentResponse.getMlShipmentEntries().get(0).getSourceReferenceId(), Id, "Invalid Order ID ");
    }
}