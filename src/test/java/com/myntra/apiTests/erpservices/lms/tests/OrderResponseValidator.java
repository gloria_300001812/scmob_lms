/**
 * Created by Abhishek Jaiswal on 15/11/18.
 */
package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.lms.client.response.OrderResponse;
import org.apache.log4j.Logger;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;

public class OrderResponseValidator {
    OrderResponse lmsOrderResponse;
    private static Logger log = Logger.getLogger(OrderResponseValidator.class);//to remove


    private  static List<String> rtoComments = (List<String>) Arrays.asList("Requested re-schedule", "Refused to Accept", "Incomplete/Incorrect address", "Not reachable/unavailable");

    public OrderResponseValidator(OrderResponse lmsOrderResponse)
    {
        this.lmsOrderResponse = lmsOrderResponse;
    }

    /* BASE MODULAR FUNCTIONS FOR Order Response STARTS*/

    public void validateStatusType(String statusType)
    {
        Assert.assertEquals(lmsOrderResponse.getStatus().getStatusType().toString(), statusType, "Wrong status");
    }

    public void validateRtoCommentIsValid(int index)
    {
        //for (int i = 0; i < lmsOrderResponse.getOrders().size(); i++)
        Assert.assertTrue(rtoComments.contains(lmsOrderResponse.getOrders().get(index).getrTOComment()), "Invalid RTO Comment" + lmsOrderResponse.getOrders().get(index).getrTOComment());
    }

    public void validateStatusCode(String statusCode)
    {
        Assert.assertEquals(lmsOrderResponse.getStatus().getStatusCode(), Integer.parseInt(statusCode), "Status code Differs");
    }

    public void validateDeliveryCenterID(Long DcId)
    {
        Assert.assertEquals(lmsOrderResponse.getOrders().get(0).getDeliveryCenterId(), DcId, "Invalida DcID");
    }

    public void validateDeliveryCenterID(Long DcId, int index)
    {
        Assert.assertEquals(lmsOrderResponse.getOrders().get(index).getDeliveryCenterId(), DcId, "Invalida DcID");
    }

    public void validateTrackingNumber(String trackingNum)
    {
        Assert.assertEquals(lmsOrderResponse.getOrders().get(0).getTrackingNumber(), trackingNum, "Tracking Number Invalid");
    }

    public void validateTrackingNumber(String trackingNum, int index)
    {
        Assert.assertEquals(lmsOrderResponse.getOrders().get(index).getTrackingNumber(), trackingNum, "Tracking Number Invalid");
    }

    public  void validateOrderStatus(String status, int index)
    {
        Assert.assertEquals(lmsOrderResponse.getOrders().get(index).getStatus().toString(), status, "Unexpected Order Status");
    }

    public  void validateOrderStatus(String status)
    {
        Assert.assertEquals(lmsOrderResponse.getOrders().get(0).getStatus().toString(), status, "Unexpected Order Status");
    }

    public void validateStatusMessage(String statusMessage)
    {
        Assert.assertEquals(lmsOrderResponse.getStatus().getStatusMessage(), statusMessage, "Status Message Differs");
    }

    public void validateShipmentType(String shipmentType, int index)
    {
        Assert.assertEquals(lmsOrderResponse.getOrders().get(index).getShipmentType().toString(), shipmentType, "Unexpected shipment Type for index no : "+ index);
    }

    public void validateShipmentType(String shipmentType)
    {
        Assert.assertEquals(lmsOrderResponse.getOrders().get(0).getShipmentType().toString(), shipmentType, "Unexpected shipment Type for index no : ");
    }

    public void validateShipmentStatus(String shipmentStatus, int index)
    {
        Assert.assertEquals(lmsOrderResponse.getOrders().get(index).getLmsShipmentStatus().toString(), shipmentStatus, "Unexpected Shipment Status for index no :"+ index);
    }

    public void validateShipmentStatus(String shipmentStatus)
    {
        Assert.assertEquals(lmsOrderResponse.getOrders().get(0).getLmsShipmentStatus().toString(), shipmentStatus, "Unexpected Shipment Status for index no :");
    }

    public void validateFailedDeliveryCount(int index)
    {
        int failedCount = lmsOrderResponse.getOrders().get(index).getFailDeliveryCount().intValue();
        Assert.assertTrue(failedCount>=1, "Invalid Failed Delivery");
    }

    //NOT NULL

    //NOT NULL ENDS
    /* BASE MODULAR FUNCTIONS FOR Order Response ENDS*/


    //Validations
    public void validateGetTripResult()
    {
        for (int count = 0; count < lmsOrderResponse.getOrders().size(); count++)
        {
            validateRtoCommentIsValid(count);
            validateShipmentType("EXCHANGE", count);
            validateFailedDeliveryCount(count);
        }
    }

    public void validateGetTripResultPost(String shipmentType)
    {
        log.info("SHIPMENT TYPE : "+ shipmentType);
        for (int count = 0; count < lmsOrderResponse.getOrders().size(); count++) {
            validateShipmentType(shipmentType, count);
        }
    }

    public void validateShipmentStatusAndType(String shipmentType, String shipmentStatus, int index)
    {
        validateShipmentType(shipmentType,index);
        validateShipmentStatus(shipmentStatus, index);
    }

    public void validateStatusCodeAndMessage(String statusCode, String statusMessage)
    {
        validateStatusCode(statusCode);
        validateStatusMessage(statusMessage);
    }
}
