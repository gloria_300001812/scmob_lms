package com.myntra.apiTests.erpservices.lms.tests;
import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_StoreFlowsDP;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.utils.Context;
import com.myntra.lastmile.client.client.MLShipmentServiceV2Client;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.DeliveryStaffCommute;
import com.myntra.lastmile.client.code.utils.DeliveryStaffRole;
import com.myntra.lastmile.client.code.utils.DeliveryStaffType;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.ItemEntry;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.platform.domain.*;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.lordoftherings.SlackMessenger;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import org.apache.log4j.Logger;
import org.drools.command.assertion.AssertEquals;
import org.springframework.http.MediaType;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class LMS_MultipleSDA {

    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);


    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    StoreClient storeClient;
    TripOrderAssignmentClient tripOrderAssignmentClient;
    MLShipmentClientV2 mlShipmentServiceV2Client;
    LMSOrderDetailsClient lmsOrderDetailsClient;
    MasterBagClient masterBagClient;
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private OrderEntry orderEntry = new OrderEntry();
    private ProcessRelease processRelease = new ProcessRelease();
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa=new DeliveryStaffClient_QA();
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    private static Logger log = Logger.getLogger(LMS_Trip.class);
    HashMap<String, String> pincodeDCMap;
    ItemEntry itemEntry = new ItemEntry();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();

        storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        deliveryCenterClient = new DeliveryCenterClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        deliveryStaffClient = new DeliveryStaffClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }

    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void InActiveStoreandValidateforInActiveMultipleSDA(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                               String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        int noOfDeliveryStaffs = 2;
        List<Long> deliveryStaffIds = new ArrayList<>();
        Random r = new Random(System.currentTimeMillis());
        String searchParams = "code.like:" + code;

        // create Store and validate manager delivery staff

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        log.info("\n_____________Store Code : " +storeResponse.getStoreEntries().get(0).getCode()+ "Store ID :" +storeHlPId + "with Store Tenant ID : " +storeTenantId);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId,storeHlPId,storeTenantId,"MANAGER");
        deliveryStaffIds.add(storeDeliveryStaffId);

        // create multiple deliveryStaff for Store and Validate

        for (int o = 0 ; o< noOfDeliveryStaffs;o ++ ) {
            int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
            lastmileHelper.createStoreDeliveryStaff(storeHlPId,storeTenantId,contactNumber,DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
            long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getId();
            lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, storeTenantId, "STAFF");
            deliveryStaffIds.add(storeDeliveryStaffId1);
        }

        //inactive the Store and validate Store and Delivery Staff inactive
        Boolean isActive = false;

        StoreResponse storeResponse1 = lastmileHelper.UpdateStoreInActive(name, address, pincode, city, state,
                storeTenantId, isAvailable, isDeleted, isCardEnabled, isActive, code ,hlpReconType, storeHlPId );
        log.info(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive());
        Assert.assertEquals(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive().toString(),"false");

        for (int i =0 ; i<deliveryStaffIds.size();i++){
            long deliveryStaffId = deliveryStaffIds.get(i);
            Assert.assertEquals(deliveryStaffClient_qa.searchDeliveryStaffbyDeliveryStaffId(deliveryStaffId).getDeliveryStaffs().get(0).getAvailable().toString(),"false");
        }
    }

    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void InActiveStoreandValidateforInActiveMultipleSDACreateStoreBag(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                               String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        int noOfDeliveryStaffs = 2;
        List<Long> deliveryStaffIds = new ArrayList<>();
        Random r = new Random(System.currentTimeMillis());
        Long originPremiseId = 5l;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;

        // create Store and validate manager delivery staff

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        log.info("\n_____________Store Code : " +storeResponse.getStoreEntries().get(0).getCode()+ "Store ID :" +storeHlPId + "with Store Tenant ID : " +storeTenantId);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId,storeHlPId,storeTenantId,"MANAGER");
        deliveryStaffIds.add(storeDeliveryStaffId);

        // create multiple deliveryStaff for Store and Validate

        for (int o = 0 ; o< noOfDeliveryStaffs;o ++ ) {
            int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
            lastmileHelper.createStoreDeliveryStaff(storeHlPId,storeTenantId,contactNumber,DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
            long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getId();
            lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, storeTenantId, "STAFF");
            deliveryStaffIds.add(storeDeliveryStaffId1);
        }

              //inactive the Store and validate Store and Delivery Staff inactive
        Boolean isActive = false;

        StoreResponse storeResponse1 = lastmileHelper.UpdateStoreInActive(name, address, pincode, city, state,
                storeTenantId, isAvailable, isDeleted, isCardEnabled, isActive, code ,hlpReconType, storeHlPId );
        log.info(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive());
        Assert.assertEquals(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive().toString(),"false");

        // create Store Bag and Validate

        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        log.info("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        log.info("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        try {
            orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.ROLLING_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            log.info("The orderId is : +" + orderID);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Order creation in SH state failed : " + e.toString());
        }

        String packetId = omsServiceHelper.getPacketId(orderID);
        log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        log.info("Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
        log.info("\n_____________Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        try {

            String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (AssertionError a) {
            a.printStackTrace();
        }
        log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
        log.info("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
    }


    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void AssignOrderstoInActiveDeliveryStaff(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                                             String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        List<Long> deliveryStaffIds = new ArrayList<>();
        Random r = new Random(System.currentTimeMillis());
        Long originPremiseId = 5l;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        List<String> TrackingNumbersList1 = new ArrayList<>();
        Map<Long, List<String>> DeliveryStaffOrderMap = new HashMap<>();

        // create Store and validate manager delivery staff

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        log.info("\n_____________Store Code : " +storeResponse.getStoreEntries().get(0).getCode()+ "Store ID :" +storeHlPId + "with Store Tenant ID : " +storeTenantId);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId,storeHlPId,storeTenantId,"MANAGER");
        deliveryStaffIds.add(storeDeliveryStaffId);

        // create multiple deliveryStaff for Store and make that deliveryStaff inactive

            int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
            lastmileHelper.createStoreDeliveryStaff(storeHlPId,storeTenantId,contactNumber,DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
            long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getId();
            lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, storeTenantId, "STAFF");
            deliveryStaffIds.add(storeDeliveryStaffId1);
            long disabledDeliveryStaffId = storeDeliveryStaffId1;
            lastmileHelper.UpdateStoreDeliveryStaff(storeDeliveryStaffId1 , storeHlPId , storeTenantId , contactNumber , false , true , true);

        // create Store Bag and Validate

        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        log.info("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        log.info("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        try {
            orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.ROLLING_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            log.info("The orderId is : +" + orderID);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Order creation in SH state failed : " + e.toString());
        }

        String packetId = omsServiceHelper.getPacketId(orderID);
        log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        forwardTrackingNumbersList.add(trackingNo);
        trackingNoOrderIdMap.put(trackingNo,orderID);
        log.info("Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
        log.info("\n_____________Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        try {

            String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (AssertionError a) {
            a.printStackTrace();
        }
        log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
        log.info("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
        log.info("\n______________The status of the orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
        log.info("_____________The status of the  order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());


        // Create Trip

        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();

        // Assign storeBag to Trip and validate

        TripShipmentAssociationResponse tripShipmentAssociationResponse =tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        lastmileHelper.validateTripStartedForMB(masterBagId);

        // Delivery Store Bag to Store.

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId),forwardTrackingNumbersList , String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");

        //Validate if shipment_order_map status is DELIVERED

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));


        //Vaidate new ML entry created for Store tenant

        lastmileHelper.validateMLShipmentDetails(trackingNo , MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER , originPremiseId , tenantId , LMS_CONSTANTS.CLIENTID);

        // Assign Orders to multipleDeliveryStaff
//
//        TrackingNumbersList1.add(forwardTrackingNumbersList.get(0));
//        DeliveryStaffOrderMap.put(disabledDeliveryStaffId,TrackingNumbersList1);
//        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(DeliveryStaffOrderMap.get(disabledDeliveryStaffId), disabledDeliveryStaffId);
//


    }


    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void AssignOrderstoDeletedDeliveryStaff(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                    String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        List<Long> deliveryStaffIds = new ArrayList<>();
        Random r = new Random(System.currentTimeMillis());
        Long originPremiseId = 5l;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        List<String> TrackingNumbersList1 = new ArrayList<>();
        Map<Long, List<String>> DeliveryStaffOrderMap = new HashMap<>();

        // create Store and validate manager delivery staff

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        log.info("\n_____________Store Code : " +storeResponse.getStoreEntries().get(0).getCode()+ "Store ID :" +storeHlPId + "with Store Tenant ID : " +storeTenantId);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId,storeHlPId,storeTenantId,"MANAGER");
        deliveryStaffIds.add(storeDeliveryStaffId);

        // create multiple deliveryStaff for Store and DELETE that deliveryStaff

        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        lastmileHelper.createStoreDeliveryStaff(storeHlPId,storeTenantId,contactNumber,DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
        long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, storeTenantId, "STAFF");
        deliveryStaffIds.add(storeDeliveryStaffId1);
        long disabledDeliveryStaffId = storeDeliveryStaffId1;
        lastmileHelper.UpdateStoreDeliveryStaff(storeDeliveryStaffId1 , storeHlPId , storeTenantId , contactNumber , true , true , true);

        // create Store Bag and Validate

        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        log.info("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        log.info("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        try {
            orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.ROLLING_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            log.info("The orderId is : +" + orderID);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Order creation in SH state failed : " + e.toString());
        }

        String packetId = omsServiceHelper.getPacketId(orderID);
        log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        forwardTrackingNumbersList.add(trackingNo);
        trackingNoOrderIdMap.put(trackingNo,orderID);
        log.info("Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
        log.info("\n_____________Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        try {

            String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (AssertionError a) {
            a.printStackTrace();
        }
        log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
        log.info("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
        log.info("\n______________The status of the orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
        log.info("_____________The status of the  order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());


        // Create Trip

        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();

        // Assign storeBag to Trip and validate

        TripShipmentAssociationResponse tripShipmentAssociationResponse =tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        lastmileHelper.validateTripStartedForMB(masterBagId);

        // Delivery Store Bag to Store.

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId),forwardTrackingNumbersList , String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");

        //Validate if shipment_order_map status is DELIVERED

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));


        //Vaidate new ML entry created for Store tenant

        lastmileHelper.validateMLShipmentDetails(trackingNo , MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER , originPremiseId , tenantId , LMS_CONSTANTS.CLIENTID);

        // Assign Orders to multipleDeliveryStaff
//
//        TrackingNumbersList1.add(forwardTrackingNumbersList.get(0));
//        DeliveryStaffOrderMap.put(disabledDeliveryStaffId,TrackingNumbersList1);
//        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(DeliveryStaffOrderMap.get(disabledDeliveryStaffId), disabledDeliveryStaffId);
//


    }


    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void MarkDeliveryStaffInTriptoInActive(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                    String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        List<Long> deliveryStaffIds = new ArrayList<>();
        Random r = new Random(System.currentTimeMillis());
        Long originPremiseId = 5l;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        List<String> TrackingNumbersList1 = new ArrayList<>();
        Map<Long, List<String>> DeliveryStaffOrderMap = new HashMap<>();

        // create Store and validate manager delivery staff

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        log.info("\n_____________Store Code : " +storeResponse.getStoreEntries().get(0).getCode()+ "Store ID :" +storeHlPId + "with Store Tenant ID : " +storeTenantId);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId,storeHlPId,storeTenantId,"MANAGER");
        deliveryStaffIds.add(storeDeliveryStaffId);

        // create multiple deliveryStaff for Store and make that deliveryStaff inactive

        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        lastmileHelper.createStoreDeliveryStaff(storeHlPId,storeTenantId,contactNumber,DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
        long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, storeTenantId, "STAFF");
        deliveryStaffIds.add(storeDeliveryStaffId1);
        long disabledDeliveryStaffId = storeDeliveryStaffId1;

        // create Store Bag and Validate

        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        log.info("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        log.info("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        try {
            orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.ROLLING_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            log.info("The orderId is : +" + orderID);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Order creation in SH state failed : " + e.toString());
        }

        String packetId = omsServiceHelper.getPacketId(orderID);
        log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        forwardTrackingNumbersList.add(trackingNo);
        trackingNoOrderIdMap.put(trackingNo,orderID);
        log.info("Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
        log.info("\n_____________Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        try {

            String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (AssertionError a) {
            a.printStackTrace();
        }
        log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
        log.info("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
        log.info("\n______________The status of the orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
        log.info("_____________The status of the  order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());


        // Create Trip

        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();

        // Assign storeBag to Trip and validate

        TripShipmentAssociationResponse tripShipmentAssociationResponse =tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        lastmileHelper.validateTripStartedForMB(masterBagId);

        // Delivery Store Bag to Store.

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId),forwardTrackingNumbersList , String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");

        //Validate if shipment_order_map status is DELIVERED

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));


        //Vaidate new ML entry created for Store tenant

        lastmileHelper.validateMLShipmentDetails(trackingNo , MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER , originPremiseId , tenantId , LMS_CONSTANTS.CLIENTID);

       //  Assign Orders to multipleDeliveryStaff

        TrackingNumbersList1.add(forwardTrackingNumbersList.get(0));
        DeliveryStaffOrderMap.put(disabledDeliveryStaffId,TrackingNumbersList1);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(DeliveryStaffOrderMap.get(disabledDeliveryStaffId), disabledDeliveryStaffId);
        DeliveryStaffResponse deliveryStaffResponse = lastmileHelper.UpdateStoreDeliveryStaff(storeDeliveryStaffId1 , storeHlPId , storeTenantId , contactNumber , false , true , true);
        Assert.assertEquals(deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getAvailable().toString(),"true"
        ,"Able to make DeliveryStaff with Trip InActive");
    }



    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void MarkStorewithOrderstoInActive(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                  String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        List<Long> deliveryStaffIds = new ArrayList<>();
        Random r = new Random(System.currentTimeMillis());
        Long originPremiseId = 5l;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        List<String> TrackingNumbersList1 = new ArrayList<>();
        Map<Long, List<String>> DeliveryStaffOrderMap = new HashMap<>();

        // create Store and validate manager delivery staff

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        log.info("\n_____________Store Code : " +storeResponse.getStoreEntries().get(0).getCode()+ "Store ID :" +storeHlPId + "with Store Tenant ID : " +storeTenantId);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId,storeHlPId,storeTenantId,"MANAGER");
        deliveryStaffIds.add(storeDeliveryStaffId);

        // create multiple deliveryStaff for Store and make that deliveryStaff inactive

        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        lastmileHelper.createStoreDeliveryStaff(storeHlPId,storeTenantId,contactNumber,DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
        long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, storeTenantId, "STAFF");
        deliveryStaffIds.add(storeDeliveryStaffId1);
        long disabledDeliveryStaffId = storeDeliveryStaffId1;

        // create Store Bag and Validate

        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        log.info("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        log.info("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        try {
            orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.ROLLING_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            log.info("The orderId is : +" + orderID);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Order creation in SH state failed : " + e.toString());
        }

        String packetId = omsServiceHelper.getPacketId(orderID);
        log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        forwardTrackingNumbersList.add(trackingNo);
        trackingNoOrderIdMap.put(trackingNo,orderID);
        log.info("Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
        log.info("\n_____________Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        try {

            String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (AssertionError a) {
            a.printStackTrace();
        }
        log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
        log.info("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
        log.info("\n______________The status of the orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
        log.info("_____________The status of the  order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());


        // Create Trip

        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();

        // Assign storeBag to Trip and validate

        TripShipmentAssociationResponse tripShipmentAssociationResponse =tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        lastmileHelper.validateTripStartedForMB(masterBagId);

        // Delivery Store Bag to Store.

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId),forwardTrackingNumbersList , String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");

        //Validate if shipment_order_map status is DELIVERED

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));


        //Vaidate new ML entry created for Store tenant

        lastmileHelper.validateMLShipmentDetails(trackingNo , MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER , originPremiseId , tenantId , LMS_CONSTANTS.CLIENTID);

        //  Mark Store InActive

        //inactive the Store and validate Store and Delivery Staff inactive
        Boolean isActive = false;

        StoreResponse storeResponse1 = lastmileHelper.UpdateStoreInActive(name, address, pincode, city, state,
                storeTenantId, isAvailable, isDeleted, isCardEnabled, isActive, code ,hlpReconType, storeHlPId );
        log.info(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive());
        Assert.assertEquals(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive().toString(),"true","Able to mark store with Orders Inactive");

    }


    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void MarkStorewithFDOrderstoInActive(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                              String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        List<Long> deliveryStaffIds = new ArrayList<>();
        Random r = new Random(System.currentTimeMillis());
        Long originPremiseId = 5l;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        List<String> TrackingNumbersList1 = new ArrayList<>();
        Map<Long, List<String>> DeliveryStaffOrderMap = new HashMap<>();

        // create Store and validate manager delivery staff

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        log.info("\n_____________Store Code : " +storeResponse.getStoreEntries().get(0).getCode()+ "Store ID :" +storeHlPId + "with Store Tenant ID : " +storeTenantId);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId,storeHlPId,storeTenantId,"MANAGER");
        deliveryStaffIds.add(storeDeliveryStaffId);

        // create multiple deliveryStaff for Store and make that deliveryStaff inactive

        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        lastmileHelper.createStoreDeliveryStaff(storeHlPId,storeTenantId,contactNumber,DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
        long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, storeTenantId, "STAFF");
        deliveryStaffIds.add(storeDeliveryStaffId1);
        long disabledDeliveryStaffId = storeDeliveryStaffId1;

        // create Store Bag and Validate

        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        log.info("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        log.info("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        try {
            orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.ROLLING_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            log.info("The orderId is : +" + orderID);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Order creation in SH state failed : " + e.toString());
        }

        String packetId = omsServiceHelper.getPacketId(orderID);
        log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        forwardTrackingNumbersList.add(trackingNo);
        trackingNoOrderIdMap.put(trackingNo,orderID);
        log.info("Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
        log.info("\n_____________Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        try {

            String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (AssertionError a) {
            a.printStackTrace();
        }
        log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
        log.info("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
        log.info("\n______________The status of the orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
        log.info("_____________The status of the  order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());


        // Create Trip

        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();

        // Assign storeBag to Trip and validate

        TripShipmentAssociationResponse tripShipmentAssociationResponse =tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        lastmileHelper.validateTripStartedForMB(masterBagId);

        // Delivery Store Bag to Store.

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId),forwardTrackingNumbersList , String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");

        //Validate if shipment_order_map status is DELIVERED

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));


        //Vaidate new ML entry created for Store tenant

        lastmileHelper.validateMLShipmentDetails(trackingNo , MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER , originPremiseId , tenantId , LMS_CONSTANTS.CLIENTID);

        // Assign Orders to multipleDeliveryStaff

        TrackingNumbersList1.add(forwardTrackingNumbersList.get(0));
        DeliveryStaffOrderMap.put(disabledDeliveryStaffId,TrackingNumbersList1);


        // delivery orders to customer

            //Create a Store Trip and validate Trip

            String tracking_number = forwardTrackingNumbersList.get(0);
            TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(DeliveryStaffOrderMap.get(disabledDeliveryStaffId), disabledDeliveryStaffId);
            String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
            Long storeTripId = storeTrip.getTrips().get(0).getId();
            Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.name()));
            Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
            log.info("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " +storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + disabledDeliveryStaffId + " for the tracking number :  " + tracking_number + "\n\n********");

            //Validate the Store ML Shipment goes for OFD

            MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_number, storeTenantId);
            //Validate tenant is Storetenant Id and client is 2297
            Assert.assertTrue(mlShipmentResponse1.getMlShipmentEntries().get(0).getClientId().equals(LMS_CONSTANTS.TENANTID));
            Assert.assertTrue(mlShipmentResponse1.getMlShipmentEntries().get(0).getTenantId().equals(storeTenantId));
            //Validate the status for the Store Tenant ML entry as OUT_FOR_DELIVERY
            Assert.assertTrue(mlShipmentResponse1.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.OUT_FOR_DELIVERY.name()));
            //Validate DC id is the store Id
            Assert.assertTrue(mlShipmentResponse1.getMlShipmentEntries().get(0).getDeliveryCenterId().equals(storeHlPId));
            //Validate the OrderToShip (Platform ) status is OFD
            OrderResponse orderResponse = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(tracking_number),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
//            Assert.assertTrue(orderResponse.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.OUT_FOR_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.OUT_FOR_DELIVERY.name());

            // Mark FAILED_DELIVERY Orders to customer

            TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getTripDetails(tracking_number, storeTenantId);
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
            tripClient_qa.failedDeliverStoreOrders(storeTripId, tracking_number, tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);

            //Validate Store ML shipment is FAILED_DELIVERY

            MLShipmentResponse mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_number, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //Validate Myntra tenant ML Shipment is FAILED_DELIVERY

            mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_number, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.FAILED_DELIVERY.name()));

            //Validate the ORderToShip (Platform ) status is FAILED_DELIVERY

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(tracking_number),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
           // Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.FAILED_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.DELIVERED.name());

        //  Mark Store InActive

        Boolean isActive = false;

        StoreResponse storeResponse1 = lastmileHelper.UpdateStoreInActive(name, address, pincode, city, state,
                storeTenantId, isAvailable, isDeleted, isCardEnabled, isActive, code ,hlpReconType, storeHlPId );
        log.info(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive());
        Assert.assertEquals(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive().toString(),"true","Able to mark store with Orders Inactive");

    }



    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void AssignSameOrderstoMultipleSDA(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        List<Long> deliveryStaffIds = new ArrayList<>();
        Random r = new Random(System.currentTimeMillis());
        Long originPremiseId = 5l;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        List<String> TrackingNumbersList1 = new ArrayList<>();
        Map<Long, List<String>> DeliveryStaffOrderMap = new HashMap<>();

        // create Store and validate manager delivery staff

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        log.info("\n_____________Store Code : " +storeResponse.getStoreEntries().get(0).getCode()+ "Store ID :" +storeHlPId + "with Store Tenant ID : " +storeTenantId);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId,storeHlPId,storeTenantId,"MANAGER");
        deliveryStaffIds.add(storeDeliveryStaffId);

        // create multiple deliveryStaff for Store and make that deliveryStaff inactive

        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        lastmileHelper.createStoreDeliveryStaff(storeHlPId,storeTenantId,contactNumber,DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
        long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, storeTenantId, "STAFF");
        deliveryStaffIds.add(storeDeliveryStaffId1);
        long disabledDeliveryStaffId = storeDeliveryStaffId1;

        // create Store Bag and Validate

        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        log.info("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        log.info("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        try {
            orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.ROLLING_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
            log.info("The orderId is : +" + orderID);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Order creation in SH state failed : " + e.toString());
        }

        String packetId = omsServiceHelper.getPacketId(orderID);
        log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        forwardTrackingNumbersList.add(trackingNo);
        trackingNoOrderIdMap.put(trackingNo,orderID);
        log.info("Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
        log.info("\n_____________Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
        try {

            String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (AssertionError a) {
            a.printStackTrace();
        }
        log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
        log.info("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

        //Verify if the order is added in Bag
        OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
        Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
        log.info("\n______________The status of the orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
        log.info("_____________The status of the  order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());


        // Create Trip

        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();

        // Assign storeBag to Trip and validate

        TripShipmentAssociationResponse tripShipmentAssociationResponse =tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        lastmileHelper.validateTripStartedForMB(masterBagId);

        // Delivery Store Bag to Store.

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId),forwardTrackingNumbersList , String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");

        //Validate if shipment_order_map status is DELIVERED

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
        Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));

        //Vaidate new ML entry created for Store tenant

        lastmileHelper.validateMLShipmentDetails(trackingNo , MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER , originPremiseId , tenantId , LMS_CONSTANTS.CLIENTID);

        // Assign Orders to multipleDeliveryStaff

        TrackingNumbersList1.add(forwardTrackingNumbersList.get(0));
        DeliveryStaffOrderMap.put(disabledDeliveryStaffId,TrackingNumbersList1);
        DeliveryStaffOrderMap.put(storeDeliveryStaffId,TrackingNumbersList1);

        // Assign same orders for multiple SDA
        //Create a Store Trip and validate Trip

        String tracking_number = forwardTrackingNumbersList.get(0);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(DeliveryStaffOrderMap.get(disabledDeliveryStaffId), disabledDeliveryStaffId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.name()));
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        log.info("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " +storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + disabledDeliveryStaffId + " for the tracking number :  " + tracking_number + "\n\n********");


        String tracking_number1 = forwardTrackingNumbersList.get(0);
        TripResponse storeTrip1 = mlShipmentServiceV2Client_qa.createStoreTrip(DeliveryStaffOrderMap.get(storeDeliveryStaffId), storeDeliveryStaffId);
        Assert.assertNull(storeTrip1.getTrips(), "Able to create trip with same order for multiple SDA");

    }

    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void AssignOrderstoMultipleSDASameDayWithoutClosingPreviousTrip(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                              String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        List<Long> deliveryStaffIds = new ArrayList<>();
        Random r = new Random(System.currentTimeMillis());
        Long originPremiseId = 5l;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        String orderID = null;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        List<String> TrackingNumbersList1 = new ArrayList<>();
        List<String> TrackingNumbersList2 = new ArrayList<>();
        Map<Long, List<String>> DeliveryStaffOrderMap = new HashMap<>();
        int noOfDLorders = 2;

        // create Store and validate manager delivery staff

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        log.info("\n_____________Store Code : " +storeResponse.getStoreEntries().get(0).getCode()+ "Store ID :" +storeHlPId + "with Store Tenant ID : " +storeTenantId);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId,storeHlPId,storeTenantId,"MANAGER");
        deliveryStaffIds.add(storeDeliveryStaffId);

        // create multiple deliveryStaff for Store and make that deliveryStaff inactive

        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        lastmileHelper.createStoreDeliveryStaff(storeHlPId,storeTenantId,contactNumber,DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
        long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, storeTenantId, "STAFF");
        deliveryStaffIds.add(storeDeliveryStaffId1);
        long disabledDeliveryStaffId = storeDeliveryStaffId1;

        // create Store Bag and Validate

        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        log.info("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        log.info("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);


        // Create Orders till SH and Add it to StoreBag


        for (int i = 0; i < noOfDLorders; i++) {
            try {
                orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.ROLLING_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
                log.info("The orderId is : +" + orderID);
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Order creation in SH state failed : " + e.toString());
            }

            String packetId = omsServiceHelper.getPacketId(orderID);
            log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
            forwardTrackingNumbersList.add(trackingNo);
            trackingNoOrderIdMap.put(trackingNo, packetId);
            log.info("Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
            log.info("\n_____________Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

            ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
            try {

                String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
                if (response.toLowerCase().contains("error")) {
                    Assert.fail("Error while adding shipment to store bag");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
            log.info("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

            //Verify if the order is added in Bag
            OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
            Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
            log.info("\n______________The status of the " + i + " orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
            log.info("_____________The status of the " + i + " order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());

        }

        // Create Trip

        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();

        // Assign storeBag to Trip and validate

        TripShipmentAssociationResponse tripShipmentAssociationResponse =tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, noOfDLorders, code, originPremiseId);

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        lastmileHelper.validateTripStartedForMB(masterBagId);

        // Delivery Store Bag to Store.

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId),forwardTrackingNumbersList , String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");

        //Validate if shipment_order_map status is DELIVERED
        for (String trackingNo : forwardTrackingNumbersList) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));

            //Vaidate new ML entry created for Store tenant

            lastmileHelper.validateMLShipmentDetails(trackingNo , MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER , originPremiseId , tenantId , LMS_CONSTANTS.CLIENTID);

        }
        // Assign Orders to multipleDeliveryStaff

        TrackingNumbersList1.add(forwardTrackingNumbersList.get(0));
        TrackingNumbersList2.add(forwardTrackingNumbersList.get(1));

        //Create a Store Trip and validate Trip

        String tracking_number = forwardTrackingNumbersList.get(0);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(TrackingNumbersList1, disabledDeliveryStaffId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.name()));
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        log.info("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " +storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + disabledDeliveryStaffId + " for the tracking number :  " + TrackingNumbersList1 + "\n\n********");

        TripResponse storeTrip1 = mlShipmentServiceV2Client_qa.createStoreTrip(TrackingNumbersList2, disabledDeliveryStaffId);
        String storeTripNumber1 = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId1 = storeTrip1.getTrips().get(0).getId();
        Assert.assertTrue(storeTrip1.getTrips().get(0).getTripStatus().name().equals(com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.name()));
        Assert.assertTrue(storeTrip1.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        log.info("\n\n*****The Store trip id : " + storeTrip1.getTrips().get(0).getId() + " ,and its tripNumber is " +storeTripNumber1 + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + disabledDeliveryStaffId + " for the tracking number :  " + TrackingNumbersList2 + "\n\n********");


    }



    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")

    public void createMultipleSDAforStore(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                          String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws WebClientException ,Exception
    {
        int noOfDeliveryStaffs = 2;
        String orderID = null;
        int noOfDLPrepaidOrders = 3;
        int noOfDLcashOrders = 1;
        int noOfFDOrders = 1;
        int noOfPickupOrders = 2;
        int noOfExchangeOrders = 2;
        int noOfTNBOrders = 3;
        int noOfDLorders = noOfDLPrepaidOrders+noOfDLcashOrders+noOfFDOrders;
        Long originPremiseId = 5l;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String searchParams = "code.like:" + code;
        String destinationStoreCity = null;
        List<Long> deliveryStaffIds = new ArrayList<>();
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        Map<String, String> trackingNoOrderIdMap = new HashMap<>();
        List<String> TrackingNumbersList1 = new ArrayList<>();
        List<String> TrackingNumbersList2 = new ArrayList<>();
        List<String> TrackingNumbersList3 = new ArrayList<>();
        List<String> exchangeTrackingNumbersList = new ArrayList<>();
        Map<Long, List<String>> DeliveryStaffOrderMap = new HashMap<>();
        Random r = new Random(System.currentTimeMillis());



        // create Store and validate manager delivery staff

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        log.info("\n_____________Store Code : " +storeResponse.getStoreEntries().get(0).getCode()+ "Store ID :" +storeHlPId + "with Store Tenant ID : " +storeTenantId);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId,storeHlPId,storeTenantId,"MANAGER");
        deliveryStaffIds.add(storeDeliveryStaffId);

        // create multiple deliveryStaff for Store and Validate

        for (int o = 0 ; o< noOfDeliveryStaffs;o ++ ) {
            int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
            lastmileHelper.createStoreDeliveryStaff(storeHlPId, storeTenantId, contactNumber,DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
            long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getId();
            lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, storeTenantId, "STAFF");
            deliveryStaffIds.add(storeDeliveryStaffId1);
        }

        // create Store Bag and Validate

        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, PremisesType.DC, storeHlPId, PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        log.info("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        log.info("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);


        // Create Orders till SH and Add it to StoreBag


        for (int i = 0; i < noOfDLPrepaidOrders; i++) {
            try {
                orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.ROLLING_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
                log.info("The orderId is : +" + orderID);
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Order creation in SH state failed : " + e.toString());
            }

            String packetId = omsServiceHelper.getPacketId(orderID);
            log.info("Adding packetId " + packetId + " is MasterBag " + masterBagId);
            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
            forwardTrackingNumbersList.add(trackingNo);
            trackingNoOrderIdMap.put(trackingNo, packetId);
            log.info("Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
            log.info("\n_____________Adding forward trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);

            ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
            try {

                String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
                if (response.toLowerCase().contains("error")) {
                    Assert.fail("Error while adding shipment to store bag");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
            log.info("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);

            //Verify if the order is added in Bag
            OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
            Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
            log.info("\n______________The status of the " + i + " orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
            log.info("_____________The status of the " + i + " order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());

        }
//
//      //  Create Exchange order still SH and Add it to StoreBag
//
//        for (int n =0 ; n < noOfExchangeOrders ; n++){
//            orderID = lmsHelper.createMockOrder(EnumSCM.SH, LASTMILE_CONSTANTS.ROLLING_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
//            String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LASTMILE_CONSTANTS.ROLLING_PINCODE, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
//            String packetId = omsServiceHelper.getPacketId(exchangeOrder);
//            OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
//            String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
//            exchangeTrackingNumbersList.add(trackingNo);
//            trackingNoOrderIdMap.put(trackingNo, packetId);
//
//            log.info("Adding exchange trackingNumber " + trackingNo + " of the packet Id " + packetId + " whose orderId is " + orderID + " into the masterBag " + masterBagId + " which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + "whose storeId is " + storeHlPId);
//            log.info("\n_____________Adding exchange trackingNumber " + trackingNo + " of the packet Id " + packetId + " ,whose orderId is " + orderID + " into the masterBag " + masterBagId + " ,which is created for the store " + storeResponse.getStoreEntries().get(0).getCode() + " ,whose storeId is " + storeHlPId);
//
//            ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNo, null, null, null, null, null, null, false).build();
//            try {
//
//                String response = masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
//                if (response.toLowerCase().contains("error")) {
//                    Assert.fail("Error while adding shipment to store bag");
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            log.info("Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
//            log.info("\n_____________Scanned the tracking number " + trackingNo + " into the Bag " + masterBagId);
//
//            //Verify if the order is added in Bag
//            OrderShipmentAssociationStatus orderShipmentAssociationStatus = lastmileHelper.validateOrderAddedInStoreBag(masterBagId, trackingNo);
//            Assert.assertEquals(orderShipmentAssociationStatus.name(), OrderShipmentAssociationStatus.NEW.name(), "The status of the order added in the shipment is NOT : " + OrderShipmentAssociationStatus.NEW.name());
//            log.info("\n______________The status of the " + n + " orderId -  " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
//            log.info("_____________The status of the " + n + " order " + orderID + " , with trackingNumber " + trackingNo + " , added in the masterBag - " + masterBagId + " is  :" + OrderShipmentAssociationStatus.NEW.name());
//
//        }

        // Create Trip

        String deliveryStaffID = String.valueOf(lmsServiceHelper.getDeliveryStaffID(originPremiseId));
        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();

        // Assign storeBag to Trip and validate

        TripShipmentAssociationResponse tripShipmentAssociationResponse =tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, noOfDLPrepaidOrders, code, originPremiseId);

        //Start the trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.startTrip(tripId);
        lastmileHelper.validateTripStartedForMB(masterBagId);

        // Delivery Store Bag to Store.

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");

        //Validate if shipment_order_map status is DELIVERED

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (String trackingNo : forwardTrackingNumbersList) {
            OrderShipmentAssociationStatus shipmentOrderMapStatus = lastmileHelper.getShipmentOrderMapStatus(trackingNo, masterBagId);
            Assert.assertTrue(shipmentOrderMapStatus.name().equals(OrderShipmentAssociationStatus.DELIVERED.name()));
        }

        //Vaidate new ML entry created for Store tenant

        for (String track : forwardTrackingNumbersList) {
            lastmileHelper.validateMLShipmentDetails(track , MLDeliveryShipmentStatus.HANDED_TO_LAST_MILE_PARTNER , originPremiseId , tenantId , LMS_CONSTANTS.CLIENTID);
        }

        // Assign Orders to multipleDeliveryStaff

        TrackingNumbersList1.add(forwardTrackingNumbersList.get(0));
        TrackingNumbersList2.add(forwardTrackingNumbersList.get(1));
        TrackingNumbersList3.add(forwardTrackingNumbersList.get(2));

        DeliveryStaffOrderMap.put(deliveryStaffIds.get(0),TrackingNumbersList1);
        DeliveryStaffOrderMap.put(deliveryStaffIds.get(1),TrackingNumbersList2);
        DeliveryStaffOrderMap.put(deliveryStaffIds.get(2),TrackingNumbersList3);

        // delivery orders to customer

        for ( Long deliverystaff : deliveryStaffIds ) {

            //Create a Store Trip and validate Trip

            String tracking_number = DeliveryStaffOrderMap.get(deliverystaff).get(0);
            TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(DeliveryStaffOrderMap.get(deliverystaff), deliverystaff);
            String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
            Long storeTripId = storeTrip.getTrips().get(0).getId();
            Assert.assertTrue(storeTrip.getTrips().get(0).getTripStatus().name().equals(com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.name()));
            Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
            log.info("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " +storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + deliverystaff + " for the tracking number :  " + tracking_number + "\n\n********");

            //Validate the Store ML Shipment goes for OFD

            lastmileHelper.validateMLShipmentDetails(tracking_number , MLDeliveryShipmentStatus.OUT_FOR_DELIVERY , storeHlPId , storeTenantId , LMS_CONSTANTS.TENANTID);

            //Validate the OrderToShip (Platform ) status is OFD
            OrderResponse orderResponse = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(tracking_number),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.OUT_FOR_DELIVERY), "The platform  orderToship status is not : " + ShipmentStatus.OUT_FOR_DELIVERY.name());

            // Deliver Orders to customer by PrePaid

            TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getTripDetails(tracking_number, storeTenantId);
            Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
            tripClient_qa.deliverStoreOrders(storeTripId, tracking_number, tripOrderAssignmentId, storeTenantId, "PRE_PAID",LASTMILE_CONSTANTS.TENANT_ID);

            //Validate Store ML shipment is Delivered

            MLShipmentResponse mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_number, storeTenantId);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

            //Validate Myntra tenant ML Shipment is Delivered

            mlShipmentResponse2 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_number, LMS_CONSTANTS.TENANTID);
            Assert.assertTrue(mlShipmentResponse2.getMlShipmentEntries().get(0).getShipmentStatus().equals(MLDeliveryShipmentStatus.DELIVERED.name()));

            //Validate the ORderToShip (Platform ) status is Delivered

            OrderResponse orderResponse2 = lmsOrderDetailsClient_qa.getLMSOrderDetails(trackingNoOrderIdMap.get(tracking_number),LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            Assert.assertTrue(orderResponse2.getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.DELIVERED), "The platform  orderToship status is not : " + ShipmentStatus.DELIVERED.name());

        }
    }

    @Test (dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void getAllDeliveryStaffofStore_withStoreId(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                           String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        int noOfDeliveryStaffs = 2;
        List<Long> deliveryStaffIds = new ArrayList<>();
        Random r = new Random(System.currentTimeMillis());

        // create Store and validate manager delivery staff

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        log.info("\n_____________Store Code : " +storeResponse.getStoreEntries().get(0).getCode()+ "Store ID :" +storeHlPId + "with Store Tenant ID : " +storeTenantId);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId,storeHlPId,storeTenantId,"MANAGER");
        deliveryStaffIds.add(storeDeliveryStaffId);

        // create multiple deliveryStaff for Store and Validate

        for (int o = 0 ; o< noOfDeliveryStaffs;o ++ ) {
            int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
            lastmileHelper.createStoreDeliveryStaff(storeHlPId, storeTenantId, contactNumber,DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
            long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + contactNumber).getDeliveryStaffs().get(0).getId();
            lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, storeTenantId, "STAFF");
            deliveryStaffIds.add(storeDeliveryStaffId1);
        }

        //getAllDeliveryStaffofStore by searching with Store ID

//        DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId);
//        Assert.assertEquals(deliveryStaffResponse.getDeliveryStaffs().getStatus().getTotalCount(),noOfDeliveryStaffs+1 ,"Expected count of delivery Staff does not match with Actual ");
//        Assert.assertEquals(deliveryStaffResponse.getDeliveryStaffs().get(0).getId,deliveryStaffIds.get(0));
//        Assert.assertEquals(deliveryStaffResponse.getDeliveryStaffs().get(1).getId,deliveryStaffIds.get(1));
//        Assert.assertNotEquals(deliveryStaffResponse.getDeliveryStaffs().get(2).getId,deliveryStaffIds.get(2));
    }
    @Test ()
    public void getAllSDAbyDeliveryCenter() throws WebClientException {
        DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(5l);
        log.info(deliveryStaffResponse);


    }

}
