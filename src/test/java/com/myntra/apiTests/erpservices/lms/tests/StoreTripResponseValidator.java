package com.myntra.apiTests.erpservices.lms.tests;


import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.lastmile.client.response.StoreTripResponse;
import org.testng.Assert;

public class StoreTripResponseValidator {

    StoreTripResponse lmsStoreTripResponse;

    public StoreTripResponseValidator(StoreTripResponse lmsStoreTripResponse) {
        this.lmsStoreTripResponse = lmsStoreTripResponse;
        Assert.assertEquals(lmsStoreTripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        if (lmsStoreTripResponse.getStoreTripEntries() == null || lmsStoreTripResponse.getStoreTripEntries().isEmpty())
            Assert.fail("Could not find trip in LMS");
    }
}
