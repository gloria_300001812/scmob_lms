package com.myntra.apiTests.erpservices.lms.dp;

import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.lordoftherings.Toolbox;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;


/**
 * @author Shanmugam Yuvaraj
 * @since sep-2019
 */
public class OrderSearchDP {

    @DataProvider
    public static Object[][] SearchByOrderId(ITestContext testContext) {
        Object[] arr1 = { ShipmentStatus.DELIVERED.toString(), ShipmentType.DL.toString()};
        Object[] arr2 ={ ShipmentStatus.DELIVERED.toString(), ShipmentType.TRY_AND_BUY.toString()};

        Object[][] dataSet = new Object[][]{arr1, arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }
}
