package com.myntra.apiTests.erpservices.lms.tests;

import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_ReturnHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.test.commons.testbase.BaseTest;

import java.util.List;
import java.util.Map;

public class LMS_Notification extends BaseTest {

    private static org.slf4j.Logger log = LoggerFactory.getLogger(LMS_Notification.class);

    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LMS_ReturnHelper lmsReturnHelper= new LMS_ReturnHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();

    @Test(groups = { "Notification", "Smoke", "Regression",
            "P0s" }, priority = 5, dataProviderClass = LMSTestsDP.class, dataProvider = "notification", description = "ID: C377 , ", enabled = true)
    public void testnotification(String toSTatus, String courierCode, String status, String pincode,
                                                          String statusType,String returnstate) throws Exception {
        String orderid=lmsHelper.createMockOrder(toSTatus, pincode, courierCode, "36", EnumSCM.NORMAL, "cod", false, true);

         String releaseId = omsServiceHelper.getPacketId(orderid);

         if(statusType=="Forward")
         {
             lmsServiceHelper.validatenotification(releaseId,status);
         }
         if(statusType=="Reverse")
         {
             OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderid));
             OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
             ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
             Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
             Long returnId = returnResponse.getData().get(0).getId();
             lmsReturnHelper.processOpenBoxReturn(returnId.toString(), returnstate);
             lmsServiceHelper.validatenotification(returnId.toString(),status);
         }

    }



}
