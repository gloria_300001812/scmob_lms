package com.myntra.apiTests.erpservices.ims.Tests;

import com.myntra.apiTests.erpservices.ims.IMSServiceHelper;
import com.myntra.apiTests.erpservices.wms.WMSHelper;
import com.myntra.apiTests.erpservices.wms.dp.ItemStateDp_PPMP;
import com.myntra.client.wms.codes.utils.ItemStatus;
import com.myntra.client.wms.response.ItemResponse;
import com.myntra.ims.client.response.CoreInventoryCountResponse;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.RequestGenerator;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sneha.Agarwal on 02/04/18.
 */
public class ItemStateTransitionStates_MT_PPMP {

        private IMSServiceHelper imsServiceHelper = new IMSServiceHelper();

        Initialize init = new Initialize("/Data/configuration");
        private Logger log = Logger.getLogger(ItemStateTransitionStates_MT_PPMP.class);
        private WMSHelper WMSItemTransitionHelper1 = new WMSHelper();
        private  String ITEM_TRAN_SKU = "6251";
        private  String seller_Id = "48";
        private  String store_Id = "1";
        private  String wh_id = "1";
        private SoftAssert sft;
        private String owner_partner_id ="3974";
        private String owner_partner_id_myntra ="3974";
        private String owner_partner_id_jabong ="4602";
        private String owner_partner_id_flipkart="3854";
        private List<Integer> invlist = new ArrayList<Integer>();


        // return order
        // Add a RO for INWARD_REJECTS
        // Add a RO for CUSTOMER_RETURNS
        // Add a RO for OUTWARD_REJECTS
        // Add a RO for STOCK_CORRECTION
        // Add a item for INWARD_REJECTS
        // Add a item for CUSTOMER_RETURNS
        // Add a item for OUTWARD_REJECTS
        // Add a item for STOCK_CORRECTION
        // Sent for Approval
        // Approve the RO
        //

        // 2.accepted return to not_found
        @Test(groups = { "Regression" }, dataProvider = "acceptedReturnToNotFound", dataProviderClass = ItemStateDp_PPMP.class)
        public void acceptedReturnToNotFound(String item_id, ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft=new SoftAssert();


            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int inv_count_b = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_mi_cb++;
            b_p_cb--;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,"21");
            int wh_invcounta_SIM = inventory_data.get("ims_inventory_count");
            int atp_invcounta_SIM = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a_SIM = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a_SIM = inventory_data.get("atp_blocked_order_count");

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int inv_count_a = coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(inv_count_b, inv_count_a, "cic inventory count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp inventory boc count");
         //   sft.assertEquals(wh_invcounta_SIM, wh_invcount_boc_a_SIM, "wh_inventory IC should be equal to boc as it acts like seller inventory migration");
          //  sft.assertEquals(atp_invcounta_SIM, atp_invcount_boc_a_SIM, "ATP IC should be equal to boc as it acts like seller inventory migration");

            sft.assertAll();

        }

        // 3.accepted return to shipped
        @Test(groups = { "Regression" }, dataProvider = "acceptedReturnToShipped", dataProviderClass = ItemStateDp_PPMP.class)
        public void acceptedReturnToShipped(String item_id, ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft=new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int inv_count_b = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();


            b_p_cb--;
            inv_count_b--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int inv_count_a = coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(inv_count_a, inv_count_b, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "inventory boc count");
            sft.assertAll();

        }

        // 4.customer_return to stored
        @Test(groups = {
                "Regression" }, dataProvider = "customer_ReturnToStored", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public void customer_ReturnToStored(String item_id, ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft=new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int inv_count_b = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb--;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            Thread.sleep(1000);

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int inv_count_a = coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(inv_count_a, inv_count_b, "core_inventory inventory count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "inventory boc count");
            sft.assertAll();

        }

        // 5.customer_returned to not_found
        @Test(groups = {
                "Regression" }, dataProvider = "customer_ReturnToNotFound", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public void customer_ReturnToNotFound(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft=new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int inv_count_b = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb--;
            b_mi_cb++;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int inv_count_a = coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(inv_count_b, inv_count_a, "cic inventory count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "inventory boc count");
            sft.assertAll();
            //  sft.assertEquals(inv_count_a, inv_count_b,
            // "core_inventory inventory count");

        }

        // 6.detached to accepted_return
	/*	@Test(groups = {
				"Regression" }, dataProvider = "detached_to_accepted_return", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
		public  void detached_to_accepted_return(String item_id,  ItemStatus status, String quality, String response)
				throws InterruptedException, UnsupportedEncodingException, JAXBException {
			int wh_invcountb = getwh_inv_details(ITEM_TRAN_SKU, "1", seller_Id, "1");
			int atp_invcountb = getatp_inv_details(ITEM_TRAN_SKU, seller_Id, "1");
			int b_p_cb = imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU, "1").get(0);
			b_p_cb++;
			int b_mi_cb = imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU, "1").get(1);
			int b_ma_cb = imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU, "1").get(2);
			int inv_count_b = imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU, "1").get(3);
			inv_count_b++;
			ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
					quality,"0");

			log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
			int wh_invcounta = getwh_inv_details(ITEM_TRAN_SKU, "1", seller_Id, "1");
			int atp_invcounta = getatp_inv_details(ITEM_TRAN_SKU, seller_Id, "1");
			int b_p_ca = imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU, "1").get(0);
			int b_mi_ca = imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU, "1").get(1);
			int b_ma_ca = imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU, "1").get(2);
			int inv_count_a = imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU, "1").get(3);
			 sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
			 sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
			 sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
			 sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
			 sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
			 sft.assertEquals(inv_count_a, inv_count_b, "core_inventory inventory count");

		}
	*/
        /*
         * //7.detached to processing
         *
         * @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression",
         * "MiniRegression", "ExhaustiveRegression" }, dataProvider =
         * "detached_to_processing") public  void
         * detached_to_processing(String item_id ,String status ,String
         * quality,String response) { int
         * wh_invcountb=getwh_inv_details(ITEM_TRAN_SKU,"1",seller_Id,"1"); int
         * atp_invcountb=getatp_inv_details(ITEM_TRAN_SKU,seller_Id,"1"); int b_p_cb
         * =imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU,"1").get(0); int
         * b_mi_cb=imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU,"1").get(1); int
         * b_ma_cb=imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU,"1").get(2); RequestGenerator
         * WMSbulkupdateitem =
         * WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id,
         * status,quality); String getOrderResponse = WMSbulkupdateitem.respvalidate
         * .returnresponseasstring();log.info(
         * "\nPrinting WMSbulupdateitem API response :\n\n" + getOrderResponse +
         * "\n"); log.info("\nPrinting WMSbulupdateitem  API response :\n\n" +
         * getOrderResponse + "\n");
         *
         * AssertJUnit .assertEquals("WMSbulupdateitem  API is not working",
         * Integer.parseInt(response), WMSbulkupdateitem.response.getStatus()); int
         * wh_invcounta=getwh_inv_details(ITEM_TRAN_SKU,"1",seller_Id,"1"); int
         * atp_invcounta=getatp_inv_details(ITEM_TRAN_SKU,seller_Id,"1"); int b_p_ca
         * =imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU,"1").get(0); int
         * b_mi_ca=imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU,"1").get(1); int
         * b_ma_ca=imsServiceHelper.getcore_inv_count_details(ITEM_TRAN_SKU,"1").get(2);
         *  sft.assertEquals(b_p_ca, b_p_cb);  sft.assertEquals(b_ma_ca,
         * b_ma_cb);  sft.assertEquals(b_mi_ca, b_mi_cb);
         *  sft.assertEquals(atp_invcounta, atp_invcountb);
         *  sft.assertEquals(wh_invcounta, wh_invcountb);
         *
         *
         * }
         */
        // 8.detached to stored
        @Test(groups = {
                "Regression" }, dataProvider = "detached_to_stored", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public void detached_to_stored(String item_id, ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();


            core_inv_countb++;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta = coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "inventory boc count");
            sft.assertAll();

        }

        // 9.found to stored
        @Test(groups = {
                "Regression" }, dataProvider = "found_to_stored", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void found_to_stored(String item_id, ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException, UnsupportedEncodingException, JAXBException {
            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            b_mi_cb--;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta = coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "inventory boc count");
            sft.assertAll();

        }


        // 11.issued to shipped

        @Test(groups = { "Regression" }, dataProvider = "issued_to_shipped",
                dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true) public
        void issued_to_shipped(String item_id,  ItemStatus status, String quality,
                               String response) throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft = new SoftAssert();

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            core_inv_countb--;
            RequestGenerator imsSyncInv = imsServiceHelper
                    .invokeIMSSyncinventoryAPI("1", "1", seller_Id,
                            "ON_HAND", ITEM_TRAN_SKU, "MSMRFHSR00015", "SHIPPED",
                            "1", owner_partner_id);
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            wh_invcounta--;
            atp_invcounta--;
            wh_invcount_boc_a--;
            atp_invcount_boc_a--;

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta = coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb,
                    "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "inventory boc count");
            sft.assertAll();

        }


    @Test(groups = { "Regression" }) public
    void order_reassignment() throws InterruptedException, UnsupportedEncodingException, JAXBException {
        sft = new SoftAssert();
        HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
        int wh_invcountb = inventory_data.get("ims_inventory_count");
        int atp_invcountb = inventory_data.get("atp_inventory_count");
        int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
        int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

        CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,"Q1", owner_partner_id);

        int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
        int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
        int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
        int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

        RequestGenerator imsSyncInv = imsServiceHelper
                .invokeIMSSyncinventoryAPI("1", "1", "48",
                        "ON_HAND", ITEM_TRAN_SKU, "MSMRFHSR00015", "ORDER_REASSIGNMENT",
                        "1", owner_partner_id);
        inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
        int wh_invcounta = inventory_data.get("ims_inventory_count");
        int atp_invcounta = inventory_data.get("atp_inventory_count");
        int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
        int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
        CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,"Q1", owner_partner_id);

        coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
        int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
        int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
        int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
        int core_inv_counta = coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

        sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
        sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
        sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
        sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
        sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
        sft.assertEquals(atp_invcount_boc_a, atp_invcount_boc_b, "atp boc count");
        sft.assertEquals(wh_invcount_boc_a, wh_invcount_boc_b, "wh_inventory boc count");
        sft.assertEquals(core_inv_counta, core_inv_countb,
                "core_inventory inventory count");
        sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
        sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "inventory boc count");
        sft.assertAll();

    }


    // 12.issued to not found
        @Test(groups = {
                "Regression" }, dataProvider = "issued_to_notfound", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public void issued_to_notfound(String item_id, ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft = new SoftAssert();

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_mi_cb++;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "inventory boc count");
            sft.assertAll();

        }

        // 13.issued to rfo
        @Test(groups = {
                "Regression" }, dataProvider = "issued_to_rfo", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void issued_to_rfo(String item_id, ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft = new SoftAssert();

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb++;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();

            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "inventory boc count");
            sft.assertAll();

        }

        // 14.not_found to shrinkage
        @Test(groups = {
                "Regression" }, dataProvider = "not_found_to_shrinkage", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public void not_found_to_shrinkage(String item_id, ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            b_mi_cb--;

            core_inv_countb--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "inventory boc count");
            sft.assertAll();

        }

        // 15.not received to stored
        @Test(groups = {
                "Regression" }, dataProvider = "not_receivedto_stored", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void not_receivedto_stored(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft = new SoftAssert();

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            core_inv_countb++;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "inventory boc count");
            sft.assertAll();

        }

        // 16.processing to not found
        @Test(groups = {
                "Regression" }, dataProvider = "processingToNotfound", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public void processingToNotfound(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft= new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb--;
            b_mi_cb++;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 17.processing to stored
        @Test(groups = {
                "Regression" }, dataProvider = "processing_to_stored", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void processing_to_stored(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft= new SoftAssert();

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb--;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();
        }

        // 18.rfo to not found
        @Test(groups = {
                "Regression" }, dataProvider = "rfo_to_not_found", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void rfo_to_not_found(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");


            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            b_p_cb--;
            b_mi_cb++;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // processing to issued_for_ops
        @Test(groups = {
                "Regression" }, dataProvider = "processing_to_issued_for_ops", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void processing_to_issued_for_ops(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 21.rfo to stored
        @Test(groups = {
                "Regression" }, dataProvider = "rfo_to_stored", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
                public  void rfo_to_stored(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb--;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 20.shrinkage to found
        @Test(groups = {
                "Regression" }, dataProvider = "shrinkage_to_found", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void shrinkage_to_found(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");


            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            core_inv_countb++;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 21.stored to notfound
        @Test(groups = {
                "Regression" }, dataProvider = "stored_to_notfound", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void stored_to_notfound(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_mi_cb++;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 22.stored to return
        @Test(groups = {
                "Regression" }, dataProvider = "stored_to_return", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void stored_to_return(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            core_inv_countb--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            Thread.sleep(2000);

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 23.stored to processing
        @Test(groups = {
                "Regression" }, dataProvider = "stored_to_processing", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void stored_to_processing(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb++;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            Thread.sleep(5000);

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 24.stored to transit
        @Test(groups = {
                "Regression" }, dataProvider = "stored_to_transit", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void stored_to_transit(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft= new SoftAssert();

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb++;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 25.transit to detached
        @Test(groups = {
                "Regression" }, dataProvider = "transit_to_detached", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void transit_to_detached(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft= new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb--;
            core_inv_countb--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();


        }

        // 26.transit to not found
        @Test(groups = {
                "Regression" }, dataProvider = "transit_to_notfound", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void transit_to_notfound(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft=new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb--;
            b_mi_cb++;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 27. stored q1 to q2
        @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression",
                "ExhaustiveRegression" }, dataProvider = "storedq1toq2", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void storedq1toq2(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft=new SoftAssert();
            int b_p_c_Q2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);
            int core_inv_countQ2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            core_inv_countQ2_b++;

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            core_inv_countb--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");
            Thread.sleep(10000);

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,"Q1", owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            int b_p_c_Q2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);
            int core_inv_countQ2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(b_p_c_Q2_a, b_p_c_Q2_b, "blocked processing count Q2");
            sft.assertEquals(core_inv_countQ2_a, core_inv_countQ2_b, "core_inventory inventory count Q2");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 28.rfo to store q1 to q2
        @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression",
                "ExhaustiveRegression" }, dataProvider = "rfo_to_storeq1toq2", dataProviderClass = ItemStateDp_PPMP.class)
        public  void rfo_to_storeq1toq2(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();

            invlist=imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id);
            int b_p_c_Q2_b = invlist.get(0);
            int core_inv_countQ2_b = invlist.get(3);
            core_inv_countQ2_b++;

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb--;
            core_inv_countb--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");
            Thread.sleep(10000);

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,"Q1", owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            invlist=imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id);
            int b_p_c_Q2_a = invlist.get(0);
            int core_inv_countQ2_a = invlist.get(3);
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(b_p_c_Q2_a, b_p_c_Q2_b, "blocked processing count Q2");
            sft.assertEquals(core_inv_countQ2_a, core_inv_countQ2_b, "core_inventory inventory count Q2");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 29.rfo to rfo q1 to q2
        @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression",
                "ExhaustiveRegression" }, dataProvider = "rfo_to_rfo_q1_to_q2", dataProviderClass = ItemStateDp_PPMP.class)
        public  void rfo_to_rfo_q1_to_q2(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            int b_p_c_Q2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);
            b_p_c_Q2_b++;
            int core_inv_countQ2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            core_inv_countQ2_b++;

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb--;
            core_inv_countb--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");
            Thread.sleep(10000);
            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,"Q1", owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            int b_p_c_Q2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);
            int core_inv_countQ2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(b_p_c_Q2_a, b_p_c_Q2_b, "blocked processing count Q2");
            sft.assertEquals(core_inv_countQ2_a, core_inv_countQ2_b, "core_inventory inventory count Q2");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // 30.found to stores q1 to q2
        @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression",
                "ExhaustiveRegression" }, dataProvider = "found_to_stores_q1_to_q2", dataProviderClass = ItemStateDp_PPMP.class)
        public  void found_to_stores_q1_to_q2(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            int b_mi_c_Q2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(1);
            int core_inv_countQ2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            core_inv_countQ2_b++;

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_mi_cb--;
            int b_p_c_Q2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);
            core_inv_countb--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,"Q1", owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            int b_p_c_Q2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);
            int b_mi_c_Q2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(1);
            int core_inv_countQ2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(b_p_c_Q2_a, b_p_c_Q2_b, "blocked processing count Q2");
            sft.assertEquals(core_inv_countQ2_a, core_inv_countQ2_b, "core_inventory inventory count Q2");
            sft.assertEquals(b_mi_c_Q2_a, b_mi_c_Q2_b, "blocked missed count Q2");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // stored_to_deleted
        @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression",
                "ExhaustiveRegression" }, dataProvider = "stored_to_deleted", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void stored_to_deleted(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {
            sft = new SoftAssert();


            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            core_inv_countb--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // found_to_deleted
        @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression",
                "ExhaustiveRegression" }, dataProvider = "found_to_deleted", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void found_to_deleted(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_mi_cb--;
            core_inv_countb--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // not_found_to_deleted
        @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression",
                "ExhaustiveRegression" }, dataProvider = "not_found_to_deleted", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void not_found_to_deleted(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            b_mi_cb--;
            core_inv_countb--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // processing_to_deleted
        @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression",
                "ExhaustiveRegression" }, dataProvider = "processing_to_deleted", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void processing_to_deleted(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb--;
            core_inv_countb--;

            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();

        }

        // storedq2toq1
        @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression",
                "ExhaustiveRegression" }, dataProvider = "storedq2toq1", dataProviderClass = ItemStateDp_PPMP.class)
        public  void storedq2toq1(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft= new SoftAssert();
            int b_mi_c_Q2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(1);
            int core_inv_countQ2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            core_inv_countQ2_b--;

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            int b_p_c_Q2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);
            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            core_inv_countb++;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,"Q1", owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            int b_p_c_Q2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);
            int b_mi_c_Q2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(1);
            int core_inv_countQ2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(b_p_c_Q2_a, b_p_c_Q2_b, "blocked processing count Q2");
            sft.assertEquals(core_inv_countQ2_a, core_inv_countQ2_b, "core_inventory inventory count Q2");
            sft.assertEquals(b_mi_c_Q2_a, b_mi_c_Q2_b, "blocked missed count Q2");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();
        }

        // rfo_to_rfo_q2_to_q1
        @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression",
                "ExhaustiveRegression" }, dataProvider = "rfo_to_rfo_q2_to_q1", dataProviderClass = ItemStateDp_PPMP.class)
        public  void rfo_to_rfo_q2_to_q1(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            int b_p_c_Q2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);
            b_p_c_Q2_b--;
            int core_inv_countQ2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            core_inv_countQ2_b--;
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb++;
            core_inv_countb++;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,"Q1", owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            int b_p_c_Q2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);
            int core_inv_countQ2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(b_p_c_Q2_a, b_p_c_Q2_b, "blocked processing count Q2");
            sft.assertEquals(core_inv_countQ2_a, core_inv_countQ2_b, "core_inventory inventory count Q2");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();
            //  sft.assertEquals(b_mi_c_Q2_a, b_mi_c_Q2_b,"blocked missed count
            // Q2");
        }

        // found_to_stored_q2_to_q1
        @Test(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression",
                "ExhaustiveRegression" }, dataProvider = "found_to_stored_q2_to_q1", dataProviderClass = ItemStateDp_PPMP.class)
        public  void found_to_stored_q2_to_q1(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            int b_mi_c_Q2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(1);
            b_mi_c_Q2_b--;
            int b_p_c_Q2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);

            int core_inv_countQ2_b = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            core_inv_countQ2_b--;


            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            core_inv_countb++;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            log.info(wh_invcounta + "before" + wh_invcountb);
            log.info(atp_invcounta + "before" + atp_invcountb);
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,"Q1", owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            int b_p_c_Q2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(0);
            log.info(b_p_c_Q2_a + "before" + b_p_c_Q2_b);
            int core_inv_countQ2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(3);
            log.info(b_p_c_Q2_a + "before" + b_p_c_Q2_b);
            int b_mi_c_Q2_a = imsServiceHelper.getcore_inv_count_detailsQ2(ITEM_TRAN_SKU, "1",owner_partner_id).get(1);
            //log.info(core_inv_countQ2_a+"before"+b_mi_c_Q2_a);
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(b_p_c_Q2_a, b_p_c_Q2_b, "blocked processing count Q2");
            sft.assertEquals(core_inv_countQ2_a, core_inv_countQ2_b, "core_inventory inventory count Q2");
            sft.assertEquals(b_mi_c_Q2_a, b_mi_c_Q2_b, "blocked missed count Q2");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();
        }
        //issued_for_ops to transit
        @Test(groups = {
                "Regression" }, dataProvider = "issued_for_ops_to_transit", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void issued_for_ops_to_transit(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            b_p_cb++;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();
        }
        //issued_for_ops to liquidated
        @Test(groups = {
                "Regression" }, dataProvider = "issued_for_ops_to_liquidated", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void issued_for_ops_to_liquidated(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();

            core_inv_countb--;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();
        }
        //issued_for_ops to returned
        @Test(groups = {
                "Regression" }, dataProvider = "issued_for_ops_to_returned", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void issued_for_ops_to_returned(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");
            core_inv_countb--;

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();
        }


        @Test(groups = {"Regression" }, dataProvider = "issued_for_ops_to_notFound", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void issued_for_ops_to_notFound(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();
            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            b_p_cb--;
            b_mi_cb++;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();
        }

        //issued_for_ops_to_returnFromOps
        @Test(groups = {
                "Regression" }, dataProvider = "issued_for_ops_to_returnFromOps", dataProviderClass = ItemStateDp_PPMP.class, alwaysRun = true)
        public  void issued_for_ops_to_returnFromOps(String item_id,  ItemStatus status, String quality, String response)
                throws InterruptedException, UnsupportedEncodingException, JAXBException {

            sft = new SoftAssert();

            HashMap<String,Integer> inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcountb = inventory_data.get("ims_inventory_count");
            int atp_invcountb = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_b = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_b = inventory_data.get("atp_blocked_order_count");

            CoreInventoryCountResponse coreInventoryCount_b=imsServiceHelper
                    .searchCoreInvOwnerId("1", "1", "1", ITEM_TRAN_SKU,quality, owner_partner_id);

            int b_p_cb = coreInventoryCount_b.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_cb = coreInventoryCount_b.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_cb = coreInventoryCount_b.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_countb = coreInventoryCount_b.getData().get(0).getInventoryCount().intValue();
            b_p_cb++;
            ItemResponse WMSbulkupdateitem = WMSItemTransitionHelper1.invokeBulkItemTransitionAPI(item_id, status,
                    quality,"0");

            log.info("\nPrinting WMSbulupdateitem  API response :\n\n" + WMSbulkupdateitem + "\n");
            inventory_data=imsServiceHelper.imsandatpdetails(ITEM_TRAN_SKU, store_Id,wh_id,seller_Id);
            int wh_invcounta = inventory_data.get("ims_inventory_count");
            int atp_invcounta = inventory_data.get("atp_inventory_count");
            int wh_invcount_boc_a = inventory_data.get("ims_blocked_order_count");
            int atp_invcount_boc_a = inventory_data.get("atp_blocked_order_count");
            CoreInventoryCountResponse coreInventoryCount_a=imsServiceHelper
                    .searchCoreInvOwnerId("10", "DESC", "1", ITEM_TRAN_SKU,quality, owner_partner_id);
            coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_p_ca = coreInventoryCount_a.getData().get(0).getBlockedProcessingCount().intValue();
            int b_mi_ca = coreInventoryCount_a.getData().get(0).getBlockedMissedCount().intValue();
            int b_ma_ca = coreInventoryCount_a.getData().get(0).getBlockedManualCount().intValue();
            int core_inv_counta=coreInventoryCount_a.getData().get(0).getInventoryCount().intValue();

            sft.assertEquals(b_ma_ca, b_ma_cb, "blocked manual count");
            sft.assertEquals(b_mi_ca, b_mi_cb, "blocked missed count");
            sft.assertEquals(atp_invcounta, atp_invcountb, "atp inventory count");
            sft.assertEquals(wh_invcounta, wh_invcountb, "wh_inventory inventory count");
            sft.assertEquals(b_p_ca, b_p_cb, "blocked processing count");
            sft.assertEquals(core_inv_counta, core_inv_countb, "core_inventory inventory count");
            sft.assertEquals(wh_invcount_boc_b, wh_invcount_boc_a, "wh_inventory boc count");
            sft.assertEquals(atp_invcount_boc_b, atp_invcount_boc_a, "atp boc count");
            sft.assertAll();
        }



        @BeforeClass(groups = { "Smoke", "Sanity", "ProdSanity", "Regression", "MiniRegression", "ExhaustiveRegression" })
        public  void insertitems() {
            log.info("Entered insertion loop");
            String queryToDeleteData = "delete from item where id in (1000221,1000222,1000223,1000224,1000225,1000226,1000227,1000228,1000229,1000230,1000231,1000232,1000258,1000234,1000235,1000236,1000237,1000238,1000239,1000240,1000241,1000242,1000243,1000244,1000245,1000246,1000247,1000248,1000249,1000250,1000251,1000252,1000253,1000255,1000256,1000257,1000254,1000259,1000260,1000261,1000262,1000263,1000264,1000265,9100000086116,1000270,1000271,1000272,1000273,1000274,1000275)";
            String queryToDeleteItemInfo="delete from item_info where item_id in (1000221,1000222,1000223,1000224,1000225,1000226,1000227,1000228,1000229,1000230,1000231,1000232,1000233,1000258,1000234,1000235,1000236,1000237,1000238,1000239,1000240,1000241,1000242,1000243,1000244,1000245,1000246,1000247,1000248,1000249,1000250,1000251,1000252,1000253,1000255,1000256,1000257,1000254,1000259,1000258,1000260,1000261,1000262,1000263,1000264,1000265,9100000086116,1000270,1000271,1000272,1000273,1000274,1000275)";
            String queryToClearCIC="delete from core_inventory_counts where sku_id="+ITEM_TRAN_SKU;
            String queryToClearATP="delete from inventory where sku_id="+ITEM_TRAN_SKU;
            String queryToClearIMS="delete from wh_inventory where sku_id="+ITEM_TRAN_SKU;
            String queryToDeleteSTN="delete from core_stns where barcode='STNJEBA301114-00'";
            String queryToDeleteSTN_Items="delete from core_stn_items where item_id=1000273";
            String queryToDeleteSTN_skus="delete from core_stn_skus where sku_id="+ITEM_TRAN_SKU;
            DBUtilities.exUpdateQuery(queryToDeleteData, "myntra_wms");
            DBUtilities.exUpdateQuery(queryToDeleteItemInfo, "myntra_wms");
            DBUtilities.exUpdateQuery(queryToClearCIC, "myntra_ims");
            DBUtilities.exUpdateQuery(queryToClearIMS, "myntra_ims");
            DBUtilities.exUpdateQuery(queryToClearATP, "myntra_atp");
            DBUtilities.exUpdateQuery(queryToDeleteSTN, "myntra_wms");
            DBUtilities.exUpdateQuery(queryToDeleteSTN_Items, "myntra_wms");
            DBUtilities.exUpdateQuery(queryToDeleteSTN_skus, "myntra_wms");

            String insertItemInfo="INSERT INTO `item_info` ( `item_id`, `item_action_status`, `task_id`, `order_barcode`, `created_on`, `created_by`, `last_modified_on`, `version`, `order_id`, `invoice_sku_id`, `agreement_type`, `source_owner_id`,`buyer_id`, `manufacturing_date`)"
                    +"VALUES(1000221, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000222, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000223, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000224, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000225, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000226, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000227, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000228, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    // +"(1000129, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', 3974, NULL),"
                    +"(1000230, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    // +"(1000131, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', 3974, NULL),"
                    +"(1000232, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000233, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000234, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000235, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000236, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000237, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000238, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000239, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000240, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000241, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000242, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000243, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000244, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000245, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000246, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000247, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000248, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000249, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000250, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000251, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000252, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000253, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000254, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000255, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000256, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000257, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000258, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000260, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000262, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000263, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000264, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000270, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000271, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000272, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000273, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000274, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000275, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT', "+owner_partner_id+"," + owner_partner_id+", NULL),"

                    +"(1000265, 'NEW', NULL, NULL, '2017-08-10 17:51:54', 'erpMessageQueue', '2017-08-10 17:51:54', 0, NULL, NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL);";
            String Query1 = "insert into item (id,barcode,sku_id,quality,item_status,warehouse_id,enabled,po_id,po_barcode,po_sku_id,lot_id,lot_barcode,comments,bin_id)"
                    + "values(1000221,'1000221',"+ITEM_TRAN_SKU+",'Q1','ACCEPTED_RETURNS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',128),"
                    + "(1000222,'1000222',"+ITEM_TRAN_SKU+",'Q1','ACCEPTED_RETURNS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',128),"
                    + "(1000223,'1000223',"+ITEM_TRAN_SKU+",'Q1','CUSTOMER_RETURNED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',128),"
                    + "(1000224,'1000224',"+ITEM_TRAN_SKU+",'Q1','CUSTOMER_RETURNED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',128),"
                    + "(1000225,'1000225',"+ITEM_TRAN_SKU+",'Q1','DETACHED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',50614),"
                    + "(1000226,'1000226',"+ITEM_TRAN_SKU+",'Q1','DETACHED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',50614),"
                    + "(1000250,'1000250',"+ITEM_TRAN_SKU+",'Q1','DETACHED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',50614),"
                    + "(1000227,'1000227',"+ITEM_TRAN_SKU+",'Q1','FOUND',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',186),"
                    + "(1000228,'1000228',"+ITEM_TRAN_SKU+",'Q1','FOUND',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',186),"
                    //  + "(1000129,'1000129',"+ITEM_TRAN_SKU+",'Q1','ISSUED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48849),"
                    + "(1000230,'1000230',"+ITEM_TRAN_SKU+",'Q1','ISSUED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48849),"
                    //+ "(1000131,'1000131',"+ITEM_TRAN_SKU+",'Q1','ISSUED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48849),"
                    + "(1000232,'1000232',"+ITEM_TRAN_SKU+",'Q1','NOT_FOUND',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',186),"
                    + "(1000258,'1000258',"+ITEM_TRAN_SKU+",'Q1','NOT_RECEIVED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',186),"
                    + "(1000275,'1000275',12070042,'Q1','NOT_RECEIVED',28,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',186),"
                    + "(1000234,'1000234',"+ITEM_TRAN_SKU+",'Q1','PROCESSING',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48849),"
                    + "(1000235,'1000235',"+ITEM_TRAN_SKU+",'Q1','PROCESSING',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48849),"
                    + "(1000236,'1000236',"+ITEM_TRAN_SKU+",'Q1','RETURN_FROM_OPS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000237,'1000237',"+ITEM_TRAN_SKU+",'Q1','RETURN_FROM_OPS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000238,'1000238',"+ITEM_TRAN_SKU+",'Q1','SHRINKAGE',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',221),"
                    + "(1000239,'1000239',"+ITEM_TRAN_SKU+",'Q1','STORED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',403),"
                    + "(1000240,'1000240',"+ITEM_TRAN_SKU+",'Q1','STORED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',403),"
                    + "(1000241,'1000241',"+ITEM_TRAN_SKU+",'Q1','STORED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',403),"
                    + "(1000242,'1000242',"+ITEM_TRAN_SKU+",'Q1','STORED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',403),"
                    + "(1000243,'1000243',"+ITEM_TRAN_SKU+",'Q1','TRANSIT',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',50614),"
                    + "(1000244,'1000244',"+ITEM_TRAN_SKU+",'Q1','TRANSIT',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',50614),"
                    + "(1000249,'1000249',"+ITEM_TRAN_SKU+",'Q1','STORED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',186),"
                    + "(1000245,'1000245',"+ITEM_TRAN_SKU+",'Q1','RETURN_FROM_OPS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',403),"
                    + "(1000246,'1000246',"+ITEM_TRAN_SKU+",'Q1','RETURN_FROM_OPS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000247,'1000247',"+ITEM_TRAN_SKU+",'Q1','FOUND',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000248,'1000248',"+ITEM_TRAN_SKU+",'Q1','STORED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',186),"
                    + "(1000251,'1000251',"+ITEM_TRAN_SKU+",'Q1','STORED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000252,'1000252',"+ITEM_TRAN_SKU+",'Q1','FOUND',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',186),"
                    + "(1000253,'1000253',"+ITEM_TRAN_SKU+",'Q1','NOT_FOUND',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',186),"
                    + "(1000254,'1000254',"+ITEM_TRAN_SKU+",'Q1','PROCESSING',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48849),"
                    + "(1000255,'1000255',"+ITEM_TRAN_SKU+",'Q2','STORED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000260,'1000260',"+ITEM_TRAN_SKU+",'Q1','STORED',36,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000262,'1000262',"+ITEM_TRAN_SKU+",'Q1','STORED',36,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000263,'1000263',"+ITEM_TRAN_SKU+",'Q1','STORED',36,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000265,'1000265',"+ITEM_TRAN_SKU+",'Q1','STORED',36,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000256,'1000256',"+ITEM_TRAN_SKU+",'Q2','FOUND',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',186),"
                    + "(1000257,'1000257',"+ITEM_TRAN_SKU+",'Q2','RETURN_FROM_OPS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000270,'1000270',"+ITEM_TRAN_SKU+",'Q1','ISSUED_FOR_OPS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000271,'1000271',"+ITEM_TRAN_SKU+",'Q1','ISSUED_FOR_OPS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000272,'1000272',"+ITEM_TRAN_SKU+",'Q1','ISSUED_FOR_OPS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000273,'1000273',"+ITEM_TRAN_SKU+",'Q1','ISSUED_FOR_OPS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"
                    + "(1000274,'1000274',"+ITEM_TRAN_SKU+",'Q1','ISSUED_FOR_OPS',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48574),"

                    + "(1000259,'1000259',"+ITEM_TRAN_SKU+",'Q1','PROCESSING',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48849)";

            DBUtilities.exUpdateQuery(Query1, "myntra_wms");
            DBUtilities.exUpdateQuery(insertItemInfo, "myntra_wms");

            String QuerywithOrder = "insert into item (id,barcode,sku_id,quality,item_status,warehouse_id,enabled,po_id,po_barcode,po_sku_id,lot_id,lot_barcode,comments,bin_id,order_id)"
                    + "values(1000231,'1000231',"+ITEM_TRAN_SKU+",'Q1','ISSUED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-01','Automatio item',48849,'2147613971256'),"
                    + "(1000229,'1000229',"+ITEM_TRAN_SKU+",'Q1','ISSUED',1,1,313,'OPST050911-09',1,1,'LOTVHGA-02','Automatio item',48849,'2147613971263')";
            DBUtilities.exUpdateQuery(QuerywithOrder, "myntra_wms");

            String insertItemInfoWithOrder="INSERT INTO `item_info` ( `item_id`, `item_action_status`, `task_id`, `order_barcode`, `created_on`, `created_by`, `last_modified_on`, `version`, `order_id`, `invoice_sku_id`, `agreement_type`, `source_owner_id`, `buyer_id`, `manufacturing_date`)"
                    +"VALUES(1000231, 'NEW', NULL, '2147613971256','2027-08-10 17:51:54', 'erpMessageQueue', '2027-08-10 17:51:54', 0, '2147613971256', NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL),"
                    +"(1000229, 'NEW', NULL, '2147613971263', '2027-08-10 17:51:54', 'erpMessageQueue', '2027-08-10 17:51:54', 0, '2147613971263', NULL, 'OUTRIGHT',"+owner_partner_id+"," + owner_partner_id+", NULL)";
            DBUtilities.exUpdateQuery(insertItemInfoWithOrder, "myntra_wms");


            String query_CIC_Q1="INSERT INTO `core_inventory_counts` (`id`, `warehouse_id`, `warehouse_name`, `quality`, `owner_partner_id`, `sku_id`, `sku_code`, `inv_count`, `blocked_manually_count`, `blocked_missed_count`, `created_by`, `created_on`, `last_modified_on`, `version`, `blocked_processing_count`)\n" +
                    "VALUES (null, 1, 'Bangalore', 'Q1',"+ owner_partner_id_jabong +","+ ITEM_TRAN_SKU +", 'MSMRFHSR00025', 30, 0, 6, 'sneha', '2028-02-26 19:55:46', '2028-02-26 19:55:46', 11, 13),"+
                    "(null, 1, 'Bangalore', 'Q1',"+ owner_partner_id_flipkart +","+ ITEM_TRAN_SKU +", 'MSMRFHSR00025', 30, 0, 6, 'sneha', '2028-02-26 19:55:46', '2028-02-26 19:55:46', 11, 13),"+
                    "(null, 1, 'Bangalore', 'Q1',"+ owner_partner_id_myntra +","+ ITEM_TRAN_SKU +", 'MSMRFHSR00025', 30, 0, 6, 'sneha', '2028-02-26 19:55:46', '2028-02-26 19:55:46', 11, 13);";



            String Query2 = "update core_inventory_counts set inv_count =30, blocked_manually_count =0,blocked_missed_count=6,blocked_processing_count=13 where sku_id = "+ITEM_TRAN_SKU+" and warehouse_id =1 and quality='Q1' and owner_partner_id="+owner_partner_id;
            DBUtilities.exUpdateQuery(query_CIC_Q1, "myntra_ims");
            log.info("core_inv_count updated");


            String query_inv_IMS="INSERT INTO `wh_inventory` (`id`, `warehouse_id`, `warehouse_name`, `store_id`, `supply_type`, `sku_id`, `sku_code`, `inventory_count`, `blocked_order_count`, `created_by`, `created_on`, `last_modified_on`, `last_synced_on`, `pushed_order_count`, `version`, `vendor_id`, `seller_id`, `proc_sla`, `override_auto_realloc`, `store_partner_id`, `seller_partner_id`)\n" +
                    "VALUES (null, 1, NULL, 1, 'ON_HAND',"+ITEM_TRAN_SKU+", 'MSMRFHSR00025', 11, 10, 'sneha', '2025-05-05 14:54:36', '2028-04-04 18:54:08', '2025-05-05 14:54:36', 0, 0, 0, 21, 0, 1, NULL, 4216),\n" +
                    "(null, 1, NULL, 1, 'ON_HAND',"+ITEM_TRAN_SKU+", 'MSMRFHSR00025', 11, 10, 'sneha', '2025-05-05 14:54:36', '2028-04-04 18:54:08', '2025-05-05 14:54:36', 0, 0, 0, "+seller_Id+", 0, 1, NULL, 4024),\n" +
                    "(null, 1, NULL, 5, 'ON_HAND',"+ITEM_TRAN_SKU+", 'MSMRFHSR00025', 11, 10, 'sneha', '2025-05-05 14:54:36', '2028-04-04 18:54:08', '2025-05-05 14:54:36', 0, 0, 0, "+seller_Id+", 0, 1, NULL, 4024);";
            DBUtilities.exUpdateQuery(query_inv_IMS, "myntra_ims");


            log.info("wh_inv updated");


            String query_ATP="INSERT INTO `inventory` (`id`, `store_id`, `seller_id`, `seller_partner_id`, `sku_id`, `sku_code`, `inventory_count`, `blocked_order_count`, `supply_type`, `lead_time`, `enabled`, `last_synced_on`, `available_in_warehouses`, `vendor_id`, `created_by`, `created_on`, `last_modified_on`, `version`, `blocked_future_count`)\n" +
                    "VALUES (null, 1, 21, NULL,"+ITEM_TRAN_SKU+ ", 'MSMRFHSR00025', 11, 10, 'ON_HAND', 0, 1, NULL, '', NULL, 'erpadmin', '2028-04-24 13:04:43', '2028-04-25 20:30:53', 11, 0)," +
                    "(null, 1, "+seller_Id+", NULL,"+ITEM_TRAN_SKU+ ", 'MSMRFHSR00025', 11, 10, 'ON_HAND', 0, 1, NULL, '', NULL, 'erpadmin', '2028-04-24 13:04:43', '2028-04-25 20:30:53', 11, 0),"+
                    "(null, 2, "+seller_Id+", NULL,"+ITEM_TRAN_SKU+ ", 'MSMRFHSR00025', 11, 10, 'ON_HAND', 0, 1, NULL, '', NULL, 'erpadmin', '2028-04-24 13:04:43', '2028-04-25 20:30:53', 11, 0)," +
                    "(null, 5, "+seller_Id+", NULL,"+ITEM_TRAN_SKU+ ", 'MSMRFHSR00025', 11, 10, 'ON_HAND', 0, 1, NULL, '', NULL, 'erpadmin', '2028-04-24 13:04:43', '2028-04-25 20:30:53', 11, 0);";
            DBUtilities.exUpdateQuery(query_ATP, "myntra_atp");
            log.info("atp updated");

            String query_CIC_Q2="INSERT INTO `core_inventory_counts` (`id`, `warehouse_id`, `warehouse_name`, `quality`, `owner_partner_id`, `sku_id`, `sku_code`, `inv_count`, `blocked_manually_count`, `blocked_missed_count`, `created_by`, `created_on`, `last_modified_on`, `version`, `blocked_processing_count`)\n" +
                    "VALUES (null, 1, 'Bangalore', 'Q2',"+ owner_partner_id +","+ ITEM_TRAN_SKU +", 'MSMRFHSR00025', 30, 0, 6, 'sneha', '2028-02-26 19:55:46', '2028-02-26 19:55:46', 11, 13)";
            DBUtilities.exUpdateQuery(query_CIC_Q2, "myntra_ims");
            log.info("core_inv_count updated for q2 ");

            String query_stn="INSERT INTO `core_stns` (`id`, `barcode`, `stn_type`, `created_by`, `created_on`, `last_modified_on`, `source_warehouse_id`, `destination_warehouse_id`, `item_status`, `quality`, `approver`, `stn_status`, `received_date`, `version`, `dispatch_date`, `delivery_warehouse_id`, `transporter`, `lr_awb_num`, `remarks`, `approval_date`, `no_of_cartons`, `store_id`, `stn_category`, `buyer_id`)\n" +
                    "VALUES (null, 'STNJEBA301114-00', 'NORMAL', 'sneha', '2022-11-30 18:38:29', '2022-11-30 18:44:52', 1, 36, 'STORED', 'Q1', 'sneha', 'PICK_INITIATED', '2022-11-30 18:44:38', 5, '2022-11-30 18:44:27', 1, '1', '1', 'automation test', '2022-11-30 18:43:24', 1, 1, NULL, NULL);";

            DBUtilities.exUpdateQuery(query_stn, "myntra_wms");
            String get_stnId="select id from `core_stns` where `barcode`= 'STNJEBA301114-00'";
            String stn_id=DBUtilities.exSelectQueryForSingleRecord(get_stnId,"myntra_wms").get("id").toString();

            String stn_skus="INSERT INTO `core_stn_skus` (`id`, `qty`, `mrp`, `created_by`, `created_on`, `last_modified_on`, `sku_code`, `stn_id`, `version`, `dispatched_qty`, `received_qty`, `landed_price`, `sku_id`)\n" +
                    "VALUES (null, 1, 4999, 'sneha', '2022-04-17 15:52:32', '2022-04-17 16:11:49', 'MSMRFHSR00025',"+stn_id+", 2, 1, 1, 2885.43,"+ITEM_TRAN_SKU+" );";
            DBUtilities.exUpdateQuery(stn_skus, "myntra_wms");

            String query_stn_items="INSERT INTO `core_stn_items` (`id`, `item_id`, `stn_id`, `created_by`, `created_on`, `last_modified_on`, `version`, `carton_barcode`, `received`, `is_dispatched`, `mrp`, `landed_price`, `list_price`, `owner_id`)\n" +
                    "VALUES (null, 1000273,"+stn_id+", 'sneha', '2022-04-17 15:54:40', '2022-04-17 15:54:40', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL);";
            DBUtilities.exUpdateQuery(query_stn_items, "myntra_wms");
            log.info("Insertion and update done sucessfully");

        }

}
