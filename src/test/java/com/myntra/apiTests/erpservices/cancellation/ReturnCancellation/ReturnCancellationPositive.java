package com.myntra.apiTests.erpservices.cancellation.ReturnCancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.ReturnCancellation.validators.ReturnCancellationValidator;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.tests.LMS_Trip;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.domain.response.PickupResponse;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.lordoftherings.SlackMessenger;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.oms.client.response.PacketResponse;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lms.validators.StatusPoller.lmsClient;

public class ReturnCancellationPositive {

    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    TripClient_QA tripClient_qa = new TripClient_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa = new MLShipmentClientV2_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa = new TripOrderAssignmentClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa = new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa = new DeliveryStaffClient_QA();
    StoreClient_QA storeClient_qa = new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa = new MasterBagClient_QA();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    PickupShipmentClient_QA pickupShipmentClient_qa = new PickupShipmentClient_QA();

    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private OrderEntry orderEntry = new OrderEntry();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private ProcessRelease processRelease = new ProcessRelease();
    private ItemEntry itemEntry = new ItemEntry();
    MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(LMS_Trip.class);
    HashMap<String, String> pincodeDCMap;
    ReturnCancellationValidator returnCancellationValidator = new ReturnCancellationValidator();
    Map<String, Object> eventEntries = null;

    //creating HashMap to maintain pincode and DcId
    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
    }

    public Long createReturn() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, "560068", "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        Thread.sleep(3000);
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "ReturnCancellation", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnId);
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnId);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        Thread.sleep(15000);
        return returnId;
    }

    // PQCP DC onhold cc reject reship to customer create return again cancellation should be allowed
    @Test(description = "C28013")
    public void pqcpDcOnHoldCCRejectReship() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        Long returnID = createReturn();
        log.info("________ReturnId: " + returnID);

        ReturnResponse q = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID = q.getData().get(0).getOrderId().toString();

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Cancellation should not be allowed" + trackingNo);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
         returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Cancellation should not be allowed" + trackingNo);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Cancellation should not be allowed" + trackingNo);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
         returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Cancellation should not be allowed" + trackingNo);

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);
        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Cancellation should not be allowed" + trackingNo);

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        //TODO : cancellation should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
         returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Cancellation should not be allowed" + trackingNo);

        //cc reject
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_REJECTED, true);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.REJECTED_ONHOLD_PICKUP_WITH_DC, "QC rejected by cc status mismatch");

        //TODO : cancellation should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Cancellation should not be allowed" + trackingNo);


        StoreHelper storeHelper = new StoreHelper(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
        String filePath = storeHelper.createCSVFile(returnID, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, trackingNo);
        String reshipToCustomerResponse = null;
        int i = 0;
        while (i < 2) {
            reshipToCustomerResponse = storeHelper.reshipToCustomer(filePath);
            i++;
        }
        Assert.assertTrue(reshipToCustomerResponse.contains(ResponseMessageConstants.reshipToCustomer), "Return order is not reshipped to customer");

        StoreValidator storeValidator = new StoreValidator();
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse9 = storeHelper.getReturnStatusInLMS(returnID.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse9, com.myntra.lastmile.client.status.ShipmentStatus.RESHIPPED_DELIVERED_TO_CUSTOMER);

        //TODO : cancellation should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
         returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Cancellation should not be allowed" + trackingNo);

        //TODO : create return
        returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();

        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        returnID = returnResponse.getData().get(0).getId();
        Thread.sleep(6000);
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));
        returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();

       // TODO : Cancellation should be allowed
        Thread.sleep(6000);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        Thread.sleep(6000);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID, EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

    //cancel it after manifestation should be allowed
    @Test(description = "C27918 C27921")
    public void cancelAfterManifestation() throws Exception {

        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
         pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);
    }

    //create a return before manifesting preapprove it cancel it should be allowed.
    //TODO : As the picked tracking number is already manifested so this case needs to be tested manually
    @Test(description = "C27906 C27919")
    public void preApproveBeforeManifestation() throws Exception {

        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, "560068", "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);


        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        // preapprove by cc
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_APPROVE, true);

        //TODO : Cancellation should be allowed
        //returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        OrderResponse returnShipmentLMSObject= null;
        for (int times =0; times<4; times++) {

            returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
            Thread.sleep(2000);
        }
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();

        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        Map<String, Object> pickupShipmentMap = null;Map<String, Object> returnShipmentMap = null;

        int pickup_is_active=0,return_is_active =0;
        for(int times=0; times<7;times++) {
            pickupShipmentMap = DBUtilities.exSelectQueryForSingleRecord("select is_active from return_pickup_association where pickup_shipment_id_fk=" + pickupShipmentID, "myntra_lms");
            if (Integer.parseInt(pickupShipmentMap.get("is_active").toString()) != pickup_is_active)
                Thread.sleep(10000);
            else break;
        }
        Assert.assertTrue(pickupShipmentMap.get("is_active").equals(pickup_is_active));

        for(int times=0;times<7;times++)
        {
            returnShipmentMap = DBUtilities.exSelectQueryForSingleRecord("select is_active from return_pickup_association where return_shipment_id_fk=" + returnShipmentID, "myntra_lms");
            if(Integer.parseInt(returnShipmentMap.get("is_active").toString()) == return_is_active)
                Thread.sleep(10000);
            else break;
        }
        Assert.assertTrue(returnShipmentMap.get("is_active").equals(return_is_active));
    }

    //create a return after manifesting approve it cancel it should be allowed.
    @Test(description = "C27918")
    public void AfterManifestApproveAndCancel() throws Exception {
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);

        // preapprove by cc
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_APPROVE, true);

        //TODO : Cancellation should be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();

        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

    //TODO : Create a return and cancel it should be allowed
    @Test(description = "C27920")
    public void createAndCancel() throws Exception {
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        //TODO : Cancellation should be allowed.
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        Thread.sleep(3000);
        //Cancelation of return should be allowed
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

    // TODO : Create a return cancel it create another return and cancel it
    // TODO : Another return wont get created untill code is fixed
    @Test(description = "C27923")
    public void cancelSecondReturn() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //TODO : Cancellation should be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        Thread.sleep(3000);
        //Cancelation of return should be allowed
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

        Thread.sleep(5000);

        //create another return
        returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        returnID = returnResponse.getData().get(0).getId();
        orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
        ExceptionHandler.handleTrue(rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID)).getData().get(0).getReturnMode().toString().equals(EnumSCM.OPEN_BOX_PICKUP), "");
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //cancel the return
        //TODO : Cancel the return
        Thread.sleep(5000);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        Thread.sleep(3000);
        //Cancelation of return should be allowed
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

    //C27927
    //assign to trip unassign from trip cancellation should be allowed
    @Test(description = "C27927")
    public void unassignFromTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "cancel should not be allowed for the order with trackingNo" + trackingNo);

        //unassign order to trip
        Assert.assertEquals(((TripOrderAssignmentResponse) tripClient_qa.unassignOrderFromTripThroughTripId(packetId, trackingNo, tripId, ShipmentType.DL)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : cancellation should be allowed
        //returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);

        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

    //create a return cancel from CC should be allowed

    @Test(description = "C27934")
    public void cancelFromCC() throws Exception {
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        log.info("________ReturnId: " + returnID);

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();

        //TODO : Cancel it from CC should be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);

        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.CustomerCare);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);


    }

    //customer marked happy with product cancellation should be allowed

    @Test(description = "C27892 C27944")
    public void happyWithProduct() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        log.info("________ReturnId: " + returnID);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //   DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO: //cancellation should not be allowed now as trip has started
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        // Assert.assertTrue(lmsServiceHelper.updateShipmentStatusV3(trackingNo, ShipmentUpdateEvent.CANCEL, ShipmentType.RETURN, ShipmentUpdateActivityTypeSource.Customer).contains("ERROR"),"customer marked happy with product cancellation should be allowed");
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);


        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();
        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.HAPPY_WITH_PRODUCT, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        //reconcile not required for failed pickup
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.FAILED_PICKUP, "shipment status mismatch");

        //complete trip
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.HAPPY_WITH_PRODUCT, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.ASSIGNED_TO_DC, "shipment status mismatch");

        //TODO : Cancellation should be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);

        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

    //FP -> cancel not allowed  ->  close trip ->  cancel should be allowed.
    @Test(description = "C28123 C28124")
    public void cancelBeforeClosingTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();

        // scan tracking number in trip
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.FAILED, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        //reconcile not required for failed pickup

        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.FAILED_PICKUP, "shipment status mismatch");

        //Cancellation should not be allowed
        returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        PickupShipmentClient_QA pickupShipmentClient_qa = new PickupShipmentClient_QA();

        //complete trip
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        //TODO : Cancellation should be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);

        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);


    }

    //cancellation should be allowed after creating a scheduled pickup
    //TODO : cannot be done do it manually
    @Test(description = "C27943")
    public void scheduledPickup() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        OrderResponse orderResponse01 = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse01, com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse01.getOrders().get(0).getDeliveryCenterId();
        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        String ppsId = orderReleaseEntry.getOrderLines().get(0).getPaymentPpsId();
        LocalDate localDate = LocalDate.now();
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = storeHelper.createReturnWithSchedulePickup(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071",
                "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO, ppsId,
                storeHelper.convertTimetoEpoch(String.format("%sT%s+05:30", localDate, "09:00:00")), storeHelper.convertTimetoEpoch(String.format("%sT%s+05:30", localDate, "12:00:00")));
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        String returnID = returnResponse.getData().get(0).getStoreReturnId();
        Thread.sleep(4000);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));

        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        //scheduled pickup cancellation should be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        //validation
        //String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        String pickupShipmentID = lmsHelper.getPickupShipmentIdByTrackingNumber(trackingNo);
        Map<String, Object> shipment_status = (Map<String, Object>) lmsHelper.getShipmentStatusForPickup.apply(trackingNo);
        Assert.assertEquals(shipment_status.get("shipment_status"), EnumSCM.CANCELLED, "Expected failed pickup");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        Map<String, Object> pickupShipmentMap = null;Map<String, Object> returnShipmentMap = null;
        int pickup_is_active=0,return_is_active =0;
        for(int times=0; times<7;times++) {
            pickupShipmentMap = DBUtilities.exSelectQueryForSingleRecord("select is_active from return_pickup_association where pickup_shipment_id_fk=" + pickupShipmentID, "myntra_lms");
            if (Integer.parseInt(pickupShipmentMap.get("is_active").toString()) != pickup_is_active)
                Thread.sleep(10000);
            else break;
        }
        Assert.assertTrue(pickupShipmentMap.get("is_active").equals(pickup_is_active));

        for(int times=0;times<7;times++)
        {
            returnShipmentMap = DBUtilities.exSelectQueryForSingleRecord("select is_active from return_pickup_association where return_shipment_id_fk=" + returnShipmentID, "myntra_lms");
            if(Integer.parseInt(returnShipmentMap.get("is_active").toString()) == return_is_active)
                Thread.sleep(10000);
            else break;
        }
        Assert.assertTrue(returnShipmentMap.get("is_active").equals(return_is_active));
    }

    //Assign return to another dc cancellation should be allowed
    @Test(description = "C27942")
    public void dcToDc() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();

        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        List<String> trackingNum = new ArrayList<>();
        trackingNum.add(returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo());
        Assert.assertEquals(lmsServiceHelper.assignOrderToDC.apply(DcId, 1, returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo()), EnumSCM.SUCCESS);

        //TODO : cancellation should be allowed be
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);

        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

    //Create a return mark FP requeue cancellation should be allowed
    @Test(description = "C28127")
    public void fpRequeue() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
       lms_returnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.FAILED_PICKUP);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();

        Map<String, Object> shipment_status = (Map<String, Object>) lmsHelper.getShipmentStatusForPickup.apply(trackingNo);
        Assert.assertEquals(shipment_status.get("shipment_status"), EnumSCM.FAILED_PICKUP, "Expected failed pickup");
        //requeue
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntry = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntry.setTrackingNumber(trackingNo);
        mlShipmentUpdateEntry.setEventTime(date);
        mlShipmentUpdateEntry.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntry.setEventLocation("DC- 5");
        mlShipmentUpdateEntry.setRemarks("failed pickup requeued for another attempt");
        mlShipmentUpdateEntry.setEvent(MLShipmentUpdateEvent.UNASSIGN);
        mlShipmentUpdateEntry.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntry.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntry.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        lms_returnHelper.processReturnWithoutNewValidation(returnID.toString(), EnumSCM.FAILED_PICKUP);
        //requeue
        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntry);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        //cancellation should be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);

        returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

 }

