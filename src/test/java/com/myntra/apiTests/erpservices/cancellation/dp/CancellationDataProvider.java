package com.myntra.apiTests.erpservices.cancellation.dp;

import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.Toolbox;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class CancellationDataProvider {

    static Initialize init = new Initialize("/Data/configuration");

    @DataProvider(parallel = false)
    public static Object[][] ForwardOrderStatus(ITestContext testContext) {
        Object[] NORMAL_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.NORMAL}; //Cancellation not allowed
        Object[] NORMAL_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.NORMAL};

        Object[] SDD_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.SDD}; //Cancellation not allowed
        Object[] SDD_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.SDD};

        Object[] EXPRESS_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.EXPRESS}; //Cancellation not allowed
        Object[] EXPRESS_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_PK, NORMAL_IS ,NORMAL_ADD_TO_MB,NORMAL_SHIPPED,NORMAL_OFD,NORMAL_DELIVERED,
                //SDD_PK, SDD_IS, SDD_ADD_TO_MB, SDD_SHIPPED, SDD_OFD, SDD_DELIVERED,
                //EXPRESS_PK, EXPRESS_IS, EXPRESS_ADD_TO_MB, EXPRESS_SHIPPED, EXPRESS_OFD, EXPRESS_DELIVERED
        };
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider(parallel = false)
    public static Object[][] TryAndBuyOrderStatus() {
        Object[] NORMAL_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.NORMAL}; //Cancellation not allowed
        Object[] NORMAL_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.NORMAL};

        Object[] SDD_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.SDD}; //Cancellation not allowed
        Object[] SDD_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.SDD};

        Object[] EXPRESS_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.EXPRESS}; //Cancellation not allowed
        Object[] EXPRESS_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_PK, NORMAL_IS ,NORMAL_ADD_TO_MB,NORMAL_SHIPPED,NORMAL_OFD,NORMAL_DELIVERED,
                //SDD_PK, SDD_IS, SDD_ADD_TO_MB, SDD_SHIPPED, SDD_OFD, SDD_DELIVERED,
                //EXPRESS_PK, EXPRESS_IS, EXPRESS_ADD_TO_MB, EXPRESS_SHIPPED, EXPRESS_OFD, EXPRESS_DELIVERED
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] ExchangeOrderStatus() {
        Object[] NORMAL_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.NORMAL}; //Cancellation not allowed
        Object[] NORMAL_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.NORMAL};

        Object[] SDD_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.SDD}; //Cancellation not allowed
        Object[] SDD_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.SDD};

        Object[] EXPRESS_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.EXPRESS}; //Cancellation not allowed
        Object[] EXPRESS_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_PK, NORMAL_IS ,NORMAL_ADD_TO_MB,NORMAL_SHIPPED,NORMAL_OFD,NORMAL_DELIVERED,
                //SDD_PK, SDD_IS, SDD_ADD_TO_MB, SDD_SHIPPED, SDD_OFD, SDD_DELIVERED,
                //EXPRESS_PK, EXPRESS_IS, EXPRESS_ADD_TO_MB, EXPRESS_SHIPPED, EXPRESS_OFD, EXPRESS_DELIVERED
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] AddShipmenToStoreBag() {
        Object[] DL = {ShipmentType.DL};
        Object[] TryAndBuy = {ShipmentType.TRY_AND_BUY};
        Object[][] dataSet = new Object[][]{
                //DL,
                TryAndBuy
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] AddAndRemoveShipmenToStoreBag() {
        Object[] DL = {ShipmentType.DL};
        Object[] TryAndBuy = {ShipmentType.TRY_AND_BUY};
        Object[][] dataSet = new Object[][]{
                DL, TryAndBuy
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] CancelONPackedState() {
        Object[] NORMAL_PACKED = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] SDD_PACKED = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] EXPRESS_PACKED = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_PACKED
                //, SDD_PACKED, EXPRESS_PACKED
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] CancelONInsanncedShipment() {
        Object[] NORMAL_ADD_TO_MB = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] SDD_ADD_TO_MB = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] EXPRESS_ADD_TO_MB = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_ADD_TO_MB
                //, SDD_ADD_TO_MB, EXPRESS_ADD_TO_MB
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] CancelONAddedToMasterBag() {
        Object[] NORMAL_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] SDD_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] EXPRESS_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_ADD_TO_MB
                //, SDD_ADD_TO_MB, EXPRESS_ADD_TO_MB
        };
        return dataSet;
    }


    @DataProvider(parallel = false)
    public static Object[][] CancelONClosedMasterBag() {
        Object[] NORMAL_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] SDD_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] EXPRESS_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_ADD_TO_MB
                //, SDD_ADD_TO_MB, EXPRESS_ADD_TO_MB
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] IntegrationForwardOrderStatus(ITestContext testContext) {
        Object[] NORMAL_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.NORMAL}; //Cancellation not allowed
        Object[] NORMAL_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.NORMAL};

        Object[][] dataSet = new Object[][]{
                NORMAL_PK, NORMAL_IS ,NORMAL_ADD_TO_MB,NORMAL_SHIPPED,NORMAL_OFD,NORMAL_DELIVERED,
        };
        //return dataSet;
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider(parallel = false)
    public static Object[][] CancelON_PackedState() {
        Object[] NORMAL_PK = {ShipmentStatus.PACKED, com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod.NORMAL};
        Object[] SDD_PK = {ShipmentStatus.PACKED, com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod.SDD};
        Object[] EXPRESS_PK = {ShipmentStatus.PACKED, com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_PK
                //, SDD_PK, EXPRESS_PK
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] AdminCorrectionForwardCancelledOrderStatus(ITestContext testContext) {
        Object[] NORMAL_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] NORMAL_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.NORMAL}; //Cancellation not allowed
        Object[] NORMAL_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.NORMAL};

        Object[] SDD_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] SDD_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.SDD}; //Cancellation not allowed
        Object[] SDD_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.SDD};

        Object[] EXPRESS_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_SHIPPED = {ReleaseStatus.SH, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};
        Object[] EXPRESS_OFD = {ReleaseStatus.OFD, ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.EXPRESS}; //Cancellation not allowed
        Object[] EXPRESS_DELIVERED = {ReleaseStatus.DL, ShipmentStatus.DELIVERED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_PK, NORMAL_IS ,NORMAL_ADD_TO_MB,NORMAL_SHIPPED,NORMAL_OFD,NORMAL_DELIVERED,
                //SDD_PK, SDD_IS, SDD_ADD_TO_MB, SDD_SHIPPED, SDD_OFD, SDD_DELIVERED,
                //EXPRESS_PK, EXPRESS_IS, EXPRESS_ADD_TO_MB, EXPRESS_SHIPPED, EXPRESS_OFD, EXPRESS_DELIVERED
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] FlipkartCancellationFlow(ITestContext testContext) {
        Object[] NORMAL_PK = {ReleaseStatus.PK,ReleaseStatus.EXPECTED_IN_DC,ReleaseStatus.CANCELLED,ReleaseStatus.CANCELLED, com.myntra.lms.client.status.ShippingMethod.NORMAL};
        Object[] NORMAL_IS = {ReleaseStatus.IS,ReleaseStatus.EXPECTED_IN_DC, ReleaseStatus.CANCELLED, ReleaseStatus.CANCELLED,com.myntra.lms.client.status.ShippingMethod.NORMAL};
        Object[] NORMAL_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB,ReleaseStatus.EXPECTED_IN_DC, ReleaseStatus.CANCELLED,ReleaseStatus.CANCELLED, com.myntra.lms.client.status.ShippingMethod.NORMAL};
        Object[] NORMAL_SHIPPED = {ReleaseStatus.SH, ReleaseStatus.UNASSIGNED,ReleaseStatus.CANCELLED,ReleaseStatus.CANCELLED, com.myntra.lms.client.status.ShippingMethod.NORMAL};
        Object[] NORMAL_ASSIGNED_TO_SDA = {ReleaseStatus.ASSIGNED_TO_SDA, ReleaseStatus.ASSIGNED_TO_SDA,ReleaseStatus.SHIPPED,ReleaseStatus.ASSIGNED_TO_SDA, com.myntra.lms.client.status.ShippingMethod.NORMAL};//Cancellation not allowed
        Object[] NORMAL_OFD = {ReleaseStatus.OFD,ReleaseStatus.OUT_FOR_DELIVERY, ReleaseStatus.OUT_FOR_DELIVERY,ReleaseStatus.OUT_FOR_DELIVERY, com.myntra.lms.client.status.ShippingMethod.NORMAL}; //Cancellation not allowed
        Object[] NORMAL_DELIVERED = {ReleaseStatus.DL,ReleaseStatus.DELIVERED, ReleaseStatus.DELIVERED, ReleaseStatus.DELIVERED,com.myntra.lms.client.status.ShippingMethod.NORMAL}; //Cancellation not allowed

        Object[][] dataSet = new Object[][]{
                NORMAL_PK, NORMAL_IS ,NORMAL_ADD_TO_MB,NORMAL_SHIPPED,NORMAL_ASSIGNED_TO_SDA,NORMAL_OFD,NORMAL_DELIVERED,
        };
        //return dataSet;
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

}
