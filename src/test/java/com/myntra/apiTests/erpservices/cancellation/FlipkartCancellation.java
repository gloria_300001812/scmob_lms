package com.myntra.apiTests.erpservices.cancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.cancellation.dp.CancellationDataProvider;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.DB.IQueries;
import com.myntra.apiTests.erpservices.lms.Flipkart.Domain.ScanAndSortResponse;
import com.myntra.apiTests.erpservices.lms.Flipkart.FlipkartOrderProcess;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.apiTests.erpservices.oms.OMSHelpersEnums;
import com.myntra.apiTests.erpservices.tools.ToolsHelper;
import com.myntra.client.tools.response.ApplicationPropertiesResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.OrderShipmentAssociationStatus;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.platform.domain.Premise;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.response.PacketResponse;
import com.myntra.sortation.response.SortationConfigResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import lombok.SneakyThrows;
import org.hsqldb.scriptio.ScriptReaderText;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.Map;

public class FlipkartCancellation implements StatusPoller {

    FlipkartOrderProcess flipkartOrderProcess=new FlipkartOrderProcess();


    @Test(dataProvider = "FlipkartCancellationFlow", dataProviderClass = CancellationDataProvider.class, description = "ID C28818 :To check and verify post packed cancellation after Packed(PK)")
    @SneakyThrows
    public void cancellationPointsForFKOrders(ReleaseStatus releaseStatus, ReleaseStatus mlStatusBeforeCancelled,ReleaseStatus orderToShipStatus, ReleaseStatus mlStatusAfterCancelled,ShippingMethod shippingMethod){

        String packetId = flipkartOrderProcess.processOrder(releaseStatus.toString(), CourierCode.ML.toString(), "560068","36",shippingMethod);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("[%s]: packetId:[%s], trackingNumber:[%s]", releaseStatus,packetId, trackingNumber));

        orderValidator.validateOrderIsCancellable(packetId, releaseStatus,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber,LMS_CONSTANTS.TENANTID,mlStatusBeforeCancelled.toString());

        //Cancel API
        lmsServiceHelper.updateShipmentStatusV3(trackingNumber, ShipmentUpdateEvent.CANCEL, ShipmentType.DL, ShipmentUpdateActivityTypeSource.MyntraLogistics);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber,LMS_CONSTANTS.TENANTID,mlStatusAfterCancelled.toString());
        statusPollingValidator.validateOrderStatus(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID,orderToShipStatus.toString());
    }

    @Test(enabled = true,description = "ID C28818 :To check and verify post packed cancellation after Packed(PK)")
    @SneakyThrows
    public void verifyCancellationNotAllowedForStatusASSIGNED_TO_SDA(){

        String packetId = flipkartOrderProcess.processOrder(ReleaseStatus.SH.toString(), CourierCode.ML.toString(), "560068","36",ShippingMethod.NORMAL);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("[%s]: packetId:[%s], trackingNumber:[%s]", ReleaseStatus.SH,packetId, trackingNumber));

        //assign order to SDA
        long tripId = flipkartStateHelper.process_SHToASSIGNED_TO_SDA_State(packetId);

        orderValidator.validateOrderIsCancellable(packetId, ReleaseStatus.ASSIGNED_TO_SDA,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber,LMS_CONSTANTS.TENANTID,ReleaseStatus.ASSIGNED_TO_SDA.toString());

        //Cancel API
        PacketResponse packetResponse = omsServiceHelper.cancelPacket(String.valueOf(packetId), OMSHelpersEnums.PacketEvents.CANCEL_PACKET);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber,LMS_CONSTANTS.TENANTID,ReleaseStatus.ASSIGNED_TO_SDA.toString());
        statusPollingValidator.validateOrderStatus(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID,ReleaseStatus.SHIPPED.toString());

        //remove shipment from trip
        String tripOrder = Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.unassignOrderFromTrip(tripOrder);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "shipment unassigned from trip was unsuccessful");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(packetId, EnumSCM.UNASSIGNED, 2));
        orderValidator.validateOrderIsCancellable(packetId, ReleaseStatus.SH,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID);

        //Cancel API
        omsServiceHelper.cancelPacket(String.valueOf(packetId), OMSHelpersEnums.PacketEvents.CANCEL_PACKET);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber,LMS_CONSTANTS.TENANTID,ReleaseStatus.UNASSIGNED.toString());
        statusPollingValidator.validateOrderStatus(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID,ReleaseStatus.SHIPPED.toString());

    }

    @Test(enabled = true,description = "TC ID - C28819 - Showing sellerId on the scan and sort app during inscan. [app changes yet to be verified]")
    @SneakyThrows
    public void validateSellerIdForMyntra(){
        String property_name="lms.sellerid_displayed_for_client";

        //Search Property by Name
        ApplicationPropertiesResponse searchByName = ToolsHelper.getApplicationProperty(property_name);
        Assert.assertEquals(searchByName.getStatus().getStatusMessage(), "Property(s) retrieved successfully","Property search by name not successful");

        if(searchByName.getData().size()>0){
            if(!(searchByName.getData().get(0).getValue().contains(LMS_CONSTANTS.CLIENTID))){
                //Update Property
                ApplicationPropertiesResponse appPropResponse_Update = ToolsHelper.updateProperty(property_name, searchByName.getData().get(0).getValue()+","+LMS_CONSTANTS.CLIENTID, "lms", "String", searchByName.getData().get(0).getId());
                Assert.assertEquals(appPropResponse_Update.getStatus().getStatusMessage(), "Property updated successfully","Update operation is not successful");
                Assert.assertEquals(appPropResponse_Update.getData().get(0).getName(), property_name,"Property name is not changing after updating new value, it is still same");

                //Refresh Property
                ApplicationPropertiesResponse appPropResponse_Refresh3=ToolsHelper.getPropertiesRefresh();
                Assert.assertEquals(appPropResponse_Refresh3.getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Refresh is successful");

                //Search Property by Name
                ApplicationPropertiesResponse updated_appPropResponse_SearchByName = ToolsHelper.getApplicationProperty(property_name);
                Assert.assertEquals(updated_appPropResponse_SearchByName.getStatus().getStatusMessage(), "Property(s) retrieved successfully","Property search by name not successful");
                Assert.assertEquals(updated_appPropResponse_SearchByName.getData().get(0).getName(), property_name,"Property name is not changing after updating new value, it is still same");
                Assert.assertEquals(updated_appPropResponse_SearchByName.getData().get(0).getValue(),searchByName.getData().get(0).getValue()+","+LMS_CONSTANTS.CLIENTID,"updated value for the properties are not displayed ");
            }

        }else{
            ApplicationPropertiesResponse appPropResponse_Create = ToolsHelper.createProperty(property_name,LMS_CONSTANTS.CLIENTID,"lms","String");
            Assert.assertEquals(appPropResponse_Create.getStatus().getStatusMessage(), "Property added successfully","Property is not added successfully");
            String name_property = appPropResponse_Create.getData().get(0).getName();
            Assert.assertEquals(name_property, property_name,"Property name is not matching from property name which is taken from api and given property name");
            Long property_Id = appPropResponse_Create.getData().get(0).getId();
            storeValidator.isNull(String.valueOf(property_Id));

            //Refresh Property
            ApplicationPropertiesResponse appPropResponse_Refresh=ToolsHelper.getPropertiesRefresh();
            Assert.assertEquals(appPropResponse_Refresh.getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Refresh is successful");

            //Search Property by Name
            ApplicationPropertiesResponse appPropResponse_SearchByName = ToolsHelper.getApplicationProperty(property_name);
            Assert.assertEquals(appPropResponse_SearchByName.getStatus().getStatusMessage(), "Property(s) retrieved successfully","Property search by name not successful");
            Assert.assertEquals(appPropResponse_SearchByName.getData().get(0).getId().toString(),property_Id.toString(),"created property id is not present");
        }

        //Create Mock order Till PK
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsByOrderId(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.PACKED);
        String location = orderResponse.getOrders().get(0).getDispatchHubCode();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        // do inscan using sortand
        ScanAndSortResponse inscanUsingScanAndSort = flipkartHelper.scanAndSort(LMS_CONSTANTS.TENANTID,trackingNo,location, Premise.PremiseType.HUB,true);
        Assert.assertEquals(inscanUsingScanAndSort.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"inscan is not successful using scanAndSort api");
        try {
            Assert.assertNotNull(inscanUsingScanAndSort.getSellerId(), "seller id is null when the FG is ON");
        } catch (NullPointerException n) {
            n.getMessage();
            Assert.fail("Seller id value is null when FG is present");
        }
        //get seller id from shipment item table and validate
        Map<String, Object> shipmentItemTableData = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.ShipmentItemTableValue.getShipmentItemTabelData), trackingNo), "myntra_lms");
        Assert.assertEquals(shipmentItemTableData.get("seller_id").toString(),inscanUsingScanAndSort.getSellerId(),"Seller id is not matching");
    }

    @Test(enabled = true,description = "TC ID - C28819 - Showing sellerId on the scan and sort app during inscan. [app changes yet to be verified]")
    @SneakyThrows
    public void validateSellerIdForFlipkart(){
        String property_name="lms.sellerid_displayed_for_client";

        //Search Property by Name
        ApplicationPropertiesResponse searchByName = ToolsHelper.getApplicationProperty(property_name);
        Assert.assertEquals(searchByName.getStatus().getStatusMessage(), "Property(s) retrieved successfully","Property search by name not successful");

        if(searchByName.getData().size()>0){
            if(!(searchByName.getData().get(0).getValue().contains(LMS_CONSTANTS.FLIPKART_CLIENTID))){
                //Update Property
                ApplicationPropertiesResponse appPropResponse_Update = ToolsHelper.updateProperty(property_name, searchByName.getData().get(0).getValue()+","+LMS_CONSTANTS.FLIPKART_CLIENTID, "lms", "String", searchByName.getData().get(0).getId());
                Assert.assertEquals(appPropResponse_Update.getStatus().getStatusMessage(), "Property updated successfully","Update operation is not successful");
                Assert.assertEquals(appPropResponse_Update.getData().get(0).getName(), property_name,"Property name is not changing after updating new value, it is still same");

                //Refresh Property
                ApplicationPropertiesResponse appPropResponse_Refresh3=ToolsHelper.getPropertiesRefresh();
                Assert.assertEquals(appPropResponse_Refresh3.getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Refresh is successful");

                //Search Property by Name
                ApplicationPropertiesResponse updated_appPropResponse_SearchByName = ToolsHelper.getApplicationProperty(property_name);
                Assert.assertEquals(updated_appPropResponse_SearchByName.getStatus().getStatusMessage(), "Property(s) retrieved successfully","Property search by name not successful");
                Assert.assertEquals(updated_appPropResponse_SearchByName.getData().get(0).getName(), property_name,"Property name is not changing after updating new value, it is still same");
                Assert.assertEquals(updated_appPropResponse_SearchByName.getData().get(0).getValue(),searchByName.getData().get(0).getValue()+","+LMS_CONSTANTS.FLIPKART_CLIENTID,"updated value for the properties are not displayed ");
            }

        }else{
            ApplicationPropertiesResponse appPropResponse_Create = ToolsHelper.createProperty(property_name,LMS_CONSTANTS.FLIPKART_CLIENTID,"lms","String");
            Assert.assertEquals(appPropResponse_Create.getStatus().getStatusMessage(), "Property added successfully","Property is not added successfully");
            String name_property = appPropResponse_Create.getData().get(0).getName();
            Assert.assertEquals(name_property, property_name,"Property name is not matching from property name which is taken from api and given property name");
            Long property_Id = appPropResponse_Create.getData().get(0).getId();
            storeValidator.isNull(String.valueOf(property_Id));

            //Refresh Property
            ApplicationPropertiesResponse appPropResponse_Refresh=ToolsHelper.getPropertiesRefresh();
            Assert.assertEquals(appPropResponse_Refresh.getStatus().getStatusType().toString(), EnumSCM.SUCCESS,"Refresh is successful");

            //Search Property by Name
            ApplicationPropertiesResponse appPropResponse_SearchByName = ToolsHelper.getApplicationProperty(property_name);
            Assert.assertEquals(appPropResponse_SearchByName.getStatus().getStatusMessage(), "Property(s) retrieved successfully","Property search by name not successful");
            Assert.assertEquals(appPropResponse_SearchByName.getData().get(0).getId().toString(),property_Id.toString(),"created property id is not present");
        }

        //Create Mock order Till PK
        String packetId =flipkartHelper.createFlipKartOrder(CourierCode.ML.toString(),"560068", LMS_CONSTANTS.TENANTID,"36", ShippingMethod.NORMAL);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsByOrderId(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.FLIPKART_CLIENTID);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.PACKED);
        String location = orderResponse.getOrders().get(0).getDispatchHubCode();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.FLIPKART_CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        // do inscan using scanandsort
        ScanAndSortResponse inscanUsingScanAndSort = flipkartHelper.scanAndSort(LMS_CONSTANTS.TENANTID,trackingNo,location, Premise.PremiseType.HUB,true);
        Assert.assertEquals(inscanUsingScanAndSort.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"inscan is not successful using scanAndSort api");
        try {
            Assert.assertNotNull(inscanUsingScanAndSort.getSellerId(), "seller id is null when the FG is ON for flip-kart order");
        } catch (NullPointerException n) {
            n.getMessage();
            Assert.fail("Seller id value is null when FG is present");
        }
        //get seller id from shipment item table and validate
        Map<String, Object> shipmentItemTableData = DBUtilities.exSelectQueryForSingleRecord(IQueries.formatQuery(IQueries.formatQuery(IQueries.ShipmentItemTableValue.getShipmentItemTabelData), trackingNo), "myntra_lms");
        Assert.assertEquals(shipmentItemTableData.get("seller_id").toString(),inscanUsingScanAndSort.getSellerId(),"Seller id is not matching");
    }

}
