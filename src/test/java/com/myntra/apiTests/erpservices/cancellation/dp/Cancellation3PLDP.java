package com.myntra.apiTests.erpservices.cancellation.dp;

import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import org.testng.annotations.DataProvider;

/**
 * @author Bharath.MC
 * @since Jul-2019
 */
public class Cancellation3PLDP {

    @DataProvider(parallel = false)
    public static Object[][] CancelONPackedState() {
        Object[] NORMAL_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] SDD_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] EXPRESS_PK = {ReleaseStatus.PK, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_PK
                //, SDD_PK, EXPRESS_PK
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] CancelONInscannedState() {
        Object[] NORMAL_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.NORMAL};
        Object[] SDD_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.SDD};
        Object[] EXPRESS_IS = {ReleaseStatus.IS, ShipmentStatus.CANCELLED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_IS
                //, SDD_IS, EXPRESS_IS
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] CancelON_AddToMBState() {
        Object[] NORMAL_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.ADDED_TO_MB, ShippingMethod.NORMAL};
        Object[] SDD_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.ADDED_TO_MB, ShippingMethod.SDD};
        Object[] EXPRESS_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.ADDED_TO_MB, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_ADD_TO_MB
                //, SDD_ADD_TO_MB, EXPRESS_ADD_TO_MB
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] CancelON_AddToMBAndClosedState() {
        Object[] NORMAL_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.ADDED_TO_MB, ShippingMethod.NORMAL};
        Object[] SDD_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.ADDED_TO_MB, ShippingMethod.SDD};
        Object[] EXPRESS_ADD_TO_MB = {ReleaseStatus.ADDED_TO_MB, ShipmentStatus.ADDED_TO_MB, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_ADD_TO_MB
                //, SDD_ADD_TO_MB, EXPRESS_ADD_TO_MB
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] CancelON_ShippedState() {
        Object[] NORMAL_Shipped = {ShipmentStatus.SHIPPED, ShippingMethod.NORMAL};
        Object[] SDD_Shipped = {ShipmentStatus.SHIPPED, ShippingMethod.SDD};
        Object[] EXPRESS_Shipped = {ShipmentStatus.SHIPPED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_Shipped
                //, SDD_Shipped, EXPRESS_Shipped
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] CancelON_OFDState() {
        Object[] NORMAL_OFD = {ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.NORMAL};
        Object[] SDD_OFD = {ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.SDD};
        Object[] EXPRESS_OFD = {ShipmentStatus.OUT_FOR_DELIVERY, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_OFD
                //, SDD_OFD, EXPRESS_OFD
        };
        return dataSet;
    }

    @DataProvider(parallel = false)
    public static Object[][] CancelON_DeliveredState() {
        Object[] NORMAL_OFD = {ShipmentStatus.DELIVERED, ShippingMethod.NORMAL};
        Object[] SDD_OFD = {ShipmentStatus.DELIVERED, ShippingMethod.SDD};
        Object[] EXPRESS_OFD = {ShipmentStatus.DELIVERED, ShippingMethod.EXPRESS};

        Object[][] dataSet = new Object[][]{
                NORMAL_OFD
                //, SDD_OFD, EXPRESS_OFD
        };
        return dataSet;
    }

}
