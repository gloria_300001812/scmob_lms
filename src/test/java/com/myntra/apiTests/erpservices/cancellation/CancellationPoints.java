package com.myntra.apiTests.erpservices.cancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.LMSCancellation.CancelationHelper;
import com.myntra.apiTests.erpservices.LMSCancellation.validators.OrderValidator;
import com.myntra.apiTests.erpservices.cancellation.dp.CancellationDataProvider;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.cms.pim.dto.StatusType;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.ShipmentEntry;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponseCode;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class CancellationPoints {
    String env = getEnvironment();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    OrderValidator orderValidator = new OrderValidator();
    LMSOperations lmsOperations = new LMSOperations();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    LastmileOperations lastmileOperations = new LastmileOperations();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    TMSOperations tmsOperations = new TMSOperations();
    CancelationHelper cancelationHelper = new CancelationHelper();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    TripClient_QA tripClient_qa=new TripClient_QA();

    @BeforeClass
    public void setUp() throws Exception {
        //cancelationHelper.enableSorterOnHub("DH-BLR");
    }

    @Test(dataProvider = "CancelONPackedState", dataProviderClass = CancellationDataProvider.class,
            description = "C21234", retryAnalyzer = Retry.class)
    public void CancelAfterPackedState(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        //already persent in Sanity case - will move it to here later
        String orderId, packetId, trackingNumber;
        MasterbagUpdateResponse closeMBResponse;
        TMSMasterbagReponse tmsMasterbagReponse;
        MasterbagDomain mbCreateResponse;
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560068", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        Long masterBagId;
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);

        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, releaseStatus);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());

        //Order Inscan V2
        String orderInScanResponse= lmsOperations.orderInscanV2(trackingNumber, wareHouseId, false);
        orderValidator.validateCancelledShipmentOrderInscan(orderInScanResponse, ShipmentStatus.CANCELLED, originHubCode, returnHubCode);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());
        //orderValidator.validateShipmentOrderDestinationHub(packetId, returnHubCode);

        //MasterBag V2 -- Adding Cancelled shipments to Forward bag should Fail
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), StatusType.ERROR.toString(), "Adding Cancelled shipment to forward MasterBag should not be allowed");
        Assert.assertEquals(addShipmentToMbResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());

        //Restock here

    }

    @Test(dataProvider = "CancelONInsanncedShipment", dataProviderClass = CancellationDataProvider.class,
            retryAnalyzer = Retry.class)
    public void CancelAfterInscannedState(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String orderId, packetId, trackingNumber;
        MasterbagUpdateResponse closeMBResponse;
        TMSMasterbagReponse tmsMasterbagReponse;
        MasterbagDomain mbCreateResponse;
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560068", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        Long masterBagId;
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, packetId, trackingNumber));

        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, releaseStatus);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());

        //MasterBag V2 -- Adding Cancelled shipments to Forward bag should Fail
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), StatusType.ERROR.toString(), "Adding Cancelled shipment to forward MasterBag should not be allowed");
        Assert.assertEquals(addShipmentToMbResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());
//        Assert.assertTrue(addShipmentToMbResponse.getErrorMessage().
//                contains("Order trackingNumber to be sent to destination returnsHub, but being added in masterbag with destination destinationHub"
//                        .replace("trackingNumber", trackingNumber)
//                .replace("destinationHub", returnHubCode)
//                .replace("returnsHub", returnHubCode)));
        //Assert.assertTrue(addShipmentToMbResponse.getStatus().getStatusMessage().contains("Update to Masterbag masterBagId with event SHIPMENTS_ADDED failed.".replace("masterBagId", String.valueOf(masterBagId))));
        Assert.assertEquals(masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), StatusType.ERROR.toString());
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, packetId, trackingNumber));

        //Call Restock API
    }

    @Test(description = "C21024", dataProvider = "CancelONAddedToMasterBag", dataProviderClass = CancellationDataProvider.class,
            retryAnalyzer = Retry.class)
    public void CancelAfterAddedToMasterBagState(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String orderId, packetId, trackingNumber;
        MasterbagUpdateResponse closeMBResponse;
        TMSMasterbagReponse tmsMasterbagReponse;
        MasterbagDomain mbCreateResponse;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipcode = "560068", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        Long masterBagId;
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, packetId, trackingNumber));
        Map<String, Object> shipmentOrderMap = DBUtilities.exSelectQueryForSingleRecord("SELECT shipment_id FROM shipment_order_map where tracking_Number = '" + trackingNumber + "';", "myntra_lms");
        masterBagId = Long.parseLong(shipmentOrderMap.get("shipment_id").toString());
        System.out.println("Forward MasterBAgId="+masterBagId);
        orderValidator.validateOrderIsCancellable(packetId, releaseStatus);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.NEW);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, releaseStatus);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());

        //Close MasterBag
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(closeMBResponse.getStatus().getStatusType().toString(), StatusType.SUCCESS.toString());

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //Recieve Cancelled order in DC
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        //Validate RTO CONfirmed
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SH);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, packetId, trackingNumber));

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(destinationHubCode, returnHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("Reverse masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        tmsOperations.createAndShipReturnContainer(masterBagId, destinationHubCode, returnHubCode, LaneType.INTRACITY, LMS_CONSTANTS.TENANTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_DISPATCHED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, destinationHubCode, returnHubCode);

        //Call Restock API
    }

    @Test(description = "C21024", dataProvider = "CancelONClosedMasterBag", dataProviderClass = CancellationDataProvider.class,
            retryAnalyzer = Retry.class)
    public void CancelAfterMasterBagClosedState(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String orderId, packetId, trackingNumber;
        MasterbagUpdateResponse closeMBResponse;
        TMSMasterbagReponse tmsMasterbagReponse;
        MasterbagDomain mbCreateResponse;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipcode = "560068", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        Long masterBagId;
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, packetId, trackingNumber));
        orderValidator.validateOrderIsCancellable(packetId, releaseStatus);
        Map<String, Object> shipmentOrderMap = DBUtilities.exSelectQueryForSingleRecord("SELECT shipment_id FROM shipment_order_map where tracking_Number = '" + trackingNumber + "';", "myntra_lms");
        masterBagId = Long.parseLong(shipmentOrderMap.get("shipment_id").toString());
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(closeMBResponse.getStatus().getStatusType().toString(), StatusType.SUCCESS.toString());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, releaseStatus);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //Recieve Cancelled order in DC
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        //Validate RTO CONfirmed
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SH);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, packetId, trackingNumber));
        //Process RTO and Restock
        //Call Restock API
    }

    @Test(description = "C21025, C21057", retryAnalyzer = Retry.class)
    public void CancelAfterRecieveMasterBagInTransportHub() throws Exception {
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        String inScanResponse;
        Long masterBagId;
        String clientId = LMS_CONSTANTS.CLIENTID;
        TMSMasterbagReponse tmsMasterbagReponse;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        String zipcode = "560068", wareHouseId = "36";
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR

        sortationHelper.computeAndPrintCompletePath(originHubCode, destinationHubCode, clientId, courierCode);

        orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Order Inscan V2
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, wareHouseId, false, LMS_CONSTANTS.TENANTID);
        System.out.println("response=" + inScanResponse);
        System.out.println("inScanResponse=");

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());

        //Cancel API - AT DC
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.ADDED_TO_MB);

        //Prceed further with TMS operations
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //Recieve Cancelled order in DC
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        //Validate RTO CONfirmed
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SH);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(destinationHubCode, returnHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("Reverse masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        tmsOperations.createAndShipReturnContainer(masterBagId, destinationHubCode, returnHubCode, LaneType.INTRACITY, LMS_CONSTANTS.TENANTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_DISPATCHED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, destinationHubCode, returnHubCode);

        //Call Restock API
    }

    @Test(description = "C21025, C21027", retryAnalyzer = Retry.class)
    public void CancelAfterMasterBagAddedToContainer() throws Exception {
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        String inScanResponse;
        Long masterBagId;
        String clientId = LMS_CONSTANTS.CLIENTID;
        TMSMasterbagReponse tmsMasterbagReponse;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        String zipcode = "560068", wareHouseId = "36";
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR

        sortationHelper.computeAndPrintCompletePath(originHubCode, destinationHubCode, clientId, courierCode);

        orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Order Inscan V2
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, wareHouseId, false, LMS_CONSTANTS.TENANTID);
        System.out.println("response=" + inScanResponse);
        System.out.println("inScanResponse=");

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.ADDED_TO_MB);

        //Cancel API - AT DC
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.ADDED_TO_MB);

        //Continue TMS Operation
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        //Validate RTO CONfirmed
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SH);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(destinationHubCode, returnHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("Reverse masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        tmsOperations.createAndShipReturnContainer(masterBagId, destinationHubCode, returnHubCode, LaneType.INTRACITY, LMS_CONSTANTS.TENANTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_DISPATCHED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, destinationHubCode, returnHubCode);

        //Call Restock API

    }

    /**
     * Shipped from hub and not yet received in DC
     */
    @Test(description = "C21028", retryAnalyzer = Retry.class)
    public void CancelAfterShippContainer() throws Exception {
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        String inScanResponse;
        Long masterBagId;
        String clientId = LMS_CONSTANTS.CLIENTID;
        TMSMasterbagReponse tmsMasterbagReponse;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        String zipcode = "560068", wareHouseId = "36";
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR

        sortationHelper.computeAndPrintCompletePath(originHubCode, destinationHubCode, clientId, courierCode);

        orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Order Inscan V2
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, wareHouseId, false, LMS_CONSTANTS.TENANTID);
        System.out.println("response=" + inScanResponse);
        System.out.println("inScanResponse=");

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);

        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);

        //Cancel API - AT DC
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SHIPPED);

        //Continue TMS operations
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        //Validate RTO CONfirmed
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SH);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(destinationHubCode, returnHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("Reverse masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        tmsOperations.createAndShipReturnContainer(masterBagId, destinationHubCode, returnHubCode, LaneType.INTRACITY, LMS_CONSTANTS.TENANTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_DISPATCHED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, destinationHubCode, returnHubCode);

        //Call Restock API
    }

    @Test(description = "C21029 , C21030, C21031 C21194 - Received Container At Transport Hub ", retryAnalyzer = Retry.class)
    public void CancelAtRecieveContainer() throws Exception {
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        String inScanResponse;
        Long masterBagId;
        String clientId = LMS_CONSTANTS.CLIENTID;
        TMSMasterbagReponse tmsMasterbagReponse;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        String zipcode = "560068", wareHouseId = "36";
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR

        sortationHelper.computeAndPrintCompletePath(originHubCode, destinationHubCode, clientId, courierCode);

        orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Order Inscan V2
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, wareHouseId, false, LMS_CONSTANTS.TENANTID);
        System.out.println("response=" + inScanResponse);
        System.out.println("inScanResponse=");

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);

        //Cancel API - AT DC
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SHIPPED);

        //Continue TMS Operations
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        //Validate RTO CONfirmed
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SH);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(destinationHubCode, returnHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("Reverse masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        tmsOperations.createAndShipReturnContainer(masterBagId, destinationHubCode, returnHubCode, LaneType.INTRACITY, LMS_CONSTANTS.TENANTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_DISPATCHED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, destinationHubCode, returnHubCode);

        //Call Restock API
    }

    @Test(description = "C21025 , C21030 C21194 - Received MasterBag At Transport Hub ", retryAnalyzer = Retry.class)
    public void CancelAtRecieveMasterBagAtDC() throws Exception {
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        String inScanResponse;
        Long masterBagId;
        String clientId = LMS_CONSTANTS.CLIENTID;
        TMSMasterbagReponse tmsMasterbagReponse;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        String zipcode = "560068", wareHouseId = "36";
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR

        sortationHelper.computeAndPrintCompletePath(originHubCode, destinationHubCode, clientId, courierCode);

        orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Order Inscan V2
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, wareHouseId, false, LMS_CONSTANTS.TENANTID);
        System.out.println("response=" + inScanResponse);
        System.out.println("inScanResponse=");

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);

        //Cancel API - AT DC
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SHIPPED);

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        //Validate RTO CONfirmed
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SH);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(destinationHubCode, returnHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("Reverse masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        tmsOperations.createAndShipReturnContainer(masterBagId, destinationHubCode, returnHubCode, LaneType.INTRACITY, LMS_CONSTANTS.TENANTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_DISPATCHED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, destinationHubCode, returnHubCode);

        //Call Restock API
    }

    @Test(description = "C21031, C21061, C21058, C21161", retryAnalyzer = Retry.class)
    public void RecieveShipmentsInDCandCancel() throws Exception {
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        String inScanResponse;
        Long masterBagId;
        String clientId = LMS_CONSTANTS.CLIENTID;
        TMSMasterbagReponse tmsMasterbagReponse;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        String zipcode = "560068", wareHouseId = "36";
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR

        sortationHelper.computeAndPrintCompletePath(originHubCode, destinationHubCode, clientId, courierCode);

        orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Order Inscan V2
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, wareHouseId, false, LMS_CONSTANTS.TENANTID);
        System.out.println("response=" + inScanResponse);
        System.out.println("inScanResponse=");

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //MasterBag In-scan V2 - Recieve Shipment in DC
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, tenantId);
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        while (shipmentEntryItr.hasNext()) {
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), shipmentEntry.getOrderShipmentAssociationEntries().get(0).getTrackingNumber(),
                    shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId(),
                    ShipmentType.DL, PremisesType.DC);
        }
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.UNASSIGNED.toString());

        //Cancel API - AT DC
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SH);

        //Validate RTO CONfirmed
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SH);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());

        //Last Mile Operations
        long tripId;
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        System.out.println(lmsOperations.getDeliveryCenterID(zipcode));
        tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(zipcode)).get("tripId"));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Cancelled/RTO CONFIRMED Shipment should not be allowed to add to Trip\n" + tripOrderAssignmentResponse);
        System.out.println("");

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(destinationHubCode, returnHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("Reverse masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        tmsOperations.createAndShipReturnContainer(masterBagId, destinationHubCode, returnHubCode, LaneType.INTRACITY, LMS_CONSTANTS.TENANTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_DISPATCHED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, destinationHubCode, returnHubCode);

        //Call Restock API
    }

    //Exchange Orders

    @Test(dataProvider = "CancelONPackedState", dataProviderClass = CancellationDataProvider.class,
            description = "C21234", retryAnalyzer = Retry.class)
    public void CancelAfterPackedState_Exchange(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        //already persent in Sanity case - will move it to here later
        String orderId, packetId, trackingNumber;
        MasterbagUpdateResponse closeMBResponse;
        TMSMasterbagReponse tmsMasterbagReponse;
        MasterbagDomain mbCreateResponse;
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560068", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        Long masterBagId;
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR
        orderId = lmsHelper.createMockOrder(ReleaseStatus.DL.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);

        String exchangeOrder = lms_createOrder.createMockOrderExchange(releaseStatus.toString(), zipcode, courierCode, wareHouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrder);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, packetId, trackingNumber));
        System.out.println(String.format("exchangeOrder:[%s], packetId:[%s], trackingNumber:[%s]", exchangeOrder, exchangePacketId, exchangeTrackingNumber));

        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(exchangePacketId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(exchangePacketId, "IC");
        orderValidator.validateShipmentOrderStatus(exchangePacketId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(exchangeTrackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(exchangeTrackingNumber, releaseStatus);
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, ShipmentStatus.CANCELLED.toString());

        //Order Inscan V2
        String orderInScanResponse= lmsOperations.orderInscanV2(exchangeTrackingNumber, wareHouseId, false);
        orderValidator.validateCancelledShipmentOrderInscan(orderInScanResponse, ShipmentStatus.CANCELLED, originHubCode, returnHubCode);
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, ShipmentStatus.CANCELLED.toString());
        //orderValidator.validateShipmentOrderDestinationHub(packetId, returnHubCode);

        //MasterBag V2 -- Adding Cancelled shipments to Forward bag should Fail
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, exchangeTrackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), StatusType.ERROR.toString(), "Adding Cancelled shipment to forward MasterBag should not be allowed");
        Assert.assertEquals(addShipmentToMbResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());

        //Restock here
    }

    @Test(dataProvider = "CancelONInsanncedShipment", dataProviderClass = CancellationDataProvider.class,
            retryAnalyzer = Retry.class)
    public void CancelAfterInscannedState_Exchange(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String orderId, packetId, trackingNumber;
        MasterbagUpdateResponse closeMBResponse;
        TMSMasterbagReponse tmsMasterbagReponse;
        MasterbagDomain mbCreateResponse;
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560068", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        Long masterBagId;
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR
        orderId = lmsHelper.createMockOrder(ReleaseStatus.DL.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);

        String exchangeOrder = lms_createOrder.createMockOrderExchange(releaseStatus.toString(), zipcode, courierCode, wareHouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrder);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, packetId, trackingNumber));
        System.out.println(String.format("exchangeOrder:[%s], packetId:[%s], trackingNumber:[%s]", exchangeOrder, exchangePacketId, exchangeTrackingNumber));

        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(exchangePacketId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(exchangePacketId, "IC");
        orderValidator.validateShipmentOrderStatus(exchangePacketId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(exchangeTrackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(exchangeTrackingNumber, releaseStatus);
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, ShipmentStatus.CANCELLED.toString());

        //MasterBag V2 -- Adding Cancelled shipments to Forward bag should Fail
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, exchangeTrackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), StatusType.ERROR.toString(), "Adding Cancelled shipment to forward MasterBag should not be allowed");
        Assert.assertEquals(addShipmentToMbResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());
        Assert.assertEquals(masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), StatusType.ERROR.toString());
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, ShipmentStatus.CANCELLED.toString());
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, packetId, trackingNumber));
        System.out.println(String.format("exchangeOrder:[%s], packetId:[%s], trackingNumber:[%s]", exchangeOrder, exchangePacketId, exchangeTrackingNumber));

        //Call Restock API
    }


    @Test(description = "C21024", dataProvider = "CancelONAddedToMasterBag", dataProviderClass = CancellationDataProvider.class,
            retryAnalyzer = Retry.class)
    public void CancelAfterAddedToMasterBagState_Exchange(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String orderId, packetId, trackingNumber;
        MasterbagUpdateResponse closeMBResponse;
        TMSMasterbagReponse tmsMasterbagReponse;
        MasterbagDomain mbCreateResponse;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipcode = "560068", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        Long masterBagId;
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR
        orderId = lmsHelper.createMockOrder(ReleaseStatus.DL.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(releaseStatus.toString(), zipcode, courierCode, wareHouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrder);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, packetId, trackingNumber));
        System.out.println(String.format("exchangeOrder:[%s], packetId:[%s], trackingNumber:[%s]", exchangeOrder, exchangePacketId, exchangeTrackingNumber));

        Map<String, Object> shipmentOrderMap = DBUtilities.exSelectQueryForSingleRecord("SELECT shipment_id FROM shipment_order_map where tracking_Number = '" + exchangeTrackingNumber + "';", "myntra_lms");
        masterBagId = Long.parseLong(shipmentOrderMap.get("shipment_id").toString());
        System.out.println("Forward MasterBAgId="+masterBagId);
        orderValidator.validateOrderIsCancellable(exchangePacketId, releaseStatus);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.NEW);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(exchangePacketId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(exchangePacketId, "IC");
        orderValidator.validateShipmentOrderStatus(exchangePacketId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(exchangeTrackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(exchangeTrackingNumber, releaseStatus);
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, ShipmentStatus.CANCELLED.toString());

        //Close MasterBag
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(closeMBResponse.getStatus().getStatusType().toString(), StatusType.SUCCESS.toString());

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //Recieve Cancelled order in DC
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, ShipmentStatus.CANCELLED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        //Validate RTO CONfirmed
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(exchangePacketId), ReleaseStatus.SH);
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus, orderId, exchangePacketId, trackingNumber));

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(destinationHubCode, returnHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("Reverse masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, exchangeTrackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        tmsOperations.createAndShipReturnContainer(masterBagId, destinationHubCode, returnHubCode, LaneType.INTRACITY, LMS_CONSTANTS.TENANTID);
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, ShipmentStatus.RTO_DISPATCHED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, destinationHubCode, returnHubCode);
        System.out.println(String.format("exchangeOrder:[%s], packetId:[%s], trackingNumber:[%s]", exchangeOrder, exchangePacketId, exchangeTrackingNumber));

        //Call Restock API
    }


    @Test(description = "C21025 , C21030 C21194 - Received MasterBag At Transport Hub ", retryAnalyzer = Retry.class)
    public void CancelAtRecieveMasterBagAtDC_Exchange() throws Exception {
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        String inScanResponse;
        Long masterBagId;
        String clientId = LMS_CONSTANTS.CLIENTID;
        TMSMasterbagReponse tmsMasterbagReponse;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        String zipcode = "560068", wareHouseId = "36";
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR

        sortationHelper.computeAndPrintCompletePath(originHubCode, destinationHubCode, clientId, courierCode);

        orderId = lmsHelper.createMockOrder(EnumSCM.DL, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.IS, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
        String exchangePacketId = omsServiceHelper.getPacketId(exchangeOrder);
        String exchangeTrackingNumber = lmsHelper.getTrackingNumber(exchangePacketId);

        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", EnumSCM.PK, orderId, packetId, trackingNumber));
        System.out.println(String.format("exchangeOrder:[%s], packetId:[%s], trackingNumber:[%s]", exchangeOrder, exchangePacketId, exchangeTrackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(exchangePacketId, true);

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, exchangeTrackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        orderValidator.validateShipmentOrderStatus(exchangePacketId, ShipmentStatus.SHIPPED);

        //Cancel API - AT DC
        orderValidator.validateIsCancelableFlagSetforOrder(exchangePacketId, true);
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(exchangePacketId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(exchangePacketId, "IC");
        orderValidator.validateShipmentOrderStatus(exchangePacketId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(exchangePacketId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, ShipmentStatus.CANCELLED.toString());
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(exchangePacketId), ReleaseStatus.SHIPPED);

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        //Validate RTO CONfirmed
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(exchangePacketId), ReleaseStatus.SH);
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(destinationHubCode, returnHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("Reverse masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, exchangeTrackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        tmsOperations.createAndShipReturnContainer(masterBagId, destinationHubCode, returnHubCode, LaneType.INTRACITY, LMS_CONSTANTS.TENANTID);
        orderValidator.validateMLShipmentStatus(exchangeTrackingNumber, ShipmentStatus.RTO_DISPATCHED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, destinationHubCode, returnHubCode);
        System.out.println(String.format("exchangeOrder:[%s], packetId:[%s], trackingNumber:[%s]", exchangeOrder, exchangePacketId, exchangeTrackingNumber));

        //Call Restock API
    }

}
