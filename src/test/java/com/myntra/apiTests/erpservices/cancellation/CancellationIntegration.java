package com.myntra.apiTests.erpservices.cancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.erpservices.LMSCancellation.CancelationHelper;
import com.myntra.apiTests.erpservices.LMSCancellation.validators.OrderValidator;
import com.myntra.apiTests.erpservices.cancellation.dp.CancellationDataProvider;
import com.myntra.apiTests.erpservices.lastmile.client.TripOrderAssignmentClient;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSHelpersEnums;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.oms.client.response.PacketResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class CancellationIntegration {

    String env = getEnvironment();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    OrderValidator orderValidator = new OrderValidator();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    SortationHelper sortationHelper = new SortationHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TMSOperations tmsOperations = new TMSOperations();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    SortationValidator sortationValidator = new SortationValidator();
    CancelationHelper cancelationHelper = new CancelationHelper();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);


    @Test(enabled = true)
    public void cancelUsingOMSapi() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        PacketResponse packetResponse = omsServiceHelper.cancelPacket(packetId, OMSHelpersEnums.PacketEvents.CANCEL_PACKET);
        System.out.println(packetResponse);
    }

    @Test(dataProvider = "IntegrationForwardOrderStatus", dataProviderClass = CancellationDataProvider.class,
            description = "ID:[C21033, C21034, C21035, C21038, C21055]")
    public void CancellationPointsForwardIntegration(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipcode = "560068", wareHouseId = "36";
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus,  orderId, packetId, trackingNumber));
        orderValidator.validateOrderIsCancellable(packetId, releaseStatus);
        if (releaseStatus == ReleaseStatus.SH) {
            orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        }
        //Cancel API
        PacketResponse packetResponse = omsServiceHelper.cancelPacket(packetId, OMSHelpersEnums.PacketEvents.CANCEL_PACKET);
        //orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        if(!(releaseStatus== ReleaseStatus.OFD || releaseStatus== ReleaseStatus.DL)) {
            orderValidator.validatePacketStatusInOMS(packetId, "IC");
            orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, releaseStatus);
        }
        if(releaseStatus== ReleaseStatus.DL)
            orderValidator.validatePacketStatusInOMS(packetId, "D");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus,  orderId, packetId, trackingNumber));
    }

}
