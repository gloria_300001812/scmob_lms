package com.myntra.apiTests.erpservices.cancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.LMSCancellation.CancelationHelper;
import com.myntra.apiTests.erpservices.LMSCancellation.validators.OrderValidator;
import com.myntra.apiTests.erpservices.cancellation.dp.CancellationDataProvider;
import com.myntra.apiTests.erpservices.lastmile.client.TripOrderAssignmentClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripOrderAssignmentClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.HubToTransportHubConfigResponse;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.apiTests.erpservices.lms.tests.TripOrderAssignmentResponseValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.cms.pim.dto.StatusType;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.*;
import com.myntra.test.commons.testbase.BaseTest;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import edu.emory.mathcs.backport.java.util.Arrays;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class CancellationSanity extends BaseTest {
    String env = getEnvironment();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    OrderValidator orderValidator = new OrderValidator();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    SortationHelper sortationHelper = new SortationHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TMSOperations tmsOperations = new TMSOperations();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    SortationValidator sortationValidator = new SortationValidator();
    CancelationHelper cancelationHelper = new CancelationHelper();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripClient_QA tripClient_qa=new TripClient_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa=new TripOrderAssignmentClient_QA();


    @BeforeClass
    public void setup() throws Exception {
        cancelationHelper.disableSorterOnHub("DH-BLR");
        cancelationHelper.disableSorterOnHub("DH-DEL");
    }

    @Test(dataProvider = "ForwardOrderStatus", dataProviderClass = CancellationDataProvider.class,
            description = "ID:[C21033, C21034, C21035, C21038, C21055]",
            retryAnalyzer = Retry.class,
            priority = 4,
    enabled = true)
    public void CancellationPointsForward(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipcode = "560068", wareHouseId = "36";
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus,  orderId, packetId, trackingNumber));
        orderValidator.validateOrderIsCancellable(packetId, releaseStatus);
        if (releaseStatus == ReleaseStatus.SH) {
            orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        }
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, releaseStatus);
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus,  orderId, packetId, trackingNumber));
    }

    @Test(dataProvider = "TryAndBuyOrderStatus", dataProviderClass = CancellationDataProvider.class,
            description = "ID:[C21033, C21034, C21035, C21038, C21055]",
            retryAnalyzer = Retry.class,
            enabled = true)
    public void CancellationPointsTryAndBuy(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipcode = "560068", wareHouseId = "36";
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", true, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateOrderIsCancellable(packetId, releaseStatus);
        if (releaseStatus == ReleaseStatus.SH) {
            orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        }
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, releaseStatus);
        System.out.println(String.format("releaseStatus[%s], orderId:[%s], packetId:[%s], trackingNumber:[%s]",releaseStatus, orderId, packetId, trackingNumber));
    }

    @Test(dataProvider = "ExchangeOrderStatus", dataProviderClass = CancellationDataProvider.class,
            description = "ID:[C21019, C21018, C21033, C21034, C21035, C21038, C21055, C21239]"
    ,retryAnalyzer = Retry.class,
            enabled = true)
    public void CancellationPointsExchange(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String orderId, packetId = null, trackingNumber = null;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipcode = "560068", wareHouseId = "36";
        String exchangeOrder = null;
        orderId = lmsHelper.createMockOrder(EnumSCM.DL, zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        if(releaseStatus!= ReleaseStatus.DL) {
            exchangeOrder = lms_createOrder.createMockOrderExchange(releaseStatus.toString(), LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
            packetId = omsServiceHelper.getPacketId(exchangeOrder);
            trackingNumber = lmsHelper.getTrackingNumber(packetId);
        }else{
            //Since Exchange order PK to DL is not added in createMockOrderExchange. Adding this step.
            exchangeOrder = lms_createOrder.createMockOrderExchange(ReleaseStatus.OFD.toString(), LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
            packetId = omsServiceHelper.getPacketId(exchangeOrder);
            trackingNumber = lmsHelper.getTrackingNumber(packetId);
            TripOrderAssignmentResponse tripDetails = tripClient_qa.getTripDetails(trackingNumber, LMS_CONSTANTS.TENANTID);
            String tripNumber = tripDetails.getTripOrders().get(0).getTripNumber();
            Long tripId = tripDetails.getTripOrders().get(0).getTripId();
            long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
            OrderEntry orderEntry = new OrderEntry();
            TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripNumber, LMS_CONSTANTS.TENANTID);
            TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
            tripOrderAssignmentResponseValidator.validateTripOrderStatus(EnumSCM.OFD);
            tripOrderAssignmentResponseValidator.validateTripId(tripId);
            tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNumber);
            tripOrderAssignmentResponseValidator.validateExchangeOrderId(packetId);
            Assert.assertEquals(tripOrderAssignmentResponse.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant Id in response");
            orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                    EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
            TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
            Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
            Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                    EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        }
        System.out.println(String.format("[%s] orderId:[%s], packetId:[%s], trackingNumber:[%s]",releaseStatus, orderId, packetId, trackingNumber));
        orderValidator.validateOrderIsCancellable(packetId, releaseStatus);
        if (releaseStatus == ReleaseStatus.SH)
            orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, releaseStatus);
        if(!(releaseStatus== ReleaseStatus.DL || releaseStatus== ReleaseStatus.OFD)) {
            orderValidator.validateExcahngeOrdersOnCancelation(trackingNumber);
            //Also validate status is  RTO of cancelled shipment
        }
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }

    @Test(description = "C21032, C21065", retryAnalyzer = Retry.class)
    public void OutScanForwardShipmentToTripAndAssign() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        ShipmentType shipmentType = ShipmentType.DL;
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.SH, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        //validate is_cancellable = false
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.toString());

        lastmileOperations.startTrip(tripId, trackingNumber);
        lastmileOperations.updateDeliveryTripByTrackingNumber(trackingNumber);
        lastmileOperations.completeTrip(trackingNumber);
        System.out.println(String.format("tripId:[%s]", tripId));
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }


    @Test(description = "C21032, C21065", retryAnalyzer = Retry.class)
    public void OutScanForwardTryAndBuyShipmentToTripAndAssign() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.SH, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", true, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        //validate is_cancellable = false
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.toString());

        Long deliveryCenterId = lmsOperations.getDeliveryCenterID(zipCode);
        lastmileOperations.startTrip(tripId, trackingNumber);
        lastmileOperations.updateTryAndBuyTrip(String.valueOf(tripId),
                Arrays.asList(new String[]{trackingNumber}),
                Arrays.asList(new String[]{packetId}),
                Arrays.asList(new String[]{orderId}),
                TryAndBuyItemStatus.TRIED_AND_BOUGHT, deliveryCenterId);
        lastmileOperations.completeTrip(trackingNumber);
        System.out.println(String.format("tripId:[%s]", tripId));
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }


    @Test(description = "C21032, C21065", retryAnalyzer = Retry.class)
    public void OutScanExchangeShipmentToTripAndAssign() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.DL, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
        packetId = omsServiceHelper.getPacketId(exchangeOrder);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        //validate is_cancellable = false as per the Event
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.toString());

        //Lastmile
        List<OrderEntry> orderEntries;
        lastmileOperations.startTrip(tripId, trackingNumber);
        orderEntries = lastmileOperations.setOperationalTrackingIds(Arrays.asList(new String[]{trackingNumber}));
        lastmileOperations.updateExchangeTrip(tripId, Arrays.asList(new String[]{packetId}), orderEntries);
        //Recieve Shipments
        lastmileOperations.recieveExchangeTripOrders(Arrays.asList(new String[]{trackingNumber}));
        //Trip Complete
        lastmileOperations.completeExchangeTrip(tripId, Arrays.asList(new String[]{packetId}), Arrays.asList(new String[]{orderId}));
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }


    @Test(description = "C21032", retryAnalyzer = Retry.class)
    public void OutScanForwardShipmentToTripAndUnassign() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        ShipmentType shipmentType = ShipmentType.TRY_AND_BUY;
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.SH, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        //validate is_cancellable = false
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        String tripOrder = Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.unassignOrderFromTrip(tripOrder);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.UNASSIGNED, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }

    @Test(description = "C21032", retryAnalyzer = Retry.class)
    public void OutScanForwardTryAndBuyShipmentToTripAndUnassign() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.SH, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", true, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        //validate is_cancellable = false
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        String tripOrder = Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.unassignOrderFromTrip(tripOrder);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.UNASSIGNED, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }


    @Test(description = "C21032", retryAnalyzer = Retry.class)
    public void OutScanExchangeShipmentToTripAndUnassign() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.DL, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
        packetId = omsServiceHelper.getPacketId(exchangeOrder);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //validate is_cancellable = false as per the Event
        String tripOrder = Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.unassignOrderFromTrip(tripOrder);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.UNASSIGNED, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }


    @Test(description = "C21032, C21065", retryAnalyzer = Retry.class)
    public void AssignForwardShipmentToTripUsingAssignAPI() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.SH, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.toString());

        lastmileOperations.startTrip(tripId, trackingNumber);
        lastmileOperations.updateDeliveryTripByTrackingNumber(trackingNumber);
        lastmileOperations.completeTrip(trackingNumber);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }


    @Test(description = "C21032, C21065", retryAnalyzer = Retry.class)
    public void AssignForwardTryAndBuyShipmentToTripUsingAssignAPI() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.SH, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", true, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.toString());

        Long deliveryCenterId = lmsOperations.getDeliveryCenterID(zipCode);
        lastmileOperations.startTrip(tripId, trackingNumber);
        lastmileOperations.updateTryAndBuyTrip(String.valueOf(tripId),
                Arrays.asList(new String[]{trackingNumber}),
                Arrays.asList(new String[]{packetId}),
                Arrays.asList(new String[]{orderId}),
                TryAndBuyItemStatus.TRIED_AND_BOUGHT, deliveryCenterId);
        lastmileOperations.completeTrip(trackingNumber);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }


    @Test(description = "C21032, C21065", retryAnalyzer = Retry.class)
    public void AssignExchangeShipmentToTripUsingAssignAPI() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        Map<String, Object> returnData;
        List<String> exchangeTrackingNumbers , returnTrackingNumbers,  exchangePacketIds,  exchangeOrderIds;
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.DL, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
        packetId = omsServiceHelper.getPacketId(exchangeOrder);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.ASSIGNED_TO_SDA.toString());
        //Lastmile
        List<OrderEntry> orderEntries;
        lastmileOperations.startTrip(tripId, trackingNumber);
        orderEntries = lastmileOperations.setOperationalTrackingIds(Arrays.asList(new String[]{trackingNumber}));
        returnData = lastmileOperations.updateExchangeTrip(tripId, Arrays.asList(new String[]{packetId}), orderEntries);
        //Recieve Shipments
        lastmileOperations.recieveExchangeTripOrders(Arrays.asList(new String[]{trackingNumber}));
        //Trip Complete
        lastmileOperations.completeExchangeTrip(tripId, Arrays.asList(new String[]{packetId}), Arrays.asList(new String[]{orderId}));
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }


    @Test(description = "C21032", retryAnalyzer = Retry.class)
    public void AssignForwardShipmentToTripAndUnAssign() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.SH, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        String tripOrder = Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.unassignOrderFromTrip(tripOrder);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.UNASSIGNED, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }

    @Test(description = "C21032", retryAnalyzer = Retry.class)
    public void AssignForwardTryAndBuyShipmentToTripAndUnAssign() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.SH, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", true, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        String tripOrder = Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.unassignOrderFromTrip(tripOrder);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.UNASSIGNED, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }


    @Test(description = "C21032", retryAnalyzer = Retry.class)
    public void AssignExchangeShipmentToTripAndUnAssign() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        orderId = lmsHelper.createMockOrder(EnumSCM.DL, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
        packetId = omsServiceHelper.getPacketId(exchangeOrder);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        System.out.println(String.format("deliveryStaffID:[%s] , tripId:[%s]", deliveryStaffID, tripId));
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        String tripOrder = Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.unassignOrderFromTrip(tripOrder);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.UNASSIGNED, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }


    @Test(dataProvider = "AddShipmenToStoreBag", dataProviderClass = CancellationDataProvider.class,
            retryAnalyzer = Retry.class)
    public void AssignForwardShipmentToStoreBag(ShipmentType shipmentType) throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        Boolean isTryAndBuy = (ShipmentType.TRY_AND_BUY == shipmentType) ? true : false;
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        ShipmentResponse storeMasterBagResponse = null;
        Long storeMasterBagId;
        Integer originPremisesId = 5;
        Integer storeHlPId = 6021;
        orderId = lmsHelper.createMockOrder(EnumSCM.SH, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", isTryAndBuy, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        //Create StoreBag
        storeMasterBagResponse = lastmileHelper.createMasterBag(originPremisesId, PremisesType.DC, storeHlPId, PremisesType.DC, com.myntra.lms.client.status.ShippingMethod.NORMAL, LMS_CONSTANTS.TENANTID, "Bangalore", "Bangalore");
        storeMasterBagId = storeMasterBagResponse.getEntries().get(0).getId();
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(storeMasterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();
        //masterBagClient.addShipmentToStoreBag(storeMasterBagId, shipmentUpdateInfo);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(storeMasterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }

    @Test(retryAnalyzer = Retry.class)
    public void AssignExchangeShipmentToStoreBag() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        ShipmentResponse storeMasterBagResponse = null;
        Long storeMasterBagId;
        Integer originPremisesId = 5;
        Integer storeHlPId = 6021;
        orderId = lmsHelper.createMockOrder(EnumSCM.DL, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
        packetId = omsServiceHelper.getPacketId(exchangeOrder);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        //Create StoreBag
        storeMasterBagResponse = lastmileHelper.createMasterBag(originPremisesId, PremisesType.DC, storeHlPId, PremisesType.DC, com.myntra.lms.client.status.ShippingMethod.NORMAL, LMS_CONSTANTS.TENANTID, "Bangalore", "Bangalore");
        storeMasterBagId = storeMasterBagResponse.getEntries().get(0).getId();
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(storeMasterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();
        //masterBagClient.addShipmentToStoreBag(storeMasterBagId, shipmentUpdateInfo);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(storeMasterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.ASSIGNED_TO_LAST_MILE_PARTNER.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }

    @Test(dataProvider = "AddAndRemoveShipmenToStoreBag", dataProviderClass = CancellationDataProvider.class,
            retryAnalyzer = Retry.class)
    public void AddAndRemoveForwardShipmentFromStoreBag(ShipmentType shipmentType) throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        Boolean isTryAndBuy = (ShipmentType.TRY_AND_BUY == shipmentType) ? true : false;
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        ShipmentResponse storeMasterBagResponse = null;
        Long storeMasterBagId;
        Integer originPremisesId = 5;
        Integer storeHlPId = 6021;
        orderId = lmsHelper.createMockOrder(EnumSCM.SH, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", isTryAndBuy, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        //Create StoreBag
        storeMasterBagResponse = lastmileHelper.createMasterBag(originPremisesId, PremisesType.DC, storeHlPId, PremisesType.DC, com.myntra.lms.client.status.ShippingMethod.NORMAL, LMS_CONSTANTS.TENANTID, "Bangalore", "Bangalore");
        storeMasterBagId = storeMasterBagResponse.getEntries().get(0).getId();
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(storeMasterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();
        //masterBagClient.addShipmentToStoreBag(storeMasterBagId, shipmentUpdateInfo);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(storeMasterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        masterBagServiceHelper.removeShipmentsFromMasterBag(storeMasterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.UNASSIGNED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }

    @Test(retryAnalyzer = Retry.class)
    public void AddAndRemoveExchangeShipmentFromStoreBag() throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipCode = "560068", wareHouseId = "36";
        long DcId = lmsOperations.getDeliveryCenterID(zipCode);
        ShipmentResponse storeMasterBagResponse = null;
        Long storeMasterBagId;
        Integer originPremisesId = 5;
        Integer storeHlPId = 6021;
        orderId = lmsHelper.createMockOrder(EnumSCM.DL, zipCode, courierCode, wareHouseId, ShippingMethod.NORMAL.toString(),
                "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderId));
        packetId = omsServiceHelper.getPacketId(exchangeOrder);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        //Lastmile operations
        //Create StoreBag
        storeMasterBagResponse = lastmileHelper.createMasterBag(originPremisesId, PremisesType.DC, storeHlPId, PremisesType.DC, com.myntra.lms.client.status.ShippingMethod.NORMAL, LMS_CONSTANTS.TENANTID, "Bangalore", "Bangalore");
        storeMasterBagId = storeMasterBagResponse.getEntries().get(0).getId();
        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(storeMasterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();
        //masterBagClient.addShipmentToStoreBag(storeMasterBagId, shipmentUpdateInfo);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(storeMasterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        masterBagServiceHelper.removeShipmentsFromMasterBag(storeMasterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
        orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL,courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.UNASSIGNED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }

    @Test(description = "C21056", enabled = false, retryAnalyzer = Retry.class)
    public void RestockCancelledOrder() throws Exception {
        ReleaseStatus releaseStatus = ReleaseStatus.PK;
        ShipmentStatus expectedStatus;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipcode = "560068", wareHouseId = "36";
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        if (releaseStatus == ReleaseStatus.SH) {
            orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        }
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        //orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, courierCode);
        orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, releaseStatus);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

//        RejoyServiceHelper rejoyServiceHelper = new RejoyServiceHelper();
//        RejoyServiceHelper.recieveShipmentInRejoy(packetId);
//        rejoyServiceHelper.reStockItemInRejoy()

        Assert.assertEquals(
                rmsServiceHelper.recieveShipmentInRejoy(packetId, 36).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
        Assert.assertTrue(
                lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.RECEIVED_IN_RETURNS_HUB, 3));
    }

    /**
     * EXPECTED_IN_DC -> CANCELLED -> Recive in DC -> RTO CONFIRMED
     * Cancellad after TMS recieve MB
     */
    @Test(description = "C21026, C21061, C21160", retryAnalyzer = Retry.class)
    public void recieveCancelledShipmentInDC() throws Exception {
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        String inScanResponse;
        Long masterBagId;
        String clientId = LMS_CONSTANTS.CLIENTID;
        TMSMasterbagReponse tmsMasterbagReponse;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        String zipcode = "560068", wareHouseId = "36";
        long laneId = 13, transporterId = 1; //LMS_PINCODE.ML_BLR

        sortationHelper.computeAndPrintCompletePath(originHubCode, destinationHubCode, clientId, courierCode);

        orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);

        //Order Inscan V2
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, wareHouseId, false, LMS_CONSTANTS.TENANTID);
        System.out.println("response=" + inScanResponse);
        System.out.println("inScanResponse=");

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        //cancel here
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());

        //Recieve Cancelled order in DC
        //MasterBag In-scan V2
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, tenantId);
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        while (shipmentEntryItr.hasNext()) {
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), shipmentEntry.getOrderShipmentAssociationEntries().get(0).getTrackingNumber(),
                    shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId(),
                    ShipmentType.DL, PremisesType.DC);
        }

        //Validate RTO CONfirmed
        orderValidator.validateMLShipmentStatusOnCancel(lmsHelper.getTrackingNumber(packetId), ReleaseStatus.SH);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());

        //Last Mile Operations
        long tripId;
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        System.out.println(lmsOperations.getDeliveryCenterID(zipcode));
        tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(zipcode)).get("tripId"));
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Cancelled/RTO CONFIRMED Shipment should not be added to Trip\n" + tripOrderAssignmentResponse);
        System.out.println("");

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(destinationHubCode, returnHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("Reverse masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        tmsOperations.createAndShipReturnContainer(masterBagId, destinationHubCode, returnHubCode, LaneType.INTRACITY, LMS_CONSTANTS.TENANTID);
        orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_DISPATCHED.toString());

        //MasterBag In-scan V2
        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, destinationHubCode, returnHubCode);

        //Call Restock API
    }

    /**
     * Disable cancelation at hub level and cancelation of shipment should fail.
     * 	[Disable hub cancellation on Hub and try cancellation]
     */
    @Test(enabled = false, description = "C21072", retryAnalyzer = Retry.class)
    public void TestCancellationOnCancellationDisabledIntermediateHub() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(com.myntra.lms.client.status.ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 2;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2 DH-DEL
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Cancel API -- Cancelled
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);
        //Search Hub and it is_cancellation is false
        cancelationHelper.disableCancelationOnHub(currentHub);
        orderValidator.validateMLShipmentStatus(trackingNumbers, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());

        //Cancellation at hub
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);
        for(String packetId : packetIds) {
            String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
            orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
            orderValidator.validatePacketStatus(packetId, "IC");
            orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
            orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            orderValidator.validateMLShipmentStatus(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED.toString());
        }
        //process RTO
        cancelationHelper.enableCancelationOnHub(currentHub);
    }

    //disabling this as it will disable cancellation on HUB
    @Test(enabled = false, description = "", retryAnalyzer = Retry.class)
    public void TestCancellationOnCancellationDisabledSourceHub() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(com.myntra.lms.client.status.ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        try {
            //Disable is_cancellation is false on Source Hub
            cancelationHelper.disableCancelationOnHub(orderData.getOriginHubCode());

            sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
            orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
            orderData.setOrderMap(orderMap);
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            orderIds = (List<String>) orderMap.get("orderIds");
            System.out.println(String.format("currentHub=%s , destinationHub=%s", currentHub, nextHub));
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            orderValidator.validateIsCancelableFlagSetforOrder(packetIds, false);

            //Order Inscan V2 & Sortation
            nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
            orderData.setNextHub(nextHub);
            System.out.println(String.format("currentHub=%s , nextHub=%s", currentHub, nextHub));
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

            //Cancellation at hub
            orderValidator.validateIsCancelableFlagSetforOrder(packetIds, false);
            for (String packetId : packetIds) {
                String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
                orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED, "Shipment Cancelation should not be allowed at this point, as hub config is disabled");
                orderValidator.validatePacketStatus(packetId, "S");
                orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.INSCANNED);
                orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.INSCANNED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
                orderValidator.validateMLShipmentStatus(lmsHelper.getTrackingNumber(packetId), MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
            }

            //MasterBag V2
            masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
            sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(), nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

            //TMS Operations
            tmsOperations.createAndShipForwardContainer(masterBagId, orderData, nextHub, LaneType.INTERCITY);
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

            //save current node (origin at current hop)
            currentHub = nextHub;
            orderData.setCurrentHub(currentHub);

            //MasterBag In-scan V2 DH-DEL
            nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
            orderData.setNextHub(nextHub);
            System.out.println(String.format("currentHub=%s , nextHub=%s", currentHub, nextHub));
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
            orderValidator.validateMLShipmentStatus(trackingNumbers, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());

            //Cancellation at hub - Even on enabled hub cancellation is not allowed, as we consider only source hub
            orderValidator.validateIsCancelableFlagSetforOrder(packetIds, false);
            for (String packetId : packetIds) {
                String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
                orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED, "Shipment Cancelation should not be allowed at this point, as hub config is disabled");
                orderValidator.validatePacketStatus(packetId, "S");
                orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.SHIPPED);
                orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.SHIPPED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
                orderValidator.validateMLShipmentStatus(lmsHelper.getTrackingNumber(packetId), MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
            }

            //MasterBag V2
            masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
            sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(), nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

            //TMS Operations
            tmsOperations.createAndShipForwardContainer(masterBagId, orderData, nextHub, LaneType.INTRACITY);
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

            //MasterBag In-scan V2
            lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

            //Last Mile Operations
            lastmileOperations.deliverOrders(orderData.getZipcode(), trackingNumbers);
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
            System.out.println("trackingNumbers:" + trackingNumbers);

        }finally {
            //Re enable is_cancellation is false on Source Hub
            cancelationHelper.enableCancelationOnHub(orderData.getOriginHubCode());
        }
    }


    @Test(description = "C21060", retryAnalyzer = Retry.class)
    public void TestCancelledShipmentToForwardMasterBag() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(com.myntra.lms.client.status.ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 2;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2 DH-DEL
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Cancel API -- Cancelled
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);
        HubResponse hubResponse = lmsServiceHelper.searchHubByCode(currentHub);
        Boolean isCancellationAllowed = hubResponse.getHub().get(0).getAllowsCancellation();
        if(!isCancellationAllowed){
            System.out.println("isCancellationAllowed:"+isCancellationAllowed);
            HubEntry hubEntry = new HubEntry();
            hubEntry.setAllowsCancellation(true);
            HubResponse editHubResponse = lmsServiceHelper.editHub(hubResponse.getHub().get(0).getId(), hubEntry);
            Assert.assertEquals(editHubResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString(), "hub Edit Failed");
            hubResponse = lmsServiceHelper.searchHubByCode(currentHub);
            isCancellationAllowed = hubResponse.getHub().get(0).getAllowsCancellation();
            Assert.assertTrue(isCancellationAllowed, "Edit hub Failed");
        }

        for(String packetId : packetIds) {
            orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
            String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
            orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
            orderValidator.validatePacketStatus(packetId, "IC");
            orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
            orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            orderValidator.validateMLShipmentStatus(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED.toString());
        }

        //MasterBag V2 -- Adding Cancelled shipments to Forward bag should Fail
        MasterbagUpdateResponse masterbagUpdateResponse;
        mbCreateResponse = masterBagServiceHelper.createMasterBag(currentHub, nextHub, orderData.getShippingMethod(), orderData.getCourierCode(), orderData.getTenantId());
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, currentHub, nextHub, orderData.getShippingMethod(), orderData.getCourierCode(), orderData.getTenantId());
        masterBagId = mbCreateResponse.getId();
        System.out.println(String.format("Created MasterBag[%s] from %s to %s", masterBagId, currentHub, nextHub));
        for (String trackingNumber : trackingNumbers) {
            MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), StatusType.ERROR.toString(), "Adding Cancelled shipment to forward MasterBag should not be allowed");
            Assert.assertEquals(addShipmentToMbResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());
            //Error message has been changed , will update later with correct one
            //Assert.assertTrue(addShipmentToMbResponse.getErrorMessage().contains("Order is in CANCELLED state, cannot be added to master bag."));
            //Assert.assertTrue(addShipmentToMbResponse.getStatus().getStatusMessage().contains("Update to Masterbag masterBagId with event SHIPMENTS_ADDED failed.".replace("masterBagId", String.valueOf(masterBagId))));
        }
        //Process RTO - Reverse MB from here
    }

    @Test(retryAnalyzer = Retry.class)
    public void testEditHub() throws Exception {
        String hubCode = "MyHub";
        HubResponse hubResponse = lmsServiceHelper.searchHubByCode(hubCode);
        Boolean isCancellationAllowed = hubResponse.getHub().get(0).getAllowsCancellation();
        System.out.println(isCancellationAllowed);
        if(isCancellationAllowed){
            System.out.println("isCancellationAllowed:"+isCancellationAllowed);
            HubEntry hubEntry = new HubEntry();
            hubEntry.setAllowsCancellation(false);
            HubResponse editHubResponse = lmsServiceHelper.editHub(hubResponse.getHub().get(0).getId(), hubEntry);
            Assert.assertEquals(editHubResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString(), "hub Edit Failed");
            hubResponse = lmsServiceHelper.searchHubByCode(hubCode);
            isCancellationAllowed = hubResponse.getHub().get(0).getAllowsCancellation();
            Assert.assertFalse(isCancellationAllowed);
        }else{
            System.out.println("isCancellationAllowed:"+isCancellationAllowed);
            HubEntry hubEntry = new HubEntry();
            hubEntry.setAllowsCancellation(true);
            HubResponse editHubResponse = lmsServiceHelper.editHub(hubResponse.getHub().get(0).getId(), hubEntry);
            Assert.assertEquals(editHubResponse.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString(), "hub Edit Failed");
            hubResponse = lmsServiceHelper.searchHubByCode(hubCode);
            isCancellationAllowed = hubResponse.getHub().get(0).getAllowsCancellation();
            Assert.assertTrue(isCancellationAllowed);
        }
    }
}
