package com.myntra.apiTests.erpservices.cancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.LMSCancellation.CancelationHelper;
import com.myntra.apiTests.erpservices.LMSCancellation.validators.OrderValidator;
import com.myntra.apiTests.erpservices.cancellation.dp.CancellationDataProvider;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.cms.pim.dto.StatusType;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentEntry;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.Premise;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponseCode;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.sortation.domain.SortLocationType;
import com.myntra.sortation.response.SortationConfigResponse;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import edu.emory.mathcs.backport.java.util.Arrays;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class CancellationGOR {
    String env = getEnvironment();
    LMSHelper lmsHelper = new LMSHelper();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    OrderValidator orderValidator = new OrderValidator();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LastmileOperations lastmileOperations = new LastmileOperations();
    GOR_Helper gorHelper = new GOR_Helper();
    CancelationHelper cancelationHelper = new CancelationHelper();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    TMSOperations tmsOperations = new TMSOperations();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();

    /**
     * IF is_sorter_enabled = 0:
     * After Inscan (Cancelled) -> cannot add to MasterBag
     * IF is_sorter_enabled = 1:
     * Cancellation can happen anywhere and Add to MB is allowed, but once it recieves in DC , it will be RTO
     */

    @BeforeClass
    public void setup() throws Exception {
        cancelationHelper.enableSorterOnHub("DH-BLR");
        cancelationHelper.disableSorterOnHub("DH-DEL");
        cancelationHelper.disableSorterOnHub("RHD");
    }

    @AfterClass
    public void tearDown() throws Exception {
        cancelationHelper.disableSorterOnHub("DH-BLR");
        cancelationHelper.disableSorterOnHub("DH-DEL");
        cancelationHelper.disableSorterOnHub("RHD");
    }

    @Test(description = "THis is GOR sanity Case. It enables GOR on the HUB and runs the sanity", retryAnalyzer = Retry.class)
    public void testGORSanityFlow() throws Exception {
        Integer consolidationBagId;
        long masterBagId;
        String zipcode = "560068", originHubCode = "DH-BLR", destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        TMSMasterbagReponse tmsMasterbagReponse;
        long laneId = 13, transporterId = 1;
        cancelationHelper.enableSorterOnHub(originHubCode);
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        Integer httpStatusCode = gorHelper.OrderInscan(originHubCode, Premise.PremiseType.HUB, packetId);
        Assert.assertTrue(httpStatusCode == 200, "GOR INSCAN Operation failed!");
        consolidationBagId = gorHelper.createAndGetConsolodationBagID(originHubCode, 1);
        System.out.println("consolidationBagId=" + consolidationBagId);
        httpStatusCode = gorHelper.addShipmentToConsolidationBagAndClose(consolidationBagId, packetId, destinationHubCode, shippingMethod.toString(), "ML");
        Assert.assertTrue(httpStatusCode == 200, "GOR: Add shipment to Consolidation Bag Operation failed.");
        masterBagId = Long.parseLong(lmsServiceHelper.getMasterBagIdForConsolidationBag.apply(consolidationBagId).toString());
        System.out.println("masterBagId=" + masterBagId);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        masterBagValidator.validateMultipleShipmentsInMasterBag(masterBagId, Arrays.asList(new String[]{trackingNumber}));
        masterBagValidator.validateMasterBagDetails(masterBagId, originHubCode, destinationHubCode, shippingMethod, courierCode, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //MasterBag In-scan V2 - Recieve Shipment in DC
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, tenantId);
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        while (shipmentEntryItr.hasNext()) {
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), shipmentEntry.getOrderShipmentAssociationEntries().get(0).getTrackingNumber(),
                    shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId(),
                    ShipmentType.DL, PremisesType.DC);
        }
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.UNASSIGNED.toString());
        //Last Mile Operations
        long tripId;
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        System.out.println(lmsOperations.getDeliveryCenterID(zipcode));
        tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(zipcode)).get("tripId"));
        lastmileOperations.assignOrderToTripByTrackingNumber(trackingNumber, tripId);
        lastmileOperations.startTrip(tripId, trackingNumber);
        lastmileOperations.updateDeliveryTripByTrackingNumber(trackingNumber);
        lastmileOperations.completeTrip(trackingNumber);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }


    @Test(description = "ID: C21143, C21144, C21240", retryAnalyzer = Retry.class)
    public void CancelAfterGORPacked() throws Exception {
        Integer consolidationBagId;
        long masterBagId;
        String zipcode = "560068";
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        TMSMasterbagReponse tmsMasterbagReponse;
        long laneId = 13, transporterId = 1;
        //cancelationHelper.enableSorterOnHub(originHubCode);
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //Cancel API -- Cancelled
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED.toString());

        //GOR Insan
        Integer httpStatusCode = gorHelper.OrderInscan(originHubCode, Premise.PremiseType.HUB, packetId);
        Assert.assertTrue(httpStatusCode == 200, "GOR INSCAN Operation failed!");

        //Proceed with Add to MB
        consolidationBagId = gorHelper.createAndGetConsolodationBagID(originHubCode, 1);
        System.out.println("consolidationBagId=" + consolidationBagId);
        httpStatusCode = gorHelper.addShipmentToConsolidationBagAndClose(consolidationBagId, packetId, destinationHubCode, shippingMethod.toString(), "ML");
        Assert.assertTrue(httpStatusCode == 200, "GOR: Add shipment to Consolidation Bag Operation failed.");
        masterBagId = Long.parseLong(lmsServiceHelper.getMasterBagIdForConsolidationBag.apply(consolidationBagId).toString());
        System.out.println("masterBagId=" + masterBagId);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        masterBagValidator.validateMultipleShipmentsInMasterBag(masterBagId, Arrays.asList(new String[]{trackingNumber}));
        masterBagValidator.validateMasterBagDetails(masterBagId, originHubCode, destinationHubCode, shippingMethod, courierCode, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //MasterBag In-scan V2 - Recieve Shipment in DC
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //Restock from here
    }


    @Test(description = "ID: C21143, C21144", retryAnalyzer = Retry.class)
    public void CancelAfterGORInscan() throws Exception {
        Integer consolidationBagId;
        long masterBagId;
        String zipcode = "560068";
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        TMSMasterbagReponse tmsMasterbagReponse;
        long laneId = 13, transporterId = 1;
        //cancelationHelper.enableSorterOnHub(originHubCode);
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        //GOR Insan
        Integer httpStatusCode = gorHelper.OrderInscan(originHubCode, Premise.PremiseType.HUB, packetId);
        Assert.assertTrue(httpStatusCode == 200, "GOR INSCAN Operation failed!");

        //Cancel API -- Cancelled
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED.toString());

        //Proceed with Add to MB
        consolidationBagId = gorHelper.createAndGetConsolodationBagID(originHubCode, 1);
        System.out.println("consolidationBagId=" + consolidationBagId);
        httpStatusCode = gorHelper.addShipmentToConsolidationBagAndClose(consolidationBagId, packetId, destinationHubCode, shippingMethod.toString(), "ML");
        Assert.assertTrue(httpStatusCode == 200, "GOR: Add shipment to Consolidation Bag Operation failed.");
        masterBagId = Long.parseLong(lmsServiceHelper.getMasterBagIdForConsolidationBag.apply(consolidationBagId).toString());
        System.out.println("masterBagId=" + masterBagId);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        masterBagValidator.validateMultipleShipmentsInMasterBag(masterBagId, Arrays.asList(new String[]{trackingNumber}));
        masterBagValidator.validateMasterBagDetails(masterBagId, originHubCode, destinationHubCode, shippingMethod, courierCode, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        containerResponse = (ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode);
        Assert.assertEquals(containerResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, containerResponse.getStatus().getStatusMessage());
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //MasterBag In-scan V2 - Recieve Shipment in DC
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //Restock from here
    }

    @Test(description = "ID: C21147, C21144", retryAnalyzer = Retry.class)
    public void CancelAfterGORAddToMB() throws Exception {
        Integer consolidationBagId;
        long masterBagId;
        String zipcode = "560068";
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        TMSMasterbagReponse tmsMasterbagReponse;
        long laneId = 13, transporterId = 1;
        //cancelationHelper.enableSorterOnHub(originHubCode);
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        Integer httpStatusCode = gorHelper.OrderInscan(originHubCode, Premise.PremiseType.HUB, packetId);
        Assert.assertTrue(httpStatusCode == 200, "GOR INSCAN Operation failed!");
        consolidationBagId = gorHelper.createAndGetConsolodationBagID(originHubCode, 1);
        System.out.println("consolidationBagId=" + consolidationBagId);
        httpStatusCode = gorHelper.addShipmentToConsolidationBagAndClose(consolidationBagId, packetId, destinationHubCode, shippingMethod.toString(), "ML");
        Assert.assertTrue(httpStatusCode == 200, "GOR: Add shipment to Consolidation Bag Operation failed.");
        masterBagId = Long.parseLong(lmsServiceHelper.getMasterBagIdForConsolidationBag.apply(consolidationBagId).toString());
        System.out.println("masterBagId=" + masterBagId);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        masterBagValidator.validateMultipleShipmentsInMasterBag(masterBagId, Arrays.asList(new String[]{trackingNumber}));
        masterBagValidator.validateMasterBagDetails(masterBagId, originHubCode, destinationHubCode, shippingMethod, courierCode, LMS_CONSTANTS.TENANTID);

        //Cancel API -- Cancelled
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //MasterBag In-scan V2 - Recieve Shipment in DC
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //Restock from here
    }


    @Test(description = "ID: C21145", retryAnalyzer = Retry.class)
    public void CancelledShipmentToAddToForwardMB() throws Exception {
        Integer consolidationBagId;
        long masterBagId;
        String zipcode = "560068";
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", returnHubCode = "RT-BLR";
        String courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        TMSMasterbagReponse tmsMasterbagReponse;
        long laneId = 13, transporterId = 1;
        //cancelationHelper.enableSorterOnHub(originHubCode);
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        Integer httpStatusCode = gorHelper.OrderInscan(originHubCode, Premise.PremiseType.HUB, packetId);
        Assert.assertTrue(httpStatusCode == 200, "GOR INSCAN Operation failed!");
        //Cancel API -- Cancelled
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
        orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatus(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED.toString());
        //Add to MB
        consolidationBagId = gorHelper.createAndGetConsolodationBagID(originHubCode, 1);
        System.out.println("consolidationBagId=" + consolidationBagId);
        httpStatusCode = gorHelper.addShipmentToConsolidationBagAndClose(consolidationBagId, packetId, destinationHubCode, shippingMethod.toString(), "ML");
        Assert.assertTrue(httpStatusCode == 200, "GOR: Add shipment to Consolidation Bag Operation failed.");
        String statusType = APIUtilities.getElement(lmsServiceHelper.getConsolidationBagDetails.apply(consolidationBagId).toString(), "shipmentResponse.status.statusType", "json");
        Assert.assertEquals(statusType, "SUCCESS", "Cancelled Shipment should be allowed to Add to MasterBag");
        masterBagId = Long.parseLong(lmsServiceHelper.getMasterBagIdForConsolidationBag.apply(consolidationBagId).toString());
        System.out.println("masterBagId=" + masterBagId);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        masterBagValidator.validateMultipleShipmentsInMasterBag(masterBagId, Arrays.asList(new String[]{trackingNumber}));
        masterBagValidator.validateMasterBagDetails(masterBagId, originHubCode, destinationHubCode, shippingMethod, courierCode, LMS_CONSTANTS.TENANTID);

        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = (HubToTransportHubConfigResponse) tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode=" + transportHubCode);
        tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        System.out.println("tmsMasterbagReponse=" + tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println("containerId=" + containerId);
        Thread.sleep(5000);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);

        //MasterBag In-scan V2 - Recieve Shipment in DC
        CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, destinationHubCode, returnHubCode);

        orderValidator.validateMLShipmentStatus(trackingNumber, MLDeliveryShipmentStatus.RTO_CONFIRMED.toString());
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));

        //Restock from here
    }


    /**
     * Path:- DH-BLR(GOR) -> DH-DEL -> DELHI-DC
     * <p>
     * Cancelled the shipment IN DH-BLR - Packed,
     * Added the shipment in to the forward masterBag.[Reverse masterbag is not Possible at DH-BLR].
     * Receive the shipment in DH-DEL. Here we can create reverse bag
     * Any status we can canceled the shipment till Receive in DC
     * cancelationHelper.enableSorterOnHub(originHubCode);
     */
    @Test(description = "C21149, C21162, C21204")
    public void CancelAtPackedState() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setRtoHubCode("RT-BLR");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        //Setup: DH-BLR(GOR) -> DH-DEL -> DELHI-DC
        cancelationHelper.enableSorterOnHub("DH-BLR");
        cancelationHelper.disableSorterOnHub("DH-DEL");

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Cancel API -- Cancelled at DH-DEL
        for (String packetId : packetIds) {
            orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
            String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
            orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
            orderValidator.validatePacketStatus(packetId, "IC");
            orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
            orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        }

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);


        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(orderData.getCurrentHub(), orderData.getNextHub(), ShippingMethod.NORMAL, orderData.getCourierCode(), LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        for(String trackingNumber : trackingNumbers) {
            masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        }
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2 - DH-DEL
        nextHub = CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, currentHub, orderData.getRtoHubCode());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(orderData.getCurrentHub(), nextHub, ShippingMethod.NORMAL, orderData.getCourierCode(), LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        for(String trackingNumber : trackingNumbers) {
            masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        }
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        System.out.println(String.format("trackingNumbers:[%s], packetIds:[%s], orderIds:[%s]",trackingNumbers, packetIds, orderIds));
    }

    /**
     * Path:- DH-BLR(GOR) -> DH-DEL -> DELHI-DC
     * <p>
     * Inscan the shipment in DH-BLR,
     * Cancelled the shipment IN DH-BLR,
     * Added the shipment in to the forward masterBag.[Reverse masterbag is not Possible].
     * Receive the shipment in DH-DEL. Here we can create reverse bag
     * Any status we can canceled the shipment till Receive in DC
     * cancelationHelper.enableSorterOnHub(originHubCode);
     */
    @Test(description = "C21149, C21162, C21204")
    public void CancelAtInscannedState() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setRtoHubCode("RT-BLR");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        //Setup: DH-BLR(GOR) -> DH-DEL -> DELHI-DC
        cancelationHelper.enableSorterOnHub("DH-BLR");
        cancelationHelper.disableSorterOnHub("DH-DEL");

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Cancel API -- Cancelled at DH-DEL
        for (String packetId : packetIds) {
            orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
            String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
            orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
            orderValidator.validatePacketStatus(packetId, "IC");
            orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
            orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        }

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(orderData.getCurrentHub(), orderData.getNextHub(), ShippingMethod.NORMAL, orderData.getCourierCode(), LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        for(String trackingNumber : trackingNumbers) {
            masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        }
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2 - DH-DEL
        nextHub = CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, currentHub, orderData.getRtoHubCode());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Process RTO and Restock

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(orderData.getCurrentHub(), nextHub, ShippingMethod.NORMAL, orderData.getCourierCode(), LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        for(String trackingNumber : trackingNumbers) {
            masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        }
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        System.out.println(String.format("trackingNumbers:[%s], packetIds:[%s], orderIds:[%s]",trackingNumbers, packetIds, orderIds));
    }



    /**
     * Path:- DH-BLR(GOR) -> DH-DEL(GOR) -> DELHI-DC
     */
    @Test(description = "C21150", retryAnalyzer = Retry.class, enabled = false)
    public void testGOROnAllHubs() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        String orderId, trackingNumber;
        Map<String, Object> orderMap;
        List<String> trackingNumbers, packetIds, orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortationConfigResponse nextSortationLocation;
        Integer consolidationBagId = null;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null, null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 2;

        try {
            //Enable GOR on both DH-BLR and DH-DEL
            cancelationHelper.enableSorterOnHub(orderData.getOriginHubCode());
            cancelationHelper.enableSorterOnHub("DH-DEL");

            sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
            orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
            orderData.setOrderMap(orderMap);
            trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
            packetIds = (List<String>) orderMap.get("packetIds");
            orderIds = (List<String>) orderMap.get("orderIds");
            System.out.println(String.format("currentHub=%s , destinationHub=%s", currentHub, nextHub));
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

            //NextsortLocation
            nextSortationLocation = sortationHelper.getNextSortLocation(trackingNumbers.get(0), orderData.getCurrentHub(), SortLocationType.EXTERNAL);
            nextHub = nextSortationLocation.getSortationConfigList().get(0).getSortLocation().getSourceReferenceCode();
            orderData.setNextHub(nextHub);

            //GOR Inscan
            for (String packetId : packetIds) {
                Integer httpStatusCode = gorHelper.OrderInscan(orderData.getOriginHubCode(), Premise.PremiseType.HUB, packetId);
                Assert.assertTrue(httpStatusCode == 200, "GOR INSCAN Operation failed!");
                consolidationBagId = gorHelper.createAndGetConsolodationBagID(orderData.getOriginHubCode(), 1);
                System.out.println("consolidationBagId=" + consolidationBagId);
            }
            //Add to Consolidation BAg
            Integer httpStatusCode = gorHelper.addShipmentsToConsolidationBagAndClose(consolidationBagId, packetIds, orderData.getNextHub(), orderData.getShippingMethod().toString(), "ML");
            Assert.assertTrue(httpStatusCode == 200, "GOR: Add shipment to Consolidation Bag Operation failed.");
            masterBagId = Long.parseLong(lmsServiceHelper.getMasterBagIdForConsolidationBag.apply(consolidationBagId).toString());
            System.out.println("masterBagId=" + masterBagId);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
            masterBagValidator.validateMultipleShipmentsInMasterBag(masterBagId, trackingNumbers);
            masterBagValidator.validateMasterBagDetails(masterBagId, orderData.getOriginHubCode(), orderData.getNextHub(), orderData.getShippingMethod(), orderData.getCourierCode(), LMS_CONSTANTS.TENANTID);

            //Cancel API -- Cancelled
            for (String packetId : packetIds) {
                orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
                String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
                orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
                orderValidator.validatePacketStatus(packetId, "IC");
                orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
                orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
                orderValidator.validateMLShipmentStatus(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED.toString());
            }

            //TMS Operations
            tmsOperations.createAndShipForwardContainer(masterBagId, orderData, nextHub, LaneType.INTERCITY);
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

            //save current node (origin at current hop)
            currentHub = nextHub;
            orderData.setCurrentHub(currentHub);

            //MasterBag In-scan V2

            nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
            orderData.setNextHub(nextHub);
            System.out.println(String.format("currentHub=%s , nextHub=%s", currentHub, nextHub));
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

            //MasterBag V2
            masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
            sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(), nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

            //TMS Operations
            tmsOperations.createAndShipForwardContainer(masterBagId, orderData, nextHub, LaneType.INTRACITY);
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

            //MasterBag In-scan V2
            lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

            //Last Mile Operations
            lastmileOperations.deliverOrders(orderData.getZipcode(), trackingNumbers);
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.DELIVERED, ShipmentUpdateEvent.DELIVERED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
            System.out.println("trackingNumbers:" + trackingNumbers);
        } finally {
            cancelationHelper.disableSorterOnHub("DH-DEL");
        }
    }



    //3PL cases - After Packed state -  NO CANCELLATON

    @Test(description = "C21202", dataProvider = "CancelON_PackedState", dataProviderClass = CancellationDataProvider.class,
            retryAnalyzer = Retry.class)
    public void Cancellation3Pl_Packed_State(ShipmentStatus expectedStatus, com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod shippingMethod) throws Exception {
        String courierCode = "EK", tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560069", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "EKART", returnHubCode = "RT-BLR";
        String orderId, packetId, trackingNumber;
        long masterBagId;
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse closeMBResponse;
        ReleaseStatus releaseStatus = ReleaseStatus.PK;
        cancelationHelper.enableSorterOnHub("DH-BLR");
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(), "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        CancelationHelper.SetCourierCreationStatusFor3PL(packetId);
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);

        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED, "Shipment Cancelation should not be allowed at this point, as hub config is disabled");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);

        //Order Inscan V2
        String orderInScanResponse= lmsOperations.orderInscanV2(trackingNumber, wareHouseId, false);
        sortationValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(closeMBResponse.getStatus().getStatusType().toString(), StatusType.SUCCESS.toString());

        //Ship MB
        lmsServiceHelper.shipMasterBag(masterBagId);
        // OFD
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.OUT_FOR_DELIVERY , courierCode ,ShipmentType.DL);
        // Deliver the 3pl Order
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.DELIVERED , courierCode ,ShipmentType.DL);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",  orderId, packetId, trackingNumber));
    }

}
