package com.myntra.apiTests.erpservices.cancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.erpservices.LMSCancellation.CancelationHelper;
import com.myntra.apiTests.erpservices.LMSCancellation.validators.OrderValidator;
import com.myntra.apiTests.erpservices.lastmile.client.TripClient;
import com.myntra.apiTests.erpservices.lastmile.client.TripOrderAssignmentClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripOrderAssignmentClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.tests.TripOrderAssignmentResponseValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.status.MLDeliveryShipmentStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.domain.response.ReturnResponse;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponseCode;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class CacellationFlows {
    String env = getEnvironment();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    OrderValidator orderValidator = new OrderValidator();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    CancelationHelper cancelationHelper = new CancelationHelper();
    TMSOperations tmsOperations = new TMSOperations();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    TripClient tripClient = new TripClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa=new TripOrderAssignmentClient_QA();
    TripClient_QA tripClient_qa=new TripClient_QA();

    @Test(enabled = false)
    public void FailedAndRequeue() throws Exception {
        Long DcId = lmsOperations.getDeliveryCenterID(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID =  "" +lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //requeue
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.autoAssignmentOfOrderToTrip(packetId, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator3 = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse3);

        String deliveryStaffID2 =  "" +lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID2 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");

        TripResponse tripResponse2 = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID2));
        Assert.assertEquals(tripResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId2 = tripResponse2.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId2, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId2).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId2),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId2),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }


    @Test(description = "C21039")
    public void CancelOnFailedAndRequeue() throws Exception {
        Long DcId = lmsOperations.getDeliveryCenterID(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID =  "" +lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        TripResponse tripResponse = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);

        //requeue
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.autoAssignmentOfOrderToTrip(packetId, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator3 = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse3);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);

        //Cancel here
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_CONFIGURED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.FAILED_DELIVERY);
        orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);

        String deliveryStaffID2 =  "" +lmsServiceHelper.getAndAddDeliveryStaffID(DcId);
        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID2 + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");

        TripResponse tripResponse2 = tripClient_qa.createTrip(DcId, Long.parseLong(deliveryStaffID2));
        Assert.assertEquals(tripResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        long tripId2 = tripResponse2.getTrips().get(0).getId();
        Assert.assertEquals(lmsServiceHelper.assignOrderToTrip(tripId2, lmsHelper.getTrackingNumber(packetId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to assignOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId2).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId2),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId2),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
    }


    /**
     * Disable cancelation at hub level and cancelation of shipment should fail.
     * 	[Disable hub cancellation on Hub and try cancellation]
     */
    @Test(description = "C21056 - Pending")
    public void TestCancellationOnIntermediateHubAndRestock() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig forwardSortConfig = new SortConfig(SortConfig.SortConfigType.FORWARD);
        Map<String, SortationData> forwardSortConfigData = forwardSortConfig.getSortConfig(null,null);
        orderData.setSortConfigForward(forwardSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("DELHIS");
        orderData.setCourierCode(LMSConstants.IN_HOUSE_COURIER);
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("100002");
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(com.myntra.lms.client.status.ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 2;

        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2 DH-DEL
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.FORWARD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(), OrderTrackingDetailLevel.LEVEL2,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //Cancel API -- Cancelled
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);
        orderValidator.validateMLShipmentStatus(trackingNumbers, MLDeliveryShipmentStatus.EXPECTED_IN_DC.toString());

        //Cancellation at hub
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);
        for(String packetId : packetIds) {
            String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
            orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
            orderValidator.validatePacketStatus(packetId, "IC");
            orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
            orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, "ML",OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            orderValidator.validateMLShipmentStatus(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED.toString());
        }
        //process REstock in same Hub, instead of RT-BLR
    }

    /**
     * Reverse MsterBag should not sent any even/triger to update is_cancelble status.
     * DC_TO_X  : is_cancelble = false
     */
    @Test
    public void ReverseBaggingEvent(){

    }

    @Test(description = "C21060")
    public void CancelShipmentAndAddtoFWMasterBag(){

    }

    @Test(description = "C21060")
    public void CancelPartialShipmentFromMasterBag(){

    }

    @Test()
    public void PlaceExchangeOnCancelledOrder(){

    }

    @Test()
    public void PlaceReturnOnCancelledOrder() throws Exception {
        LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
        ReturnResponse s = lmsServiceHelper.getReturnStatusInLMS("4001573517", LMS_CONSTANTS.TENANTID ,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        System.out.println(s);
    }



    @Test(enabled = false)
    public void testSanity() throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, "500003", "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
    }

    @Test(enabled = false)
    public void test1() throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, "560068", "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, "560068", "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println("Tracking Number is :- "+trackingNumber );
    }

}
