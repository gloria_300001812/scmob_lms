package com.myntra.apiTests.erpservices.cancellation.ReturnCancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.ReturnCancellation.validators.ReturnCancellationValidator;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.tests.LMS_Trip;
import com.myntra.apiTests.erpservices.lms.validators.NDRValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.lastmile.client.status.MLPickupShipmentStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.domain.response.PickupResponse;
import com.myntra.lms.client.response.ItemEntry;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.NDRStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class ReturnCancellationNdr {

    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    TripClient_QA tripClient_qa = new TripClient_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa = new MLShipmentClientV2_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa = new TripOrderAssignmentClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa = new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa = new DeliveryStaffClient_QA();
    StoreClient_QA storeClient_qa = new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa = new MasterBagClient_QA();
    NDRValidator ndrValidator = new NDRValidator();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private OrderEntry orderEntry = new OrderEntry();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private ProcessRelease processRelease = new ProcessRelease();
    private ItemEntry itemEntry = new ItemEntry();
    MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(LMS_Trip.class);
    HashMap<String, String> pincodeDCMap;
    PickupShipmentClient_QA  pickupShipmentClient_qa = new PickupShipmentClient_QA();
    ReturnCancellationValidator returnCancellationValidator = new ReturnCancellationValidator();
    Map<String, Object> eventEntries = null;

    //creating HashMap to maintain pincode and DcId
    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() throws InterruptedException {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));

        //setting ndr_config to 1
        lmsHelper.setNdrConfigTo0();
        lmsHelper.setNdrConfigTo1();
        Thread.sleep(3000);
    }

    @AfterClass
    public void ndrChangeTo0()
    {
        lmsHelper.setNdrConfigTo0();
    }

    public Long createReturn() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, "560068", "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        Thread.sleep(3000);
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "ReturnCancellation", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnId);
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnId);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        Thread.sleep(15000);
        return returnId;
    }

    //Mark FP and cancel before ndr call
    @Test(description = "C27930 C27909")
    public void fpCancelBeforeNdrCall() throws Exception {
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        log.info("________ReturnId: " + returnID);
        Thread.sleep(3000);
        String trackingNumberForPickup = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();

        lmsReturnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.FAILED_PICKUP);
        //ndrValidator.ValidateShipmentThroughNDRConfig(packetId, "NDR status should be in progress");

        ShipmentUpdateResponse shipmentUpdateResponse1 = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.IN_PROGRESS, trackingNumberForPickup, LASTMILE_CONSTANTS.TENANT_ID);
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse1, trackingNumberForPickup, NDRStatus.IN_PROGRESS, ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLPickupShipmentStatus.FAILED_PICKUP.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());


        //TODO: Cancel the order should be allowed when ndr is in_progress
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        Thread.sleep(3000);
        //Cancelation of return should be allowed
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

    // cancel after ndr call reshedule/REATTEMPT request
    @Test(description = "C27931 C27935 C27937")
    public void cancelAfterNdrCallReschedule() throws Exception {
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        log.info("________ReturnId: " + returnID);
        Thread.sleep(3000);
        String trackingNumberForPickup = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();

        lmsReturnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.FAILED_PICKUP);
        Thread.sleep(3000);
        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.REATTEMPT, trackingNumberForPickup, LASTMILE_CONSTANTS.TENANT_ID);
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumberForPickup, NDRStatus.REATTEMPT, ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLPickupShipmentStatus.FAILED_PICKUP.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());
        Thread.sleep(3000);
        //TODO: Cancel the order should  be allowed when ndr is RE_ATTEMPT by customer and not allowed by customercare
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        Thread.sleep(3000);
        //Cancelation of return should be allowed
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.CustomerCare);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);


    }

    // cancel after ndr call RTO cancel request
    @Test(description = "C27932")
    public void cancelfterNdrCallRTO() throws Exception {
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        log.info("________ReturnId: " + returnID);
        Thread.sleep(3000);
        String trackingNumberForPickup = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();

        lmsReturnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.FAILED_PICKUP);

        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.RTO, trackingNumberForPickup, LASTMILE_CONSTANTS.TENANT_ID);
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumberForPickup, NDRStatus.RTO, ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLPickupShipmentStatus.FAILED_PICKUP.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());
        Thread.sleep(3000);
        //TODO: Cancel the order should be allowed when ndr is rto
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        Thread.sleep(3000);
        //Cancelation of return should be allowed
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

    // FP -> NDR RTO -> cc can cancel
    @Test(description = "C27936")
    public void fPNdrRtoCCcancel() throws Exception {
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);

        log.info("________ReturnId: " + returnID);
        Thread.sleep(3000);
        String trackingNumberForPickup = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();
        lmsReturnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.FAILED_PICKUP);

        ShipmentUpdateResponse shipmentUpdateResponse = lmsServiceHelper.updateBaseShipmentEventNDR(NDRStatus.RTO, trackingNumberForPickup, LASTMILE_CONSTANTS.TENANT_ID);
        NDRValidator.ValidateNDRbaseShipmentResponse(shipmentUpdateResponse, trackingNumberForPickup, NDRStatus.RTO, ShipmentType.PU, LASTMILE_CONSTANTS.COURIER_CODE_ML, MLPickupShipmentStatus.FAILED_PICKUP.toString(), ShipmentUpdateActivityTypeSource.NdrGateway.toString());
        Thread.sleep(3000);

        //TODO : CC Can cancel
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        Thread.sleep(3000);
        //Cancelation of return should be allowed
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.CustomerCare);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }



}