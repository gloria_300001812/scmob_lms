package com.myntra.apiTests.erpservices.cancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.csvutility.CSVReader;
import com.myntra.apiTests.erpservices.lms.Constants.FileUploadConstants;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.OrderUpdateTabHelper;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.apiTests.erpservices.lmsUploder.LMSUploadHelper;
import com.myntra.lms.client.status.BulkJobType;
import org.testng.annotations.Test;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Shanmugam Yuvaraj
 * Since: Jul:2019
 */
public class BulkUploadCancellation implements StatusPoller {

    LMSUploadHelper lmsUploadHelper;

    @Test(enabled = true, priority = 1, description = "TCID [C21236,C21235] Test Bulk order Cancellation through csv file upload")
    public void testBulkOrderCancellationUsingUpload() throws Exception {
        lmsUploadHelper = new LMSUploadHelper();
        Integer noOfOrders = 2;
        String location = "ELC";
        List<Map<String, String>> fileRules = csvReader.readCsvAsListOfMap(FileUploadConstants.bulkUploadCancellation);

        for (Map rule : fileRules) {
            ArrayList<String> trackingNumbers = new ArrayList<>();
            ArrayList<String> toStates = new ArrayList<>();
            ArrayList<String> orderIds = new ArrayList<>();
            int j = 0;
            while (j++ != noOfOrders) {
                String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(rule.get("FromState").toString(), LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true));
                String trackingNumber = lmsHelper.getTrackingNumber(packetId);
                statusPollingValidator.validateOrderStatus(packetId, rule.get("OrderToShipStatusBeforeUpload").toString());
                orderIds.add(packetId);
                trackingNumbers.add(trackingNumber);
                toStates.add(rule.get("ToState").toString());
            }

            String file_Path = orderUpdateTabHelper.generateOrderUpdateCSV(Long.parseLong(LASTMILE_CONSTANTS.TENANT_ID), trackingNumbers, toStates, location, toStates);
            lmsUploadHelper.configurationUpload(BulkJobType.BULK_ORDER_STATUS_UPDATE.name(), file_Path);

            for (int i = 0; i <= trackingNumbers.size() - 1; i++) {
                System.out.println(MessageFormat.format("From state - {0} and toState - {1} upload inProgress for tracking number {2} and order_id {3}",
                        rule.get("FromState").toString(), rule.get("ToState").toString(), trackingNumbers.get(i), orderIds.get(i)));
                statusPollingValidator.validateML_ShipmentStatus(trackingNumbers.get(i), LASTMILE_CONSTANTS.TENANT_ID, rule.get("MLShipmentStatus").toString());
                statusPollingValidator.validateOrderStatus(orderIds.get(i), rule.get("OrderToShipStatus").toString());
                statusPollingValidator.validate_PacketStatusOMS(orderIds.get(i), LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.CLIENT_ID, rule.get("OMSStatus").toString());
            }
        }
    }
}
