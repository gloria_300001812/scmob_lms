package com.myntra.apiTests.erpservices.cancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.erpservices.LMSCancellation.CancelationHelper;
import com.myntra.apiTests.erpservices.LMSCancellation.validators.OrderValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_ReturnHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.sortation.csv.SortConfig;
import com.myntra.apiTests.erpservices.sortation.csv.SortationData;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.Pojos.OrderData;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.cms.pim.dto.StatusType;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponseCode;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.tms.lane.LaneType;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class CancellationRHC {
    String env = getEnvironment();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TMSOperations tmsOperations = new TMSOperations();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    OrderValidator orderValidator = new OrderValidator();
    CancelationHelper cancelationHelper = new CancelationHelper();

    @BeforeMethod(alwaysRun = true)
    public void initilize(){
        String query = "update tracking_number set status = 0 \n" +
                "where courier_code = \"DE-COD\" and tenant_id = \"4019\" \n" +
                "ORDER BY `last_modified_on` ASC limit 10;";
        DBUtilities.exUpdateQuery(query, "myntra_lms");
    }

    @BeforeClass
    public void setup() throws Exception {
        cancelationHelper.disableSorterOnHub("DH-BLR");
        cancelationHelper.disableSorterOnHub("DH-DEL");
        cancelationHelper.disableSorterOnHub("RHD");
    }

    /**
     * Core Scenario
     * Path:- DH-BLR -> DH-DEL -> RHC -> 3PL
     */

    @Test(description = "C21207", retryAnalyzer = Retry.class)
    public void CancelAtSourceDishpatchHub() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        Map<String, Object> orderMap;
        Map<String, String>  containerDetails;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig rhdSortConfig = new SortConfig(SortConfig.SortConfigType.RHD);
        Map<String, SortationData> rhdSortConfigData = rhdSortConfig.getSortConfig(null,null);
        //orderData.setSortConfigForward(rhdSortConfigData);
        orderData.setSortConfigRHD(rhdSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("RHD");
        orderData.setCourierCode("DE");
        orderData.setRHDDestinationHubCode("DE");
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("400053");  //110001    122002
        orderData.setWareHouseId("36");
        orderData.setRtoHubCode("RT-BLR");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        lmsReturnHelper.insertTrackingNumberClosedBox("DE");
        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //in case of RHD, we need to update status as accepted manually
        lmsHelper.updateOrderTrackingToAccepted(packetIds);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.RHD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //Cancellation at hub
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);
        for(String packetId : packetIds) {
            String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
            orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
            orderValidator.validatePacketStatus(packetId, "IC");
            orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
            orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        }

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2 - DH-DEL
        nextHub = CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, currentHub, orderData.getRtoHubCode());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2 -- Forward masterbag should not be allowed
        mbCreateResponse = masterBagServiceHelper.createMasterBag(currentHub, "RHD", ShippingMethod.NORMAL, orderData.getCourierCode(), LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        for(String trackingNumber : trackingNumbers) {
            MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), StatusType.ERROR.toString(), "Adding Cancelled shipment to forward MasterBag should not be allowed");
        }
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);


        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(currentHub, nextHub, ShippingMethod.NORMAL, orderData.getCourierCode(), LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        for(String trackingNumber : trackingNumbers) {
            MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), StatusType.SUCCESS.toString(), "Adding Cancelled shipment to forward MasterBag should not be allowed");
        }
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //Process RTO and Restock

//        //TMS Operations
//        tmsOperations.createAndShipReturnContainer(masterBagId, currentHub, orderData.getRtoHubCode(), LaneType.INTERCITY, LMS_CONSTANTS.TENANTID);
//
//        //MasterBag In-scan V2
//        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, currentHub, orderData.getRtoHubCode());
//
//        //Call Restock API

        System.out.println("trackingNumbers:"+trackingNumbers);
    }

    @Test(description = "C21207", retryAnalyzer = Retry.class)
    public void CancelAtIntermendiateDishpatchHub() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        Map<String, Object> orderMap;
        Map<String, String>  containerDetails;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig rhdSortConfig = new SortConfig(SortConfig.SortConfigType.RHD);
        Map<String, SortationData> rhdSortConfigData = rhdSortConfig.getSortConfig(null,null);
        //orderData.setSortConfigForward(rhdSortConfigData);
        orderData.setSortConfigRHD(rhdSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("RHD");
        orderData.setCourierCode("DE");
        orderData.setRHDDestinationHubCode("DE");
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("400053");  //110001    122002
        orderData.setWareHouseId("36");
        orderData.setRtoHubCode("RT-BLR");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        lmsReturnHelper.insertTrackingNumberClosedBox("DE");
        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //in case of RHD, we need to update status as accepted manually
        lmsHelper.updateOrderTrackingToAccepted(packetIds);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.RHD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2 - DH-DEL
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RHD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        containerDetails = tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //Cancellation at hub
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);
        for(String packetId : packetIds) {
            String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
            orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
            orderValidator.validatePacketStatus(packetId, "IC");
            orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
            orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        }

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2 - RHC
        nextHub = CancelationHelper.MasterBagInscanV2OnCancelledOrder(masterBagId, currentHub, orderData.getRtoHubCode());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED_AT_HANDOVER_CENTER);

        //Process RTO and Restock

//        //MasterBag V2
//        mbCreateResponse = masterBagServiceHelper.createMasterBag(currentHub, orderData.getRtoHubCode(), com.myntra.lms.client.status.ShippingMethod.NORMAL, orderData.getCourierCode(), LMS_CONSTANTS.TENANTID);
//        masterBagId = mbCreateResponse.getId();
//        System.out.println("Reverse masterBagId=" + masterBagId);
//        for(String trackingNumber : trackingNumbers) {
//            masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
//        }
//        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
//
//        //TMS Operations
//        tmsOperations.createAndShipReturnContainer(masterBagId, currentHub, orderData.getRtoHubCode(), LaneType.INTERCITY, LMS_CONSTANTS.TENANTID);
//
//        //MasterBag In-scan V2
//        CancelationHelper.MasterBagInscanV2OnCancelledOrderAtRTO(masterBagId, currentHub, orderData.getRtoHubCode());

        //Call Restock API

        System.out.println("trackingNumbers:"+trackingNumbers);
    }

    @Test(description = "C21205 - 5, C21089, C21207", retryAnalyzer = Retry.class)
    public void CancelAtRHC_Hub() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        Map<String, Object> orderMap;
        Map<String, String>  containerDetails;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig rhdSortConfig = new SortConfig(SortConfig.SortConfigType.RHD);
        Map<String, SortationData> rhdSortConfigData = rhdSortConfig.getSortConfig(null,null);
        //orderData.setSortConfigForward(rhdSortConfigData);
        orderData.setSortConfigRHD(rhdSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("RHD");
        orderData.setRtoHubCode("RT-BLR");
        orderData.setCourierCode("DE");
        orderData.setRHDDestinationHubCode("DE");
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("400053");  //110001    122002
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        lmsReturnHelper.insertTrackingNumberClosedBox("DE");
        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //in case of RHD, we need to update status as accepted manually
        lmsHelper.updateOrderTrackingToAccepted(packetIds);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.RHD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RHD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        containerDetails = tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2 -- Recieve MB
        nextHub = lmsOperations.masterBagInScanV2RHD(masterBagId, orderData);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED_AT_HANDOVER_CENTER);

        //Cancellation at RHC
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);
        for(String packetId : packetIds) {
            String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
            orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, ReleaseStatus.SH);
            orderValidator.validatePacketStatus(packetId, "IC");
            orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
            orderValidator.validateOrderTrackingDetail(lmsHelper.getTrackingNumber(packetId), ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        }

        //MasterBag V2 -- Forward masterbag should not be allowed
        mbCreateResponse = masterBagServiceHelper.createMasterBag(currentHub, nextHub, ShippingMethod.NORMAL, orderData.getCourierCode(), LMS_CONSTANTS.TENANTID);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        for(String trackingNumber : trackingNumbers) {
            MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), StatusType.ERROR.toString(), "Adding Cancelled shipment to forward MasterBag should not be allowed");
        }

        //MasterBag V2 -- Reverse MasterBag - RHC to RT-BLR
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, orderData.getRtoHubCode());
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),orderData.getRtoHubCode(), trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //process RTO and Restock at RT-BLR

        System.out.println("trackingNumbers:"+trackingNumbers);
    }

    @Test(description = "C21205 - 4, C21094, C21207", retryAnalyzer = Retry.class)
    public void CancelAtRHC_AfterAddToMB() throws Exception {
        OrderData orderData = new OrderData();
        MasterbagDomain mbCreateResponse;
        Map<String, Object> orderMap;
        Map<String, String>  containerDetails;
        List<String> trackingNumbers , packetIds,  orderIds;
        Long masterBagId, containerId = null;
        TMSMasterbagReponse tmsMasterbagReponse;
        SortConfig rhdSortConfig = new SortConfig(SortConfig.SortConfigType.RHD);
        Map<String, SortationData> rhdSortConfigData = rhdSortConfig.getSortConfig(null,null);
        //orderData.setSortConfigForward(rhdSortConfigData);
        orderData.setSortConfigRHD(rhdSortConfigData);
        orderData.setOriginHubCode("DH-BLR");
        orderData.setDestinationHubCode("RHD");
        orderData.setCourierCode("DE");
        orderData.setRHDDestinationHubCode("DE");
        orderData.setTenantId(LMS_CONSTANTS.TENANTID);
        orderData.setZipcode("400053");  //110001    122002
        orderData.setWareHouseId("36");
        orderData.setShippingMethod(ShippingMethod.NORMAL);
        orderData.setTryAndBuy(false);
        orderData.setCurrentHub(orderData.getOriginHubCode());
        String currentHub = orderData.getOriginHubCode();
        String nextHub = orderData.getDestinationHubCode();
        long laneId = 0;
        long transporterId = 0;
        Integer numberOfShipments = 1;

        lmsReturnHelper.insertTrackingNumberClosedBox("DE");
        sortationHelper.computeAndPrintCompletePath(orderData.getOriginHubCode(), orderData.getDestinationHubCode(), orderData.getClientId(), orderData.getCourierCode());
        orderMap = lmsOperations.createMockOrders(orderData, EnumSCM.PK, numberOfShipments);
        orderData.setOrderMap(orderMap);
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("currentHub=%s , destinationHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.PACKED, ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //in case of RHD, we need to update status as accepted manually
        lmsHelper.updateOrderTrackingToAccepted(packetIds);

        //Order Inscan V2 & Sortation
        nextHub = lmsOperations.orderInscanV2(orderData, false, SortConfig.SortConfigType.RHD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.INSCANNED, ShipmentUpdateEvent.INSCAN, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getOriginHubCode(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.ADDED_TO_MB, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //TMS Operations
        tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        nextHub = lmsOperations.masterBagInScanV2(masterBagId, orderData, SortConfig.SortConfigType.RHD);
        orderData.setNextHub(nextHub);
        System.out.println(String.format("currentHub=%s , nextHub=%s",currentHub,nextHub));
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //TMS Operations
        containerDetails = tmsOperations.createAndShipForwardContainer(masterBagId,orderData, nextHub, LaneType.INTERCITY);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.SHIPPED, ShipmentUpdateEvent.IN_TRANSIT, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);

        //save current node (origin at current hop)
        currentHub = nextHub;
        orderData.setCurrentHub(currentHub);

        //MasterBag In-scan V2
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);
        nextHub = lmsOperations.masterBagInScanV2RHD(masterBagId, orderData);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER, ShipmentUpdateEvent.SHIPPED, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED_AT_HANDOVER_CENTER);
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, true);

        //MasterBag V2
        masterBagId = lmsOperations.masterBagV2Operations(orderData, trackingNumbers, currentHub, nextHub);
        sortationValidator.validateMasterBagOperation(masterBagId, orderData.getCurrentHub(),nextHub, trackingNumbers, orderData.getShippingMethod(), orderData.getTenantId());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);
        sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Cancellation at RHC after add to MB - NOT allowed
        orderValidator.validateIsCancelableFlagSetforOrder(packetIds, false);
        for(String packetId : packetIds) {
            String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
            orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED);
            orderValidator.validatePacketStatus(packetId, "IC");
            orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER);
            sortationValidator.validateOrderTrackingDetail(trackingNumbers, ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER, ShipmentUpdateEvent.ADD_TO_MASTERBAG, orderData.getCourierCode(),OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        }

        //ship master bag (Delhivery (DE)-DC)
        ShipmentResponse shipmentMasterBag = lmsServiceHelper.shipMasterBag(masterBagId);
        Assert.assertEquals(shipmentMasterBag.getStatus().getStatusType().toString(), StatusResponse.Type.SUCCESS.toString(), "Ship Masterbag to 3PL failed") ;
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.HANDED_OVER_TO_3PL);
        System.out.println("trackingNumbers:"+trackingNumbers);
    }
}
