package com.myntra.apiTests.erpservices.cancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.erpservices.LMSCancellation.CancelationHelper;
import com.myntra.apiTests.erpservices.LMSCancellation.validators.OrderValidator;
import com.myntra.apiTests.erpservices.cancellation.dp.CancellationDataProvider;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.PickupResponse;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import org.testng.annotations.Test;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class AdminCorrections {

    String env = getEnvironment();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    OrderValidator orderValidator = new OrderValidator();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();
    LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    SortationHelper sortationHelper = new SortationHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LastmileOperations lastmileOperations = new LastmileOperations();
    TMSOperations tmsOperations = new TMSOperations();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    SortationValidator sortationValidator = new SortationValidator();
    CancelationHelper cancelationHelper = new CancelationHelper();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);

    /**
     * TRANSITION: STATE(X) -> STATE(CANCELLED) -> OPERATION(LOST) -> STATE(CANCELLED_LOST) -> OPERATION(RTO) -> STATE(RTO)
     */

    @Test(description = "C21167, C21168, C21171, C21172",
            dataProvider = "AdminCorrectionForwardCancelledOrderStatus", dataProviderClass = CancellationDataProvider.class)
    public void CancelToLost(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String orderId, packetId, trackingNumber;
        String courierCode = LMSConstants.IN_HOUSE_COURIER;
        String zipcode = "560068", wareHouseId = "36";
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus,  orderId, packetId, trackingNumber));
        orderValidator.validateOrderIsCancellable(packetId, releaseStatus);
        if (releaseStatus == ReleaseStatus.SH) {
            orderValidator.validateMLShipmentStatus(trackingNumber, EnumSCM.UNASSIGNED);
        }
        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, releaseStatus);

        if(!(releaseStatus== ReleaseStatus.OFD || releaseStatus== ReleaseStatus.DL)) {
            //Mark to LOST
            PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
            if(releaseStatus== ReleaseStatus.SH) {
                orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.LOST.toString());
            }else {
                orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, ReleaseStatus.CANCELLED_LOST);
                orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED_LOST.toString());
            }
            orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED_LOST);
            orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.CANCELLED_LOST, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

            //Mark to RTO
            statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
            if(releaseStatus== ReleaseStatus.SH) {
                orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
                orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.RTO_CONFIRMED.toString());
            }else {
                orderValidator.validateMLShipmentStatusOnCancel(trackingNumber, ReleaseStatus.RTO);
                orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.CANCELLED);
                orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.CANCELLED.toString());
            }
            orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.CANCELLED, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        }else{
            if(releaseStatus== ReleaseStatus.OFD) {
                //Mark to LOST
                PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
                orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.OUT_FOR_DELIVERY);
                orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.OUT_FOR_DELIVERY.toString());
                orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.OUT_FOR_DELIVERY, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

                //Mark to RTO
                statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
                orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.OUT_FOR_DELIVERY);
                orderValidator.validateMLShipmentStatus(trackingNumber, ShipmentStatus.OUT_FOR_DELIVERY.toString());
                orderValidator.validateOrderTrackingDetail(trackingNumber, ShipmentStatus.OUT_FOR_DELIVERY, ShipmentUpdateEvent.OUT_FOR_DELIVERY, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
            }else if(releaseStatus== ReleaseStatus.DL){
                // no need to validate
            }
        }

        System.out.println(String.format("[%s]: orderId:[%s], packetId:[%s], trackingNumber:[%s]", releaseStatus,  orderId, packetId, trackingNumber));
    }
}
