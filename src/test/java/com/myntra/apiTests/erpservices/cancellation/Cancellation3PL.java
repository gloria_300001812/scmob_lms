package com.myntra.apiTests.erpservices.cancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.erpservices.LMSCancellation.CancelationHelper;
import com.myntra.apiTests.erpservices.LMSCancellation.validators.OrderValidator;
import com.myntra.apiTests.erpservices.cancellation.dp.Cancellation3PLDP;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.lms.lmsClient.ShippingMethod;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.TMSOperations;
import com.myntra.apiTests.erpservices.sortation.service.SortationHelper;
import com.myntra.apiTests.erpservices.sortation.service.SortationValidator;
import com.myntra.cms.pim.dto.StatusType;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.OrderTrackingDetailLevel;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateResponseCode;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since May-2019
 */
public class Cancellation3PL {
    String env = getEnvironment();
    LMSHelper lmsHelper = new LMSHelper();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    OrderValidator orderValidator = new OrderValidator();
    LMSOperations lmsOperations = new LMSOperations();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    LastmileOperations lastmileOperations = new LastmileOperations();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    SortationHelper sortationHelper = new SortationHelper();
    SortationValidator sortationValidator = new SortationValidator();
    TMSOperations tmsOperations = new TMSOperations();
    CancelationHelper cancelationHelper = new CancelationHelper();
    LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    MasterBagValidator masterBagValidator = new MasterBagValidator();

    /**
     * Cancel is allowed till Add to MB
     */

    @Test(retryAnalyzer = Retry.class)
    public void test3PlSanity() throws Exception {
        String courierCode = "EK", tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560069", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "EKART", returnHubCode = "RT-BLR";
        long masterBagId;
        MasterbagDomain mbCreateResponse;
        String orderId, packetId, trackingNumber;
        orderId = lmsHelper.createMockOrder(EnumSCM.IS, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        CancelationHelper.SetCourierCreationStatusFor3PL(packetId);
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);

        //Ship MB
        lmsServiceHelper.shipMasterBag(masterBagId);
        // OFD
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.OUT_FOR_DELIVERY , courierCode ,ShipmentType.DL);
        // Deliver the 3pl Order
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.DELIVERED , courierCode ,ShipmentType.DL);
    }

    @Test(description = "C21067, C21203, C21267",
            dataProvider = "CancelONPackedState", dataProviderClass = Cancellation3PLDP.class,
            retryAnalyzer = Retry.class)
    public void Cancellation3Pl_PackedState(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String courierCode = "EK", tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560069", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "EKART", returnHubCode = "RT-BLR";
        String orderId, packetId, trackingNumber;
        long masterBagId;
        MasterbagDomain mbCreateResponse;

        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(), "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        CancelationHelper.SetCourierCreationStatusFor3PL(packetId);
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);

        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Order Inscan V2
        String orderInScanResponse= lmsOperations.orderInscanV2(trackingNumber, wareHouseId, false);
        orderValidator.validateCancelledShipmentOrderInscan(orderInScanResponse, ShipmentStatus.CANCELLED, originHubCode, returnHubCode);

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), StatusType.ERROR.toString(), "Adding Cancelled shipment to forward MasterBag should not be allowed");
        Assert.assertEquals(addShipmentToMbResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());
        Assert.assertTrue(addShipmentToMbResponse.getErrorMessage().contains("Order is in CANCELLED state, cannot be added to master bag."));
        Assert.assertTrue(addShipmentToMbResponse.getStatus().getStatusMessage().contains("Update to Masterbag masterBagId with event SHIPMENTS_ADDED failed.".replace("masterBagId", String.valueOf(masterBagId))));
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",  orderId, packetId, trackingNumber));

        //Restock here
    }

    @Test(description = "C21048, C21067, C21203, C21267" ,
            dataProvider = "CancelONInscannedState", dataProviderClass = Cancellation3PLDP.class,
            retryAnalyzer = Retry.class)
    public void Cancellation3Pl_InscannedState(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String courierCode = "EK", tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560069", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "EKART", returnHubCode = "RT-BLR";
        String orderId, packetId, trackingNumber;
        long masterBagId;
        MasterbagDomain mbCreateResponse;

        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(), "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        CancelationHelper.SetCourierCreationStatusFor3PL(packetId);

        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponseML(cancelShipmentResponse, releaseStatus);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), StatusType.ERROR.toString(), "Adding Cancelled shipment to forward MasterBag should not be allowed");
        Assert.assertEquals(addShipmentToMbResponse.getResponseCode().toString(), ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED.toString());
        Assert.assertTrue(addShipmentToMbResponse.getErrorMessage().contains("Order is in CANCELLED state, cannot be added to master bag."));
        Assert.assertTrue(addShipmentToMbResponse.getStatus().getStatusMessage().contains("Update to Masterbag masterBagId with event SHIPMENTS_ADDED failed.".replace("masterBagId", String.valueOf(masterBagId))));
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",  orderId, packetId, trackingNumber));

        //Restock here
    }

    @Test(description = "C21049, C21050, C21069, C21203",
            dataProvider = "CancelON_AddToMBState", dataProviderClass = Cancellation3PLDP.class,
            retryAnalyzer = Retry.class)
    public void Cancellation3Pl_ADD_TO_MB_State(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String courierCode = "EK", tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560069", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "EKART", returnHubCode = "RT-BLR";
        String orderId, packetId, trackingNumber;
        long masterBagId;
        MasterbagDomain mbCreateResponse;

        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(), "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        CancelationHelper.SetCourierCreationStatusFor3PL(packetId);

        Map<String, Object> shipmentOrderMap = DBUtilities.exSelectQueryForSingleRecord("SELECT shipment_id FROM shipment_order_map where tracking_Number = '" + trackingNumber + "';", "myntra_lms");
        masterBagId = Long.parseLong(shipmentOrderMap.get("shipment_id").toString());

        //Cancel API
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse3PL(cancelShipmentResponse, releaseStatus);
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Ship MB
        lmsServiceHelper.shipMasterBag(masterBagId);
        // OFD
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.OUT_FOR_DELIVERY , courierCode ,ShipmentType.DL);
        // Deliver the 3pl Order
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.DELIVERED , courierCode ,ShipmentType.DL);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.DELIVERED);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",  orderId, packetId, trackingNumber));
    }


    @Test(description = "C21050, C21069",
            dataProvider = "CancelON_AddToMBState", dataProviderClass = Cancellation3PLDP.class,
            retryAnalyzer = Retry.class)
    public void Cancellation3Pl_ADD_TO_MB_CLOSED_State(ReleaseStatus releaseStatus, ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String courierCode = "EK", tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560069", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "EKART", returnHubCode = "RT-BLR";
        String orderId, packetId, trackingNumber;
        long masterBagId;
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse closeMBResponse;

        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(), "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        CancelationHelper.SetCourierCreationStatusFor3PL(packetId);

        Map<String, Object> shipmentOrderMap = DBUtilities.exSelectQueryForSingleRecord("SELECT shipment_id FROM shipment_order_map where tracking_Number = '" + trackingNumber + "';", "myntra_lms");
        masterBagId = Long.parseLong(shipmentOrderMap.get("shipment_id").toString());
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(closeMBResponse.getStatus().getStatusType().toString(), StatusType.SUCCESS.toString());
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.CLOSED);

        //Cancel API
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse3PL(cancelShipmentResponse, releaseStatus);
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        //Ship MB
        lmsServiceHelper.shipMasterBag(masterBagId);
        // OFD
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.OUT_FOR_DELIVERY , courierCode ,ShipmentType.DL);
        // Deliver the 3pl Order
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.DELIVERED , courierCode ,ShipmentType.DL);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        orderValidator.validateShipmentOrderStatus(packetId, ShipmentStatus.DELIVERED);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",  orderId, packetId, trackingNumber));
    }

    @Test(description =  "C21071, C21069",
            dataProvider = "CancelON_ShippedState", dataProviderClass = Cancellation3PLDP.class,
            retryAnalyzer = Retry.class)
    public void Cancellation3Pl_Shipped_State(ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String courierCode = "EK", tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560069", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "EKART", returnHubCode = "RT-BLR";
        String orderId, packetId, trackingNumber;
        long masterBagId;
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse closeMBResponse;
        ReleaseStatus releaseStatus = ReleaseStatus.IS;
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(), "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        CancelationHelper.SetCourierCreationStatusFor3PL(packetId);
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, true);

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(closeMBResponse.getStatus().getStatusType().toString(), StatusType.SUCCESS.toString());

        //Ship MB
        lmsServiceHelper.shipMasterBag(masterBagId);

        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse3PL(cancelShipmentResponse, ReleaseStatus.SHIPPED);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        // OFD
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.OUT_FOR_DELIVERY , courierCode ,ShipmentType.DL);
        // Deliver the 3pl Order
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.DELIVERED , courierCode ,ShipmentType.DL);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",  orderId, packetId, trackingNumber));
    }

    @Test(description = "C21052",
            dataProvider = "CancelON_OFDState", dataProviderClass = Cancellation3PLDP.class,
            retryAnalyzer = Retry.class)
    public void Cancellation3Pl_OFD_State(ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String courierCode = "EK", tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560069", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "EKART", returnHubCode = "RT-BLR";
        String orderId, packetId, trackingNumber;
        long masterBagId;
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse closeMBResponse;
        ReleaseStatus releaseStatus = ReleaseStatus.IS;
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(), "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        CancelationHelper.SetCourierCreationStatusFor3PL(packetId);
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(closeMBResponse.getStatus().getStatusType().toString(), StatusType.SUCCESS.toString());

        //Ship MB
        lmsServiceHelper.shipMasterBag(masterBagId);
        Thread.sleep(5000);

        // OFD
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.OUT_FOR_DELIVERY , courierCode ,ShipmentType.DL);
        Thread.sleep(5000);

        //Cancel API
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse3PL(cancelShipmentResponse, ReleaseStatus.OFD);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);

        // Deliver the 3pl Order
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.DELIVERED , courierCode ,ShipmentType.DL);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",  orderId, packetId, trackingNumber));
    }


    @Test(description = "C21053",
            dataProvider = "CancelON_DeliveredState", dataProviderClass = Cancellation3PLDP.class,
            retryAnalyzer = Retry.class)
    public void Cancellation3Pl_Delivered_State(ShipmentStatus expectedStatus, ShippingMethod shippingMethod) throws Exception {
        String courierCode = "EK", tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560069", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "EKART", returnHubCode = "RT-BLR";
        String orderId, packetId, trackingNumber;
        long masterBagId;
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse closeMBResponse;
        ReleaseStatus releaseStatus = ReleaseStatus.IS;
        orderId = lmsHelper.createMockOrder(releaseStatus.toString(), zipcode, courierCode, wareHouseId, shippingMethod.toString(), "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        CancelationHelper.SetCourierCreationStatusFor3PL(packetId);
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);

        //MasterBag V2
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(closeMBResponse.getStatus().getStatusType().toString(), StatusType.SUCCESS.toString());

        //Ship MB
        lmsServiceHelper.shipMasterBag(masterBagId);
        // OFD
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.OUT_FOR_DELIVERY , courierCode ,ShipmentType.DL);
        // Deliver the 3pl Order
        lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.DELIVERED , courierCode ,ShipmentType.DL);

        //Cancel API
        String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
        orderValidator.validateCancelShipmentResponse3PL(cancelShipmentResponse, ReleaseStatus.DL);
        orderValidator.validatePacketStatus(packetId, "IC");
        orderValidator.validateShipmentOrderStatus(packetId, expectedStatus);
        orderValidator.validateOrderTrackingDetail(trackingNumber, expectedStatus, ShipmentUpdateEvent.CANCEL, courierCode,OrderTrackingDetailLevel.LEVEL2, LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",  orderId, packetId, trackingNumber));
    }

    //Disabling as this affects other testing of cancellation
    @Test(description = "C21073", enabled = false, retryAnalyzer = Retry.class)
    public void Cancellation3Pl_ON_CancellationDisabledHub() throws Exception {
        String courierCode = "EK", tenantId = LMS_CONSTANTS.TENANTID;;
        String zipcode = "560069", wareHouseId = "36";
        String originHubCode = "DH-BLR", destinationHubCode = "EKART", returnHubCode = "RT-BLR";
        String orderId, packetId, trackingNumber;
        long masterBagId;
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse closeMBResponse;
        ReleaseStatus releaseStatus = ReleaseStatus.PACKED;
        ShipmentStatus expectedStatus;
        ShippingMethod shippingMethod;

        try {
            //Disable is_cancellation is false on Source Hub
            cancelationHelper.disableCancelationOnHub(originHubCode);

            orderId = lmsHelper.createMockOrder(EnumSCM.PK, zipcode, courierCode, wareHouseId, EnumSCM.NORMAL, "cod", false, true);
            packetId = omsServiceHelper.getPacketId(orderId);
            trackingNumber = lmsHelper.getTrackingNumber(packetId);
            CancelationHelper.SetCourierCreationStatusFor3PL(packetId);
            OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
            orderValidator.validateIsCancelableFlagSetforOrder(packetId, false);

            //Cancel
            String cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
            orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED, "Shipment Cancelation should not be allowed at this point, as hub config is disabled");

            //Order Inscan V2
            String orderInScanResponse= lmsOperations.orderInscanV2(trackingNumber, wareHouseId, false);
            orderValidator.validateCancelledShipmentOrderInscan(orderInScanResponse, ShipmentStatus.CANCELLED, originHubCode, returnHubCode);

            //Cancel
            cancelShipmentResponse = lmsServiceHelper.cancelShipmentInLMS2(packetId);
            orderValidator.validateCancelShipmentResponse(cancelShipmentResponse, ShipmentUpdateResponseCode.TRANSITION_NOT_ALLOWED, "Shipment Cancelation should not be allowed at this point, as hub config is disabled");

            //MasterBag V2
            mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, com.myntra.lms.client.status.ShippingMethod.NORMAL, courierCode, tenantId);
            masterBagId = mbCreateResponse.getId();
            System.out.println("masterBagId=" + masterBagId);
            masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
            Assert.assertEquals(closeMBResponse.getStatus().getStatusType().toString(), StatusType.SUCCESS.toString());

            //Ship MB
            lmsServiceHelper.shipMasterBag(masterBagId);

            // OFD
            lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.OUT_FOR_DELIVERY , courierCode ,ShipmentType.DL);
            // Deliver the 3pl Order
            lmsServiceHelper.updateShipmentfor3PL(trackingNumber , ShipmentUpdateEvent.DELIVERED , courierCode ,ShipmentType.DL);

            //Restock here
        }finally {
            //Re enable is_cancellation is false on Source Hub
            cancelationHelper.enableCancelationOnHub(originHubCode);
        }
    }

}
