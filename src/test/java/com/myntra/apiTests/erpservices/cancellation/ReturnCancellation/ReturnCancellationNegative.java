package com.myntra.apiTests.erpservices.cancellation.ReturnCancellation;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.ReturnCancellation.validators.ReturnCancellationValidator;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.tests.LMS_Trip;
import com.myntra.apiTests.erpservices.lms.tests.TripOrderAssignmentResponseValidator;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.*;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.domain.response.PickupResponse;
import com.myntra.lms.client.response.ItemEntry;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.logistics.platform.domain.*;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.common.enums.code.ReturnStatus;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.testbase.BaseTest;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lms.validators.StatusPoller.lastmileClient;
import static com.myntra.apiTests.erpservices.lms.validators.StatusPoller.lmsClient;


public class ReturnCancellationNegative extends BaseTest{

    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    TripClient_QA tripClient_qa = new TripClient_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa = new MLShipmentClientV2_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa = new TripOrderAssignmentClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa = new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa = new DeliveryStaffClient_QA();
    StoreClient_QA storeClient_qa = new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa = new MasterBagClient_QA();
    private StoreValidator storeValidator = new StoreValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    Map<String, Object> eventEntries = null;

    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private OrderEntry orderEntry = new OrderEntry();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private ProcessRelease processRelease = new ProcessRelease();
    private ItemEntry itemEntry = new ItemEntry();
    MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(LMS_Trip.class);
    HashMap<String, String> pincodeDCMap;
    PickupShipmentClient_QA pickupShipmentClient_qa = new PickupShipmentClient_QA();
    ReturnCancellationValidator returnCancellationValidator = new ReturnCancellationValidator();
    // variables for store
    static String searchParams;
    static Long storeHlPId;
    static Long storeSDAId;
    static String storeTenantId;
    static String tenantId;
    static String pincode;
    static String mobileNumber;
    static String storeCourierCode;
    static String code;

    static String code1;
    static String storeCourierCode1;
    static String searchParams1;
    static Long storeHlPId1;
    static Long storeSDAId1;
    static String storeTenantId1;
    static String mobileNumber1;
    //creating HashMap to maintain pincode and DcId
    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
    }

    public Long createReturn() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, "560068", "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        Thread.sleep(3000);
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "ReturnCancellation", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnId);
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnId);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        Thread.sleep(15000);
        return returnId;
    }

    //creation of store
    public void createStore() {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");

        Random r = new Random(System.currentTimeMillis());
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        String name = "BSI" + String.valueOf(contactNumber1).substring(5, 9);
        String ownerFirstName = "BSI" + String.valueOf(contactNumber1).substring(5, 9);
        String ownerLastName = "BSI" + String.valueOf(contactNumber1).substring(5, 9);
        code = "BSI" + String.valueOf(contactNumber1).substring(5, 9);
        //code1 = "BSI" + String.valueOf(contactNumber2).substring(5, 9);
        String latLong = "LA_latlong" + String.valueOf(contactNumber1).substring(5, 9);
        String emailId = "test@lastmileAutomation.com";
        mobileNumber = String.valueOf(contactNumber1);
        String address = "Automation Address";
        pincode = LASTMILE_CONSTANTS.STORE_PINCODE;
        String city = "Bangalore";
        String state = "Karnataka";
        String mappedDcCode = LASTMILE_CONSTANTS.ROLLING_DC_CODE;
        String gstin = "LA_gstin" + String.valueOf(contactNumber1).substring(5, 9);
        tenantId = LMS_CONSTANTS.TENANTID;
        searchParams = "code.like:" + code;
        //Create Store
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code);
        //validate Store created successfully
        Assert.assertTrue(storeResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.storeCreated), "Store is not created");
        //get Store required values
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        DeliveryStaffEntry deliveryStaffEntry = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry();
        storeSDAId = deliveryStaffEntry.getId();
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeCourierCode = storeResponse.getStoreEntries().get(0).getCourierEntry().getCode();

    }

    // Create Return Mark PQCP DC Approve
    @Test(description = "C27893 C27894")
    public void pqcpDcApprove() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : Cancel the return should not be allowed
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //TODO : Cancel the return should not be allowed
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);


        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        //TODO : Cancel the return should not be allowed
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        //TODO : Cancel the return should not be allowed
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_DONE, trackingNo);
        //TODO : Cancel the return should not be allowed
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        //TODO : Cancel the return should not be allowed
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);


        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_SUCCESSFUL, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RETURN_SUCCESSFUL);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

    }

    // Create Return Mark PQCP DC ON HOLD(REJECT), CC APPROVE
    @Test(description = "C27895 C27896 C27898")
    public void pqcpDcOnHoldDCOnHoldCCApprove() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : Cancel the return should not be allowed
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //TODO : Cancel the return should not be allowed
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);


        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        //TODO : Cancel the return should not be allowed
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        //TODO : Cancel the return should not be allowed
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);
        //TODO : Cancel the return should not be allowed
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        //TODO : Cancel the return should not be allowed
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_APPROVE, false);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKUP_SUCCESSFUL, " Pickup expected to be successful");
        //TODO : Cancel the return should not be allowed
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);


        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_SUCCESSFUL, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RETURN_SUCCESSFUL);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

    }

    // Create Return Mark PQCP DC ON HOLD(REJECT), CC REJECT
    @Test(description = "C27899")
    public void pqcpDcOnHoldCCReject() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        //Long returnID = createReturn();
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.DL, "560068", "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID1));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        Thread.sleep(3000);
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "abhi", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));
        Thread.sleep(15000);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);


        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);
        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_REJECTED, false);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.REJECTED_ONHOLD_PICKUP_WITH_DC, "QC rejected by cc status mismatch");
        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);


        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_REJECTED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RETURN_REJECTED);

        //Please create another return and cancel it for the same shipment
        //Assert.assertFalse(true,"PLEASE CREATE RETURN FOR SAME SHIPMENT AND CANCEL IT ");
        //reship to customer
        String filePath=storeHelper.createCSVFile(returnID,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,trackingNo);
        String reshipToCustomerResponse = null;
        int i=0;
        while(i<2){
            reshipToCustomerResponse=storeHelper.reshipToCustomer(filePath);
            i++;
        }
        Assert.assertTrue(reshipToCustomerResponse.contains(ResponseMessageConstants.reshipToCustomer),"Return order is not reshipped to customer");

        //creating another return and cancelling it
        returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "abhi", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        Thread.sleep(7000);
        returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();

        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2("" + returnID);
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnID));
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);

        //TODO : created another return now cancel it
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID, EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

    // Mark customer on hold(Shipment with customer) cc Reject
    @Test(description = "C27899")
    public void onHoldWithCustomerCCReject() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);


        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);


        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER, "shipment status mismatch");

        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(tripId)).equalsIgnoreCase(EnumSCM.SUCCESS));

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_REJECTED, true);
        Thread.sleep(7000);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER, "Return rejected by cc status mismatch");
        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);

        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);


        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_REJECTED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RETURN_REJECTED);

    }

    //on hold with customer cc approve assign to trip cancellation should not be allowed
    @Test(description = " C27898  C27897 C27905")
    public void OnHoldWithCustomerCCApproveAssignTrip() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.ON_HOLD_DAMAGED_PRODUCT, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.ONHOLD_PICKUP_WITH_CUSTOMER, "shipment status mismatch");

        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(tripId)).equalsIgnoreCase(EnumSCM.SUCCESS));

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_APPROVE, true);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.UNASSIGNED, "Return rejected by cc status mismatch");
        //assign to trip
         tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
         tripNumber = tripResponse.getTrips().get(0).getTripNumber();
         tripId = tripResponse.getTrips().get(0).getId();
         scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);


        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.FAILED_PICKUP, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RETURN_CREATED);
    }


     // PQCP DC onhold cc reject reship to customer.
    @Test(description = "C27901")
    public void pqcpDcOnHoldCCRejectReshipToCustomer() throws Exception {

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);


        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);
        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        //complete trip
        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");

        //TODO : cancellation should not be allowed
        //cc reject
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_REJECTED, true);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.REJECTED_ONHOLD_PICKUP_WITH_DC, "QC rejected by cc status mismatch");
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        StoreHelper storeHelper = new StoreHelper(env,LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
        String filePath=storeHelper.createCSVFile(returnID,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,trackingNo);
        String reshipToCustomerResponse = null;
        int i=0;
        while(i<2){
            reshipToCustomerResponse=storeHelper.reshipToCustomer(filePath);
            i++;
        }
        Assert.assertTrue(reshipToCustomerResponse.contains(ResponseMessageConstants.reshipToCustomer),"Return order is not reshipped to customer");

        StoreValidator storeValidator = new StoreValidator();
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse9=storeHelper.getReturnStatusInLMS(returnID.toString(),LMS_CONSTANTS.TENANTID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse9, com.myntra.lastmile.client.status.ShipmentStatus.RESHIPPED_DELIVERED_TO_CUSTOMER);

        //TODO : cancellation should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_REJECTED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RESHIPPED_DELIVERED_TO_CUSTOMER);

    }

    // dc onhold cc approve  acknowledge approve pickup
    @Test(description = "C27900 C27903")
    public void dcOnHoldCCApproveAckApprovePickup() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);

        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);


        // Start Trip
        TripOrderAssignmentResponse startTripRes = tripClient_qa.startTrip(tripId);
        Assert.assertEquals(startTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Map<String, Object> toaId = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select id from trip_order_assignment where trip_id = " + tripId, "lms");
        String tripOrderAssignmentId = toaId.get("id").toString();

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);


        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKUP_SUCCESSFUL_QC_PENDING, EnumSCM.TRIP_COMPLETE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to complete Trip.");
        lmsHelper.updateOperationalTrackingNum(trackingNo);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "false", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKED_UP, "shipment status mismatch");

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        //validateDuringReconciliation(deliveryStaffID, DcId, trackingNo.substring(2, trackingNo.length()),1L,EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNo.substring(2, trackingNo.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), "true", "Wrong status in trip_order_assignment");

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        lms_returnHelper.mlOpenBoxQC(String.valueOf(returnID), ShipmentUpdateEvent.PICKUP_ON_HOLD, trackingNo);
        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        //complete trip
//        response1 = lmsServiceHelper.updatePickupInTrip(Long.parseLong(tripOrderAssignmentId), EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.TRIP_COMPLETE);
//        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete Trip.");
//        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(tripId)).equalsIgnoreCase(EnumSCM.SUCCESS));

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(tripId)).equalsIgnoreCase(EnumSCM.SUCCESS));
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);


        lms_returnHelper.returnApproveOrReject(String.valueOf(returnID), LMS_CONSTANTS.RETURN_APPROVE, false);
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(trackingNo), EnumSCM.PICKUP_SUCCESSFUL, " Pickup expected to be successful");
        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        //TODO : acknowledge approve pickup
        lmsServiceHelper.AcknowledgeApprovePickup(Long.parseLong(DcId), trackingNo);
        //TODO :  Cancellation Should not be ALLOWED
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_SUCCESSFUL, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RETURN_SUCCESSFUL);

    }

//adding cancelled return to a trip should not be allowed
    @Test(description = "C27904")
    public void addCancelledReturnToTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();


        //TODO : Cancellation should be allowed.
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        // After cancelling shipment should not be assigned to the trip
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);


        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.ERROR);

    }

    // Add return to a trip -> remove from trip -> add to another trip cancellation should not be allowed
    @Test(description = "C27928")
    public void assignReturnToSecondTrip() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();

        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : Cancel the return should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);

        //unassign order to trip
        Assert.assertEquals(((TripOrderAssignmentResponse) tripClient_qa.unassignOrderFromTripThroughTripId(packetId, trackingNo, tripId, ShipmentType.DL)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //reassign
        String deliveryStaffID1 = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO : cancellation should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_CREATED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RETURN_CREATED);

    }

    // create return assign to trip remove from trip assign to store cancellation should not be allowed
    @Test(description = "C27917 C27929")
    public void assignReturnToStore() throws Exception {
        //Create Mock order Till DL
        createStore();
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String deliveryCenterName=orderResponse.getOrders().get(0).getDeliveryCenterCode();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2=storeHelper.getReturnStatusInLMS(returnId.toString(),LASTMILE_CONSTANTS.TENANT_ID,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse2, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnHub=returnResponse2.getDomainReturnShipments().get(0).getReturnHubCode();

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //assign and unassign the order
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, returnTrackingNumber);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //unassign order from trip
        Assert.assertEquals(((TripOrderAssignmentResponse) tripClient_qa.unassignOrderFromTripThroughTripId(packetId, returnTrackingNumber, tripId, ShipmentType.DL)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);


        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,LASTMILE_CONSTANTS.TENANT_ID,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.UNASSIGNED);

        //TODO : Cancellation should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnId),false);
        returnResponse2  = lmsServiceHelper.cancelReturn(String.valueOf(returnId), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse2.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnResponse2 = lmsServiceHelper.cancelReturn(String.valueOf(returnId), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.CustomerCare);
        Assert.assertEquals(returnResponse2.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(returnTrackingNumber);
        Thread.sleep(4000);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_CREATED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RETURN_CREATED);


    }


    //Cancellation of picked up return added to reverse bag should not be allowed
    @Test(description = "C27916")
    public void returnAddedToRevBag() throws Exception {
        //Create Mock order Till DL
        createStore();
        String tenantId = LASTMILE_CONSTANTS.TENANT_ID;
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType,LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        storeValidator.isNull(packetId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder),"ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        Long originPremiseId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        String deliveryCenterName=orderResponse.getOrders().get(0).getDeliveryCenterCode();

        //get order tracking number
        OrderResponse lmsOrderDetails = lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID);
        String trackingNo = lmsOrderDetails.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create Return
        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnId = returnResponse.getData().get(0).getId();
        storeValidator.isNull(returnId.toString());

        // Validate the respective RMS and LMS return fields
        lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
        //manifest return order
        lms_returnHelper.manifestOpenBoxPickups(String.valueOf(returnId));
        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse2=storeHelper.getReturnStatusInLMS(returnId.toString(),tenantId,Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse2, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnHub=returnResponse2.getDomainReturnShipments().get(0).getReturnHubCode();

        //getReturnTrackingNumber
        ReturnResponse returnResponse1 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId));
        String returnTrackingNumber = returnResponse1.getData().get(0).getReturnTrackingDetailsEntry().getTrackingNo();
        storeValidator.isNull(returnTrackingNumber);

        //AssignPickupToHLP
        String statusType = (String) storeHelper.assignPickupToHLPWithCourierCode.apply(returnTrackingNumber, code, tenantId);
        Assert.assertTrue(statusType.equalsIgnoreCase(ResponseMessageConstants.success1), "Return Order is not assigned to store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.HANDED_TO_LAST_MILE_PARTNER);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.UNASSIGNED);

        //Assign pickup to Store SDA
        List<String> lstTrackingNumbers = new ArrayList<>();
        lstTrackingNumbers.add(returnTrackingNumber);
        TripResponse tripResponse = mlShipmentClientV2_qa.createStoreTrip(lstTrackingNumbers, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");
        Long storetripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storetripId.toString());

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.OUT_FOR_PICKUP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.OUT_FOR_PICKUP);

        //get tripOrderAssignment id
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.findOrdersByTripId(String.valueOf(storetripId), ShipmentType.PU, storeTenantId);
        Long storeTripOrderId = tripOrderAssignmentResponse.getTripOrders().get(0).getId();
        storeValidator.isNull(storeTripOrderId.toString());
        //update order as Pickup Successful in store
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = lastmileClient.updatePickupInTrip(storeTripOrderId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE,tenantId);
        Assert.assertTrue(tripOrderAssignmentResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Status message is not matching");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //Create Reverse Trip
        Long dcSDAId = null;
        DeliveryStaffResponse deliveryStaffResponse = storeHelper.getDeliverySDABasedOnDC(originPremiseId, tenantId, 0, 20, "code", "DESC");
        Assert.assertTrue(deliveryStaffResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedDeliveryStaff),"Status message is not matching");

        List<DeliveryStaffEntry> lstDeliveryStaffEntry = deliveryStaffResponse.getDeliveryStaffs();
        if (!(lstDeliveryStaffEntry.size() == 0)) {
            dcSDAId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        } else {
            Assert.fail("Delivery Staff entry is empty for given DC id");
        }
        storeValidator.isNull(String.valueOf(dcSDAId));

        TripResponse tripResponse1 = tripClient_qa.createTrip(originPremiseId, dcSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.tripAdded), "Trip is not created successfully");
        List<TripEntry> lstTripEntry = tripResponse1.getTrips();
        Long reverseTripId = null;
        if (!(lstTripEntry.size() == 0)) {
            reverseTripId = lstTripEntry.get(0).getId();
        } else {
            Assert.fail("Trip entry is empty ");
        }
        storeValidator.isNull(String.valueOf(reverseTripId));
        statusPollingValidator.validateTripStatus(reverseTripId, TripStatus.CREATED.toString());

        //create reverse MB
        String reverseShipmentId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, tenantId);
        storeValidator.isNull(String.valueOf(reverseShipmentId));
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(reverseShipmentId), com.myntra.lms.client.status.ShipmentStatus.ADDED_TO_TRIP.toString());
        //Start trip
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.startTrip(reverseTripId);
        Assert.assertTrue(tripOrderAssignmentResponse3.getStatus().getStatusType().toString().equalsIgnoreCase(EnumSCM.SUCCESS),"Status message is not matching");
        //validate trip status
        statusPollingValidator.validateTripStatus(reverseTripId,TripStatus.OUT_FOR_DELIVERY.toString());
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //Collect orders from store
        List<String> lstreturnTrackingNumber = new ArrayList<>();
        lstreturnTrackingNumber.add(returnTrackingNumber);
        TripShipmentAssociationResponse tripShipmentAssociationResponse = storeHelper.pickupReverseBagFromStore(reverseShipmentId, lstreturnTrackingNumber, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY,
                3, 4, 0, tenantId, TripOrderStatus.PS,ShipmentType.PU);
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "All the orders was not picked up successfully from store");

        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.PICKED_UP);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);


        //TODO : Cancellation should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnId),false);
        returnResponse2  = lmsServiceHelper.cancelReturn(String.valueOf(returnId), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse2.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnResponse2 = lmsServiceHelper.cancelReturn(String.valueOf(returnId), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.CustomerCare);
        Assert.assertEquals(returnResponse2.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(returnTrackingNumber);
        Thread.sleep(4000);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_DONE, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.PICKUP_DONE);


        //Receive In DC
        lmsHelper.updateOperationalTrackingNum(returnTrackingNumber);
        String receiveInDc1 = storeHelper.receiveShipmentInDC(returnTrackingNumber.substring(2, trackingNo.length()), tenantId);
        Assert.assertTrue(receiveInDc1.contains(ResponseMessageConstants.success1), "Order is not received in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,tenantId,EnumSCM.RECEIVED_IN_DC);
        statusPollingValidator.validateML_ShipmentStatus(returnTrackingNumber,storeTenantId,EnumSCM.PICKUP_SUCCESSFUL);

        //TODO : Cancellation should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnId),false);
        returnResponse2  = lmsServiceHelper.cancelReturn(String.valueOf(returnId), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse2.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnResponse2 = lmsServiceHelper.cancelReturn(String.valueOf(returnId), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.CustomerCare);
        Assert.assertEquals(returnResponse2.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(returnTrackingNumber);
        Thread.sleep(4000);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(returnTrackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_DONE, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.PICKUP_DONE);


    }

    // pickup successful item  should not be allowed to cancel
    @Test(description = "C27908")
    public void pickupSuccessful() throws Exception {

        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();

        lmsReturnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.PICKED_UP_SUCCESSFULLY);
        //cancelling
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_SUCCESSFUL, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.DELIVERED_TO_SELLER);
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
    }

    //Reshedule pickup cancellation should be allowed by customer
    @Test(description = "C27910")
    public void reschedulePickup() throws Exception {

        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();

        log.info("________ReturnId: " + returnID);
        lmsReturnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.RESCHEDULED_CUSTOMER_NOT_AVAILABLE);
        //TODO: cancellation should be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

    }

    // FP -> RTO -> cancellation not allowed
    @Test(description = "C27911")
    public void fpRTO() throws Exception {
        lmsHelper.setNdrConfigTo0();
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
        lms_returnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.FAILED_PICKUP);
        String trackingNo = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();
        Map<String, Object> shipment_status = (Map<String, Object>) lmsHelper.getShipmentStatusForPickup.apply(trackingNo);
        Assert.assertEquals(shipment_status.get("shipment_status"), EnumSCM.FAILED_PICKUP, "Expected failed pickup");
        //rto
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());

        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(trackingNo);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("failed pickup rejected");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RETURN_REJECTED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.PU);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");
        Thread.sleep(7000);
        Map<String, Object> shipment_status1 = (Map<String, Object>) lmsHelper.getShipmentStatusForPickup.apply(trackingNo);
        Assert.assertEquals(shipment_status1.get("shipment_status"), EnumSCM.PICKUP_REJECTED, "Expected failed pickup");

        //TODO : cancellation should not be allowed
//        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_REJECTED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RETURN_REJECTED);

    }

    //return of exchange order -> cancellation allowed

    @Test(description = "C27912")
    public void returnOfExcOrder() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripNumber, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateTripOrderStatus(EnumSCM.OFD);
        tripOrderAssignmentResponseValidator.validateTripId(tripId);
        tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNumber);
        tripOrderAssignmentResponseValidator.validateExchangeOrderId(packetId);
        Assert.assertEquals(tripOrderAssignmentResponse.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant Id in response");
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        // validateDuringReconciliation(deliveryStaffID, DcId, trackingNumber.substring(2, trackingNumber.length()), 1L, EnumSCM.SUCCESS);
        tripOrderAssignmentResponse = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");


        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(exchangeOrder));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());

        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        lmsReturnHelper.manifestOpenBoxPickups(returnID.toString());
        Thread.sleep(4000);

        //Cancellation should  be allowed
//        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
//        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
//        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        String trackingNo = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();

        Thread.sleep(3000);
        //Cancelation of return should be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);
    }

    //cancellation of try and buy return should be allowed
    @Test(description = "C27913")
    public void returnTryNBuyOrder() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);
        Double amount = orderEntryDetails.getFinalAmount();


        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("Bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        //reconcile
        returnCancellationValidator.checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderID));
        OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());

        ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 49L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LASTMILE_CONSTANTS.MOBILE_NUMBER_RETURN);
        Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");

        Long returnID = returnResponse.getData().get(0).getId();
        log.info("________ReturnId: " + returnID);
        lmsReturnHelper.manifestOpenBoxPickups(String.valueOf(returnID));

        //lms_returnHelper.processOpenBoxReturn(returnID.toString(), EnumSCM.PICKED_UP_SUCCESSFULLY);
        Thread.sleep(4000);
        String trackingNo = (String) lmsHelper.getReturnsTrackingNumber.apply(returnID);

        //Cancellation should be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        //checkReturnShipmentStatus(returnShipmentID);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);


    }

    //cancel a return and mark it PQCP
    @Test(description = "C27914")
    public void cancelReturnMarkPQCP() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));

        Long returnID = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnID));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        log.info("________ReturnId: " + returnID);
        //TODO : Cancellation should be allowed.
//        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),true);
        //cancellation
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnID));
        String trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        Thread.sleep(3000);
        //Cancelation of return should be allowed
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnID),trackingNo);

        //after cancelling the return mark it PQCP

        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Long tripId = tripResponse.getTrips().get(0).getId();
        TripOrderAssignmentResponse scanTrackingNoInTripRes = lmsServiceHelper.assignOrderToTrip(tripId, trackingNo);
        Assert.assertEquals(scanTrackingNoInTripRes.getStatus().getStatusType().toString(), EnumSCM.ERROR);

    }

    //cancellation of return created for exchange order should not be allowed
    @Test(description = "C28138")
    public void cancelReturnCreatedForExcOrder() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String returnID = lmsHelper.getSourceReturnId(packetId);
        String trackingNo = lmsHelper.getReturnsTrackingNumber.apply(returnID).toString();

        //cancellation of return of exchange order should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_CREATED, "Pickup shipment not in cancelled state");


    }

    //cancellation  of return of tried and not bought order should not be allowed
    @Test(description = "C28139")
    public void cancelReturnOfTryNbuy() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String deliveryStaffID = "" + lmsServiceHelper.getAndAddDeliveryStaffID(Long.parseLong(DcId));
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        Double amount = orderEntryDetails.getFinalAmount();
        OrderResponse orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNumber);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);
        itemEntry.setRemarks("not bought");
        itemEntry.setQcStatus(ItemQCStatus.PASSED);
        itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
// mark delivered and reconcile shouldn't work

        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        String ml_shipment_id = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
        String return_tracking_num = lmsHelper.updateOperationalTrackingNumForTryAndBuy(ml_shipment_id);
        //reconcile
        String returnID = lmsHelper.getSourceRefIdFromTrackingNum(return_tracking_num);
        //cancel should be unsuccessful
//        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnID),false);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + return_tracking_num);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + return_tracking_num);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(return_tracking_num, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(return_tracking_num);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnID), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + return_tracking_num);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(return_tracking_num, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_SUCCESSFUL, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RETURN_SUCCESSFUL);
        returnCancellationValidator.checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(return_tracking_num.substring(2, return_tracking_num.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Long tripOrderAssignmentIdForReturn = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(return_tracking_num);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForReturn)), "true", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(return_tracking_num), EnumSCM.PICKUP_SUCCESSFUL, "Status not updated to received in DC");
        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

    }

    //green channel return cancellation should not be allowed after refund approved
    @Test(description = "C27907")
    public void greenChannelApprovedReturn() throws Exception {
        Long returnId1 = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId1));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        String trackingNo = lmsHelper.getReturnsTrackingNumber.apply(returnId1).toString();

        //approve this return using green channel
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId1));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse03 = storeHelper.getReturnStatusInLMS(returnId1.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse03, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber1 = returnResponse03.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber1);
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        //TODO Green channel approval is not upload successfully when you hit first time
        String filePath1 = storeHelper.createCSVFileForGreenChannel(Arrays.asList(new String[]{returnTrackingNumber1}), LMS_CONSTANTS.TENANTID);
        String greenChannelResponse1 = storeHelper.uploadGreenChannel(filePath1);
        if(!(greenChannelResponse1.contains(ResponseMessageConstants.reshipToCustomer))){
            greenChannelResponse1 = storeHelper.uploadGreenChannel(filePath1);
        }
        Assert.assertTrue(greenChannelResponse1.contains(ResponseMessageConstants.reshipToCustomer), "Green channel update is not completed successfully");

        //check the return shipment status
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse04 = storeHelper.getReturnStatusInLMS(returnId1.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        Assert.assertEquals(returnResponse04.getDomainReturnShipments().get(0).getApprovalFlag().toString(), "APPROVED", "Approval flag is not expected ");
        Assert.assertEquals(returnResponse04.getDomainReturnShipments().get(0).getGreenChannelApproved().toString(), "true", "Green channel approved value is not expected ");
        //check the refund status in RMS
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse05 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId1));
        Assert.assertEquals(returnResponse05.getStatus().getStatusMessage(), ResponseMessageConstants.returnReleseRetrieved, "Return is not retrieved successfully");
        Assert.assertEquals(returnResponse05.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "true", "Refund is not expected");

        //Cancellation should not be allowed
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnId1),false);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnId1), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnId1), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(4000);
        returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnId1), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.PICKUP_CREATED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.RETURN_CREATED);


    }

    // green channel approval for a cancelled return should be blocked
    @Test(description = "C27915")
    public void greenChannelApprovedForCanceledReturn() throws Exception {
        Long returnId1 = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId1));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        String trackingNo = lmsHelper.getReturnsTrackingNumber.apply(returnId1).toString();

        //approve this return using green channel
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId1));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse03 = storeHelper.getReturnStatusInLMS(returnId1.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse03, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber1 = returnResponse03.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber1);
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        // cancel return by customer
        returnCancellationValidator.validateIsCancellableFlag(String.valueOf(returnId1),true);
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnId1));
        trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(8000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnId1), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.Customer);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnId1),trackingNo);

        //TODO Green channel approval is not upload successfully when you hit first time
        String filePath1 = storeHelper.createCSVFileForGreenChannel(Arrays.asList(new String[] {returnTrackingNumber1}), LMS_CONSTANTS.TENANTID);
        String greenChannelResponse1 = storeHelper.uploadGreenChannel(filePath1);
        if(!(greenChannelResponse1.contains(ResponseMessageConstants.reshipToCustomer))){
            greenChannelResponse1 = storeHelper.uploadGreenChannel(filePath1);
        }
        Assert.assertTrue(greenChannelResponse1.contains("failed"), "Green channel update is not completed successfully");

    }
    // green channel approval for a cancelled return should be blocked customercare
    @Test(description = "C27915")
    public void greenChannelApprovalBlockedForCanceledReturnByCC() throws Exception {
        Long returnId1 = createReturn();
        ReturnResponse returnResponse = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId1));
        String orderID1 = String.valueOf(returnResponse.getData().get(0).getOrderId());
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        String trackingNo = lmsHelper.getReturnsTrackingNumber.apply(returnId1).toString();

        //approve this return using green channel
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        ReturnResponse returnResponse02 = rmsServiceHelper.getReturnDetailsNew(String.valueOf(returnId1));
        storeValidator.validateReturnOrderStatusInRMS(returnResponse02, ReturnStatus.LPI);
        Assert.assertEquals(returnResponse02.getData().get(0).getReturnRefundDetailsEntry().getRefunded().toString(), "false", "Refund is not expected");

        //validate return status in LMS
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse03 = storeHelper.getReturnStatusInLMS(returnId1.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
        storeValidator.validateReturnOrderStatusInLMS(returnResponse03, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
        String returnTrackingNumber1 = returnResponse03.getDomainReturnShipments().get(0).getTrackingNumber();
        storeValidator.isNull(returnTrackingNumber1);
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getApprovalFlag(), "Approval flag is not null ");
        Assert.assertNull(returnResponse03.getDomainReturnShipments().get(0).getGreenChannelApproved(), "Green channel approved value is not null ");

        // cancel return by customer
        OrderResponse returnShipmentLMSObject = lms_returnHelper.getReturnShipmentDetailsLMS(String.valueOf(returnId1));
        trackingNo = returnShipmentLMSObject.getOrders().get(0).getTrackingNumber();
        PickupResponse pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        String returnShipmentID = returnCancellationValidator.getReturnShipmentIdByTrackingNumber(trackingNo);
        Thread.sleep(8000);
        com.myntra.lms.client.domain.response.ReturnResponse returnResponse1 = lmsServiceHelper.cancelReturn(String.valueOf(returnId1), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, ShipmentUpdateActivityTypeSource.CustomerCare);
        Assert.assertEquals(returnResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to cancel the order with trackingNo" + trackingNo);
        pickupResponse = pickupShipmentClient_qa.findPickupByTrackingNumber(trackingNo, LASTMILE_CONSTANTS.TENANT_ID);
        //validation
        String pickupShipmentID = pickupResponse.getDomainPickupShipments().get(0).getSourceReferenceId();
        Assert.assertEquals(pickupResponse.getDomainPickupShipments().get(0).getShipmentStatus().toString(), EnumSCM.CANCELLED, "Pickup shipment not in cancelled state");
        returnCancellationValidator.checkReturnShipmentStatus(returnShipmentID,EnumSCM.CANCELLED);
        returnCancellationValidator.validateAssociation(pickupShipmentID, returnShipmentID, 0, 0, String.valueOf(returnId1),trackingNo);

        //TODO Green channel approval is not upload successfully when you hit first time
        String filePath1 = storeHelper.createCSVFileForGreenChannel(Arrays.asList(new String[] {returnTrackingNumber1}), LMS_CONSTANTS.TENANTID);
        String greenChannelResponse1 = storeHelper.uploadGreenChannel(filePath1);
        if(!(greenChannelResponse1.contains(ResponseMessageConstants.reshipToCustomer))){
            greenChannelResponse1 = storeHelper.uploadGreenChannel(filePath1);
        }
        Assert.assertTrue(greenChannelResponse1.contains("failed"), "Green channel update is not completed successfully");

    }
}


