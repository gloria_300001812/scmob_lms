package com.myntra.apiTests.erpservices.khata;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.apiTests.erpservices.khata.validator.KhataAccountValidator;
import com.myntra.apiTests.erpservices.khata.validator.PaymentValidator;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.response.AccountAssociationResponse;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import lombok.SneakyThrows;
import org.testng.Assert;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.*;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class khata_accounts {
    KhataServiceHelper khataHelper = new KhataServiceHelper();


    @Test(dataProviderClass = Account_dp.class, dataProvider = "createAccount")
    public void createAccountinKhata (Boolean isActive , AccountType accountType , String contactNumber , String ownerRerId , Long parentAccountId ,AccountType parentAccountType , Long reportingParent ,String tenantId ) throws UnsupportedEncodingException, JAXBException  {
        AccountResponse response = KhataServiceHelper.createAccount(isActive ,accountType,contactNumber,ownerRerId,parentAccountId,parentAccountType,reportingParent,tenantId);
        KhataAccountValidator.validateAccountCreation(Long.parseLong(ownerRerId),"Automation","Test",contactNumber,1l,accountType,true);
    }

    @Test
    public void getAccountinKhataById () throws UnsupportedEncodingException, JAXBException  {
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyId(2l);
        Map<String, Object> accountEntries = DBUtilities.exSelectQueryForSingleRecord("select * from account where id = 2","myntra_khata");
        Assert.assertEquals(accountResponse.getAccounts().get(0).getOwnerReferenceId(),accountEntries.get("owner_ref_id"));
        Assert.assertEquals(accountResponse.getAccounts().get(0).getType().toString(),accountEntries.get("type").toString());
        Assert.assertEquals(accountResponse.getAccounts().get(0).getIsActive(),accountEntries.get("is_active"));
    }

    @Test
    public void getAccountinKhataByType () throws UnsupportedEncodingException, JAXBException  {
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyType(AccountType.PARTNER);
        System.out.println(accountResponse);
    }

    @Test(dataProviderClass = Account_dp.class, dataProvider = "createAccount")
    public void updateAccountByIdinKhata (Boolean isActive , AccountType accountType , String contactNumber , String ownerRerId , Long parentAccountId ,AccountType parentAccountType , Long reportingParent ,String tenantId ) throws UnsupportedEncodingException, JAXBException  {
        AccountResponse response = KhataServiceHelper.createAccount(isActive ,accountType,contactNumber,ownerRerId,parentAccountId,parentAccountType,reportingParent,tenantId);
        Long accountId = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(ownerRerId),accountType).getAccounts().get(0).getId();
        AccountResponse accountResponse = KhataServiceHelper.updateAccountbyId(accountId , false,AccountType.PARTNER,contactNumber , ownerRerId , parentAccountId , accountType , reportingParent ,tenantId );
        KhataAccountValidator.validateAccountCreation(Long.parseLong(ownerRerId),"Automation","Test",contactNumber,1l,accountType,false);
    }

    @Test(dataProviderClass = Account_dp.class, dataProvider = "OwnerReferenceandType")
    public void getAccountinKhataByOwnerReferenceandType (Long ownerRefId , AccountType accountType) throws UnsupportedEncodingException, JAXBException  {
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(ownerRefId , accountType);
        Map<String, Object> accountEntries = DBUtilities.exSelectQueryForSingleRecord("select * from account where owner_ref_id = "+ownerRefId+" and type = '"+accountType+"'","myntra_khata");
        Assert.assertEquals(accountResponse.getAccounts().get(0).getOwnerReferenceId(),accountEntries.get("owner_ref_id"));
        Assert.assertEquals(accountResponse.getAccounts().get(0).getType().toString(),accountEntries.get("type").toString());
        Assert.assertEquals(accountResponse.getAccounts().get(0).getIsActive(),accountEntries.get("is_active"));
    }

    @Test(dataProviderClass = Account_dp.class, dataProvider = "createAccount")
    public void updateAccountinKhata (Boolean isActive , AccountType accountType , String contactNumber , String ownerRerId , Long parentAccountId ,AccountType parentAccountType , Long reportingParent ,String tenantId ) throws UnsupportedEncodingException, JAXBException  {
        AccountResponse response = KhataServiceHelper.createAccount(isActive ,accountType,contactNumber,ownerRerId,parentAccountId,parentAccountType,reportingParent,tenantId);
        AccountResponse accountResponse = KhataServiceHelper.updateAccount(false,AccountType.PARTNER,contactNumber , ownerRerId , parentAccountId , accountType , reportingParent ,tenantId );
        KhataAccountValidator.validateAccountCreation(Long.parseLong(ownerRerId),"Automation","Test",contactNumber,1l,accountType,false);
    }

    @Test
    public void searchAccountinKhata () throws UnsupportedEncodingException, JAXBException  {
        AccountResponse accountResponse = KhataServiceHelper.searchAccountByOwnerRefId(4019l);
        Map<String, Object> accountEntries = DBUtilities.exSelectQueryForSingleRecord("select * from account where owner_ref_id  = 4019","myntra_khata");
        Assert.assertEquals(accountResponse.getAccounts().get(0).getOwnerReferenceId(),accountEntries.get("owner_ref_id"));
        Assert.assertEquals(accountResponse.getAccounts().get(0).getType().toString(),accountEntries.get("type").toString());
        Assert.assertEquals(accountResponse.getAccounts().get(0).getIsActive(),accountEntries.get("is_active"));
    }

    @Test
    public void getAccountAssociationById () throws UnsupportedEncodingException, JAXBException  {
        AccountAssociationResponse accountAssociationResponse = KhataServiceHelper.getAccountAssociationById(11l);
        Map<String, Object> accountAssociateEntries = DBUtilities.exSelectQueryForSingleRecord("select * from account_association where id = 11","myntra_khata");
        Assert.assertEquals(accountAssociationResponse.getAccountAssociation().get(0).getAssociateAccountId(),accountAssociateEntries.get("associate_account_id"));
        Assert.assertEquals(accountAssociationResponse.getAccountAssociation().get(0).getParentAccountId(),accountAssociateEntries.get("parent_account_id"));
        System.out.println(accountAssociationResponse);
    }

    @Test
    public void updateAccountAssociationByIdinKhata () throws UnsupportedEncodingException, JAXBException  {
        AccountAssociationResponse accountAssociationResponse = KhataServiceHelper.updateAccountAssociationById(11l, 81l , 1l);
        KhataAccountValidator.validateAccountAssociationCreation(81l,1l);
        System.out.println(accountAssociationResponse);
    }

    @Test
    public void createAccountAssociationinKhata () throws UnsupportedEncodingException, JAXBException  {
        AccountAssociationResponse accountAssociationResponse = KhataServiceHelper.createAccountAssociation(81l , 1l);
        KhataAccountValidator.validateAccountAssociationCreation(81l,1l);
    }

    @Test
    public void getAccountAssociationByParentAccountId () throws UnsupportedEncodingException , JAXBException {
        AccountAssociationResponse accountAssociationResponse = KhataServiceHelper.getAccountAssociationbyparentAccount(1l);
        System.out.println(accountAssociationResponse);
    }

    @Test
    public void searchAccountAssociation () throws UnsupportedEncodingException , JAXBException {
        AccountAssociationResponse accountAssociationResponse = KhataServiceHelper.searchAccountAssociation("100" , 1l , "createdOn");
        System.out.println(accountAssociationResponse);
    }

    @Test
    public void getAccountPendencyById () throws UnsupportedEncodingException , JAXBException {
        AccountPendencyResponse accountPendencyResponseResponse = KhataServiceHelper.getAccountPendencyById(12l);
        System.out.println(accountPendencyResponseResponse);
    }

    @Test
    public void updateAccountPendencyByIdinKhata () throws UnsupportedEncodingException , JAXBException {
        AccountPendencyResponse accountPendencyResponse = KhataServiceHelper.updateAccountPendencyById(12l,1l, 1l, 0.0f,0.0f);
        System.out.println(accountPendencyResponse);
    }

    @Test
    public void createAccountPendencyinKhata () throws  UnsupportedEncodingException , JAXBException {
        AccountPendencyResponse accountPendencyResponse = KhataServiceHelper.createAccountPendency(1l, 1l, 0.0f,0.0f);
        System.out.println(accountPendencyResponse);
    }

    @Test
    public void getAccountPendencyByAccount () throws UnsupportedEncodingException , JAXBException {
        AccountPendencyResponse accountPendencyResponse = KhataServiceHelper.getAccountPendencyByaccount(1l);
        System.out.println(accountPendencyResponse);

    }

//    @Test
//    public void getAccountPendencyByOwnerRefIdandType () throws UnsupportedEncodingException , JAXBException {
//        AccountPendencyResponse accountPendencyResponse = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(9999l , AccountType.PARTNER,1l, AccountType.SDA);
//        System.out.println(accountPendencyResponse);
//
//    }

    @Test
    public void createLedger () throws UnsupportedEncodingException, JAXBException {
        LedgerResponse response =  khataHelper.createLedger(5L, 1L, 1000F, "e3","4019", LedgerType.GOODS);
        System.out.println(response);
    }

    @Test
    public void getBulkLedger () throws UnsupportedEncodingException, JAXBException {
        LedgerResponse response =  khataHelper.getBulkLedger("5", "4019", LedgerType.GOODS.toString(), "3","1");
        System.out.println(response);
    }

    @Test
    public void ledgerSearchByParam() throws UnsupportedEncodingException, JAXBException {
        String searchparam = "search?accountId=5&type=GOODS";
        LedgerResponse response =  khataHelper.ledgerSearchByParam(searchparam);
        System.out.println(response);
    }

    @Test
    public void getLedgerDeatilsById() throws UnsupportedEncodingException, JAXBException {
        String ledgerId = "38";
        LedgerResponse response =  khataHelper.getLedgerDetailsByLedgerId(ledgerId);
        System.out.println(response);
    }

    @Test
    public void ledgerUpdate() throws UnsupportedEncodingException, JAXBException {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        LedgerResponse response =  khataHelper.ledgerUpdate(5L, 1094F,"system",  new Date(), "e3", 1L, 42L, 0L, new Date(), "test", "4019", LedgerType.CASH);
        System.out.println(response);
    }

    @Test
    public void bulkCreateLedger () throws UnsupportedEncodingException, JAXBException {
        LedgerResponse response =  khataHelper.bulkCreateLedger(5L, 1L, 1000F, "e3","4019", LedgerType.GOODS, 3);
        System.out.println(response);
    }

    @Test
    public void createPayment() throws UnsupportedEncodingException, JAXBException {
        PaymentResponse response =  khataHelper.createPayment(81L, 1196L, 500F, PaymentType.CASH,"3456789");
        System.out.println(response);
    }

    @Test
    public void getPaymentById() throws UnsupportedEncodingException, JAXBException {
        PaymentResponse response =  khataHelper.getPaymentById("1");
        System.out.println(response);
    }

    @Test
    public void paymentUpdate() throws UnsupportedEncodingException, JAXBException {
        PaymentResponse response =  khataHelper.paymentUpdate(81L, 1196L, 500F, PaymentType.CASH, 1L, PaymentStatus.PENDING );
        System.out.println(response);
    }

    @Test
    public void getPaymentDetailsFromAccountToAccount() throws UnsupportedEncodingException, JAXBException {
        //PaymentResponse response =  khataHelper.getPaymentDetailsFromAccountToAccount( "1196","81");
        PaymentValidator paymentValidator =new PaymentValidator();
        paymentValidator.paymentValidation("5483", "84", "50.0", "APPROVED");
        //System.out.println(response);
    }

    @Test
    public void paymentSearch() throws UnsupportedEncodingException, JAXBException {
        String param  = "search?fromAccount=1196&toAccount=81";
        PaymentResponse response =  khataHelper.paymentSearch(param);
        System.out.println(response);
    }
    @Test
    public void createAndApprove() throws UnsupportedEncodingException, JAXBException {
        PaymentResponse response =  khataHelper.createAndApprove(84L, 5483L, 50F, PaymentType.CASH);
        System.out.println(response);
    }

    @Test(dataProviderClass = Account_dp.class, dataProvider = "createAccount")
    public void createDuplicateAccountinKhata (Boolean isActive , AccountType accountType , String contactNumber , String ownerRerId , Long parentAccountId ,AccountType parentAccountType , Long reportingParent ,String tenantId ) throws UnsupportedEncodingException, JAXBException  {
        AccountResponse response = KhataServiceHelper.createAccount(isActive ,accountType,contactNumber,ownerRerId,parentAccountId,parentAccountType,reportingParent,tenantId);
        KhataAccountValidator.validateAccountCreation(Long.parseLong(ownerRerId),"Automation","Test",contactNumber,1l,accountType,true);
        AccountResponse response1 = KhataServiceHelper.createAccount(isActive ,accountType,contactNumber,ownerRerId,parentAccountId,parentAccountType,reportingParent,tenantId);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(),EnumSCM.ERROR,"Able to create Duplicate Account in Khata");
    }

    @Test
    public void createDuplicateAccountPendencyinKhata () throws  UnsupportedEncodingException , JAXBException {
        AccountPendencyResponse accountPendencyResponse = KhataServiceHelper.createAccountPendency(1l, 1l, 0.0f,0.0f);
        AccountPendencyResponse accountPendencyResponse1 = KhataServiceHelper.createAccountPendency(1l, 1l, 0.0f,0.0f);
        Assert.assertEquals(accountPendencyResponse1.getStatus().getStatusType().toString(),EnumSCM.ERROR,"Able to create Duplicate Account pendency in Khata");

    }

    @Test
    public void createDuplicateAccountAssociationinKhata () throws UnsupportedEncodingException , JAXBException{
        AccountAssociationResponse accountAssociationResponse = KhataServiceHelper.createAccountAssociation(81l , 1l);
        AccountAssociationResponse accountAssociationResponse1 = KhataServiceHelper.createAccountAssociation(81l , 1l);
        Assert.assertEquals(accountAssociationResponse1.getStatus().getStatusType().toString(),EnumSCM.ERROR,"Able to create Duplicate Account Association in Khata");
    }

    @Test
    public void createDuplicateLedger () throws UnsupportedEncodingException , JAXBException{
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        LedgerResponse response =  khataHelper.ledgerUpdate(5L, 1094F,"system",  new Date(), "e3", 1L, 42L, 0L, new Date(), "test", "4019", LedgerType.CASH);
        LedgerResponse response1 =  khataHelper.ledgerUpdate(5L, 1094F,"system",  new Date(), "e3", 1L, 42L, 0L, new Date(), "test", "4019", LedgerType.CASH);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(),EnumSCM.ERROR,"Able to create Duplicate LEdger in Khata");
    }

    @Test
    public void createDuplicatePayment () throws UnsupportedEncodingException , JAXBException{
        PaymentResponse response =  khataHelper.createPayment(81L, 1196L, 500F, PaymentType.CASH,"3456789");
        PaymentResponse response1 =  khataHelper.createPayment(81L, 1196L, 500F, PaymentType.CASH,"3456789");
        Assert.assertEquals(response1.getStatus().getStatusType().toString(),EnumSCM.ERROR,"Able to create Duplicate Payment for same paymentReferenceId in Khata");
    }


    @Test(dataProviderClass = Account_dp.class, dataProvider = "createExcess")
    public void createExcesPayment(Long accountId, Long fromAccountId, Float amount, PaymentType paymentType , Long paymentId, AccountType fromAccountType , String fromAccountOwnerRefId, AccountType AccountType , String AccountOwnerRefId , String expectedStatus ) throws UnsupportedOperationException, JAXBException, UnsupportedEncodingException {
        ExcessResponse excessResponse = khataHelper.createExcess( accountId, fromAccountId ,  amount,  paymentType ,  paymentId,  fromAccountType ,  fromAccountOwnerRefId,  AccountType ,  AccountOwnerRefId);
        Assert.assertEquals(excessResponse.getStatus().getStatusType().toString(),expectedStatus,"Unable to create excess payment");

    }

    @Test()
    public void test() throws JAXBException, UnsupportedEncodingException {
        ExcessResponse excessResponse = khataHelper.createExcess( 1l,82l,100.00f,PaymentType.EXCESS,43256l,AccountType.DC,"2",AccountType.PARTNER,"4019");

    }

    @Test(enabled = true,description = "TC ID: C28826 - CASH RECON : check getAccountPendency API with Null / invalid data. should throw proper error message.")
    @SneakyThrows
    public void getAccountPendencyByIdNegativeScenario() {
        AccountPendencyResponse accountPendencyResponse = KhataServiceHelper.getAccountPendencyById((long) new Random().nextInt());
        Assert.assertEquals(accountPendencyResponse.getStatus().getStatusMessage(),"Row with given id not found","Account pendency search successful when we gave id which is not there in DB");
    }
}
