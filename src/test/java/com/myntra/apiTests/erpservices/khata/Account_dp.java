package com.myntra.apiTests.erpservices.khata;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.PaymentType;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lordoftherings.Toolbox;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import scala.xml.Null;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.Random;

public class Account_dp {
    private static Object Random;

    @DataProvider
    public static Object[][] createAccount(ITestContext testContext) {
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        Object[] arr1 = {true, AccountType.PARTNER,""+contactNumber ,String.valueOf(contactNumber).substring(5, 9) , 1l , AccountType.PARTNER , 1l ,"4019"};
        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }
    @DataProvider
    public static Object[][] OwnerReferenceandType(ITestContext testContext) {
        Random r = new Random(System.currentTimeMillis());
        Object[] arr1 = {4019l,AccountType.PARTNER};
        Object[] arr2 = {5l,AccountType.DC};
        Object[][] dataSet = new Object[][]{arr1,arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }

    @DataProvider
    public static Object[][] createExcess(ITestContext testContext) {
        Random r = new Random(System.currentTimeMillis());
        long paymentId = Long.valueOf(String.valueOf(r).substring(2, 9));

        Object[] arr1 = {1l,82l,100.00f,PaymentType.EXCESS,paymentId,AccountType.DC,"2",AccountType.PARTNER,"4019", EnumSCM.SUCCESS};
        Object[] arr2 = {82l,1208l,100.00f,PaymentType.EXCESS,paymentId,AccountType.SDA,"26",AccountType.DC,"2", EnumSCM.SUCCESS};
        Object[] arr3 = {1208l,974l,100.00f,PaymentType.EXCESS,paymentId,AccountType.PARTNER,"49170",AccountType.SDA,"26", EnumSCM.SUCCESS};
        Object[] arr4 = {null,null,100.00f,PaymentType.EXCESS,paymentId,AccountType.PARTNER,"49170",AccountType.SDA,"26", EnumSCM.ERROR};
        Object[] arr5 = {1208l,974l,null,PaymentType.EXCESS,paymentId,AccountType.PARTNER,"49170",AccountType.SDA,"26", EnumSCM.ERROR};
        Object[] arr6 = {1208l,974l,100.00f,PaymentType.EXCESS,null,AccountType.PARTNER,"49170",AccountType.SDA,"26", EnumSCM.ERROR};
        Object[] arr7 = {1208l,974l,100.00f,PaymentType.EXCESS,paymentId,null,"49170",AccountType.SDA,"26", EnumSCM.ERROR};
        Object[] arr8 = {1208l,974l,100.00f,PaymentType.EXCESS,paymentId,AccountType.PARTNER,null,AccountType.SDA,"26", EnumSCM.ERROR};

        Object[][] dataSet = new Object[][]{arr1,arr2,arr3};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }


    }
