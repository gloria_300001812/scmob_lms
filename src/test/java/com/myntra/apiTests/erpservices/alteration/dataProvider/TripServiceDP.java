package com.myntra.apiTests.erpservices.alteration.dataProvider;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.tripService.helper.TripServiceHelper;
import com.myntra.lastmile.client.code.utils.AlterationMode;
import com.myntra.lastmile.client.response.tripservice.ShipmentPendencyCreationResponse;
import com.myntra.lastmile.client.tripservice.ShipmentPendencyStatus;
import com.myntra.lms.client.status.ShipmentSourceEnum;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lordoftherings.Toolbox;
import com.myntra.lms.client.status.ShippingMethod;
import org.codehaus.jettison.json.JSONException;
import org.joda.time.DateTime;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.lang.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.myntra.apiTests.common.Constants.ProductionData.pincode;

public class TripServiceDP {
    private static TripServiceHelper tripServiceHelper;

    @DataProvider
    public static Object[][] createAlterationShipment(ITestContext testContext) {
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        Object[] argCreateShipment = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTimeHelper.convertTimetoEpoch(1), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.SUCCESS};
        Object[][] dataSet = new Object[][]{argCreateShipment};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }


    @DataProvider
    public static Object[][] createAlterationShipments(ITestContext testContext) {
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));

        Object[] argCreateShipment = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
        "560068" , 1099.0f , DateTimeHelper.convertTimetoEpoch(1), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.SUCCESS};
        Object[] argCreateShipmentWithNullSourecID = {null , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR }; // "Shipment with tracking number "+"ML"+String.valueOf(contactNumber).substring(2, 9)+" and source reference id "+String.valueOf(contactNumber).substring(2, 9)+" already exists"
        Object[] argCreateShipmentWithNullSourecRefId = {"2297" , null, "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR}; //,"Another shipment with tracking number" +"ML"+String.valueOf(contactNumber).substring(2, 9)+" exists with source reference id "+String.valueOf(contactNumber).substring(2, 9)
        Object[] argCreateShipmentWithNullTrackingNumber = {"2297" , String.valueOf(contactNumber).substring(2, 9), null,5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
//        Object[] arg5 = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),null, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
//                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullShipmentType = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, null, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullShipmentMethod = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, null ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithPincode = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                null , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullPromiseDate = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , null, false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullTenantId = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , null , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullClientId = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , null , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullSourcePath = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , null , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullALterationMode = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , null, 1 , EnumSCM.ERROR};
        Object[][] dataSet = new Object[][]{argCreateShipment,argCreateShipmentWithNullSourecID,argCreateShipmentWithNullSourecRefId,argCreateShipmentWithNullTrackingNumber,argCreateShipmentWithNullShipmentType,argCreateShipmentWithNullShipmentMethod,argCreateShipmentWithPincode,argCreateShipmentWithNullPromiseDate,argCreateShipmentWithNullTenantId,
                argCreateShipmentWithNullClientId,argCreateShipmentWithNullSourcePath,argCreateShipmentWithNullALterationMode};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 13, 13);
    }

    @DataProvider
    public static Object [][] getShipmentPendencyForDc(ITestContext testContext)
    {
        List<ShipmentPendencyStatus> shipmentPendencyStatus = new ArrayList<>();
        shipmentPendencyStatus.add(ShipmentPendencyStatus.UNASSIGNED);
        shipmentPendencyStatus.add(ShipmentPendencyStatus.DELIVERED);
        Object[] argGetPedency = {LMS_CONSTANTS.TENANTID,5l,ShipmentType.ALTERATION,shipmentPendencyStatus};
        Object[] argGetPedencyforJabong = {LMS_CONSTANTS.JABONG_CLIENT_ID_,5l,ShipmentType.ALTERATION,shipmentPendencyStatus};
        Object[] argGetPendencyWithNullTenant = {null,5l,ShipmentType.ALTERATION,shipmentPendencyStatus};
        Object[] argGetPendencyWithNullDcId = {LMS_CONSTANTS.TENANTID,null,ShipmentType.ALTERATION,shipmentPendencyStatus};
        Object[] argGetPendencyWithNullShipmentType = {LMS_CONSTANTS.TENANTID,null,null,shipmentPendencyStatus};
        Object[] argGetPendencyWithNullShipmentStatus = {LMS_CONSTANTS.TENANTID,null,null,null};
        Object[][] dataSet = new Object[][]{argGetPedency,argGetPedencyforJabong,argGetPendencyWithNullTenant,argGetPendencyWithNullDcId,argGetPendencyWithNullShipmentType,argGetPendencyWithNullShipmentStatus};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 6, 6);
    }

    @DataProvider
    public static Object[][] updateAlterationShipments(ITestContext testContext) throws IOException, XMLStreamException, JSONException, JAXBException {
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment("2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTimeHelper.convertTimetoEpoch(1), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.SUCCESS);
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        Object[] argCreateShipment = {"2297" , String.valueOf(contactNumber1).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTimeHelper.convertTimetoEpoch(1), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.SUCCESS};
        Object[] argCreateShipmentWithNullSourecID = {null , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.SUCCESS }; // "Shipment with tracking number "+"ML"+String.valueOf(contactNumber).substring(2, 9)+" and source reference id "+String.valueOf(contactNumber).substring(2, 9)+" already exists"
        Object[] argCreateShipmentWithNullSourecRefId = {"2297" , null, "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR}; //,"Another shipment with tracking number" +"ML"+String.valueOf(contactNumber).substring(2, 9)+" exists with source reference id "+String.valueOf(contactNumber).substring(2, 9)
        Object[] argCreateShipmentWithNullTrackingNumber = {"2297" , String.valueOf(contactNumber).substring(2, 9), null,5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
//        Object[] arg5 = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),null, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
//                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullShipmentType = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, null, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullShipmentMethod = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, null ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithPincode = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                null , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullPromiseDate = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , null, false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullTenantId = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , null , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.ERROR};
        Object[] argCreateShipmentWithNullClientId = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , null , ShipmentSourceEnum.MYNTRA , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.SUCCESS};
        Object[] argCreateShipmentWithNullSourcePath = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , null , AlterationMode.SAMPLE_CLOTH , 1 , EnumSCM.SUCCESS};
        Object[] argCreateShipmentWithNullALterationMode = {"2297" , String.valueOf(contactNumber).substring(2, 9), "ML"+String.valueOf(contactNumber).substring(2, 9),5l, ShipmentType.ALTERATION, ShippingMethod.NORMAL ,
                "560068" , 1099.0f , DateTime.now(), false , LMS_CONSTANTS.TENANTID , LMS_CONSTANTS.CLIENTID , ShipmentSourceEnum.MYNTRA , null, 1 , EnumSCM.SUCCESS};
        Object[][] dataSet = new Object[][]{argCreateShipment,argCreateShipmentWithNullSourecID,argCreateShipmentWithNullSourecRefId,argCreateShipmentWithNullTrackingNumber,argCreateShipmentWithNullShipmentType,argCreateShipmentWithNullShipmentMethod,argCreateShipmentWithPincode,argCreateShipmentWithNullPromiseDate,argCreateShipmentWithNullTenantId,
                argCreateShipmentWithNullClientId,argCreateShipmentWithNullSourcePath,argCreateShipmentWithNullALterationMode};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 13, 13);
    }


}
