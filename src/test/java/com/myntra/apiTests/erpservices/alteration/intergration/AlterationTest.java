package com.myntra.apiTests.erpservices.alteration.intergration;

import com.myntra.apiTests.erpservices.lastmile.client.TripOrderAssignmentClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClientV2_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.shipmentPlatform.helper.ShipmentPlatformHelper;
import com.myntra.apiTests.erpservices.shipmentPlatform.pojo.ShipmentCreationResponse;
import com.myntra.apiTests.erpservices.tripService.helper.TripServiceHelper;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.shipment.types.Mode;
import com.myntra.shipment.types.ScheduleType;
import com.myntra.shipment.types.ShipmentType;
import lombok.SneakyThrows;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import java.io.IOException;
import java.util.Date;
import java.util.Random;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class AlterationTest {

    static Logger log = Logger.getLogger(AlterationTest.class);

    private LMSOrderDetailsClient lmsOrderDetailsClient;
    private TripOrderAssignmentClient tripOrderAssignmentClient;
    private MLShipmentClientV2 mlShipmentServiceV2Client;
    private LastmileHelper lastmileHelper;
    private StoreValidator storeValidator = new StoreValidator();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private TripServiceHelper tripServiceHelper;
    private ShipmentPlatformHelper shipmentPlatformHelper;
    MLShipmentClientV2_QA mlShipmentServiceV2ClientQA=new MLShipmentClientV2_QA();
    TripClient_QA tripClient_qa = new TripClient_QA();
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();


    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }


    static Long storeHlPId;
    static Long storeSDAId;
    static String storeTenantId;
    static String code;
    static String mobileNumber;

  //  @BeforeTest
    public void createStore(){
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        String name="BSJ" + String.valueOf(contactNumber).substring(5,9);
        String ownerFirstName= "BSJ" + String.valueOf(contactNumber).substring(5,9);
        String ownerLastName= "BSJ" + String.valueOf(contactNumber).substring(5,9);
        code="BSJ" + String.valueOf(contactNumber).substring(5,9);
        String latLong="LA_latlong" + String.valueOf(contactNumber).substring(5,9);
        String emailId="test@lastmileAutomation.com";
        mobileNumber=String.valueOf(contactNumber);
        String address="Automation Address";
        String city="Bangalore";
        String state="Karnataka";
        String mappedDcCode= LASTMILE_CONSTANTS.ROLLING_DC_CODE;
        String gstin="LA_gstin" + String.valueOf(contactNumber).substring(5,9);
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, LMS_PINCODE.ML_BLR, city, state, mappedDcCode,
                gstin, LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code);
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        storeSDAId = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry().getId();
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
    }

    @Test(description = "C28975 : Create Alteration Shipment and Cancel it ")
    public void MarkAlterationShipmentCancel(String sourceReferenceId, String sourceItemReferenceId , String pincode,  ShipmentType shipmentType , String tenantId , String clientId ,
                                             ScheduleType scheduleType , Date scheduleStart , Date scheduleEnd , Mode mode, String skuId , String styleId , Double itemValue , Double shipmentValue) throws IOException {
        ShipmentCreationResponse shipmentCreationResponse = shipmentPlatformHelper.createAlterationShipmentInSP( sourceReferenceId, "ELC", sourceItemReferenceId ,  pincode,  shipmentType ,  tenantId ,  clientId ,
                 scheduleType ,  scheduleStart ,  scheduleEnd ,  mode,  skuId ,  styleId ,  itemValue ,  shipmentValue);


    }

    //TODO: for testing purpose for Helpers creation
    @Test
    @SneakyThrows
    public void test(){
        ShipmentCreationResponse shipmentUpdateResponse=ShipmentPlatformHelper.pushAlterationToLMS(318l, "PUSHED_TO_LOGISTICS","ML", "ML0001633726","","","","2297");
        shipmentUpdateResponse.getStatus();


    }

}
