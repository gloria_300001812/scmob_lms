package com.myntra.apiTests.erpservices.alteration.tripService;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.alteration.dataProvider.TripServiceDP;
import com.myntra.apiTests.erpservices.lastmile.client.TripOrderAssignmentClient;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.MLShipmentClientV2_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.tripService.helper.TripServiceHelper;
import com.myntra.apiTests.erpservices.tripService.validator.TripServiceValidator;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.AlterationMode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.AttemptReasonCodeEntry;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.tripservice.ShipmentPendencyCreationResponse;
import com.myntra.lastmile.client.response.tripservice.ShipmentPendencyResponse;
import com.myntra.lastmile.client.status.ShipmentPendencyUpdateEvent;
import com.myntra.lastmile.client.status.ShipmentPendencyUpdateResponse;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripUpdateEvent;
import com.myntra.lastmile.client.tripservice.ShipmentPendencyStatus;
import com.myntra.lms.client.status.ShipmentSourceEnum;
import com.myntra.lms.client.status.ShipmentStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.codehaus.jettison.json.JSONException;
import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class TripService {
    static Logger log = Logger.getLogger(TripService.class);

    private LMSOrderDetailsClient lmsOrderDetailsClient;
    private TripOrderAssignmentClient tripOrderAssignmentClient;
    private MLShipmentClientV2 mlShipmentServiceV2Client;
    private LastmileHelper lastmileHelper;
    private StoreValidator storeValidator = new StoreValidator();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private TripServiceHelper tripServiceHelper;
    MLShipmentClientV2_QA mlShipmentServiceV2ClientQA=new MLShipmentClientV2_QA();
    TripClient_QA tripClient_qa = new TripClient_QA();
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();



    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }


    static Long storeHlPId;
    static Long storeSDAId;
    static String storeTenantId;
    static String code;
    static String mobileNumber;

    @BeforeTest
    public void createStore(){
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        String name="BSJ" + String.valueOf(contactNumber).substring(5,9);
        String ownerFirstName= "BSJ" + String.valueOf(contactNumber).substring(5,9);
        String ownerLastName= "BSJ" + String.valueOf(contactNumber).substring(5,9);
        code="BSJ" + String.valueOf(contactNumber).substring(5,9);
        String latLong="LA_latlong" + String.valueOf(contactNumber).substring(5,9);
        String emailId="test@lastmileAutomation.com";
        mobileNumber=String.valueOf(contactNumber);
        String address="Automation Address";
        String city="Bangalore";
        String state="Karnataka";
        String mappedDcCode= LASTMILE_CONSTANTS.ROLLING_DC_CODE;
        String gstin="LA_gstin" + String.valueOf(contactNumber).substring(5,9);
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, LMS_PINCODE.ML_BLR, city, state, mappedDcCode,
                gstin, LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.EOD_RECON, 500, code);
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        storeSDAId = storeResponse.getStoreEntries().get(0).getDeliveryStaffEntry().getId();
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
    }

    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "createAlterationShipments")
    public void createAlterationShipment(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                         String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws JAXBException, IOException, JSONException, XMLStreamException {
        if(status.contains("SUCCESS")) {
            ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                    pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
            Assert.assertEquals(shipmentPendencyCreationResponse.getStatus().getStatusType().toString(), status);
        }
        else {
            String statusResponse = tripServiceHelper.createAlterationShipmentNegative(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                    pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
        }
    }


    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "getShipmentPendencyForDc")
    public void getShipmentPendencyForDc(String tenantId , Long deliveryCenterId , ShipmentType shipmentType , List<ShipmentPendencyStatus> shipmentPendencyStatus ) throws IOException, JAXBException {
        String shipmentPendencyResponse = tripServiceHelper.getShipmentPendencyForDc(tenantId, deliveryCenterId, shipmentType, shipmentPendencyStatus);
    }

    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "updateAlterationShipments" , enabled = false)
    public void updateAlterationShipment(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                         String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws JAXBException, IOException, JSONException, XMLStreamException {
        if(status.contains("SUCCESS")) {
            ShipmentPendencyCreationResponse shipmentPendencyUpdate = tripServiceHelper.updateAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                    pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem);
            Assert.assertEquals(shipmentPendencyUpdate.getStatus().getStatusType().toString(), status);
        }
        else {
            StatusResponse statusResponse = tripServiceHelper.updateAlterationShipmentNegative(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                    pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem);
        }

    }

    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "createAlterationShipment")
    public void addAlterationShipmentstoTrip(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                   String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws Exception {
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
        List<String> alterationtrackingNumber = new ArrayList<>();;
        alterationtrackingNumber.add(shipmentPendencyCreationResponse.getShipmentPendencyEntry().getTrackingNumber());
        Long deliveryStaffId = lmsServiceHelper.getAndAddDeliveryStaffID(deliveryCenterId);
        Long tripId = tripClient_qa.createTrip(deliveryCenterId, deliveryStaffId).getTrips().get(0).getId();
        String tripResponse = tripServiceHelper.updateTrip(tripId, TripUpdateEvent.ADD_SHIPMENT,alterationtrackingNumber);
    }

    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "createAlterationShipment")
    public void startTripWithAlterationshipment(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                   String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws Exception {
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
        List<String> alterationtrackingNumber = new ArrayList<>();;
        alterationtrackingNumber.add(shipmentPendencyCreationResponse.getShipmentPendencyEntry().getTrackingNumber());
        Long deliveryStaffId = lmsServiceHelper.getAndAddDeliveryStaffID(deliveryCenterId);
        Long tripId = tripClient_qa.createTrip(deliveryCenterId, deliveryStaffId).getTrips().get(0).getId();
        tripServiceHelper.updateTrip(tripId, TripUpdateEvent.ADD_SHIPMENT,alterationtrackingNumber);
        tripServiceHelper.updateTrip(tripId, TripUpdateEvent.START,alterationtrackingNumber);

    }

    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "createAlterationShipment")
    public void removeAlterationShipmentfromTrip(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                                String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws Exception {
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
        List<String> alterationtrackingNumber = new ArrayList<>();;
        alterationtrackingNumber.add(shipmentPendencyCreationResponse.getShipmentPendencyEntry().getTrackingNumber());
        Long deliveryStaffId = lmsServiceHelper.getAndAddDeliveryStaffID(deliveryCenterId);
        Long tripId = tripClient_qa.createTrip(deliveryCenterId, deliveryStaffId).getTrips().get(0).getId();
        tripServiceHelper.updateTrip(tripId, TripUpdateEvent.ADD_SHIPMENT,alterationtrackingNumber);
        tripServiceHelper.updateTrip(tripId, TripUpdateEvent.REMOVE_SHIPMENT,alterationtrackingNumber);
    }

    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "createAlterationShipment")
    public void removeShipmentAfterTripStart(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                                String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws Exception {
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
        List<String> alterationtrackingNumber = new ArrayList<>();;
        alterationtrackingNumber.add(shipmentPendencyCreationResponse.getShipmentPendencyEntry().getTrackingNumber());
        Long deliveryStaffId = lmsServiceHelper.getAndAddDeliveryStaffID(deliveryCenterId);
        Long tripId = tripClient_qa.createTrip(deliveryCenterId, deliveryStaffId).getTrips().get(0).getId();
        tripServiceHelper.updateTrip(tripId, TripUpdateEvent.ADD_SHIPMENT,alterationtrackingNumber);
        tripServiceHelper.updateTrip(tripId, TripUpdateEvent.START,alterationtrackingNumber);
        String tripUpdateResponse = tripServiceHelper.updateTrip(tripId, TripUpdateEvent.REMOVE_SHIPMENT,alterationtrackingNumber);
    }

    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "createAlterationShipment")
    public void removeShipmentBeforeAddingtoTrip(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                             String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws Exception {
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
        List<String> alterationtrackingNumber = new ArrayList<>();
        alterationtrackingNumber.add(shipmentPendencyCreationResponse.getShipmentPendencyEntry().getTrackingNumber());
        Long deliveryStaffId = lmsServiceHelper.getAndAddDeliveryStaffID(deliveryCenterId);
        Long tripId = tripClient_qa.createTrip(deliveryCenterId, deliveryStaffId).getTrips().get(0).getId();
        String tripUpdateResponse = tripServiceHelper.updateTrip(tripId, TripUpdateEvent.REMOVE_SHIPMENT,alterationtrackingNumber);
        Assert.assertTrue(tripUpdateResponse.contains("BAD_REQUEST"),"Remove Shipment from Trip is allowed without adding shipments to trip");
    }


    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "createAlterationShipment" , description = "Marking alteration shipment as PickupSuccessful.")
    public void markAlterationShipmentasPS(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                                 String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws Exception {
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
        String shipmentPendencyUpdateResponse = tripServiceHelper.updateShipmentPendency(deliveryCenterId,trackingNumber, ShipmentPendencyUpdateEvent.CANCEL,ShipmentType.ALTERATION,tenantId,clientId,null);
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse1 = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, storeHlPId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, storeTenantId, tenantId, sourcePath, alterationMode, noOfItem, status);
        List<String> alterationtrackingNumber = new ArrayList<>();
        alterationtrackingNumber.add(trackingNumber);
        TripResponse tripResponse = mlShipmentServiceV2ClientQA.createStoreTrip(alterationtrackingNumber, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Long storeTripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(tripOrderAssignmentId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
    }



    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "createAlterationShipment" , description = "Marking alteration shipment as PickupSuccessful.")
    public void markAlterationShipmentasFP(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                           String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws Exception {
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
        String shipmentPendencyUpdateResponse = tripServiceHelper.updateShipmentPendency(deliveryCenterId,trackingNumber, ShipmentPendencyUpdateEvent.CANCEL,ShipmentType.ALTERATION,tenantId,clientId,null);
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse1 = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, storeHlPId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, storeTenantId, tenantId, sourcePath, alterationMode, noOfItem, status);
        List<String> alterationtrackingNumber = new ArrayList<>();
        alterationtrackingNumber.add(trackingNumber);
        TripResponse tripResponse = mlShipmentServiceV2ClientQA.createStoreTrip(alterationtrackingNumber, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Long storeTripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(tripOrderAssignmentId, EnumSCM.INCOMPLETE_INCORRECT_ADDRESS, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
    }

    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "createAlterationShipment" , description = "Marking alteration shipment as PickupSuccessful.")
    public void markAlterationShipmentasPSandDL(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                           String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws Exception {
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
        String shipmentPendencyUpdateResponse = tripServiceHelper.updateShipmentPendency(deliveryCenterId,trackingNumber, ShipmentPendencyUpdateEvent.CANCEL,ShipmentType.ALTERATION,tenantId,clientId,null);
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse1 = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, storeHlPId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, storeTenantId, tenantId, sourcePath, alterationMode, noOfItem, status);
        List<String> alterationtrackingNumber = new ArrayList<>();
        alterationtrackingNumber.add(trackingNumber);
        TripResponse tripResponse = mlShipmentServiceV2ClientQA.createStoreTrip(alterationtrackingNumber, storeSDAId);
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Long storeTripId = tripResponse.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(tripOrderAssignmentId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        String shipmentPendencyUpdateResponse1 = tripServiceHelper.updateShipmentPendency(storeHlPId,trackingNumber, ShipmentPendencyUpdateEvent.RESHIP,ShipmentType.ALTERATION,storeTenantId,tenantId,null);
        TripResponse tripResponse1 = mlShipmentServiceV2ClientQA.createStoreTrip(alterationtrackingNumber, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Long storeTripId1 = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId1.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId1 = tripOrderAssignment1.getTripOrders().get(0).getId();
        TripOrderAssignmentResponse response1 = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1, EnumSCM.DELIVERED, EnumSCM.UPDATE);
        Assert.assertEquals(response1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

    }

    @Test(dataProviderClass = TripServiceDP.class , dataProvider = "createAlterationShipment" , description = "Marking alteration shipment as PickupSuccessful.")
    public void markAlterationShipmentasPSandFD(String sourceId , String sourceRefernceId , String trackingNumber , long deliveryCenterId , ShipmentType shipmentType , ShippingMethod shippingmethod ,
                                                String pincode , Float shipmentValue , DateTime promiseDate , Boolean deleted , String tenantId , String clientId , ShipmentSourceEnum sourcePath , AlterationMode alterationMode , int noOfItem , String status) throws Exception {
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, deliveryCenterId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, tenantId, clientId, sourcePath, alterationMode, noOfItem, status);
        String shipmentPendencyUpdateResponse = tripServiceHelper.updateShipmentPendency(deliveryCenterId,trackingNumber, ShipmentPendencyUpdateEvent.CANCEL,ShipmentType.ALTERATION,tenantId,clientId,null);
        ShipmentPendencyCreationResponse shipmentPendencyCreationResponse1 = tripServiceHelper.createAlterationShipment(sourceId, sourceRefernceId, trackingNumber, storeHlPId, shipmentType, shippingmethod,
                pincode, shipmentValue, promiseDate, deleted, storeTenantId, tenantId, sourcePath, alterationMode, noOfItem, status);
        List<String> alterationtrackingNumber = new ArrayList<>();
        alterationtrackingNumber.add(trackingNumber);
        TripResponse tripResponse = mlShipmentServiceV2ClientQA.createStoreTrip(alterationtrackingNumber, storeSDAId);
        Long storeTripId = tripResponse.getTrips().get(0).getId();
        Assert.assertTrue(tripResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");
        DBUtilities.exUpdateQuery("update trip_order_assignment set trip_order_status = 'OFD' where trip_id = " + storeTripId, "LMS");
        DBUtilities.exUpdateQuery("update ml_shipment set shipment_status = 'OUT_FOR_PICKUP' where tracking_number = '"+trackingNumber+"' and delivery_center_id = " + deliveryCenterId , "LMS");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        storeValidator.isNull(storeTripId.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId, TripStatus.PROCESSING.toString());
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().get(0).getId();
        TripOrderAssignmentResponse response = lmsServiceHelper.updatePickupInTrip(tripOrderAssignmentId, EnumSCM.PICKED_UP_SUCCESSFULLY, EnumSCM.UPDATE);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(storeTripId)).equalsIgnoreCase(EnumSCM.SUCCESS));
        String shipmentPendencyUpdateResponse1 = tripServiceHelper.updateShipmentPendency(storeHlPId,trackingNumber, ShipmentPendencyUpdateEvent.RESHIP,ShipmentType.ALTERATION,storeTenantId,tenantId,null);
        TripResponse tripResponse1 = mlShipmentServiceV2ClientQA.createStoreTrip(alterationtrackingNumber, storeSDAId);
        Assert.assertTrue(tripResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success),"Assign order to store SDA ia not successfull");
        Thread.sleep(LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.wait_Time);
        Long storeTripId1 = tripResponse1.getTrips().get(0).getId();
        storeValidator.isNull(storeTripId1.toString());
        statusPollingValidator.validateTripStatusWithVNM(storeTripId1, com.myntra.lastmile.client.code.utils.TripStatus.OUT_FOR_DELIVERY.toString());
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Long tripOrderAssignmentId1 = tripOrderAssignment1.getTripOrders().get(0).getId();
        TripOrderAssignmentResponse response11 = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1, EnumSCM.FAILED_DELIVERY, EnumSCM.UPDATE);
        Assert.assertEquals(response11.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

    }

}
