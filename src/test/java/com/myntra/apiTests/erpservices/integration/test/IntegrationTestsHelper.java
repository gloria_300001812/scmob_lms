package com.myntra.apiTests.erpservices.integration.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.myntra.apiTests.common.Utils.DBHelper;
import com.myntra.apiTests.common.Utils.DBHelperEnums;
import com.myntra.apiTests.end2end.FetchEnvUtil;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.common.Constants.ShipmentSource;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.common.entries.ATPandIMSDataEntry;
import com.myntra.apiTests.common.entries.ReleaseEntry;
import com.myntra.apiTests.end2end.End2EndHelper;
import com.myntra.apiTests.erpservices.atp.ATPServiceHelper;
import com.myntra.apiTests.erpservices.ims.IMSServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSHelpersEnums.RefundType;
import com.myntra.apiTests.portalservices.pps.v2.helper.PpsAssist;
import com.myntra.commons.codes.StatusResponse.Type;
import com.myntra.commons.exception.ManagerException;
import com.myntra.createorder.builder.CreateOrderEntryBuilder;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.oms.client.response.OrderReleaseResponse;
import com.myntra.oms.client.response.OrderResponse;
import com.myntra.scm.pojos.CreateOrderEntry;
import com.myntra.scm.pojos.SkuEntry;

public class IntegrationTestsHelper {
	
	
	
	private String orderID;
	End2EndHelper end2EndHelper = new End2EndHelper();
	OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
	ATPServiceHelper atpServiceHelper = new ATPServiceHelper();
	IMSServiceHelper imsServiceHelper = new IMSServiceHelper();
	private List<OrderReleaseEntry> orderReleaseEntries;
	private ArrayList<String> listOfRelease = null;
	private long delayedTime=5000;
	private SoftAssert sft;
	private OrderResponse cancelOrderResponse;
	private OrderReleaseResponse cancelLineResponse;
	private OrderReleaseResponse cancelReleaseResponse;
	ProcessRelease processRelease = new ProcessRelease();
	PpsAssist ppsAssist = new PpsAssist();
	long waitTime = 1000;
	
	public enum CancelType{
		ORDER_CANCEL,
		ORDER_LINE_CANCEL,
		ORDER_RELEASE_CANCEL
	}

	/**
	 * This is to create order and return release entries
	 * @param login
	 * @param password
	 * @param pincode
	 * @param skuEntries
	 * @return
	 * @throws Exception
	 */
	public String createOrderHelper(String login,String password,Long pincode,SkuEntry[] skuEntries,
			String clientId_myntra,String tenantId_myntra,String dataSource) throws Exception{
		CreateOrderEntry createOrderEntry = new CreateOrderEntryBuilder(login,password,pincode,skuEntries).environment(FetchEnvUtil.getEnvironment()).build();
		orderID = end2EndHelper.createOrder(createOrderEntry);
		
		//TODO: Till the time MT createorder is not working we will use old one only
/*		SkuEntry skuEntry = skuEntries[0];
		String storeOrderID=ppsAssist.createCodOrder(login,password,clientId_myntra,tenantId_myntra,dataSource,skuEntry.getSkuId()
				,""+skuEntry.getQuantity(),skuEntry.getStyleId());
        orderID = omsServiceHelper.getOrderEntryByStoreOrderID(storeOrderID).getId().toString();*/
        
		return orderID;
	}
	
	
    public Boolean waitForOrderLevelOnHoldToResolve(String orderID,int delayToCheck) throws Exception{
    	Boolean isOnhold = omsServiceHelper.getOrderEntry(orderID).getOnHold();
    	for(int i=0;i<delayToCheck;i++){
    		if(isOnhold==false){
    			return isOnhold;
    		}
    		End2EndHelper.sleep(delayedTime);
    		isOnhold = omsServiceHelper.getOrderEntry(orderID).getOnHold();
    	}
    	
    	return isOnhold;
    }
    


	/**
	 * @param orderReleaseId
	 * @param login
	 * @param qty
	 * @param reasonId
	 * @throws Exception
	 */
	public void cancellationSuccess(String id, String login, int qty, int reasonId,String comment,CancelType cancelType) throws Exception {
		// TODO Auto-generated method stub
		sft = new SoftAssert();
		switch (cancelType) {

				case ORDER_CANCEL: {
					cancelOrderResponse = omsServiceHelper.cancelOrder(id,""+reasonId, login, comment);
			        sft.assertEquals(cancelOrderResponse.getStatus().getStatusType(), Type.SUCCESS,"Verify response after cancel order Actual:"+cancelOrderResponse.getStatus().getStatusType());
			        End2EndHelper.sleep(5 *waitTime);
			        break;
				}
				case ORDER_LINE_CANCEL: {
			    	List<OrderLineEntry> orderLineEntries = omsServiceHelper.getOrderLineEntries(id);
			        String lineID = ""+orderLineEntries.get(0).getId();
			    	cancelLineResponse = omsServiceHelper.cancelLine(id,login, new String[] {lineID +":"+qty}, reasonId);
			        sft.assertEquals(cancelLineResponse.getStatus().getStatusType(), Type.SUCCESS,"Verify response after cancel order Actual:"+cancelLineResponse.getStatus().getStatusType());
			        End2EndHelper.sleep(5 *waitTime);
			        break;
				}
				case ORDER_RELEASE_CANCEL: {
					cancelReleaseResponse = omsServiceHelper.cancelOrderRelease(id,""+reasonId, login, comment);
			        sft.assertEquals(cancelReleaseResponse.getStatus().getStatusType(), Type.SUCCESS,"Verify response after cancel order Actual:"+cancelReleaseResponse.getStatus().getStatusType());
			        End2EndHelper.sleep(5 *waitTime);
			        break;
				}
				
		}
		
		validateStatusInOMSAfterCancel(id,cancelType);

		sft.assertAll();
	}
	
	/**
	 * @param id
	 * @param cancelType
	 * @throws Exception
	 */
	public void validateStatusInOMSAfterCancel(String id,CancelType cancelType) throws Exception{
		sft = new SoftAssert();
		switch (cancelType) {
			case ORDER_CANCEL: {
				List<OrderReleaseEntry> orderReleaseEntries = omsServiceHelper.getOrderEntry(id).getOrderReleases();
				for(OrderReleaseEntry orderReleaseEntry:orderReleaseEntries){
					String releaseId = orderReleaseEntry.getId().toString();
			    	List<OrderLineEntry> orderLineEntries = omsServiceHelper.getOrderLineEntries(releaseId);
			    	for(OrderLineEntry orderLineEntry:orderLineEntries){
			    		String lineID = ""+orderLineEntry.getId();
				        sft.assertTrue(omsServiceHelper.validateReleaseStatusInOMS(releaseId,EnumSCM.F, 10),"Release is not in F status");
				        sft.assertTrue(omsServiceHelper.validateLineStatusInOMS(lineID,EnumSCM.IC, 10),"Release is not in F status");
			    	}
			        
				}
				break;
			}
			case ORDER_LINE_CANCEL:
			case ORDER_RELEASE_CANCEL:{
		    	List<OrderLineEntry> orderLineEntries = omsServiceHelper.getOrderLineEntries(id);
		        String lineID = ""+orderLineEntries.get(0).getId();
		        sft.assertTrue(omsServiceHelper.validateReleaseStatusInOMS(id,EnumSCM.F, 10),"Release is not in F status");
		        sft.assertTrue(omsServiceHelper.validateLineStatusInOMS(lineID,EnumSCM.IC, 10),"Release is not in F status");
			}
		}

        sft.assertAll();
	}
	
    /**
     * This is to mark order is particular status
     * @param orderReleasId
     * @param releaseStatus
     * @throws Exception
     */
    public void markOrderInParticularStatus (String orderReleasId, ReleaseStatus releaseStatus ) throws Exception {
        List<ReleaseEntry> releaseEntries = new ArrayList<>();
        releaseEntries.add(new ReleaseEntry.Builder(orderReleasId, releaseStatus).shipmentSource(ShipmentSource.MYNTRA).build());
        processRelease.processReleaseToStatus(processRelease.getReleaseEntryList(releaseEntries));
    }


	public void verifyRefundPPSIdGeneratedInOMS(String orderReleaseId) throws ManagerException {
		sft = new SoftAssert();
		String releaseCancelTaxRefIdValue=null;
		String releaseCancelRefundPPSIDValue=null;
		
		// TODO Auto-generated method stub
		HashMap<String,Object> releaseCancelTaxRefId = omsServiceHelper.getOrderReleaseAdditionalInfoDBEntry(orderReleaseId, "RELEASE_CANCELLATION_TX_REF_ID",RefundType.POSITIVE);
		if(releaseCancelTaxRefId!=null && releaseCancelTaxRefId.get("value")!=null){
			releaseCancelTaxRefIdValue = releaseCancelTaxRefId.get("value").toString();
		}
		
		sft.assertNotNull(releaseCancelTaxRefIdValue, "Refund tax refId should not be null");
		
		HashMap<String,Object> releaseCancelRefundPPSID = omsServiceHelper.getOrderReleaseAdditionalInfoDBEntry(orderReleaseId, "RELEASE_CANCELLATION_PPS_ID",RefundType.POSITIVE);
		if(releaseCancelRefundPPSID!=null && releaseCancelRefundPPSID.get("value")!=null){
			releaseCancelRefundPPSIDValue = releaseCancelRefundPPSID.get("value").toString();
		}
		sft.assertNotNull(releaseCancelRefundPPSIDValue, "Refund PPSId should not be null");

		sft.assertAll();
	}

	public void updateTaxDataAtLine(String orderLineId,Double taxRate,Double taxamount){

    	Map<String,String> keyValuePair = new HashMap<String, String>();
		keyValuePair.put("tax_rate", taxRate.toString());
		keyValuePair.put("tax_amount", taxamount.toString());
		Map<String,String> whereValuePair = new HashMap<String, String>();
		whereValuePair.put("id", orderLineId.toString());

		DBHelper.updateDataInTable(keyValuePair, whereValuePair, DBHelperEnums.TablesName.order_line, DBHelperEnums.DBName.myntra_oms);
	}
}
