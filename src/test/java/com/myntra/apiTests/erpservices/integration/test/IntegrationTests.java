package com.myntra.apiTests.erpservices.integration.test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.myntra.apiTests.end2end.End2EndHelper;
import com.myntra.apiTests.portalservices.styleservice.StyleServiceApiHelper;
import com.myntra.oms.client.entry.OrderLineEntry;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.common.Utils.CommonUtils;
import com.myntra.apiTests.common.entries.ATPandIMSDataEntry;
import com.myntra.apiTests.erpservices.atp.ATPServiceHelper;
import com.myntra.apiTests.erpservices.ims.IMSServiceHelper;
import com.myntra.apiTests.erpservices.integration.test.IntegrationTestsHelper.CancelType;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.oms.Test.OMSTCConstants;
import com.myntra.apiTests.erpservices.oms.Test.OMSTCConstants.AllStyleList;
import com.myntra.apiTests.erpservices.oms.Test.OMSTCConstants.IntegrationSkuList;
import com.myntra.apiTests.erpservices.oms.Test.OMSTCConstants.LoginAndPassword;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.scm.pojos.SkuEntry;
import com.myntra.test.commons.testbase.BaseTest;

import javax.xml.bind.JAXBException;

public class IntegrationTests extends BaseTest {
	SoftAssert sft = null;
	IMSServiceHelper imsServiceHelper = new IMSServiceHelper();
	ATPServiceHelper atpServiceHelper = new ATPServiceHelper();
	IntegrationTestsHelper integrationTestsHelper = new IntegrationTestsHelper();
	private String skuId1= IntegrationSkuList.sku_47466_21;
	private String skuId2= IntegrationSkuList.sku_47467_21;
	private String skuId3= IntegrationSkuList.sku_47468_21;
	private String skuId4= IntegrationSkuList.sku_984026_21;
	private String skuId5= IntegrationSkuList.sku_984027_21;
	private String styleId1 = AllStyleList.styleId_10022;
	private String styleId2 = AllStyleList.styleId_294215;
	private Long warehouse28 = OMSTCConstants.WareHouseIds.warehouseId28_GN;
	private Long warehouse36 = OMSTCConstants.WareHouseIds.warehouseId36_BW;
	private Long warehouse20 = OMSTCConstants.WareHouseIds.warehouseId20_BW;
	private long sellerId_HEAL=21;
	private String supplyTypeOnHand="ON_HAND";
	private String supplyTypeJIT="JUST_IN_TIME";
	String login = LoginAndPassword.IntegrationTestsLogin;
	String password = LoginAndPassword.IntegrationTestsPassword;
	private Long pincode_560068 = Long.parseLong(OMSTCConstants.Pincodes.PINCODE_560068);
	private List<OrderReleaseEntry> orderReleaseEntries;
	private String orderID;
	OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
	StyleServiceApiHelper styleServiceApiHelper = new StyleServiceApiHelper();
	private String orderReleaseId1;
	ATPandIMSDataEntry atPandIMSDataEntryBeforePlacingOrder;
	ATPandIMSDataEntry atPandIMSDataEntryAfterPlacingOrder;
	private String storeIDMyntra="1";
	private ATPandIMSDataEntry atPandIMSDataEntryAfterCancel;
	private String cancelComment="Test cancellation";
	private int cancelReasonId=41;
	private ATPandIMSDataEntry atPandIMSDataEntryAfterOrderPacked;
	private ATPandIMSDataEntry atPandIMSDataEntryAfterOrderDelivered;
    String clientId_myntra= EnumSCM.clientId_myntra;
    String client_jabong=EnumSCM.client_jabong;
    String tenantId_myntra=EnumSCM.tenantId_myntra;
    String tenantId_jabong=EnumSCM.tenantId_jabong;
    String dataSource = EnumSCM.dataSource;
    CommonUtils commonUtils = new CommonUtils();
	private Double taxRate = 50.0;
	private Double taxamount = null;


	//Update inventory in ATP before placing order

    @BeforeMethod()
    public void beforeMethod() throws SQLException, IOException, JAXBException {
        atpServiceHelper.updateInventoryDetailsForSeller(new String[]{skuId1+":"+warehouse28+":100:0:"+sellerId_HEAL+":1"},supplyTypeOnHand);
		atpServiceHelper.updateInventoryDetailsForSeller(new String[]{skuId4+":"+warehouse20+":100:0:"+sellerId_HEAL+":1"},supplyTypeJIT);
        styleServiceApiHelper.styleReindexForStyleIDsPost(new String[]{styleId1,styleId2});
		omsServiceHelper.refreshOMSApplicationPropertyCache();
		End2EndHelper.sleep(5000L);
    }


	@Test(enabled=true,groups={"integration"}, description="TC001:Cancel OrderLine when warehouse is not assigned. Verify ATP inventory and BOC.")
    public void cancelOrderLineForWhichWHIsNotAssigned() throws Exception {
		sft = new SoftAssert();
		SkuEntry[] skuEntries = {new SkuEntry(skuId1, 1,styleId1)};

		//Update inventory in IMS before placing order

		imsServiceHelper.updateInventoryForSeller(new String[]{skuId1+":"+warehouse28+":0:0:"+sellerId_HEAL},supplyTypeOnHand);



		//Get ATP and IMS data before placing order
		atPandIMSDataEntryBeforePlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);

		//Create order and check order level onhold is resolved and orderRelease is stuck with Onhold-34 due to lack of inventory
		orderID = integrationTestsHelper.createOrderHelper(login, password, pincode_560068, skuEntries,clientId_myntra,tenantId_myntra,dataSource);
		sft.assertEquals(omsServiceHelper.validateOrderOFFHoldStatusInOMS(orderID,15), Boolean.TRUE.booleanValue(),"Order LevelOnhold is not resolved");
		orderReleaseId1 = omsServiceHelper.getOrderEntry(orderID).getOrderReleases().get(0).getId().toString();
		sft.assertEquals(omsServiceHelper.verifyOrderIsInOnHoldWithReasonId(orderReleaseId1,EnumSCM.IMS_OOS_34,10), Boolean.TRUE.booleanValue(),"Release is not in onhold");
		//Check order is stuck in Q status
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.Q);

		//Get ATP and IMS data After placing order
		atPandIMSDataEntryAfterPlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is placed
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount());

		//Cancel Line
		integrationTestsHelper.cancellationSuccess(orderReleaseId1, login, 1, cancelReasonId,cancelComment,CancelType.ORDER_LINE_CANCEL);

		//Get ATP and IMS data After Cancel Line
		atPandIMSDataEntryAfterCancel = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is canceled
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterCancel.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryAfterCancel.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterCancel.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount(),
				atPandIMSDataEntryAfterCancel.getImsBOCCount());

		sft.assertAll();

	}


	@Test(enabled=true,groups={"integration"}, description="TC002:Cancel OrderLine when warehouse is assigned. Verify ATP and IMS inventory and BOC.")
    public void cancelOrderLineForWhichWHIsAssigned() throws Exception {

		sft = new SoftAssert();
		SkuEntry[] skuEntries = {new SkuEntry(skuId1, 1,styleId1)};

		//Update inventory in IMS before placing order

		imsServiceHelper.updateInventoryForSeller(new String[]{skuId1+":"+warehouse28+":100:0:"+sellerId_HEAL},supplyTypeOnHand);



		//Get ATP and IMS data before placing order
		atPandIMSDataEntryBeforePlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);

		//Create order and check order level onhold is resolved and orderRelease is stuck with Onhold-34 due to lack of inventory
		orderID = integrationTestsHelper.createOrderHelper(login, password, pincode_560068, skuEntries,clientId_myntra,tenantId_myntra,dataSource);
		sft.assertEquals(omsServiceHelper.validateOrderOFFHoldStatusInOMS(orderID,15), Boolean.TRUE.booleanValue(),"Order LevelOnhold is not resolved");
		orderReleaseId1 = omsServiceHelper.getOrderEntry(orderID).getOrderReleases().get(0).getId().toString();
		//Check order is in WP status
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.WP);

		//Get ATP and IMS data After placing order
		atPandIMSDataEntryAfterPlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is placed
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount());

		//Cancel Line
		integrationTestsHelper.cancellationSuccess(orderReleaseId1, login, 1, cancelReasonId,cancelComment,CancelType.ORDER_LINE_CANCEL);

		//Get ATP and IMS data After Cancel Line
		atPandIMSDataEntryAfterCancel = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is canceled
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterCancel.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryAfterCancel.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterCancel.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount(),
				atPandIMSDataEntryAfterCancel.getImsBOCCount());

		sft.assertAll();

	}

	@Test(enabled=true,groups={"integration"}, description="TC003:Cancel OrderRelease when warehouse is not assigned. Verify ATP inventory and BOC.")
    public void cancelOrderReleaseForWhichWHIsNotAssigned() throws Exception {

		sft = new SoftAssert();
		SkuEntry[] skuEntries = {new SkuEntry(skuId1, 1,styleId1)};

		//Update inventory in IMS before placing order

		imsServiceHelper.updateInventoryForSeller(new String[]{skuId1+":"+warehouse28+":0:0:"+sellerId_HEAL},supplyTypeOnHand);



		//Get ATP and IMS data before placing order
		atPandIMSDataEntryBeforePlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);

		//Create order and check order level onhold is resolved and orderRelease is stuck with Onhold-34 due to lack of inventory
		orderID = integrationTestsHelper.createOrderHelper(login, password, pincode_560068, skuEntries,clientId_myntra,tenantId_myntra,dataSource);
		sft.assertEquals(omsServiceHelper.validateOrderOFFHoldStatusInOMS(orderID,15), Boolean.TRUE.booleanValue(),"Order LevelOnhold is not resolved");
		orderReleaseId1 = omsServiceHelper.getOrderEntry(orderID).getOrderReleases().get(0).getId().toString();
		sft.assertEquals(omsServiceHelper.verifyOrderIsInOnHoldWithReasonId(orderReleaseId1,EnumSCM.IMS_OOS_34,10), Boolean.TRUE.booleanValue(),"Release is not in onhold");
		//Check order is stuck in Q status
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.Q);

		//Get ATP and IMS data After placing order
		atPandIMSDataEntryAfterPlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is placed
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount());

		//Cancel Release
		integrationTestsHelper.cancellationSuccess(orderReleaseId1, login, 1, cancelReasonId,cancelComment,CancelType.ORDER_RELEASE_CANCEL);

		//Get ATP and IMS data After Cancel Line
		atPandIMSDataEntryAfterCancel = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is canceled
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterCancel.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryAfterCancel.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterCancel.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount(),
				atPandIMSDataEntryAfterCancel.getImsBOCCount());

		sft.assertAll();

	}


	@Test(enabled=true,groups={"integration"}, description="TC004:Cancel OrderRelease when warehouse is assigned. Verify ATP and IMS inventory and BOC.")
    public void cancelOrderReleaseForWhichWHIsAssigned() throws Exception {


		sft = new SoftAssert();
		SkuEntry[] skuEntries = {new SkuEntry(skuId1, 1,styleId1)};

		//Update inventory in IMS before placing order

		imsServiceHelper.updateInventoryForSeller(new String[]{skuId1+":"+warehouse28+":100:0:"+sellerId_HEAL},supplyTypeOnHand);



		//Get ATP and IMS data before placing order
		atPandIMSDataEntryBeforePlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);

		//Create order and check order level onhold is resolved and orderRelease is stuck with Onhold-34 due to lack of inventory
		orderID = integrationTestsHelper.createOrderHelper(login, password, pincode_560068, skuEntries,clientId_myntra,tenantId_myntra,dataSource);
		sft.assertEquals(omsServiceHelper.validateOrderOFFHoldStatusInOMS(orderID,15), Boolean.TRUE.booleanValue(),"Order LevelOnhold is not resolved");
		orderReleaseId1 = omsServiceHelper.getOrderEntry(orderID).getOrderReleases().get(0).getId().toString();
		//Check order is in WP status
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.WP);

		//Get ATP and IMS data After placing order
		atPandIMSDataEntryAfterPlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is placed
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount());

		//Cancel Release
		integrationTestsHelper.cancellationSuccess(orderReleaseId1, login, 1, cancelReasonId,cancelComment,CancelType.ORDER_RELEASE_CANCEL);

		//Get ATP and IMS data After Cancel Line
		atPandIMSDataEntryAfterCancel = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is canceled
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterCancel.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryAfterCancel.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterCancel.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount(),
				atPandIMSDataEntryAfterCancel.getImsBOCCount());

		sft.assertAll();

	}

	@Test(enabled=true,groups={"integration"}, description="TC005:Cancel Order when warehouse is not assigned. Verify ATP inventory and BOC.")
    public void cancelOrderForWhichWHIsNotAssigned() throws Exception {


		sft = new SoftAssert();
		SkuEntry[] skuEntries = {new SkuEntry(skuId1, 1,styleId1)};

		//Update inventory in IMS before placing order

		imsServiceHelper.updateInventoryForSeller(new String[]{skuId1+":"+warehouse28+":0:0:"+sellerId_HEAL},supplyTypeOnHand);



		//Get ATP and IMS data before placing order
		atPandIMSDataEntryBeforePlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);

		//Create order and check order level onhold is resolved and orderRelease is stuck with Onhold-34 due to lack of inventory
		orderID = integrationTestsHelper.createOrderHelper(login, password, pincode_560068, skuEntries,clientId_myntra,tenantId_myntra,dataSource);
		sft.assertEquals(omsServiceHelper.validateOrderOFFHoldStatusInOMS(orderID,15), Boolean.TRUE.booleanValue(),"Order LevelOnhold is not resolved");
		orderReleaseId1 = omsServiceHelper.getOrderEntry(orderID).getOrderReleases().get(0).getId().toString();
		sft.assertEquals(omsServiceHelper.verifyOrderIsInOnHoldWithReasonId(orderReleaseId1,EnumSCM.IMS_OOS_34,10), Boolean.TRUE.booleanValue(),"Release is not in onhold");
		//Check order is stuck in Q status
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.Q);

		//Get ATP and IMS data After placing order
		atPandIMSDataEntryAfterPlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is placed
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount());

		//Cancel Order
		integrationTestsHelper.cancellationSuccess(orderID, login, 1, cancelReasonId,cancelComment,CancelType.ORDER_CANCEL);

		//Get ATP and IMS data After Cancel Line
		atPandIMSDataEntryAfterCancel = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is canceled
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterCancel.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryAfterCancel.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterCancel.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount(),
				atPandIMSDataEntryAfterCancel.getImsBOCCount());

		sft.assertAll();

	}


	@Test(enabled=true,groups={"integration"}, description="TC006:Cancel Order when warehouse is assigned. Verify ATP and IMS inventory and BOC.")
    public void cancelOrderForWhichWHIsAssigned() throws Exception {



		sft = new SoftAssert();
		SkuEntry[] skuEntries = {new SkuEntry(skuId1, 1,styleId1)};

		//Update inventory in IMS before placing order

		imsServiceHelper.updateInventoryForSeller(new String[]{skuId1+":"+warehouse28+":100:0:"+sellerId_HEAL},supplyTypeOnHand);



		//Get ATP and IMS data before placing order
		atPandIMSDataEntryBeforePlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);

		//Create order and check order level onhold is resolved and orderRelease is stuck with Onhold-34 due to lack of inventory
		orderID = integrationTestsHelper.createOrderHelper(login, password, pincode_560068, skuEntries,clientId_myntra,tenantId_myntra,dataSource);
		sft.assertEquals(omsServiceHelper.validateOrderOFFHoldStatusInOMS(orderID,15), Boolean.TRUE.booleanValue(),"Order LevelOnhold is not resolved");
		orderReleaseId1 = omsServiceHelper.getOrderEntry(orderID).getOrderReleases().get(0).getId().toString();
		//Check order is in WP status
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.WP);

		//Get ATP and IMS data After placing order
		atPandIMSDataEntryAfterPlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is placed
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount());

		//Cancel Order
		integrationTestsHelper.cancellationSuccess(orderID, login, 1, cancelReasonId,cancelComment,CancelType.ORDER_CANCEL);

		//Get ATP and IMS data After Cancel Line
		atPandIMSDataEntryAfterCancel = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse28, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is canceled
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterCancel.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryAfterCancel.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterCancel.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount(),
				atPandIMSDataEntryAfterCancel.getImsBOCCount());

		sft.assertAll();

	}

	@Test(enabled=true,groups={"integration"}, description="TC007:Create order and Mark order Delivered, verify BOC and inventory in ATP and IMS and check order is shipped in LMS, WMS and OMS.")
    public void createOrderAndMarkOrderDeliveredWhereTaxIsIGSTWithVatRefund() throws Exception{
		sft = new SoftAssert();
		SkuEntry[] skuEntries = {new SkuEntry(skuId1, 1,styleId1)};

		//Update inventory in IMS before placing order

		imsServiceHelper.updateInventoryForSeller(new String[]{skuId1+":"+warehouse36+":100:0:"+sellerId_HEAL},supplyTypeOnHand);



		//Get ATP and IMS data before placing order
		atPandIMSDataEntryBeforePlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		//Create order and check order level onhold is resolved
		orderID = integrationTestsHelper.createOrderHelper(login, password, pincode_560068, skuEntries,clientId_myntra,tenantId_myntra,dataSource);
		sft.assertEquals(omsServiceHelper.validateOrderOFFHoldStatusInOMS(orderID,15), Boolean.TRUE.booleanValue(),"Order LevelOnhold is not resolved");
		orderReleaseId1 = omsServiceHelper.getOrderEntry(orderID).getOrderReleases().get(0).getId().toString();
		//Check order is in WP status
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.WP);


		OrderLineEntry orderLineEntry = omsServiceHelper.getOrderReleaseEntry(orderReleaseId1).getOrderLines().get(0);

		integrationTestsHelper.updateTaxDataAtLine(orderLineEntry.getId().toString(),taxRate,(orderLineEntry.getFinalAmount()*taxRate)/100);

		//Get ATP and IMS data After placing order
		atPandIMSDataEntryAfterPlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is placed
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount());

		//Mark order Packed
		integrationTestsHelper.markOrderInParticularStatus(orderReleaseId1,ReleaseStatus.PK);
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.PK);

		//Verify ATP and IMS inventory after order is packed
		atPandIMSDataEntryAfterOrderPacked = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getAtpBOCCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getImsBOCCount());

		//Mark order Delivered and check
		integrationTestsHelper.markOrderInParticularStatus(orderReleaseId1,ReleaseStatus.DL);
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.DL);

		//Verify ATP and IMS inventory after order is Delivered
		//This is just additional check that after pack there should not be any change in ATP and IMS
		atPandIMSDataEntryAfterOrderDelivered = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryAfterOrderDelivered.getAtpInventoryCount(),
				atPandIMSDataEntryAfterOrderPacked.getAtpInventoryCount(),
				atPandIMSDataEntryAfterOrderDelivered.getAtpBOCCount(),
				atPandIMSDataEntryAfterOrderPacked.getAtpBOCCount(),
				atPandIMSDataEntryAfterOrderDelivered.getImsInventoryCount(),
				atPandIMSDataEntryAfterOrderPacked.getImsInventoryCount(),
				atPandIMSDataEntryAfterOrderDelivered.getImsBOCCount(),
				atPandIMSDataEntryAfterOrderPacked.getImsBOCCount());

		//TODO: Check Data in LMS system after Packet is Delivered

		//TODO: Check Data in WMS system after order  is Packed

		sft.assertAll();
	}

	@Test(enabled=true,groups={"integration"}, description="TC007:Create order and Mark order Delivered, verify BOC and inventory in ATP and IMS and check order is shipped in LMS, WMS and OMS.")
    public void createOrderAndMarkOrderDeliveredWhereTaxIsCGST_SGST() throws Exception{
		sft = new SoftAssert();
		SkuEntry[] skuEntries = {new SkuEntry(skuId1, 1,styleId1)};

		//Update inventory in IMS before placing order

		imsServiceHelper.updateInventoryForSeller(new String[]{skuId1+":"+warehouse36+":100:0:"+sellerId_HEAL},supplyTypeOnHand);



		//Get ATP and IMS data before placing order
		atPandIMSDataEntryBeforePlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		//Create order and check order level onhold is resolved
		orderID = integrationTestsHelper.createOrderHelper(login, password, pincode_560068, skuEntries,clientId_myntra,tenantId_myntra,dataSource);
		sft.assertEquals(omsServiceHelper.validateOrderOFFHoldStatusInOMS(orderID,15), Boolean.TRUE.booleanValue(),"Order LevelOnhold is not resolved");
		orderReleaseId1 = omsServiceHelper.getOrderEntry(orderID).getOrderReleases().get(0).getId().toString();
		//Check order is in WP status
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.WP);
		omsServiceHelper.checkIfDataIsnotNullAtLineAndReleaseLevelInOMS(orderID);

		//Get ATP and IMS data After placing order
		atPandIMSDataEntryAfterPlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is placed
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount());

		//Mark order Packed
		integrationTestsHelper.markOrderInParticularStatus(orderReleaseId1,ReleaseStatus.PK);
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.PK);

		//Verify ATP and IMS inventory after order is packed
		atPandIMSDataEntryAfterOrderPacked = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getAtpBOCCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getImsBOCCount());

		//Mark order Delivered and check
		integrationTestsHelper.markOrderInParticularStatus(orderReleaseId1,ReleaseStatus.DL);
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.DL);

		//Verify ATP and IMS inventory after order is Delivered
		//This is just additional check that after pack there should not be any change in ATP and IMS
		atPandIMSDataEntryAfterOrderDelivered = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryAfterOrderDelivered.getAtpInventoryCount(),
				atPandIMSDataEntryAfterOrderPacked.getAtpInventoryCount(),
				atPandIMSDataEntryAfterOrderDelivered.getAtpBOCCount(),
				atPandIMSDataEntryAfterOrderPacked.getAtpBOCCount(),
				atPandIMSDataEntryAfterOrderDelivered.getImsInventoryCount(),
				atPandIMSDataEntryAfterOrderPacked.getImsInventoryCount(),
				atPandIMSDataEntryAfterOrderDelivered.getImsBOCCount(),
				atPandIMSDataEntryAfterOrderPacked.getImsBOCCount());

		//TODO: Check Data in LMS system after Packet is Delivered

		//TODO: Check Data in WMS system after order  is Packed

		sft.assertAll();
	}



	@Test(enabled=true,groups={"integration"}, description="TC007:Create order and Mark order Delivered, verify BOC and inventory in ATP and IMS and check order is shipped in LMS, WMS and OMS.")
	public void createOrderForJITSkuTillWP() throws Exception{
		sft = new SoftAssert();
		SkuEntry[] skuEntries = {new SkuEntry(skuId5, 1,styleId2)};

		//Update inventory in IMS before placing order
		imsServiceHelper.updateInventoryForSeller(new String[]{skuId5+":"+warehouse20+":100:0:"+sellerId_HEAL},supplyTypeJIT);

		//Get ATP and IMS data before placing order
		atPandIMSDataEntryBeforePlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId5, warehouse20, storeIDMyntra, sellerId_HEAL);

		//Create order and check order level onhold is resolved
		orderID = integrationTestsHelper.createOrderHelper(login, password, pincode_560068, skuEntries,clientId_myntra,tenantId_myntra,dataSource);
		sft.assertEquals(omsServiceHelper.validateOrderOFFHoldStatusInOMS(orderID,15), Boolean.TRUE.booleanValue(),"Order LevelOnhold is not resolved");
		orderReleaseId1 = omsServiceHelper.getOrderEntry(orderID).getOrderReleases().get(0).getId().toString();
		//Check order is in WP status
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.WP);
		omsServiceHelper.checkIfDataIsnotNullAtLineAndReleaseLevelInOMS(orderID);

		integrationTestsHelper.cancellationSuccess(orderReleaseId1, login, 1, cancelReasonId,cancelComment,CancelType.ORDER_LINE_CANCEL);

	}

	@Test(enabled=true,groups={"integration"}, description="TC008:Create order and Mark order LOST, verify BOC and inventory in ATP and IMS and check order is LOST in LMS and OMS and shipped in WMS. Verify Refund.")
    public void createOrderAndMarkOrderLOST() throws Exception{
		sft = new SoftAssert();
		SkuEntry[] skuEntries = {new SkuEntry(skuId1, 1,styleId1)};

		//Update inventory in IMS before placing order

		imsServiceHelper.updateInventoryForSeller(new String[]{skuId1+":"+warehouse36+":100:0:"+sellerId_HEAL},supplyTypeOnHand);



		//Get ATP and IMS data before placing order
		atPandIMSDataEntryBeforePlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		//Create order and check order level onhold is resolved
		orderID = integrationTestsHelper.createOrderHelper(login, password, pincode_560068, skuEntries,clientId_myntra,tenantId_myntra,dataSource);
		sft.assertEquals(omsServiceHelper.validateOrderOFFHoldStatusInOMS(orderID,15), Boolean.TRUE.booleanValue(),"Order LevelOnhold is not resolved");
		orderReleaseId1 = omsServiceHelper.getOrderEntry(orderID).getOrderReleases().get(0).getId().toString();
		//Check order is in WP status
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.WP);

		//Get ATP and IMS data After placing order
		atPandIMSDataEntryAfterPlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is placed
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount());

		//Mark order Packed
		integrationTestsHelper.markOrderInParticularStatus(orderReleaseId1,ReleaseStatus.PK);
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.PK);

		//Verify ATP and IMS inventory after order is packed
		atPandIMSDataEntryAfterOrderPacked = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getAtpBOCCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getImsBOCCount());

		//Mark order LOST and check
		integrationTestsHelper.markOrderInParticularStatus(orderReleaseId1,ReleaseStatus.LOST);
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.L);

		//Verify ATP and IMS inventory after order is Delivered
		//This is just additional check that after pack there should not be any change in ATP and IMS
		atPandIMSDataEntryAfterOrderDelivered = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryAfterOrderDelivered.getAtpInventoryCount(),
				atPandIMSDataEntryAfterOrderPacked.getAtpInventoryCount(),
				atPandIMSDataEntryAfterOrderDelivered.getAtpBOCCount(),
				atPandIMSDataEntryAfterOrderPacked.getAtpBOCCount(),
				atPandIMSDataEntryAfterOrderDelivered.getImsInventoryCount(),
				atPandIMSDataEntryAfterOrderPacked.getImsInventoryCount(),
				atPandIMSDataEntryAfterOrderDelivered.getImsBOCCount(),
				atPandIMSDataEntryAfterOrderPacked.getImsBOCCount());

		//Verify refund PPS Id in OMS once order is LOST
		integrationTestsHelper.verifyRefundPPSIdGeneratedInOMS(orderReleaseId1);

		//TODO: Check Data in WMS system after order  is Packed

		//TODO: Check Data in LMS system after Packet is LOST

		sft.assertAll();
	}

	@Test(enabled=true,groups={"integration"}, description="TC009:Create order and Mark order RTO, verify BOC and inventory in ATP and IMS and check order is RTO in LMS, and OMS and shipped in WMS. Verify Refund.")
    public void createOrderAndMarkOrderRTO() throws Exception{
		sft = new SoftAssert();
		SkuEntry[] skuEntries = {new SkuEntry(skuId1, 1,styleId1)};

		//Update inventory in IMS before placing order

		imsServiceHelper.updateInventoryForSeller(new String[]{skuId1+":"+warehouse36+":100:0:"+sellerId_HEAL},supplyTypeOnHand);



		//Get ATP and IMS data before placing order
		atPandIMSDataEntryBeforePlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		//Create order and check order level onhold is resolved
		orderID = integrationTestsHelper.createOrderHelper(login, password, pincode_560068, skuEntries,clientId_myntra,tenantId_myntra,dataSource);
		sft.assertEquals(omsServiceHelper.validateOrderOFFHoldStatusInOMS(orderID,15), Boolean.TRUE.booleanValue(),"Order LevelOnhold is not resolved");
		orderReleaseId1 = omsServiceHelper.getOrderEntry(orderID).getOrderReleases().get(0).getId().toString();
		//Check order is in WP status
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.WP);

		//Get ATP and IMS data After placing order
		atPandIMSDataEntryAfterPlacingOrder = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);
		//Validate ATP and IMS data after order is placed
		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryBeforePlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getAtpBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount(),
				atPandIMSDataEntryBeforePlacingOrder.getImsBOCCount()+1,
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount());

		//Mark order Packed
		integrationTestsHelper.markOrderInParticularStatus(orderReleaseId1,ReleaseStatus.PK);
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.PK);

		//Verify ATP and IMS inventory after order is packed
		atPandIMSDataEntryAfterOrderPacked = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryAfterPlacingOrder.getAtpInventoryCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getAtpInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getAtpBOCCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getAtpBOCCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsInventoryCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getImsInventoryCount(),
				atPandIMSDataEntryAfterPlacingOrder.getImsBOCCount()-1,
				atPandIMSDataEntryAfterOrderPacked.getImsBOCCount());

		//Mark order LOST and check
		integrationTestsHelper.markOrderInParticularStatus(orderReleaseId1,ReleaseStatus.RTO);
		omsServiceHelper.checkReleaseStatusForOrder(orderID, EnumSCM.RTO);

		//Verify ATP and IMS inventory after order is Delivered
		//This is just additional check that after pack there should not be any change in ATP and IMS
		atPandIMSDataEntryAfterOrderDelivered = commonUtils.getATPAndIMSInventoryAndBOC(skuId1, warehouse36, storeIDMyntra, sellerId_HEAL);

		commonUtils.verfyIMSAndATPData(atPandIMSDataEntryAfterOrderDelivered.getAtpInventoryCount(),
				atPandIMSDataEntryAfterOrderPacked.getAtpInventoryCount(),
				atPandIMSDataEntryAfterOrderDelivered.getAtpBOCCount(),
				atPandIMSDataEntryAfterOrderPacked.getAtpBOCCount(),
				atPandIMSDataEntryAfterOrderDelivered.getImsInventoryCount(),
				atPandIMSDataEntryAfterOrderPacked.getImsInventoryCount(),
				atPandIMSDataEntryAfterOrderDelivered.getImsBOCCount(),
				atPandIMSDataEntryAfterOrderPacked.getImsBOCCount());

		//Verify refund PPS Id in OMS once order is RTO
		integrationTestsHelper.verifyRefundPPSIdGeneratedInOMS(orderReleaseId1);

		//TODO: Check Data in WMS system after order  is Packed

		//TODO: Check Data in LMS system after Packet is RTO

		sft.assertAll();
	}



    

}
