package com.myntra.apiTests.erpservices.CashRecon.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class cashRecon_NonStore {

    static Logger log = Logger.getLogger(cashRecon_NonStore.class);

    StoreClient storeClient;
    DeliveryCenterClient deliveryCenterClient;

    LMSOrderDetailsClient lmsOrderDetailsClient;
    MasterBagClient masterBagClient;
    TripOrderAssignmentClient tripOrderAssignmentClient;

    MLShipmentClientV2 mlShipmentServiceV2Client;
    DeliveryStaffClient deliveryStaffClient;
    private LMSHelper lmsHepler = new LMSHelper();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private LastmileHelper lastmileHelper;
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        deliveryCenterClient = new DeliveryCenterClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        deliveryStaffClient = new DeliveryStaffClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }


    @Test
    public void createShipment() throws  Exception{

        String orderId = lmsHepler.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);

    }

}
