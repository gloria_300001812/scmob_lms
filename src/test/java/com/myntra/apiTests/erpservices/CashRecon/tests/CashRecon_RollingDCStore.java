package com.myntra.apiTests.erpservices.CashRecon.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.apiTests.erpservices.khata.validator.KhataAccountValidator;
import com.myntra.apiTests.erpservices.khata.validator.LedgerValidator;
import com.myntra.apiTests.erpservices.khata.validator.PaymentValidator;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_StoreFlowsDP;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.tests.LastMileTestDP;
import com.myntra.apiTests.erpservices.lms_khata.validator.LMSKhataValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;
import com.myntra.khata.response.PaymentResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.ItemEntry;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lms.khata.domain.EventType;
import com.myntra.lms.khata.domain.PaymentMode;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class CashRecon_RollingDCStore {



    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    LMSKhataValidator lmsKhataValidator = new LMSKhataValidator();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();

    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private OrderEntry orderEntry = new OrderEntry();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private ProcessRelease processRelease = new ProcessRelease();
    private ItemEntry itemEntry = new ItemEntry();
    MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(CashReconIntegration.class);
    HashMap<String, String> pincodeDCMap;
    String deliveryStaffID1, deliveryStaffID2, DcId;
    PaymentValidator paymentValidator =new PaymentValidator();
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa=new DeliveryStaffClient_QA();

    //DATA for FD_DL to be used for completing the trip
    String packetId, packetId1, trackingNumber, trackingNumber1;
    long tripId;
    List<Map<String, Object>> tripData = new ArrayList<>();
    Map<String, Object> dataMap1 = new HashMap<>();
    Map<String, Object> dataMap2 = new HashMap<>();
    TripOrderAssignmentResponse tripOrderAssignmentResponse1;
    KhataServiceHelper khataHelper = new KhataServiceHelper();
    Long SDA1_account_id, SDA2_account_id;

    //TODO : Account ID's
    Long INSTAKART, MYNTRA, DC ;

    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() throws IOException, JAXBException, InterruptedException {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.CASH_RECON_ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.CASH_RECON_ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
        DcId = pincodeDCMap.get(LMS_PINCODE.HSR_ML);
        //TODO : uncomment as account is not getting created so using an existing 1
        deliveryStaffID1 = ""+ lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        deliveryStaffID2 = ""+ lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
//        deliveryStaffID1 = "3396";
//        deliveryStaffID2 = "1503";

        Thread.sleep(5000); // waiting for SDA account to be created in khata service
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();
        AccountResponse accountResponse2 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID2), AccountType.SDA);
        SDA2_account_id = accountResponse2.getAccounts().get(0).getId();

        AccountResponse accountResponse3 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_INSTAKART_ID), AccountType.PARTNER);
        INSTAKART = accountResponse3.getAccounts().get(0).getId();
        AccountResponse accountResponse4 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_MYNTRA_ID), AccountType.PARTNER);
        MYNTRA = accountResponse4.getAccounts().get(0).getId();
        AccountResponse accountResponse5 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(DcId), AccountType.DC);
        DC = accountResponse5.getAccounts().get(0).getId();
    }




    //TODO : Receive Storebag at Store already with Shipment and CASH Pendency(Rolling)
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllwithHSR", enabled = true)
    public void store13(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                        String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        Double cash = 0.0, goods = 0.0, cash_new = 0.0, goods_new = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;
        LedgerValidator ledgerValidator = new LedgerValidator();

        Long originPremiseId = 1l;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> forwardTrackingNumbersList1 = new ArrayList<>();
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();




        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());


        // Update Store as Rolling RECON.

        StoreResponse storeResponse2 = lastmileHelper.updateStore(name, address, pincode, city, state, storeTenantId,
                true, true, isCardEnabled, code, ReconType.ROLLING_RECON, storeHlPId);
        StoreResponse storeResponse4 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");

        Assert.assertEquals(storeResponse4.getStoreEntries().get(0).getReconType().toString(),ReconType.ROLLING_RECON.toString(), "The Store Type is NOT matching");


        //get the store account
        Thread.sleep(3000);
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse.getAccounts().get(0).getId();

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse3 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse3.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);


        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 2nd order
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount1 = orderResponse1.getOrders().get(0).getCodAmount(); // for 2nd order

        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);
        String formatted_amount1 = format.format(amount1);

        forwardTrackingNumbersList.add(trackingNum);
        forwardTrackingNumbersList.add(trackingNum1);
        //Validate Event , Ledger , shipment pendency creation for SHIPMENT_CREATION EVENT

        Thread.sleep(2000);

        lmsKhataValidator.validateEventCreation(trackingNum, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum, INSTAKART.toString(), "0.00", "0.00", formatted_amount, "false", "4019", "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum1, INSTAKART.toString(), "0.00", "0.00", formatted_amount1, "false", "4019", "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);


        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);


        //Validate Event , Ledger , shipment pendency creation for RECEIVE_IN_DC EVENT
        Thread.sleep(5000);

        lmsKhataValidator.validateEventCreation(trackingNum, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", null, formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", null, formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum, DC.toString(), "0.00", "0.00", formatted_amount, "false", "4019", "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum1, DC.toString(), "0.00", "0.00", formatted_amount1, "false", "4019", "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);


        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNum, null, null, null, null, null, null, false).build();
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNum1, null, null, null, null, null, null, false).build();


        //adding seperately and not in loop to make the pendency calculation easier
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));

        // getting account of SDA
        accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), trackingNum, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");



        //Delivering
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");


        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNum, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), Store_account_id.toString(), "true", null, formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(Store_account_id), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);

        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), Store_account_id.toString(), "true", null, formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(Store_account_id), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNum, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        //after failing the delivery in store nothing should change
        Thread.sleep(5000);
        //balancing the account pendency of DC
        //khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
        //khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));

        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(trackingNum, EventType.DELIVERED.toString(), Store_account_id.toString(), INSTAKART.toString(), "true", "Null", formatted_amount, formatted_amount, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(Store_account_id), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNum);

        long tripOrderAssignmentId2 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();

        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNum1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.failedDeliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);


        Thread.sleep(8000);


        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(Store_account_id), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);

        // modify to yesterday date

        lastmileHelper.modifiedMLLastmilePartnerShipmentDate(code);


        ShipmentResponse masterBagResponse1 = null;

        try {
            masterBagResponse1 = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        Long masterBagId1 = masterBagResponse1.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId1);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId1);


        String orderId2 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId2 = omsServiceHelper.getPacketId(orderId2);
        String trackingNum2 = lmsHelper.getTrackingNumber(packetId2);
        String orderReleaseId2 = omsServiceHelper.getReleaseId(orderId2);// 1st order
        OrderResponse orderResponse2 = lmsServiceHelper.getOrderDetails(packetId2);
        Double amount2 = orderResponse2.getOrders().get(0).getCodAmount(); // for 1st order

        String formatted_amount2 = format.format(amount);
        forwardTrackingNumbersList1.add(trackingNum2);


        //Validate Event , Ledger , shipment pendency creation for SHIPMENT_CREATION EVENT

        Thread.sleep(2000);

        lmsKhataValidator.validateEventCreation(trackingNum2, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, INSTAKART.toString(), "0.00", "0.00", formatted_amount2, "false", "4019", "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNum2);


        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId2, packetId2);


        //Validate Event , Ledger , shipment pendency creation for RECEIVE_IN_DC EVENT
        Thread.sleep(5000);

        lmsKhataValidator.validateEventCreation(trackingNum2, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", null, formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, DC.toString(), "0.00", "0.00", formatted_amount2, "false", "4019", "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), trackingNum2);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNum2);


        ShipmentUpdateInfo shipmentUpdateInfo2 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, trackingNum2, null, null, null, null, null, null, false).build();


        //adding seperately and not in loop to make the pendency calculation easier
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo2);
        } catch (IOException e) {
            e.printStackTrace();
        }


        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));

        // getting account of SDA
        accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.


        TripResponse tripResponse2 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Long tripId1 = tripResponse2.getTrips().get(0).getId();
        String tripNumber1 = tripResponse2.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse2.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId1 + " , which is created from delivery staff id - " + tripResponse2.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse2.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber1 + " and the trip status is : " + tripResponse2.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse2.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse2.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber1 + " and the trip status is : " + tripResponse2.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId1), String.valueOf(masterBagId1),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId1 + " to the trip : " + tripId1 + " , trip number : " + tripResponse2.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId1 + " to the trip : " + tripId1 + " , trip number : " + tripResponse2.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse2.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse2.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId1)), "The shipmentId is " + tripShipmentAssociationResponse2.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId1);
        Assert.assertTrue(tripShipmentAssociationResponse2.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId1)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId1, 1, code, originPremiseId);
        String param1 = tripNumber1 + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse3 = lmsServiceHelper.findShipmentsByTripNumber(param1);
        Assert.assertEquals(tripShipmentAssociationResponse3.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse3.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse3.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), trackingNum2, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        TripShipmentAssociationResponse tripShipmentAssociationResponse4 = lmsServiceHelper.findShipmentsByTripNumber(param1);
        Assert.assertEquals(tripShipmentAssociationResponse4.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");



        // get Current Account pendency for store : before delivering storebag to store


        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(Store_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        //Delivering
        TripShipmentAssociationResponse shipmentResponse1 = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId1), forwardTrackingNumbersList1, String.valueOf(tripId1), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId1)));
        //TODO : response object returns tripId null
        Assert.assertTrue(shipmentResponse1.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse1.getStatus().getTotalCount() == 1);
        TripShipmentAssociationResponse tripShipmentAssociationResponse5 = lmsServiceHelper.findShipmentsByTripNumber(param1);
        Assert.assertEquals(tripShipmentAssociationResponse5.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");


        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNum2, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), Store_account_id.toString(), "true", null, formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(Store_account_id), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), trackingNum2);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNum2);

        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        // validate  Account pendency for store : after delivering storebag to store

        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash), String.valueOf(goods + amount2));


        TripResponse storeTrip1 = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList1, storeDeliveryStaffId);



        // get Current Account pendency for store : before delivering order to customer


        AccountPendencyResponse accountPendencyResponse3 = khataHelper.getAccountPendencyByaccount(Store_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse3.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getGoods().toString());
        }


        Assert.assertTrue(storeTrip1.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber1 = storeTrip1.getTrips().get(0).getTripNumber();
        Long storeTripId1 = storeTrip1.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip1.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber1 + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList1.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap1 = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment1 = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId3 = tripOrderAssignment1.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList1.get(0))).findFirst().  get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId3, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse4 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNum2, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse5 = tripClient_qa.deliverStoreOrders(storeTripId1, String.valueOf(mlShipmentResponse4.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId3, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        //after failing the delivery in store nothing should change
        Thread.sleep(5000);


        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(trackingNum2, EventType.DELIVERED.toString(), Store_account_id.toString(), INSTAKART.toString(), "true", "Null", formatted_amount2, formatted_amount2, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(Store_account_id), formatted_amount2, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount2), LedgerType.GOODS.toString(), trackingNum2);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount2), LedgerType.CASH.toString(), trackingNum2);


        // validate  Account pendency for store: after delivering order to customer

        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash + amount2), String.valueOf(goods - amount2 ));


    }



    // DL store bag-> Add less amount to reverse bag -> HAndover In DC
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void excessPaymentByRollingStore(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = 1l;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> forwardTrackingNumbersList1 = new ArrayList<>();
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();




        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());


        // Update Store as Rolling RECON.

        StoreResponse storeResponse2 = lastmileHelper.updateStore(name, address, pincode, city, state, storeTenantId,
                true, true, isCardEnabled, code, ReconType.ROLLING_RECON, storeHlPId);
        StoreResponse storeResponse4 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");

        Assert.assertEquals(storeResponse4.getStoreEntries().get(0).getReconType().toString(),ReconType.ROLLING_RECON.toString(), "The Store Type is NOT matching");

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, tenantId);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));



        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");


        //wfd
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickip
        // Pickup the reverse bag

        float amount = 10000.00f;

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,amount,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");
        log.info(reverseShipmentResponse.getTripShipmentAssociationEntryList().get(0).getAmountCollected());
        log.info(reverseShipmentResponse.getTripShipmentAssociationEntryList().get(0).getAmountToBeCollected());

    }





}
