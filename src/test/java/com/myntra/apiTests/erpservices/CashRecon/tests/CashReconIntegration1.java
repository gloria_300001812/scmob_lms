package com.myntra.apiTests.erpservices.CashRecon.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.apiTests.erpservices.khata.validator.KhataAccountValidator;
import com.myntra.apiTests.erpservices.khata.validator.LedgerValidator;
import com.myntra.apiTests.erpservices.khata.validator.PaymentValidator;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.tests.LastMileTestDP;
import com.myntra.apiTests.erpservices.lms.tests.MLShipmentResponseValidator;
import com.myntra.apiTests.erpservices.lms.tests.TripOrderAssignmentResponseValidator;
import com.myntra.apiTests.erpservices.lms_khata.validator.LMSKhataValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;
import com.myntra.khata.response.PaymentResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.DeliveryStaffCommute;
import com.myntra.lastmile.client.code.utils.DeliveryStaffRole;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lms.khata.domain.EventType;
import com.myntra.lms.khata.domain.PaymentMode;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateActivityTypeSource;
import com.myntra.logistics.platform.domain.ShipmentUpdateAdditionalInfo;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class CashReconIntegration1 {


    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    LMSKhataValidator lmsKhataValidator = new LMSKhataValidator();
    PaymentValidator paymentValidator =new PaymentValidator();
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa=new DeliveryStaffClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa=new TripOrderAssignmentClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();

    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private OrderEntry orderEntry = new OrderEntry();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private ProcessRelease processRelease = new ProcessRelease();
    private ItemEntry itemEntry = new ItemEntry();
    MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(CashReconIntegration1.class);
    HashMap<String, String> pincodeDCMap;
    String deliveryStaffID1, deliveryStaffID2, DcId;


    //DATA for FD_DL to be used for completing the trip
    String packetId, packetId1, trackingNumber, trackingNumber1;
    long tripId;
    List<Map<String, Object>> tripData = new ArrayList<>();
    Map<String, Object> dataMap1 = new HashMap<>();
    Map<String, Object> dataMap2 = new HashMap<>();
    TripOrderAssignmentResponse tripOrderAssignmentResponse1;
    KhataServiceHelper khataHelper = new KhataServiceHelper();
    Long SDA1_account_id, SDA2_account_id;
    String storeTenantId , storeCode , mobileNumber;
    Long storeHlPId;


    //TODO : Account ID's
    Long INSTAKART, MYNTRA, DC;

    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() throws IOException, JAXBException, InterruptedException {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.CASH_RECON_ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.CASH_RECON_ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
        DcId = pincodeDCMap.get(LMS_PINCODE.CASH_RECON_ML_BLR);
        //TODO : uncomment as account is not getting created so using an existing 1
        deliveryStaffID1 = ""+ lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
//        deliveryStaffID2 = ""+ lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
//        deliveryStaffID1 = "3396";
//        deliveryStaffID2 = "1503";

        Thread.sleep(7000); // waiting for SDA account to be created in khata service
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();
//        AccountResponse accountResponse2 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID2), AccountType.SDA);
//        SDA2_account_id = accountResponse2.getAccounts().get(0).getId();

        AccountResponse accountResponse3 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_INSTAKART_ID), AccountType.PARTNER);
        INSTAKART = accountResponse3.getAccounts().get(0).getId();
        AccountResponse accountResponse4 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_MYNTRA_ID), AccountType.PARTNER);
        MYNTRA = accountResponse4.getAccounts().get(0).getId();
        AccountResponse accountResponse5 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(DcId), AccountType.DC);
        DC = accountResponse5.getAccounts().get(0).getId();

    }

    @BeforeClass(alwaysRun = true)
    public void createStore() throws IOException, JAXBException, InterruptedException {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        StoreResponse storeResponse = null;


        while(storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), "code.like:" + ("KL" + String.valueOf(contactNumber).substring(5, 9)), "").getStoreEntries() == null){
            r = new Random(System.currentTimeMillis());
            contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        }

        storeResponse = lastmileHelper.createStore("KL" + String.valueOf(contactNumber).substring(5, 9), "KL" + String.valueOf(contactNumber).substring(5, 9), "KL" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.DELIVERY_HUB_CODE_ELC,
                "LA_gstin" + String.valueOf(contactNumber).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.ROLLING_RECON, 500, "KL" + String.valueOf((contactNumber)+r.nextInt(1000)).substring(5, 9));
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeCode = storeResponse.getStoreEntries().get(0).getCode();
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        mobileNumber =  "" + contactNumber;
    }




//TODO : DL 2(1099*2) orders from a SDA and close trip and pay 200 and check LIFO works - > DL 1 FD 1 more with new trip -> pay complete and all should be settled

//TODO : Order of shipment creation and DL is same here . Lifo is applied on DL time
    //TODO : Uncomment Event creation 4 are commented out
    @Test(groups = {"Smoke", "Regression"}, priority = 1, description = "Master Bag with one shipment", enabled = true)
    public void sdaPayment() throws Exception {

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_DC_Initial=0.0, goods_DC_Initial = 0.0;
        Double cash_pending_amount_INSTAKART = 0.0, goods_pending_amount_INSTAKART = 0.0, cash_collected_amount_INSTAKART = 0.0;
        Double cash_pending_amount_DC = 0.0, goods_pending_amount_DC = 0.0, cash_collected_amount_DC = 0.0;
        Double cash_pending_amount_SDA = 0.0, goods_pending_amount_SDA = 0.0, cash_collected_amount_SDA = 0.0;

        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

        //get current account pendency of IK to Myntra
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// DL
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId);

        Thread.sleep(7000);
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId1 = omsServiceHelper.getPacketId(orderId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1); // DL
        String trackingNum2 = lmsHelper.getTrackingNumber(packetId1);
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);

        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse1.getOrders().get(0).getCodAmount(); // for 2nd order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount2 = format1.format(amount2);



        Thread.sleep(7000);
        //TODO : Check Shipment Creation event and shipment pendency
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1.toString(), "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateEventCreation(trackingNum2, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount2.toString(), "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount2.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validation pendency for IK to myntra
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount1 + amount2)); // 2 orders amount plus existing 1

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNum2);

        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC_Initial = cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC_Initial = goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);

        Thread.sleep(7000);
        //TODO : check Receive_in_dc event
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNum2, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(DC), "0.00", "0.00", formatted_amount2.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 + amount2));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), trackingNum2);


        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNum2);


        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum2, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId1, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.OUT_FOR_DELIVERY, 2));

        //
        Thread.sleep(7000);
        //TODO : Delivered Event will  be triggered on event table
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNum2, EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        //TODO : should knock off prev pendency of DC and create pendency of SDA
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount2.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validate account pendency on SDA to DC but account will be used for instakart
        Thread.sleep(5000);
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1 + amount2));

        //ledger validation
        //DC to INSTAKART will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), trackingNum2);

        //SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNum2);



        // delivering both the order

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(7000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        //complete trip
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        //complete trip
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //TODO: Dl event will be generated
        Thread.sleep(7000);
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.DELIVERED.toString(), SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, formatted_amount1, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID ,"Null");

        lmsKhataValidator.validateEventCreation(trackingNum2, EventType.DELIVERED.toString(), SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount2, formatted_amount2, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");

        //shipment pendency will move from sda from goods to cash
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), formatted_amount1.toString(), "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(SDA1_account_id), formatted_amount2.toString(), "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validate Account pendency
        // SDA will now have a pendency of cash and goods both as 1 FD and 1 DL
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount2 +amount1), String.valueOf(goods_SDA));

        //ledger validation
        // item delivered goods pendency will be removed  from both  item
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount2), LedgerType.GOODS.toString(), trackingNum2);

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount2), LedgerType.CASH.toString(), trackingNum2);

        //TODO : Now as Cash pendency is created for both the tracking number. we will be paying a 200 and see if Last Delivered is pendency is removed
        // TODO : Continued ->  then we will pay 1500 and check last DL item is removed and some amount on 2nd last item is removed.
        System.out.println("stop"); //reconcile the money -200



        //Paying RS 200 for the SDA
        Float sda_payment_1 = 200F;
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);

        PaymentResponse paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = "PAYMENT_"+paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_payment_1.toString(), PaymentStatus.APPROVED.toString());

        //TODO : Event creation for payment is_processed is 0 but it's processed need to comment out to proceed further

        //validate event, payment event will be created
        Thread.sleep(7000);
        lmsKhataValidator.validateEventCreation(trackingNum2, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "200.00", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID ,payment_id_1);

        //validate shipment pendency the amount will be cleared off the 2nd Delivered item first
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(SDA1_account_id), "899.00", "200.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        //for DC pending will be 200 for same tracking num
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(DC), "200.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());

        //validate account pendency
        cash_SDA =  cash_SDA + amount1+amount2 -200;
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA ), String.valueOf(goods_SDA));
        cash_DC = cash_DC + 200.00;// existing + 200 will be given by DC
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));

        //validate ledger

        //knocking off 200 from sda account
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(200.00), LedgerType.CASH.toString(), trackingNum2);
        //adding 200 to dc account
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-200.00), LedgerType.CASH.toString(), trackingNum2);

        //part 2


        String orderId3 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId3 = omsServiceHelper.getPacketId(orderId3);
        String orderReleaseId3 = omsServiceHelper.getReleaseId(orderId3);// DL

        String trackingNum3 = lmsHelper.getTrackingNumber(packetId3);

        String orderId4 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId4 = omsServiceHelper.getPacketId(orderId4);
        String orderReleaseId4 = omsServiceHelper.getReleaseId(orderId4);// DL

        String trackingNum4 = lmsHelper.getTrackingNumber(packetId4);

        //amount
        OrderResponse orderResponse2 = lmsServiceHelper.getOrderDetails(packetId);
        Double amount3 = orderResponse2.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount3 = format2.format(amount3);


        OrderResponse orderResponse3 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount4 = orderResponse3.getOrders().get(0).getCodAmount(); // for 2nd order
        DecimalFormat format3 = new DecimalFormat("0.00");
        String formatted_amount4 = format3.format(amount4);

        Thread.sleep(7000);
        //TODO : Check Shipment Creation event and shipment pendency
        lmsKhataValidator.validateEventCreation(trackingNum3, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount3, formatted_amount3, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNum4, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount4, formatted_amount4, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum3, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount3, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum4, String.valueOf(INSTAKART), "0.00", "0.00",formatted_amount4, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validation pendency for IK to myntra
        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount3 + amount4)); // 2 orders amount plus existing 1

        //validation of ledger pendency
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount3), LedgerType.GOODS.toString(), trackingNum3);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount4), LedgerType.GOODS.toString(), trackingNum4);


        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId3, packetId3);
        //
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId4, packetId4);

        Thread.sleep(7000);
        //TODO : check Receive_in_dc event
        lmsKhataValidator.validateEventCreation(trackingNum3, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount3, formatted_amount3, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateEventCreation(trackingNum4, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount4, formatted_amount4, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");


        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNum3, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum4, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        lmsKhataValidator.validateShipmentPendency(trackingNum3, String.valueOf(DC), "0.00", "0.00", formatted_amount3.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum4, String.valueOf(DC), "0.00", "0.00", formatted_amount4.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount3 + amount4));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount3), LedgerType.GOODS.toString(), trackingNum3);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount4), LedgerType.GOODS.toString(), trackingNum4);


        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount3), LedgerType.GOODS.toString(), trackingNum3);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount4), LedgerType.GOODS.toString(), trackingNum4);





        //deliver 4th  order and FD 3rd order
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Long tripId1 = tripResponse1.getTrips().get(0).getId();
        String tripNumber1 = tripResponse1.getTrips().get(0).getTripNumber();



        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum3, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum4, tripId1, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");


        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId3, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId3, EnumSCM.OUT_FOR_DELIVERY, 2));

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId4, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId4, EnumSCM.OUT_FOR_DELIVERY, 2));

        Thread.sleep(10000);
        // OUT_FOR_DELIVERY
        //event creation
        lmsKhataValidator.validateEventCreation(trackingNum3, EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount3, formatted_amount3, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateEventCreation(trackingNum4, EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount4, formatted_amount4, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");

        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNum3, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum4, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        lmsKhataValidator.validateShipmentPendency(trackingNum3, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount3.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum4, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount4.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //account pendency
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount3 + amount4));


        //validate ledger
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount3), LedgerType.GOODS.toString(), trackingNum3);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount4), LedgerType.GOODS.toString(), trackingNum4);


        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount3), LedgerType.GOODS.toString(), trackingNum3);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount4), LedgerType.GOODS.toString(), trackingNum4);



        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId1),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId4, tripId1),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        //after marking the ordeer 3 as failed and 4th as DL
        //Delivered event will be triggered
        Thread.sleep(7000);
        //event creation
        lmsKhataValidator.validateEventCreation(trackingNum4, EventType.DELIVERED.toString(), SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount4, formatted_amount4, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");

        //shipment pendency
        lmsKhataValidator.validateShipmentPendency(trackingNum3, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount3.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum4, String.valueOf(SDA1_account_id), formatted_amount4.toString(), "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validate account pendency
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA+ amount4), String.valueOf(goods_SDA + amount3));

        //validate ledger
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount3), LedgerType.GOODS.toString(), trackingNum3);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount4), LedgerType.GOODS.toString(), trackingNum4);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount4), LedgerType.CASH.toString(), trackingNum4);


        //complete trip
        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId3, tripId1));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId4, tripId1));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        tripData.add(dataMap1);
        tripData.add(dataMap2);
        //complete trip

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNum3, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Thread.sleep(7000);
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        // after receiving the FD order in DC
        //validate Event creation
        lmsKhataValidator.validateEventCreation(trackingNum3, EventType.RECEIVED_IN_DC.toString(), SDA1_account_id.toString(), DC.toString(), "true", "Null", formatted_amount3, formatted_amount3, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        Thread.sleep(17000);

        //shipment pendency
        lmsKhataValidator.validateShipmentPendency(trackingNum3, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum3, String.valueOf(DC), "0.00", "0.00", formatted_amount3.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //account pendency
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA+ amount4), String.valueOf(goods_SDA));
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount3));

        //ledger
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount3), LedgerType.GOODS.toString(), trackingNum3);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount3), LedgerType.GOODS.toString(), trackingNum3);

        goods_DC = goods_DC+amount3;
        cash_SDA = cash_SDA + amount4;


        //paying the complete amount
        Float sda_payment_2 = 3097F;
        response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_2, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);

        paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_2 = "PAYMENT_"+paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_payment_2.toString(), PaymentStatus.APPROVED.toString());

        //TODO : Event creation for payment is_processed is 0 but it's processed
        //event creation is_setteled =1 at every place
        //3 event will be created with the same payment id knocking off 4 then 2 then 1 in LIFO sequence
        Thread.sleep(20000);
        lmsKhataValidator.validateEventCreation(trackingNum4, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "1099.00", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID ,payment_id_2);
        lmsKhataValidator.validateEventCreation(trackingNum2, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "1099.00", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID ,payment_id_2);
        lmsKhataValidator.validateEventCreation(trackingNum1, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "899.00", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID ,payment_id_2);

        //3 shipment pendency will be resolved and is_setteled will be 1
        lmsKhataValidator.validateShipmentPendency(trackingNum4, String.valueOf(SDA1_account_id), "0.00", "1099.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(SDA1_account_id), "0.00", "1099.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), "0.00", "1099.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());

        lmsKhataValidator.validateShipmentPendency(trackingNum4, String.valueOf(DC), "1099.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(DC), "1099.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "1099.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());

        //account pendency
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), "0.0", "0.0"); //sda paid all the amount
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC_Initial+sda_payment_2+sda_payment_1), String.valueOf(goods_DC_Initial + amount3));

        //validate ledger
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount3), LedgerType.GOODS.toString(), trackingNum3);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount3), LedgerType.GOODS.toString(), trackingNum3);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount4), LedgerType.CASH.toString(), trackingNum4);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount4), LedgerType.CASH.toString(), trackingNum4);

    }


    //adding  payment cases
    //TODO : less and excess payment
    @Test(groups = {"Smoke", "Regression"}, priority = 1, description = "C20280,C20284,C20285,C20296,C20345,C20346,C20347", enabled = true)
    public void excessCashDepositedBySDA() throws Exception {
        Float sda_payment_1 = 2000F;
        Float sda_correct_amount = 1099F;
        Long wrong_DC_acnt_id = 81L, wrong_SDA_account_id = 5263L;

        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);

        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        SoftAssert softAssert = new SoftAssert();

        //Wrong DC
        PaymentResponse response =  khataHelper.createAndApprove(wrong_DC_acnt_id, SDA1_account_id, sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "creation of payment for wrong DC and Correct SDA should not be allowed" + " DC and SDA Account ID:" + wrong_DC_acnt_id +" "+ SDA1_account_id);

        //wrong SDA
         response =  khataHelper.createAndApprove(DC, wrong_SDA_account_id, sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "creation of payment with wrong SDA should not be allowed"+ " DC and SDA Account ID:" + DC +" "+ wrong_SDA_account_id);
        //correct SDA and DC Excess amount
        response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Creation of payment with excess amount should not be allowed" + " DC and SDA Account ID:" + DC +" "+ SDA1_account_id);

        //correct sda dc amount
        response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "exact amount payment failed"+ " DC and SDA Account ID:" + DC +" "+ SDA1_account_id);

        Thread.sleep(7000);
        //paying again with no pendency should throw error
        response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "paying again with same amount should throw error"+ " DC and SDA Account ID:" + DC +" "+ SDA1_account_id );

        //without DC
        response =  khataHelper.createAndApproveForNegScenarios("", SDA1_account_id.toString(), sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Without DC account ID payment should not be allowed "+ " DC and SDA Account ID:" + DC +" "+ SDA1_account_id);

        //without SDA
        response =  khataHelper.createAndApproveForNegScenarios(DC.toString(), "", sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "without sda account id payment should not be allowed"+ " DC and SDA Account ID:" + DC +" "+ SDA1_account_id);

        //without DC and SDA
        response =  khataHelper.createAndApproveForNegScenarios("", "", sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "without sda and dc account id payment should not be allowed"+ " DC and SDA Account ID:" + DC +" "+ SDA1_account_id);

        softAssert.assertAll();

    }

    //deleting the sda
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , Forward all flow test", enabled = true)
    public void deletingSDA() throws Exception {

        Map<String, Object> sda = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select * from delivery_staff where id = " + deliveryStaffID1, "lms");

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Thread.sleep(3000);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateGetTripOrderByTripNumber(lmsHelper.getTrackingNumber(packetId), packetId, ShippingMethod.NORMAL.toString(), "Delivered");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //DELETING the SDA
        //lmsServiceHelper.updateDeliveryStaff(sda.get("id").toString(),sda.get("code").toString(),sda.get("first_name").toString(),sda.get("last_name").toString(),Long.parseLong(sda.get("delivery_center_id").toString()),sda.get("mobile").toString(),sda.get("created_by").toString(),true,true, DeliveryStaffCommute.BIKER,sda.get("emp_code").toString(),true, DeliveryStaffRole.STAFF);

        //Paying RS 200 for the SDA
        Float sda_payment_1 = 200F;
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);


        PaymentResponse paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = "PAYMENT_"+paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_payment_1.toString(), PaymentStatus.APPROVED.toString());

        //TODO : Event creation for payment is_processed is 0 but it's processed need to comment out to proceed further

        //validate event, payment event will be created
        Thread.sleep(7000);
        lmsKhataValidator.validateEventCreation(trackingNum, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "200.00", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID ,payment_id_1);

        //validate shipment pendency the amount will be cleared off the 2nd Delivered item first
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        //for DC pending will be 200 for same tracking num

        //validate account pendency
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();


        //validate ledger

        //knocking off 200 from sda account
        LedgerValidator ledgerValidator = new LedgerValidator();

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(200.00), LedgerType.CASH.toString(), trackingNum);
        //adding 200 to dc account
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-200.00), LedgerType.CASH.toString(), trackingNum);

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , Forward all flow test", enabled = true)
    public void inactiveSDA() throws Exception {

        Map<String, Object> sda = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select * from delivery_staff where id = " + deliveryStaffID1, "lms");

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Thread.sleep(3000);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateGetTripOrderByTripNumber(lmsHelper.getTrackingNumber(packetId), packetId, ShippingMethod.NORMAL.toString(), "Delivered");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //inactive the SDA
        //lmsServiceHelper.updateDeliveryStaff(sda.get("id").toString(),sda.get("code").toString(),sda.get("first_name").toString(),sda.get("last_name").toString(),Long.parseLong(sda.get("delivery_center_id").toString()),sda.get("mobile").toString(),sda.get("created_by").toString(),false,true, DeliveryStaffCommute.BIKER,sda.get("emp_code").toString(),true, DeliveryStaffRole.STAFF);

        //Paying RS 200 for the SDA
        Float sda_payment_1 = 200F;
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);


        PaymentResponse paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = "PAYMENT_"+paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_payment_1.toString(), PaymentStatus.APPROVED.toString());

        //TODO : Event creation for payment is_processed is 0 but it's processed need to comment out to proceed further

        //validate event, payment event will be created
        Thread.sleep(7000);
        lmsKhataValidator.validateEventCreation(trackingNum, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "200.00", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID ,payment_id_1);

        //validate shipment pendency the amount will be cleared off the 2nd Delivered item first
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        //for DC pending will be 200 for same tracking num

        //validate account pendency
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();


        //validate ledger

        //knocking off 200 from sda account
        LedgerValidator ledgerValidator = new LedgerValidator();

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(200.00), LedgerType.CASH.toString(), trackingNum);
        //adding 200 to dc account
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-200.00), LedgerType.CASH.toString(), trackingNum);

    }

    //marking store inactive
    // Dl store bag -> dl orders
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C20361 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentFromInactiveStore(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                         String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);

        //validation pendency for IK to myntra
        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //validate account pendency and ledger. now pendency will go to DC

        //Instakart amount will be balanced after receiving the shipment in DC
        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);

        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);




        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);

        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(Store_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        //Delivering
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");


        //once store bag is delivered to store pendency will move from SDA to store . as of now pendency will move to store
        //validate account pendency which will move to store

        Thread.sleep(5000);
        //balancing the account pendency of DC
        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), Store_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, Store_account_id.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));

        //validate ledger dc to instakart will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);



        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        // Assert.assertEquals(tripShipmentAssociationResponse.get);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        // DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient.findDeliveryStaffByMobNo(mobileNumber);
//        long expectedstoreDeliveryStaffId = Long.parseLong(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile());
//        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);

        //tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(tripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to deliver orders in store bag");

        //after delivering the order to customer pendency of the store will move from goods to cash
        //account pendency
        Thread.sleep(5000);

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), Store_account_id.toString(), Store_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, Store_account_id.toString(), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //balancing the account pendency of store will move from goods to cash
        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE+ amount1), String.valueOf(goods_STORE));

        //validate ledger goods will be balanced and cash will formed
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        //marking the store inactive
        StoreResponse storeResponse1 = lastmileHelper.UpdateStoreInActive(name, address, pincode, city, state,
                storeTenantId, isAvailable, isDeleted, isCardEnabled, false, code ,hlpReconType, storeHlPId );
        log.info(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive());
        Assert.assertEquals(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive().toString(),"false");

        //creating a payment from store
        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickup
        // Pickup the reverse bag

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStore(String.valueOf(reverseBagId), null, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, forwardTrackingNumbersList.size(), 0, amount1.floatValue(),LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        Thread.sleep(5000);


        lmsKhataValidator.validateEventCreation(tracking_num, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_ADDED_TO_REVERSE_BAG,Store_account_id.toString() , SDA1_account_id.toString(), "true", "Null", formatted_amount1.toString(), "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, Store_account_id.toString(), "0.00", formatted_amount1, "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, SDA1_account_id.toString(), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");



        //Once reverese bag is picked up from store pendency should move from Store to SDA
        //account pendency balancing the store
        //balancing the account pendency of DC
        //khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE +amount1 ));
        // TODO : once code is pushed from dev side we need to validate this event as well
        //khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA +amount1 ));

        //validate ledger store penedency will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Store_account_id.toString(), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), SDA1_account_id.toString(), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);

    }

    // Dl store bag -> dl orders
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C20361 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentFromDeletedStore(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                         String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);

        //validation pendency for IK to myntra
        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //validate account pendency and ledger. now pendency will go to DC

        //Instakart amount will be balanced after receiving the shipment in DC
        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);

        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);




        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);

        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(Store_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        //Delivering
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");


        //once store bag is delivered to store pendency will move from SDA to store . as of now pendency will move to store
        //validate account pendency which will move to store

        Thread.sleep(5000);
        //balancing the account pendency of DC
        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), Store_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, Store_account_id.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));

        //validate ledger dc to instakart will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);



        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        // Assert.assertEquals(tripShipmentAssociationResponse.get);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        // DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient.findDeliveryStaffByMobNo(mobileNumber);
//        long expectedstoreDeliveryStaffId = Long.parseLong(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile());
//        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);

        //tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(tripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to deliver orders in store bag");

        //after delivering the order to customer pendency of the store will move from goods to cash
        //account pendency
        Thread.sleep(5000);

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), Store_account_id.toString(), Store_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, Store_account_id.toString(), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //balancing the account pendency of store will move from goods to cash
        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE+ amount1), String.valueOf(goods_STORE));

        //validate ledger goods will be balanced and cash will formed
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        //deleted store
        StoreResponse storeResponse1 = lastmileHelper.UpdateStoreInActive(name, address, pincode, city, state,
                storeTenantId, isAvailable, true, isCardEnabled, true, code ,hlpReconType, storeHlPId );
        log.info(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive());
        Assert.assertEquals(storeClient_qa.findStoreById(storeHlPId).getStoreEntries().get(0).getActive().toString(),"false");

        //creating a payment from store
        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickup
        // Pickup the reverse bag

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStore(String.valueOf(reverseBagId), null, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, forwardTrackingNumbersList.size(), 0, amount1.floatValue(),LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        Thread.sleep(5000);


        lmsKhataValidator.validateEventCreation(tracking_num, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_ADDED_TO_REVERSE_BAG,Store_account_id.toString() , SDA1_account_id.toString(), "true", "Null", formatted_amount1.toString(), "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, Store_account_id.toString(), "0.00", formatted_amount1, "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, SDA1_account_id.toString(), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");



        //Once reverese bag is picked up from store pendency should move from Store to SDA
        //account pendency balancing the store
        //balancing the account pendency of DC
        //khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE +amount1 ));
        // TODO : once code is pushed from dev side we need to validate this event as well
        //khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA +amount1 ));

        //validate ledger store penedency will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Store_account_id.toString(), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), SDA1_account_id.toString(), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);

    }

    // prepaid normal flow
    //TODO : DL a normal order and pay via prepaid and check if pendency is removed from every stake holder
    @Test
    public void prepaid_dl_order() throws Exception {
        //for first order
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// FD
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId);


        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);



        Thread.sleep(7000);
        //TODO : Check Shipment Creation event and shipment pendency
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validation pendency for IK to myntra
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount1 )); // 2 orders amount plus existing 1

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);

        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //

        Thread.sleep(7000);
        //TODO : check Receive_in_dc event
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");


        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);


        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);


        // OFD for FD and DL Order
        // Received_in_dc
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        trackingNumber = lmsHelper.getTrackingNumber(packetId);

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }

        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        //

        Thread.sleep(7000);
        //TODO : Delivered Event will  be triggered on event table
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");

        //TODO : should knock off prev pendency of DC and create pendency of SDA
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validate account pendency on SDA to DC but account will be used for instakart
        Thread.sleep(5000);
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1));

        //ledger validation
        //DC to INSTAKART will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);

        //SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);


        //Failing and delivering the order
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_PAYMENT_TYPE_DEBIT_CARD ).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_PAYMENT_TYPE_DEBIT_CARD ).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


        Thread.sleep(9000);

        // TODO : DL event will be generated
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.DELIVERED.toString(), SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CARD_ON_DELIVERY.toString(), formatted_amount1, formatted_amount1, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


        //all pendency should be removed from all stake holders as payment was done online
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), "0.00", "0.00","0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate Account pendency
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));

        // SDA will now have a pendency of cash and goods both as 1 FD and 1 DL
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA ));

        //ledger validation
        // item delivered goods pendency will be removed and item which got Failed ledger will remain same. 1st will remain same ledger

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);
        //ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNum1);


    }

    //prepaid for store orders
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void prepaid_store_orders(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                       String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);

        //validation pendency for IK to myntra
        Thread.sleep(5000);
        // validate Event and shipment pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //validate account pendency and ledger. now pendency will go to DC

        //Instakart amount will be balanced after receiving the shipment in DC
        Thread.sleep(5000);
        //validate Event and shipment pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);

        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);




        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);

        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(Store_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        //Delivering
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");


        //once store bag is delivered to store pendency will move from SDA to store . as of now pendency will move to store
        //validate account pendency which will move to store

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), Store_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, Store_account_id.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //balancing the account pendency of DC
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));

        //validate ledger dc to instakart will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);



        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        // Assert.assertEquals(tripShipmentAssociationResponse.get);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        // DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient.findDeliveryStaffByMobNo(mobileNumber);
//        long expectedstoreDeliveryStaffId = Long.parseLong(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile());
//        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);

        //tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(tripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = tripClient_qa.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_PAYMENT_TYPE_DEBIT_CARD,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to deliver orders in store bag");

        //after delivering the order to customer pendency of the store will move from goods to cash
        //account pendency
        Thread.sleep(5000);
        //Deliver Store Order
        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), Store_account_id.toString(), INSTAKART.toString(), "true", PaymentMode.CARD_ON_DELIVERY.toString(), formatted_amount1, formatted_amount1, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


        //all pendency should be removed from all stake holders as payment was done online
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA1_account_id), "0.00", "0.00","0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //balancing the account pendency of store will move from goods to cash
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));

        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE));

        //validate ledger goods will be balanced and cash will formed
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);


    }


}
