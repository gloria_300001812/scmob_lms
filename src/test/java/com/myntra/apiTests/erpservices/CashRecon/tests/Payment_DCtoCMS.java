package com.myntra.apiTests.erpservices.CashRecon.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.apiTests.erpservices.khata.validator.KhataAccountValidator;
import com.myntra.apiTests.erpservices.khata.validator.LedgerValidator;
import com.myntra.apiTests.erpservices.khata.validator.PaymentValidator;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.tests.LastMileTestDP;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.lms_khata.validator.LMSKhataValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;
import com.myntra.khata.response.ExcessResponse;
import com.myntra.khata.response.PaymentResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.PickupResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lms.khata.domain.EventType;
import com.myntra.lms.khata.domain.PaymentMode;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import com.myntra.lordoftherings.boromir.DBUtilities;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import java.awt.peer.ListPeer;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT;

public class Payment_DCtoCMS {


    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);



    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(Payment_DCtoCMS.class);
    HashMap<String, String> pincodeDCMap;
    String deliveryStaffID1, deliveryStaffID2, DCId = "1";
    PaymentValidator paymentValidator =new PaymentValidator();
    LedgerValidator ledgerValidator = new LedgerValidator();
    LMSKhataValidator lmsKhataValidator = new LMSKhataValidator();
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa=new DeliveryStaffClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    StatusPollingValidator statusPollingValidator=new StatusPollingValidator();
    LastmileOperations lastmileOperations = new LastmileOperations();




    //DATA for FD_DL to be used for completing the trip
    String packetId, packetId1, trackingNumber, trackingNumber1;
    long tripId;
    List<Map<String, Object>> tripData = new ArrayList<>();
    Map<String, Object> dataMap1 = new HashMap<>();
    Map<String, Object> dataMap2 = new HashMap<>();
    KhataServiceHelper khataHelper = new KhataServiceHelper();
    Long SDA1_account_id, SDA2_account_id;

    //TODO : Account ID's
    Long INSTAKART, MYNTRA, DC , EKART;

    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() throws IOException, JAXBException, InterruptedException {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.CASH_RECON_ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.CASH_RECON_ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
        DCId = pincodeDCMap.get(LMS_PINCODE.CASH_RECON_ML_BLR);
        //TODO : uncomment as account is not getting created so using an existing 1
        deliveryStaffID1 = ""+ lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DCId));
        deliveryStaffID2 = ""+ lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DCId));

        Thread.sleep(7000); // waiting for SDA account to be created in khata service
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();
        AccountResponse accountResponse2 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID2), AccountType.SDA);
        SDA2_account_id = accountResponse2.getAccounts().get(0).getId();

        AccountResponse accountResponse3 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_INSTAKART_ID), AccountType.PARTNER);
        INSTAKART = accountResponse3.getAccounts().get(0).getId();
        AccountResponse accountResponse4 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_MYNTRA_ID), AccountType.PARTNER);
        MYNTRA = accountResponse4.getAccounts().get(0).getId();
        AccountResponse accountResponse5 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(DCId), AccountType.DC);
        DC = accountResponse5.getAccounts().get(0).getId();
        AccountResponse accountResponse6 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EKART_ID), AccountType.PARTNER);
        EKART = accountResponse6.getAccounts().get(0).getId();
    }

    @Test
    public void paymentDctoCMS_less() throws Exception , UnmarshalException {


        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;

        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount();
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);
        Thread.sleep(7000);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId1 = omsServiceHelper.getPacketId(orderId1);
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId1);

        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount1 = orderResponse1.getOrders().get(0).getCodAmount();
        String formatted_amount1 = format.format(amount1);



        // Create Trip
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DCId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Thread.sleep(7000);

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //getting current pendency of DC to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }

    //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        //validate account pendency on SDA to DC but account will be used for Instakart
        Thread.sleep(5000);
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
   //     khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC - amount - amount1));
     //   khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount+amount1 ));

        //ledger validation - DC to INSTAKART will be balanced

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);

        //ledger validation - SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);




        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse3 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse3.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getGoods().toString());
        }



        //Deliver the Order

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Thread.sleep(7000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


        //validate Account pendency and ledger
        Thread.sleep(7000);
     //   khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount + amount1), String.valueOf(goods_SDA - amount - amount1));
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNum);

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNum1);



        //Todo: Add payment settlement by DC to SDA.



        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse4 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse4.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse4.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse4.getAccountPendency().get(0).getGoods().toString());
        }


        //Paying RS 200 for the SDA
        Float sda_payment_1 = 1000F;
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);

        PaymentResponse paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_payment_1.toString(), PaymentStatus.APPROVED.toString());

        //TODO : Event creation for payment is_processed is 0 but it's processed need to comment out to proceed further

        //validate event, payment event will be created
        Thread.sleep(7000);
        //lmsKhataValidator.validateEventCreation(trackingNum1, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", "Null", "Null" ,payment_id_1 , "false");

            //validate shipment pendency the amount will be cleared off the 2nd Delivered item first
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(SDA1_account_id), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), "99.00", "1000.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        //for DC pending will be 200 for same tracking num
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "1000.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());



        //validate account pendency
        cash_SDA =  cash_SDA + amount+amount1 -sda_payment_1;
       // khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA ), String.valueOf(0.0));
        cash_DC = cash_DC + sda_payment_1;// existing + 200 will be given by DC
       // khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_SDA));

        //validate ledger

        //knocking off 200 from sda account
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(sda_payment_1), LedgerType.CASH.toString(), trackingNum1);
        //adding 200 to dc account
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-sda_payment_1), LedgerType.CASH.toString(), trackingNum1);


        // get current cash pendency on DC.

        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        Float amount_depostied_by_DC = 100f;
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));

    // Create Payment from DC to CMS.

        Long paymentId = KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getPayments().get(0).getId();

        //validate payment creation from DC to INSTAKART
        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,"PENDING",PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,null);

        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();


    // MIS Report upload from DC

        String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,amount_depostied_by_DC.toString());
        KhataServiceHelper.uploadMISReport(file_path);

        //validate payment approval .

        Thread.sleep(5000);

        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,"APPROVED",PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);


        // validate shipment pendency shoratge amount
        //LMSKhataValidator.validateShortageamount(DC,INSTAKART,"4019",currentAmountPendency-amount_depostied_by_DC);
        Float AmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Assert.assertEquals(currentAmountPendency-amount_depostied_by_DC,AmountPendency,"Amount Pendency is not changing in account pendency ");

    }


    @Test
    public void paymentDctoCMS_equal() throws Exception , UnmarshalException {


        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;

        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount();
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);
        Thread.sleep(7000);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId1 = omsServiceHelper.getPacketId(orderId1);
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId1);

        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount1 = orderResponse1.getOrders().get(0).getCodAmount();
        String formatted_amount1 = format.format(amount1);



        // Create Trip
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DCId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Thread.sleep(7000);

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //getting current pendency of DC to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }

        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        //validate account pendency on SDA to DC but account will be used for Instakart
        Thread.sleep(5000);
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC - amount - amount1));
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount+amount1 ));
//
//        //ledger validation - DC to INSTAKART will be balanced

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);

        //ledger validation - SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);




        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse3 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse3.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getGoods().toString());
        }



        //Deliver the Order

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Thread.sleep(7000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


        //validate Account pendency and ledger
        Thread.sleep(7000);
     //   khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount + amount1), String.valueOf(goods_SDA - amount - amount1));
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNum);

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNum1);



        //Todo: Add payment settlement by DC to SDA.



        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse4 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse4.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse4.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse4.getAccountPendency().get(0).getGoods().toString());
        }


        //Paying RS 200 for the SDA
        Float sda_payment_1 = 1000F;
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);

        PaymentResponse paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_payment_1.toString(), PaymentStatus.APPROVED.toString());

        //TODO : Event creation for payment is_processed is 0 but it's processed need to comment out to proceed further

        //validate event, payment event will be created
        Thread.sleep(7000);
//        lmsKhataValidator.validateEventCreation(trackingNum1, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "200.00", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID ,payment_id_1);

        //validate shipment pendency the amount will be cleared off the 2nd Delivered item first
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(SDA1_account_id), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), "99.00", "1000.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        //for DC pending will be 200 for same tracking num
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "1000.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());



        //validate account pendency
        cash_SDA =  cash_SDA + amount+amount1 - sda_payment_1;
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA ), String.valueOf(0.0));
        cash_DC = cash_DC + sda_payment_1;// existing + 200 will be given by DC
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_SDA));

        //validate ledger

        //knocking off 200 from sda account
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(sda_payment_1), LedgerType.CASH.toString(), trackingNum1);
        //adding 200 to dc account
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-sda_payment_1), LedgerType.CASH.toString(), trackingNum1);


        // get current cash pendency on DC.

        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));

        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();

        Float amount_depostied_by_DC = currentAmountPendency;


        // Create Payment from DC to CMS.

        Long paymentId = KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getPayments().get(0).getId();

        //validate payment creation from DC to INSTAKART
        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,"PENDING",PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,null);

        // MIS Report upload from DC

        String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,amount_depostied_by_DC.toString());
        KhataServiceHelper.uploadMISReport(file_path);

        Thread.sleep(5000);


        //validate payment approval .

        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,"APPROVED",PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);


        // validate shipment pendency shoratge amount
        //LMSKhataValidator.validateShortageamount(DC,INSTAKART,"4019",currentAmountPendency-amount_depostied_by_DC);
        Float AmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Assert.assertEquals(currentAmountPendency-amount_depostied_by_DC,AmountPendency,"Amount Pendency is not changing in account pendency ");




    }
//
//
//
//    @Test
//    public void paymentDctoCMS_more() throws Exception , UnmarshalException {
//
//
//        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
//        Random r = new Random(System.currentTimeMillis());
//        int random = Math.abs(1000000000 + r.nextInt(2000000000));
//        String paymentReferenceId= String.valueOf(random).substring(2, 9);
//        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));
//
//        // Get current pendency on DC
//
//        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
//        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();
//
//        Float amount_depostied_by_DC = currentAmountPendency+100.00f;
//
//        // Create Payment from DC to CMS.
//
//        Assert.assertEquals(KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getStatus().getStatusType().toString(),"ERROR","Excess amount is allowing");
//        log.info(KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getStatus().getStatusMessage());
//
//        Thread.sleep(5000);
//        // validate shipment pendency shoratge amount
//        //LMSKhataValidator.validateShortageamount(DC,INSTAKART,"4019",currentAmountPendency);
//        Float AmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
//        Assert.assertEquals(currentAmountPendency,AmountPendency, "Excess is allowing");
//
//
//
//    }


    @Test

    public void WrongMISupload() throws Exception {


        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;

        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount();
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);
        Thread.sleep(7000);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId1 = omsServiceHelper.getPacketId(orderId1);
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId1);

        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount1 = orderResponse1.getOrders().get(0).getCodAmount();
        String formatted_amount1 = format.format(amount1);



        // Create Trip
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DCId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Thread.sleep(7000);

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //getting current pendency of DC to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }

        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        //validate account pendency on SDA to DC but account will be used for Instakart
        Thread.sleep(5000);
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC - amount - amount1));
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount+amount1 ));

        //ledger validation - DC to INSTAKART will be balanced

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);

        //ledger validation - SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);




        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse3 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse3.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getGoods().toString());
        }



        //Deliver the Order

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Thread.sleep(7000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


        //validate Account pendency and ledger
        Thread.sleep(7000);
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount + amount1), String.valueOf(goods_SDA - amount - amount1));
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNum);

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNum1);






        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse4 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse4.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse4.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse4.getAccountPendency().get(0).getGoods().toString());
        }


        Float sda_payment_1 = 1000F;
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);

        PaymentResponse paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_payment_1.toString(), PaymentStatus.APPROVED.toString());

        //TODO : Event creation for payment is_processed is 0 but it's processed need to comment out to proceed further

        //validate event, payment event will be created
        Thread.sleep(7000);
        lmsKhataValidator.validateEventCreation(trackingNum1, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "200.00", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID ,payment_id_1);

        //validate shipment pendency the amount will be cleared off the 2nd Delivered item first
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(SDA1_account_id), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), "99.00", "1000.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        //for DC pending will be 200 for same tracking num
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "1000.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());



        //validate account pendency
        cash_SDA =  cash_SDA + amount+amount1 -sda_payment_1;
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA ), String.valueOf(0.0));
        cash_DC = cash_DC + sda_payment_1;// existing + 200 will be given by DC
        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_SDA));

        //validate ledger

        //knocking off 200 from sda account
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(sda_payment_1), LedgerType.CASH.toString(), trackingNum1);
        //adding 200 to dc account
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-sda_payment_1), LedgerType.CASH.toString(), trackingNum1);


        // get current cash pendency on DC.

        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        Float amount_depostied_by_DC = 100f;
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));

        // Create Payment from DC to CMS.

        Long paymentId = KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getPayments().get(0).getId();

        //validate payment creation from DC to INSTAKART
        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,"PENDING",PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,null);

        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();

        Float MISUploadAmount =amount_depostied_by_DC+100.00f;

        // MIS Report upload from DC

        String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,MISUploadAmount.toString());
        KhataServiceHelper.uploadMISReport(file_path);

        //validate payment approval .


        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,"PENDING",PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,false);

    }




    @Test
    public void paymentCouriertoCMS_less() throws Exception , UnmarshalException {

        Double cash = 0.0, goods = 0.0 , IKcash = 0.0 , IKgoods = 0.0 , EKcash = 0.0 , EKgoods = 0.0;
        Double  cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;


        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

        // create Shipment

        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.IS, "560069", "EK", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        log.info("The value of shipment : "+amount);

        //validation pendency for IK to myntra
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount )); // 2 orders amount plus existing 1

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);


        // Add to MB

        Long masterBagId = lmsServiceHelper.createMasterBag(18l,"HUB","Bangalore",1638l, "DC","Bangalore", EnumSCM.SUCCESS).getEntries().get(0)
                .getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, ShipmentType.DL), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close masterBag");



        //get current account pendency of EKART to Myntra


        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(EKART, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
            log.info("cash and goods :" + goods +"," +cash);
        }

        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            IKcash = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            IKgoods = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }
        Thread.sleep(5000);

        //Ship MB

        lmsServiceHelper.shipMasterBag(masterBagId);

        //validation pendency for IK to Ekart

        Thread.sleep(5000);
      //  khataAccountValidator.validateAccountPendency(Long.toString(EKART), Long.toString(INSTAKART), cash.toString(), String.valueOf(goods + amount ) ); // 2 orders amount plus existing 1
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);

        //get current account pendency of IK to Myntra

//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), IKcash.toString(), String.valueOf(IKgoods - amount ) ); // 2 orders amount plus existing 1
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);



        // OFD

        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.OUT_FOR_DELIVERY , "EK" ,ShipmentType.DL);

        AccountPendencyResponse accountPendencyResponse3 = khataHelper.getAccountPendencyByaccount(EKART,INSTAKART ); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            EKcash = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getCash().toString());
            EKgoods = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getGoods().toString());
            log.info("cash and goods :" + EKcash +"," +EKgoods);
        }

        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.DELIVERED , "EK" ,ShipmentType.DL);

        Thread.sleep(10000);

      //  khataAccountValidator.validateAccountPendency(Long.toString(EKART), Long.toString(INSTAKART), String.valueOf(EKcash+amount ),String.valueOf(EKgoods - amount )); // 2 orders amount plus existing 1
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);


        // get current cash pendency on DC.

        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(EKART).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        Float amount_depostied_by_DC = 100f;
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER));

        // Create Payment from DC to CMS.

        Long paymentId = KhataServiceHelper.createPayment(INSTAKART,EKART,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getPayments().get(0).getId();

        //validate payment creation from DC to INSTAKART
        PaymentValidator.validatePaymentCreation(paymentId,EKART,INSTAKART,"PENDING",PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,null);

        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();


        // MIS Report upload from DC

        String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,amount_depostied_by_DC.toString());
        KhataServiceHelper.uploadMISReport(file_path);

        Thread.sleep(17000);

        //validate payment approval .

        PaymentValidator.validatePaymentCreation(paymentId,EKART,INSTAKART,"APPROVED",PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);


        // validate shipment pendency shoratge amount
        //LMSKhataValidator.validateShortageamount(EKART,INSTAKART,"4019",currentAmountPendency-amount_depostied_by_DC);
        Float AmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Assert.assertEquals(currentAmountPendency-amount_depostied_by_DC,AmountPendency,"Amount Pendency is not changing in account pendency ");

    }

    @Test
    public void paymentCouriertoCMS_equal() throws Exception , UnmarshalException {

        Double cash = 0.0, goods = 0.0 , IKcash = 0.0 , IKgoods = 0.0 , EKcash = 0.0 , EKgoods = 0.0;
        Double  cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;


        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

        // create Shipment

        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.IS, "560069", "EK", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        log.info("The value of shipment : "+amount);

        //validation pendency for IK to myntra
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
  //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount )); // 2 orders amount plus existing 1

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);


        // Add to MB

        Long masterBagId = lmsServiceHelper.createMasterBag(18l,"HUB","Bangalore",1638l, "DC","Bangalore", EnumSCM.SUCCESS).getEntries().get(0)
                .getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, ShipmentType.DL), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close masterBag");



        //get current account pendency of EKART to Myntra


        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(EKART, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
            log.info("cash and goods :" + goods +"," +cash);
        }

        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            IKcash = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            IKgoods = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }
        Thread.sleep(5000);

        //Ship MB

        lmsServiceHelper.shipMasterBag(masterBagId);

        //validation pendency for IK to Ekart

        Thread.sleep(5000);
    //    khataAccountValidator.validateAccountPendency(Long.toString(EKART), Long.toString(INSTAKART), cash.toString(), String.valueOf(goods + amount ) ); // 2 orders amount plus existing 1
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);

        //get current account pendency of IK to Myntra

    //    khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), IKcash.toString(), String.valueOf(IKgoods - amount ) ); // 2 orders amount plus existing 1
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);



        // OFD

        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.OUT_FOR_DELIVERY , "EK" ,ShipmentType.DL);

        AccountPendencyResponse accountPendencyResponse3 = khataHelper.getAccountPendencyByaccount(EKART,INSTAKART ); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            EKcash = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getCash().toString());
            EKgoods = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getGoods().toString());
            log.info("cash and goods :" + EKcash +"," +EKgoods);
        }

        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.DELIVERED , "EK" ,ShipmentType.DL);

        Thread.sleep(10000);

     //   khataAccountValidator.validateAccountPendency(Long.toString(EKART), Long.toString(INSTAKART), String.valueOf(EKcash+amount ),String.valueOf(EKgoods - amount )); // 2 orders amount plus existing 1
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);


        // get current cash pendency on DC.

        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(EKART).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER));


        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();
        Float amount_depostied_by_DC = currentAmountPendency;


        // Create Payment from DC to CMS.

        Long paymentId = KhataServiceHelper.createPayment(INSTAKART,EKART,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getPayments().get(0).getId();

        //validate payment creation from DC to INSTAKART
        PaymentValidator.validatePaymentCreation(paymentId,EKART,INSTAKART,"PENDING",PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,null);


        // MIS Report upload from DC

        String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,amount_depostied_by_DC.toString());
        KhataServiceHelper.uploadMISReport(file_path);


        Thread.sleep(5000);
        //validate payment approval .

       PaymentValidator.validatePaymentCreation(paymentId,EKART,INSTAKART,"APPROVED",PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,null);

        // validate shipment pendency shoratge amount
        // LMSKhataValidator.validateAllSettlement(EKART,INSTAKART,"4019",currentAmountPendency-amount_depostied_by_DC);

        Float AmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Assert.assertEquals(currentAmountPendency-amount_depostied_by_DC,AmountPendency,"Amount Pendency is not changing in account pendency ");


    }


    @Test
    public void paymentCouriertoCMS_more() throws Exception , UnmarshalException {

        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(EKART).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER));

        // Get current pendency on DC
        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();
        Float amount_depostied_by_DC = currentAmountPendency+100.00f;

        // Create Payment from DC to CMS.

        Assert.assertEquals(KhataServiceHelper.createPayment(INSTAKART,EKART,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getStatus().getStatusType().toString(),"ERROR","Excess amount is allowing");
        log.info(KhataServiceHelper.createPayment(INSTAKART,EKART,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getStatus().getStatusMessage());

        Thread.sleep(5000);
        // validate Account pendency shoratge amount
        Float AmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(ownerRefId,AccountType.PARTNER,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Assert.assertEquals(currentAmountPendency,AmountPendency,"amount pendency is changed for excess payment");


    }

  //TEST CASE 1: DC pendency 1000 -> pays 2099( 1000 + 1099)  -> DC pendency for 2nd item worth 1099  -> will trigger API(24 hr) to check if it's balanced or not.

    @Test(groups = {"Excess"})
    public void paymentDctoCMS_morethanexcessByTriggerAPI() throws Exception , UnmarshalException {


        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));

        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();
        Float excess_amount_deposited = 1099.00f;
        Float amount_depostied_by_DC = currentAmountPendency+excess_amount_deposited;


        // Create Excess Payment from DC to CMS.It should allow to create.

        PaymentResponse paymentResponse = KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId);
        log.info(KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getStatus().getStatusMessage());
        Assert.assertEquals(paymentResponse.getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");
        Long paymentId = paymentResponse.getPayments().get(0).getId();
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);
        String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,amount_depostied_by_DC.toString());
        KhataServiceHelper.uploadMISReport(file_path);
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,PaymentStatus.APPROVED.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);

        // Validate Excess entry is created
        ExcessResponse excessResponse = KhataServiceHelper.searchExcessByPaymentId(paymentId);
        Assert.assertEquals(excessResponse.getExcesses().get(0).getStatus(),PaymentStatus.CREATED,"Excess payment status is not matching");

        // Deliver the another order.

        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DCId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Thread.sleep(4000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount();
        DecimalFormat format = new DecimalFormat("0.00");
        DecimalFormat format1 = new DecimalFormat("0.0");
        String formatted_amount = format.format(amount);
        Float payment = amount.floatValue();
        String formatted_payment= format1.format(payment);

        // SDA pay amount.floatValue() to DC
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, payment, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);
        PaymentResponse paymentResponse1 =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        String payment_id_1 = paymentResponse1.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), formatted_payment, PaymentStatus.APPROVED.toString());

         ExcessResponse excessResponse2 = khataHelper.pushExcess(DC,INSTAKART);
         Thread.sleep(5000);
         ExcessResponse excessResponse1 = khataHelper.searchExcessByPaymentId(paymentId);
         Assert.assertEquals(excessResponse1.getExcesses().get(0).getStatus(),PaymentStatus.PROCESSING);

        // Validate Shipment Pendency

        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(DC), "0.00", formatted_amount, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(SDA1_account_id), "0.00", formatted_amount, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(INSTAKART), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, MYNTRA.toString());

    }


    //TEST CASE 1: DC pendency 1000 -> pays 2099( 1000 + 1099)  -> DC pendency for 2nd item worth 1099  -> will trigger API(24 hr) to check if it's balanced or not.

    @Test(groups = {"Excess"})
    public void paymentDctoCMS_morethanexcessByPayment() throws Exception , UnmarshalException {


        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));

        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();
        Float excess_amount_deposited = 100.00f;
        Float amount_depostied_by_DC = currentAmountPendency+excess_amount_deposited;


        // Create Excess Payment from DC to CMS.It should allow to create.

        PaymentResponse paymentResponse = KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId);
        log.info(KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getStatus().getStatusMessage());
        Assert.assertEquals(paymentResponse.getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");
        Long paymentId = paymentResponse.getPayments().get(0).getId();
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);
        String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,amount_depostied_by_DC.toString());
        KhataServiceHelper.uploadMISReport(file_path);
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,PaymentStatus.APPROVED.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);

        // Validate Excess entry is created
        ExcessResponse excessResponse = KhataServiceHelper.searchExcessByPaymentId(paymentId);
        Assert.assertEquals(excessResponse.getExcesses().get(0).getStatus(),PaymentStatus.CREATED,"Excess payment status is not matching");

        // Deliver the another order.

        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DCId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Thread.sleep(4000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount();
        DecimalFormat format = new DecimalFormat("0.00");
        DecimalFormat format1 = new DecimalFormat("0.0");
        String formatted_amount = format.format(amount);
        Float payment = amount.floatValue();
        String formatted_payment= format1.format(payment);

        // SDA pay amount.floatValue() to DC
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, payment, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);
        PaymentResponse paymentResponse1 =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        String payment_id_1 = paymentResponse1.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), formatted_payment, PaymentStatus.APPROVED.toString());

        // DC pay amount.floatValue()- excess_amount_deposited- Rs to CMS

        Float second_Excess = 100.00f;
        Float DC_Pendency = amount.floatValue()-excess_amount_deposited+second_Excess;
        String formatted_DC_Pendency= format.format(DC_Pendency);
        String paymentReferenceId1= String.valueOf(random).substring(1, 7);

        PaymentResponse paymentResponse3 = KhataServiceHelper.createPayment(INSTAKART,DC,DC_Pendency,PaymentType.CASH,paymentReferenceId1);
        log.info(KhataServiceHelper.createPayment(INSTAKART,DC,DC_Pendency,PaymentType.CASH,paymentReferenceId1).getStatus().getStatusMessage());
        Assert.assertEquals(paymentResponse3.getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");
        Long paymentId2 = paymentResponse3.getPayments().get(0).getId();
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreation(paymentId2,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId1,DC_Pendency,true);
        String file_path2 = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId1,DC_Pendency.toString());
        KhataServiceHelper.uploadMISReport(file_path2);
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreation(paymentId2,DC,INSTAKART,PaymentStatus.APPROVED.toString(),PaymentType.CASH,paymentReferenceId1,DC_Pendency,true);


        ExcessResponse excessResponse1 = khataHelper.searchExcessByPaymentId(paymentId);
        Assert.assertEquals(excessResponse1.getExcesses().get(0).getStatus(),PaymentStatus.PROCESSING);
        ExcessResponse excessResponse2 = khataHelper.searchExcessByPaymentId(paymentId2);
        Assert.assertEquals(excessResponse2.getExcesses().get(0).getStatus(),PaymentStatus.CREATED);

        // Validate Shipment Pendency

        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(DC), "0.00", formatted_amount, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(SDA1_account_id), "0.00", formatted_amount, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(INSTAKART), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, MYNTRA.toString());


    }



    // Test Case 2: SDA DL item 100 -> pays 200 -> SDA DL 2nd item worth 200  -> pays 100 and excess 100 should  be used  to balance it out.
     @Test(groups = {"Excess"})

    public void paymentDctoCMS_equaltoexcessByPayment() throws Exception , UnmarshalException {


        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));

        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();
        Float excess_amount_deposited = 100.00f;
        Float amount_depostied_by_DC = currentAmountPendency+excess_amount_deposited;


        // Create Excess Payment from DC to CMS.It should allow to create.

         PaymentResponse paymentResponse = KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId);
         log.info(KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getStatus().getStatusMessage());
         Assert.assertEquals(paymentResponse.getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");
         Long paymentId = paymentResponse.getPayments().get(0).getId();
         Thread.sleep(5000);
         PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);
         String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,amount_depostied_by_DC.toString());
         KhataServiceHelper.uploadMISReport(file_path);
         Thread.sleep(5000);
         PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,PaymentStatus.APPROVED.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);

         // Validate Excess entry is created
         ExcessResponse excessResponse = KhataServiceHelper.searchExcessByPaymentId(paymentId);
         Assert.assertEquals(excessResponse.getExcesses().get(0).getStatus(),PaymentStatus.CREATED,"Excess payment status is not matching");

        // Deliver the another order.

        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DCId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Thread.sleep(4000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount();
        DecimalFormat format = new DecimalFormat("0.00");
         DecimalFormat format1 = new DecimalFormat("0.0");
        String formatted_amount = format.format(amount);
        Float payment = amount.floatValue();
        String formatted_payment= format1.format(payment);

        // SDA pay amount.floatValue() to DC
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, payment, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);
        PaymentResponse paymentResponse1 =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        String payment_id_1 = paymentResponse1.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), formatted_payment, PaymentStatus.APPROVED.toString());

        // DC pay amount.floatValue()- excess_amount_deposited- Rs to CMS

         Float DC_Pendency = amount.floatValue()-excess_amount_deposited;
         String formatted_DC_Pendency= format.format(DC_Pendency);
         String paymentReferenceId1= String.valueOf(random).substring(1, 7);

         PaymentResponse paymentResponse3 = KhataServiceHelper.createPayment(INSTAKART,DC,DC_Pendency,PaymentType.CASH,paymentReferenceId1);
         log.info(KhataServiceHelper.createPayment(INSTAKART,DC,DC_Pendency,PaymentType.CASH,paymentReferenceId1).getStatus().getStatusMessage());
         Assert.assertEquals(paymentResponse3.getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");
         Long paymentId2 = paymentResponse3.getPayments().get(0).getId();
         Thread.sleep(5000);
         PaymentValidator.validatePaymentCreation(paymentId2,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId1,DC_Pendency,true);
         String file_path2 = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId1,DC_Pendency.toString());
         KhataServiceHelper.uploadMISReport(file_path2);
         Thread.sleep(5000);
         PaymentValidator.validatePaymentCreation(paymentId2,DC,INSTAKART,PaymentStatus.APPROVED.toString(),PaymentType.CASH,paymentReferenceId1,DC_Pendency,true);


         ExcessResponse excessResponse1 = khataHelper.searchExcessByPaymentId(paymentId);
         Assert.assertEquals(excessResponse1.getExcesses().get(0).getStatus(),PaymentStatus.PROCESSING);

         // Validate Shipment Pendency

         lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(DC), "0.00", formatted_amount, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
         lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(SDA1_account_id), "0.00", formatted_amount, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
         lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(INSTAKART), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, MYNTRA.toString());



     }



   // Test Case 3: SDA DL item 100 -> pays 200 (100 excess) -> DL item worth 200 -> pendency =100 ,pay 200    @Test
   @Test(groups = {"Excess"})
    public void payment3() throws Exception , UnmarshalException {


        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));

        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();
        Float excess_amount_deposited = 100.00f;
        Float amount_depostied_by_DC = currentAmountPendency+excess_amount_deposited;


        // Create Excess Payment from DC to CMS.It should allow to create.

        Assert.assertEquals(KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");
        log.info(KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getStatus().getStatusMessage());
        Thread.sleep(5000);

        //Todo add event and shipment pendency  ,  Excess entry is created.

        // Validate event and shipment pendency .

        // Validate Excess entry is created

        // validate shipment pendency shoratge amount
        LMSKhataValidator.validateShortageamount(DC,INSTAKART,"4019",amount_depostied_by_DC);
        Float AmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Assert.assertEquals(currentAmountPendency-amount_depostied_by_DC,AmountPendency, "Amount Pendency is worked as expected");

        // Deliver the another order.


        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DCId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Thread.sleep(4000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount();
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);
        Float payment = amount.floatValue();
        String formatted_payment= format.format(payment);


        //Paying RS 200 for the SDA
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, payment, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);

        PaymentResponse paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), formatted_payment, PaymentStatus.APPROVED.toString());



        //todo excess payment should settledown.
        // new Excess payment should created.
        // check if event entry is created.

        LMSKhataValidator.validateShortageamount(DC,INSTAKART,"4019",0.0f);

    }


    @Test(groups = "Excess")
    public void uploadMISfirstWithExcess() throws Exception {


        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));

        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float excess_amount_deposited = 100.00f;
        Float amount_depostied_by_DC = currentAmountPendency+excess_amount_deposited;


        // Create Excess Payment from DC to CMS.It should allow to create.
        String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,amount_depostied_by_DC.toString());
        KhataServiceHelper.uploadMISReport(file_path);
        Long paymentId = KhataServiceHelper.paymentSearchByPaymentReferenceId(paymentReferenceId).getPayments().get(0).getId();
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreationByMIS(paymentId,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);
        PaymentResponse paymentResponse = KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId);
        log.info(KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId).getStatus().getStatusMessage());
        Assert.assertEquals(paymentResponse.getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,PaymentStatus.APPROVED.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);

        // Validate Excess entry is created
        ExcessResponse excessResponse = KhataServiceHelper.searchExcessByPaymentId(paymentId);
        Assert.assertEquals(excessResponse.getExcesses().get(0).getStatus(),PaymentStatus.CREATED,"Excess payment status is not matching");

        // Deliver the another order.

        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DCId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Thread.sleep(4000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount();
        DecimalFormat format = new DecimalFormat("0.00");
        DecimalFormat format1 = new DecimalFormat("0.0");
        String formatted_amount = format.format(amount);
        Float payment = amount.floatValue();
        String formatted_payment= format1.format(payment);

        // SDA pay amount.floatValue() to DC
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, payment, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);
        PaymentResponse paymentResponse1 =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        String payment_id_1 = paymentResponse1.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), formatted_payment, PaymentStatus.APPROVED.toString());

        // DC pay amount.floatValue()- excess_amount_deposited- Rs to CMS

        Float DC_Pendency = amount.floatValue()-excess_amount_deposited;
        String formatted_DC_Pendency= format.format(DC_Pendency);
        String paymentReferenceId1= String.valueOf(random).substring(1, 7);

        String file_path2 = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId1,DC_Pendency.toString());
        KhataServiceHelper.uploadMISReport(file_path2);
        Long paymentId2 = KhataServiceHelper.paymentSearchByPaymentReferenceId(paymentReferenceId1).getPayments().get(0).getId();

        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreationByMIS(paymentId2,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId1,DC_Pendency,true);
        PaymentResponse paymentResponse3 = KhataServiceHelper.createPayment(INSTAKART,DC,DC_Pendency,PaymentType.CASH,paymentReferenceId1);
        log.info(KhataServiceHelper.createPayment(INSTAKART,DC,DC_Pendency,PaymentType.CASH,paymentReferenceId1).getStatus().getStatusMessage());
        Assert.assertEquals(paymentResponse3.getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");

        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreation(paymentId2,DC,INSTAKART,PaymentStatus.APPROVED.toString(),PaymentType.CASH,paymentReferenceId1,DC_Pendency,true);


        ExcessResponse excessResponse1 = khataHelper.searchExcessByPaymentId(paymentId);
        Assert.assertEquals(excessResponse1.getExcesses().get(0).getStatus(),PaymentStatus.PROCESSING);

        // Validate Shipment Pendency

        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(DC), "0.00", formatted_amount, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(SDA1_account_id), "0.00", formatted_amount, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(INSTAKART), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, MYNTRA.toString());


    }


    @Test(groups = "Excess")
    public void uploadMISfirstWithExcess_byTriggerAPI() throws Exception {


        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));

        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float currentGoodsPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getGoods();
        Float excess_amount_deposited = 1099.00f;
        Float amount_depostied_by_DC = currentAmountPendency+excess_amount_deposited;


        // Create Excess Payment from DC to CMS.It should allow to create.

        // Create Excess Payment from DC to CMS.It should allow to create.
        String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,amount_depostied_by_DC.toString());
        KhataServiceHelper.uploadMISReport(file_path);
        Long paymentId = KhataServiceHelper.paymentSearchByPaymentReferenceId(paymentReferenceId).getPayments().get(0).getId();
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreationByMIS(paymentId,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);
       // PaymentResponse paymentResponse = KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId);
        PaymentResponse paymentResponse = KhataServiceHelper.paymentUpdate(INSTAKART, DC, amount_depostied_by_DC, PaymentType.CASH, paymentId, PaymentStatus.APPROVED);
        Assert.assertEquals(paymentResponse.getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");
        Thread.sleep(3000);
        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,PaymentStatus.APPROVED.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);

        // Validate Excess entry is created
        ExcessResponse excessResponse = KhataServiceHelper.searchExcessByPaymentId(paymentId);
        Assert.assertEquals(excessResponse.getExcesses().get(0).getStatus(),PaymentStatus.CREATED,"Excess payment status is not matching");

        // Deliver the another order.

        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DCId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Thread.sleep(4000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount();
        DecimalFormat format = new DecimalFormat("0.00");
        DecimalFormat format1 = new DecimalFormat("0.0");
        String formatted_amount = format.format(amount);
        Float payment = amount.floatValue();
        String formatted_payment= format1.format(payment);

        // SDA pay amount.floatValue() to DC
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, payment, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);
        PaymentResponse paymentResponse1 =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        String payment_id_1 = paymentResponse1.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), formatted_payment, PaymentStatus.APPROVED.toString());

        ExcessResponse excessResponse2 = khataHelper.pushExcess(DC,INSTAKART);
        Thread.sleep(5000);
        ExcessResponse excessResponse1 = khataHelper.searchExcessByPaymentId(paymentId);
        Assert.assertEquals(excessResponse1.getExcesses().get(0).getStatus(),PaymentStatus.PROCESSING);

        System.out.println("Excess payment " + paymentId);

        // Validate Shipment Pendency
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(DC), "0.00", formatted_amount, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(SDA1_account_id), "0.00", formatted_amount, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNum, String.valueOf(INSTAKART), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, MYNTRA.toString());

    }


    @Test( groups = "Excess")
    public void uploadMISfirstandCMSupload_diffpaymentreferenceId() throws Exception {

        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));

        // Get current pendency on DC

        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float excess_amount_deposited = 1099.00f;
        Float amount_depostied_by_DC = currentAmountPendency+excess_amount_deposited;


        String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,amount_depostied_by_DC.toString());
        KhataServiceHelper.uploadMISReport(file_path);
        Long paymentId = KhataServiceHelper.paymentSearchByPaymentReferenceId(paymentReferenceId).getPayments().get(0).getId();
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreationByMIS(paymentId,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);

        String paymentReferenceId1= String.valueOf(random).substring(1, 8);

        PaymentResponse paymentResponse = KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId1);
        log.info(KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId1).getStatus().getStatusMessage());
        Assert.assertEquals(paymentResponse.getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");
        Long paymentId1= paymentResponse.getPayments().get(0).getId();
        Thread.sleep(3000);
        PaymentValidator.validatePaymentCreationByMIS(paymentId,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);
        PaymentValidator.validatePaymentCreation(paymentId1,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId1,amount_depostied_by_DC,true);

    }

    @Test( groups = "Excess")
    public void uploadMISfirstandCMSupload_diffammount() throws Exception {
        Long ownerRefId = Long.parseLong(KhataServiceHelper.getAccountbyId(Long.parseLong(DCId)).getAccounts().get(0).getOwnerReferenceId());
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));
        String paymentReferenceId= String.valueOf(random).substring(2, 9);
        log.info(KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER));

        // Get current pendency on DC
        Float currentAmountPendency = KhataServiceHelper.getAccountPendencyByOwnerRefIdandType(Long.parseLong(DCId),AccountType.DC,Long.parseLong("4019"),AccountType.PARTNER).getAccountPendency().get(0).getCash();
        Float excess_amount_deposited = 1099.00f;
        Float amount_depostied_by_DC = currentAmountPendency+excess_amount_deposited;
        Float wrong_amount = 1000.00f;


        String file_path = KhataServiceHelper.generateMISReport("ELC",paymentReferenceId,amount_depostied_by_DC.toString());
        KhataServiceHelper.uploadMISReport(file_path);
        Long paymentId = KhataServiceHelper.paymentSearchByPaymentReferenceId(paymentReferenceId).getPayments().get(0).getId();
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreationByMIS(paymentId,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);
        //upload the amount in 2nd time with same paymentReferenceId
        String upload_File_path = KhataServiceHelper.generateMISReport("ELC",paymentReferenceId, String.valueOf(amount_depostied_by_DC+120));
        KhataServiceHelper.uploadMISReport(upload_File_path);
        PaymentValidator.validatePaymentCreationByMIS(paymentId,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);

        /*
        PaymentResponse paymentResponse = KhataServiceHelper.createPayment(INSTAKART,DC,amount_depostied_by_DC,PaymentType.CASH,paymentReferenceId);
        log.info(KhataServiceHelper.createPayment(INSTAKART,DC,wrong_amount,PaymentType.CASH,paymentReferenceId).getStatus().getStatusMessage());
        Assert.assertEquals(paymentResponse.getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");
        Long paymentId1= paymentResponse.getPayments().get(0).getId();
        Thread.sleep(3000);
        // Todo

        PaymentValidator.validatePaymentCreationByMIS(paymentId,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);
        PaymentValidator.validatePaymentCreation(paymentId1,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,amount_depostied_by_DC,true);
*/
    }




    // SDA -> DL(Rs 1000) -> PAID 900  -> FD -> process till DLS. same SDA deliver another item pay 100 all balanced for sda and and after dls pendency should move to INSTAKART.
    @Test(groups = {"Excess", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", enabled = true)
    public void paymentStoretoSDA6() throws Exception {
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;


        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);


        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.HSR_ML, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String tracking_num1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount2 = format2.format(amount2);
        Thread.sleep(10000);

//validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);



        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DCId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(tracking_num, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(tracking_num1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Thread.sleep(4000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        float sdapayment = amount1.floatValue() + 200.00f;
        String formatted_sdapayment = format2.format(sdapayment);
        PaymentResponse SDApayment = khataHelper.createAndApprove(DC, SDA1_account_id, sdapayment, PaymentType.CASH);
        Assert.assertEquals(SDApayment.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "creation of payment for wrong DC and Correct SDA should not be allowed" + " DC and SDA Account ID:" + DC.toString() + " " + SDA1_account_id);

        // SDA pays amount to DC . (999.00f)

        float DCpayment = sdapayment - 300.00f;
        String formatted_DCpayment = format2.format(DCpayment);
        Random r = new Random(System.currentTimeMillis());
        int random = Math.abs(1000000000 + r.nextInt(2000000000));

        String paymentReferenceId= String.valueOf(random).substring(2, 9);

        String file_path = KhataServiceHelper.generateMISReport("HSR",paymentReferenceId,formatted_DCpayment.toString());
        KhataServiceHelper.uploadMISReport(file_path);
        Long paymentId = KhataServiceHelper.paymentSearchByPaymentReferenceId(paymentReferenceId).getPayments().get(0).getId();
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreationByMIS(paymentId,DC,INSTAKART,PaymentStatus.PENDING.toString(),PaymentType.CASH,paymentReferenceId,DCpayment,true);
        PaymentResponse paymentResponse = KhataServiceHelper.paymentUpdate(INSTAKART, DC, DCpayment, PaymentType.CASH, paymentId, PaymentStatus.APPROVED);
        Assert.assertEquals(paymentResponse.getStatus().getStatusType().toString(),"SUCCESS","Excess amount is allowing");
        Thread.sleep(3000);
        PaymentValidator.validatePaymentCreation(paymentId,DC,INSTAKART,PaymentStatus.APPROVED.toString(),PaymentType.CASH,paymentReferenceId,DCpayment,true);


        // deliver the second order

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Thread.sleep(5000);


        List<String> forwardTrackingNumbersList = new ArrayList<>();
        forwardTrackingNumbersList.add(tracking_num);
        forwardTrackingNumbersList.add(tracking_num1);

        lastmileOperations.completeTrip(forwardTrackingNumbersList);


   // mark DL order to FD by admin correction.

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.FD,AttemptReasonCode
                .INCOMPLETE_INCORRECT_ADDRESS.toString(), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        lmsKhataValidator.validateEventCreation(tracking_num, EnumSCM.FAILED_DELIVERY, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID, "Null", "true");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "true", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "true", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate Ledger Entries .
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);

        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", "Null", INSTAKART.toString());
//        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "true", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate Ledger Entries .
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);
//        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);



    }






}
