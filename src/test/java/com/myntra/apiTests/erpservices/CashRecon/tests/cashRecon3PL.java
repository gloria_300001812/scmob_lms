package com.myntra.apiTests.erpservices.CashRecon.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.apiTests.erpservices.khata.validator.KhataAccountValidator;
import com.myntra.apiTests.erpservices.khata.validator.LedgerValidator;
import com.myntra.apiTests.erpservices.khata.validator.PaymentValidator;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.service.LMSOrderDetailsClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.lms_khata.validator.LMSKhataValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.utility.RejoyServiceHelper;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;
import com.myntra.khata.response.PaymentResponse;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.response.TransporterResponse;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lms.khata.domain.EventType;
import com.myntra.lms.khata.domain.PaymentMode;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.DecimalFormat;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class cashRecon3PL {


    String env = getEnvironment();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();

    private static Logger log = Logger.getLogger(cashRecon3PL.class);
    KhataServiceHelper khataHelper = new KhataServiceHelper();
    Long INSTAKART, MYNTRA, DC , EKART , DELHIVERY;
    LMSKhataValidator lmsKhataValidator = new LMSKhataValidator();
    LMSOrderDetailsClient lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMSOrderDetailsClient_QA lmsOrderDetailsClient_qa=new LMSOrderDetailsClient_QA();



    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() throws IOException, JAXBException {
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_INSTAKART_ID), AccountType.PARTNER);
        INSTAKART = accountResponse1.getAccounts().get(0).getId();
        AccountResponse accountResponse2 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_MYNTRA_ID), AccountType.PARTNER);
        MYNTRA = accountResponse2.getAccounts().get(0).getId();
        AccountResponse accountResponse3 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EKART_ID), AccountType.PARTNER);
        EKART = accountResponse3.getAccounts().get(0).getId();
        AccountResponse accountResponse4 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_DELHIVERY_ID), AccountType.PARTNER);
        DELHIVERY = accountResponse4.getAccounts().get(0).getId();

    }


    @Test(groups = {"Smoke", "Regression"}, priority = 1, description = "Master Bag with one shipment", enabled = true)
    public void DLShipmentOfEK() throws Exception {

        Double cash = 0.0, goods = 0.0 , IKcash = 0.0 , IKgoods = 0.0 , EKcash = 0.0 , EKgoods = 0.0;

    //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

    // create Shipment

        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.IS, "560069", "EK", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        log.info("The value of shipment : "+amount);

        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);

        // validate event creation
        lmsKhataValidator.validateEventCreation(trackingNum, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,INSTAKART.toString(),"0.00","0.00",formatted_amount,"false","4019","Null");

        //validation pendency for IK to myntra
            //KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
            //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount )); // 2 orders amount plus existing 1

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);


        // Add to MB
        Long masterBagId = lmsServiceHelper.createMasterBag(18l,"HUB","Bangalore",1638l, "DC","Bangalore", EnumSCM.SUCCESS).getEntries().get(0)
                .getId();

        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, ShipmentType.DL), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close masterBag");


        //get current account pendency of EKART to Myntra


        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(EKART, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
            log.info("cash and goods :" + goods +"," +cash);
        }

        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            IKcash = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            IKgoods = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }

    //Ship MB

        lmsServiceHelper.shipMasterBag(masterBagId);

        Thread.sleep(5000);

        // Validate Event creation for SHIPPED

        lmsKhataValidator.validateEventCreation(trackingNum, com.myntra.lms.khata.domain.EventType.SHIPPED.toString(), INSTAKART.toString(), EKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        //Validate Shipment Pendency
        lmsKhataValidator.validateShipmentPendency(trackingNum,INSTAKART.toString(),"0.00","0.00","0.00","false","4019","Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,EKART.toString(),"0.00","0.00",formatted_amount,"false","4019","Null");


        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);

        //get current account pendency of IK to Myntra
            //khataAccountValidator.validateAccountPendency(Long.toString(EKART), Long.toString(INSTAKART), cash.toString(), String.valueOf(goods + amount ) ); // 2 orders amount plus existing 1
            //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), IKcash.toString(), String.valueOf(IKgoods - amount ) ); // 2 orders amount plus existing 1

        // OFD

        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.OUT_FOR_DELIVERY , "EK" ,ShipmentType.DL);

        AccountPendencyResponse accountPendencyResponse3 = khataHelper.getAccountPendencyByaccount(EKART,INSTAKART ); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            EKcash = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getCash().toString());
            EKgoods = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getGoods().toString());
            log.info("cash and goods :" + EKcash +"," +EKgoods);
        }

    // Deliver the 3pl Order

        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.DELIVERED , "EK" ,ShipmentType.DL);

        Thread.sleep(10000);
        lmsKhataValidator.validateEventCreation(trackingNum, EventType.DELIVERED.toString(), EKART.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(),formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,EKART.toString(),formatted_amount,"0.00","0.00","false","4019","Null");

        //khataAccountValidator.validateAccountPendency(Long.toString(EKART), Long.toString(INSTAKART), String.valueOf(EKcash+amount ),String.valueOf(EKgoods - amount )); // 2 orders amount plus existing 1
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
    }



    @Test(groups = {"Smoke", "Regression"}, priority = 1, description = "Master Bag with one shipment", enabled = true)
    public void ReceiveFDShipmentOfEKinWH() throws Exception {

        Double cash = 0.0, goods = 0.0 , IKcash = 0.0 , IKgoods = 0.0 , EKcash = 0.0 , EKgoods = 0.0;

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

    // create Shipment

        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.IS, "560069", "EK", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        log.info("The value of shipment : "+amount);


        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);

            //validation pendency for IK to myntra
                //KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
                //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount )); // 2 orders amount plus existing 1

        // validate event creation
            lmsKhataValidator.validateEventCreation(trackingNum, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
            lmsKhataValidator.validateShipmentPendency(trackingNum,INSTAKART.toString(),"0.00","0.00",formatted_amount,"false","4019","Null");

        //validation of ledger pendency
            LedgerValidator ledgerValidator = new LedgerValidator();
            ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);


        // Add to MB

        Long masterBagId = lmsServiceHelper.createMasterBag(18l,"HUB","Bangalore",1638l, "DC","Bangalore", EnumSCM.SUCCESS).getEntries().get(0)
                .getId();
        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, ShipmentType.DL), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close masterBag");


        //get current account pendency of EKART to Myntra


        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(EKART, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
            log.info("cash and goods :" + goods +"," +cash);
        }

        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            IKcash = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            IKgoods = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }

    //Ship MB

        lmsServiceHelper.shipMasterBag(masterBagId);

        Thread.sleep(5000);

        //Event Creation for SHIPPED
            lmsKhataValidator.validateEventCreation(trackingNum, com.myntra.lms.khata.domain.EventType.SHIPPED.toString(), INSTAKART.toString(), EKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        // Shipment pendency for SHIPPED

            lmsKhataValidator.validateShipmentPendency(trackingNum,INSTAKART.toString(),"0.00","0.00","0.00","false","4019","Null");
            lmsKhataValidator.validateShipmentPendency(trackingNum,EKART.toString(),"0.00","0.00",formatted_amount,"false","4019","Null");


        //validation pendency for IK to Ekart

            //khataAccountValidator.validateAccountPendency(Long.toString(EKART), Long.toString(INSTAKART), cash.toString(), String.valueOf(goods + amount ) ); // 2 orders amount plus existing 1
            ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);

        //get current account pendency of IK to Myntra

            //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), IKcash.toString(), String.valueOf(IKgoods - amount ) ); // 2 orders amount plus existing 1
            ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);

        // OFD

            lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.OUT_FOR_DELIVERY , "EK" ,ShipmentType.DL);
//
//        AccountPendencyResponse accountPendencyResponse3 = khataHelper.getAccountPendencyByaccount(INSTAKART, EKART); //to get current pendency
//        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
//            EKcash = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getCash().toString());
//            EKgoods = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getGoods().toString());
//            log.info("cash and goods :" + EKcash +"," +EKgoods);
//        }

        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.FAILED_DELIVERY , "EK" ,ShipmentType.DL);
        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.RTO_CONFIRMED , "EK" ,ShipmentType.DL);
        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.RTO_DISPATCHED , "EK" ,ShipmentType.DL);


     //Receive FD order in Warehouse
        RejoyServiceHelper.recieveShipmentInRejoy(packetId);


        Thread.sleep(10000);
        lmsKhataValidator.validateEventCreation(trackingNum,"RECEIVE_IN_RETURNS_HUB", EKART.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(),formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);

        lmsKhataValidator.validateEventCreation(trackingNum, EventType.DELIVERED_TO_SELLER.toString(), INSTAKART.toString(), MYNTRA.toString(), "true", PaymentMode.CASH.toString(),formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,EKART.toString(),"0.00","0.00","0.00","false","4019","Null");

        //  khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(EKcash+amount ),String.valueOf(EKgoods - amount )); // 2 orders amount plus existing 1

    }


    @Test(dataProviderClass = LMSTestsDP.class, dataProvider = "RegionalHandOver")

    public void RegionalHandover_FDShipmentOfDEinWH(String pincode, String courier, String shippingMethod , Long originPremisesId , Long destPremisesId , String source , String destination , Long courierDC ) throws Exception {
        LedgerValidator ledgerValidator = new LedgerValidator();

        // Create tracking number for DE .

        lmsReturnHelper.insertTrackingNumberRHD("DE-COD");

        // Create an order with PK with DE courier

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
                false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.PACKED),"Order is not moved to PACKED status , Check whether pincode have serviceability , lms and lms3pl is up  ");
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        log.info("The value of shipment : "+amount);

        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);

        // Validate Event Creation , Shipment Pendency and Ledger .
        Thread.sleep(3000);
        lmsKhataValidator.validateEventCreation(trackingNum, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,INSTAKART.toString(),"0.00","0.00",formatted_amount,"false","4019","Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);


        // Inscan that order at DH-BLR

        if (!courier.equals("ML"))
            DBUtilities
                    .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                            + packetId + "'", "lms");
        Assert.assertEquals(lmsServiceHelper.orderInScanGOR(omsServiceHelper.getPacketId(orderId), "36").getStatus()
                .getStatusType().toString(), EnumSCM.SUCCESS);
        String hubCode = lmsServiceHelper.getHubConfig("36", "DL");
        lmsHelper.inscanValidation(omsServiceHelper.getPacketId(orderId), hubCode);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.INSCANNED),"Order is not moved to INSCANNED status , Check whether pincode have serviceability , lms and lms3pl is up  ");


        // Create a Masterbag from DH-BLR to BHU ( Myntra Regional Handover Bhubaneswar )


        Long masterBagId = masterBagServiceHelper.createMasterBag(originPremisesId , destPremisesId, ShippingMethod.valueOf(shippingMethod), "ML" ).getId();


        // Add Shipment to Masterbag.

        lmsServiceHelper.addAndSaveMasterBag("" + packetId, "" + masterBagId, ShipmentType.DL);
        System.out.println("\nShipment is added to Masterbag :: " +masterBagId);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.ADDED_TO_MB),"Order is not moved to ADDED_TO_MB status  ");
        // Close MasterBag.

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");


        // Receive Masterbag at TH-BLR transport hub

        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(source, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");

        // Create Container from TH-BLR to BHU.

        long laneId = ((LaneResponse)tmsServiceHelper.getSupportedLanes.apply(source, destination)).getLaneEntries().get(1).getId();
        long transporterId = ((TransporterResponse)tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        ContainerResponse containerResponse = (ContainerResponse)tmsServiceHelper.createContainer.apply(source, destination, laneId, transporterId);
        long containerId = containerResponse.getContainerEntries().get(0).getId();

        // Add Masterbag to Container.

        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Error occurred during state transition");
        System.out.println("\nMasterbag "+ masterBagId+" is added to Container :: " +containerId);
        // Ship the Container.

        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "CONTAINER SHIP IS FAILING :: " +containerId);
        Thread.sleep(5000);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.SHIPPED),"Order is not moved to SHIPPED status  ");

        // Receive the Masterbag at BHU transport hub.

        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destination, masterBagId, containerId);


        // Masterbag Inscan at BHU DC.

        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.IN_TRANSIT, "Bangalore", destPremisesId, "DC");
        ExceptionHandler.handleEquals(lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2_RHD(masterBagId,trackingNum,"Bangalore",destPremisesId , ShipmentType.DL ,PremisesType.DC).
                getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        lmsServiceHelper.masterBagInScanUpdateNew(masterBagId, packetId, "Bangalore","Myntra BHU", destPremisesId, "DC",originPremisesId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT);
        ExceptionHandler.handleEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId, packetId, "Bangalore",destPremisesId
                , "DC", originPremisesId).
                getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive master bag in DC");
        ExceptionHandler.handleEquals(lmsHelper.getMasterBagStatus(masterBagId),EnumSCM.RECEIVED_AT_HANDOVER_CENTER,
                "Masterbag status is not update in DB to `RECEIVED`");


        // Create a MasterBag from BHU to Delhivery (DE) with courier DE.

        Long masterBagId1 = masterBagServiceHelper.createMasterBag(destPremisesId , courierDC, ShippingMethod.valueOf(shippingMethod), courier).getId();

        // Add Shipment to Masterbag.

        lmsServiceHelper.addAndSaveMasterBag("" + packetId, "" + masterBagId1, ShipmentType.DL);
        System.out.println("\nShipment is added to Masterbag :: " +masterBagId1);

        // Close Masterbag.

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId1).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag ::" + masterBagId1 );
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER),"Order is not moved to RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER status , Check whether FG is enabled . ( Should lms.enable_rhc_v2 = TRUE )");


        // Ship the Masterbag at Delhivery (DE)

        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId1).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Shipping masterbag is failed ::" +masterBagId1);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.SHIPPED),"Order is not moved to SHIPPED status , Check whether FG is enabled . ( Should lms.enable_rhc_v2 = TRUE )");
        System.out.println("Tracking Number With Courier DE :: " + trackingNum);


//     Validate Event Creation , Shipment Pendency and Ledger .

        Thread.sleep(3000);
        lmsKhataValidator.validateEventCreation(trackingNum, EventType.SHIPPED.toString(), INSTAKART.toString(), DELHIVERY.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,INSTAKART.toString(),"0.00","0.00","0.00","false","4019","Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,DELHIVERY.toString(),"0.00","0.00",formatted_amount,"false","4019","Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DELHIVERY), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);


        // OFD

        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.OUT_FOR_DELIVERY , courier ,ShipmentType.DL);
        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.FAILED_DELIVERY , courier ,ShipmentType.DL);
        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.RTO_CONFIRMED , courier ,ShipmentType.DL);
        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.RTO_DISPATCHED , courier ,ShipmentType.DL);


        //Receive FD order in Warehouse
        RejoyServiceHelper.recieveShipmentInRejoy(packetId);


        Thread.sleep(10000);
        lmsKhataValidator.validateEventCreation(trackingNum,"RECEIVE_IN_RETURNS_HUB", DELHIVERY.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(),formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNum, EventType.DELIVERED_TO_SELLER.toString(), INSTAKART.toString(), MYNTRA.toString(), "true", PaymentMode.CASH.toString(),formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,DELHIVERY.toString(),"0.00","0.00","0.00","false","4019","Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,INSTAKART.toString(),"0.00","0.00","0.00","false","4019","Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DELHIVERY), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);

    }


    @Test(dataProviderClass = LMSTestsDP.class, dataProvider = "RegionalHandOver")

    public void RegionalHandover_DL(String pincode, String courier, String shippingMethod , Long originPremisesId , Long destPremisesId , String source , String destination , Long courierDC) throws Exception {
        LedgerValidator ledgerValidator = new LedgerValidator();

        // Create tracking number for DE .

        lmsReturnHelper.insertTrackingNumberRHD("DE-COD");

        // Create an order with PK with DE courier

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, pincode, courier, "36", shippingMethod, "cod", false,
                false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNum = lmsHelper.getTrackingNumber(packetId);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.PACKED),"Order is not moved to PACKED status , Check whether pincode have serviceability , lms and lms3pl is up  ");
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        log.info("The value of shipment : "+amount);

        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);

        // Validate Event Creation , Shipment Pendency and Ledger .
        Thread.sleep(3000);
        lmsKhataValidator.validateEventCreation(trackingNum, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,INSTAKART.toString(),"0.00","0.00",formatted_amount,"false","4019","Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);


        // Inscan that order at DH-BLR

        if (!courier.equals("ML"))
            DBUtilities
                    .exUpdateQuery("update order_tracking set courier_creation_status = 'ACCEPTED' where order_id = '"
                            + packetId + "'", "lms");
        Assert.assertEquals(lmsServiceHelper.orderInScanGOR(omsServiceHelper.getPacketId(orderId), "36").getStatus()
                .getStatusType().toString(), EnumSCM.SUCCESS);
        String hubCode = lmsServiceHelper.getHubConfig("36", "DL");
        lmsHelper.inscanValidation(omsServiceHelper.getPacketId(orderId), hubCode);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.INSCANNED),"Order is not moved to INSCANNED status , Check whether pincode have serviceability , lms and lms3pl is up  ");


        // Create a Masterbag from DH-BLR to BHU ( Myntra Regional Handover Bhubaneswar )


        Long masterBagId = masterBagServiceHelper.createMasterBag(originPremisesId , destPremisesId, ShippingMethod.valueOf(shippingMethod), "ML" ).getId();


        // Add Shipment to Masterbag.

        lmsServiceHelper.addAndSaveMasterBag("" + packetId, "" + masterBagId, ShipmentType.DL);
        System.out.println("\nShipment is added to Masterbag :: " +masterBagId);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.ADDED_TO_MB),"Order is not moved to ADDED_TO_MB status  ");
        // Close MasterBag.

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");


        // Receive Masterbag at TH-BLR transport hub

        Assert.assertEquals(((TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(source, masterBagId)).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Unable to receive masterBag in TMS HUB");

        // Create Container from TH-BLR to BHU.

        long laneId = ((LaneResponse)tmsServiceHelper.getSupportedLanes.apply(source, destination)).getLaneEntries().get(1).getId();
        long transporterId = ((TransporterResponse)tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        ContainerResponse containerResponse = (ContainerResponse)tmsServiceHelper.createContainer.apply(source, destination, laneId, transporterId);
        long containerId = containerResponse.getContainerEntries().get(0).getId();

        // Add Masterbag to Container.

        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Error occurred during state transition");
        System.out.println("\nMasterbag "+ masterBagId+" is added to Container :: " +containerId);
        // Ship the Container.

        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "CONTAINER SHIP IS FAILING :: " +containerId);
        Thread.sleep(5000);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.SHIPPED),"Order is not moved to SHIPPED status  ");

        // Receive the Masterbag at BHU transport hub.

        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destination, masterBagId, containerId);


        // Masterbag Inscan at BHU DC.

        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.IN_TRANSIT, "Bangalore", destPremisesId, "DC");
        ExceptionHandler.handleEquals(lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2_RHD(masterBagId,trackingNum,"Bangalore",destPremisesId , ShipmentType.DL ,PremisesType.DC).
                getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        lmsServiceHelper.masterBagInScanUpdateNew(masterBagId, packetId, "Bangalore","Myntra BHU", destPremisesId, "DC",originPremisesId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT);
        ExceptionHandler.handleEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId, packetId, "Bangalore",destPremisesId
                , "DC", originPremisesId).
                getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive master bag in DC");
        ExceptionHandler.handleEquals(lmsHelper.getMasterBagStatus(masterBagId),EnumSCM.RECEIVED_AT_HANDOVER_CENTER,
                "Masterbag status is not update in DB to `RECEIVED`");


        // Create a MasterBag from BHU to Delhivery (DE) with courier DE.

        Long masterBagId1 = masterBagServiceHelper.createMasterBag(destPremisesId , courierDC, ShippingMethod.valueOf(shippingMethod), courier).getId();

        // Add Shipment to Masterbag.

        lmsServiceHelper.addAndSaveMasterBag("" + packetId, "" + masterBagId1, ShipmentType.DL);
        System.out.println("\nShipment is added to Masterbag :: " +masterBagId1);

        // Close Masterbag.

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId1).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag ::" + masterBagId1 );
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER),"Order is not moved to RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER status , Check whether FG is enabled . ( Should lms.enable_rhc_v2 = TRUE )");


        // Ship the Masterbag at Delhivery (DE)

        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId1).getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Shipping masterbag is failed ::" +masterBagId1);
        Assert.assertTrue(lmsOrderDetailsClient_qa.getLMSOrderDetails(packetId,LMS_CONSTANTS.TENANTID,LMS_CONSTANTS.CLIENTID).getOrders().get(0).getPlatformShipmentStatus().equals(ShipmentStatus.SHIPPED),"Order is not moved to SHIPPED status , Check whether FG is enabled . ( Should lms.enable_rhc_v2 = TRUE )");
        System.out.println("Tracking Number With Courier DE :: " + trackingNum);


//     Validate Event Creation , Shipment Pendency and Ledger .

        Thread.sleep(3000);
        lmsKhataValidator.validateEventCreation(trackingNum, EventType.SHIPPED.toString(), INSTAKART.toString(), DELHIVERY.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,INSTAKART.toString(),"0.00","0.00","0.00","false","4019","Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,DELHIVERY.toString(),"0.00","0.00",formatted_amount,"false","4019","Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DELHIVERY), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);


        // OFD and Deliver the Order.

        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.OUT_FOR_DELIVERY , "DE" ,ShipmentType.DL);
        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.DELIVERED , "DE" ,ShipmentType.DL);


        Thread.sleep(10000);
        lmsKhataValidator.validateEventCreation(trackingNum,EventType.DELIVERED.toString(), DELHIVERY.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(),formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,DELHIVERY.toString(),formatted_amount,"0.00","0.00","false","4019","Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DELHIVERY), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DELHIVERY), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNum);

    }

    @Test(groups = {"Smoke", "Regression"}, priority = 1, description = "Master Bag with one shipment", enabled = false)
    public void CouriertoCMSPayment() throws Exception {

        Double cash = 0.0, goods = 0.0 , IKcash = 0.0 , IKgoods = 0.0 , EKcash = 0.0 , EKgoods = 0.0;

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

        // create Shipment

        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.IS, "560069", "EK", "36", EnumSCM.NORMAL, "cod", false, true));
        String trackingNum = lmsHelper.getTrackingNumber(packetId);

        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        log.info("The value of shipment : "+amount);

        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);

        // validate event creation
        lmsKhataValidator.validateEventCreation(trackingNum, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,INSTAKART.toString(),"0.00","0.00",formatted_amount,"false","4019","Null");

        //validation pendency for IK to myntra
        //KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount )); // 2 orders amount plus existing 1

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);


        // Add to MB
        Long masterBagId = lmsServiceHelper.createMasterBag(18l,"HUB","Bangalore",1638l, "DC","Bangalore", EnumSCM.SUCCESS).getEntries().get(0)
                .getId();

        Assert.assertEquals(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, ShipmentType.DL), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close masterBag");


        //get current account pendency of EKART to Myntra


        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(EKART, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
            log.info("cash and goods :" + goods +"," +cash);
        }

        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            IKcash = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            IKgoods = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }

        //Ship MB

        lmsServiceHelper.shipMasterBag(masterBagId);

        Thread.sleep(5000);

        // Validate Event creation for SHIPPED

        lmsKhataValidator.validateEventCreation(trackingNum, com.myntra.lms.khata.domain.EventType.SHIPPED.toString(), INSTAKART.toString(), EKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        //Validate Shipment Pendency
        lmsKhataValidator.validateShipmentPendency(trackingNum,INSTAKART.toString(),"0.00","0.00","0.00","false","4019","Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,EKART.toString(),"0.00","0.00",formatted_amount,"false","4019","Null");


        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);

        //get current account pendency of IK to Myntra
        //khataAccountValidator.validateAccountPendency(Long.toString(EKART), Long.toString(INSTAKART), cash.toString(), String.valueOf(goods + amount ) ); // 2 orders amount plus existing 1
        //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), IKcash.toString(), String.valueOf(IKgoods - amount ) ); // 2 orders amount plus existing 1

        // OFD

        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.OUT_FOR_DELIVERY , "EK" ,ShipmentType.DL);

        AccountPendencyResponse accountPendencyResponse3 = khataHelper.getAccountPendencyByaccount(EKART,INSTAKART ); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            EKcash = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getCash().toString());
            EKgoods = Double.parseDouble(accountPendencyResponse3.getAccountPendency().get(0).getGoods().toString());
            log.info("cash and goods :" + EKcash +"," +EKgoods);
        }

        // Deliver the 3pl Order

        lmsServiceHelper.updateShipmentfor3PL(trackingNum , ShipmentUpdateEvent.DELIVERED , "EK" ,ShipmentType.DL);

        Thread.sleep(10000);
        lmsKhataValidator.validateEventCreation(trackingNum, EventType.DELIVERED.toString(), EKART.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(),formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum,EKART.toString(),formatted_amount,"0.00","0.00","false","4019","Null");

        //khataAccountValidator.validateAccountPendency(Long.toString(EKART), Long.toString(INSTAKART), String.valueOf(EKcash+amount ),String.valueOf(EKgoods - amount )); // 2 orders amount plus existing 1
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNum);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(EKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNum);

        // 3pl to CMS upload

        String file_path = khataHelper.generateMISReport3Pl("EK",trackingNum,amount.floatValue());
        khataHelper.upload3pltoCMS(file_path);
        Thread.sleep(2000);

        String paymentReferenceId = khataHelper.getPaymentDetailsFromAccountToAccount(EKART.toString(),INSTAKART.toString()).getPayments().get(0).getPaymentReferenceId();
        Long paymentId = khataHelper.getPaymentDetailsFromAccountToAccount(EKART.toString(),INSTAKART.toString()).getPayments().get(0).getId();

        // upload MIS upload.

        String file_path1 = KhataServiceHelper.generateMISReport("EK",paymentReferenceId,amount.toString());
        KhataServiceHelper.uploadMISReport(file_path1);
        Thread.sleep(5000);
        PaymentValidator.validatePaymentCreationByMIS(paymentId,DC,INSTAKART,PaymentStatus.APPROVED.toString(),PaymentType.CASH,paymentReferenceId,amount.floatValue(),true);




    }



}
