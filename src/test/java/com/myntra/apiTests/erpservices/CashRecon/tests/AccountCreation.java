package com.myntra.apiTests.erpservices.CashRecon.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.apiTests.erpservices.khata.validator.KhataAccountValidator;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.dp.Lastmile_StoreFlowsDP;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.DeliveryCenterClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.DeliveryStaffClient_QA;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_CreateOrder;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.LMSOrderDetailsClient;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;
import com.myntra.khata.response.PaymentResponse;
import com.myntra.lastmile.client.code.utils.*;
import com.myntra.lastmile.client.entry.DeliveryCenterEntry;
import com.myntra.apiTests.erpservices.CashRecon.dp.dataProvider;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.response.DeliveryCenterResponse;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lms.client.response.CourierResponse;
import com.myntra.lms.client.status.CourierType;
import com.myntra.lms.client.status.TrackingNoSource;
import com.myntra.lms.client.types.ManifestType;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.Random;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class AccountCreation {

    static Logger log = Logger.getLogger(AccountCreation.class);
    StoreClient storeClient;
    DeliveryCenterClient deliveryCenterClient;

    LMSOrderDetailsClient lmsOrderDetailsClient;
    MasterBagClient masterBagClient;
    TripOrderAssignmentClient tripOrderAssignmentClient;

    MLShipmentClientV2 mlShipmentServiceV2Client;
    DeliveryStaffClient deliveryStaffClient;
    private LMSHelper lmsHepler = new LMSHelper();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private LastmileHelper lastmileHelper;
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa=new DeliveryStaffClient_QA();

    @BeforeSuite
    public void setEnv() {
        String env = getEnvironment();
        storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        deliveryCenterClient = new DeliveryCenterClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        deliveryStaffClient = new DeliveryStaffClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        lmsOrderDetailsClient = new LMSOrderDetailsClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
        tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    }


    public DeliveryCenterResponse createDC(String code, String tmsHubCode, String pincode, String tenantId, Boolean isSelfShipSupporte, Boolean isStrictServiceable, Boolean isCardEnabled, Boolean isActive, String courierCode, ReconType reconType, String contactNumber, Integer masterbagCapacity) {
        DeliveryCenterEntry deliveryCenterEntry = new DeliveryCenterEntry();
        deliveryCenterEntry.setCode(code);
        deliveryCenterEntry.setName(code + "Auto");
        deliveryCenterEntry.setManager("Automation");
        deliveryCenterEntry.setAddress("Automation");
        deliveryCenterEntry.setCity("Bangalore");
        deliveryCenterEntry.setTmsHubCode(tmsHubCode);
        deliveryCenterEntry.setCityCode("Bangalore");
        deliveryCenterEntry.setState("Karnataka");
        deliveryCenterEntry.setPincode(pincode);
        deliveryCenterEntry.setTenantId(tenantId);
        deliveryCenterEntry.setSelfShipSupported(isSelfShipSupporte);
        deliveryCenterEntry.setIsStrictServiceable(isStrictServiceable);
        deliveryCenterEntry.setIsCardEnabled(isCardEnabled);
        deliveryCenterEntry.setActive(isActive);
        deliveryCenterEntry.setCourierCode(courierCode);
        deliveryCenterEntry.setReconType(reconType);
        deliveryCenterEntry.setContactNumber(contactNumber);
        deliveryCenterEntry.setMasterbagCapacity(masterbagCapacity);
        deliveryCenterEntry.setType(DeliveryCenterType.ML);
        DeliveryCenterResponse deliveryCenterResponse = deliveryCenterClient_qa.createDeliveryCenter(deliveryCenterEntry);
        return deliveryCenterResponse;
    }

    public DeliveryCenterResponse updateDC(Long DCId, String code, String tmsHubCode, String pincode, String tenantId, Boolean isSelfShipSupporte, Boolean isStrictServiceable, Boolean isCardEnabled, Boolean isActive, String courierCode, ReconType reconType, String contactNumber, Integer masterbagCapacity) {
        DeliveryCenterEntry deliveryCenterEntry = new DeliveryCenterEntry();
        deliveryCenterEntry.setCode(code);
        deliveryCenterEntry.setName(code + "Auto");
        deliveryCenterEntry.setManager("Automation");
        deliveryCenterEntry.setAddress("Automation");
        deliveryCenterEntry.setCity("Bangalore");
        deliveryCenterEntry.setTmsHubCode(tmsHubCode);
        deliveryCenterEntry.setCityCode("Bangalore");
        deliveryCenterEntry.setState("Karnataka");
        deliveryCenterEntry.setPincode(pincode);
        deliveryCenterEntry.setTenantId(tenantId);
        deliveryCenterEntry.setSelfShipSupported(isSelfShipSupporte);
        deliveryCenterEntry.setIsStrictServiceable(isStrictServiceable);
        deliveryCenterEntry.setIsCardEnabled(isCardEnabled);
        deliveryCenterEntry.setActive(isActive);
        deliveryCenterEntry.setCourierCode(courierCode);
        deliveryCenterEntry.setReconType(reconType);
        deliveryCenterEntry.setContactNumber(contactNumber);
        deliveryCenterEntry.setMasterbagCapacity(masterbagCapacity);
        deliveryCenterEntry.setType(DeliveryCenterType.ML);
        DeliveryCenterResponse deliveryCenterResponse = deliveryCenterClient_qa.updateDeliveryCenter(deliveryCenterEntry, DCId);
        return deliveryCenterResponse;
    }


    public DeliveryStaffResponse createDeliveryStaff(String code, Long DcId, String ContactNumber, String empCode, DeliveryStaffCommute deliveryStaffCommute, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, DeliveryStaffType deliveryStaffType) throws Exception {
        DeliveryStaffEntry deliveryStaffEntry = new DeliveryStaffEntry();
        deliveryStaffEntry.setCode(code);
        deliveryStaffEntry.setFirstName(code + "SDA");
        deliveryStaffEntry.setLastName("Automation");
        deliveryStaffEntry.setDeliveryCenterId(DcId);
        deliveryStaffEntry.setMobile(ContactNumber);
        deliveryStaffEntry.setEmpCode(empCode);
        deliveryStaffEntry.setModeOfCommute(deliveryStaffCommute);
        deliveryStaffEntry.setTenantId(tenantId);
        deliveryStaffEntry.setAvailable(isAvailable);
        deliveryStaffEntry.setDeleted(isDeleted);
        deliveryStaffEntry.setIsCardEnabled(isCardEnabled);
        deliveryStaffEntry.setDeliveryStaffType(deliveryStaffType);
        DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient_qa.create(deliveryStaffEntry);
        return deliveryStaffResponse;
    }

    public DeliveryStaffResponse updateDeliveryStaff(Long SDAId, String code, Long DcId, String ContactNumber, String empCode, DeliveryStaffCommute deliveryStaffCommute, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, DeliveryStaffType deliveryStaffType , DeliveryStaffRole deliveryStaffRole) throws Exception {
        DeliveryStaffEntry deliveryStaffEntry = new DeliveryStaffEntry();
        deliveryStaffEntry.setCode(code);
        deliveryStaffEntry.setFirstName(code + "SDA");
        deliveryStaffEntry.setLastName("Automation");
        deliveryStaffEntry.setDeliveryCenterId(DcId);
        deliveryStaffEntry.setMobile(ContactNumber);
        deliveryStaffEntry.setEmpCode(empCode);
        deliveryStaffEntry.setModeOfCommute(deliveryStaffCommute);
        deliveryStaffEntry.setTenantId(tenantId);
        deliveryStaffEntry.setAvailable(isAvailable);
        deliveryStaffEntry.setDeleted(isDeleted);
        deliveryStaffEntry.setIsCardEnabled(isCardEnabled);
        deliveryStaffEntry.setDeliveryStaffType(deliveryStaffType);
        deliveryStaffEntry.setDeliveryStaffRole(deliveryStaffRole);
        DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient_qa.update(deliveryStaffEntry, SDAId);
        return deliveryStaffResponse;
    }


    @Test(dataProviderClass = dataProvider.class, dataProvider = "createDC")
    public void CreateAndUpdateDC(String code, String tmsHubCode, String pincode, String tenantId, Boolean isSelfShipSupporte, Boolean isStrictServiceable, Boolean isCardEnabled, Boolean isActive, String courierCode, ReconType reconType, String contactNumber, Integer masterbagCapacity) throws Exception {
        // create DC and validate the Account creation

        DeliveryCenterResponse deliveryCenterResponse = createDC(code, tmsHubCode, pincode, tenantId, isSelfShipSupporte, isStrictServiceable, isCardEnabled, isActive, courierCode, reconType, contactNumber, masterbagCapacity);
        Long ownerReferenceId = deliveryCenterResponse.getDeliveryCenters().get(0).getId();
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code + "Auto", contactNumber, 1l, AccountType.DC, true);
        AccountResponse accountResponse = KhataServiceHelper.searchAccountByOwnerRefId(ownerReferenceId);
        Long associateAccountId = accountResponse.getAccounts().get(0).getId();
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId, 1l);


        // update DC mobile number , recon type , is active , pincode and validate the Account creation
        Random r = new Random(System.currentTimeMillis());
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        String newContactNumber = String.valueOf(contactNumber1).substring(3, 9);
        DeliveryCenterResponse deliveryCenterResponse1 = updateDC(ownerReferenceId, code, tmsHubCode, pincode, tenantId, isSelfShipSupporte, isStrictServiceable, isCardEnabled, false, courierCode, ReconType.EOD_RECON, newContactNumber, masterbagCapacity);
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code + "Auto", newContactNumber, 1l, AccountType.DC, false);
        AccountResponse accountResponse1 = KhataServiceHelper.searchAccountByOwnerRefId(ownerReferenceId);
        Long associateAccountId1 = accountResponse1.getAccounts().get(0).getId();
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId1, 1l);
    }

    @Test(dataProviderClass = dataProvider.class, dataProvider = "createSDA")
    public void CreateAndUpdateSDA(String code, Long DcId, String ContactNumber, String empCode, DeliveryStaffCommute deliveryStaffCommute, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, DeliveryStaffType deliveryStaffType) throws Exception {
        // create DC and validate the Account creation

        DeliveryStaffResponse deliveryStaffResponse = createDeliveryStaff(code, DcId, ContactNumber, empCode, deliveryStaffCommute, tenantId, isAvailable, isDeleted, isCardEnabled, deliveryStaffType);
        Long ownerReferenceId = deliveryStaffResponse.getDeliveryStaffs().get(0).getId();
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code + "SDA Automation", ContactNumber, 1l, AccountType.SDA, true);
        AccountResponse accountResponse = KhataServiceHelper.searchAccountByOwnerRefId(ownerReferenceId);
        Long associateAccountId = accountResponse.getAccounts().get(0).getId();
        Long parentAccountId = KhataServiceHelper.getAccountbyownerRefernceandType(DcId, AccountType.DC).getAccounts().get(0).getId();
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId, parentAccountId);

        // update DC mobile number , recon type , is active , pincode and validate the Account creation
        Random r = new Random(System.currentTimeMillis());
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        String newContactNumber = String.valueOf(contactNumber1).substring(3, 9);
        DeliveryStaffResponse deliveryStaffResponse1 = updateDeliveryStaff(ownerReferenceId, code, DcId, newContactNumber, empCode, DeliveryStaffCommute.DELIVERY_VAN, tenantId, false, false, isCardEnabled, DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffRole.STAFF);
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code + "SDA Automation", newContactNumber, 1l, AccountType.SDA, false);
        AccountResponse accountResponse1 = KhataServiceHelper.searchAccountByOwnerRefId(ownerReferenceId);
        Long associateAccountId1 = accountResponse1.getAccounts().get(0).getId();
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId1, parentAccountId);
    }

    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void CreateAndUpdateStore(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                     String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        // create Store  and validate the Account creation for Store and SDA

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long ownerReferenceId = Long.parseLong(storeResponse.getStoreEntries().get(0).getTenantId());
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        Long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        AccountResponse accountResponse = KhataServiceHelper.searchAccountByOwnerRefId(storeDeliveryStaffId);
        Long associateAccountId = accountResponse.getAccounts().get(0).getId();
        Long parentAccountId = KhataServiceHelper.getAccountbyownerRefernceandType(ownerReferenceId, AccountType.PARTNER).getAccounts().get(0).getId();
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code, mobileNumber, null, AccountType.PARTNER, true);
        KhataAccountValidator.validateAccountCreation(storeDeliveryStaffId, code, code + " " + code, mobileNumber, parentAccountId, AccountType.SDA, true);
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId, parentAccountId);

        // upadte Store

        StoreResponse storeResponse1 = lastmileHelper.updateStore(name, address, pincode, city, state, ownerReferenceId.toString(),
                false, true, isCardEnabled, code, hlpReconType, storeHlPId);
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code, mobileNumber, null, AccountType.PARTNER, false);
        KhataAccountValidator.validateAccountCreation(storeDeliveryStaffId, code, code + " " + code, mobileNumber, parentAccountId, AccountType.SDA, true);
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId, parentAccountId);
    }

    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void CreateAndUpdateStoreSDA(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                        String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        // create Store  and validate the Account creation for Store and SDA

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long ownerReferenceId = Long.parseLong(storeResponse.getStoreEntries().get(0).getTenantId());
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        Long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        AccountResponse accountResponse = KhataServiceHelper.searchAccountByOwnerRefId(storeDeliveryStaffId);
        Long associateAccountId = accountResponse.getAccounts().get(0).getId();
        Long parentAccountId = KhataServiceHelper.getAccountbyownerRefernceandType(ownerReferenceId, AccountType.PARTNER).getAccounts().get(0).getId();
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code, mobileNumber, null, AccountType.PARTNER, true);
        KhataAccountValidator.validateAccountCreation(storeDeliveryStaffId, code, code + " " + code, mobileNumber, parentAccountId, AccountType.SDA, true);
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId, parentAccountId);

        // upadte StoreSDA

        StoreResponse storeResponse1 = lastmileHelper.updateStore(name, address, pincode, city, state, ownerReferenceId.toString(),
                false, false, isCardEnabled, code, hlpReconType, storeHlPId);
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code, mobileNumber, null, AccountType.PARTNER, false);
        KhataAccountValidator.validateAccountCreation(storeDeliveryStaffId, code, code + " " + code, mobileNumber, parentAccountId, AccountType.SDA, true);
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId, parentAccountId);

    }


    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllFlows")
    public void CreateAndUpdateStoreMultipleSDA(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        // create Store  and validate the Account creation for Store and SDA

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long ownerReferenceId = Long.parseLong(storeResponse.getStoreEntries().get(0).getTenantId());
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        Long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        AccountResponse accountResponse = KhataServiceHelper.searchAccountByOwnerRefId(storeDeliveryStaffId);
        Long associateAccountId = accountResponse.getAccounts().get(0).getId();
        Long parentAccountId = KhataServiceHelper.getAccountbyownerRefernceandType(ownerReferenceId, AccountType.PARTNER).getAccounts().get(0).getId();
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code, mobileNumber, null, AccountType.PARTNER, true);
        KhataAccountValidator.validateAccountCreation(storeDeliveryStaffId, code, code + " " + code, mobileNumber, parentAccountId, AccountType.SDA, true);
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId, parentAccountId);

        // upadte Store

        StoreResponse storeResponse1 = lastmileHelper.updateStore(name, address, pincode, city, state, ownerReferenceId.toString(),
                false, true, isCardEnabled, code, hlpReconType, storeHlPId);
//        KhataAccountValidator.validateAccountCreation(ownerReferenceId,code,code ,mobileNumber,null,AccountType.PARTNER,false);
//        KhataAccountValidator.validateAccountCreation(storeDeliveryStaffId,code,code+" "+code ,mobileNumber,parentAccountId,AccountType.SDA,false);
//        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId,parentAccountId);

        // multiple SDA
        Random r = new Random(System.currentTimeMillis());

        for (int o = 0; o < 2; o++) {
            int SDAcontactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
            lastmileHelper.createStoreDeliveryStaff(storeHlPId, ownerReferenceId.toString(), SDAcontactNumber, DeliveryStaffType.MYNTRA_PAYROLL, DeliveryStaffCommute.BIKER);
            long storeDeliveryStaffId1 = deliveryStaffClient_qa.findDeliveryStaffByMobNo("" + SDAcontactNumber).getDeliveryStaffs().get(0).getId();
            lastmileHelper.validateDeliveryStaffCreation(storeDeliveryStaffId1, storeHlPId, ownerReferenceId.toString(), "STAFF");
            AccountResponse accountResponse1 = KhataServiceHelper.searchAccountByOwnerRefId(storeDeliveryStaffId1);
            Long associateAccountId1 = accountResponse1.getAccounts().get(0).getId();
            KhataAccountValidator.validateAccountCreation(storeDeliveryStaffId1, "DS" + String.valueOf(SDAcontactNumber).substring(5, 9), "D" + String.valueOf(SDAcontactNumber).substring(5, 9) + " " + "S" + String.valueOf(SDAcontactNumber).substring(5, 9), "" + SDAcontactNumber, parentAccountId, AccountType.SDA, true);
            KhataAccountValidator.validateAccountAssociationCreation(associateAccountId1, parentAccountId);
        }
    }


    @Test(dataProviderClass = dataProvider.class, dataProvider = "createDC")
    public void CreateRollingDC(String code, String tmsHubCode, String pincode, String tenantId, Boolean isSelfShipSupporte, Boolean isStrictServiceable, Boolean isCardEnabled, Boolean isActive, String courierCode, ReconType reconType, String contactNumber, Integer masterbagCapacity) throws Exception {
        // create DC and validate the Account creation

        DeliveryCenterResponse deliveryCenterResponse = createDC(code, tmsHubCode, pincode, tenantId, isSelfShipSupporte, isStrictServiceable, isCardEnabled, isActive, courierCode, ReconType.ROLLING_RECON, contactNumber, masterbagCapacity);
        Long ownerReferenceId = deliveryCenterResponse.getDeliveryCenters().get(0).getId();
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code + "Auto", contactNumber, 1l, AccountType.DC, true);
        AccountResponse accountResponse = KhataServiceHelper.searchAccountByOwnerRefId(ownerReferenceId);
        Long associateAccountId = accountResponse.getAccounts().get(0).getId();
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId, 1l);


        // update DC mobile number , recon type , is active , pincode and validate the Account creation
        Random r = new Random(System.currentTimeMillis());
        int contactNumber1 = Math.abs(1000000000 + r.nextInt(2000000000));
        String newContactNumber = String.valueOf(contactNumber1).substring(3, 9);
        DeliveryCenterResponse deliveryCenterResponse1 = updateDC(ownerReferenceId, code, tmsHubCode, pincode, tenantId, isSelfShipSupporte, isStrictServiceable, isCardEnabled, false, courierCode, ReconType.ROLLING_RECON, newContactNumber, masterbagCapacity);
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code + "Auto", newContactNumber, 1l, AccountType.DC, false);
        AccountResponse accountResponse1 = KhataServiceHelper.searchAccountByOwnerRefId(ownerReferenceId);
        Long associateAccountId1 = accountResponse1.getAccounts().get(0).getId();
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId1, 1l);


    }

    @Test(dataProviderClass = Lastmile_StoreFlowsDP.class, dataProvider = "rollingReconAllwithHSR")
    public void CreateRollingStore(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                   String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        // create Store  and validate the Account creation for Store and SDA

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Long ownerReferenceId = Long.parseLong(storeResponse.getStoreEntries().get(0).getTenantId());
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        Long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId2(storeHlPId).getDeliveryStaffs().get(0).getId();
        AccountResponse accountResponse = KhataServiceHelper.searchAccountByOwnerRefId(storeDeliveryStaffId);
        Long associateAccountId = accountResponse.getAccounts().get(0).getId();
        Long parentAccountId = KhataServiceHelper.getAccountbyownerRefernceandType(ownerReferenceId, AccountType.PARTNER).getAccounts().get(0).getId();
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code, mobileNumber, null, AccountType.PARTNER, true);
        KhataAccountValidator.validateAccountCreation(storeDeliveryStaffId, code, code + " " + code, mobileNumber, parentAccountId, AccountType.SDA, true);
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId, parentAccountId);

        // upadte Store

        StoreResponse storeResponse1 = lastmileHelper.updateStore(name, address, pincode, city, state, ownerReferenceId.toString(),
                true, true, isCardEnabled, code, ReconType.ROLLING_RECON, storeHlPId);
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, code, code, mobileNumber, null, AccountType.PARTNER, true);
        KhataAccountValidator.validateAccountCreation(storeDeliveryStaffId, code, code + " " + code, mobileNumber, parentAccountId, AccountType.SDA, true);
        KhataAccountValidator.validateAccountAssociationCreation(associateAccountId, parentAccountId);
    }


    @Test()
    public void createAndUpdateCourier() throws JAXBException, UnsupportedEncodingException {
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        String courierCode = "CC" + String.valueOf(contactNumber).substring(6, 9);

        //Create CourierCode and validate Account creation

        CourierResponse courierResponse = lmsServiceHelper.createCourier(courierCode , 1000l, TrackingNoSource.GENERATED, "SHIPMENT_LEVEL_CONSOLIDATED", "SHIPMENT_LEVEL_CONSOLIDATED", true, CourierType.NORMAL, "#00FF00", ManifestType.SHIPMENT_LEVEL_CONSOLIDATED, true, true, false, false, false, false, false, false);
        Long ownerReferenceId = Long.parseLong(courierResponse.getCourierEntries().get(0).getLogisticsPartnerId());
        Long courierId = courierResponse.getCourierEntries().get(0).getId();
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, courierCode, courierCode, null, null, AccountType.PARTNER, true);

        //Update CourierCode and Valiadte Account creation

        CourierResponse courierResponse1 = lmsServiceHelper.updateCourier(courierId ,courierCode , 1000l, TrackingNoSource.GENERATED, "SHIPMENT_LEVEL_CONSOLIDATED", "SHIPMENT_LEVEL_CONSOLIDATED", true, CourierType.NORMAL, "#00FF00", ManifestType.SHIPMENT_LEVEL_CONSOLIDATED, true, true, false, false, false, false, false, false);
        KhataAccountValidator.validateAccountCreation(ownerReferenceId, courierCode, courierCode, null, null, AccountType.PARTNER, true);

    }

}