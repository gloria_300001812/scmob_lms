package com.myntra.apiTests.erpservices.CashRecon.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.erpservices.Constants;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.apiTests.erpservices.khata.validator.KhataAccountValidator;
import com.myntra.apiTests.erpservices.khata.validator.LedgerValidator;
import com.myntra.apiTests.erpservices.khata.validator.PaymentValidator;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.lms.tests.CashReconTestDP;
import com.myntra.apiTests.erpservices.lms.tests.LastMileTestDP;
import com.myntra.apiTests.erpservices.lms.tests.MLShipmentResponseValidator;
import com.myntra.apiTests.erpservices.lms.tests.TripOrderAssignmentResponseValidator;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.lms_khata.validator.LMSKhataValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.erpservices.utility.RejoyServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;
import com.myntra.khata.response.PaymentResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.DeliveryStaffCommute;
import com.myntra.lastmile.client.code.utils.DeliveryStaffRole;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.status.MLShipmentUpdateEvent;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.ItemQCStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lms.khata.domain.EventType;
import com.myntra.lms.khata.domain.PaymentMode;
import com.myntra.logistics.platform.domain.*;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_ADDED_TO_REVERSE_BAG;

public class CashReconSanity1 {


    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    LMSKhataValidator lmsKhataValidator = new LMSKhataValidator();
    PaymentValidator paymentValidator = new PaymentValidator();
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa=new DeliveryStaffClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    TripOrderAssignmentClient_QA tripOrderAssignmentClient_qa=new TripOrderAssignmentClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA shipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentClientV2_qa=new MLShipmentClientV2_QA();

    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private OrderEntry orderEntry = new OrderEntry();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private ProcessRelease processRelease = new ProcessRelease();
    private ItemEntry itemEntry = new ItemEntry();
    TripClient tripClient = new TripClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(CashReconSanity1.class);
    StatusPollingValidator statusPollingValidator=new StatusPollingValidator();

    HashMap<String, String> pincodeDCMap;
    String deliveryStaffID1, deliveryStaffID2, DcId;


    //DATA for FD_DL to be used for completing the trip
    String packetId, packetId1, trackingNumber, trackingNumber1;
    long tripId;
    List<Map<String, Object>> tripData = new ArrayList<>();
    Map<String, Object> dataMap1 = new HashMap<>();
    Map<String, Object> dataMap2 = new HashMap<>();
    TripOrderAssignmentResponse tripOrderAssignmentResponse1;
    KhataServiceHelper khataHelper = new KhataServiceHelper();
    //Long SDA1_account_id;//, SDA2_account_id;
    String storeTenantId , storeCode , mobileNumber;
    Long storeHlPId;

    public void checkReconStatus(String orderId, String tripOrderAssignmentId, String status, String isReceived) {
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML(orderId, status, 4), "Wrong status in ml_shipment");
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(tripOrderAssignmentId), isReceived, "Wrong status in trip_order_assignment");
    }

    //TODO : Account ID's
    Long INSTAKART, MYNTRA, DC;

    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() throws IOException, JAXBException, InterruptedException {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.CASH_RECON_ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.CASH_RECON_ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
        DcId = pincodeDCMap.get(LMS_PINCODE.CASH_RECON_ML_BLR);
        //TODO : uncomment as account is not getting created so using an existing 1
        //deliveryStaffID1 = ""+ lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        //   deliveryStaffID2 = ""+ lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
//        deliveryStaffID1 = "3396";
//        deliveryStaffID2 = "1503";

        Thread.sleep(7000); // waiting for SDA account to be created in khata service
//        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
//        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();
//        AccountResponse accountResponse2 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID2), AccountType.SDA);
//        SDA2_account_id = accountResponse2.getAccounts().get(0).getId();

        AccountResponse accountResponse3 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_INSTAKART_ID), AccountType.PARTNER);
        INSTAKART = accountResponse3.getAccounts().get(0).getId();
        AccountResponse accountResponse4 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_MYNTRA_ID), AccountType.PARTNER);
        MYNTRA = accountResponse4.getAccounts().get(0).getId();
        AccountResponse accountResponse5 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(DcId), AccountType.DC);
        DC = accountResponse5.getAccounts().get(0).getId();

    }

    @BeforeClass(alwaysRun = true)
    public void createStore() throws IOException, JAXBException, InterruptedException {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        StoreResponse storeResponse = null;


        while(storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), "code.like:" + ("MC" + String.valueOf(contactNumber).substring(5, 9)), "").getStoreEntries() == null){
            r = new Random(System.currentTimeMillis());
            contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        }

        storeResponse = lastmileHelper.createStore("MA" + String.valueOf(contactNumber).substring(5, 9), "MA_FN" + String.valueOf(contactNumber).substring(5, 9), "MA_LN" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.DELIVERY_HUB_CODE_ELC,
                "LA_gstin" + String.valueOf(contactNumber).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.ROLLING_RECON, 500, "MC" + String.valueOf((contactNumber)+r.nextInt(1000)).substring(5, 9));
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        storeCode = storeResponse.getStoreEntries().get(0).getCode();
        mobileNumber =  "" + contactNumber;
    }


    //adding  payment cases
    //TODO : less and excess payment
    @Test(groups = {"Smoke", "Regression"}, priority = 1, description = "C20280,C20284,C20285,C20296,C20345,C20346,C20347", enabled = true)
    public void excessCashDepositedBySDA() throws Exception {
        Long SDA1_account_id;
        String deliveryStaffID1;
        deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();

        Float sda_payment_1 = 2000F;
        Float sda_correct_amount = 1099F;
        Long wrong_DC_acnt_id = 81L, wrong_SDA_account_id = 5263L;

        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);

        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        SoftAssert softAssert = new SoftAssert();

        //Wrong DC
        PaymentResponse response = khataHelper.createAndApprove(wrong_DC_acnt_id, SDA1_account_id, sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "creation of payment for wrong DC and Correct SDA should not be allowed" + " DC and SDA Account ID:" + wrong_DC_acnt_id + " " + SDA1_account_id);

        //wrong SDA
        response = khataHelper.createAndApprove(DC, wrong_SDA_account_id, sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "creation of payment with wrong SDA should not be allowed" + " DC and SDA Account ID:" + DC + " " + wrong_SDA_account_id);
        //correct SDA and DC Excess amount
        response = khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Creation of payment with excess amount should not be allowed" + " DC and SDA Account ID:" + DC + " " + SDA1_account_id);

        //correct sda dc amount
//        response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_correct_amount, PaymentType.CASH);
//        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "exact amount payment failed"+ " DC and SDA Account ID:" + DC +" "+ SDA1_account_id);

        Thread.sleep(7000);
        //paying again with no pendency should throw error
        response = khataHelper.createAndApprove(DC, SDA1_account_id, sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "paying again with same amount should throw error" + " DC and SDA Account ID:" + DC + " " + SDA1_account_id);

        //without DC
        response = khataHelper.createAndApproveForNegScenarios("", SDA1_account_id.toString(), sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "Without DC account ID payment should not be allowed " + " DC and SDA Account ID:" + DC + " " + SDA1_account_id);

        //without SDA
        response = khataHelper.createAndApproveForNegScenarios(DC.toString(), "", sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "without sda account id payment should not be allowed" + " DC and SDA Account ID:" + DC + " " + SDA1_account_id);

        //without DC and SDA
        response = khataHelper.createAndApproveForNegScenarios("", "", sda_correct_amount, PaymentType.CASH);
        softAssert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.ERROR, "without sda and dc account id payment should not be allowed" + " DC and SDA Account ID:" + DC + " " + SDA1_account_id);

        softAssert.assertAll();

    }

    //TODO : try and buy cases
    //TODO:  check manually the return tracking number no entries are not made in DB

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , Forward all flow test", enabled = true)
    public void triedAndNotBought() throws Exception {

        Long SDA1_account_id;
        String deliveryStaffID1;
        deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;
        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderID);// DL
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId);

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);

        Thread.sleep(7000);
        //TODO : Check Shipment Creation event and shipment pendency
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validation pendency for IK to myntra
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        //    khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount1)); // 1 order

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);

        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //

        Thread.sleep(7000);
        //TODO : check Receive_in_dc event
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
        //     khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
        //    khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);


        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);


        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        Thread.sleep(10000);
        //TODO : Delivered Event will  be triggered on event table
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        //TODO : should knock off prev pendency of DC and create pendency of SDA

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validate account pendency on SDA to DC but account will be used for instakart
        Thread.sleep(5000);
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1));

        //ledger validation
        //DC to INSTAKART will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);

        //SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);


        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        Double amount = orderEntryDetails.getFinalAmount();
        orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNum1);
        Thread.sleep(5000);

        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNum1.substring(2, trackingNum1.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_NOT_BOUGHT);
        itemEntry.setRemarks("not bought");
        itemEntry.setQcStatus(ItemQCStatus.PASSED);
        itemEntry.setTriedAndNotBoughtReason(TryAndBuyNotBoughtReason.SIZE_TOO_SMALL);
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);
// mark delivered and reconcile shouldn't work

        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        String ml_shipment_id = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
        String return_tracking_num = lmsHelper.updateOperationalTrackingNumForTryAndBuy(ml_shipment_id);
        //reconcile

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //Item is delivered
        Thread.sleep(7000);

        // TODO : DL event will be generated
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.DELIVERED.toString(), SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, formatted_amount1, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        //shipment pendency will move from sda from goods to cash
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validate Account pendency
        // SDA will now have a pendency of cash for DL
        //      khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount1), String.valueOf(goods_SDA));

        //ledger validation
        // item delivered goods pendency will be removed and item which got Failed ledger will remain same. 1st will remain same ledger

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNum1);


        //validateDuringReconciliation(deliveryStaffID, DcId, return_tracking_num.substring(2,return_tracking_num.length()),1L,EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(return_tracking_num.substring(2, return_tracking_num.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        Long tripOrderAssignmentIdForReturn = (long) lmsHelper.getTripOrderAssignmentIdByTrackingNum.apply(return_tracking_num);
        Assert.assertEquals(lmsServiceHelper.validateIsReceivedStatusForShipmentRecon(String.valueOf(tripOrderAssignmentIdForReturn)), "true", "Wrong status in trip_order_assignment");
        Assert.assertEquals(lmsServiceHelper.getOrderStatusByTrackingNum(return_tracking_num), EnumSCM.PICKUP_SUCCESSFUL, "Status not updated to received in DC");
        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //after receiving the shipment in dc as it's a try buy order nothing should happen
        //shipment pendency will move from sda from goods to cash

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        //    khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount1), String.valueOf(goods_SDA));

        //taking the tried and not bought order till dls

        //TODO: From here check the return tracking number no entries are made in DB
        System.out.println("Returns Tracking Number  " + return_tracking_num);

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(new Date());
        MLShipmentUpdateEntry mlShipmentUpdateEntryForRTO = new MLShipmentUpdateEntry();
        mlShipmentUpdateEntryForRTO.setTrackingNumber(return_tracking_num);
        mlShipmentUpdateEntryForRTO.setEventTime(date);
        mlShipmentUpdateEntryForRTO.setDeliveryCenterId(Long.valueOf(DcId));
        mlShipmentUpdateEntryForRTO.setEventLocation("DC- 5");
        mlShipmentUpdateEntryForRTO.setRemarks("Customer Refused");
        mlShipmentUpdateEntryForRTO.setEvent(MLShipmentUpdateEvent.RTO_CONFIRMED);
        mlShipmentUpdateEntryForRTO.setShipmentType(ShipmentType.TRY_AND_BUY);
        mlShipmentUpdateEntryForRTO.setShipmentUpdateMode(ShipmentUpdateActivityTypeSource.MyntraLogistics);
        mlShipmentUpdateEntryForRTO.setTenantId(LASTMILE_CONSTANTS.TENANT_ID);
        mlShipmentUpdateEntryForRTO.setEventAdditionalInfo(ShipmentUpdateAdditionalInfo.REFUSED);
        mlShipmentUpdateEntryForRTO.setUserName(null);
        mlShipmentUpdateEntryForRTO.setTripId(null);

        String mlShipmentResponse1 = lmsServiceHelper.updateStatus(mlShipmentUpdateEntryForRTO);
        Assert.assertTrue(mlShipmentResponse1.contains(EnumSCM.SUCCESS), "Status not updated successfully");

        MLShipmentResponse mlShipmentResponse2 = shipmentServiceV2Client_qa.getMLShipmentDetails(return_tracking_num, LMS_CONSTANTS.TENANTID);
        String returnId = mlShipmentResponse2.getMlShipmentEntries().get(0).getSourceReferenceId();
        System.out.println("stop");

        long masterBagId = (long) tmsServiceHelper.createNcloseMBforReturn.apply(returnId);
        tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);


        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WH");
        Assert.assertEquals(
                lmsServiceHelper.receiveReturnShipmentFromMasterbag(masterBagId, String.valueOf(returnId)).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive return from masterbag in warehouse");


        ExceptionHandler.handleTrue(lmsReturnHelper.validateReturnShipmentStatusInLMS(returnId, ShipmentStatus.DELIVERED_TO_SELLER.toString(), 10));
        lmsServiceHelper.validateRmsStatusAndRefund(returnId, EnumSCM.RRC, true, Constants.LMS_PATH.sleepTime);

        System.out.println("Tried and not bought should not flow reverse vlidate it: " + return_tracking_num);
    }


    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , Forward all flow test", enabled = true)
    public void triedAndBought() throws Exception {

        Long SDA1_account_id;
        String deliveryStaffID1;
        deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;
        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderID);// DL
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId);

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);

        Thread.sleep(7000);
        //TODO : Check Shipment Creation event and shipment pendency
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validation pendency for IK to myntra
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        //    khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount1)); // 1 order

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);

        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //

        Thread.sleep(7000);
        //TODO : check Receive_in_dc event
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);


        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);


        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        Thread.sleep(10000);
        //TODO : Delivered Event will  be triggered on event table
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        //TODO : should knock off prev pendency of DC and create pendency of SDA

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validate account pendency on SDA to DC but account will be used for instakart
        Thread.sleep(5000);
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1));

        //ledger validation
        //DC to INSTAKART will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);

        //SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);


        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        Double amount = orderEntryDetails.getFinalAmount();
        orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNum1);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNum1.substring(2, trackingNum1.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.TRIED_AND_BOUGHT);
        itemEntry.setRemarks("bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);

        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        String ml_shipment_id = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
//        String return_tracking_num = lmsHelper.updateOperationalTrackingNumForTryAndBuy(ml_shipment_id);
        //reconcile

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //Item is delivered
        Thread.sleep(7000);

        // TODO : DL event will be generated
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.DELIVERED.toString(), SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, formatted_amount1, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        //shipment pendency will move from sda from goods to cash
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validate Account pendency
        // SDA will now have a pendency of cash for DL
        //     khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount1), String.valueOf(goods_SDA));

        //ledger validation
        // item delivered goods pendency will be removed and item which got Failed ledger will remain same. 1st will remain same ledger

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNum1);


        //validateDuringReconciliation(deliveryStaffID, DcId, return_tracking_num.substring(2,return_tracking_num.length()),1L,EnumSCM.SUCCESS);
        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //after receiving the shipment in dc as it's a try buy order nothing should happen
        //shipment pendency will move from sda from goods to cash
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        //     khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount1), String.valueOf(goods_SDA));

    }

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , Forward all flow test", enabled = true)
    public void notTriedAndBought() throws Exception {

        Long SDA1_account_id;
        String deliveryStaffID1;
        deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;
        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderID);// DL
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId);

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);

        Thread.sleep(7000);
        //TODO : Check Shipment Creation event and shipment pendency
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validation pendency for IK to myntra
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        //      khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount1)); // 1 order

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);

        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //

        Thread.sleep(7000);
        //TODO : check Receive_in_dc event
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);


        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);


        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        Thread.sleep(7000);
        //TODO : Delivered Event will  be triggered on event table
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        //TODO : should knock off prev pendency of DC and create pendency of SDA

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validate account pendency on SDA to DC but account will be used for instakart
        Thread.sleep(5000);
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1));

        //ledger validation
        //DC to INSTAKART will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);

        //SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);


        com.myntra.oms.client.entry.OrderEntry orderEntryDetails = omsServiceHelper.getOrderEntry(orderID);

        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId);
        Double amount = orderEntryDetails.getFinalAmount();
        orderResponse = mlShipmentClientV2_qa.getTryAndBuyItems(LASTMILE_CONSTANTS.TENANT_ID, trackingNum1);
        Long Id = orderResponse.getOrders().get(0).getItemEntries().get(0).getId();//ml_try_and_buy_item
        orderEntry.setOperationalTrackingId(trackingNum1.substring(2, trackingNum1.length()));
        orderEntry.setOrderId(packetId);
        orderEntry.setDeliveryCenterId(Long.valueOf(DcId));
        orderEntry.setCodAmount(amount);
        itemEntry.setId(Id);
        itemEntry.setStatus(TryAndBuyItemStatus.NOT_TRIED);
        itemEntry.setRemarks("bought");
        List<ItemEntry> entries = new ArrayList<>();
        entries.add(itemEntry);
        orderEntry.setItemEntries(entries);

        TripOrderAssignmentResponse tripOrderAssignmentResponse = lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId, EnumSCM.DELIVERED, EnumSCM.UPDATE, tripId, orderEntry);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Trip not updated");
        String ml_shipment_id = tripOrderAssignmentResponse.getTripOrders().get(0).getOrderEntry().getId().toString();
        //String return_tracking_num = lmsHelper.updateOperationalTrackingNumForTryAndBuy(ml_shipment_id);
        //reconcile

        checkReconStatus(packetId, String.valueOf(tripOrderAssignmentId), EnumSCM.DELIVERED, "false");

        //Item is delivered
        Thread.sleep(7000);

        // TODO : DL event will be generated
        lmsKhataValidator.validateEventCreation(trackingNum1, EventType.DELIVERED.toString(), SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, formatted_amount1, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        //shipment pendency will move from sda from goods to cash
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validate Account pendency
        // SDA will now have a pendency of cash for DL
        //     khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount1), String.valueOf(goods_SDA));

        //ledger validation
        // item delivered goods pendency will be removed and item which got Failed ledger will remain same. 1st will remain same ledger

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNum1);


        //validateDuringReconciliation(deliveryStaffID, DcId, return_tracking_num.substring(2,return_tracking_num.length()),1L,EnumSCM.SUCCESS);
        //complete trip
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //after receiving the shipment in dc as it's a try buy order nothing should happen
        //shipment pendency will move from sda from goods to cash
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        //   khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount1), String.valueOf(goods_SDA));

    }

    //Adding validation in cases

    // Dl store bag -> FD orders -> receive in DC -> requeue -> assign to another DC -> DL it.
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void store5(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber1, String address, String pincode, String city, String state, String mappedDcCode,
                       String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        Long SDA1_account_id, SDA2_account_id;
        String deliveryStaffID1;

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0, cash_STORE1 = 0.0, goods_STORE1 = 0.0, cash_SDA1 = 0.0, goods_SDA1 = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);//

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);

        //validation pendency for IK to myntra
        Thread.sleep(7000);
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));

        //validation of event creation, ledger pendency, shipment pendency

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1.toString(), "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);

        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
        Thread.sleep(7000);
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //event, shipment pendency,Ledger validation
        // Myntra to Ik will be balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));

        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);
        //Delivering

        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(Store_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        //Once store bag is delivered to store it will be liable for that goods pendency on DC(Account instakart)
        //validate account pendency
        Thread.sleep(7000);
        //balancing the account pendency of DC
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));

        //validate ledger, event , shipment pendency dc to instakart will be balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), Store_account_id.toString(), "true", null, formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(Store_account_id), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        // Assert.assertEquals(tripShipmentAssociationResponse.get);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        // DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient.findDeliveryStaffByMobNo(mobileNumber);
//        long expectedstoreDeliveryStaffId = Long.parseLong(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile());
//        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);

        //tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(tripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Thread.sleep(2000);
         tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Thread.sleep(2000);
         tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Thread.sleep(2000);
        tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);

        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);
        //after failing the delivery in store nothing should change
        Thread.sleep(7000);
        //balancing the account pendency of DC
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));

        //validate ledger , event , shipment pendency

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickip
        // Pickup the reverse bag

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStore(String.valueOf(reverseBagId), forwardTrackingNumbersList, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, forwardTrackingNumbersList.size(), 0, 0.0f,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");


        //validate event and shipment pendency, ledger
        Thread.sleep(10000);
        lmsKhataValidator.validateEventCreation(tracking_num, CASH_RECON_EVENT_TYPE_ADDED_TO_REVERSE_BAG, Store_account_id.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //receive trip order
        //validateDuringReconciliation(deliveryStaffID, DcId, tracking_num, 1L, EnumSCM.SUCCESS);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);


        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");

        //once reverse bag is received in DC Pendency will move from Store to DC
        //validate account pendency
//        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1));

        //validate ledger
        //balancing store ledger
        // ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        //ledger will be created for DC to Instakart for the same amount
        // ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);
        Thread.sleep(7000);

        //validating ledger balancing SDA ka pendency
        lmsKhataValidator.validateEventCreation(tracking_num, EventType.RECEIVED_IN_DC.toString(), SDA1_account_id.toString(), DC.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //requeue
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.autoAssignmentOfOrderToTrip(packetId, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);

        //once you requeue the order nothing should change in account and ledger entry as it still remains in DC

        //assign to another DC


        //Create Another store and add the order in store bag and dl it
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        mobileNumber = String.valueOf(contactNumber);
        code = String.valueOf(contactNumber).substring(5, 9);
        StoreResponse storeResponse = lastmileHelper.createStore("AJ" + name, "AJ" + ownerFirstName, "AJ" + ownerLastName, latLong, emailId, mobileNumber1, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse2 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id1 = accountResponse2.getAccounts().get(0).getId();

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra

        shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        deliveryStaffID1 = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));

        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA2_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA2_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA1 = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA1 = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }

        tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);
        param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);

        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(Store_account_id1, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_STORE1 = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods_STORE1 = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }

        //Delivering
        shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        //pendency will be made for the new store on instakart
        //dc to instakart
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id1), Long.toString(INSTAKART), String.valueOf(cash_STORE1), String.valueOf(goods_STORE1 + amount1));

        Thread.sleep(7000);
        //validate ledger DC will be balanced and store will be created
        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), Store_account_id1.toString(), "true", null, formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(Store_account_id1), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id1), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        // DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient.findDeliveryStaffByMobNo(mobileNumber);
//        long expectedstoreDeliveryStaffId = Long.parseLong(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile());
//        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);

        //tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(tripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        tripOrderIdAttemptReasonMap = new HashMap<>();
        tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber1, storeTenantId);
        tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);
        mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);

        //Dl the order
        tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to deliver orders in store bag");

        //once order is delivered to the customer the goods pendency will be removed and cash pendency will be created
        //    khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id1), Long.toString(INSTAKART), String.valueOf(cash_STORE1+ amount1), String.valueOf(goods_STORE1));

        Thread.sleep(7000);
        //validate ledger
        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), Store_account_id1.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(Store_account_id1), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id1), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id1), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


    }

    //TODO : Check manually if the online order is not flowing in the system
    @Test
    public void online() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "on", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId);
        System.out.println("Tracking Number" + trackingNum1);
        System.out.println("validate if it's not flowing in lms-khata");
        lmsKhataValidator.validateEventCreationNeg(trackingNum1);

    }

    //FD of Store bag -> Receive in DC
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag",  enabled = true)
    public void store3() throws Exception {

        Long SDA1_account_id;
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        String pincode = LMS_PINCODE.ML_BLR;
        String tenantId = "4019";



        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);


        //validation pendency for IK to myntra
        Thread.sleep(5000);
        //event and shipment pendency
        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1.toString(), "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(tracking_num, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1));

        //Ledger validation
        // Myntra to Ik will be balanced

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);

        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));

        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);
        //Delivering
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.FaileddeliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Store should have failed ");

        //TODO : After store bag is with SDA then pendency should move to SDA, but as of now as code is not written for this pendency remainds with DC

        //nothing should change as of now
        Thread.sleep(5000);
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1));
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);

        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(tracking_num, LASTMILE_CONSTANTS.TENANT_ID);

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);

        //TODO : after receiving the order pendency should from SDA to dc but as code is not there currently notheing should change

        Thread.sleep(5000);
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1));
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);

        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


    }

    //TODO: Test 5 -> FD->Receive in DC->requeu-> DL
    @Test
    public void FD_ReceiveDC_Requeue_DL_and_DL_LOST_RTO() throws Exception {
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;

        Long SDA1_account_id, SDA2_account_id;
        String deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        String deliveryStaffID2 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));

        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();
        AccountResponse accountResponse2 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID2), AccountType.SDA);
        SDA2_account_id = accountResponse2.getAccounts().get(0).getId();

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// FD
        //String trackingNum1 = lmsHelper.getTrackingNumber(packetId);

        trackingNumber = lmsHelper.getTrackingNumber(packetId);

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);


        //validation pendency for IK to myntra
        //event shipment pendency
        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
       // khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount1)); // 2 orders amount plus existing 1

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);

        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //

        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //Ledger validation
        // Myntra to Ik will be balanced
        Thread.sleep(7000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);


        // OFD for FD and DL Order
        // Received_in_dc
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }

        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));


        //

        //validate account pendency on SDA to DC but account will be used for instakart
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1 ));

        //ledger validation
        //DC to INSTAKART will be balanced
        Thread.sleep(7000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        //TODO : should knock off prev pendency of DC and create pendency of SDA

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);

        //SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);


        //Failing and delivering the order

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        //validate Account pendency
        // SDA will now have a pendency of cash and goods both as 1 FD and 1 DL
        Thread.sleep(7000);
        //   khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1));

        //ledger validation
        // item delivered goods pendency will be removed and item which got Failed ledger will remain same. 1st will remain same ledger
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);


        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.NOT_REACHABLE_UNAVAILABLE);
        tripData.add(dataMap1);

        //Receiving the FD order in DC
        //after receiving the FD order at dc liability will be removed from SDA of that good

        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");

        //complete trip
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //account pendency
        Thread.sleep(7000);

//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA));//goods pendency will be removed cash will remain same
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1));

        //validate ledger
        //goods pendency will be removed of first order which is received in dc
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.RECEIVED_IN_DC.toString(), SDA1_account_id.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        //TODO : should knock off prev pendency of DC and create pendency of SDA

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
        //cash pendency should remain as it is

        //ledger entry will be created for DC to instakart
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);


        //FD order after received in dc to Requeue
        //requeue

        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.autoAssignmentOfOrderToTrip(packetId, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator1 = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse3);

        //goods pendency will remain same on DC to instakart
        //account pendency
        Thread.sleep(7000);

//        khataAccountValidator.validateAccountPendency(Long.toString(SDA2_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA));//goods pendency will be removed cash will remain same
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1));

        //validate ledger
        //goods pendency will be removed of first order which is received in dc
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
        //cash pendency should remain as it is

        //ledger entry will be created for DC to instakart
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);

        //validate account

        //after marking everything should remain as it is
        //account pendency
        Thread.sleep(7000);

//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1));

        //validate ledger
        //goods pendency will be removed of first order which is received in dc
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
        //cash pendency should remain as it is

        //ledger entry will be created for DC to instakart
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);


        //Delivering the [FD->Receive in DC->requeued order]


        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID2));
        long tripId = tripResponse1.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        //validate account pendency on sda to DC [GOODS]
        //goods account pendency on SDA to Instakrt as trip started
        //Account pendency OF DC to INstakart will be balanced
        //   khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));


        //   khataAccountValidator.validateAccountPendency(Long.toString(SDA2_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA ), String.valueOf(goods_SDA + amount1));

        //validate ledger pendency on SDA will be created for the requeue order
        //balancing goods pendency on DC to Instakart for requeued order
        Thread.sleep(7000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA2_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA2_account_id), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
        //Goods pendency will be created on SDA to INStakart for requeud order being delivered
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA2_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);


        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.DELIVERED, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //AS order is deliverd now GOods pendency will be removed and cash pendency will be created on SDA
        // Account pendency
        //     khataAccountValidator.validateAccountPendency(Long.toString(SDA2_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount1), String.valueOf(goods_SDA));

        //validate ledger Goods pendency on SDA will be balanced and Cash pendency will be created


        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.DELIVERED.toString(), SDA2_account_id.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA2_account_id), formatted_amount1.toString(), "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA2_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
        //sda cash pendency will be created

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA2_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber);

        //TODO: Add validation
        //Cash pendency should be removed from SDA Goods pendency on DC to INstakart
        //validate account pendency{Remove cash pendency from SDA}
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA2_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount1), String.valueOf(goods_SDA));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));


    }

    //TODO : 7
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void onlineStore(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                            String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);


        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "on", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order

        //validation pendency for IK to myntra

        //getting current pendency of dc to instakart

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //validate account pendency and ledger. now pendency will go to DC

        //Instakart amount will be balanced after receiving the shipment in DC
        //validate Event and shipment pendency creation


        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        // getting account of SDA


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);

        //Delivering
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");


        //once store bag is delivered to store pendency will move from SDA to store . as of now pendency will move to store
        //validate account pendency which will move to store


        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        // Assert.assertEquals(tripShipmentAssociationResponse.get);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        // DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient.findDeliveryStaffByMobNo(mobileNumber);
//        long expectedstoreDeliveryStaffId = Long.parseLong(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile());
//        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);

        //tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(tripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);

        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Thread.sleep(2000);
        tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        Thread.sleep(2000);
        tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);

        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_PAYMENT_TYPE_DEBIT_CARD,LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to deliver orders in store bag");

        //after delivering the order to customer pendency of the store will move from goods to cash
        //account pendency

        System.out.println("Check if the order didn't flow in lms-khata for : " + tracking_num);
        lmsKhataValidator.validateEventCreationNeg(tracking_num);

    }

    //TODO : 6 assigning sda to another dc
    @Test
    public void assignSDAToAnotherDC() throws Exception {
        Long SDA1_account_id, SDA1_account_id2;
        Double cash_SDA = 0.0, cash_SDA1 = 0.0;

        String deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        Map<String, Object> sda = (Map<String, Object>) DBUtilities.exSelectQueryForSingleRecord("select * from delivery_staff where id = " + deliveryStaffID1, "lms");

        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Thread.sleep(3000);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findOrdersByTrip(tripNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateGetTripOrderByTripNumber(lmsHelper.getTrackingNumber(packetId), packetId, ShippingMethod.NORMAL.toString(), "Delivered");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();
        // assign SDA to another DC
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
        }

        Assert.assertEquals(cash_SDA, 1099.00, "cash sda should be liable");
        DeliveryStaffResponse deliveryStaffResponse1 = lmsServiceHelper.updateDeliveryStaff(sda.get("id").toString(), sda.get("code").toString(), sda.get("first_name").toString(), sda.get("last_name").toString(), 1l, sda.get("mobile").toString(), sda.get("created_by").toString(), true, true, DeliveryStaffCommute.BIKER, sda.get("emp_code").toString(), true, DeliveryStaffRole.STAFF, sda.get("tenant_id").toString());
        Assert.assertEquals(deliveryStaffResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "SDA is not marked inactive");

        AccountResponse accountResponse2 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id2 = accountResponse2.getAccounts().get(0).getId();

        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA1 = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
        }
        Assert.assertEquals(SDA1_account_id, SDA1_account_id2, "Account id changed");
        Assert.assertEquals(cash_SDA1, 1099.00, "cash sda should be liable");


    }

    //TODO : 12
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID: , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void multipleStoreBag(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                 String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;
        Double cash1 = 0.0, goods1 = 0.0, cash_DC1 = 0.0, goods_DC1 = 0.0, cash_SDA1 = 0.0, goods_SDA1 = 0.0, cash_STORE1 = 0.0, goods_STORE1 = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();


        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();

        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " +storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " +storeCode+ " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);


        //validation pendency for IK to myntra
        Thread.sleep(5000);
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        //     khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));


        // Event creation And Shipment Pendency

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //validate account pendency and ledger. now pendency will go to DC

        //Instakart amount will be balanced after receiving the shipment in DC
        Thread.sleep(5000);
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //Ledger validation
        // Myntra to Ik will be balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", null, formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);

        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
         //create 1 more master bag for store and add it in same trip

        //creating another store


        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        mobileNumber = String.valueOf(contactNumber);
        code = String.valueOf(contactNumber).substring(5, 9);
        StoreResponse storeResponse3 = lastmileHelper.createStore("AJ" + name, "AJ" + ownerFirstName, "AJ" + ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse3.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId1 = storeResponse3.getStoreEntries().get(0).getTenantId();
        Long storeHlPId1 = storeResponse3.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId1 + " and the code is " + storeResponse3.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId1 + " and the code is " + storeResponse3.getStoreEntries().get(0).getCode());

        Long masterBagId1 = null;
        ShipmentResponse masterBagResponse1 = null;
        String originDCCity1 = null;
        String destinationStoreCity1 = null;
        String searchParams1 = "code.like:" + code;
        List<String> forwardTrackingNumbersList1 = new ArrayList<>();
        Thread.sleep(5000);
        AccountResponse accountResponse2 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId1), AccountType.PARTNER);
        Long Store_account_id1 = accountResponse2.getAccounts().get(0).getId();

        Long originPremiseId1 = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity1 = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse4 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams1, "");
        destinationStoreCity1 = storeResponse4.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse1 = lastmileHelper.createMasterBag(originPremiseId1, com.myntra.lms.client.status.PremisesType.DC, storeHlPId1, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity1, destinationStoreCity1);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId1 = masterBagResponse1.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId1);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId1);

        log.info("The masterBag id created for store : " + storeResponse3.getStoreEntries().get(0).getCode() + " is : " + masterBagId1);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse3.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash1 = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods1 = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String tracking_num1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 2nd order

        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse1.getOrders().get(0).getCodAmount(); // for 1st order
        format1 = new DecimalFormat("0.00");
        String formatted_amount2 = format1.format(amount2);


        //validation pendency for IK to myntra
        Thread.sleep(5000);
        //     khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));


        // Event creation And Shipment Pendency

        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validation of ledger pendency
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        //getting current pendency of dc to instakart
        accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC1 = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC1 = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);
        //validate account pendency and ledger. now pendency will go to DC

        //Instakart amount will be balanced after receiving the shipment in DC
        Thread.sleep(5000);
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //Ledger validation
        // Myntra to Ik will be balanced

        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", null, formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);

        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId1), ShipmentType.DL, tracking_num1, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId1, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("check if 2 bags are created " + masterBagId + "      " + masterBagId1);

        // add both the masterbag to 1 trip
        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId1));

        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA1 = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA1 = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId1, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        TripShipmentAssociationResponse tripShipmentAssociationResponse2 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId1),LMS_CONSTANTS.TENANTID);

        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");

        System.out.println("check if account id of sda has no pendency");
        accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            Assert.assertEquals(cash_SDA1.toString(), "0.00", "no cash pendency should be found");
            Assert.assertEquals(goods_SDA1.toString(), "0.00", "no goods pendency should be found");
        }
    }

  //TODO : 17-> Receive store bag at store already having cash pendency
  // Dl store bag -> dl orders
  @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", enabled = true)
  public void store() throws Exception {

      Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

      Long originPremiseId = null;
      Long masterBagId = null;
      ShipmentResponse masterBagResponse = null;
      String originDCCity = null;
      String destinationStoreCity = null;
      String searchParams = "code.like:" + storeCode;
      List<String> forwardTrackingNumbersList = new ArrayList<>();

      String pincode = LMS_PINCODE.ML_BLR;
      String tenantId = "4019";

      log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
      System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

      //get the store account
      Thread.sleep(5000);
      AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
      Long Store_account_id = accountResponse1.getAccounts().get(0).getId();

      originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
      originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
      StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
      destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
      try {
          masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
      } catch (IOException e) {
          e.printStackTrace();
      } catch (JAXBException e) {
          e.printStackTrace();
      }
      masterBagId = masterBagResponse.getEntries().get(0).getId();
      log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
      System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

      log.info("The masterBag id created for store : " + storeCode+ " is : " + masterBagId);
      System.out.println("\n_____________The masterBag id created for store : " +storeCode + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);


        //validation pendency for IK to myntra
        Thread.sleep(5000);
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        //     khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));
        // Event creation And Shipment Pendency

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //validate account pendency and ledger. now pendency will go to DC

        //Instakart amount will be balanced after receiving the shipment in DC
        Thread.sleep(5000);
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));

        //Ledger validation
        // Myntra to Ik will be balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", null, formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);

        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

      com.myntra.lastmile.client.response.TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
      log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
      System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
      //TODO : add below in validation
      Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
      Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
      Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
      Thread.sleep(3000);
      lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, storeCode, originPremiseId);
      String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
      com.myntra.lastmile.client.response.TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
      Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
      Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
      Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
      Assert.assertEquals(tripClient.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);

        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(Store_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        //Delivering
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        //   Assert.assertTrue(shipmentResponse.getStatus().getStatusMessage().equals("success"));
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");


        //once store bag is delivered to store pendency will move from SDA to store . as of now pendency will move to store
        //validate account pendency which will move to store

        Thread.sleep(5000);
        //balancing the account pendency of DC
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));

        //validate ledger dc to instakart will be balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), Store_account_id.toString(), "true", null, formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(Store_account_id), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        // Assert.assertEquals(tripShipmentAssociationResponse.get);
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);

        //Now check if the above store Delivery Staff is the same as the one which was created during store creation

        // DeliveryStaffResponse deliveryStaffResponse = deliveryStaffClient.findDeliveryStaffByMobNo(mobileNumber);
//        long expectedstoreDeliveryStaffId = Long.parseLong(deliveryStaffResponse.getDeliveryStaffs().get(0).getMobile());
//        Assert.assertTrue(storeDeliveryStaffId == expectedstoreDeliveryStaffId);

        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);

        //tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(tripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);

      Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
      String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
      Long storeTripId = storeTrip.getTrips().get(0).getId();
      System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
      TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to deliver orders in store bag");

        //after delivering the order to customer pendency of the store will move from goods to cash
        //account pendency
        Thread.sleep(5000);
        //balancing the account pendency of store will move from goods to cash
        //  khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE+ amount1), String.valueOf(goods_STORE));

        //validate ledger goods will be balanced and cash will formed
        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), Store_account_id.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID, "Null");

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);

        //deliver 1 more store bag to same store
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String tracking_num1 = lmsHelper.getTrackingNumber(packetId1);

        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse1.getOrders().get(0).getCodAmount(); // for 1st order
        format1 = new DecimalFormat("0.00");
        String formatted_amount2 = format1.format(amount2);

        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num1, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        // getting account of SDA
        Thread.sleep(5000);
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        Long SDA2_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

      tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
      log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
      System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
      //TODO : add below in validation
      Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
      Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
      Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
      Thread.sleep(3000);
      lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, storeCode, originPremiseId);
      param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
      tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
      Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
      Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
      Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num1, "Wrong order in master bag");
      Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
      tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
      Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num1);


        //Delivering
        shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));

      //pendency should move from dc to store

      Thread.sleep(5000);
      lmsKhataValidator.validateEventCreation(tracking_num1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), Store_account_id.toString(), "true", null, formatted_amount2, formatted_amount2, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
      lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        Thread.sleep(6000);
        //pendency should move from dc to store
        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), Store_account_id.toString(), "true", null, formatted_amount2, formatted_amount2, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(Store_account_id), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);

        khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), amount1.toString(), String.valueOf(amount2)); // 2 orders amount plus existing 1


    }


    //TODO : 25. 1 exchange and dl order deliver both and verify if on payment dl order is only getting removed
    @Test()
    public void exc_dl_order() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        Float sda_correct_amount = 1099F;

        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String exchangeOrder = lms_createOrder.createMockOrderExchange(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true, omsServiceHelper.getReleaseId(orderID));
        String packetId = omsServiceHelper.getPacketId(exchangeOrder);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);

        //Normal order to be delivered
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId1);



        String deliveryStaffID = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.

        //DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNum1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        long tripOrderAssignmentId = (long) lmsHelper.getTripOrderAssignemntIdForExchange.apply(packetId);
        long tripOrderAssignmentId1 = (long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId);

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.findExchangesByTrip(tripNumber, LMS_CONSTANTS.TENANTID);
        TripOrderAssignmentResponseValidator tripOrderAssignmentResponseValidator = new TripOrderAssignmentResponseValidator(tripOrderAssignmentResponse);
        tripOrderAssignmentResponseValidator.validateTripOrderStatus(EnumSCM.OFD);
        tripOrderAssignmentResponseValidator.validateTripId(tripId);
        tripOrderAssignmentResponseValidator.validateTrackingNum(trackingNumber);
        tripOrderAssignmentResponseValidator.validateExchangeOrderId(packetId);
        Assert.assertEquals(tripOrderAssignmentResponse.getTripOrders().get(0).getTenantId(), LASTMILE_CONSTANTS.TENANT_ID, "Invalid Tenant Id in response");
        orderEntry.setOperationalTrackingId(trackingNumber.substring(2, trackingNumber.length()));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(9000);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripOrderAssignmentClient_qa.receiveTripOrder(trackingNumber.substring(2, trackingNumber.length()), LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive");
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip(tripOrderAssignmentId1,
                EnumSCM.DELIVERED, EnumSCM.UPDATE, packetId1, tripId, orderEntry).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);

        tripData.add(dataMap1);
        Thread.sleep(7000);

        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Thread.sleep(7000);
        //after payment DL item should be removed
        PaymentResponse response = khataHelper.createAndApprove(DC, SDA1_account_id, sda_correct_amount, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "creation of payment for wrong DC and Correct SDA should not be allowed" + " DC and SDA Account ID:" + DC.toString() + " " + SDA1_account_id);


        Thread.sleep(17000);

        PaymentResponse paymentResponse = khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(), DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = "" + paymentResponse.getPayments().get(paymentResponse.getPayments().size() - 1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_correct_amount.toString(), PaymentStatus.APPROVED.toString());

        //TODO : Event creation for payment is_processed is 0 but it's processed need to comment out to proceed further

        //validate event, payment event will be created

        lmsKhataValidator.validateEventCreation(trackingNum1, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", "Null", "0.00", "1099.00", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, payment_id_1, "Null");

        //validate shipment pendency the amount will be cleared off the 2nd Delivered item first
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA1_account_id), "0.00", "1099.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        System.out.println("stop and check Exchange didn't flow at all");
        lmsKhataValidator.validateEventCreationNeg(trackingNumber);

    }


    //TODO : 29
    @Test
    public void unassignOrder() throws Exception {

        Long SDA1_account_id;
        String deliveryStaffID1;
        deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();

        Thread.sleep(5000);
        Long SDA2_account_id;
        String deliveryStaffID2;
        deliveryStaffID2 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        AccountResponse accountResponse2 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID2), AccountType.SDA);
        SDA2_account_id = accountResponse2.getAccounts().get(0).getId();
        Thread.sleep(5000);

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// FD
        String trackingNum1 = lmsHelper.getTrackingNumber(packetId);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId1 = omsServiceHelper.getPacketId(orderId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1); // DL
        String trackingNum2 = lmsHelper.getTrackingNumber(packetId1);


        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount1 = format.format(amount1);


        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse1.getOrders().get(0).getCodAmount(); // for 2nd order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount2 = format1.format(amount2);


        Thread.sleep(7000);
        //TODO : Check Shipment Creation event and shipment pendency
        lmsKhataValidator.validateEventCreation(trackingNum1, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNum2, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

//        //validation pendency for IK to myntra
//        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount1 + amount2)); // 2 orders amount plus existing 1

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNum2);

        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        //
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);

        Thread.sleep(7000);
        //TODO : check Receive_in_dc event
        lmsKhataValidator.validateEventCreation(trackingNum1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNum2, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(DC), "0.00", "0.00", formatted_amount2.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 + amount2));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), trackingNum2);


        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNum2);


        // OFD for FD and DL Order
        // Received_in_dc
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        // unassigning order from trip
        String tripOrder = Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        String tripOrder1 = Long.toString((Long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));

        TripOrderAssignmentResponse tripOrderAssignmentResponse = tripClient_qa.unassignOrderFromTrip(tripOrder);
        TripOrderAssignmentResponse tripOrderAssignmentResponse1 = tripClient_qa.unassignOrderFromTrip(tripOrder1);
        Assert.assertEquals(tripOrderAssignmentResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "unassigning was unsuccesful");
        Assert.assertEquals(tripOrderAssignmentResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "unassigning was unsuccesful");

        tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID2));
        tripId = tripResponse.getTrips().get(0).getId();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        //check pendency moved from DC to 2 nd SDA
        Thread.sleep(7000);
        //TODO : Delivered Event will  be triggered on event table
        lmsKhataValidator.validateEventCreation(trackingNum1, com.myntra.lms.khata.domain.EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA2_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNum2, com.myntra.lms.khata.domain.EventType.OUT_FOR_DELIVERY.toString(), DC.toString(), SDA2_account_id.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNum1, String.valueOf(SDA2_account_id), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(SDA2_account_id), "0.00", "0.00", formatted_amount2.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //validate account pendency on SDA to DC but account will be used for instakart
        Thread.sleep(5000);
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1 + amount2));

        //ledger validation
        //DC to INSTAKART will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), trackingNum2);

        //SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA2_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNum1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA2_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNum2);

        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
            Assert.assertEquals(cash.toString(), "0.0");
            Assert.assertEquals(goods.toString(), "0.0");

        }

        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(SDA2_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());

        }
        Assert.assertEquals(cash.toString(), "0.0");
        Assert.assertEquals(goods.toString(), "2198.0");

    }

    // Admin correction
    // pendency before admin correction
    // TODO: DL 2 items pay for 1 which was last delivered , mark it lost/RTO , check goods pendency on DC and cash pendency removed from 2nd item
    @Test(dataProviderClass = CashReconTestDP.class, dataProvider = "lostRto")
    public void pendencyBeforeAdminCorrection(String adminStatus) throws Exception {
        Long SDA1_account_id;
        String deliveryStaffID1;
        deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;
        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// dl
        //String trackingNum1 = lmsHelper.getTrackingNumber(packetId);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId1 = omsServiceHelper.getPacketId(orderId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1); // DL
        //String trackingNum2 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);

        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse1.getOrders().get(0).getCodAmount(); // for 2nd order

        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount2 = format1.format(amount2);

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validation pendency for IK to myntra
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        //  khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount1 + amount2)); // 2 orders amount plus existing 1

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNumber1);

        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);


        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);

        Thread.sleep(10000);

        //TODO : check Receive_in_dc event
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount2.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 + amount2));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), trackingNumber1);


        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNumber1);


        // OFD for FD and DL Order
        // Received_in_dc
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
//        trackingNumber = lmsHelper.getTrackingNumber(packetId);
//        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }

        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));

        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId1, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId1, EnumSCM.OUT_FOR_DELIVERY, 2));

        //

        //validate account pendency on SDA to DC but account will be used for instakart
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1 + amount2));

        //ledger validation
        //DC to INSTAKART will be balanced
        Thread.sleep(15000);

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), trackingNumber1);

        //SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNumber1);


        //delivering  both the order

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(4000);
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


        Thread.sleep(7000);
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.DELIVERED.toString(), SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.DELIVERED.toString(), SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(SDA1_account_id), formatted_amount2, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate Account pendency
        // SDA will now have a pendency of cash and goods both as 1 FD and 1 DL
        //     khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount2), String.valueOf(goods_SDA + amount1));

        //ledger validation
        // item delivered goods pendency will be removed and item which got Failed ledger will remain same. 1st will remain same ledger
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber);

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount2), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount2), LedgerType.CASH.toString(), trackingNumber1);


        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        dataMap2.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId));
        dataMap2.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        tripData.add(dataMap1);
        tripData.add(dataMap2);


        //complete trip
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //do a payment of half the amount
        PaymentResponse response = khataHelper.createAndApprove(DC, SDA1_account_id, Float.valueOf(formatted_amount1), PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "creation of payment for wrong DC and Correct SDA should not be allowed" + " DC and SDA Account ID:" + DC + " " + SDA1_account_id);

        //After payment pendency will be removed from last delivered item

        PaymentResponse paymentResponse = khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(), DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = "" + paymentResponse.getPayments().get(paymentResponse.getPayments().size() - 1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), amount1.toString(), PaymentStatus.APPROVED.toString());

        //TODO : Event creation for payment is_processed is 0 but it's processed need to comment out to proceed further

        //validate event, payment event will be created
        Thread.sleep(10000);
        lmsKhataValidator.validateEventCreation(trackingNumber1, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", "Null", formatted_amount2, formatted_amount2.toString(), "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, payment_id_1, "Null");
        //validate shipment pendency the amount will be cleared off the 2nd Delivered item first
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(SDA1_account_id), "0.00", formatted_amount2, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount2), LedgerType.CASH.toString(), trackingNumber1);

        //now after marking dl order lost
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, adminStatus, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        Thread.sleep(12000);
        if (adminStatus == EnumSCM.RTO) {
            adminStatus = EnumSCM.RTO_CONFIRMED;
            lmsKhataValidator.validateEventCreation(trackingNumber1, adminStatus, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        }
        //shipment pendency
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), "0.00", "1099.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());

        //ledger for the item lossed
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount2), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), trackingNumber1);

        // ledger will be created
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);

        //account pendency of that sda should ne 0
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), "0.0", "0.0");

    }

    // pendency after admin correction
    @Test(dataProviderClass = CashReconTestDP.class, dataProvider = "lostRto")
    public void pendencyAfterAdminCorrection(String adminStatus) throws Exception {
        Long SDA1_account_id;
        String deliveryStaffID1;
        deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0;
        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }
        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// dl
        //String trackingNum1 = lmsHelper.getTrackingNumber(packetId);

        //String trackingNum2 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);

        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);


        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validation pendency for IK to myntra
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        //  khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), cash.toString(), String.valueOf(goods + amount1 + amount2)); // 2 orders amount plus existing 1

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);

        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        //shipment creation
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);


        Thread.sleep(10000);

        //TODO : check Receive_in_dc event
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount1.toString(), "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate account pendency and ledger. now pendency will go from DC to IK

        //Instakart amount will be balanced after receiving the shipment in DC
//        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 + amount2));

        //Ledger validation
        // Myntra to Ik will be balanced
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);


        //new ledger will be created from Ik to DC
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);


        // OFD for FD and DL Order
        // Received_in_dc
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
//        trackingNumber = lmsHelper.getTrackingNumber(packetId);
//        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");

        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }

        //start trip
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));


        //

        //validate account pendency on SDA to DC but account will be used for instakart
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1 + amount2));

        //ledger validation
        //DC to INSTAKART will be balanced
        Thread.sleep(15000);

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);

        //SDA to INSTAKART will be liable
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);


        //delivering  both the order

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


        Thread.sleep(7000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.DELIVERED.toString(), SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


        //TODO : knock off prev pending amount and add it to another account.
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate Account pendency
        // SDA will now have a pendency of cash and goods both as 1 FD and 1 DL
        //     khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA + amount2), String.valueOf(goods_SDA + amount1));

        //ledger validation
        // item delivered goods pendency will be removed and item which got Failed ledger will remain same. 1st will remain same ledger
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber);


        dataMap1.put("trip_order_assignment_id", lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId));
        dataMap1.put("AttemptReasonCode", AttemptReasonCode.DELIVERED);
        tripData.add(dataMap1);


        //complete trip
        Assert.assertEquals(lmsServiceHelper.completeTrip(tripData).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //do a payment of half the amount
        PaymentResponse response = khataHelper.createAndApprove(DC, SDA1_account_id, Float.valueOf(formatted_amount1), PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "creation of payment for wrong DC and Correct SDA should not be allowed" + " DC and SDA Account ID:" + DC + " " + SDA1_account_id);

        //After payment pendency will be removed from last delivered item

        PaymentResponse paymentResponse = khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(), DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = "" + paymentResponse.getPayments().get(paymentResponse.getPayments().size() - 1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), amount1.toString(), PaymentStatus.APPROVED.toString());

        //TODO : Event creation for payment is_processed is 0 but it's processed need to comment out to proceed further

        //validate event, payment event will be created
        Thread.sleep(10000);
        lmsKhataValidator.validateEventCreation(trackingNumber, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", "Null", formatted_amount1, formatted_amount1.toString(), "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, payment_id_1, "Null");
        //validate shipment pendency the amount will be cleared off the 2nd Delivered item first
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), trackingNumber);

        //now after marking dl order lost
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, adminStatus, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        Thread.sleep(10000);
        if (adminStatus == EnumSCM.RTO) {
            adminStatus = EnumSCM.RTO_CONFIRMED;
        }
        lmsKhataValidator.validateEventCreation(trackingNumber, adminStatus, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

        //shipment pendency
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());


        //ledger for the item lossed
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber);

        //deliver another item with the same staff

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.CASH_RECON_ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        packetId1 = omsServiceHelper.getPacketId(orderId1);
        //String trackingNum1 = lmsHelper.getTrackingNumber(packetId);

        //String trackingNum2 = lmsHelper.getTrackingNumber(packetId1);
        trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);

        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId);
        Double amount2 = orderResponse1.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount2 = format1.format(amount2);
        TripResponse tripResponse1 = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID1));
        Assert.assertEquals(tripResponse1.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"create trip failed");
        tripId = tripResponse1.getTrips().get(0).getId();
        tripNumber = tripResponse1.getTrips().get(0).getTripNumber();

        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(trackingNumber1, tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId1, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");


        khataHelper.pushExcess(SDA1_account_id,DC);
        Thread.sleep(5000);

        // ledger will be created
        //ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);

        //account pendency of that sda should ne 0
         khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), "0.0", "0.0");

    }

    //marking lost at diff status

    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, dataProviderClass = CashReconTestDP.class, dataProvider = "markingLostForDiffOrder", description = "ID: C19282 , markingLostForDiffOrder", enabled = true)
    public void markingLostForDiffOrder(String toStatus, String res) throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderID = lmsHelper.createMockOrder(toStatus, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderID);
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), res, "Mismatch");

        if (res == EnumSCM.SUCCESS) {
            MLShipmentResponse mlShipmentResponse1 = shipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, LMS_CONSTANTS.TENANTID);
            MLShipmentResponseValidator mlShipmentResponseValidator1 = new MLShipmentResponseValidator(mlShipmentResponse1);
            if (toStatus == EnumSCM.PK || toStatus == EnumSCM.IS) {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.EXPECTED_IN_DC);
                //lmsKhataValidator.validateEventCreation(trackingNumber, EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");

            } else {
                mlShipmentResponseValidator1.validateShipmentStatus(EnumSCM.LOST);
            }

            //after marking lost from the following statuses event should not be triggered
            lmsKhataValidator.validateEventCreationNeg(tracking_num, EventType.LOST.toString());

            //marking RTO after lost
            statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_CONFIRMED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
            Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), res, "Mismatch");
            System.out.println("stop");
            lmsKhataValidator.validateEventCreationNeg(tracking_num, EventType.RTO_CONFIRMED.toString());

        }
    }

    //marking lost for a FD order event shouldn't be triggered
    @Test
    public void markingLostForFD() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID =  "" +lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        String tracking_num = lmsHelper.getTrackingNumber(packetId);

        // DBUtilities.exUpdateQuery("update trip set trip_status='COMPLETED' where delivery_staff_id=" + deliveryStaffID + " AND `tenant_id`=" + LASTMILE_CONSTANTS.TENANT_ID, "lms");
        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");

        //marking lost
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");
        lmsKhataValidator.validateEventCreationNeg(tracking_num, EventType.LOST.toString());

        //lost to rto
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_CONFIRMED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);

//        Thread.sleep(7000);
//        //TODO : Check Shipment Creation event and shipment pendency
//        lmsKhataValidator.validateEventCreation(tracking_num, EventType.RTO_CONFIRMED.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        // as per the new change RTO_CONFIRMED event wont flow and RTO_DISPATCH will flow

        TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
        long masterBagId = (long) tmsServiceHelper.createcloseMBforRTO(20L, packetId, DcId);
        tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBagId);

        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WH");

        Assert.assertEquals(
                lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive shipment in DC");

        ExceptionHandler.handleTrue(lmsServiceHelper.validateOrderStatusInLMS(packetId, ShipmentStatus.RTO_IN_TRANSIT.toString(),10));
        System.out.println("stop");

        //RTO_DISPATCHED event
        Thread.sleep(7000);
        lmsKhataValidator.validateEventCreation(tracking_num, EventType.RTO_DISPATCHED.toString(), DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");


    }

    //TODO : needs discussion on what should happen
    // Flow : DL -> FD -> Lost -> DL
    @Test
    public void dlFdLostDL() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String deliveryStaffID =  "" +lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA1_account_id = accountResponse1.getAccounts().get(0).getId();

        String tracking_num = lmsHelper.getTrackingNumber(packetId);

        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.DELIVERED, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");

        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //FD
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.FD, EnumSCM.NOT_REACHABLE_UNAVAILABLE, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        //Failed delivery event should pass as we have to convert the cash pendency to goods


        //LOST
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        // after lost nothing should change
        //DL
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.DL, EnumSCM.DELIVERED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        Thread.sleep(7000);
        //In ledger SDA -> Cash balanced and for DC -> Cash should be created.
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), "0.0", "0.0");

    }

    //TODO : needs discussion on what should happen DEV should block or handel
    // FD -> DL (Admin) -> Lost -> DL .
    @Test(enabled = true)
    public void fdDlLostDl() throws Exception {
        String DcId = pincodeDCMap.get(LMS_PINCODE.ML_BLR);
        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);

        String deliveryStaffID =  "" +lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA1_account_id = accountResponse1.getAccounts().get(0).getId();

        TripResponse tripResponse = tripClient_qa.createTrip(Long.parseLong(DcId), Long.parseLong(deliveryStaffID));
        Assert.assertEquals(tripResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to create Trip");
        long tripId = tripResponse.getTrips().get(0).getId();
        Assert.assertEquals(tripClient_qa.addAndOutscanNewOrderToTrip(lmsHelper.getTrackingNumber(packetId), tripId, LASTMILE_CONSTANTS.TENANT_ID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to addAndOutscanNewOrderToTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.ASSIGNED_TO_SDA, 2));
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to startTrip");
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInML("" + packetId, EnumSCM.OUT_FOR_DELIVERY, 2));
        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
                EnumSCM.FAILED, EnumSCM.UPDATE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to update orders in Trip.");
        Thread.sleep(2000);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.FAILED_DELIVERY, 3), "Order status is not in FAILED_DELIVERY in order_to_ship");

//        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
//                EnumSCM.FAILED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //Dl the order
        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.DL, EnumSCM.DELIVERED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(7000);

        //After marking DL from FD the gooods pendency should be removed from SDA and cash pendency will be created
        lmsKhataValidator.validateEventCreation(tracking_num, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_DELIVERED_STATUS_CORRECTION, DC.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        Assert.assertEquals(lmsServiceHelper.updateOrderInTrip((long) lmsHelper.getTripOrderAssignemntIdForOrder.apply(packetId, tripId),
        EnumSCM.DELIVERED, EnumSCM.TRIP_COMPLETE).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to complete trip.");

        //lost STOP
         statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        //after marking lost pendency will move to DC from SDA
        Thread.sleep(7000);

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.LOST.toString(), DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //DL
        statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.DL, EnumSCM.DELIVERED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(7000);

        lmsKhataValidator.validateEventCreation(tracking_num, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_DELIVERED_STATUS_CORRECTION, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


    }
}