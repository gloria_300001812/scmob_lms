package com.myntra.apiTests.erpservices.CashRecon.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.common.Constants.ShipmentSource;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.common.ProcessOrder.ServiceImpl.ProcessReleaseExecutor;
import com.myntra.apiTests.common.ProcessOrder.ServiceImpl.ProcessReleaseLMSHelper;
import com.myntra.apiTests.common.entries.ReleaseDetailsEntry;
import com.myntra.apiTests.common.entries.ReleaseEntry;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.apiTests.erpservices.khata.validator.KhataAccountValidator;
import com.myntra.apiTests.erpservices.khata.validator.LedgerValidator;
import com.myntra.apiTests.erpservices.khata.validator.PaymentValidator;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.tests.CashReconTestDP;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.lms_khata.validator.LMSKhataValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.portalservices.pps.PPSServiceHelper;
import com.myntra.cache.T;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;
import com.myntra.khata.response.PaymentResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lms.khata.domain.EventType;
import com.myntra.lms.khata.domain.PaymentMode;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_ADDED_TO_REVERSE_BAG;
import static com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_DELIVERED_STATUS_CORRECTION;
import static com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT;

public class StoreAdmin {


    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentClientV2 = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    LMSKhataValidator lmsKhataValidator = new LMSKhataValidator();
    LedgerValidator ledgerValidator = new LedgerValidator();
    TripClient_QA tripClient_qa=new TripClient_QA();
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa=new DeliveryStaffClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    StatusPollingValidator statusPollingValidator=new StatusPollingValidator();


    PaymentValidator paymentValidator = new PaymentValidator();

    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private PPSServiceHelper ppsServiceHelper = new PPSServiceHelper();
    private OrderEntry orderEntry = new OrderEntry();
    private LMS_CreateOrder lms_createOrder = new LMS_CreateOrder();
    private ProcessRelease processRelease = new ProcessRelease();
    private ItemEntry itemEntry = new ItemEntry();
    MLShipmentClient mlShipmentClient = new MLShipmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 shipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(StoreAdmin.class);
    HashMap<String, String> pincodeDCMap;
    String deliveryStaffID1, deliveryStaffID2, DcId;


    //DATA for FD_DL to be used for completing the trip
    String packetId, packetId1, trackingNumber, trackingNumber1;
    long tripId;
    List<Map<String, Object>> tripData = new ArrayList<>();
    Map<String, Object> dataMap1 = new HashMap<>();
    Map<String, Object> dataMap2 = new HashMap<>();
    TripOrderAssignmentResponse tripOrderAssignmentResponse1;
    KhataServiceHelper khataHelper = new KhataServiceHelper();
    Long SDA1_account_id, SDA2_account_id;
    String storeTenantId, storeCode, mobileNumber;
    Long storeHlPId;

    //TODO : Account ID's
    Long INSTAKART, MYNTRA, DC, STORE;

    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() throws IOException, JAXBException, InterruptedException {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.CASH_RECON_ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.CASH_RECON_ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
        DcId = pincodeDCMap.get(LMS_PINCODE.CASH_RECON_ML_BLR);
        //TODO : uncomment as account is not getting created so using an existing 1
        deliveryStaffID1 = "" + lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DcId));

        Thread.sleep(7000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();
        AccountResponse accountResponse3 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_INSTAKART_ID), AccountType.PARTNER);
        INSTAKART = accountResponse3.getAccounts().get(0).getId();
        AccountResponse accountResponse4 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_MYNTRA_ID), AccountType.PARTNER);
        MYNTRA = accountResponse4.getAccounts().get(0).getId();
        AccountResponse accountResponse5 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(DcId), AccountType.DC);
        DC = accountResponse5.getAccounts().get(0).getId();

    }

    @BeforeClass(alwaysRun = true)
    public void createStore() throws IOException, JAXBException, InterruptedException {
        String pattern = "YYYY MM DD HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "In"));
        String date = simpleDateFormat.format(new Date()).replace(" ", "").replace(":", "");
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        StoreResponse storeResponse = null;


        while (storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), "code.like:" + ("XC" + String.valueOf(contactNumber).substring(5, 9)), "").getStoreEntries() == null) {
            r = new Random(System.currentTimeMillis());
            contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        }

        storeResponse = lastmileHelper.createStore("SA" + String.valueOf(contactNumber).substring(5, 9), "SA_FN" + String.valueOf(contactNumber).substring(5, 9), "SA_LN" + String.valueOf(contactNumber).substring(5, 9),
                "LA_latlong" + String.valueOf(contactNumber).substring(5, 6), "test@lastmileAutomation.com", "" + contactNumber,
                "Automation Address", LASTMILE_CONSTANTS.STORE_PINCODE, "Bangalore", "Karnataka", LASTMILE_CONSTANTS.DELIVERY_HUB_CODE_ELC,
                "LA_gstin" + String.valueOf(contactNumber).substring(5, 9), LMS_CONSTANTS.TENANTID, true, false, true, 5, StoreType.MASTER, ReconType.ROLLING_RECON, 500, "XC" + String.valueOf((contactNumber) + r.nextInt(1000)).substring(5, 9));
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        storeCode = storeResponse.getStoreEntries().get(0).getCode();
        mobileNumber = "" + contactNumber;
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        STORE = accountResponse1.getAccounts().get(0).getId();
    }

    // Payment Done before admin correction .
    // TODO: DL 2 items pay for 1 which was last delivered , mark it lost/RTO , check goods pendency on DC and cash pendency removed from 2nd item
    @Test(dataProviderClass = CashReconTestDP.class, dataProvider = "StorelostRto")
    public void StorependencyBeforeAdminCorrection(String adminStatus , String ShipmentStatus) throws Exception {

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        String pincode = LMS_PINCODE.ML_BLR;
        String tenantId = LMS_CONSTANTS.TENANTID;

        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        // Create Storebag for store.
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);


        // Create 2 orders in PK state

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount1 = orderResponse1.getOrders().get(0).getCodAmount(); // for 1st order
        String formatted_amount1 = format.format(amount1);

        // validate event creation SHIPMENT_CREATION and shipment pendency

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - SHIPMENT_CREATION  .
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // PK to RECEIVE_IN_DC

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);


        // validate event creation RECEIVE_IN_DC and shipment pendency

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - RECEIVE_IN_DC.

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // Add shipments to Masterbag

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber1, null, null, null, null, null, null, false).build();

        try {
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create a Deliverystaff .

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.

        // Create a trip with Storebag.

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(trackingNumber);
        forwardTrackingNumbersList.add(trackingNumber1);


        //Delivering the store bag to store and close the Trip
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        // Validate Event and Shipment pendency for HAND_OVER_TO_LAST_MILE_PARTNER

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate ledger for HAND_OVER_TO_LAST_MILE_PARTNER
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);


        // Create a StoreTrip and Delivery the Order .
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        TripResponse storeTrip = mlshipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");


        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Thread.sleep(5000);
        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId1, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse1.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Thread.sleep(5000);

        // Validate Event and Shipment Pendency for DELIVERED

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate Ledger Entries for DELIVERED.

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber1);

        // create a reverse trip .

        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();

        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + trackingNumber + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + trackingNumber1 + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");

        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        // Add amount to reverse bag trip .

        float amountCollected = amount.floatValue() + 200.0f; // Amount collected from Store in reverse bag trip.
        Thread.sleep(2000);
        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, forwardTrackingNumbersList.size(), 0, amountCollected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");

        // calculate cashCollected for each shipment based on LIFO

        double cashtobeCollected = -((amount + amount1) - amountCollected);
        double cashPending = -amountCollected + amount1;
        String formatted_cashPending = format.format(cashPending);
        double cashPending1 = cashPending + amount;
        String formatted_cashPending1 = format.format(cashPending1);

        double cashCollected = amount - cashPending1;
        String formatted_cashCollected = format.format(cashCollected);

        // Validate Shipment pendency after Store paid to SDA

        Thread.sleep(5000);
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), formatted_cashPending1, formatted_cashCollected, "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), formatted_cashCollected, "0.00", "0.00", "false", "Null", INSTAKART.toString());

        // Validate Ledger Entry for STORE and SDA.

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(cashCollected), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-cashCollected), LedgerType.CASH.toString(), trackingNumber);

        // Now , Mark LOST/RTO by Admin Correction .

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, adminStatus, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        // Validate Event and  Shipment Creation for LOST/RTO.


        lmsKhataValidator.validateEventCreation(trackingNumber, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA1_account_id.toString(), "true", "Null", "Null", formatted_cashPending1, "Null", "Null", "Null", "Null", "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, ShipmentStatus, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID, "Null", "true");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(STORE), "0.00", "0.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", "Null", INSTAKART.toString());

        // Validate Ledger Entries .

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(cashPending1), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-cashPending1), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

    }

    // Payment Done after admin correction .
    // TODO: DL 2 items pay for 1 which was last delivered , mark it lost/RTO , check goods pendency on DC and cash pendency removed from 2nd item
    @Test(dataProviderClass = CashReconTestDP.class, dataProvider = "StorelostRto")
    public void StorependencyAfterAdminCorrection(String adminStatus , String ShipmentStatus) throws Exception {

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        String pincode = LMS_PINCODE.ML_BLR;
        String tenantId = LMS_CONSTANTS.TENANTID;

        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        // Create Storebag for store.
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);


        // Create 2 orders in PK state

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount1 = orderResponse1.getOrders().get(0).getCodAmount(); // for 1st order
        String formatted_amount1 = format.format(amount1);

        // validate event creation SHIPMENT_CREATION and shipment pendency

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - SHIPMENT_CREATION  .
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // PK to RECEIVE_IN_DC

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);


        // validate event creation RECEIVE_IN_DC and shipment pendency

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - RECEIVE_IN_DC.

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // Add shipments to Masterbag

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber1, null, null, null, null, null, null, false).build();

        try {
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create a Deliverystaff .

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.

        // Create a trip with Storebag.

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(trackingNumber);
        forwardTrackingNumbersList.add(trackingNumber1);


        //Delivering the store bag to store and close the Trip
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        // Validate Event and Shipment pendency for HAND_OVER_TO_LAST_MILE_PARTNER

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate ledger for HAND_OVER_TO_LAST_MILE_PARTNER
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);


        // Create a StoreTrip and Delivery the Order .
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");


        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Thread.sleep(5000);
        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId1, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse1.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Thread.sleep(5000);

        // Validate Event and Shipment Pendency for DELIVERED

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate Ledger Entries for DELIVERED.

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber1);

        // Now , Mark LOST/RTO by Admin Correction .

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, adminStatus, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        // Validate Event and  Shipment Creation for LOST/RTO.

        lmsKhataValidator.validateEventCreation(trackingNumber1, ShipmentStatus, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID, "Null", "true");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(STORE), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate Ledger Entries .

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNumber);

        // create a reverse trip .

        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();

        //Assign reverse Bag to Trip and get the reverseBag id

        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + trackingNumber + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + trackingNumber1 + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");

        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        // Add amount to reverse bag trip .

        float amountCollected = amount.floatValue(); // Amount collected from Store in reverse bag trip.
        String formatted_amountCollected= format.format(amountCollected);

        Thread.sleep(2000);
        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, forwardTrackingNumbersList.size(), 0, amountCollected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");

        // Validate Shipment pendency after Store paid to SDA

        Thread.sleep(5000);
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE),"0.00", formatted_amountCollected, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), formatted_amountCollected, "0.00", "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(STORE), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate Ledger Entry for STORE and SDA.

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amountCollected), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amountCollected), LedgerType.CASH.toString(), trackingNumber);


    }

    // Payment Done before admin correction - DL to FD.
    // TODO: DL 2 items pay for 1 which was last delivered , mark it lost/RTO , check goods pendency on DC and cash pendency removed from 2nd item
    @Test()
    public void StorePendencybeforeDLtoFD() throws Exception {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        String pincode = LMS_PINCODE.ML_BLR;
        String tenantId = LMS_CONSTANTS.TENANTID;

        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        // Create Storebag for store.
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);


        // Create 2 orders in PK state

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount1 = orderResponse1.getOrders().get(0).getCodAmount(); // for 1st order
        String formatted_amount1 = format.format(amount1);

        // validate event creation SHIPMENT_CREATION and shipment pendency

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - SHIPMENT_CREATION  .
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // PK to RECEIVE_IN_DC

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);


        // validate event creation RECEIVE_IN_DC and shipment pendency

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - RECEIVE_IN_DC.

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // Add shipments to Masterbag

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber1, null, null, null, null, null, null, false).build();

        try {
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create a Deliverystaff .

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.

        // Create a trip with Storebag.

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(trackingNumber);
        forwardTrackingNumbersList.add(trackingNumber1);


        //Delivering the store bag to store and close the Trip
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        // Validate Event and Shipment pendency for HAND_OVER_TO_LAST_MILE_PARTNER

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate ledger for HAND_OVER_TO_LAST_MILE_PARTNER
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);


        // Create a StoreTrip and Delivery the Order .
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");


        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Thread.sleep(5000);
        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId1, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse1.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Thread.sleep(5000);

        // Validate Event and Shipment Pendency for DELIVERED

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate Ledger Entries for DELIVERED.

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber1);

        // create a reverse trip .

        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();

        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + trackingNumber + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + trackingNumber1 + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");

        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        // Add amount to reverse bag trip .

        float amountCollected = amount.floatValue() + 200.0f; // Amount collected from Store in reverse bag trip.
        Thread.sleep(2000);
        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, forwardTrackingNumbersList.size(), 0, amountCollected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");

        // calculate cashCollected for each shipment based on LIFO

        double cashtobeCollected = -((amount + amount1) - amountCollected);
        double cashPending = -amountCollected + amount1;
        String formatted_cashPending = format.format(cashPending);
        double cashPending1 = cashPending + amount;
        String formatted_cashPending1 = format.format(cashPending1);

        double cashCollected = amount - cashPending1;
        String formatted_cashCollected = format.format(cashCollected);

        // Validate Shipment pendency after Store paid to SDA

        Thread.sleep(5000);
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), formatted_cashPending1, formatted_cashCollected, "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), formatted_cashCollected, "0.00", "0.00", "false", "Null", INSTAKART.toString());

        // Validate Ledger Entry for STORE and SDA.

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(cashCollected), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-cashCollected), LedgerType.CASH.toString(), trackingNumber);

        // Now , Mark LOST/RTO by Admin Correction .

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.FD,AttemptReasonCode
                .INCOMPLETE_INCORRECT_ADDRESS.toString(), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        // Validate Event and  Shipment Creation for FD.


        lmsKhataValidator.validateEventCreation(trackingNumber, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA1_account_id.toString(), "true", "Null", "Null", formatted_cashPending1, "Null", "Null", "Null", "Null", "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber,CASH_RECON_EVENT_TYPE_PAYMENT,STORE.toString(),SDA1_account_id.toString(),"true","Null","Null",formatted_cashPending1,"Null","Null","Null","Null","Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EnumSCM.FAILED_DELIVERY, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID, "Null", "true");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(STORE), "0.00", "0.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", "Null", INSTAKART.toString());

        // Validate Ledger Entries .

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(cashPending1), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-cashPending1), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);
    }

    // DL by store -> Payment done by STORE to SDA -> Payment done by SDA to DC -> RTO -> DL -> DC Payment to IK -> RTO -> RTOL

    @Test()
    public void StoreAdmin() throws Exception {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        String pincode = LMS_PINCODE.ML_BLR;
        String tenantId = LMS_CONSTANTS.TENANTID;

        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        // Create Storebag for store.
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);


        // Create 2 orders in PK state

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount1 = orderResponse1.getOrders().get(0).getCodAmount(); // for 1st order
        String formatted_amount1 = format.format(amount1);

        // validate event creation SHIPMENT_CREATION and shipment pendency

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - SHIPMENT_CREATION  .
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // PK to RECEIVE_IN_DC

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);


        // validate event creation RECEIVE_IN_DC and shipment pendency

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - RECEIVE_IN_DC.

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // Add shipments to Masterbag

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber1, null, null, null, null, null, null, false).build();

        try {
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create a Deliverystaff .

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.

        // Create a trip with Storebag.

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(trackingNumber);
        forwardTrackingNumbersList.add(trackingNumber1);


        //Delivering the store bag to store and close the Trip
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        // Validate Event and Shipment pendency for HAND_OVER_TO_LAST_MILE_PARTNER

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate ledger for HAND_OVER_TO_LAST_MILE_PARTNER
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);


        // Create a StoreTrip and Delivery the Order .
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");


        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Thread.sleep(5000);
        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId1, AttemptReasonCode.DELIVERED);
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse1.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Thread.sleep(5000);

        // Validate Event and Shipment Pendency for DELIVERED

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate Ledger Entries for DELIVERED.

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber1);

        // create a reverse trip .

        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();

        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + trackingNumber + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + trackingNumber1 + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");

        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        // Add amount to reverse bag trip .

        float amountCollected = amount.floatValue() + 200.0f; // Amount collected from Store in reverse bag trip.
        Thread.sleep(2000);
        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, forwardTrackingNumbersList.size(), 0, amountCollected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");

        // calculate cashCollected for each shipment based on LIFO

        double cashtobeCollected = -((amount + amount1) - amountCollected);
        double cashPending = -amountCollected + amount1;
        String formatted_cashPending = format.format(cashPending);
        double cashPending1 = cashPending + amount;
        String formatted_cashPending1 = format.format(cashPending1);

        double cashCollected = amount - cashPending1;
        String formatted_cashCollected = format.format(cashCollected);

        // Validate Shipment pendency after Store paid to SDA

        Thread.sleep(5000);
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), formatted_cashPending1, formatted_cashCollected, "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), formatted_cashCollected, "0.00", "0.00", "false", "Null", INSTAKART.toString());

        // Validate Ledger Entry for STORE and SDA.

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(cashCollected), LedgerType.CASH.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-cashCollected), LedgerType.CASH.toString(), trackingNumber);

        // Payment Done by SDA to DC.

        Float sda_pending_payment = 200F;
        Float sda_payment_1 = amountCollected - sda_pending_payment;
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);
        PaymentResponse paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        String payment_id_1 = "PAYMENT_"+paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_payment_1.toString(), PaymentStatus.APPROVED.toString());

        // calculate Payment for SDA pendency
//
//        Float sdaCollected = formatted_cashCollected;
//
//
//        // Validate Event and Shipment Pendency for Payment done by SDA to DC
//
//        Thread.sleep(7000);
//        lmsKhataValidator.validateEventCreation(trackingNumber1, LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT, SDA1_account_id.toString(), DC.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "200.00", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID ,payment_id_1);
//        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(SDA1_account_id), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
//        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(SDA1_account_id), "899.00", "200.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
//        //for DC pending will be 200 for same tracking num
//        lmsKhataValidator.validateShipmentPendency(trackingNum2, String.valueOf(DC), "200.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());


    }


    @Test()
    public void FDtoLOSTandFDtoDL_afterReverseBag() throws Exception {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> reverseBagTrackingNumbersList = new ArrayList<>();
        String pincode = LMS_PINCODE.ML_BLR;
        String tenantId = LMS_CONSTANTS.TENANTID;

        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        // Create Storebag for store.
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);


        // Create 2 orders in PK state

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount1 = orderResponse1.getOrders().get(0).getCodAmount(); // for 1st order
        String formatted_amount1 = format.format(amount1);

        // validate event creation SHIPMENT_CREATION and shipment pendency

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - SHIPMENT_CREATION  .
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // PK to RECEIVE_IN_DC

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);


        // validate event creation RECEIVE_IN_DC and shipment pendency

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - RECEIVE_IN_DC.

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // Add shipments to Masterbag

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber1, null, null, null, null, null, null, false).build();

        try {
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create a Deliverystaff .

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.

        // Create a trip with Storebag.

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(trackingNumber);
        forwardTrackingNumbersList.add(trackingNumber1);


        //Delivering the store bag to store and close the Trip
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        // Validate Event and Shipment pendency for HAND_OVER_TO_LAST_MILE_PARTNER

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate ledger for HAND_OVER_TO_LAST_MILE_PARTNER
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);


        // Create a StoreTrip and Delivery the Order .
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");


        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);

        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId1, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.failedDeliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);

        // Validate Event and Shipment Pendency for FAILED_DELIVERED

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate ledger for FAILED_DELIVERED

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // create a reverse trip .

        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();

        //Assign reverse Bag to Trip and get the reverseBag id

        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + trackingNumber + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + trackingNumber1 + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");

        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        // Add amount to reverse bag trip .

        reverseBagTrackingNumbersList.add(trackingNumber1);
        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStore(String.valueOf(reverseBagId),reverseBagTrackingNumbersList, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, reverseBagTrackingNumbersList.size(), 0, 0.00f,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");

        // Validate Event Shipment pendency after Store paid to SDA

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber1, CASH_RECON_EVENT_TYPE_ADDED_TO_REVERSE_BAG, STORE.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(STORE), "0.00","0.00" , "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount1, "false", "Null","Null");

        // Validate Ledger Entry for STORE and SDA.

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // Now , Mark LOST/RTO by Admin Correction .

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.DL,AttemptReasonCode
                .DELIVERED.toString(), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        PickupResponse statusResponse1 = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Thread.sleep(5000);
        Assert.assertEquals(statusResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        // Validate Event and  Shipment Creation .

//
//        lmsKhataValidator.validateEventCreation(trackingNumber, CASH_RECON_EVENT_TYPE_DELIVERED_STATUS_CORRECTION, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID, "Null", "true");
//        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());

        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate Ledger Entries .
//
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber);


        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

 }



    @Test()
    public void FDtoLOSTandFDtoDL_beforeReverseBag() throws Exception {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> reverseBagTrackingNumbersList = new ArrayList<>();
        String pincode = LMS_PINCODE.ML_BLR;
        String tenantId = LMS_CONSTANTS.TENANTID;

        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        // Create Storebag for store.
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);


        // Create 2 orders in PK state

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);

        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount1 = orderResponse1.getOrders().get(0).getCodAmount(); // for 1st order
        String formatted_amount1 = format.format(amount1);

        // validate event creation SHIPMENT_CREATION and shipment pendency

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - SHIPMENT_CREATION  .
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // PK to RECEIVE_IN_DC

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);
        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);


        // validate event creation RECEIVE_IN_DC and shipment pendency

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // validate ledger entries for event - RECEIVE_IN_DC.

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

        // Add shipments to Masterbag

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();
        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber1, null, null, null, null, null, null, false).build();

        try {
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create a Deliverystaff .

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.

        // Create a trip with Storebag.

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(trackingNumber);
        forwardTrackingNumbersList.add(trackingNumber1);


        //Delivering the store bag to store and close the Trip
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        // Validate Event and Shipment pendency for HAND_OVER_TO_LAST_MILE_PARTNER

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateEventCreation(trackingNumber1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate ledger for HAND_OVER_TO_LAST_MILE_PARTNER
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);


        // Create a StoreTrip and Delivery the Order .
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");


        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);

        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId1, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.failedDeliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);

        // Validate Event and Shipment Pendency for FAILED_DELIVERED

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate ledger for FAILED_DELIVERED

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);


        // Now , Mark FD to LOST by Admin Correction .

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.DL, AttemptReasonCode
                .DELIVERED.toString(), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");


        PickupResponse statusResponse1 = lmsServiceHelper.forceUpdateForForward(packetId1, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Thread.sleep(5000);
        Assert.assertEquals(statusResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");

        // Validate Event and  Shipment Creation .

//
//        lmsKhataValidator.validateEventCreation(trackingNumber, CASH_RECON_EVENT_TYPE_DELIVERED_STATUS_CORRECTION, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID, "Null", "true");
//        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());

        lmsKhataValidator.validateShipmentPendency(trackingNumber1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber1, STORE.toString(), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate Ledger Entries .
//
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber);
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), trackingNumber);


        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), trackingNumber1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.GOODS.toString(), trackingNumber1);

    }



    @Test()
    public void FDtoLOST_beforeReverseBag() throws Exception {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> reverseBagTrackingNumbersList = new ArrayList<>();
        String pincode = LMS_PINCODE.ML_BLR;
        String tenantId = LMS_CONSTANTS.TENANTID;

        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        // Create Storebag for store.
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);


        // Create 2 orders in PK state

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);


        // validate event creation SHIPMENT_CREATION and shipment pendency

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // validate ledger entries for event - SHIPMENT_CREATION  .
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // PK to RECEIVE_IN_DC

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);


        // validate event creation RECEIVE_IN_DC and shipment pendency

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // validate ledger entries for event - RECEIVE_IN_DC.

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Add shipments to Masterbag

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();

        try {
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create a Deliverystaff .

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.

        // Create a trip with Storebag.

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(trackingNumber);


        //Delivering the store bag to store and close the Trip
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        // Validate Event and Shipment pendency for HAND_OVER_TO_LAST_MILE_PARTNER

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate ledger for HAND_OVER_TO_LAST_MILE_PARTNER
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Create a StoreTrip and Delivery the Order .
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");


        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);

        // Validate Event and Shipment Pendency for FAILED_DELIVERED

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate ledger for FAILED_DELIVERED

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Now , Mark LOST/RTO by Admin Correction .

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST,EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        // Validate Event and Shipment Pendency for LOST

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate ledger for LOST

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

    }



    @Test()
    public void FDtoLOSTtoRTOtoRTOLOSTtoRTOtoDL_afterReverseBag() throws Exception {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> reverseBagTrackingNumbersList = new ArrayList<>();
        String pincode = LMS_PINCODE.ML_BLR;
        String tenantId = LMS_CONSTANTS.TENANTID;

        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        // Create Storebag for store.
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);


        // Create 2 orders in PK state

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);


        // validate event creation SHIPMENT_CREATION and shipment pendency

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // validate ledger entries for event - SHIPMENT_CREATION  .
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // PK to RECEIVE_IN_DC

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);


        // validate event creation RECEIVE_IN_DC and shipment pendency

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // validate ledger entries for event - RECEIVE_IN_DC.

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Add shipments to Masterbag

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();

        try {
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create a Deliverystaff .

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.

        // Create a trip with Storebag.

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(trackingNumber);


        //Delivering the store bag to store and close the Trip
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        // Validate Event and Shipment pendency for HAND_OVER_TO_LAST_MILE_PARTNER

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate ledger for HAND_OVER_TO_LAST_MILE_PARTNER
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Create a StoreTrip and Delivery the Order .
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");


        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);

        // Validate Event and Shipment Pendency for FAILED_DELIVERED

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate ledger for FAILED_DELIVERED

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);


        // create a reverse trip .

        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();

        //Assign reverse Bag to Trip and get the reverseBag id

        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + trackingNumber + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");

        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        // Add amount to reverse bag trip .

        reverseBagTrackingNumbersList.add(trackingNumber);
        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStore(String.valueOf(reverseBagId),reverseBagTrackingNumbersList, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, reverseBagTrackingNumbersList.size(), 0, 0.00f,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Item was picked up successfully");

        // Validate Event Shipment pendency after Store paid to SDA

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, CASH_RECON_EVENT_TYPE_ADDED_TO_REVERSE_BAG, STORE.toString(), SDA1_account_id.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), "0.00","0.00" , "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount, "false", "Null","Null");

        // Validate Ledger Entry for STORE and SDA.

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);


        // Now , Mark FD - LOST by Admin Correction .

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST,EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        // Validate Event and Shipment Pendency for LOST

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), "0.00","0.00" , "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), "0.00", "0.00", formatted_amount, "false", "Null","Null");

        // Validate ledger for FD - LOST

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Now , Mark LOST - RTO by Admin Correction .

        PickupResponse statusResponse1 = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO,EnumSCM.RTO_CONFIRMED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        // RTO - RTO_DISPTACH

        TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
        long masterBag = (long) tmsServiceHelper.createcloseMBforRTO(20L, packetId, DcId);
        tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBag);

        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBag, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WH");

        Assert.assertEquals(
                lmsServiceHelper.receiveShipmentFromMasterbag(masterBag).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive shipment in DC");

        ExceptionHandler.handleTrue(lmsServiceHelper.validateOrderStatusInLMS(packetId, ShipmentStatus.RTO_IN_TRANSIT.toString(), 10));



      //  Validate Event and Shipment Pendency for RTO - RTO_DISPTACH

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.RTO_DISPATCHED.toString(), DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", "Null","Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", "Null","Null");

        //Validate Ledger entry forRTO - RTO_DISPTACH

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Now , Mark RTO_DISPATCHED - RTOLOST by Admin Correction .

        PickupResponse statusResponse2 = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTOL,EnumSCM.RTO_LOST, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        //  Validate Shipment Pendency for RTO_DISPATCHED - RTOL ( no change in pendency ).

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", "Null","Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", "Null","Null");

        //Validate Ledger entry for RTO_DISPATCHED - RTOL ( no change in pendency ).

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);

        // Now , Mark RTOL - RTO BY ADMIN correction .

        PickupResponse statusResponse3 = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO,EnumSCM.RTO_CONFIRMED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse3.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        //  Validate Shipment Pendency for RTOL - RTO ( no change in pendency ).

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", "Null","Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", "Null","Null");

        //Validate Ledger entry for  RTOL - RTO ( no change in pendency ).

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);

         //

//        // Now , Mark RTO - DL BY ADMIN correction .
//
//        PickupResponse statusResponse4 = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.DELIVERED,AttemptReasonCode.DELIVERED.toString(), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
//        Assert.assertEquals(statusResponse4.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
//        Thread.sleep(5000);
//
//        // Validate Event and Shipment Pendency is created on DC .
//
//        lmsKhataValidator.validateEventCreation(trackingNumber, CASH_RECON_EVENT_TYPE_DELIVERED_STATUS_CORRECTION, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
//        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), "0.00","0.00" , "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
//        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", "Null","Null");
//        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), formatted_amount, "0.00", "0.00", "false", "Null","Null");
//
//        //Validate Ledger entry for RTO - DL BY ADMIN correction .
//
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNumber);
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);

    }

    @Test()
    public void FDtoLOSTtoRTOtoRTOLOSTtoRTOtoDL_beforeReverseBag() throws Exception {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> reverseBagTrackingNumbersList = new ArrayList<>();
        String pincode = LMS_PINCODE.ML_BLR;
        String tenantId = LMS_CONSTANTS.TENANTID;

        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        // Create Storebag for store.
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);


        // Create 2 orders in PK state

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);


        // validate event creation SHIPMENT_CREATION and shipment pendency

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // validate ledger entries for event - SHIPMENT_CREATION  .
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // PK to RECEIVE_IN_DC

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);


        // validate event creation RECEIVE_IN_DC and shipment pendency

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // validate ledger entries for event - RECEIVE_IN_DC.

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Add shipments to Masterbag

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();

        try {
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create a Deliverystaff .

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.

        // Create a trip with Storebag.

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(trackingNumber);


        //Delivering the store bag to store and close the Trip
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        // Validate Event and Shipment pendency for HAND_OVER_TO_LAST_MILE_PARTNER

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate ledger for HAND_OVER_TO_LAST_MILE_PARTNER
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Create a StoreTrip and Delivery the Order .
        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + storeCode + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");


        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(trackingNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.failedDeliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);

        // Validate Event and Shipment Pendency for FAILED_DELIVERED

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate ledger for FAILED_DELIVERED

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);


        // Now , Mark FD - LOST by Admin Correction .

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST,EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        // Validate Event and Shipment Pendency for LOST

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate ledger for FD - LOST

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Now , Mark LOST - RTO by Admin Correction .

        PickupResponse statusResponse1 = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO,EnumSCM.RTO_CONFIRMED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        //Validate Event and Shipment Pendency for LOST - RTO

//        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.RTO_CONFIRMED.toString(), DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
//        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), "0.00","0.00" , "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
//        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount, "false", "Null","Null");
//
//        //Validate Ledger entry for LOST - RTO
//
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Now , Mark RTO - RTOLOST by Admin Correction .

        PickupResponse statusResponse2 = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTOL,EnumSCM.RTO_LOST, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse2.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        //  Validate Shipment Pendency for RTO - RTOL ( no change in pendency ).

        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //Validate Ledger entry for  RTO - RTOL ( no change in pendency ).

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);


        // Now , Mark RTOL - RTO BY ADMIN correction .

        PickupResponse statusResponse3 = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO,EnumSCM.RTO_CONFIRMED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse3.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        //  Validate Shipment Pendency for RTOL - RTO ( no change in pendency ).

        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //Validate Ledger entry for  RTOL - RTO ( no change in pendency ).

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);


        // Now , Mark RTO - DL BY ADMIN correction .

        PickupResponse statusResponse4 = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.DL,AttemptReasonCode.DELIVERED.toString(), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse4.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        // Validate Event and Shipment Pendency is created on DC .

        lmsKhataValidator.validateEventCreation(trackingNumber, CASH_RECON_EVENT_TYPE_DELIVERED_STATUS_CORRECTION, DC.toString(), INSTAKART.toString(), "true", "CASH", formatted_amount, formatted_amount, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(STORE), formatted_amount,"0.00" , "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        //Validate Ledger entry for RTO - DL BY ADMIN correction .

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNumber);

    }



    @Test()
    public void HANDEDTOLASTMILEtoLOSTtoDL() throws Exception {


        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + storeCode;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> reverseBagTrackingNumbersList = new ArrayList<>();
        String pincode = LMS_PINCODE.ML_BLR;
        String tenantId = LMS_CONSTANTS.TENANTID;

        log.info("The store created is : " + storeHlPId + " and the code is " + storeCode);
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeCode);

        // Create Storebag for store.
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeCode + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeCode + " is : " + masterBagId);


        // Create 2 orders in PK state

        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format = new DecimalFormat("0.00");
        String formatted_amount = format.format(amount);


        // validate event creation SHIPMENT_CREATION and shipment pendency

        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // validate ledger entries for event - SHIPMENT_CREATION  .
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // PK to RECEIVE_IN_DC

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);


        // validate event creation RECEIVE_IN_DC and shipment pendency

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // validate ledger entries for event - RECEIVE_IN_DC.

        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Add shipments to Masterbag

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, trackingNumber, null, null, null, null, null, null, false).build();

        try {
            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create a Deliverystaff .

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.

        // Create a trip with Storebag.

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 1, storeCode, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(trackingNumber);


        //Delivering the store bag to store and close the Trip
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        // Validate Event and Shipment pendency for HAND_OVER_TO_LAST_MILE_PARTNER

        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(trackingNumber, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        // Validate ledger for HAND_OVER_TO_LAST_MILE_PARTNER
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Now , Mark FD - LOST by Admin Correction .

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST,EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        // Validate Event and Shipment Pendency for LOST

        lmsKhataValidator.validateShipmentPendency(trackingNumber, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), "0.00", "0.00", formatted_amount, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate ledger for HAND_OVER_TO_LAST_MILE_PARTNER - LOST

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.GOODS.toString(), trackingNumber);

        // Now , Mark LOST to DL by Admin Correction .

        PickupResponse statusResponse1 = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.DL,EnumSCM.DELIVERED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        // Validate Event and Shipment Pendency for DELIVERED ( LOST to DL by Admin Correction .)

        lmsKhataValidator.validateEventCreation(trackingNumber, CASH_RECON_EVENT_TYPE_DELIVERED_STATUS_CORRECTION, DC.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount, formatted_amount, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(trackingNumber, STORE.toString(), formatted_amount, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate ledger for LOST - DL .

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount), LedgerType.GOODS.toString(), trackingNumber);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount), LedgerType.CASH.toString(), trackingNumber);
        

    }




}

