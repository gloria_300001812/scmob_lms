package com.myntra.apiTests.erpservices.CashRecon.tests;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.khata.helpers.KhataServiceHelper;
import com.myntra.apiTests.erpservices.khata.validator.KhataAccountValidator;
import com.myntra.apiTests.erpservices.khata.validator.LedgerValidator;
import com.myntra.apiTests.erpservices.khata.validator.PaymentValidator;
import com.myntra.apiTests.erpservices.lastmile.client.*;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.LastmileHelper;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.*;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.lms.tests.LastMileTestDP;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.lms_khata.validator.LMSKhataValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MLShipmentClientV2;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.khata.enums.AccountType;
import com.myntra.khata.enums.LedgerType;
import com.myntra.khata.enums.PaymentStatus;
import com.myntra.khata.enums.PaymentType;
import com.myntra.khata.response.AccountPendencyResponse;
import com.myntra.khata.response.AccountResponse;
import com.myntra.khata.response.ExcessResponse;
import com.myntra.khata.response.PaymentResponse;
import com.myntra.lastmile.client.code.AttemptReasonCode;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lastmile.client.entry.MLShipmentResponse;
import com.myntra.lastmile.client.response.StoreResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lastmile.client.response.TripShipmentAssociationResponse;
import com.myntra.lastmile.client.status.StoreType;
import com.myntra.lastmile.client.status.TripOrderStatus;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.PickupResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.lms.khata.domain.EventType;
import com.myntra.lms.khata.domain.PaymentMode;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateInfo;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.*;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_ADDED_TO_REVERSE_BAG;
import static com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_EVENT_TYPE_PAYMENT;

public class Payment_Store {

    String env = getEnvironment();
    LastmileHelper lastmileHelper = new LastmileHelper(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient deliveryCenterClient = new DeliveryCenterClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    DeliveryStaffClient deliveryStaffClient = new DeliveryStaffClient(env, LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.TENANT_ID);
    StoreClient storeClient = new StoreClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MasterBagClient masterBagClient = new MasterBagClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    MLShipmentClientV2 mlShipmentServiceV2Client = new MLShipmentClientV2(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    DeliveryCenterClient_QA deliveryCenterClient_qa=new DeliveryCenterClient_QA();
    DeliveryStaffClient_QA deliveryStaffClient_qa=new DeliveryStaffClient_QA();
    StoreClient_QA storeClient_qa=new StoreClient_QA();
    MasterBagClient_QA masterBagClient_qa=new MasterBagClient_QA();
    MLShipmentClientV2_QA mlShipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    MLShipmentClientV2_QA mlshipmentServiceV2Client_qa=new MLShipmentClientV2_QA();
    StatusPollingValidator statusPollingValidator=new StatusPollingValidator();




    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    TripOrderAssignmentClient tripOrderAssignmentClient = new TripOrderAssignmentClient(env, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private static Logger log = Logger.getLogger(Payment_Store.class);
    HashMap<String, String> pincodeDCMap;
    String deliveryStaffID1, deliveryStaffID2, DCId = "1";
    PaymentValidator paymentValidator =new PaymentValidator();
    LedgerValidator ledgerValidator = new LedgerValidator();
    LMSKhataValidator lmsKhataValidator = new LMSKhataValidator();
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    TripClient_QA tripClient_qa=new TripClient_QA();
    StoreHelper storeHelper = new StoreHelper(env,LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);


    //DATA for FD_DL to be used for completing the trip
    String packetId, packetId1, trackingNumber, trackingNumber1;
    long tripId;
    List<Map<String, Object>> tripData = new ArrayList<>();
    Map<String, Object> dataMap1 = new HashMap<>();
    Map<String, Object> dataMap2 = new HashMap<>();
    KhataServiceHelper khataHelper = new KhataServiceHelper();
    Long SDA1_account_id, SDA2_account_id;

    //TODO : Account ID's
    Long INSTAKART, MYNTRA, DC ;

    @BeforeClass(alwaysRun = true)
    public void pincodeDcMapping() throws IOException, JAXBException, InterruptedException {
        pincodeDCMap = new HashMap<>();
        pincodeDCMap.put(LMS_PINCODE.NORTH_CGH, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.NORTH_CGH, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.CASH_RECON_ML_BLR, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.CASH_RECON_ML_BLR, LMSConstants.DEFAULT_TENANT_ID)));
        pincodeDCMap.put(LMS_PINCODE.HSR_ML, Long.toString(deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.HSR_ML, LMSConstants.DEFAULT_TENANT_ID)));
        DCId = pincodeDCMap.get(LMS_PINCODE.CASH_RECON_ML_BLR);
        //TODO : uncomment as account is not getting created so using an existing 1
        deliveryStaffID1 = ""+ lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DCId));
        deliveryStaffID2 = ""+ lmsServiceHelper.addDeliveryStaffID(Long.parseLong(DCId));

        Thread.sleep(7000); // waiting for SDA account to be created in khata service
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId();
        AccountResponse accountResponse2 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID2), AccountType.SDA);
        SDA2_account_id = accountResponse2.getAccounts().get(0).getId();

        AccountResponse accountResponse3 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_INSTAKART_ID), AccountType.PARTNER);
        INSTAKART = accountResponse3.getAccounts().get(0).getId();
        AccountResponse accountResponse4 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(LASTMILE_CONSTANTS.CASH_RECON_CONSTANTS.CASH_RECON_MYNTRA_ID), AccountType.PARTNER);
        MYNTRA = accountResponse4.getAccounts().get(0).getId();
        AccountResponse accountResponse5 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(DCId), AccountType.DC);
        DC = accountResponse5.getAccounts().get(0).getId();
    }



    // DL store bag-> Add amount to reverse bag -> HAndover In DC
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentStoretoSDA_equal(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                       String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);
        Thread.sleep(5000);


        // validate Event and shipment pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");



                        //validation pendency for IK to myntra
                    KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
                    //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);



        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String tracking_num1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount2 = format2.format(amount1);
        Thread.sleep(5000);


        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");



                  //validation pendency for IK to myntra
                  //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));

        //validation of ledger pendency
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);

        Thread.sleep(5000);

        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


                //        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
                //        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));


        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);

        Thread.sleep(5000);

        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


     // Adding 2 orders to MB

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num1, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();


        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        //TODO : THIS code is not yet written by dev so commenting it out for now.
        // once store bag is OFD  -> pendency should move from DC to SDA.
        //account pendency DC to instakart will be balanced
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1 ));
//
//        //validate ledger
//        //IK to Myntra will be balanced and new ledger from sda to IK will be created
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        forwardTrackingNumbersList.add(tracking_num);
        forwardTrackingNumbersList.add(tracking_num1);


        //before delivering the store bag to store collect  capture the current pendency of the store to Instakart
        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(Store_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        //Delivering the store bag to store
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        //once store bag is delivered to store pendency will move from SDA to store .
        //validate account pendency which will move to store

        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        // validate Event , Shipment pendency and ledger for tracking number 2



        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


             //balancing the account pendency of DC
                //khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
                //khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));




        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlShipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        //after failing the delivery in store nothing should change
        Thread.sleep(5000);
                        //balancing the account pendency of DC
                        //khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
                        //khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));

        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);

        long tripOrderAssignmentId2 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();

        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);


        Thread.sleep(8000);


        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount2, formatted_amount2, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), formatted_amount2, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num1);



        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickip
        // Pickup the reverse bag


        float amount_collected = amount1.floatValue()+amount2.floatValue();
        String formatted_amount_collected = format2.format(amount_collected);



        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,amount_collected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        Thread.sleep(5000);


       // lmsKhataValidator.validateEventCreation(tracking_num, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA.toString(), "true", "Null", null, formatted_amount1, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
       // lmsKhataValidator.validateEventCreation(tracking_num1, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA.toString(), "true", "Null", null, formatted_amount1, "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");

        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", formatted_amount2, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate ledger store penedency will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount2), LedgerType.CASH.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num1);




        //Once reverese bag is picked up from store pendency should move from Store to SDA
        //account pendency balancing the store
        //balancing the account pendency of DC
        //khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE +amount1 ));
        // TODO : once code is pushed from dev side we need to validate this event as well
        //khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA +amount1 ));



        // Amount SDA to DC

        //Paying RS 200 for the SDA
        Float sda_payment_1 = amount1.floatValue();
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);

        PaymentResponse paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = "PAYMENT_"+paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_payment_1.toString(), PaymentStatus.APPROVED.toString());


    }


    // DL store bag-> Add less amount to reverse bag -> HAndover In DC
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentStoretoSDA_less(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                       String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);
        Thread.sleep(5000);


        // validate Event and shipment pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");



        //validation pendency for IK to myntra
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);



        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String tracking_num1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount2 = format2.format(amount1);
        Thread.sleep(5000);


        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");



        //validation pendency for IK to myntra
        //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));

        //validation of ledger pendency
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);

        Thread.sleep(5000);

        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
        //        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));


        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);

        Thread.sleep(5000);

        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        // Adding 2 orders to MB

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num1, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();


        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        //TODO : THIS code is not yet written by dev so commenting it out for now.
        // once store bag is OFD  -> pendency should move from DC to SDA.
        //account pendency DC to instakart will be balanced
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1 ));
//
//        //validate ledger
//        //IK to Myntra will be balanced and new ledger from sda to IK will be created
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        forwardTrackingNumbersList.add(tracking_num);
        forwardTrackingNumbersList.add(tracking_num1);


        //before delivering the store bag to store collect  capture the current pendency of the store to Instakart
        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(Store_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        //Delivering the store bag to store
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        //once store bag is delivered to store pendency will move from SDA to store .
        //validate account pendency which will move to store

        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        // validate Event , Shipment pendency and ledger for tracking number 2



        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        //balancing the account pendency of DC
        //khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
        //khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));




        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        //after failing the delivery in store nothing should change
        Thread.sleep(5000);
        //balancing the account pendency of DC
        //khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
        //khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));

        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);

        long tripOrderAssignmentId2 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();

        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);


        Thread.sleep(8000);


        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount2, formatted_amount2, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), formatted_amount2, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num1);



        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickip
        // Pickup the reverse bag

        float amount_pending = 100.00f;
        String formatted_amount_pending = format2.format(amount_pending);
        float amount_collected = amount1.floatValue()+amount2.floatValue()-amount_pending;
        String formatted_amount_collected = format2.format(amount_collected);
        float amount_collected_balance = amount_collected - amount2.floatValue() ;
        String formatted_amount_balance = format2.format(amount_collected_balance);




        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,amount_collected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        Thread.sleep(5000);



//        lmsKhataValidator.validateEventCreation(tracking_num1, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA.toString(), "true", "Null", null, "Null", formatted_amount1, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
//        lmsKhataValidator.validateEventCreation(tracking_num, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA.toString(), "true", "Null", null,formatted_amount_pending, formatted_amount_balance, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");



        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount_pending, formatted_amount_balance, "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate ledger store penedency will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num1);

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount_collected_balance), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount_collected_balance), LedgerType.CASH.toString(), tracking_num);



        // Amount SDA to DC

        //Paying RS 200 for the SDA
        Float sda_payment_1 = amount_collected;
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);

        PaymentResponse paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = "PAYMENT_"+paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_payment_1.toString(), PaymentStatus.APPROVED.toString());


    }




    // DL store bag-> Add less amount to reverse bag -> HAndover In DC
    @Test(groups = {"Trip", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void StoreReverseBagwithPaymentandShipment(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                       String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        List<String> FDTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);
        Thread.sleep(5000);


        // validate Event and shipment pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");



        //validation pendency for IK to myntra
        KhataAccountValidator khataAccountValidator = new KhataAccountValidator();
        //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));

        //validation of ledger pendency
        LedgerValidator ledgerValidator = new LedgerValidator();
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);



        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String tracking_num1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount2 = format2.format(amount1);
        Thread.sleep(5000);


        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.SHIPMENT_CREATION.toString(), MYNTRA.toString(), INSTAKART.toString(), "true", PaymentMode.CASH.toString(), formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");



        //validation pendency for IK to myntra
        //khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods + amount1));

        //validation of ledger pendency
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        //getting current pendency of dc to instakart
        AccountPendencyResponse accountPendencyResponse1 = khataHelper.getAccountPendencyByaccount(DC, INSTAKART); //to get current pendency
        if (accountPendencyResponse1.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getCash().toString());
            goods_DC = Double.parseDouble(accountPendencyResponse1.getAccountPendency().get(0).getGoods().toString());
        }

        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId, packetId);

        Thread.sleep(5000);

        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //        khataAccountValidator.validateAccountPendency(Long.toString(INSTAKART), Long.toString(MYNTRA), String.valueOf(cash), String.valueOf(goods));
        //        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC + amount1 ));


        lmsHelper.processOrderInSCM(EnumSCM.RECEIVE_IN_DC, EnumSCM.COURIER_CODE_ML, orderReleaseId1, packetId1);

        Thread.sleep(5000);

        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        // Adding 2 orders to MB

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num1, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();


        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        //TODO : THIS code is not yet written by dev so commenting it out for now.
        // once store bag is OFD  -> pendency should move from DC to SDA.
        //account pendency DC to instakart will be balanced
//        khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
//        khataAccountValidator.validateAccountPendency(Long.toString(SDA1_account_id), Long.toString(INSTAKART), String.valueOf(cash_SDA), String.valueOf(goods_SDA + amount1 ));
//
//        //validate ledger
//        //IK to Myntra will be balanced and new ledger from sda to IK will be created
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        forwardTrackingNumbersList.add(tracking_num);
        forwardTrackingNumbersList.add(tracking_num1);


        //before delivering the store bag to store collect  capture the current pendency of the store to Instakart
        accountPendencyResponse = khataHelper.getAccountPendencyByaccount(Store_account_id, INSTAKART); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods_STORE = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        //Delivering the store bag to store
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        //once store bag is delivered to store pendency will move from SDA to store .
        //validate account pendency which will move to store

        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        // validate Event , Shipment pendency and ledger for tracking number 2



        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        //balancing the account pendency of DC
        //khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
        //khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));




        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        //after failing the delivery in store nothing should change
        Thread.sleep(5000);
        //balancing the account pendency of DC
        //khataAccountValidator.validateAccountPendency(Long.toString(DC), Long.toString(INSTAKART), String.valueOf(cash_DC), String.valueOf(goods_DC));
        //khataAccountValidator.validateAccountPendency(Long.toString(Store_account_id), Long.toString(INSTAKART), String.valueOf(cash_STORE), String.valueOf(goods_STORE + amount1));

        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);

        long tripOrderAssignmentId2 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();

        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.failedDeliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId,LASTMILE_CONSTANTS.TENANT_ID);


        Thread.sleep(8000);


        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);



        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n\n\n**************************************************\n\n\nThe Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " ,and storeTenantid : " + storeTenantId + " ,for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + " \nAnd marking the shipment with tracking number : " + forwardTrackingNumbersList.get(0) + " as Delivered and the shipment with tracking number :  " + tracking_num + " , as FailedDelivery " + "\n\n\n**************************************************\n\n\n");
        System.out.println("\n____________The Myntra SDA trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickip
        // Pickup the reverse bag

        float amount_pending = 100.00f;
        String formatted_amount_pending = format2.format(amount_pending);
        float amount_collected = amount1.floatValue()-amount_pending;
        String formatted_amount_collected = format2.format(amount_collected);



        FDTrackingNumbersList.add(tracking_num1);

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStore(String.valueOf(reverseBagId),FDTrackingNumbersList, String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,amount_collected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        Thread.sleep(5000);



        lmsKhataValidator.validateEventCreation(tracking_num1, CASH_RECON_EVENT_TYPE_ADDED_TO_REVERSE_BAG, STORE.toString(), SDA.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
       // lmsKhataValidator.validateEventCreation(tracking_num, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA.toString(), "true", "Null", null,formatted_amount_pending, formatted_amount_collected, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");



        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
       lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(SDA), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount_pending, formatted_amount_collected, "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");


        //validate ledger store penedency will be balanced.
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount_collected), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount_collected), LedgerType.CASH.toString(), tracking_num);

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num1);


        // Amount SDA to DC

        //Paying RS 200 for the SDA
        Float sda_payment_1 = amount_collected;
        PaymentResponse response =  khataHelper.createAndApprove(DC, SDA1_account_id, sda_payment_1, PaymentType.CASH);
        Assert.assertEquals(response.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Payment unsuccessful for SDA acnt ID:" + SDA1_account_id);

        PaymentResponse paymentResponse =  khataHelper.getPaymentDetailsFromAccountToAccount(SDA1_account_id.toString(),DC.toString());
        //id form payment is used in events table as source reference id .
        String payment_id_1 = "PAYMENT_"+paymentResponse.getPayments().get(paymentResponse.getPayments().size()-1).getId().toString();
        paymentValidator.paymentValidation(SDA1_account_id.toString(), DC.toString(), sda_payment_1.toString(), PaymentStatus.APPROVED.toString());

        //TODO : Event creation for payment is_processed is 0 but it's processed need to comment out to proceed further

        //validate event, payment event will be created

    }






    // DL store bag-> Add less amount to reverse bag -> HAndover In DC
    @Test(groups = {"Excess", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentStoretoSDA_excessbyTrigger(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                       String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);


        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);
        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        // Adding 1 orders to MB

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");


        forwardTrackingNumbersList.add(tracking_num);


        //Delivering the store bag to store
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);

        // Create a Reverse bag to make excess payment :


        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        float excess_amount = amount1.floatValue();
        String formatted_excess_amount = format1.format(excess_amount);

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,excess_amount,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");



        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        // statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignment =statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber,storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(5000);
        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);

        // Todo : Trigger API .
        ExcessResponse excessResponse = khataHelper.pushExcess(STORE,SDA);
        Thread.sleep(5000);

        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", formatted_excess_amount, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA), formatted_excess_amount,"0.00" , "0.00", "false", "Null", INSTAKART.toString());
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


    }



    // DL store bag-> Add less amount to reverse bag -> HAndover In DC
    @Test(groups = {"Excess", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentStoretoSDA_excessbypayment(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                            String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);


        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);
        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        // Adding 1 orders to MB

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");


        forwardTrackingNumbersList.add(tracking_num);


        //Delivering the store bag to store
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);

        // Create a Reverse bag to make excess payment :


        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        float excess_amount = amount1.floatValue()-100.00f;
        String formatted_excess_amount = format1.format(excess_amount);

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,excess_amount,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");
        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(reverseTripId)).equalsIgnoreCase(EnumSCM.SUCCESS));


        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        // statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignment =statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber,storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Thread.sleep(5000);
        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        TripResponse reverseTrip1 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId1 = reverseTrip1.getTrips().get(0).getId();
        String reverseTripNumber1 = reverseTrip1.getTrips().get(0).getTripNumber();
        String reverseBagId1 = tripClient_qa.assignReverseBagToTrip(reverseTripId1, storeHlPId, LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber1, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId1);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber1, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        TripShipmentAssociationResponse reverseShipmentResponse1 = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId1), String.valueOf(reverseTripId1), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,100.00f,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber1, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");
        Thread.sleep(5000);

        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA), formatted_amount1,"0.00" , "0.00", "false", "Null", INSTAKART.toString());

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(100.00f), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-100.00f), LedgerType.CASH.toString(), tracking_num);


    }




    // DL store bag-> Add less amount to reverse bag -> HAndover In DC
    @Test(groups = { "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentStoretoSDA_equaltoexcessEOD(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                            String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {

        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();
        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));



        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        //Assign reverse Bag to Trip and get the reverseBag id
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId + " and the reverse Trip is " + reverseTripNumber + "  , trip id " + reverseTripId + "\n\n\n");


        //wfd
        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        //ReverseBag is now In Transit
        // reverse bag should be IN_TRANSIT after pickip
        // Pickup the reverse bag

        float amount = 500.00f;

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,amount,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");
        log.info(reverseShipmentResponse.getTripShipmentAssociationEntryList().get(0).getAmountCollected());
        log.info(reverseShipmentResponse.getTripShipmentAssociationEntryList().get(0).getAmountToBeCollected());


        // create an order an assign to Store and deliver it .



        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);
        Thread.sleep(5000);

        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        // Adding 2 orders to MB
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }


        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");

        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        Thread.sleep(3000);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);
        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);
        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");
        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);
        Thread.sleep(5000);


        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        //  Do payment for 599.00f



        TripResponse reverseTrip1 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Long reverseTripId1 = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber1 = reverseTrip.getTrips().get(0).getTripNumber();
        String reverseBagId1 = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        System.out.println("\n\nThe reverse bag id is : " + reverseBagId1 + " and the reverse Trip is " + reverseTripNumber1 + "  , trip id " + reverseTripId1 + "\n\n\n");
        tripClient_qa.startTrip(reverseTripId1);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber1, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        float payment = amount1.floatValue()-amount;

        TripShipmentAssociationResponse reverseShipmentResponse1 = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId1), String.valueOf(reverseTripId1), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,payment,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber1, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");
        log.info(reverseShipmentResponse.getTripShipmentAssociationEntryList().get(0).getAmountCollected());
        log.info(reverseShipmentResponse.getTripShipmentAssociationEntryList().get(0).getAmountToBeCollected());


        // Todo : check for Excess payment is settled. and shipment pendency , event , ledger.


    }


   // Test Case 3: SDA DL item 100 -> pays 200 (100 excess) -> DL item worth 200 -> pendency =100 ,pay 200 ->  prev excess should be removed and new excess of rs 100 should be made.

    @Test(groups = {"Excess", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentStoretoSDA3(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                  String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);


        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String tracking_num1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount2 = format2.format(amount2);
        Thread.sleep(5000);

//validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        // Adding 2 orders to MB

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num1, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();


        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);
        forwardTrackingNumbersList.add(tracking_num1);


        //Delivering the store bag to store
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        //once store bag is delivered to store pendency will move from SDA to store .
        //validate account pendency which will move to store

        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);

        // validate Event , Shipment pendency and ledger for tracking number 2



        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);



        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(5000);

        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);


        float excess_amount = 100.00f;
        String formatted_excess_amount = format2.format(excess_amount);
        float amount_collected = amount1.floatValue()+excess_amount;
        String formatted_amount_collected = format2.format(amount_collected);

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,amount_collected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        Thread.sleep(5000);
        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(reverseTripId)).equalsIgnoreCase(EnumSCM.SUCCESS));


        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA), formatted_amount1,"0.00" , "0.00", "false", "Null", INSTAKART.toString());

        //validate ledger store penedency will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);




        // deliver the second order

        long tripOrderAssignmentId2 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();

        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(8000);

        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount2, formatted_amount2, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), formatted_amount2, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num1);


        // Do paymnet



        TripResponse reverseTrip1 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId1 = reverseTrip1.getTrips().get(0).getId();
        String reverseTripNumber1 = reverseTrip1.getTrips().get(0).getTripNumber();
        String reverseBagId1 = tripClient_qa.assignReverseBagToTrip(reverseTripId1, storeHlPId, LMS_CONSTANTS.TENANTID);
        tripClient_qa.startTrip(reverseTripId1);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber1, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        float payment = amount2.floatValue();

        TripShipmentAssociationResponse reverseShipmentResponse1 = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId1), String.valueOf(reverseTripId1), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,payment,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber1, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");
        log.info(reverseShipmentResponse.getTripShipmentAssociationEntryList().get(0).getAmountCollected());
        log.info(reverseShipmentResponse.getTripShipmentAssociationEntryList().get(0).getAmountToBeCollected());

        Thread.sleep(5000);

        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", formatted_amount2, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(SDA), formatted_amount2,"0.00" , "0.00", "false", "Null", INSTAKART.toString());
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount2), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num);



        // Todo : check for Excess payment is settled. and shipment pendency , event , ledger.

        // Todo : check for  new Excess payment is created for 1099.0
        // Todo : Check for shipment pendency , event , ledger

    }





    // SDA -> DL(Rs 1000) -> PAID 900  -> FD -> process till DLS. same SDA deliver another item pay 100 all balanced for sda and and after dls pendency should move to INSTAKART.
    @Test(groups = {"Excess", "Admin"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentStoretoSDA4(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                       String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);


        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String tracking_num1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount2 = format2.format(amount2);
        Thread.sleep(5000);

//validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        // Adding 2 orders to MB

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num1, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();


        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);
        forwardTrackingNumbersList.add(tracking_num1);


        //Delivering the store bag to store
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        //once store bag is delivered to store pendency will move from SDA to store .
        //validate account pendency which will move to store

        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);

        // validate Event , Shipment pendency and ledger for tracking number 2



        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);



        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(5000);

        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);


        float excess_amount = 100.00f;
        String formatted_excess_amount = format2.format(excess_amount);
        float amount_collected = amount1.floatValue()+excess_amount;
        String formatted_amount_collected = format2.format(amount_collected);

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,amount_collected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        Thread.sleep(5000);
        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(reverseTripId)).equalsIgnoreCase(EnumSCM.SUCCESS));


        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA), formatted_amount1,"0.00" , "0.00", "false", "Null", INSTAKART.toString());

        //validate ledger store penedency will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        // deliver the second order

        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(8000);

        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount2, formatted_amount2, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), formatted_amount2, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num1);

        // mark DL order to FD by admin correction.

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.FD,AttemptReasonCode
                .INCOMPLETE_INCORRECT_ADDRESS.toString(), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        lmsKhataValidator.validateEventCreation(tracking_num, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA1_account_id.toString(), "true", "Null", "Null", formatted_amount1, "Null", "Null", "Null", "Null", "Null");
        lmsKhataValidator.validateEventCreation(tracking_num, EnumSCM.FAILED_DELIVERY, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID, "Null", "true");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate Ledger Entries .
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);

        // push excess :

        khataHelper.pushExcess(STORE,SDA1_account_id);


        Thread.sleep(5000);

        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", formatted_amount2, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(SDA), formatted_amount2,"0.00" , "0.00", "false", "Null", INSTAKART.toString());
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount2), LedgerType.CASH.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num1);


    }




    // SDA -> DL(Rs 1000) -> PAID 900  -> FD -> process till DLS. same SDA deliver another item pay 100 all balanced for sda and and after dls pendency should move to INSTAKART.
    @Test(groups = {"Excess", "Admin"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentStoretoSDA5(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                   String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);


        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String tracking_num1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount2 = format2.format(amount2);
        Thread.sleep(5000);

//validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        // Adding 2 orders to MB

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num1, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();


        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);
        forwardTrackingNumbersList.add(tracking_num1);


        //Delivering the store bag to store
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        //once store bag is delivered to store pendency will move from SDA to store .
        //validate account pendency which will move to store

        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);

        // validate Event , Shipment pendency and ledger for tracking number 2



        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);



        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(5000);

        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);


        float excess_amount = 100.00f;
        String formatted_excess_amount = format2.format(excess_amount);
        float amount_collected = amount1.floatValue()+excess_amount;
        String formatted_amount_collected = format2.format(amount_collected);

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,amount_collected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        Thread.sleep(5000);
        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(reverseTripId)).equalsIgnoreCase(EnumSCM.SUCCESS));


        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA), formatted_amount1,"0.00" , "0.00", "false", "Null", INSTAKART.toString());

        //validate ledger store penedency will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);

        // SDA pays amount to DC . (999.00f)

        float sdapayment = amount_collected - 200.00f;
        String formatted_sdapayment = format2.format(sdapayment);
        PaymentResponse SDApayment = khataHelper.createAndApprove(DC, SDA1_account_id, sdapayment, PaymentType.CASH);
        Assert.assertEquals(SDApayment.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "creation of payment for wrong DC and Correct SDA should not be allowed" + " DC and SDA Account ID:" + DC.toString() + " " + SDA1_account_id);


        // deliver the second order

        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(8000);

        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount2, formatted_amount2, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), formatted_amount2, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num1);

        // mark DL order to FD by admin correction.

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.FD,AttemptReasonCode
                .INCOMPLETE_INCORRECT_ADDRESS.toString(), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

        lmsKhataValidator.validateEventCreation(tracking_num, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA1_account_id.toString(), "true", "Null", "Null", formatted_amount1, "Null", "Null", "Null", "Null", "Null");
        lmsKhataValidator.validateEventCreation(tracking_num, EnumSCM.FAILED_DELIVERY, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID, "Null", "true");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate Ledger Entries .
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(100.00f), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", formatted_amount2, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(SDA1_account_id), "100.00", "999.00", "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "999.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");

        // Validate Ledger Entries .
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount2), LedgerType.CASH.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(999.00f), LedgerType.CASH.toString(), tracking_num1);
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-1099.00f), LedgerType.GOODS.toString(), tracking_num1);


    }





    // SDA -> DL(Rs 1000) -> PAID 900  -> FD -> process till DLS. same SDA deliver another item pay 100 all balanced for sda and and after dls pendency should move to INSTAKART.
    @Test(groups = {"Excess", "Admin"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentStorewithExcess_DLtoLOST(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                   String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);


        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String tracking_num1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount2 = format2.format(amount2);
        Thread.sleep(5000);

//validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        // Adding 2 orders to MB

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num1, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();


        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);
        forwardTrackingNumbersList.add(tracking_num1);


        //Delivering the store bag to store
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        //once store bag is delivered to store pendency will move from SDA to store .
        //validate account pendency which will move to store

        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);

        // validate Event , Shipment pendency and ledger for tracking number 2



        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);



        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(5000);

        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);


        float excess_amount = 100.00f;
        String formatted_excess_amount = format2.format(excess_amount);
        float amount_collected = amount1.floatValue()+excess_amount;
        String formatted_amount_collected = format2.format(amount_collected);

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,amount_collected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        Thread.sleep(5000);
        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(reverseTripId)).equalsIgnoreCase(EnumSCM.SUCCESS));


        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA), formatted_amount1,"0.00" , "0.00", "false", "Null", INSTAKART.toString());

        //validate ledger store penedency will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        // deliver the second order

        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(8000);

        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount2, formatted_amount2, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), formatted_amount2, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num1);

        // mark DL order to FD by admin correction.

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST,EnumSCM.LOST_IN_TRANSIT.toString(), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");
        Thread.sleep(5000);

   //     lmsKhataValidator.validateEventCreation(tracking_num, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA1_account_id.toString(), "true", "Null", "Null", String.valueOf(amount1), "Null", "Null", "Null", "Null", "false");
        lmsKhataValidator.validateEventCreation(tracking_num, EnumSCM.LOST, DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID, "Null", "true");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


//        lmsKhataValidator.validateEventCreation(tracking_num1, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA1_account_id.toString(), "true", "Null", "Null", String.valueOf(-amount1), "Null", "Null", "Null", "Null", "false");

        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", formatted_amount2, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(SDA), formatted_amount2,"0.00" , "0.00", "false", "Null", INSTAKART.toString());
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount2), LedgerType.CASH.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num1);


    }




    // SDA -> DL(Rs 1000) -> PAID 900  -> FD -> process till DLS. same SDA deliver another item pay 100 all balanced for sda and and after dls pendency should move to INSTAKART.
    @Test(groups = {"Excess", "Admin"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentStorewithExcess_DLtoRTO(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();

        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);

        //get current account pendency of IK to Myntra
        AccountPendencyResponse accountPendencyResponse = khataHelper.getAccountPendencyByaccount(INSTAKART, MYNTRA); //to get current pendency
        if (accountPendencyResponse.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getCash().toString());
            goods = Double.parseDouble(accountPendencyResponse.getAccountPendency().get(0).getGoods().toString());
        }


        String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);


        String orderId1 = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        String tracking_num1 = lmsHelper.getTrackingNumber(packetId1);
        String orderReleaseId1 = omsServiceHelper.getReleaseId(orderId1);// 1st order
        //amount
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetails(packetId1);
        Double amount2 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format2 = new DecimalFormat("0.00");
        String formatted_amount2 = format2.format(amount2);
        Thread.sleep(5000);

//validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        //validate Event , shipment and Ledger pendency creation

        lmsKhataValidator.validateEventCreation(tracking_num1, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);


        // Adding 2 orders to MB

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ShipmentUpdateInfo shipmentUpdateInfo1 = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num1, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();


        // getting account of SDA
        accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        SDA1_account_id = accountResponse1.getAccounts().get(0).getId(); // SDA will be delivering the store bag to store.
        //getting current pendency of sda to instakart
        AccountPendencyResponse accountPendencyResponse2 = khataHelper.getAccountPendencyByaccount(SDA1_account_id,INSTAKART); //to get current pendency
        if (accountPendencyResponse2.getStatus().getStatusType().toString() != EnumSCM.WARNING) {
            cash_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getCash().toString());
            goods_SDA = Double.parseDouble(accountPendencyResponse2.getAccountPendency().get(0).getGoods().toString());
        }


        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        lastmileHelper.validateStoreBagAddedToTrip(masterBagId, 2, code, originPremiseId);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");

        forwardTrackingNumbersList.add(tracking_num);
        forwardTrackingNumbersList.add(tracking_num1);


        //Delivering the store bag to store
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        //TODO : response object returns tripId null
        // Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        Assert.assertTrue(shipmentResponse.getStatus().getTotalCount() == 1);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        //once store bag is delivered to store pendency will move from SDA to store .
        //validate account pendency which will move to store

        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);

        // validate Event , Shipment pendency and ledger for tracking number 2



        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount2, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", "0.00", formatted_amount2, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount2), LedgerType.GOODS.toString(), tracking_num1);



        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        TripOrderAssignmentResponse tripOrderAssignment = statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);


        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(5000);

        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));


        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        //wfd
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);


        float excess_amount = 100.00f;
        String formatted_excess_amount = format2.format(excess_amount);
        float amount_collected = amount1.floatValue()+excess_amount;
        String formatted_amount_collected = format2.format(amount_collected);

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,amount_collected,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");

        Thread.sleep(5000);
        Assert.assertTrue(lmsServiceHelper.newCompleteTrip(String.valueOf(reverseTripId)).equalsIgnoreCase(EnumSCM.SUCCESS));


        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA), formatted_amount1,"0.00" , "0.00", "false", "Null", INSTAKART.toString());

        //validate ledger store penedency will be balanced
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        // deliver the second order

        long tripOrderAssignmentId1 = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(1))).findFirst().get().getId();
        MLShipmentResponse mlShipmentResponse1 = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num1, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse3 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId1, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(8000);

        lmsKhataValidator.validateEventCreation(tracking_num1, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount2, formatted_amount2, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), formatted_amount2, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount2), LedgerType.GOODS.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num1);

        // mark DL order to FD by admin correction.

        PickupResponse statusResponse = lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO,EnumSCM.RTO_CONFIRMED.toString(), LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        Assert.assertEquals(statusResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Mismatch");


        // RTO - RTO_DISPTACH

        TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
        long masterBag = (long) tmsServiceHelper.createcloseMBforRTO(20L, packetId, DC.toString());
        tmsServiceHelper.processInTMSFromClosedToReturnHub.accept(masterBag);

        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBag, EnumSCM.RECEIVED, "Bangalore", 36, "WH")
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to inscan masterBag at WH");

        Assert.assertEquals(
                lmsServiceHelper.receiveShipmentFromMasterbag(masterBag).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive shipment in DC");

        ExceptionHandler.handleTrue(lmsServiceHelper.validateOrderStatusInLMS(packetId, ShipmentStatus.RTO_IN_TRANSIT.toString(), 10));

        Thread.sleep(5000);

        lmsKhataValidator.validateEventCreation(tracking_num, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA1_account_id.toString(), "true", "Null", "Null", String.valueOf(-amount1), "Null", "Null", "Null", "Null", "false");
        lmsKhataValidator.validateEventCreation(tracking_num, EventType.RTO_DISPATCHED.toString(), DC.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA1_account_id), "0.00", "0.00", "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1_account_id), String.valueOf(amount1), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        lmsKhataValidator.validateEventCreation(tracking_num1, CASH_RECON_EVENT_TYPE_PAYMENT, STORE.toString(), SDA1_account_id.toString(), "true", "Null", "Null", String.valueOf(-amount1), "Null", "Null", "Null", "Null", "false");

        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(STORE), "0.00", formatted_amount2, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num1, String.valueOf(SDA), formatted_amount2,"0.00" , "0.00", "false", "Null", INSTAKART.toString());
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(amount2), LedgerType.CASH.toString(), tracking_num1);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-amount2), LedgerType.CASH.toString(), tracking_num1);


    }



    // DL store bag-> Add less amount to reverse bag -> HAndover In DC
    @Test(groups = {"Excess", "Smoke", "Regression"}, priority = 8, description = "ID:C19267 , findShipmentsByTripNumberReverseBag", dataProviderClass = LastMileTestDP.class, dataProvider = "rollingTripFGOn", enabled = true)
    public void paymentStoretoSDA_excessbyTriggerwithDiffSDA(String name, String ownerFirstName, String ownerLastName, String latLong, String emailId, String mobileNumber, String address, String pincode, String city, String state, String mappedDcCode,
                                                  String gstin, String tenantId, Boolean isAvailable, Boolean isDeleted, Boolean isCardEnabled, Integer rating, StoreType storeType, ReconType hlpReconType, Integer capacity, String code) throws Exception {
        Double cash = 0.0, goods = 0.0, cash_DC = 0.0, goods_DC = 0.0, cash_SDA = 0.0, goods_SDA = 0.0, cash_STORE = 0.0, goods_STORE = 0.0;

        Long originPremiseId = null;
        Long masterBagId = null;
        ShipmentResponse masterBagResponse = null;
        String originDCCity = null;
        String destinationStoreCity = null;
        String searchParams = "code.like:" + code;
        List<String> forwardTrackingNumbersList = new ArrayList<>();
        StoreResponse storeResponse = lastmileHelper.createStore(name, ownerFirstName, ownerLastName, latLong, emailId, mobileNumber, address, pincode, city, state, mappedDcCode,
                gstin, tenantId, isAvailable, isDeleted, isCardEnabled, rating, storeType, hlpReconType, capacity, code);
        Assert.assertTrue(storeResponse.getStoreEntries().get(0).getReconType().name().equalsIgnoreCase(LASTMILE_CONSTANTS.DEFAULT_RECON_TYPE.name()), "The Store Type is NOT matching");
        String storeTenantId = storeResponse.getStoreEntries().get(0).getTenantId();
        Long storeHlPId = storeResponse.getStoreEntries().get(0).getDchlpConfigEntry().getHlpDCId();
        log.info("The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());
        System.out.println("\n_____________The store created is : " + storeHlPId + " and the code is " + storeResponse.getStoreEntries().get(0).getCode());

        AccountResponse accountResponse7 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long STORE = accountResponse7.getAccounts().get(0).getId();

        //get the store account
        Thread.sleep(5000);
        AccountResponse accountResponse1 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(storeTenantId), AccountType.PARTNER);
        Long Store_account_id = accountResponse1.getAccounts().get(0).getId();


        originPremiseId = deliveryCenterClient_qa.getOriginPremiseIdOfDC(LMS_PINCODE.ML_BLR, tenantId);
        originDCCity = deliveryCenterClient_qa.getCityOfDC(pincode, tenantId);
        StoreResponse storeResponse2 = storeClient_qa.searchStoreByCode(0, 20, "code", "ASC", Boolean.parseBoolean(null), searchParams, "");
        destinationStoreCity = storeResponse2.getStoreEntries().get(0).getCity();
        try {
            masterBagResponse = lastmileHelper.createMasterBag(originPremiseId, com.myntra.lms.client.status.PremisesType.DC, storeHlPId, com.myntra.lms.client.status.PremisesType.DC, ShippingMethod.NORMAL, tenantId, originDCCity, destinationStoreCity);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        masterBagId = masterBagResponse.getEntries().get(0).getId();
        log.info("The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);
        System.out.println("\n_____________The origin premise Id is  of the DC for pincode " + pincode + " and tenant Id : " + tenantId + " is : " + originPremiseId);

        log.info("The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);
        System.out.println("\n_____________The masterBag id created for store : " + storeResponse.getStoreEntries().get(0).getCode() + " is : " + masterBagId);


                                                                                                                                                                             String orderId = lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, false);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        String orderReleaseId = omsServiceHelper.getReleaseId(orderId);// 1st order
        //amount
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetails(packetId);
        Double amount1 = orderResponse.getOrders().get(0).getCodAmount(); // for 1st order
        DecimalFormat format1 = new DecimalFormat("0.00");
        String formatted_amount1 = format1.format(amount1);
        Thread.sleep(5000);
        lmsKhataValidator.validateEventCreation(tracking_num, com.myntra.lms.khata.domain.EventType.RECEIVED_IN_DC.toString(), INSTAKART.toString(), DC.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(INSTAKART), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(MYNTRA), Long.toString(INSTAKART), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);


        // Adding 1 orders to MB

        ShipmentUpdateInfo shipmentUpdateInfo = new ShipmentUpdateInfo.Builder(Long.toString(masterBagId), ShipmentType.DL, tracking_num, null, null, null, null, null, null, false).build();
        try {

            masterBagClient_qa.addShipmentToStoreBag(masterBagId, shipmentUpdateInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String deliveryStaffID = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse8 = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID), AccountType.SDA);
        Long SDA = accountResponse8.getAccounts().get(0).getId();

        TripResponse tripResponse = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long tripId = tripResponse.getTrips().get(0).getId();
        String tripNumber = tripResponse.getTrips().get(0).getTripNumber();
        Assert.assertTrue(tripResponse.getTrips().get(0).getTenantId().equals(LMS_CONSTANTS.TENANTID), " The trip tenantId is not 4019");
        System.out.println("\n____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());
        log.info("____________The trip id is " + tripId + " , which is created from delivery staff id - " + tripResponse.getTrips().get(0).getDeliveryStaffId() + " name : " + tripResponse.getTrips().get(0).getDeliveryStaffEntry().getFirstName() + " and the Trip number is :" + tripNumber + " and the trip status is : " + tripResponse.getTrips().get(0).getTripStatus());


        //Add the storeBag to this trip

        TripShipmentAssociationResponse tripShipmentAssociationResponse = tripClient_qa.addStoreBagToTrip(String.valueOf(tripId), String.valueOf(masterBagId),LMS_CONSTANTS.TENANTID);
        log.info("____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        System.out.println("\n____________Adding masterBag " + masterBagId + " to the trip : " + tripId + " , trip number : " + tripResponse.getTrips().get(0).getTripNumber());
        //TODO : add below in validation
        Assert.assertTrue(tripShipmentAssociationResponse.getStatus().getStatusMessage().contains("Shipments Successfully added to trip"));
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)), "The shipmentId is " + tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId() + " in the trip_shipment_association is not as expected : " + masterBagId);
        Assert.assertTrue(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getTripId().equals(String.valueOf(tripId)), " The trip Id is not matching ");
        Thread.sleep(3000);
        String param = tripNumber + "?tenantId=" + LASTMILE_CONSTANTS.TENANT_ID;
        TripShipmentAssociationResponse tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Failed");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD, "Expected DL");
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getShipmentItems().get(0).getTrackingNumber(), tracking_num, "Wrong order in master bag");
        Assert.assertEquals(tripClient_qa.startTrip(tripId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to start trip");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD, "Invalid status");


        forwardTrackingNumbersList.add(tracking_num);


        //Delivering the store bag to store
        TripShipmentAssociationResponse shipmentResponse = tripClient_qa.deliverStoreBagToStore(String.valueOf(masterBagId), forwardTrackingNumbersList, String.valueOf(tripId), AttemptReasonCode.DELIVERED,LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(shipmentResponse.getTripShipmentAssociationEntryList().get(0).getShipmentId().equals(String.valueOf(masterBagId)));
        Assert.assertTrue(shipmentResponse.getStatus().getStatusType().name().equals("SUCCESS"), "Delivering store bag failed ");
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");

        tripClient_qa.closeMyntraTripWithStoreBag(LMS_CONSTANTS.TENANTID, tripId);
        tripShipmentAssociationResponse1 = lmsServiceHelper.findShipmentsByTripNumber(param);
        Assert.assertEquals(tripShipmentAssociationResponse1.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.DL, "Invalid status");
        Thread.sleep(5000);

        // validate Event , Shipment pendency and ledger for tracking number 1

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.HAND_OVER_TO_LAST_MILE_PARTNER.toString(), DC.toString(), STORE.toString(), "true", "Null", formatted_amount1, "Null", "Null", LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(DC), "0.00", "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", "0.00", formatted_amount1, "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(DC), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(Store_account_id), String.valueOf(-amount1), LedgerType.GOODS.toString(), tracking_num);

        // Create a Reverse bag to make excess payment :


        TripResponse reverseTrip = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID));
        Long reverseTripId = reverseTrip.getTrips().get(0).getId();
        String reverseTripNumber = reverseTrip.getTrips().get(0).getTripNumber();
        String reverseBagId = tripClient_qa.assignReverseBagToTrip(reverseTripId, storeHlPId, LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        float excess_amount = 100.00f;
        String formatted_excess_amount = format1.format(excess_amount);

        TripShipmentAssociationResponse reverseShipmentResponse = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId), String.valueOf(reverseTripId), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,excess_amount,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");


        // Check Excess is created.

        ExcessResponse excessResponse = khataHelper.searchExcessByfromtoaccount(STORE,SDA);
        long paymentId = excessResponse.getExcesses().get(0).getPaymentId();
        Assert.assertEquals(excessResponse.getExcesses().get(0).getStatus(),PaymentStatus.CREATED,"payment is not processing");


        // create store trip -> mark fail ->  create reverse master bag -> add it to the rev master bag -> Reconcile - > close

        long storeDeliveryStaffId = deliveryStaffClient_qa.searchDeliveryStaffByDeliveryCenterId(storeHlPId);
        log.info("_______The store delivery staff id is : " + storeDeliveryStaffId);
        System.out.println("_______The store delivery staff id is : " + storeDeliveryStaffId);


        TripResponse storeTrip = mlShipmentServiceV2Client_qa.createStoreTrip(forwardTrackingNumbersList, storeDeliveryStaffId);


        Assert.assertTrue(storeTrip.getTrips().get(0).getTenantId().equals(storeTenantId), "The store Trip tenant id is not " + storeTenantId);
        String storeTripNumber = storeTrip.getTrips().get(0).getTripNumber();
        Long storeTripId = storeTrip.getTrips().get(0).getId();
        System.out.println("\n\n*****The Store trip id : " + storeTrip.getTrips().get(0).getId() + " ,and its tripNumber is " + storeTripNumber + " ,which is created   for the store : + " + storeHlPId + " , with code : " + code + " , for delivery staff : " + storeDeliveryStaffId + " for the tracking numbers :  " + forwardTrackingNumbersList.toString() + "\n\n********");

        HashMap<Long, AttemptReasonCode> tripOrderIdAttemptReasonMap = new HashMap<>();
        // statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignment =statusPollingValidator.getActiveTripOrdersByDeliveryStaffMobile(mobileNumber,storeTenantId);
        long tripOrderAssignmentId = tripOrderAssignment.getTripOrders().stream().filter(a -> a.getTrackingNumber().equals(forwardTrackingNumbersList.get(0))).findFirst().get().getId();
        tripOrderIdAttemptReasonMap.put(tripOrderAssignmentId, AttemptReasonCode.INCOMPLETE_INCORRECT_ADDRESS);
        MLShipmentResponse mlShipmentResponse = mlshipmentServiceV2Client_qa.getMLShipmentDetails(tracking_num, storeTenantId);
        TripOrderAssignmentResponse tripOrderAssignmentResponse2 = tripClient_qa.deliverStoreOrders(storeTripId, String.valueOf(mlShipmentResponse.getMlShipmentEntries().get(0).getId()), tripOrderAssignmentId, storeTenantId, "CASH",LASTMILE_CONSTANTS.TENANT_ID);

        Thread.sleep(5000);
        //validate Event , Shipment pendency and ledger balanced

        lmsKhataValidator.validateEventCreation(tracking_num, EventType.DELIVERED.toString(), STORE.toString(), INSTAKART.toString(), "true", "Null", formatted_amount1, formatted_amount1, "Null", storeTenantId, LASTMILE_CONSTANTS.TENANT_ID,"Null");
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), formatted_amount1, "0.00", "0.00", "false", LASTMILE_CONSTANTS.TENANT_ID, "Null");
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(amount1), LedgerType.GOODS.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(-amount1), LedgerType.CASH.toString(), tracking_num);


        String deliveryStaffID1 = String.valueOf(lmsServiceHelper.addDeliveryStaffID(originPremiseId));
        AccountResponse accountResponse = KhataServiceHelper.getAccountbyownerRefernceandType(Long.parseLong(deliveryStaffID1), AccountType.SDA);
        Long SDA1 = accountResponse.getAccounts().get(0).getId();

        TripResponse reverseTrip1 = tripClient_qa.createTrip(originPremiseId, Long.parseLong(deliveryStaffID1));
        Long reverseTripId1 = reverseTrip1.getTrips().get(0).getId();
        String reverseTripNumber1 = reverseTrip1.getTrips().get(0).getTripNumber();
        String reverseBagId1 = tripClient_qa.assignReverseBagToTrip(reverseTripId1, storeHlPId, LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber1, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getShipmentType().toString(), ShipmentType.REVERSE_BAG.toString(), "Expected Reverse Bag");
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.WFD);
        tripClient_qa.startTrip(reverseTripId1);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber1, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), EnumSCM.OFD);

        float sda1 = amount1.floatValue()-100.00f;
        String formatted_sda1_amount = format1.format(sda1);

        TripShipmentAssociationResponse reverseShipmentResponse1 = tripClient_qa.pickupReverseBagFromStoreOnlyCash(String.valueOf(reverseBagId1), String.valueOf(reverseTripId1), AttemptReasonCode.PICKED_UP_SUCCESSFULLY, 0, 0,sda1,LMS_CONSTANTS.TENANTID);
        tripShipmentAssociationResponse = tripClient_qa.findShipmentByTripNumber(reverseTripNumber, ShipmentType.REVERSE_BAG, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(tripShipmentAssociationResponse.getTripShipmentAssociationEntryList().get(0).getStatus().toString(), TripOrderStatus.PS.toString(), "Amount was picked up successfully");


        Thread.sleep(10000);
        ExcessResponse excessResponse1 = khataHelper.searchExcessByPaymentId(paymentId);
        Assert.assertEquals(excessResponse1.getExcesses().get(0).getStatus(),PaymentStatus.PROCESSING);


        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(STORE), "0.00", formatted_amount1, "0.00", "true", LASTMILE_CONSTANTS.TENANT_ID, INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA), formatted_excess_amount,"0.00" , "0.00", "false", "Null", INSTAKART.toString());
        lmsKhataValidator.validateShipmentPendency(tracking_num, String.valueOf(SDA1), formatted_sda1_amount,"0.00" , "0.00", "false", "Null", INSTAKART.toString());
//        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(excess_amount), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA), String.valueOf(-excess_amount), LedgerType.CASH.toString(), tracking_num);
        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(STORE), String.valueOf(sda1), LedgerType.CASH.toString(), tracking_num);

        ledgerValidator.validateLedger(Long.toString(INSTAKART), Long.toString(SDA1), String.valueOf(-sda1), LedgerType.CASH.toString(), tracking_num);


    }


}
