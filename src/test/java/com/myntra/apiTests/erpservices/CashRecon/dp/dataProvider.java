package com.myntra.apiTests.erpservices.CashRecon.dp;

import com.myntra.lastmile.client.code.utils.DeliveryStaffCommute;
import com.myntra.lastmile.client.code.utils.DeliveryStaffType;
import com.myntra.lastmile.client.code.utils.ReconType;
import com.myntra.lordoftherings.Toolbox;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.util.Random;

public class dataProvider {

    @DataProvider
    public static Object[][] createDC(ITestContext testContext) {
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        Object[] arr1 = {"DC"+String.valueOf(contactNumber).substring(5, 9) ,"TH-BLR" ,  String.valueOf(contactNumber).substring(3, 9) , "4019" , false , false , false , true , "ML" , ReconType.ROLLING_RECON ,String.valueOf(contactNumber).substring(5, 6) ,1000 };
        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }

    @DataProvider
    public static Object[][] createSDA(ITestContext testContext) {
        Random r = new Random(System.currentTimeMillis());
        int contactNumber = Math.abs(1000000000 + r.nextInt(2000000000));
        Object[] arr1 = {"DC"+String.valueOf(contactNumber).substring(5, 9) ,1l ,  String.valueOf(contactNumber).substring(3, 9) ,String.valueOf(contactNumber).substring(5, 9) ,DeliveryStaffCommute.BIKER, "4019" , true , false , false ,DeliveryStaffType.MYNTRA_PAYROLL};
        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 2, 2);
    }
}

