package com.myntra.apiTests.erpservices.masterbag.test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Utils.DateTimeHelper;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.DB.IQueries;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbag.dp.MasterBagV2SearchDP;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagConstants;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagV2Search;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagV2SearchHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShipmentStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.response.MasterbagResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.*;

/**
 * @author Shanmugam Yuvaraj
 * @since Aug-2019
 */
public class MasterBagV2SearchTest {
    MasterBagV2SearchHelper masterBagV2SearchHelper = new MasterBagV2SearchHelper();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    LMSHelper lmsHelper = new LMSHelper();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMSClient lmsClient = new LMSClient();
    StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    StoreValidator storeValidator = new StoreValidator();

    @Test(enabled = true, description = "ID : 22133 - search multiple master bag combination in master bag V2 page")
    public void test_Multiple_Masterbag_Id_Search() throws Exception {

        String orderID = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        String hubCode = orderResponse.getOrders().get(0).getDispatchHubCode();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, com.myntra.logistics.platform.domain.ShipmentStatus.INSCANNED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        Long masterBagId = (masterBagServiceHelper.createMasterBag(hubCode, deliveryCenterCode, ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID)).getId();
        Long masterBagId1 = (masterBagServiceHelper.createMasterBag(hubCode, "HSR", ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID)).getId();

        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNo, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), "SUCCESS", "order is not added to masterbag");
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());

        String finalQueryParam = masterBagV2SearchHelper.buildQueryParam(MessageFormat.format(MasterBagV2Search.MULTIPLE_MB_ID.getFieldValue(),
                masterBagId + "," + masterBagId1));
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterbagUsingQueryParam(finalQueryParam);
        if (masterbagResponse.getStatus().getTotalCount() > 0) {
            List<Map<String, Object>> dbData = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.MasterbagV2SearchQuery.multiple_MBId_Search, masterBagId + "," + masterBagId1));
            masterBagV2SearchHelper.validate_DBandAPIData_Without_ShippedOnDate(masterbagResponse, dbData);
        }
    }

    @Test(enabled = true, description = "ID : 22133 - search based on origin and destination fields in master bag V2 page")
    public void test_Origin_Destination_Field_Search() throws ParseException, InterruptedException {
        String finalQueryParam = masterBagV2SearchHelper.buildQueryParam(MessageFormat.format(MasterBagV2Search.ORIGIN_LOCATION.getFieldValue(),
                MasterBagConstants.originHubType, MasterBagConstants.originHubId),
                MessageFormat.format(MasterBagV2Search.DEST_LOCATION.getFieldValue(), MasterBagConstants.destHubType, MasterBagConstants.destHubId),
                MessageFormat.format(MasterBagV2Search.LAST_MODIFIED_ON.getFieldValue(),
                        DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                        , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))));
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterbagUsingQueryParam(finalQueryParam.replaceAll(" ", "%20") + "&sortBy=createdOn&sortOrder=DESC");
        if (masterbagResponse.getStatus().getTotalCount() > 0) {
            List<Map<String, Object>> dbData = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.MasterbagV2SearchQuery.origin_DestField_Search,
                    MasterBagConstants.originHubId, MasterBagConstants.originHubType, MasterBagConstants.destHubId, MasterBagConstants.destHubType,
                    DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                    , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))));
            masterBagV2SearchHelper.validateDBandAPIData(masterbagResponse, dbData);
        }
    }

    @Test(enabled = true, description = "ID : 22133 - search based on Last_modified_On and shippedOn fields in master bag V2 page")
    public void testLast_Modified_On_Shipped_OnSearch() throws ParseException {
        //URLEncoder.encode(finalQueryParam, "UTF-8")
        String finalQueryParam = masterBagV2SearchHelper.buildQueryParam(MessageFormat.format(MasterBagV2Search.SHIPPED_ON.getFieldValue(),
                DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))),
                MessageFormat.format(MasterBagV2Search.LAST_MODIFIED_ON.getFieldValue(),
                        DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                        , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))));
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterbagUsingQueryParam(finalQueryParam.replaceAll(" ", "%20"));
        if (masterbagResponse.getStatus().getTotalCount() > 0) {
            masterBagV2SearchHelper.validateGivenDateInRange(DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                    , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1)), masterbagResponse);
            List<Map<String, Object>> dbData = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.MasterbagV2SearchQuery.shippedOn_LastModifiedOn_Search,
                    DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                    , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))
                    , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                    , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))));
            masterBagV2SearchHelper.validateDBandAPIData(masterbagResponse, dbData);
        }
    }

    @Test(dataProviderClass = MasterBagV2SearchDP.class, dataProvider = "search_BasedOn_Diff_Status", enabled = true,
            description = "ID : 22133 - Search based on different master bag status and last_modified_on fields in master bag v2 page")
    public void test_Search_BasedOn_Diff_Status(String status, String originPremisesType, String originPremisesId, String destPremisesType, String destPremisesId) throws ParseException {
        String finalQueryParam = masterBagV2SearchHelper.buildQueryParam(MessageFormat.format(MasterBagV2Search.STATUS.getFieldValue(), status),
                MessageFormat.format(MasterBagV2Search.ORIGIN_LOCATION.getFieldValue(), originPremisesType, originPremisesId),
                MessageFormat.format(MasterBagV2Search.DEST_LOCATION.getFieldValue(), destPremisesType, destPremisesId),
                MessageFormat.format(MasterBagV2Search.LAST_MODIFIED_ON.getFieldValue(),
                        DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                        , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))));
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterbagUsingQueryParam(finalQueryParam.replaceAll(" ", "%20") + "&sortBy=createdOn&sortOrder=DESC");
        if (masterbagResponse.getStatus().getTotalCount() > 0) {
            List<Map<String, Object>> dbData = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.MasterbagV2SearchQuery.status_origin_DestField_Search,
                    status, originPremisesId, originPremisesType, destPremisesId, destPremisesType,
                    DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                    , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))));
            masterBagV2SearchHelper.validate_DBandAPIData_Without_ShippedOnDate(masterbagResponse, dbData);
        }
    }

    @Test(enabled = true, description = "ID : 22133 - search based on Master bag id field with wrong status in master bag V2 page")
    public void test_MasterBagId_Wrong_Status() throws Exception {
        Long masterBagId = (masterBagServiceHelper.createMasterBag(MasterBagConstants.originHubCode, MasterBagConstants.destHubCode, ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID)).getId();
        String finalQueryParam = masterBagV2SearchHelper.buildQueryParam(MessageFormat.format(MasterBagV2Search.MASTER_BAG_ID.getFieldValue(), String.valueOf(masterBagId)),
                MessageFormat.format(MasterBagV2Search.STATUS.getFieldValue(), ShipmentStatus.CLOSED.toString()));
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterbagUsingQueryParam(finalQueryParam.replaceAll(" ", "%20"));
        if (masterbagResponse.getStatus().getTotalCount() > 0) {
            Assert.assertEquals(masterbagResponse.getStatus().getTotalCount(), 0, "Master bag search is " +
                    "working for master bag id " + masterBagId + " and status =  CLOSED, but it is in new status");
        }
    }

    @Test(enabled = true, description = "ID : 22133 - search based on All the fields which is in master bag V2 page with out master bag id and status fields")
    public void test_searchWith_All_Fields() throws Exception {
        String finalQueryParam = masterBagV2SearchHelper.buildQueryParam(
                MessageFormat.format(MasterBagV2Search.SHIPPING_METHOD.getFieldValue(), ShippingMethod.NORMAL),
                MessageFormat.format(MasterBagV2Search.SHIPPED_ON.getFieldValue(),
                        DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                        , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))),
                MessageFormat.format(MasterBagV2Search.LAST_MODIFIED_ON.getFieldValue(),
                        DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                        , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))),
                MessageFormat.format(MasterBagV2Search.ORIGIN_LOCATION.getFieldValue(), MasterBagConstants.originHubType, MasterBagConstants.originHubId),
                MessageFormat.format(MasterBagV2Search.DEST_LOCATION.getFieldValue(), MasterBagConstants.destHubType, MasterBagConstants.destHubId));
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterbagUsingQueryParam(finalQueryParam.replaceAll(" ", "%20") + "&sortBy=createdOn&sortOrder=DESC");
        if (masterbagResponse.getStatus().getTotalCount() > 0) {
            List<Map<String, Object>> dbData = masterBagV2SearchHelper.getDbData(IQueries.formatQuery(IQueries.MasterbagV2SearchQuery.search_All_Fields,
                    ShippingMethod.NORMAL.toString(), MasterBagConstants.originHubId, MasterBagConstants.originHubType, MasterBagConstants.destHubId, MasterBagConstants.destHubType,
                    DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                    , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))
                    , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -5))
                    , DateTimeHelper.convertDateFormat(MasterBagConstants.datePattern3, MasterBagConstants.datePattern2, DateTimeHelper.generateDate(MasterBagConstants.datePattern3, -1))));
            masterBagV2SearchHelper.validateDBandAPIData(masterbagResponse, dbData);
        }
    }
}
