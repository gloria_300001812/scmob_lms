package com.myntra.apiTests.erpservices.masterbag.test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.HubToTransportHubConfigResponse;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.lms.Retry;
import com.myntra.apiTests.erpservices.masterbag.dp.NewMasterBagDP;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderShipmentAssociationEntry;
import com.myntra.lms.client.response.ShipmentEntry;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.core.MasterbagType;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagResponse;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.test.commons.testbase.BaseTest;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import com.myntra.tms.statemachine.masterbag.MasterbagUpdateEvent;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Apr-2019
 * New MasterBag as service
 */
public class NewMasterBagService extends BaseTest {
    String env = getEnvironment();
    LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    MasterBagValidator masterBagValidator = new MasterBagValidator();
    LMSHelper lmsHelper = new LMSHelper();
    OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    LMSOperations lmsOperations = new LMSOperations();

    @Test(dataProvider = "CreateMasterBagForwardDL", dataProviderClass = NewMasterBagDP.class,
            retryAnalyzer = Retry.class)
    public void testCreateMasterBagForwardDL(String originHubCode, String destinationHubCode, String pincode ,ShipmentType shipmentType, ShippingMethod shippingMethod, String courierCode, String tenantId, Integer capacity) throws Exception {
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse addShipmentToMbResponse, closeMBResponse;
        String orderId , packetId, trackingNumber;
        Long masterBagId;
        orderId = lmsHelper.createMockOrder(EnumSCM.IS, pincode, "ML", "36", shippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));
        if(capacity==0) {
            //verify default capacity of MB
            mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        }else {
            mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, capacity, tenantId);
        }
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId , capacity);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId="+masterBagId);
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, shipmentType, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToMasterBag(addShipmentToMbResponse, masterBagId, trackingNumber);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(closeMBResponse, masterBagId, trackingNumber);
    }

    @Test(dataProvider = "CreateMasterBagForwardTryAndBuy", dataProviderClass = NewMasterBagDP.class,
            retryAnalyzer = Retry.class)
    public void testCreateMasterBagForwardTryAndBuy(String originHubCode, String destinationHubCode, String pincode, ShipmentType shipmentType ,ShippingMethod shippingMethod, String courierCode, String tenantId, Integer capacity) throws Exception {
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse addShipmentToMbResponse, closeMBResponse;
        String orderId , packetId, trackingNumber;
        Long masterBagId;
        orderId = lmsHelper.createMockOrder(EnumSCM.IS, pincode, "ML", "36",
                (shippingMethod.toString().equals(ShippingMethod.EXPRESS.toString()) ? EnumSCM.XPRESS : shippingMethod.toString()),
                "cod", true, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));
        if(capacity==0) {
            //verify default capacity of MB
            mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        }else {
            mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, capacity, tenantId);
        }
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId , capacity);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId="+masterBagId);
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, shipmentType,  LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToMasterBag(addShipmentToMbResponse, masterBagId, trackingNumber);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(closeMBResponse, masterBagId, trackingNumber);
    }

    @Test(dataProvider = "CreateMasterBagWithDifferentHubType", dataProviderClass = NewMasterBagDP.class,
            retryAnalyzer = Retry.class)
    public void testHubTypeInCreateMasterBag(String originHubCode, String destinationHubCode, ShippingMethod shippingMethod, String courierCode, String tenantId,  MasterbagType expectedMasterbagType) throws Exception {
        MasterbagDomain mbCreateResponse;
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(mbCreateResponse.getId());
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getType().toString(), expectedMasterbagType.toString() , originHubCode +" -> "+destinationHubCode);
    }

    @Test(retryAnalyzer = Retry.class)
    public void testMasterBagBasicOperation() throws Exception {
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse addShipmentToMbResponse, closeMBResponse;
        String orderId , packetId, trackingNumber;
        String inScanResponse;
        Long masterBagId;
        String originHubCode = "DH-BLR" , destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod =ShippingMethod.NORMAL;
        orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, "36",false, LMS_CONSTANTS.TENANTID);
        System.out.println("response="+inScanResponse);
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId="+masterBagId);
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToMasterBag(addShipmentToMbResponse, masterBagId, trackingNumber);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(closeMBResponse, masterBagId, trackingNumber);
    }

    /**
     * This testcase covers all the masterbag API operations.
     * a. Create Masterbag
     * b. Close Empty MasterBag
     * c. Add Shipment -> remove shipment & Close MasterBag
     * d. Add Shipment -> close MasterBag
     * e. Close MasterBag -> twice
     */
    @Test(retryAnalyzer = Retry.class)
    public void testMasterBagCRUDOperation() throws Exception {
        MasterbagDomain createMasterBagResponse;
        String orderId , packetId, trackingNumber;
        String inScanResponse;
        MasterbagUpdateResponse masterbagUpdateResponse;
        Long masterBagId;
        String originHubCode = "DH-BLR" , destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod =ShippingMethod.NORMAL;
        orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, "36",false, LMS_CONSTANTS.TENANTID);
        System.out.println("response="+inScanResponse);
        createMasterBagResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(createMasterBagResponse,originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = createMasterBagResponse.getId();
        System.out.println("masterBagId="+masterBagId);
        masterbagUpdateResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseEmptyMasterBag(masterbagUpdateResponse);
        masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber,ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumber);
        masterbagUpdateResponse = masterBagServiceHelper.removeShipmentsFromMasterBag(masterBagId, trackingNumber,ShipmentType.DL,  LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateRemoveShipmentFromMasterBag(masterbagUpdateResponse, masterBagId, trackingNumber);
        masterBagValidator.validateIsEmptyMasterBag(masterBagId);
        masterbagUpdateResponse = masterBagServiceHelper.removeShipmentsFromMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateRemoveShipmentFromEmptyMasterBag(masterbagUpdateResponse, masterBagId);
        masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumber);
        masterbagUpdateResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(masterbagUpdateResponse, masterBagId);
        masterbagUpdateResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateClosedMasterBagClose(masterbagUpdateResponse, masterBagId);
    }

    /**
     * Add Shipments to MasterBag
     * a. Add Same shipment to masterBag multiple Times and verify DB
     * b. Create MasterBag with capacity of 5 and try adding 6 shipments aswell as exact number of shipments - P(a)
     * c. Add shipment to new masterbag which is already closed - P
     * d. Add 2 Shipments -> remove 1 shipment & Close MasterBag
     * Validation: Add shipment/s to MasterBag and verify in DB.
     */
    @Test(retryAnalyzer = Retry.class)
    public void AddMultipleShipmentsToMasterBag() throws Exception {
        MasterbagDomain masterbagDomain;
        String orderId , packetId;
        String inScanResponse, warehouseId = "36";
        MasterbagUpdateResponse masterbagUpdateResponse;
        Long masterBagId;
        Integer masterBagCapacity =2;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        String originHubCode = "DH-BLR" , destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod =ShippingMethod.NORMAL;
        orderMap = lmsOperations.createMockOrders(masterBagCapacity, EnumSCM.PK, warehouseId, "560068",
                shippingMethod.toString(), courierCode,  false, "cod");
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        lmsOperations.orderInscanV2(trackingNumbers, warehouseId, false);
        masterbagDomain = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, masterBagCapacity, tenantId);
        masterBagValidator.validateCreateMasterBag(masterbagDomain,originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = masterbagDomain.getId();
        System.out.println("masterBagId="+masterBagId);
        for(String trackingNumber : trackingNumbers) {
            masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            masterBagValidator.validateAddShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumber);
        }
        masterBagValidator.validateMultipleShipmentsAddedToMasterBag(masterBagId,trackingNumbers);
        masterbagUpdateResponse = masterBagServiceHelper.removeShipmentsFromMasterBag(masterBagId, trackingNumbers.get(0),ShipmentType.DL,  LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateRemoveShipmentFromMasterBag(masterbagUpdateResponse, masterBagId , trackingNumbers.get(0));
        masterbagUpdateResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(masterbagUpdateResponse, masterBagId);
    }

    @Test(retryAnalyzer = Retry.class)
    public void AddSameShipmentsToMasterBag() throws Exception {
        MasterbagDomain masterbagDomain;
        String orderId , packetId, trackingNumber;
        String inScanResponse;
        MasterbagUpdateResponse masterbagUpdateResponse;
        Long masterBagId, newMasterBagId;
        String originHubCode = "DH-BLR" , destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod =ShippingMethod.NORMAL;
        orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, "36",false, LMS_CONSTANTS.TENANTID);
        System.out.println("response="+inScanResponse);
        masterbagDomain = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(masterbagDomain,originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = masterbagDomain.getId();
        System.out.println("masterBagId="+masterBagId);
        masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumber);
        masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber,ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddSameSipmentToMasterBag(masterbagUpdateResponse);
        //create new MB and add same sh to new MB
        masterbagDomain = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(masterbagDomain,originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        newMasterBagId = masterbagDomain.getId();
        masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(newMasterBagId, trackingNumber,ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddSameSipmentToNewMasterBag(masterbagUpdateResponse,masterBagId,  newMasterBagId);
        masterbagUpdateResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(masterbagUpdateResponse, masterBagId);
    }

    @Test(retryAnalyzer = Retry.class)
    public void AddShipmentsToClosedMasterBag() throws Exception {
        MasterbagDomain masterbagDomain;
        String orderId , packetId, trackingNumber;
        String inScanResponse;
        MasterbagUpdateResponse masterbagUpdateResponse;
        Long masterBagId, newMasterBagId;
        String originHubCode = "DH-BLR" , destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod =ShippingMethod.NORMAL;
        orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, "36",false, LMS_CONSTANTS.TENANTID);
        System.out.println("response="+inScanResponse);
        masterbagDomain = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(masterbagDomain,originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = masterbagDomain.getId();
        System.out.println("masterBagId="+masterBagId);
        masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumber);
        masterbagUpdateResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(masterbagUpdateResponse, masterBagId);
        //validate add shipments to closed MasterBag
        orderId = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));
        masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToClosedMasterBag(masterbagUpdateResponse, masterBagId);
    }

    @Test(retryAnalyzer = Retry.class)
    public void AddExcessShipmentsToMasterBag() throws Exception {
        MasterbagDomain masterbagDomain;
        String orderId , packetId;
        String inScanResponse, warehouseId = "36";
        MasterbagUpdateResponse masterbagUpdateResponse;
        Long masterBagId;
        Integer masterBagCapacity =2;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        String originHubCode = "DH-BLR" , destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod =ShippingMethod.NORMAL;
        orderMap = lmsOperations.createMockOrders(masterBagCapacity+1, EnumSCM.PK, warehouseId, "560068",
                shippingMethod.toString(), courierCode, false, "cod");
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        lmsOperations.orderInscanV2(trackingNumbers, warehouseId, false);
        masterbagDomain = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, masterBagCapacity, tenantId);
        masterBagValidator.validateCreateMasterBag(masterbagDomain,originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = masterbagDomain.getId();
        System.out.println("masterBagId="+masterBagId);
        int i;
        for(i=0;i<trackingNumbers.size()-1;i++) {
            masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumbers.get(i), ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            masterBagValidator.validateAddShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumbers.get(i));
        }
        masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumbers.get(i),ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        trackingNumbers.remove(trackingNumbers.get(i)); //remove excess from list for validation purpose
        masterBagValidator.validateAddExcessShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumbers, masterBagCapacity);
        masterBagValidator.validateMultipleShipmentsAddedToMasterBag(masterBagId,trackingNumbers);
        masterbagUpdateResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(masterbagUpdateResponse, masterBagId);
    }

    /**
     * Create a order from BLR-DH TO DEL-DH and create masterbag with config(DH-BLR to ELC) and try adding the shipment to masterbag.
     * Remove invalid shipment from masterbag, which has not been added.
     * Add invalid tracking number to MB
     * -- src * dest diff
     */
    @Test(retryAnalyzer = Retry.class)
    public void AddShipmentsToInvalidMasterBag() throws Exception {
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse addShipmentToMbResponse, removeShipmentToMbResponse;
        String orderId , packetId, trackingNumber;
        String invalidTrackingNumber = "ML0009999999";
        String inScanResponse;
        Long masterBagId;
        String originHubCode = "DH-BLR" , destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod =ShippingMethod.NORMAL;
        orderId = lmsHelper.createMockOrder(EnumSCM.PK, "100002", "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, "36",false, LMS_CONSTANTS.TENANTID);
        System.out.println("response="+inScanResponse);
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId="+masterBagId);
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber,ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddInvalidShipmentToMasterBag(addShipmentToMbResponse, masterBagId, trackingNumber);
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, invalidTrackingNumber,ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddInvalidShipmentToMasterBag(addShipmentToMbResponse, masterBagId, invalidTrackingNumber);
        removeShipmentToMbResponse = masterBagServiceHelper.removeShipmentsFromMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateRemoveShipmentFromEmptyMasterBag(removeShipmentToMbResponse, masterBagId);
    }

    /**
     * Edit MasterBag
     * a. create MB with config X and order with config Y - add shipment to MB, it fails.
     * b. Fix the config using MB edit - check MB Type (based on location, it should change), updated Destination, update capacity
     * c. add same shipment back after edit, add shipment should succeed.
     */
    @Test(retryAnalyzer = Retry.class)
    public void EditMasterBag() throws Exception {
        MasterbagDomain mbCreateResponse, editShipmentResponse;
        MasterbagUpdateResponse addShipmentToMbResponse, closeMBResponse;
        String orderId , packetId, trackingNumber;
        String inScanResponse;
        Long masterBagId;
        Integer newCapacity =10;
        String newDestination = "ELC";
        String originHubCode = "DH-BLR" , destinationHubCode = "DELHIS", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod masterBagShippingMethod =ShippingMethod.NORMAL;
        ShippingMethod orderShippingMethod =ShippingMethod.SDD;
        orderId = lmsHelper.createMockOrder(EnumSCM.IS, "560068", "ML", "36", orderShippingMethod.toString(),
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderId, packetId, trackingNumber));
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, masterBagShippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, masterBagShippingMethod, courierCode, tenantId);
        Assert.assertEquals(mbCreateResponse.getMasterbagType(), MasterbagType.HUB_TO_DC);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId="+masterBagId);
        //it fails as MB source and dest is different than shipment
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber,ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddInvalidShipmentToMasterBag(addShipmentToMbResponse, masterBagId, trackingNumber);
        //edit and fix masterbag config
        MasterbagDomain editMasterBagConfig = MasterbagDomain.builder()
        .capacity(newCapacity)
        .originHubCode(originHubCode)
        .destinationHubCode(newDestination)
        .shippingMethod(orderShippingMethod)
        .courierCode(courierCode)
        .tenantId(tenantId)
        .originHubType(lmsServiceHelper.searchHubByCode(originHubCode).getHub().get(0).getType().name())
        .destinationHubType(lmsServiceHelper.searchHubByCode(destinationHubCode).getHub().get(0).getType().name())
        .build();
        editShipmentResponse = masterBagServiceHelper.editMasterBag(masterBagId, editMasterBagConfig);
        masterBagValidator.validateEditShipment(editShipmentResponse, newCapacity, newDestination, originHubCode, courierCode, tenantId, orderShippingMethod);
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber,ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToMasterBag(addShipmentToMbResponse, masterBagId, trackingNumber);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(closeMBResponse, masterBagId, trackingNumber);
    }

    /**
     * 1. Add masterbag to container - verify DB both lms and tms
     * 2. Add same masterbag to multiple containers - status
     * 3. Add masterbag multipe times to same container - Status
     * 4. Ship container - check status of masterbag
     * 5. recieve/inscan masterbag - chek status of masterbag
     */
    @Test(retryAnalyzer = Retry.class)
    public void masterBagTmsOperations() throws Exception {
        MasterbagDomain mbCreateResponse;
        TMSMasterbagReponse tmsMasterbagReponse;
        MasterbagUpdateResponse masterbagUpdateResponse;
        MasterbagUpdateResponse addShipmentToMbResponse, closeMBResponse;
        TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
        LastmileOperations lastmileOperations = new LastmileOperations();
        String pincode = LMS_PINCODE.ML_BLR;
        String inScanResponse;
        Long masterBagId;
        Integer numberOfShipments = 5;
        Map<String, Object> orderMap;
        List<String> trackingNumbers , packetIds,  orderIds;
        long laneId = 13 , transporterId = 1;
        String warehouseId = "36";
        String originHubCode = "DH-BLR" , destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod =ShippingMethod.NORMAL;
        orderMap = lmsOperations.createMockOrders(numberOfShipments, EnumSCM.PK, warehouseId, "560068",
                shippingMethod.toString(), courierCode,  false, "cod");
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]",orderIds, packetIds, trackingNumbers));
        lmsOperations.orderInscanV2(trackingNumbers, warehouseId, false);
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId="+masterBagId);
        for(String trackingNumber : trackingNumbers) {
            masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBagWithShipmentType(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            masterBagValidator.validateAddShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumber);
        }
        masterBagValidator.validateMultipleShipmentsAddedToMasterBag(masterBagId,trackingNumbers);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(closeMBResponse, masterBagId);
        masterBagValidator.validateTMSMasterBag(tenantId, masterBagId, MasterbagUpdateEvent.CREATE_MASTERBAG);
        //TMS Operations
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse= (HubToTransportHubConfigResponse)tmsServiceHelper.getLocationHubConfigFW.apply(originHubCode);
        String transportHubCode= hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        System.out.println("transportHubCode="+transportHubCode);
        tmsMasterbagReponse= (TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBag.apply(transportHubCode, masterBagId);
        masterBagValidator.validateTMSMasterBag(tenantId, masterBagId, MasterbagUpdateEvent.INSCAN);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED_AT_TRANSPORTER_HUB);
        System.out.println("tmsMasterbagReponse="+tmsMasterbagReponse.getStatus());
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        long unUsedContainerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transportHubCode, destinationHubCode, laneId, transporterId)).getContainerEntries().get(0).getId();
        System.out.println(String.format("containerId1[%s], containerId2[%s]",containerId,unUsedContainerId));
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        masterBagValidator.validateTMSMasterBag(tenantId, masterBagId, MasterbagUpdateEvent.ADD_TO_CONTAINER);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED_AT_TRANSPORTER_HUB);
        //add same masterbag to same container again
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.ERROR, "Unable to add masterBag to Container");
        //add masterbag to new container which is already added to different container.
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(unUsedContainerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.WARNING, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        masterBagValidator.validateTMSMasterBag(tenantId, masterBagId, MasterbagUpdateEvent.SHIP);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.getContainer.apply(containerId);
        Assert.assertEquals(((ContainerResponse)tmsServiceHelper.containerInTransitScan.apply(containerId,destinationHubCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        masterBagValidator.validateTMSMasterBag(tenantId, masterBagId, MasterbagUpdateEvent.IN_TRANSIT);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse)tmsServiceHelper.tmsReceiveMasterBagNew.apply(destinationHubCode, masterBagId, containerId);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
        masterBagValidator.validateTMSMasterBag(tenantId, masterBagId, MasterbagUpdateEvent.RECEIVE);

        //MasterBag In-scan V2
        ShipmentResponse mbInScanV2Response = lmsServiceHelper.searchMasterBagInscanV2(masterBagId, tenantId);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.IN_TRANSIT);
        //shipmentId, trackingNumber, lastScannedCity, lastScannedPremisesId, shipmentType, premisesType;
        Iterator<ShipmentEntry> shipmentEntryItr = mbInScanV2Response.getEntries().iterator();
        while(shipmentEntryItr.hasNext()){
            ShipmentEntry shipmentEntry = shipmentEntryItr.next();
            for (OrderShipmentAssociationEntry orderShipmentAssociationEntry : shipmentEntry.getOrderShipmentAssociationEntries()) {
                String trackingNumber = orderShipmentAssociationEntry.getTrackingNumber();
                ShipmentType shipmentType = orderShipmentAssociationEntry.getShipmentType();
                lmsServiceHelper.masterBagInScanV2Update(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
                lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(shipmentEntry.getId(), trackingNumber,
                        shipmentEntry.getDestinationPremisesName(), shipmentEntry.getDestinationPremisesId(),
                        shipmentType, shipmentEntry.getLastScannedPremisesType());
                lmsServiceHelper.masterBagInScanV2RecieveShipment(masterBagId,shipmentEntry.getLastScannedCity(),
                        shipmentEntry.getLastScannedPremisesId(), shipmentEntry.getLastScannedPremisesType());
            }
        }
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        //Last Mile Operations
        long tripId;
        System.out.println("Create Trip -> Assign trip -> Start Trip -> Complete Trip");
        System.out.println(lmsOperations.getDeliveryCenterID(pincode));
        tripId = Long.valueOf(lastmileOperations.createTrip(lmsOperations.getDeliveryCenterID(pincode)).get("tripId"));
        lastmileOperations.assignMultipleOrdersToTripByTrackingNumbers(trackingNumbers, tripId);
        lastmileOperations.startTrip(tripId, trackingNumbers);
        lastmileOperations.updateDeliveryTripTrackingNumbers(trackingNumbers);
        lastmileOperations.completeTrip(trackingNumbers);
        masterBagValidator.validateMasterBagStatus(masterBagId, MasterbagStatus.RECEIVED);
        masterBagValidator.validateTMSMasterBag(tenantId, masterBagId, MasterbagUpdateEvent.RECEIVE);
        System.out.println(String.format("masterBagId:[%s] , trackingNumbers:[%s]",masterBagId, trackingNumbers));
    }


    /**
     * Edit MasterBag and check if the edited conf has been updated using MasterBag Search API
     */
    @Test(dataProvider = "EditMasterBagConfigs", dataProviderClass = NewMasterBagDP.class,
            enabled = false, retryAnalyzer = Retry.class)
    public void testEditMasterBag(MasterbagDomain editMasterBagConfig) throws Exception {
        //edit masterbag with all possible input parameters - using Dataporvider
    }

    @Test
    public void testCreateMasterBagWithPremiseID() throws Exception {
        MasterbagDomain mbCreateResponse;
        mbCreateResponse = masterBagServiceHelper.createMasterBag(18l, 5l, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER);
        System.out.println(mbCreateResponse);
    }
}
