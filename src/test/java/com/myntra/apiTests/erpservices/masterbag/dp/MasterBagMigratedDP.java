package com.myntra.apiTests.erpservices.masterbag.dp;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LMS_ReturnHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ManagerException;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.status.ShipmentStatus;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.lordoftherings.Toolbox;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

/**
 * @author Bharath.MC
 * @since Jul-2019
 */
public class MasterBagMigratedDP {

    private static MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    private static LMSHelper lmsHelper=new LMSHelper();
    private static OMSServiceHelper omsServiceHelper=new OMSServiceHelper();
    private static RMSServiceHelper rmsServiceHelper=new RMSServiceHelper();
    private static StoreValidator storeValidator=new StoreValidator();
    private static LMS_ReturnHelper lms_returnHelper=new LMS_ReturnHelper();
    private static StoreHelper storeHelper=new StoreHelper(getEnvironment(),LMS_CONSTANTS.CLIENTID,LMS_CONSTANTS.TENANTID);
    private static TripClient_QA tripClient_qa=new TripClient_QA();
    private static StatusPollingValidator statusPollingValidator=new StatusPollingValidator();
    private static LmsServiceHelper lmsServiceHelper=new LmsServiceHelper();
    private static LMSClient lmsClient = new LMSClient();

    static String FGTenantID="";

    public MasterBagMigratedDP()
    {
        if(LMS_CONSTANTS.FGVersion==3)
        {
            FGTenantID="tenantId.eq:"+LMS_CONSTANTS.TENANTID+"___";
        }
        if(LMS_CONSTANTS.FGVersion==0)
        {
            FGTenantID="";
        }
    }

    @DataProvider
    public static Object[][] MasterBagShippingMethod(ITestContext testContext) throws Exception {

        //Different Shipping method of MB and order
        Object[] datasetFW1 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetFW2 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetFW3 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetFW4 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetFW5 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetFW6 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetFW7 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.EXPRESS, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetFW8 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.EXPRESS, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetFW9 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.EXPRESS, ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};

        //Try and buy with same above config
        Object[] datasetTNB1 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, true, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetTNB2 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER, true, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetTNB3 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER, true, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetTNB4 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, true, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetTNB5 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER, true, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetTNB6 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER, true, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetTNB7 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.EXPRESS, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, true, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetTNB8 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.EXPRESS, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER, true, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetTNB9 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.EXPRESS, ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER, true, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};

        //Different Source and Dest of MB and order
        Object[] datasetNSD1 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetNSD2 = {"DH-DEL", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetNSD3 = {"DH-BLR", "DH-DEL", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetNSD4 = {"DH-BLR", "DH-DEL", LMS_PINCODE.DELHIS_DC, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetESD1 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.EXPRESS, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetESD2 = {"DH-DEL", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.EXPRESS, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetESD3 = {"DH-BLR", "DH-DEL", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.EXPRESS, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetESD4 = {"DH-BLR", "DELHIS", LMS_PINCODE.DELHIS_DC, ShipmentType.DL, ShippingMethod.EXPRESS, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetSSD1 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetSSD2 = {"DH-DEL", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetSSD3 = {"DH-BLR", "DH-DEL", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetSSD4 = {"DH-BLR", "DH-DEL", LMS_PINCODE.DELHIS_DC, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetSSD5 = {"DH-BLR", "DELHIS", LMS_PINCODE.DELHIS_DC, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};

        //3pl
        Object[] dataset3plD1 = {"DH-BLR", "EKART", LMS_PINCODE.PUNE_EK, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, "EK", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] dataset3plD2 = {"DH-BLR", "ELC", LMS_PINCODE.PUNE_EK, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, "EK", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] dataset3plD3 = {"DH-BLR", "EKART", LMS_PINCODE.PUNE_EK, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.SDD, "EK", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] dataset3plD4 = {"DH-BLR", "EKART", LMS_PINCODE.PUNE_EK, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.NORMAL, "EK", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};

        //Invalid Pincodes
        Object[] datasetPinD1 = {"DH-BLR", "EKART", LMS_PINCODE.PUNE_EK, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.NORMAL, "EK", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetPinD2 = {"DH-BLR", "ELC", LMS_PINCODE.PUNE_EK, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.NORMAL, "EK", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetPinD3 = {"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};

        //RHD
        Object[] datasetRHDD1 = {"DH-DEL", "RHD", LMS_PINCODE.MUMBAI_DE_RHD, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, "DE", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetRHDD2 = {"DH-BLR", "ELC", LMS_PINCODE.MUMBAI_DE_RHD, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, "DE", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetRHDD3 = {"DH-BLR", "RHD", LMS_PINCODE.MUMBAI_DE_RHD, ShipmentType.DL, ShippingMethod.EXPRESS, ShippingMethod.NORMAL, "DE", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetRHDD4 = {"DH-BLR", "RHD", LMS_PINCODE.MUMBAI_DE_RHD, ShipmentType.DL, ShippingMethod.SDD, ShippingMethod.NORMAL, "DE", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.ERROR};
        Object[] datasetRHDD5 = {"DH-BLR", "RHD", LMS_PINCODE.MUMBAI_DE_RHD, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, "DE", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};

        Object[] datasetRHDD6 = {"DH-BLR", "RHD", LMS_PINCODE.NORTH_DE, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, "DE", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetRHDD7 = {"DH-BLR", "EKART", LMS_PINCODE.NORTH_DE, ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, "EK", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};
        Object[] datasetRHDD8 = {"DH-BLR", "DE", "122002", ShipmentType.DL, ShippingMethod.NORMAL, ShippingMethod.NORMAL, "DE", false, LMS_CONSTANTS.TENANTID, StatusResponse.Type.SUCCESS};

        Object[][] dataSet = new Object[][]{
                datasetFW1 , datasetFW2 , datasetFW3, datasetFW4, datasetFW5, datasetFW6, datasetFW7 , datasetFW8, datasetFW9,
                datasetTNB1, datasetTNB2, datasetTNB3, datasetTNB4, datasetTNB5, datasetTNB6, datasetTNB7, datasetTNB8, datasetTNB9,
                datasetNSD1, datasetNSD2, datasetNSD3, datasetNSD4,
                datasetESD1, datasetESD2, datasetESD3, datasetESD4,
                datasetSSD1, datasetSSD2, datasetSSD3, datasetSSD4, datasetSSD5,
                dataset3plD1, dataset3plD2, dataset3plD3, dataset3plD4,
                datasetPinD1, datasetPinD2, datasetPinD3,
                datasetRHDD1, datasetRHDD2, datasetRHDD3, datasetRHDD4,datasetRHDD5,
                datasetRHDD6, datasetRHDD7 , datasetRHDD8
        };
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object[][] MasterBagForwardMBinDiffState(ITestContext testContext) throws Exception {
        Object[] arr1 = {MasterbagStatus.CLOSED, StatusResponse.Type.ERROR};
        Object[] arr2 = {MasterbagStatus.RECEIVED, StatusResponse.Type.ERROR};
        Object[] arr3 = {MasterbagStatus.IN_TRANSIT, StatusResponse.Type.ERROR};
        Object[] arr4 = {MasterbagStatus.HANDED_OVER_TO_3PL, StatusResponse.Type.ERROR};
        Object[] arr5 = {MasterbagStatus.NEW, StatusResponse.Type.SUCCESS};
        Object[] arr6 = {MasterbagStatus.ADDED_TO_CONTAINER, StatusResponse.Type.ERROR};
        Object[] arr7 = {MasterbagStatus.ADDED_TO_TRIP, StatusResponse.Type.ERROR};
        Object[] arr8 = {MasterbagStatus.RECEIVED_AT_HANDOVER_CENTER, StatusResponse.Type.ERROR};
        Object[] arr9 = {MasterbagStatus.RECEIVED_AT_TRANSPORTER_HUB, StatusResponse.Type.ERROR};
        Object[] arr10 = {MasterbagStatus.LOST, StatusResponse.Type.ERROR};
        Object[] arr11 = {MasterbagStatus.DELIVERED, StatusResponse.Type.ERROR};
        Object[][] dataSet = new Object[][]{arr1, arr2, arr3, arr4, arr5,
                arr6, arr7, arr8, arr9, arr10, arr11};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object [][] MasterBagForwardWithDiffOrderStatus(ITestContext testContext) throws Exception {
        long masterBagId = masterBagServiceHelper.createMasterBag("DH-BLR", "ELC", ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID).getId();
        Object[] arr1 = {EnumSCM.PK,masterBagId,ShipmentType.DL,StatusResponse.Type.ERROR};
        Object[] arr2 = {EnumSCM.DL,masterBagId,ShipmentType.DL,StatusResponse.Type.ERROR};
        Object[] arr3 = {EnumSCM.RTO,masterBagId,ShipmentType.DL,StatusResponse.Type.ERROR};
        Object[] arr4 = {EnumSCM.LOST,masterBagId,ShipmentType.DL,StatusResponse.Type.ERROR};
        Object[] arr5 = {EnumSCM.OFD,masterBagId,ShipmentType.DL,StatusResponse.Type.ERROR};
        Object[] arr6 = {EnumSCM.FD,masterBagId,ShipmentType.DL,StatusResponse.Type.ERROR};
        //Object[] arr7 = {EnumSCM.CANCELLED_IN_HUB,masterBagId,ShipmentType.DL,StatusResponse.Type.ERROR};
        Object[][] dataSet = new Object[][] { arr1, arr2, arr3, arr4, arr5,arr6};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object [][] MasterBagForwardRHDWithNormalOrder(ITestContext testContext) throws Exception {
        long masterBagId = masterBagServiceHelper.createMasterBag("DH-BLR", "RHD", ShippingMethod.NORMAL, "DE", LMS_CONSTANTS.TENANTID).getId();
        Object[] arr1 = {masterBagId,28, LMS_PINCODE.ML_BLR,"ML",ShipmentType.DL,ShippingMethod.NORMAL,StatusResponse.Type.ERROR};
        Object[] arr2 = {masterBagId,28, LMS_PINCODE.PUNE_EK,"EK",ShipmentType.DL,ShippingMethod.NORMAL,StatusResponse.Type.ERROR};
        Object[] arr3 = {masterBagId,36, LMS_PINCODE.NORTH_DE,"DE",ShipmentType.DL,ShippingMethod.NORMAL,StatusResponse.Type.SUCCESS};
        Object[][] dataSet = new Object[][] {arr1, arr2, arr3};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object [][] MasterBagForwardWithCourierMismatch(ITestContext testContext) throws Exception {
        Object[] arr1 = {LMS_PINCODE.ML_BLR, "ML","EK",EnumSCM.ERROR,"EKART"};
        Object[] arr2 = {LMS_PINCODE.ML_BLR,"ML","DE",EnumSCM.ERROR,"DE"};
        Object[] arr3 = {LMS_PINCODE.MUMBAI_DE_RHD,"DE","ML",EnumSCM.ERROR,"ELC"};
        Object[] arr4 = {LMS_PINCODE.ML_BLR,"ML","IP",EnumSCM.ERROR,"IPOST"};
        Object[] arr5 = {LMS_PINCODE.PUNE_EK,"EK","ML",EnumSCM.ERROR,"ELC"};
        Object[] arr6 = {LMS_PINCODE.NORTH_DE,"DE","IP",EnumSCM.ERROR,"IPOST"};
        Object[][] dataSet = new Object[][] { arr1, arr2, arr3, arr4, arr5, arr6};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object [][] MasterBagForwardLastMilePartner(ITestContext testContext) throws Exception {
        Object[] arr1 = {"ELC","DCMLT","ML",ShipmentType.DL,ShippingMethod.NORMAL,EnumSCM.ERROR};
        Object[] arr2 = {"CCU","DCMLT","ML",ShipmentType.DL,ShippingMethod.EXPRESS,EnumSCM.ERROR};//EXPRESS
        Object[] arr3 = {"CCU","DCMLT","ML",ShipmentType.DL,ShippingMethod.SDD,EnumSCM.ERROR}; // SDD
        //Object[] arr4 = {"ELC","DCMLT","ML",ShipmentType.DL,ShippingMethod.NORMAL,EnumSCM.ERROR};
        //Object[] arr5 = {987655555,5,"DC",1802,"DC","ML",ShipmentType.DL,EnumSCM.NORMAL,EnumSCM.SUCCESS};//add again
        Object[][] dataSet = new Object[][] { arr1, arr2, arr3};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object [][] MasterBagForwardDCtoDC(ITestContext testContext) throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        String releaseId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVE_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,"cod",false, true));
        Object[] arr1 = {releaseId,"ELC","HSR","ML",ShipmentType.DL,ShippingMethod.NORMAL,EnumSCM.SUCCESS};
        Object[] arr2 = {releaseId,"ELC","DCMLT","ML",ShipmentType.DL,ShippingMethod.NORMAL,EnumSCM.ERROR};//EXPRESS
        Object[] arr3 = {releaseId,"HSR","DCMLT","ML",ShipmentType.DL,ShippingMethod.NORMAL,EnumSCM.ERROR}; // SDD
        Object[] arr4 = {releaseId,"ELC","MRD","ML",ShipmentType.DL,ShippingMethod.NORMAL,EnumSCM.SUCCESS};
        Object[][] dataSet = new Object[][] { arr1, arr2, arr3, arr4};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object [][] MasterBagReturnDCtoDC(ITestContext testContext) throws Exception {

        Map<String, Object> pickup = DBUtilities.exSelectQueryForSingleRecord(
                "select * from return_shipment where shipment_status = '" + "RETURN_SUCCESSFUL"
                        + "' and delivery_center_id = " + "" + 5 + " and courier_code = '" + "ML" + "'",
                "lms");
        Map<String, Object> pickup1 = DBUtilities.exSelectQueryForSingleRecord(
                "select * from return_shipment where shipment_status = '" + "RETURN_SUCCESSFUL"
                        + "' and delivery_center_id = " + "" + 5 + " and courier_code = '" + "ML" + "'",
                "lms");

        Object[] arr1 = {pickup.get("tracking_number"),"ELC","HSR","ML",ShipmentType.PU,ShippingMethod.NORMAL,EnumSCM.ERROR};
        Object[] arr2 = {pickup1.get("tracking_number"),"ELC","HSR","ML",ShipmentType.PU,ShippingMethod.NORMAL,EnumSCM.ERROR};
        Object[][] dataSet = new Object[][] { arr1, arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object [][] MasterBagReverseDCtoWH(ITestContext testContext) throws Exception {
        LMSHelper lmsHelper = new LMSHelper();
        OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
        String releaseId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.UNRTO, LMS_PINCODE.NORTH_CGH, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
//		Object[] arr1 = {returnId,5,"DC",28,"WH","ML",ShipmentType.PU,EnumSCM.NORMAL,EnumSCM.ERROR};
//		Object[] arr2 = {returnId,1,"DC",36,"WH","ML",ShipmentType.PU,EnumSCM.NORMAL,EnumSCM.ERROR};
//		Object[] arr3 = {returnId,28,"DC",1,"WH","ML",ShipmentType.PU,EnumSCM.NORMAL,EnumSCM.ERROR};
//		Object[] arr4 = {returnId,5,"DC",36,"WH","ML",ShipmentType.PU,EnumSCM.NORMAL,EnumSCM.SUCCESS};
        Object[] arr5 = {releaseId,"HSR","RT-BLR","ML",ShipmentType.DL,ShippingMethod.NORMAL,EnumSCM.SUCCESS};
        Object[] arr6 = {releaseId,"ELC","RT-DEL","ML",ShipmentType.DL,ShippingMethod.NORMAL,EnumSCM.ERROR};
        Object[] arr7 = {releaseId,"ELC","RT-BLR","ML",ShipmentType.DL,ShippingMethod.NORMAL,EnumSCM.SUCCESS};

        Object[][] dataSet = new Object[][] { /*arr1, arr2, arr3, arr4,*/ arr5, arr6, arr7};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object [][] MasterBagWHtoWHPU(ITestContext testContext) throws Exception {
        Object[] arr1 = {"TR-BLR","RT-DEL","ML", ShipmentType.PU, ShippingMethod.NORMAL, EnumSCM.SUCCESS};

        Object[][] dataSet = new Object[][]{arr1};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider
    public static Object [][] MasterBagReverseDCtoWHWithQuery(ITestContext testContext) throws Exception {
        long masterBagId = masterBagServiceHelper.createMasterBag("ELC", "RT-BLR",
                ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID).getId();
         Object[] arr1 = {masterBagId,EnumSCM.RETURN_IN_TRANSIT,EnumSCM.ERROR};
        Object[] arr2 = {masterBagId,EnumSCM.PICKUP_CREATED,EnumSCM.ERROR};
        Object[] arr3 = {masterBagId,EnumSCM.RETURN_REJECTED,EnumSCM.ERROR};
        Object[] arr4 = {masterBagId,EnumSCM.REJECTED_ONHOLD_PICKUP_WITH_COURIER,EnumSCM.ERROR};
        Object[] arr5 = {masterBagId,EnumSCM.PICKUP_DONE_QC_PENDING,EnumSCM.ERROR};
        Object[] arr6 = {masterBagId,EnumSCM.RETURN_RECEIVED,EnumSCM.ERROR};
        Object[] arr7 = {masterBagId,EnumSCM.OUT_FOR_PICKUP,EnumSCM.ERROR};
        Object[] arr8 = {masterBagId,EnumSCM.ONHOLD_PICKUP_WITH_DC,EnumSCM.ERROR};
        Object[][] dataSet = new Object[][] { arr1, arr2, arr3, arr4, arr5,arr6,arr7,arr8};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }

    @DataProvider/*(parallel = true)*/
    public static Object [][] getMasterBagWithParam(ITestContext testContext)
    {
        Map<String, Object> shipment = DBUtilities.exSelectQueryForSingleRecord("select id from shipment where order_count >0 order by last_modified_on DESC","lms");
        String shipmentId = shipment.get("id").toString();
        Object[] arr1 = {"?start=0&fetchSize=20&sortBy=id&sortOrder=DESC&um=true", "1002","Masterbag retrieved successfully", EnumSCM.SUCCESS};
        Object[] arr2 = {"?q="+FGTenantID+"shippingMethod.eq:EXPRESS&start=0&fetchSize=20&sortBy=id&sortOrder=DESC&um=true", "1002","Masterbag retrieved successfully", EnumSCM.SUCCESS};
        Object[] arr3 = {"?q="+FGTenantID+"status.eq:IN_TRANSIT___shippingMethod.eq:NORMAL&start=0&fetchSize=20&sortBy=id&sortOrder=DESC&um=true", "1002","Masterbag retrieved successfully", EnumSCM.SUCCESS};
        Object[] arr4 = {"?q="+FGTenantID+"status.eq:IN_TRANSIT___shippingMethod.eq:NORMAL___originPremisesType.eq:WH___originPremisesId.eq:1&start=0&fetchSize=20&sortBy=id&sortOrder=DESC&um=true", "1002","Masterbag retrieved successfully", EnumSCM.SUCCESS};
        Object[] arr5 = {"?q="+FGTenantID+"id.eq:"+shipmentId+"&start=0&fetchSize=20&sortBy=id&sortOrder=DESC&um=true", "1002","Masterbag retrieved successfully", EnumSCM.SUCCESS};
        Object[] arr6 = {"?q="+FGTenantID+"isDeleted.eq:true&start=0&fetchSize=20&sortBy=id&sortOrder=DESC&um=true", "1002","Masterbag retrieved successfully", EnumSCM.SUCCESS};
        Object[] arr7 = {"?q="+FGTenantID+"transporterId.eq:7&start=0&fetchSize=20&sortBy=id&sortOrder=DESC&um=true", "1002","Masterbag retrieved successfully", EnumSCM.SUCCESS};
//        Object[] arr8 = {"ssearch?q="+FGTenantID+"status.eq:RECEIVED_AT_HANDOVER_CENTER&start=0&fetchSize=20&sortBy=id&sortOrder=DESC&um=true", "1002","SMasterbag retrieved successfully", EnumSCM.SUCCESS};
        Object[][] dataSet = new Object[][] { arr1, arr2, arr3, arr4, arr5, arr6, arr7};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] updateMasterBag(ITestContext testContext)
    {
        Object[] arr1 = {20,"ELC", "HSR", ShippingMethod.NORMAL,"ML", "4019"};
        Object[] arr2 = {20,"ELC", "HSR", ShippingMethod.EXPRESS, "ML","4019"};
        Object[] arr3 = {20,"ELC", "HSR", ShippingMethod.EXPRESS, "ML","4019"};
        Object[][] dataSet = new Object[][] { arr1, arr2, arr3};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] closeAndShipMasterBag(ITestContext testContext)
    {
        Object[] arr1 = {"ML",LMS_PINCODE.ML_BLR,"36", ShippingMethod.EXPRESS,false};
        Object[] arr2 = {"ML",LMS_PINCODE.ML_BLR,"36", ShippingMethod.SDD,false};
        Object[] arr3 = {"ML",LMS_PINCODE.ML_BLR,"36", ShippingMethod.EXPRESS,false};
        Object[] arr4 = {"ML",LMS_PINCODE.ML_BLR,"36", ShippingMethod.NORMAL,true};
        Object[] arr5 = {"DE",LMS_PINCODE.NORTH_DE,"36", ShippingMethod.NORMAL,false};
        Object[] arr6 = {"DE",LMS_PINCODE.NORTH_DE,"36", ShippingMethod.EXPRESS,false};
        Object[][] dataSet = new Object[][] { arr1,arr2, arr3, arr4, arr5, arr6};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }

    @DataProvider
    public static Object [][] masterBagReciveClosedMB(ITestContext testContext) throws Exception {

        String orderId = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        MasterbagDomain masterbagDomain=masterBagServiceHelper.createMasterBag("DH-DLR","ELC",ShippingMethod.NORMAL,"ML",LMS_CONSTANTS.TENANTID);
        long masterbagIdNew = masterbagDomain.getId();
        masterBagServiceHelper.addShipmentsToMasterBag(masterbagIdNew,trackingNumber,ShipmentType.DL,LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatus(masterbagIdNew,ShipmentStatus.NEW.toString());
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        long masterBagIdClosed = Long.parseLong(lmsHelper.getMasterBagId("" + packetId1));
        statusPollingValidator.validateShipmentBagStatus(masterBagIdClosed,ShipmentStatus.CLOSED.toString());
        Object[] arr1 = {masterbagIdNew,packetId};
        Object[] arr2 = {masterBagIdClosed,packetId1};
        Object[][] dataSet = new Object[][] { arr1, arr2};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 9, 9);
    }


}
