package com.myntra.apiTests.erpservices.masterbag.test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.CourierCode;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Helper.HubToTransportHubConfigResponse;
import com.myntra.apiTests.erpservices.lms.Helper.LMSHelper;
import com.myntra.apiTests.erpservices.lms.Helper.LmsServiceHelper;
import com.myntra.apiTests.erpservices.lms.Helper.TMSServiceHelper;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.returnComplete.client.MasterBagClient;
import com.myntra.apiTests.erpservices.sortation.helpers.LastmileOperations;
import com.myntra.commons.exception.ManagerException;
import com.myntra.lms.client.response.*;
import com.myntra.lms.client.status.OrderShipmentAssociationStatus;
import com.myntra.lms.client.status.PremisesType;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.test.commons.service.Svc;
import com.myntra.tms.container.ContainerEntry;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.container.ContainerStatus;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import edu.emory.mathcs.backport.java.util.Arrays;
import org.apache.tika.exception.TikaException;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;

public class MasterBagTest implements StatusPoller {
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private LMSClient lmsClient = new LMSClient();
    private StoreValidator storeValidator = new StoreValidator();
    private MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    private StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    private StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    LastmileOperations lastmileOperations=new LastmileOperations();

    @Test(enabled = true, priority = 1, description = "C22044 : create order and add into MB, close MB, reopen MB and add another order it should allow and skip the tms operation and receive it in DC and do master bag inscan")
    public void testMasterBagReopenFunctionality() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        String hubCode = orderResponse.getOrders().get(0).getDispatchHubCode();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.INSCANNED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();

        //Create 2nd order
        String orderId = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetID = omsServiceHelper.getPacketId(orderId);
        //validate OrderToShip status
        OrderResponse orderResponse0 = lmsClient.getOrderByOrderId(packetID);
        String trackingNo1 = orderResponse0.getOrders().get(0).getTrackingNumber();

        //create masterbag
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag(hubCode, deliveryCenterCode, ShippingMethod.NORMAL, CourierCode.ML.toString(), LMS_CONSTANTS.TENANTID);
        Long masterBagId = masterbagDomain.getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());
        //assign 1st order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNo, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse masterbagUpdateResponse1 = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse1.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //Reopen the MasterBag
        masterBagServiceHelper.reopenMasterBag(masterBagId);
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //assign 2nd order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse0 = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNo1, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse0.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");

        //close Master bag
        MasterbagUpdateResponse masterbagUpdateResponse2 = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse2.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        lmsServiceHelper.processOrder_AMB_TO_Shipped(masterBagId, "TH-BLR", LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatusRespectToOrder(masterBagId, EnumSCM.RECEIVED);
    }

    @Test(enabled = true, priority = 2, description = "C22044 : mark MB status to IN_TRANSIT from CLOSED State")
    public void testMasterBagIntransitFunctionality() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        String hubCode = orderResponse.getOrders().get(0).getDispatchHubCode();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.INSCANNED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();

        //create masterbag
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag(hubCode, deliveryCenterCode, ShippingMethod.NORMAL, CourierCode.ML.toString(), LMS_CONSTANTS.TENANTID);
        Long masterBagId = masterbagDomain.getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());
        //assign order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNo, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse masterbagUpdateResponse2 = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse2.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        masterBagServiceHelper.markMasterbagStatusInTransit(masterBagId);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());

        lmsServiceHelper.processOrder_AMB_TO_Shipped(masterBagId, "TH-BLR", LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatusRespectToOrder(masterBagId, EnumSCM.RECEIVED);
    }

    @Test(enabled = true, priority = 3, description = "C24046 : Verify the order status in master bag shortage scenario using force(Admin) update")
    public void process_MasterBagSortageOrder_Successful() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        String hubCode = orderResponse.getOrders().get(0).getDispatchHubCode();
        String zipCode = orderResponse.getOrders().get(0).getZipcode();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.INSCANNED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //create master bag
        MasterbagDomain masterbagDomain1 = masterBagServiceHelper.createMasterBag(hubCode, deliveryCenterCode, ShippingMethod.NORMAL, CourierCode.ML.toString(), LMS_CONSTANTS.TENANTID);
        Long masterBagId1 = masterbagDomain1.getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());
        //assign order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId1, trackingNo, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse masterbagUpdateResponse1 = masterBagServiceHelper.closeMasterBag(masterBagId1, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse1.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse1 = storeHelper.getMappedTransportHubDetails(LMS_CONSTANTS.TENANTID, hubCode);
        Assert.assertTrue(hubToTransportHubConfigResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub1 = hubToTransportHubConfigResponse1.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //Receive MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(transporterHub1, masterBagId1, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());
        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = storeHelper.getMappedTransportHubDetails(LMS_CONSTANTS.TENANTID, deliveryCenterCode);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterCode, transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse1 = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transporterHub, deliveryCenterCode, laneId, transporterId));
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse1.getContainerEntries();
        long containerId1 = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId1 = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId1));

        //Add MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId1, masterBagId1);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());

        //Ship container
        ContainerResponse containerResponse = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId1);
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());

        //intransit scan
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId1, deliveryCenterCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //Receive MB from Container
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(deliveryCenterCode, masterBagId1, containerId1);
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());

        //order scan after DC
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(deliveryCenterCode, masterBagId1, containerId1);

        //mark master bag status as Shortage
        lmsServiceHelper.updateOrderStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.RECEIVED, deliveryCenterId, PremisesType.DC,
                trackingNo, com.myntra.lms.client.status.OrderShipmentAssociationStatus.SHORTAGE);
        statusPollingValidator.validateShipmentBagStatusRespectToOrder(masterBagId1, EnumSCM.SHORTAGE);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.EXPECTED_IN_DC);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);

        //create master bag
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag(hubCode, deliveryCenterCode, ShippingMethod.NORMAL, CourierCode.ML.toString(), LMS_CONSTANTS.TENANTID);
        Long masterBagId2 = masterbagDomain.getId();
        storeValidator.isNull(String.valueOf(masterBagId2));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());
        //assign order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse01 = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId2, trackingNo, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse01.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse masterbagUpdateResponse02 = masterBagServiceHelper.closeMasterBag(masterBagId2, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse02.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        lmsServiceHelper.processOrder_AMB_TO_Shipped(masterBagId2, transporterHub1, LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatusRespectToOrder(masterBagId2, EnumSCM.RECEIVED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.UNASSIGNED);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);

        //deliver the excess MB order
        lastmileOperations.deliverOrders(zipCode, Arrays.asList(new String[] {trackingNo}));
    }

    @Test(enabled = true, priority = 4, description = "C24046 : Verify the order status in master bag Excess scenario")
    public void process_MasterBagExcessOrder_Successful() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        String hubCode = orderResponse.getOrders().get(0).getDispatchHubCode();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.INSCANNED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        String zipCode = orderResponse.getOrders().get(0).getZipcode();
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();

        //create masterbag
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag(hubCode, deliveryCenterCode, ShippingMethod.NORMAL, CourierCode.ML.toString(), LMS_CONSTANTS.TENANTID);
        Long masterBagId = masterbagDomain.getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());
        //assign order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNo, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse masterbagUpdateResponse1 = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse1.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse1 = storeHelper.getMappedTransportHubDetails(LMS_CONSTANTS.TENANTID, hubCode);
        Assert.assertTrue(hubToTransportHubConfigResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub1 = hubToTransportHubConfigResponse1.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //Receive MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(transporterHub1, masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = storeHelper.getMappedTransportHubDetails(LMS_CONSTANTS.TENANTID, deliveryCenterCode);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterCode, transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transporterHub, deliveryCenterCode, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());

        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());

        //intransit scan
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, deliveryCenterCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        //Receive MB from Container
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply(deliveryCenterCode, masterBagId, containerId);
        Assert.assertEquals(tmsMasterbagReponse1.getStatus().getStatusMessage(),"Masterbag Received Successfully","tms masterbag is not retrieved successfully");
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());

        //order scan after DC
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(deliveryCenterCode, masterBagId, containerId);

        //mark master bag status as Shortage
        lmsServiceHelper.updateOrderStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.RECEIVED, deliveryCenterId, PremisesType.DC,
                trackingNo, OrderShipmentAssociationStatus.EXCESS);
        statusPollingValidator.validateShipmentBagStatusRespectToOrder(masterBagId, "EXCESS");
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.EXPECTED_IN_DC);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.SHIPPED);
        //receive shipments in DC
        lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(masterBagId, trackingNo, "Electronics City (ELC)-DC", deliveryCenterId, ShipmentType.DL, PremisesType.DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,LMS_CONSTANTS.TENANTID,EnumSCM.UNASSIGNED);

        //deliver the excess MB order
        lastmileOperations.deliverOrders(zipCode, Arrays.asList(new String[] {trackingNo}));
    }

    @Test(enabled = true, priority = 5, description = "create B2B Shipment and check the auto masterbagging functionality and validate MB status as CLOSED")
    public void validate_AutoMasterbagging_Functionality() throws IOException, TikaException, SAXException, InterruptedException, JSONException, ManagerException, XMLStreamException, JAXBException {
        String clientId = LMS_CONSTANTS.B2B_CLIENTID;
        String warehouseId = LMS_CONSTANTS.WAREHOUSE_36;
        String rtoWarehouseId = LMS_CONSTANTS.WAREHOUSE_36;
        String integrationId = "2297";
        String sourcePath = "WMS";
        String pincode = "100002";
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        String courierCode = "ML";
        int noOfItem = 2;
        boolean isCod = false;
        String destinationPremisesId = "28";
        String shipmentType = "B2B";
        String statusMessage = "Shipment Creation Successful";
        String statusType = EnumSCM.SUCCESS;
        String cod = "";
        String trackingNumber = "";
        if (isCod) {
            cod = "cod";
        }
        trackingNumber = lmsServiceHelper.getTrackingNumber(courierCode, warehouseId, cod, pincode, "").getTrackingNumberEntry().getTrackingNumber();
        Svc service = tmsServiceHelper.createShipment(clientId, trackingNumber, warehouseId, rtoWarehouseId, integrationId, sourcePath, pincode, shippingMethod, courierCode, noOfItem, isCod, destinationPremisesId, shipmentType);
        lmsServiceHelper.validateCreateShipment(service, statusMessage, statusType);
        Map<String, Object> dbUtilesData = DBUtilities.exSelectQueryForSingleRecord("select * from order_to_ship where tracking_number='" + trackingNumber + "'", "lms");
        String orderId = (String) dbUtilesData.get("order_id");
        String hubCode = (String) dbUtilesData.get("dispatch_hub_code");
        String destinationHubCode = (String) dbUtilesData.get("destination_hub_code");
        if (APIUtilities.getElement(service.getResponseBody(), "status.statusMessage", "json").equalsIgnoreCase("Shipment Creation Successful")) {
            //Data from DB Validation Using API
            OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsUsingTrackingNumber(trackingNumber);
            Assert.assertEquals(orderResponse.getOrders().get(0).getTrackingNumber(), trackingNumber, "Tracking number mismatch");
            Assert.assertEquals(orderResponse.getOrders().get(0).getWarehouseId(), warehouseId, "Warehouse mismatch");
            Assert.assertEquals(orderResponse.getOrders().get(0).getRtoWarehouseId(), rtoWarehouseId, "RTO Ware house Id mismatch");
        }

        //inscan the order
        lmsServiceHelper.orderInscanByTrackingNumber(trackingNumber, "36", false, LMS_CONSTANTS.TENANTID);
        OrderResponse orderResponse = lmsServiceHelper.getOrderDetailsByOrderId(orderId, LMS_CONSTANTS.TENANTID, "9999");
        Assert.assertEquals(orderResponse.getOrders().get(0).getPlatformShipmentStatus().toString(), ShipmentStatus.INSCANNED.toString());

        //AutoMaster bagging
        ShipmentResponse shipmentResponse = tmsServiceHelper.autoMasterBag(trackingNumber);
        Assert.assertEquals(shipmentResponse.getStatus().getStatusMessage(), "Success", "Auto masterbagging is not updated");
        Long shipmentId = shipmentResponse.getEntries().get(0).getId();
        storeValidator.isNull(String.valueOf(shipmentId));
        //validate order to ship status
        OrderResponse orderResponse1 = lmsServiceHelper.getOrderDetailsByOrderId(orderId, LMS_CONSTANTS.TENANTID, "9999");
        Assert.assertEquals(orderResponse1.getOrders().get(0).getPlatformShipmentStatus().toString(), ShipmentStatus.ADDED_TO_MB.toString());
        //validate master bag status
        statusPollingValidator.validateShipmentBagStatus(shipmentId, EnumSCM.CLOSED);

        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply("TH-BLR", shipmentId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive masterBag in TMS HUB");

    }

    @Test(enabled = true, priority = 6, description = "C22033 : Multiple MB in same Container and receive in DC")
    public void process_multipleMBInSingleContainer_ReceiveInDC_Successful() throws Exception {
        //Create 1st Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId1);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase("ORDER(s) retrieved successfully"), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.INSCANNED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        //get order tracking number
        String trackingNo1 = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //Create 2nd Mock order Till DL
        String orderID2 = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId2 = omsServiceHelper.getPacketId(orderID2);
        //validate OrderToShip status
        statusPollingValidator.validateOrderStatus(packetId2, ShipmentStatus.INSCANNED);

        //get order tracking number
        OrderResponse orderResponse1 = lmsClient.getOrderByOrderId(packetId2);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase("ORDER(s) retrieved successfully"), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.INSCANNED);
        String trackingNo2 = orderResponse1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo2);

        //create master bag
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag("DH-BLR", deliveryCenterCode, ShippingMethod.NORMAL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LMS_CONSTANTS.TENANTID);
        Long masterBagId = masterbagDomain.getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //assign 1st order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNo1, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse masterbagUpdateResponse1 = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse1.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //create 2nd master bag
        MasterbagDomain masterbagDomain1 = masterBagServiceHelper.createMasterBag("DH-BLR", deliveryCenterCode, ShippingMethod.NORMAL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LMS_CONSTANTS.TENANTID);
        Long masterBagId1 = masterbagDomain1.getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //assign 2nd order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse01 = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId1, trackingNo2, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse01.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse masterbagUpdateResponse03 = masterBagServiceHelper.closeMasterBag(masterBagId1, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse03.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = storeHelper.getMappedTransportHubDetails(LMS_CONSTANTS.TENANTID, "DH-BLR");
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();

        //Receive 1st MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(transporterHub, masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate 2nd MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());

        //Receive 2nd MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(transporterHub, masterBagId1, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tmsMasterbagReponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());

        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(transporterHub, deliveryCenterCode)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transporterHub, deliveryCenterCode, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add 1st MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");

        //Add 2nd MB to container
        ContainerResponse containerResponse3 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId1);
        Assert.assertEquals(containerResponse3.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");

        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, deliveryCenterCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to do intransit scan of the container");

        //Receive container in DC
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(deliveryCenterCode, masterBagId, containerId);
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(deliveryCenterCode, masterBagId1, containerId);

        //MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());

        //validate order status
        statusPollingValidator.validateOrderStatus(packetId1, ShipmentStatus.SHIPPED);
        statusPollingValidator.validateOrderStatus(packetId2, ShipmentStatus.SHIPPED);

        //validate container status
        statusPollingValidator.validateContainerStatusInTMS(containerId, transporterHub, ContainerStatus.RECEIVED.toString());
    }

    @Test(enabled = true, priority = 7, description = "C22033 : Multiple MB in different Container and receive in DC")
    public void process_multipleMBInMultipleContainer_ReceiveInDC_Successful() throws Exception {

        //Create 1st Mock order Till DL
        String orderID1 = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderID1);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId1);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase("ORDER(s) retrieved successfully"), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.INSCANNED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        //get order tracking number
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //Create 2nd Mock order Till DL
        String orderID2 = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId2 = omsServiceHelper.getPacketId(orderID2);
        OrderResponse orderResponse1 = lmsClient.getOrderByOrderId(packetId2);
        Assert.assertTrue(orderResponse1.getStatus().getStatusMessage().equalsIgnoreCase("ORDER(s) retrieved successfully"), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse1, ShipmentStatus.INSCANNED);
        //get order tracking number
        String trackingNo1 = orderResponse1.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo1);

        //create master bag
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag("DH-BLR", deliveryCenterCode, ShippingMethod.NORMAL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LMS_CONSTANTS.TENANTID);
        Long masterBagId = masterbagDomain.getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //assign 1st order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNo, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse masterbagUpdateResponse1 = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse1.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //create 2nd master bag
        MasterbagDomain masterbagDomain1 = masterBagServiceHelper.createMasterBag("DH-BLR", deliveryCenterCode, ShippingMethod.NORMAL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LMS_CONSTANTS.TENANTID);
        Long masterBagId1 = masterbagDomain1.getId();
        storeValidator.isNull(String.valueOf(masterBagId1));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //assign 2nd order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse01 = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId1, trackingNo1, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse01.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse masterbagUpdateResponse03 = masterBagServiceHelper.closeMasterBag(masterBagId1, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse03.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = storeHelper.getMappedTransportHubDetails(LMS_CONSTANTS.TENANTID, "DH-BLR");
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();

        //Receive 1st MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse)tmsServiceHelper.receiveMasterBagInTMS.apply(transporterHub, masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate 2nd MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());

        //Receive 2nd MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(transporterHub, masterBagId1, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tmsMasterbagReponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());

        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(transporterHub, deliveryCenterCode)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create 1st container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transporterHub, deliveryCenterCode, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));
        statusPollingValidator.validateContainerStatusInTMS(containerId, transporterHub, ContainerStatus.NEW.toString());

        //Add 1st MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");

        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");

        //Create 2st container
        ContainerResponse containerResponse01 = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transporterHub, deliveryCenterCode, laneId, transporterId));
        Assert.assertTrue(containerResponse01.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry1 = containerResponse01.getContainerEntries();
        long containerId1 = 0;
        if (!(lstContainerEntry1.size() == 0)) {
            containerId1 = lstContainerEntry1.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId1));
        statusPollingValidator.validateContainerStatusInTMS(containerId1, transporterHub, ContainerStatus.NEW.toString());

        //Add 2nd MB to container
        ContainerResponse containerResponse3 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId1, masterBagId1);
        Assert.assertEquals(containerResponse3.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");

        //Ship container
        ContainerResponse containerResponse0 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId1);
        Assert.assertTrue(containerResponse0.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");

        //Intransit scan
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, deliveryCenterCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to do intransit scan of the container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId1, deliveryCenterCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to do intransit scan of the container");

        //Receive container in DC
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(deliveryCenterCode, masterBagId, containerId);
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(deliveryCenterCode, masterBagId1, containerId1);

        //MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());

        //validate order status
        statusPollingValidator.validateOrderStatus(packetId1, ShipmentStatus.SHIPPED);
        statusPollingValidator.validateOrderStatus(packetId2, ShipmentStatus.SHIPPED);

        //validate container status
        statusPollingValidator.validateContainerStatusInTMS(containerId, transporterHub, ContainerStatus.RECEIVED.toString());
        statusPollingValidator.validateContainerStatusInTMS(containerId1, transporterHub, ContainerStatus.RECEIVED.toString());
    }

    @Test(enabled = true, priority = 8, description = "C22036 : TKT NO:- 2165 Restrict receiving shipments if masterBag is scanned wrong route  ")
    public void process_ReceiveContainerInDifferentDC_CheckErrorMessage_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase("ORDER(s) retrieved successfully"), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.INSCANNED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        //get order tracking number
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //create master bag
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag("DH-BLR", deliveryCenterCode, ShippingMethod.NORMAL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LMS_CONSTANTS.TENANTID);
        Long masterBagId = masterbagDomain.getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //assign  order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNo, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse masterbagUpdateResponse1 = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse1.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = storeHelper.getMappedTransportHubDetails(LMS_CONSTANTS.TENANTID, "DH-BLR");
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();

        //Receive  MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(transporterHub, masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate 2nd MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());

        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(transporterHub, deliveryCenterCode)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create  container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(transporterHub, deliveryCenterCode, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));
        statusPollingValidator.validateContainerStatusInTMS(containerId, transporterHub, ContainerStatus.NEW.toString());

        //Add  MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");

        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");

        //Intransit scan
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.containerInTransitScan.apply(containerId, deliveryCenterCode)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to do intransit scan of the container");

        //Receive container/Masterbag in other DC
        TMSMasterbagReponse tmsMasterbagReponse1 = (TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBagNew.apply("HSR", masterBagId, containerId);
        Assert.assertEquals(tmsMasterbagReponse1.getStatus().getStatusMessage(), "Show warning", "Master bag was received in different DC, expected DC is " + deliveryCenterCode);
    }

    @Test(enabled = true, priority = 9, dataProviderClass = LMSTestsDP.class, dataProvider = "DCtoDC_usingTMS", description = "C22038 : To check and verify Admin corrections after DC to DC transfer(ELC DC --> CAR DC), master Bag should be lost  ")
    public void DCtoDC_ForDLOrders_LOST(String pincode, long source, String sourceType, long dest, String destType, String shippingMethod, String courierCode, ShipmentType shipmentType, String statusType) throws Exception {

        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.SH, pincode, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        String tracking_num = lmsHelper.getTrackingNumber(packetId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.UNASSIGNED);

        // create MB from DC to DC and validate mlShipment status to be ASSIGNED_TO_OTHER_DC
        long masterBagId = lmsServiceHelper.createMasterBag(source, sourceType, dest, destType, shippingMethod, courierCode).getEntries().get(0).getId();
        Assert.assertTrue(lmsServiceHelper.addAndSaveMasterBag(packetId, "" + masterBagId, shipmentType).equals(statusType),
                "Unable to add order to MB");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.ASSIGNED_TO_OTHER_DC);

        Assert.assertEquals(lmsServiceHelper.closeMasterBag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to close the MasterBag");
        TMSMasterbagReponse masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId));

        // Add MB to container DCto DC , Ship it and validate mlShipment status to be EXPECTED_IN_DC
        String sourceHub = masterBag.getMasterbagEntries().get(0).getLastScannedHub();
        String destHub = masterBag.getMasterbagEntries().get(0).getDestinationHub();
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(sourceHub, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive masterBag in TMS HUB");
        Assert.assertEquals(((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId)).getMasterbagEntries().get(0).getStatus().toString(), EnumSCM.NEW, "Status not in NEW");
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(sourceHub, destHub)).getLaneEntries().get(1).getId();
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();
        System.out.println(laneId + "," + transporterId);
        long containerId = ((ContainerResponse) tmsServiceHelper.createContainer.apply(sourceHub, destHub, laneId, transporterId)).getContainerEntries().get(0).getId();
        Assert.assertEquals(((ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add masterBag to Container");
        ExceptionHandler.handleEquals(((ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertNotNull(((ContainerResponse) tmsServiceHelper.getContainer.apply(containerId)).getContainerEntries().get(0).getMasterbagEntries());
        tmsServiceHelper.tmsReceiveMasterBagNew.apply(destHub, masterBagId, containerId);
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.EXPECTED_IN_DC);

        //  Receive MB in DC and validate mlShipment status to be UNASSIGNED
        lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED, "Bangalore", 42, "DC");
        lmsServiceHelper.masterBagInScanUpdate(masterBagId, packetId, "Bangalore",
                42, "DC", 42);
        Assert.assertEquals(((ShipmentResponse) lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId)).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(tracking_num, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.UNASSIGNED);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());

        //mark MB as LOST
        String masterbagResponse = masterBagServiceHelper.updateMasterBagAsLost(masterBagId);
        Assert.assertEquals(masterbagResponse, ResponseMessageConstants.masterBagUpdate, "Master bag status is not updated successfully");

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.LOST.toString());
    }

    @Test(enabled = true, priority = 9, description = "C22116 : Ship MB using Admin API (Admin For Forward).")
    public void process_ShipMasterBagWhichIsInRECEIVEDState_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase("ORDER(s) retrieved successfully"), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.INSCANNED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        //get order tracking number
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //create master bag
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag("DH-BLR", deliveryCenterCode, ShippingMethod.NORMAL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LMS_CONSTANTS.TENANTID);
        Long masterBagId = masterbagDomain.getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //assign  order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNo, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse closeMasterBag = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(closeMasterBag.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = storeHelper.getMappedTransportHubDetails(LMS_CONSTANTS.TENANTID, "DH-BLR");
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();

        //Receive  MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(transporterHub, masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());

        //ship masterBag
        String shipmentResponse=masterBagServiceHelper.shipMasterBag(masterBagId);
        Assert.assertTrue(shipmentResponse.contains("Orders shipped successfully"),"Master bag Ship operation failed -"+shipmentResponse);
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());
        statusPollingValidator.validateOrderStatus(packetId,EnumSCM.SHIPPED);
        //receive shipments in DC
        lmsServiceHelper.receiveShipmentByTrackingNumberMasterBagInscanV2(masterBagId, trackingNo, "Electronics City (ELC)-DC", deliveryCenterId, ShipmentType.DL, PremisesType.DC);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,LMS_CONSTANTS.TENANTID,EnumSCM.UNASSIGNED);

    }

    @Test(enabled = true, priority = 10, description = "C22072 : Ship MB which is in CLOSED state using Admin API (Admin For Forward).")
    public void process_ShipMasterBagWhichIsInCLOSEDState_Successful() throws Exception {

        //Create Mock order Till DL
        String orderID = lmsHelper.createMockOrder(EnumSCM.IS, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase("ORDER(s) retrieved successfully"), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.INSCANNED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        //get order tracking number
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        storeValidator.isNull(trackingNo);

        //create master bag
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag("DH-BLR", deliveryCenterCode, ShippingMethod.NORMAL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LMS_CONSTANTS.TENANTID);
        Long masterBagId = masterbagDomain.getId();
        storeValidator.isNull(String.valueOf(masterBagId));

        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //assign  order to Master bag
        MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNo, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(masterbagUpdateResponse.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Order is not added to shipment");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());

        //close Master bag
        MasterbagUpdateResponse closeMasterBag = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(closeMasterBag.getStatus().getStatusType().toString().equalsIgnoreCase("SUCCESS"), "Shipment was not closed");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //ship masterBag
        String shipmentResponse=masterBagServiceHelper.shipMasterBag(masterBagId);
        Assert.assertTrue(shipmentResponse.contains("ERROR"),"Master bag Ship operation Passed when its status as CLOSED");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());
        statusPollingValidator.validateOrderStatus(packetId,EnumSCM.ADDED_TO_MB);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo,LMS_CONSTANTS.TENANTID,EnumSCM.EXPECTED_IN_DC);

    }




    }
