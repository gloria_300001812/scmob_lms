package com.myntra.apiTests.erpservices.masterbag.dp;

import com.myntra.lordoftherings.Toolbox;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

/**
 * @author Shanmugam Yuvaraj
 * @since Aug-2019
 */
public class MasterBagV2SearchDP {

    @DataProvider
    public static Object[][] search_BasedOn_Diff_Status(ITestContext testContext) {
        Object[] arr1 = {"NEW", "HUB", "18", "DC", "5"};
        Object[] arr2 = {"IN_TRANSIT", "HUB", "18", "DC", "5"};
        Object[] arr3 = {"RECEIVED", "HUB", "18", "DC", "5"};
        Object[] arr4 = {"LOST", "HUB", "18", "DC", "5"};
        Object[] arr5 = {"CLOSED", "HUB", "18", "DC", "5"};

        Object[][] dataSet = new Object[][]{arr1, arr2, arr3, arr4, arr5};
        return Toolbox.returnReducedDataSet(dataSet, testContext.getIncludedGroups(), 30, 30);
    }
}
