package com.myntra.apiTests.erpservices.masterbag.test;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.common.Constants.ReleaseStatus;
import com.myntra.apiTests.common.ExceptionHandler;
import com.myntra.apiTests.common.ProcessOrder.Service.ProcessRelease;
import com.myntra.apiTests.common.entries.ReleaseEntry;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lastmile.helpers.StoreHelper;
import com.myntra.apiTests.erpservices.lastmile.service.MasterBagClient_QA;
import com.myntra.apiTests.erpservices.lastmile.service.TripClient_QA;
import com.myntra.apiTests.erpservices.lastmile.validator.StoreValidator;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.apiTests.erpservices.lms.Helper.*;
import com.myntra.apiTests.erpservices.lms.client.LMSClient;
import com.myntra.apiTests.erpservices.lms.dp.LMSTestsDP;
import com.myntra.apiTests.erpservices.lms.validators.StatusPollingValidator;
import com.myntra.apiTests.erpservices.masterbag.dp.MasterBagMigratedDP;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagServiceHelper;
import com.myntra.apiTests.erpservices.masterbagservice.MasterBagValidator;
import com.myntra.apiTests.erpservices.oms.OMSServiceHelper;
import com.myntra.apiTests.erpservices.rms.RMSServiceHelper;
import com.myntra.apiTests.erpservices.sortation.helpers.LMSOperations;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.lastmile.client.code.utils.TripStatus;
import com.myntra.lastmile.client.entry.DeliveryStaffEntry;
import com.myntra.lastmile.client.entry.TripEntry;
import com.myntra.lastmile.client.response.DeliveryStaffResponse;
import com.myntra.lastmile.client.response.TripOrderAssignmentResponse;
import com.myntra.lastmile.client.response.TripResponse;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.ShipmentResponse;
import com.myntra.lms.client.response.TransporterResponse;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.masterbag.core.MasterbagStatus;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagResponse;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.oms.client.entry.PacketEntry;
import com.myntra.returns.common.enums.RefundMode;
import com.myntra.returns.common.enums.ReturnMode;
import com.myntra.returns.common.enums.ReturnType;
import com.myntra.returns.response.ReturnResponse;
import com.myntra.test.commons.testbase.BaseTest;
import com.myntra.tms.container.ContainerEntry;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagEntry;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.myntra.apiTests.end2end.FetchEnvUtil.getEnvironment;
import static org.testng.Assert.assertTrue;


/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class NewMasterBagMigrated extends BaseTest {
    static String env = getEnvironment();
    private static org.slf4j.Logger log = LoggerFactory.getLogger(com.myntra.apiTests.erpservices.lms.tests.LMS_Masterbag.class);
    private LmsServiceHelper lmsServiceHelper = new LmsServiceHelper();
    private LMSHelper lmsHelper = new LMSHelper();
    private OMSServiceHelper omsServiceHelper = new OMSServiceHelper();
    private RMSServiceHelper rmsServiceHelper = new RMSServiceHelper();
    private TMSServiceHelper tmsServiceHelper = new TMSServiceHelper();
    private LMS_ReturnHelper lmsReturnHelper = new LMS_ReturnHelper();
    private ProcessRelease processRelease = new ProcessRelease();
    MasterBagClient_QA masterBagClient_qa = new MasterBagClient_QA();
    private static MasterBagServiceHelper masterBagServiceHelper = new MasterBagServiceHelper();
    private MasterBagValidator masterBagValidator = new MasterBagValidator();
    LMSOperations lmsOperations = new LMSOperations();
    LMSClient lmsClient = new LMSClient();

    StoreValidator storeValidator = new StoreValidator();
    StoreHelper storeHelper = new StoreHelper(getEnvironment(), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.TENANTID);
    private LMS_ReturnHelper lms_returnHelper = new LMS_ReturnHelper();
    private static TripClient_QA tripClient_qa = new TripClient_QA();
    private static StatusPollingValidator statusPollingValidator = new StatusPollingValidator();
    ReturnHelper returnHelper = new ReturnHelper();


    @Test(groups = {"MasterBag", "P0", "Smoke", "Regression"},
            dataProvider = "MasterBagShippingMethod", dataProviderClass = MasterBagMigratedDP.class,
            description = "MasterBag with different shipping methods", enabled = true)
    public void MasterBagForwardDL(String originHubCode, String destinationHubCode, String pincode, ShipmentType shipmentType,
                                   ShippingMethod mbShippingMethod, ShippingMethod orderShippingMethod, String courierCode,
                                   boolean isTryAndBuy, String tenantId, StatusResponse.Type statusType) throws Exception {
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse addShipmentToMbResponse;
        MasterbagUpdateResponse closeMasterbagUpdateResponse;
        String trackingNumber;
        Long masterBagId;
        trackingNumber = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.IS, pincode, courierCode, "36", orderShippingMethod.toString(),
                "cod", isTryAndBuy, true)));
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, mbShippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, mbShippingMethod, courierCode, tenantId);
        System.out.println("masterBagId=" + (masterBagId = mbCreateResponse.getId()));
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, shipmentType, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), statusType.toString(),
                "Unable to add shipment to MB");
        if (statusType == StatusResponse.Type.ERROR) {
            masterBagValidator.validateIsEmptyMasterBag(masterBagId);
        } else {
            closeMasterbagUpdateResponse = masterBagServiceHelper.closeMasterBag(masterBagId, tenantId);
            masterBagValidator.validateCloseMasterBag(closeMasterbagUpdateResponse, masterBagId, trackingNumber);
        }
    }

    @Test(groups = {"MasterBag", "Smoke", "Regression"}, dataProvider = "MasterBagForwardMBinDiffState",dataProviderClass = MasterBagMigratedDP.class)
    public void MasterBagForwardMBinDiffState(MasterbagStatus masterBagStatus, StatusResponse.Type statusType)
            throws Exception {
        String trackingNumber;
        Long masterBagId;
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse addShipmentToMbResponse;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        trackingNumber = lmsHelper.getTrackingNumber(
                omsServiceHelper.getPacketId(
                        lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, LMSConstants.IN_HOUSE_COURIER, "36", shippingMethod.toString(),
                                "cod", false, true)));
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        System.out.println("masterBagId=" + (masterBagId = mbCreateResponse.getId()));
        DBUtilities.exUpdateQuery("update shipment set status = '" + masterBagStatus.name() + "' where id = " + masterBagId, "lms");
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), statusType.toString(),
                "Unable to add shipment to MB");
    }

    @Test(groups = {"MasterBag", "Smoke", "Regression"},dataProvider = "MasterBagForwardWithDiffOrderStatus", dataProviderClass = MasterBagMigratedDP.class,
            description = "ID: C315 , MasterBagForwardWithDiffOrderStatus", enabled = true)
    public void MasterBagForwardWithDiffOrderStatus(String toStatus, long masterBagId, ShipmentType shipmentType,
                                                    StatusResponse.Type statusType) throws Exception {
        String trackingNumber;
        MasterbagUpdateResponse addShipmentToMbResponse;
        String orderId = lmsHelper.createMockOrder(toStatus, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod",
                false, true);
        trackingNumber = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(orderId));
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, shipmentType, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), statusType.toString(),
                "Unable to add shipment to MB");
    }


    @Test(groups = {"MasterBag", "Smoke",
            "Regression"}, dataProvider = "MasterBagForwardRHDWithNormalOrder", dataProviderClass = MasterBagMigratedDP.class,
            description = "ID: C318 , MasterBagForwardRHD", enabled = true)
    public void MasterBagForwardRHDWithNormalOrder(long masterBagId, long warehouseID, String pincode,
                                                   String courierCode, ShipmentType shipmentType, ShippingMethod shippingMethod, StatusResponse.Type statusType) throws Exception {
        String trackingNumber;
        MasterbagUpdateResponse addShipmentToMbResponse;
        String orderId = lmsHelper.createMockOrder(EnumSCM.IS, pincode, courierCode, String.valueOf(warehouseID), EnumSCM.NORMAL, "cod",
                false, true);
        trackingNumber = lmsHelper.getTrackingNumber(omsServiceHelper.getPacketId(orderId));
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, shipmentType, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), statusType.toString(),
                "Unable to add shipment to MB");
    }

    @Test(groups = {"MasterBag", "P0", "Smoke",
            "Regression"}, dataProviderClass = MasterBagMigratedDP.class, dataProvider = "MasterBagForwardWithCourierMismatch", description = "ID: C353 , add ML shipment order with different courier DC and check the Error message",
            enabled = true)
    public void MasterBagForwardWithCourierMismatch(String pincode, String fromCourier, String toCourier,
                                                    String statusType, String destinationHubId) throws Exception {

        String orderId = lmsHelper.createMockOrder(EnumSCM.IS, pincode, fromCourier, "36", EnumSCM.NORMAL, "cod", false,
                true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        String originHubCode = orderResponse.getOrders().get(0).getDispatchHubCode();
        long masterBagId = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubId, ShippingMethod.NORMAL, "DE", LMS_CONSTANTS.TENANTID).getId();
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), statusType,
                "Unable to add shipment to MB");
    }


    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},dataProviderClass = MasterBagMigratedDP.class, dataProvider = "MasterBagForwardLastMilePartner", description = "ID: C319 , MasterBagForwardLastMilePartner",
            enabled = true)
    public void MasterBagForwardLastMilePartner(String source, String destinationHubId, String courierCode,
                                                ShipmentType shipmentType, ShippingMethod shippingMethod, String statusType)
            throws Exception {
        String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.RECEIVED_IN_DC, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true));

        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        long masterBagId = masterBagServiceHelper.createMasterBag(String.valueOf(source), String.valueOf(destinationHubId), shippingMethod, courierCode, LMS_CONSTANTS.TENANTID).getId();
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, shipmentType, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), statusType,
                "Unable to add shipment to MB");

    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  dataProviderClass = MasterBagMigratedDP.class, dataProvider = "MasterBagForwardDCtoDC", description = "ID: C320 , MasterBagForwardDCtoDC",
            enabled = true)
    public void MasterBagForwardDCtoDC(String orderId, String source, String destinationHubId, String courierCode,
                                       ShipmentType shipmentType, ShippingMethod shippingMethod, String statusType) throws Exception {
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(orderId);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        long masterBagId = masterBagServiceHelper.createMasterBag(String.valueOf(source), String.valueOf(destinationHubId), shippingMethod, courierCode, LMS_CONSTANTS.TENANTID).getId();
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, shipmentType, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), statusType,
                "Unable to add shipment to MB");
        if (statusType.equals(EnumSCM.SUCCESS)) {
            Assert.assertEquals(
                    (masterBagServiceHelper.removeShipmentsFromMasterBag(masterBagId, trackingNumber, shipmentType, LMS_CONSTANTS.TENANTID)).getStatus().getStatusType().toString(),
                    EnumSCM.SUCCESS, "Unable to add order to MB");
        }
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  dataProviderClass = MasterBagMigratedDP.class, dataProvider = "MasterBagReturnDCtoDC", description = "ID: C321 , MasterBagForwardWithDiffOrderStatus",
            enabled = true)
    public void MasterBagReturnDCtoDC(String trackingNumber, String source, String destinationHubId, String courierCode,
                                      ShipmentType shipmentType, ShippingMethod shippingMethod, String statusType) throws Exception {

        long masterBagId = masterBagServiceHelper.createMasterBag(String.valueOf(source), String.valueOf(destinationHubId), shippingMethod, courierCode, LMS_CONSTANTS.TENANTID).getId();
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, shipmentType, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), statusType,
                "Unable to add shipment to MB");

    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  dataProviderClass = MasterBagMigratedDP.class, dataProvider = "MasterBagReverseDCtoWH", description = "ID: C321 , MasterBagReverse",
            enabled = false)
    public void MasterBagReverseDCtoWH(String orderId, String source, String dest, String courierCode,
                                       ShipmentType shipmentType, ShippingMethod shippingMethod, String statusType){
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(orderId);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        long masterBagId = masterBagServiceHelper.createMasterBag(String.valueOf(source), String.valueOf(dest), shippingMethod, courierCode, LMS_CONSTANTS.TENANTID).getId();
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, shipmentType, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), statusType,
                "Unable to add shipment to MB");
    }


    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C351 , MasterBagWHtoWH",
            enabled = true)
    public void MasterBagWHtoWHRTO() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.RTO, LMS_PINCODE.ML_BLR, "ML", "28", EnumSCM.NORMAL, "on",
                false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        lmsServiceHelper.transferShipmentBackToWHRTO(packetId);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        long masterBagId = masterBagServiceHelper.createMasterBag("RT-DEL", "RT-BLR", ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID).getId();
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR,
                "able to add shipment to MB");
    }


    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  dataProviderClass = MasterBagMigratedDP.class, dataProvider = "getMasterBagWithParam", description = "ID: C326 , ",
            enabled = true)
    public void getMasterBagWithParam(String pathParam, String statusCode, String statusMessage, String statusType)
            throws NumberFormatException {
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterbagUsingQueryParam(pathParam);
        Assert.assertEquals(masterbagResponse.getStatus().getStatusCode(), Integer.parseInt(statusCode));
        Assert.assertEquals(masterbagResponse.getStatus().getStatusMessage().toString(), statusMessage,
                "Unable to get bag something wrong");
        Assert.assertEquals(masterbagResponse.getStatus().getStatusType().toString(), statusType);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  dataProviderClass = MasterBagMigratedDP.class, dataProvider = "updateMasterBag", description = "ID: C328 , updateMasterBag",
            enabled = true)
    public void updateMasterBag(Integer newCapacity, String originHubCode, String newDestination, ShippingMethod orderShippingMethod,
                                String courierCode, String tenantId) {
        long masterBagId = masterBagServiceHelper.createMasterBag("RT-DEL", "RT-BLR", ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID).getId();
        MasterbagDomain editMasterBagConfig = MasterbagDomain.builder()
                .originHubCode(originHubCode)
                .originHubType(lmsServiceHelper.searchHubByCode(originHubCode).getHub().get(0).getType().name())
                .destinationHubCode(newDestination)
                .destinationHubType(lmsServiceHelper.searchHubByCode(newDestination).getHub().get(0).getType().name())
                .shippingMethod(orderShippingMethod)
                .courierCode(courierCode)
                .capacity(newCapacity)
                .tenantId(tenantId)
                .build();
        masterBagServiceHelper.editMasterBag(masterBagId, editMasterBagConfig);

        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(masterBagId);
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getOriginPremisesCode(), originHubCode, "after updating the master bag, origin premises code is not matching");
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getDestinationPremisesCode(), newDestination, "after updating the master bag, destination premises code is not matching");
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getShippingMethod().toString(), orderShippingMethod.toString(), "after updating the master bag, Shipping method is not matching");

    }


    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C354 , MasterBagAddShipmentOnClose",
            enabled = true)
    public void MasterBagAddShipmentOnClose() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId1 = omsServiceHelper.getPacketId(orderId1);
        long masterBagId = Long.parseLong(lmsHelper.getMasterBagId(packetId));
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId1);
        String trackingNumber = orderResponse.getOrders().get(0).getTrackingNumber();
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR,
                "Unable to add shipment to MB");
    }

    /************************************************ LOST Cases ******************************************************/

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C349 , MasterBagLostOnShip",
            enabled = true)
    public void MasterBagLostOnShip() throws Exception {
        MasterbagDomain masterbagDomain;
        String warehouseId = "36";
        MasterbagUpdateResponse masterbagUpdateResponse;
        Long masterBagId;
        Map<String, Object> orderMap;
        List<String> trackingNumbers, packetIds, orderIds;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        orderMap = lmsOperations.createMockOrders(2, EnumSCM.IS, warehouseId, "560068",
                shippingMethod.toString(), courierCode, false, "cod");
        trackingNumbers = (List<String>) orderMap.get("trackingNumbers");
        packetIds = (List<String>) orderMap.get("packetIds");
        orderIds = (List<String>) orderMap.get("orderIds");

        masterbagDomain = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(masterbagDomain, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = masterbagDomain.getId();
        System.out.println("masterBagId=" + masterBagId);
        for (String trackingNumber : trackingNumbers) {
            masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            masterBagValidator.validateAddShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumber);
        }
        masterBagValidator.validateMultipleShipmentsAddedToMasterBag(masterBagId, trackingNumbers);
        masterbagUpdateResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(masterbagUpdateResponse, masterBagId);
        tmsServiceHelper.processInTMSFromClosedShipped.accept(masterBagId);
        statusPollingValidator.validate_PacketOrderLineStatusOMS(packetIds.get(0), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.CLIENTID, EnumSCM.S);
        statusPollingValidator.validate_PacketOrderLineStatusOMS(packetIds.get(1), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.CLIENTID, EnumSCM.S);
        Assert.assertEquals(lmsServiceHelper.markMasterBagLost("" + masterBagId), EnumSCM.SUCCESS);
        statusPollingValidator.validate_PacketOrderLineStatusOMS(packetIds.get(0), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.CLIENTID, EnumSCM.L);
        statusPollingValidator.validate_PacketOrderLineStatusOMS(packetIds.get(1), LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.CLIENTID, EnumSCM.L);
        masterBagValidator.validateMasterBagStatus(masterBagId, com.myntra.logistics.masterbag.core.MasterbagStatus.LOST);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C345 , MasterBagLostOnNew",
            enabled = true)
    public void MasterBagLostOnNew() throws Exception {
        MasterbagDomain createMasterBagResponse;
        String orderId, packetId, trackingNumber;
        MasterbagUpdateResponse masterbagUpdateResponse;
        String inScanResponse;
        Long masterBagId;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        orderId = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        inScanResponse = lmsServiceHelper.orderInScanNew(packetId, "36", false, LMS_CONSTANTS.TENANTID);
        System.out.println("response=" + inScanResponse);
        createMasterBagResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(createMasterBagResponse, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = createMasterBagResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToMasterBag(masterbagUpdateResponse, masterBagId, trackingNumber);
        Assert.assertEquals(lmsServiceHelper.markMasterBagLost(String.valueOf(masterBagId)), EnumSCM.SUCCESS);
        masterBagValidator.validateMasterBagStatus(masterBagId, com.myntra.logistics.masterbag.core.MasterbagStatus.LOST);
        statusPollingValidator.validate_PacketOrderLineStatusOMS(packetId, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.CLIENTID, EnumSCM.L);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C346 , MasterBagLostOnClosed",
            enabled = true)
    public void MasterBagLostOnClosed() throws Exception {
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse addShipmentToMbResponse, closeMBResponse;
        String orderId, packetId, trackingNumber;
        Long masterBagId;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        orderId = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToMasterBag(addShipmentToMbResponse, masterBagId, trackingNumber);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(closeMBResponse, masterBagId, trackingNumber);
        Assert.assertEquals(lmsServiceHelper.markMasterBagLost("" + masterBagId), EnumSCM.SUCCESS);
        masterBagValidator.validateMasterBagStatus(masterBagId, com.myntra.logistics.masterbag.core.MasterbagStatus.LOST);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.LOST_IN_HUB, 3));
        statusPollingValidator.validate_PacketOrderLineStatusOMS(packetId, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.CLIENTID, EnumSCM.L);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C347 , MasterBagLostOnClosed3PL",
            enabled = true)
    public void MasterBagLostOnClosed3PL() throws Exception {
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse closeMBResponse;
        String orderId, packetId, trackingNumber;
        Long masterBagId;
        String originHubCode = "DH-BLR", destinationHubCode = "EKART", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        orderId = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.PUNE_EK, "EK", "36", EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(closeMBResponse, masterBagId, trackingNumber);
        Assert.assertEquals(lmsServiceHelper.markMasterBagLost("" + masterBagId), EnumSCM.SUCCESS);
        masterBagValidator.validateMasterBagStatus(masterBagId, com.myntra.logistics.masterbag.core.MasterbagStatus.LOST);
        Assert.assertTrue(lmsServiceHelper.validateOrderStatusInLMS("" + packetId, EnumSCM.LOST_IN_HUB, 3));
        statusPollingValidator.validate_PacketOrderLineStatusOMS(packetId, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.CLIENTID, EnumSCM.L);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C348 , MasterBagLostOnSH3PL",
            enabled = true)
    public void MasterBagLostOnSH3PL() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.PUNE_EK, "EK", "36", EnumSCM.NORMAL, "cod",
                false, true);
        String releaseId = omsServiceHelper.getPacketId(orderId);
        String masterBagId = lmsHelper.getMasterBagId("" + releaseId);
        Assert.assertEquals(lmsServiceHelper.markMasterBagLost(masterBagId), EnumSCM.ERROR);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C355 , MasterBagLostOnReceivedAtTransportHub",
            enabled = true)
    public void MasterBagLostOnReceivedAtTransportHub() throws Exception {
        MasterbagDomain mbCreateResponse;
        MasterbagUpdateResponse addShipmentToMbResponse, closeMBResponse;
        String orderId, packetId, trackingNumber;
        Long masterBagId;
        String originHubCode = "DH-BLR", destinationHubCode = "ELC", courierCode = LMSConstants.IN_HOUSE_COURIER, tenantId = LMS_CONSTANTS.TENANTID;
        ShippingMethod shippingMethod = ShippingMethod.NORMAL;
        orderId = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        packetId = omsServiceHelper.getPacketId(orderId);
        trackingNumber = lmsHelper.getTrackingNumber(packetId);
        System.out.println(String.format("orderId:[%s], packetId:[%s], trackingNumber:[%s]", orderId, packetId, trackingNumber));
        mbCreateResponse = masterBagServiceHelper.createMasterBag(originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagValidator.validateCreateMasterBag(mbCreateResponse, originHubCode, destinationHubCode, shippingMethod, courierCode, tenantId);
        masterBagId = mbCreateResponse.getId();
        System.out.println("masterBagId=" + masterBagId);
        addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateAddShipmentToMasterBag(addShipmentToMbResponse, masterBagId, trackingNumber);
        closeMBResponse = masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        masterBagValidator.validateCloseMasterBag(closeMBResponse, masterBagId, trackingNumber);
        TMSMasterbagEntry masterBag = ((TMSMasterbagReponse) tmsServiceHelper.getTmsMasterBagById.apply(masterBagId))
                .getMasterbagEntries().get(0);
        String sourceTH = (String) tmsServiceHelper.getTHForLH_Marvel.apply(masterBag.getSourceHub());
        ExceptionHandler.handleError(
                ((TMSMasterbagReponse) tmsServiceHelper.tmsReceiveMasterBag.apply(sourceTH, masterBagId)).getStatus(),
                "Unable to receive MasterBag in source TH");
        Assert.assertEquals(lmsServiceHelper.markMasterBagLost("" + masterBagId), EnumSCM.SUCCESS);
        masterBagValidator.validateMasterBagStatus(masterBagId, com.myntra.logistics.masterbag.core.MasterbagStatus.LOST);
        statusPollingValidator.validate_PacketOrderLineStatusOMS(packetId, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.CLIENTID, EnumSCM.L);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C356 , MasterBagLostOnReceived",
            enabled = true)
    public void MasterBagLostOnReceived() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod",
                false, true);
        String releaseId = omsServiceHelper.getPacketId(orderId);
        String masterBagId = lmsHelper.getMasterBagId("" + releaseId);
        Assert.assertEquals(lmsServiceHelper.markMasterBagLost(masterBagId), EnumSCM.ERROR);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C357 , MasterBagDiffServiceability",
            enabled = true)
    public void MasterBagDiffServiceabilityWithWarningTrue() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod",
                false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);

        DBUtilities.exUpdateQuery(
                "update order_to_ship set delivery_center_id = 1 where order_id = '" + packetId + "'", "lms");
        DBUtilities.exUpdateQuery(
                "update ml_shipment set delivery_center_id = 1 where source_reference_id = '" + packetId + "'", "lms");

        long masterBagId = masterBagServiceHelper.createMasterBag("HSR", "RT-BLR", ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID).getId();
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR,
                "able to add shipment to MB");
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C358 , MasterBagDiffServiceability",
            enabled = true)
    public void MasterBagDiffServiceabilityWithWarningFalse() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod",
                false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        //TODO : need to ask why it is used
        DBUtilities.exUpdateQuery(
                "update order_to_ship set delivery_center_id = 1 where order_id = '" + packetId + "'", "lms");
        DBUtilities.exUpdateQuery(
                "update ml_shipment set delivery_center_id = 1 where source_reference_id = '" + packetId + "'", "lms");

        long masterBagId = masterBagServiceHelper.createMasterBag("HSR", "RT-BLR", ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID).getId();
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), EnumSCM.ERROR,
                "order which has ELC destination is added into Master bag which has HSR destination");

    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  dataProviderClass = MasterBagMigratedDP.class, dataProvider = "closeAndShipMasterBag", description = "ID: C331 , closeAndShipMasterBag",
            enabled = true)
    public void closeAndShipMasterBag(String courierCode, String pincode, String warehouseId, ShippingMethod shippingMethod,
                                      boolean isTnB) throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.IS, pincode, courierCode, warehouseId, String.valueOf(shippingMethod), "cod", isTnB, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String destinationHub = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        String originHub = orderResponse.getOrders().get(0).getDispatchHubCode();
        if (courierCode.equalsIgnoreCase("ML")) {
            long masterBagId = masterBagServiceHelper.createMasterBag(originHub, destinationHub, shippingMethod, courierCode, LMS_CONSTANTS.TENANTID).getId();
            MasterbagUpdateResponse addShipmentsToMasterBag = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            Assert.assertEquals(addShipmentsToMasterBag.getStatus().getStatusType().toString(), EnumSCM.SUCCESS,
                    "Unable to add shipment to MB");
           MasterbagUpdateResponse closeMasterBag =  masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
            Assert.assertEquals(closeMasterBag.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"close MB operation is not successful");

            statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());
            tmsServiceHelper.processInTMSFromClosedShipped.accept(masterBagId);
            statusPollingValidator.validate_PacketOrderLineStatusOMS(packetId, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.CLIENTID, EnumSCM.S);
            statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED.toString());

        } else {
            //MasterBag V2
            MasterbagDomain mbCreateResponse = masterBagServiceHelper.createMasterBag(originHub, destinationHub, shippingMethod, courierCode, LMS_CONSTANTS.TENANTID);
            Long masterBagId = mbCreateResponse.getId();
            System.out.println("masterBagId=" + masterBagId);
            MasterbagUpdateResponse masterbagUpdateResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID);
            Assert.assertEquals(masterbagUpdateResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Order is not added to master bag which is created for courier code " + courierCode);
            statusPollingValidator.validateShipmentBagStatusRespectToOrder(masterBagId, EnumSCM.NEW);
            MasterbagUpdateResponse closeMasterBag =masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
            Assert.assertEquals(closeMasterBag.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"close MB operation is not successful");
            statusPollingValidator.validateShipmentBagStatusRespectToOrder(masterBagId, EnumSCM.CLOSED);
            //Ship MB
            ShipmentResponse shipmentResponse = lmsServiceHelper.shipMasterBag(masterBagId);
            Assert.assertEquals(shipmentResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Ship operation is not successful");
            statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED.toString());

        }
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C332 , closeAndShipMasterBagWithOutTransporterHubScan",
            enabled = true)
    public void closeAndShipMasterBagWithOutTransporterHubScan() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String masterBagId = lmsHelper.getMasterBagId("" + omsServiceHelper.getPacketId(orderId));
        masterBagServiceHelper.closeMasterBag(Long.parseLong(masterBagId), LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(masterBagId), ShipmentStatus.CLOSED.toString());
        Assert.assertEquals(
                lmsServiceHelper.shipMasterBag(Long.parseLong(masterBagId)).getStatus().getStatusType().toString(),
                EnumSCM.ERROR);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C333 , closeAndShipMasterBag3PLWithOutTransporterHubScan",
            enabled = true)
    public void closeAndShipMasterBag3PLWithOutTransporterHubScan() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, "560069", "EK", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String masterBagId = lmsHelper.getMasterBagId(omsServiceHelper.getPacketId(orderId));
        masterBagServiceHelper.closeMasterBag(Long.parseLong(masterBagId), LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(masterBagId), ShipmentStatus.CLOSED.toString());
        Assert.assertEquals(
                lmsServiceHelper.shipMasterBag(Long.parseLong(masterBagId)).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  dataProviderClass = LMSTestsDP.class, dataProvider = "reopenMaterBagAndShip", description = "ID: C334 , reopenMaterBagAndShip",
            enabled = true)
    public void reopenMaterBagAndShip(String pincode, String courierCode) throws Exception {

        String orderId = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, pincode, courierCode, "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String masterBagId = lmsHelper.getMasterBagId(packetId);
        masterBagServiceHelper.closeMasterBag(Long.parseLong(masterBagId), LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(masterBagId), EnumSCM.CLOSED);

        Assert.assertEquals(

                masterBagServiceHelper.removeShipmentsFromMasterBag(Long.parseLong(masterBagId), trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.ERROR);
        masterBagServiceHelper.reopenMasterBag(Long.parseLong(masterBagId));
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(masterBagId), ShipmentStatus.NEW.toString());

        Assert.assertEquals(
                masterBagServiceHelper.removeShipmentsFromMasterBag(Long.parseLong(masterBagId), trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);

        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(Long.parseLong(masterBagId), trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
        masterBagServiceHelper.closeMasterBag(Long.parseLong(masterBagId), LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(masterBagId), EnumSCM.CLOSED);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C359 , This operation has been blocked after TMS so for now its not considered",
            enabled = true)
    public void reopenMaterBagAfterReceiveAtTransportHub() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String releaseId = omsServiceHelper.getPacketId(orderId);
        long masterBagId = Long.parseLong(lmsHelper.getMasterBagId("" + releaseId));

        Assert.assertEquals(masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());
        Assert.assertEquals(lmsServiceHelper.scanMasterBagInTransportHub(masterBagId), EnumSCM.SUCCESS,
                "Unable to receive in Transport Hub");
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());
        String reOpenMB = masterBagServiceHelper.reopenMasterBag(masterBagId);
        Assert.assertTrue(reOpenMB.contains(EnumSCM.SUCCESS),"MB reopen is not successful");
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.NEW.toString());
        Assert.assertEquals(masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());
        Assert.assertEquals(lmsServiceHelper.scanMasterBagInTransportHub(masterBagId), EnumSCM.SUCCESS,
                "Unable to receive in Transport Hub");
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.RECEIVED_AT_TRANSPORTER_HUB.toString());

        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId, lmsHelper.getTransporter(36, 5)).getStatus()
                .getStatusType().toString(), EnumSCM.SUCCESS, "Unable to ship masterBag");
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  dataProviderClass = LMSTestsDP.class, dataProvider = "reopenMaterOnDiffStatus", description = "ID: C360 , reopenMaterOnDiffStatus: On SH and on receieved Error Scenarios",
            enabled = true)
    public void reopenMaterOnDiffStatus(String toState, String expectedStatus, String statusType) throws Exception {
        String orderId = lmsHelper.createMockOrder(toState, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false,
                true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String masterBagId = lmsHelper.getMasterBagId(String.valueOf(packetId));
        String reopenMasterBagResponse = masterBagServiceHelper.reopenMasterBag(Long.parseLong(masterBagId));
        Assert.assertTrue(reopenMasterBagResponse.contains(statusType), "reopen functionality failed");
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(masterBagId), expectedStatus);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C335 , AddedToMB_RemoveAndCancelShipment and again try to add",
            enabled = true)
    public void AddedToMB_RemoveAndCancelShipment() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String masterBagId = lmsHelper.getMasterBagId(String.valueOf(packetId));
        Assert.assertEquals(
                masterBagServiceHelper.closeMasterBag(Long.parseLong(masterBagId), LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(masterBagId), ShipmentStatus.CLOSED.toString());
        masterBagServiceHelper.reopenMasterBag(Long.parseLong(masterBagId));
        statusPollingValidator.validateShipmentBagStatus(Long.parseLong(masterBagId), ShipmentStatus.NEW.toString());
        Assert.assertEquals(
                masterBagServiceHelper.removeShipmentsFromMasterBag(Long.parseLong(masterBagId), trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Order is not removed from master bag");
        lmsServiceHelper.cancelShipmentInLMS(String.valueOf(packetId));
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.CANCELLED);

        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(Long.parseLong(masterBagId), trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.ERROR, "cancelled Order added into master bag");
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C336 , RHDHandoverToCourierPartnerWithoutReciving",
            enabled = true)
    public void RHDHandoverToCourierPartnerWithoutReceiving() throws Exception {

        //TODO
        /**
         * Setup new pincode for DE courier to get this working.
         * It will fail as, this pincode config is used in sortation.
         * - Bharath & Shanmugam
         */

        lmsReturnHelper.insertTrackingNumberClosedBox("DE");
        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.MUMBAI_DE_RHD, "DE", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String releaseId = omsServiceHelper.getPacketId(orderId);
        long masterBagId = Long.parseLong(lmsHelper.getMasterBagId(String.valueOf(releaseId)));
        Assert.assertEquals(lmsServiceHelper.handoverToRegionalCourier(masterBagId).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        long dcId = Long.parseLong(lmsHelper.getDCId(String.valueOf(releaseId)));
        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, EnumSCM.RECEIVED_AT_HANDOVER_CENTER, "bangalore", dcId,
                "DC").getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        Assert.assertEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId, String.valueOf(releaseId), "DC-Delhi", dcId, "DC", 36,
                ShipmentStatus.RECEIVED_AT_HANDOVER_CENTER, OrderShipmentAssociationStatus.RECEIVED_AT_HANDOVER_CENTER).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.handoverToRegionalCourier(masterBagId).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
    }

    @Test(groups = {"MasterBag", "Smoke", "Regression"},  description = "ID: C337 , Scan 2 order in a shipment " +
            "and ship, scan 2nd order in other masterbag and ship, Then in 1st masterbag: recieve 1st shipment and  mark 'shortage' " +
            "for 2nd shipment. Receive 2nd masterbag", enabled = true)
    public void masterBagInScanAndUpdateWithReceiveShortage() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.PK, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod",
                false, true);
        String packetId = "" + omsServiceHelper.getPacketId(orderId);
        String packetId1 = "" + omsServiceHelper.getPacketId(orderId1);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        long masterBagId = Long.parseLong(lmsHelper.getMasterBagId(packetId));
        Assert.assertEquals(lmsServiceHelper.orderInScanNew(packetId1, "36"), EnumSCM.SUCCESS);

        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber1, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "order is not added into master bag");
        Assert.assertEquals(masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());
        tmsServiceHelper.processInTMSFromClosedShipped.accept(masterBagId);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.SHIPPED);
        statusPollingValidator.validate_PacketOrderLineStatusOMS(packetId, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.CLIENTID, EnumSCM.S);

        // -----Add releaseId1 to another bag -------------
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag("DH-BLR", "ELC", ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID);
        long masterBagId1 = masterbagDomain.getId();

        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBagWithForceUpdate(masterBagId1, trackingNumber1, ShipmentType.DL, LMS_CONSTANTS.TENANTID, true).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to save masterBag");
        Assert.assertEquals(masterBagServiceHelper.closeMasterBag(masterBagId1, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.CLOSED.toString());
        tmsServiceHelper.processInTMSFromClosedShipped.accept(masterBagId1);
        statusPollingValidator.validateShipmentBagStatus(masterBagId1, ShipmentStatus.IN_TRANSIT.toString());
        statusPollingValidator.validateOrderStatus(packetId1, EnumSCM.SHIPPED);

        //master bag in-scan
        masterBagServiceHelper.masterbagInscan(masterBagId, ShipmentStatus.IN_TRANSIT, 5l, PremisesType.DC);

        storeHelper.masterBagInscanProcess(trackingNumber, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);
        //mark order which is added into 2nd mb as SHORTAGE
        masterBagServiceHelper.markOrderAsShortage(trackingNumber1, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.SHORTAGE, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);
        //validate mb status
        ShipmentResponse shipmentResponse = masterBagClient_qa.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getStatus().toString(), EnumSCM.SHORTAGE, "order status is not in SHORTAGE");
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(1).getStatus().toString(), EnumSCM.RECEIVED, "order status is not in RECEIVED");
        //mark master bag as RECEIVED
        storeHelper.masterBagInscanProcess(trackingNumber1, masterBagId1, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);
        ShipmentResponse shipmentResponse1 = masterBagClient_qa.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse1.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getStatus().toString(), EnumSCM.SHORTAGE, "order status is not in SHORTAGE");
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C361 , masterBagReciveDamaged",
            enabled = true)
    public void masterBagReceiveDamaged() throws Exception {

        String orderId = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String releaseId = omsServiceHelper.getPacketId(orderId);

        long masterBagId = Long.parseLong(lmsHelper.getMasterBagId(String.valueOf(releaseId)));
        Assert.assertEquals(masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());
        tmsServiceHelper.processInTMSFromClosedShipped.accept(masterBagId);
        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, 5).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        Assert.assertEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId, new String[]{releaseId + ":RECEIVED_DAMAGED"},
                5, "DC").getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
    }

    @Test(groups = {"MasterBag", "Smoke", "Regression"},  description = "ID: C340 , masterBagReceiveInWrongDC", enabled = true)
    public void masterBagReceiveInWrongDC() throws Exception {

        String orderId = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String releaseId = omsServiceHelper.getPacketId(orderId);

        long masterBagId = Long.parseLong(lmsHelper.getMasterBagId(String.valueOf(releaseId)));
        Assert.assertEquals(masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());
        tmsServiceHelper.processInTMSFromClosedShipped.accept(masterBagId);
        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, 1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        //we can in-scan the MB at wrong DC but shipment cannot be received at wrong DC
        Assert.assertEquals(lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.ERROR, "Unable to receive shipment in DC");
        Assert.assertEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId, String.valueOf(releaseId), "Bangalore",
                1, "DC", 36).getStatus().getStatusType().toString(),
                EnumSCM.ERROR);
        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, 5).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        Assert.assertEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId, String.valueOf(releaseId), "Bangalore",
                5, "DC", 36).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
    }

    @Test(groups = {"MasterBag", "Smoke", "Regression"},  description = "ID: C341 , masterBagReceiveAnd_DCToDCMovement, check source and warehouse mismatch and finally with both correct", enabled = true)
    public void masterBagReceiveAnd_DCToDCMovement() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.SH, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod",
                false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        long masterBagId = Long.parseLong(lmsHelper.getMasterBagId(String.valueOf(packetId)));
        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, 5).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        Assert.assertEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId, String.valueOf(packetId), "Bangalore", 5, "DC", 36)
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        // Try add order to source mismatch
        MasterbagDomain masterbagDomain = masterBagServiceHelper.createMasterBag("HSR", "ELC", ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID);
        Long masterBagId1 = masterbagDomain.getId();
        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(masterBagId1, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), EnumSCM.ERROR);
        MasterbagDomain masterbagDomain1 = masterBagServiceHelper.createMasterBag("ELC", "HSR", ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID);
        Long masterBagId2 = masterbagDomain1.getId();
        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(masterBagId2, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);

        Assert.assertEquals(masterBagServiceHelper.closeMasterBag(masterBagId2, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        statusPollingValidator.validateShipmentBagStatus(masterBagId2, ShipmentStatus.CLOSED.toString());
        Assert.assertEquals(lmsServiceHelper.shipMasterBag(masterBagId2).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to ship masterBag");
        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId2, 10).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId2).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        Assert.assertEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId2, String.valueOf(packetId), "Bangalore", 10, "DC", 36)
                .getStatus().getStatusType().toString(), EnumSCM.ERROR);
        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId2, 1).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId2, String.valueOf(packetId), "Bangalore", 1, "DC", 36)
                .getStatus().getStatusType().toString(), EnumSCM.ERROR);
    }

    @Test(groups = {"MasterBag", "Smoke", "Regression"},  dataProviderClass = LMSTestsDP.class, dataProvider = "masterBagReceiveWithExpressNTnB", description = "ID: C342 , masterBagReceiveWithExpressNTOD and paymentMode = CC", enabled = true)
    public void masterBagReceiveWithExpressNTnB(String shippingMethod, boolean isTnB) throws Exception {

        String orderId = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, LMS_PINCODE.ML_BLR, "ML", "36", shippingMethod,
                "cod", isTnB, true);
        String releaseId = omsServiceHelper.getPacketId(orderId);
        long masterBagId = Long.parseLong(lmsHelper.getMasterBagId(String.valueOf(releaseId)));
        Assert.assertEquals(masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());
        tmsServiceHelper.processInTMSFromClosedShipped.accept(masterBagId);
        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, 5).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to receive shipment in DC");
        Assert.assertEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId, String.valueOf(releaseId), "Bangalore", 5, "DC", 36)
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C343 , masterBagReciveInWrongStatus",
            enabled = true)
    public void masterBagReciveInWrongStatus() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderId);
        long masterBagId = Long.parseLong(lmsHelper.getMasterBagId(String.valueOf(packetId)));
        Assert.assertEquals(lmsServiceHelper.masterBagInScan(masterBagId, 5).getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertEquals(lmsServiceHelper.receiveShipmentFromMasterbag(masterBagId).getStatus().getStatusType().toString(),
                EnumSCM.ERROR, "Unable to receive shipment in DC");
        Assert.assertEquals(lmsServiceHelper.masterBagInScanUpdate(masterBagId, String.valueOf(packetId), "Bangalore", 5, "DC", 36)
                .getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C362 , MasterBagWithMultiShipments",
            enabled = true)
    public void MasterBagWithMultiShipments() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String orderId2 = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String orderId3 = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", true, true);
        String orderId4 = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "on", false, true);

        String releaseId = omsServiceHelper.getPacketId(orderId);
        String releaseId1 = omsServiceHelper.getPacketId(orderId1);
        String releaseId2 = omsServiceHelper.getPacketId(orderId2);
        String releaseId3 = omsServiceHelper.getPacketId(orderId3);
        String releaseId4 = omsServiceHelper.getPacketId(orderId4);
        String trackingNumber = lmsHelper.getTrackingNumber(releaseId);
        String trackingNumber1 = lmsHelper.getTrackingNumber(releaseId1);
        String trackingNumber2 = lmsHelper.getTrackingNumber(releaseId2);
        String trackingNumber3 = lmsHelper.getTrackingNumber(releaseId3);
        String trackingNumber4 = lmsHelper.getTrackingNumber(releaseId4);

        MasterbagDomain mbCreateResponse = masterBagServiceHelper.createMasterBag("DH-BLR", "ELC", ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID);
        Long masterBagId = mbCreateResponse.getId();
        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add shipment to MB");
        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber1, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add shipment to MB");
        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber2, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add shipment to MB");
        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber3, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add shipment to MB");
        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber4, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add shipment to MB");

        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatus(masterBagId, EnumSCM.CLOSED);

        tmsServiceHelper.processInTMSFromClosedShipped.accept(masterBagId);
        Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(releaseId, EnumSCM.S, 5));
        Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(releaseId1, EnumSCM.S, 5));
        Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(releaseId2, EnumSCM.S, 5));
        Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(releaseId3, EnumSCM.S, 5));
        Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(releaseId4, EnumSCM.S, 5));

        masterBagServiceHelper.masterbagInscan(masterBagId, ShipmentStatus.IN_TRANSIT, 5l, PremisesType.DC);

        storeHelper.masterBagInscanProcess(trackingNumber, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);
        storeHelper.masterBagInscanProcess(trackingNumber1, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);
        storeHelper.masterBagInscanProcess(trackingNumber2, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);
        storeHelper.masterBagInscanProcess(trackingNumber3, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);
        storeHelper.masterBagInscanProcess(trackingNumber4, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);

        //validate mb status
        ShipmentResponse shipmentResponse = masterBagClient_qa.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getStatus().toString(), EnumSCM.RECEIVED, "order status is not in RECEIVED");
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(1).getStatus().toString(), EnumSCM.RECEIVED, "order status is not in RECEIVED");
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(2).getStatus().toString(), EnumSCM.RECEIVED, "order status is not in RECEIVED");
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(3).getStatus().toString(), EnumSCM.RECEIVED, "order status is not in RECEIVED");
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(4).getStatus().toString(), EnumSCM.RECEIVED, "order status is not in RECEIVED");

        statusPollingValidator.validateML_ShipmentStatus(trackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.UNASSIGNED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber1, LMS_CONSTANTS.TENANTID, EnumSCM.UNASSIGNED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber2, LMS_CONSTANTS.TENANTID, EnumSCM.UNASSIGNED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber3, LMS_CONSTANTS.TENANTID, EnumSCM.UNASSIGNED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber4, LMS_CONSTANTS.TENANTID, EnumSCM.UNASSIGNED);

    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C363 , MasterBagWithMultiShipments",
            enabled = true)
    public void MasterBagWithMultiWarehouseButOneHub() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true);
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "1", EnumSCM.NORMAL, "cod", false, true);

        String releaseId = omsServiceHelper.getPacketId(orderId);
        String releaseId1 = omsServiceHelper.getPacketId(orderId1);
        String trackingNumber = lmsHelper.getTrackingNumber(releaseId);
        String trackingNumber1 = lmsHelper.getTrackingNumber(releaseId1);

        MasterbagDomain mbCreateResponse = masterBagServiceHelper.createMasterBag("DH-BLR", "ELC", ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID);
        Long masterBagId = mbCreateResponse.getId();
        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add shipment to MB");
        Assert.assertEquals(masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, trackingNumber1, ShipmentType.DL, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add shipment to MB");
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatus(masterBagId, EnumSCM.CLOSED);

        tmsServiceHelper.processInTMSFromClosedShipped.accept(masterBagId);
        Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(releaseId, EnumSCM.S, 5));
        Assert.assertTrue(omsServiceHelper.validatePacketStatusInOMS(releaseId1, EnumSCM.S, 5));

        masterBagServiceHelper.masterbagInscan(masterBagId, ShipmentStatus.IN_TRANSIT, 5l, PremisesType.DC);

        storeHelper.masterBagInscanProcess(trackingNumber, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);
        storeHelper.masterBagInscanProcess(trackingNumber1, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);
        //validate mb status
        ShipmentResponse shipmentResponse = masterBagClient_qa.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getStatus().toString(), EnumSCM.RECEIVED, "order status is not in RECEIVED");
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(1).getStatus().toString(), EnumSCM.RECEIVED, "order status is not in RECEIVED");

        statusPollingValidator.validateML_ShipmentStatus(trackingNumber, LMS_CONSTANTS.TENANTID, EnumSCM.UNASSIGNED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNumber1, LMS_CONSTANTS.TENANTID, EnumSCM.UNASSIGNED);
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C364 , MasterBagReverseWithMultiWarehouseButOneHub",
            enabled = true)
    public void MasterBagReverseWithMultiWarehouseButOneHub() throws Exception {
        String returnHub=null;
        String deliveryCenterCode="ELC";
        List<String> returnTrackingNumber = new ArrayList<>();
        int noOfOrders = 1;

        for(int i=0;i<=noOfOrders;i++){
            String orderId = lmsHelper.createMockOrder(EnumSCM.DL, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod",
                    false, false);
            //Create Return
            OrderReleaseEntry orderReleaseEntry = omsServiceHelper.getOrderReleaseEntry(omsServiceHelper.getReleaseId(orderId));
            OrderLineEntry lineEntry = omsServiceHelper.getOrderLineEntry(orderReleaseEntry.getOrderLines().get(0).getId().toString());
            ReturnResponse returnResponse = rmsServiceHelper.createReturn(lineEntry.getId(), ReturnType.NORMAL, ReturnMode.OPEN_BOX_PICKUP, 1, 9L, RefundMode.NEFT, "418", "test Open box address ", "6135071", "560068", "Bangalore", "Karnataka", "India", LMS_CONSTANTS.MOBILE_NO);
            Assert.assertEquals(returnResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Return creation failed");
            Long returnId = returnResponse.getData().get(0).getId();
            storeValidator.isNull(returnId.toString());

            // Validate the respective RMS and LMS return fields
            lms_returnHelper.validateOpenBoxRmsLmsReturnCreationV2(String.valueOf(returnId));
            //validate return status in LMS
            com.myntra.lms.client.domain.response.ReturnResponse returnResponse2 = storeHelper.getReturnStatusInLMS(returnId.toString(), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID));
            storeValidator.validateReturnOrderStatusInLMS(returnResponse2, com.myntra.lastmile.client.status.ShipmentStatus.RETURN_CREATED);
            returnHub = returnResponse2.getDomainReturnShipments().get(0).getReturnHubCode();
            String trackingNumber = returnResponse2.getDomainReturnShipments().get(0).getTrackingNumber();
            returnTrackingNumber.add(trackingNumber);
            returnHelper.processReturnOrderTillPSState(returnId, ReturnMode.OPEN_BOX_PICKUP, LMS_CONSTANTS.TENANTID, LMS_CONSTANTS.CLIENTID);
            statusPollingValidator.validateReturnStatusLMS(String.valueOf(returnId), LMS_CONSTANTS.TENANTID, Long.parseLong(LMS_CONSTANTS.CLIENTID), EnumSCM.RETURN_SUCCESSFUL);

        }

        //create reverse mb
        MasterbagDomain mbCreateResponse = masterBagServiceHelper.createMasterBag("ELC", returnHub, ShippingMethod.NORMAL, "ML", LMS_CONSTANTS.TENANTID);
        Long masterBagId = mbCreateResponse.getId();
        MasterbagUpdateResponse addShipmentToMbResponse = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, returnTrackingNumber.get(0), ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add shipment to MB");
        MasterbagUpdateResponse addShipmentToMbResponse1 = masterBagServiceHelper.addShipmentsToMasterBag(masterBagId, returnTrackingNumber.get(1), ShipmentType.PU, LMS_CONSTANTS.TENANTID);
        Assert.assertEquals(addShipmentToMbResponse1.getStatus().getStatusType().toString(), EnumSCM.SUCCESS, "Unable to add shipment to MB");
        masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID);
        statusPollingValidator.validateShipmentBagStatus(masterBagId, EnumSCM.CLOSED);

        //Receive MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(deliveryCenterCode, masterBagId, LMS_CONSTANTS.TENANTID);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = storeHelper.getMappedTransportHubDetails(LMS_CONSTANTS.TENANTID, deliveryCenterCode);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterCode, transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(deliveryCenterCode, transporterHub, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse addMbIntoContainer = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, masterBagId);
        Assert.assertEquals(addMbIntoContainer.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");

        //Ship container
        ContainerResponse shipContainer = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(shipContainer.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Container is not shipped");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.IN_TRANSIT.toString());

        //Receive Container In Transport Hub
        ContainerResponse receiveContainerInTMS = (ContainerResponse) storeHelper.receiveContainerInTransportHub.apply(containerId, transporterHub);
        Assert.assertTrue(receiveContainerInTMS.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Container is not received in TRansport hub");

        masterBagServiceHelper.masterbagInscan(masterBagId, ShipmentStatus.IN_TRANSIT, 5l, PremisesType.DC);

        storeHelper.masterBagInscanProcess(returnTrackingNumber.get(0), masterBagId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
        storeHelper.masterBagInscanProcess(returnTrackingNumber.get(1), masterBagId, ShipmentType.PU, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
         //validate mb status
        ShipmentResponse shipmentResponse = masterBagClient_qa.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getStatus().toString(), EnumSCM.RECEIVED, "order status is not in RECEIVED");
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(1).getStatus().toString(), EnumSCM.RECEIVED, "order status is not in RECEIVED");
    }

    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C878, Receive Excess shipment in a masterbag",
            enabled = true)
    public void masterBagReceiveExcessShipment() throws Exception {
        String orderId = lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL,
                "cod", false, true);
        String orderId1 = lmsHelper.createMockOrder(EnumSCM.IS, LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod",
                false, true);
        String packetId = String.valueOf(omsServiceHelper.getPacketId(orderId));
        String packetId1 = String.valueOf(omsServiceHelper.getPacketId(orderId1));
        String trackingNumber = lmsHelper.getTrackingNumber(packetId);
        String trackingNumber1 = lmsHelper.getTrackingNumber(packetId1);
        long masterBagId = Long.parseLong(lmsHelper.getMasterBagId(packetId));
        Assert.assertEquals(lmsServiceHelper.orderInScanNew(packetId1, "36"), EnumSCM.SUCCESS);
        Assert.assertEquals(masterBagServiceHelper.closeMasterBag(masterBagId, LMS_CONSTANTS.TENANTID).getStatus().getStatusType().toString(),
                EnumSCM.SUCCESS, "Unable to close masterBag");
        statusPollingValidator.validateShipmentBagStatus(masterBagId, ShipmentStatus.CLOSED.toString());
        tmsServiceHelper.processInTMSFromClosedShipped.accept(masterBagId);
        statusPollingValidator.validateOrderStatus(packetId, com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED);
        statusPollingValidator.validate_PacketOrderLineStatusOMS(packetId, LMS_CONSTANTS.CLIENTID, LMS_CONSTANTS.CLIENTID, EnumSCM.S);

        masterBagServiceHelper.masterbagInscan(masterBagId, ShipmentStatus.IN_TRANSIT, 5l, PremisesType.DC);

        storeHelper.masterBagInscanProcess(trackingNumber, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);
        //mark order which is added into 2nd mb as SHORTAGE
        masterBagServiceHelper.markOrderAsShortage(trackingNumber1, masterBagId, ShipmentType.DL, OrderShipmentAssociationStatus.EXCESS, ShipmentStatus.RECEIVED, PremisesType.DC, 5l);
        //validate mb status
        ShipmentResponse shipmentResponse = masterBagClient_qa.getShipmentOrderMap(masterBagId);
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(0).getStatus().toString(), EnumSCM.RECEIVED, "order status is not in RECEIVED");
        Assert.assertEquals(shipmentResponse.getEntries().get(0).getOrderShipmentAssociationEntries().get(1).getStatus().toString(), "EXCESS", "order status is not in EXCESS");

    }


    @Test(groups = {"MasterBag", "Smoke",
            "Regression"},  description = "ID: C879, get Order shipment Association", enabled = true)
    public void getOrderShipmentAssociation() throws Exception {
        String releaseId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.ADDED_TO_MB,
                LMS_PINCODE.ML_BLR, "ML", "36", EnumSCM.NORMAL, "cod", false, true));
        MasterbagResponse masterbagResponse = masterBagServiceHelper.searchMasterBagById(Long.parseLong(lmsHelper.getMasterBagId(releaseId)));

        Assert.assertEquals(masterbagResponse.getStatus().getStatusType().toString(), EnumSCM.SUCCESS);
        Assert.assertTrue(masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries().size() > 0);
        Assert.assertEquals(masterbagResponse.getMasterbagEntries().get(0).getMasterbagShipmentAssociationEntries().get(0).getStatus().toString(), ShipmentStatus.NEW.toString());
    }
}


