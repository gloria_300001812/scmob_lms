package com.myntra.apiTests.erpservices.masterbag.dp;

import com.myntra.apiTests.erpservices.lms.Constants.LMS_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.Constants.LMS_PINCODE;
import com.myntra.lms.client.codes.LMSConstants;
import com.myntra.lms.client.status.ShipmentType;
import com.myntra.lms.client.status.ShippingMethod;
import com.myntra.logistics.masterbag.core.MasterbagType;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

/**
 * @author Bharath.MC
 * @since Apr-2019
 */
public class NewMasterBagDP {

    @DataProvider
    public static Object[][] MasterBagCRUD(ITestContext testContext){
        Object[] dataset1 ={"DH-BLR" , "ELC" , ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID};
        Object[][] dataSet = new Object[][]{dataset1};
        return dataSet;
    }

    @DataProvider(name = "CreateMasterBagForwardDL")
    public static Object[][] CreateMasterBagForwardDL(ITestContext testContext){
        Object[] dataset1 ={"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL,  ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID, 5};
        Object[] dataset2 ={"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID, 0};
        Object[] dataset3 ={"DH-BLR", "DH-DEL" , LMS_PINCODE.DELHIS_DC, ShipmentType.DL, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER , LMS_CONSTANTS.TENANTID, 20};
        Object[] dataset4 ={"DH-BLR", "ELC" , LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER , LMS_CONSTANTS.TENANTID, 7};
        Object[] dataset5 ={"DH-BLR", "ELC" , LMS_PINCODE.ML_BLR, ShipmentType.DL,  ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER , LMS_CONSTANTS.TENANTID, 99};
        Object[][] dataSet = new Object[][]{dataset1 , dataset2, dataset3, dataset4, dataset5};
        return dataSet;
    }

    @DataProvider(name = "CreateMasterBagForwardTryAndBuy")
    public static Object[][] CreateMasterBagForwardTryAndBuy(ITestContext testContext){
        Object[] dataset1 ={"DH-BLR", "ELC", LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID, 5};
        Object[] dataset2 ={"DH-BLR", "ELC", LMS_PINCODE.ML_BLR,ShipmentType.DL, ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID, 0};
        Object[] dataset3 ={"DH-BLR", "DH-DEL" , LMS_PINCODE.DELHIS_DC, ShipmentType.DL,ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER , LMS_CONSTANTS.TENANTID, 20};
        Object[] dataset4 ={"DH-BLR", "ELC" , LMS_PINCODE.ML_BLR, ShipmentType.DL, ShippingMethod.EXPRESS, LMSConstants.IN_HOUSE_COURIER , LMS_CONSTANTS.TENANTID, 7};
        Object[] dataset5 ={"DH-BLR", "ELC" , LMS_PINCODE.ML_BLR,ShipmentType.DL,  ShippingMethod.SDD, LMSConstants.IN_HOUSE_COURIER , LMS_CONSTANTS.TENANTID, 99};
        Object[][] dataSet = new Object[][]{dataset1 , dataset2, dataset3, dataset4, dataset5};
        return dataSet;
    }

    @DataProvider(name = "CreateMasterBagWithDifferentHubType")
    public static Object[][] CreateMasterBagWithDifferentHubType(ITestContext testContext){
        Object[] dataset1 ={"DH-BLR", "ELC", ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID , MasterbagType.HUB_TO_DC};
        Object[] dataset2 ={"ELC", "DH-BLR", ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID , MasterbagType.DC_TO_HUB};
        Object[] dataset3 ={"DH-BLR", "DH-DEL", ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID , MasterbagType.HUB_TO_HUB};
        Object[] dataset4 ={"DEL-DC", "DELHIS", ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID , MasterbagType.DC_TO_DC};
        Object[] dataset5 ={"DH-DEL", "DE", ShippingMethod.NORMAL, "DE", LMS_CONSTANTS.TENANTID , MasterbagType.HUB_TO_3PL};
        Object[] dataset6 ={"DH-BLR", "RHD", ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID , MasterbagType.HUB_TO_RHC};
        Object[] dataset7 ={"RHD", "DE", ShippingMethod.NORMAL, "DE", LMS_CONSTANTS.TENANTID , MasterbagType.RHC_TO_3PL};
        Object[][] dataSet = new Object[][]{dataset1 , dataset2 , dataset3, dataset4, dataset5, dataset6, dataset7};
        return dataSet;
    }

    @DataProvider(name = "EditMasterBagConfigs")
    public static Object[][] EditMasterBagConfigs(ITestContext testContext){
        Object[] dataset1 ={"DH-BLR", "ELC", ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID , MasterbagType.HUB_TO_DC};
        Object[] dataset2 ={"ELC", "DH-BLR", ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID , MasterbagType.DC_TO_HUB};
        Object[] dataset3 ={"DH-BLR", "DH-DEL", ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID , MasterbagType.HUB_TO_HUB};
        Object[] dataset4 ={"DEL-DC", "DELHIS", ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID , MasterbagType.DC_TO_DC};
        Object[] dataset5 ={"DH-DEL", "DE", ShippingMethod.NORMAL, "DE", LMS_CONSTANTS.TENANTID , MasterbagType.HUB_TO_3PL};
        Object[] dataset6 ={"DH-BLR", "RHD", ShippingMethod.NORMAL, LMSConstants.IN_HOUSE_COURIER, LMS_CONSTANTS.TENANTID , MasterbagType.HUB_TO_RHC};
        Object[] dataset7 ={"RHD", "DE", ShippingMethod.NORMAL, "DE", LMS_CONSTANTS.TENANTID , MasterbagType.RHC_TO_3PL};
        Object[][] dataSet = new Object[][]{dataset1 , dataset2 , dataset3, dataset4, dataset5, dataset6, dataset7};
        return dataSet;
    }


}
