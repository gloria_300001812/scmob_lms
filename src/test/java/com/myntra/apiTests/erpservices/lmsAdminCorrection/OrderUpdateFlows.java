package com.myntra.apiTests.erpservices.lmsAdminCorrection;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lastmile.constants.ResponseMessageConstants;
import com.myntra.apiTests.erpservices.lms.Constants.FileUploadConstants;
import com.myntra.apiTests.erpservices.lms.Helper.HubToTransportHubConfigResponse;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.apiTests.erpservices.lmsUploader.LMSUploadHelper;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.lms.client.response.TransporterResponse;
import com.myntra.lms.client.status.*;
import com.myntra.logistics.masterbag.entry.MasterbagDomain;
import com.myntra.logistics.masterbag.response.MasterbagUpdateResponse;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.tms.container.ContainerEntry;
import com.myntra.tms.container.ContainerResponse;
import com.myntra.tms.lane.LaneResponse;
import com.myntra.tms.masterbag.TMSMasterbagReponse;
import org.testng.Assert;
import org.testng.annotations.Test;


import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrderUpdateFlows implements StatusPoller {

    /**
     * Input: csv file`src/test/resources/scm/lms/rules/BulkOrderUploadRules.csv`, if you want to add new combination of uploads
     * go to the above mentioned csv file and add it as per the headers
     * @throws Exception
     */
    @Test(enabled = true, priority = 1, description = "TCID [C22043] Test Bulk order upload through csv file upload")
    public void testOrderUploadThroughCSV() throws Exception {
        LMSUploadHelper lmsUploadHelper = new LMSUploadHelper();
        Integer noOfOrders = 2;
        String location = "ELC";
        List<Map<String, String>> fileRules = csvReader.readCsvAsListOfMap(FileUploadConstants.bulkOrderUpload);

        for (Map rule : fileRules) {
            ArrayList<String> trackingNumbers = new ArrayList<>();
            ArrayList<String> toStates = new ArrayList<>();
            ArrayList<String> orderIds = new ArrayList<>();
            int j = 0;
            while (j++ != noOfOrders) {
                if(rule.get("FromState").toString().equalsIgnoreCase("RTO")){
                    String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(EnumSCM.LOST, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true));
                    String trackingNumber = lmsHelper.getTrackingNumber(packetId);
                    lmsServiceHelper.forceUpdateForForward(packetId,EnumSCM.RTO,EnumSCM.RTO_FOUND_ORDER,LASTMILE_CONSTANTS.TENANT_ID,LASTMILE_CONSTANTS.CLIENT_ID);
                    statusPollingValidator.validateOrderStatus(packetId, rule.get("OrderToShipStatusBeforeUpload").toString());
                    orderIds.add(packetId);
                    trackingNumbers.add(trackingNumber);
                    toStates.add(rule.get("ToState").toString());
                }else{
                    String packetId = omsServiceHelper.getPacketId(lmsHelper.createMockOrder(rule.get("FromState").toString(), LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true));
                    String trackingNumber = lmsHelper.getTrackingNumber(packetId);
                    statusPollingValidator.validateOrderStatus(packetId, rule.get("OrderToShipStatusBeforeUpload").toString());
                    orderIds.add(packetId);
                    trackingNumbers.add(trackingNumber);
                    toStates.add(rule.get("ToState").toString());
                }
            }

            String file_Path = orderUpdateTabHelper.generateOrderUpdateCSV(Long.parseLong(LASTMILE_CONSTANTS.TENANT_ID), trackingNumbers, toStates, location, toStates);
            lmsUploadHelper.configurationUpload(BulkJobType.BULK_ORDER_STATUS_UPDATE, file_Path);

            for (int i = 0; i <= trackingNumbers.size() - 1; i++) {
                System.out.println(MessageFormat.format("***** From state - {0} and toState - {1} upload inProgress for tracking number {2} and order_id {3}",
                        rule.get("FromState").toString(), rule.get("ToState").toString(), trackingNumbers.get(i), orderIds.get(i)));
                statusPollingValidator.validateML_ShipmentStatus(trackingNumbers.get(i), LASTMILE_CONSTANTS.TENANT_ID, rule.get("MLShipmentStatus").toString());
                statusPollingValidator.validateOrderStatus(orderIds.get(i), rule.get("OrderToShipStatus").toString());
                statusPollingValidator.validate_PacketStatusOMS(orderIds.get(i), LASTMILE_CONSTANTS.CLIENT_ID, LASTMILE_CONSTANTS.CLIENT_ID, rule.get("OMSStatus").toString());
            }
        }
    }

    @Test(enabled = true, priority = 2, description = "C22040 : SCMLMS-2621 -> Flow : DL -> Lost -> RTO Ship it in reverse bag DC TO WH")
    public void process_OrderStatus_DL_LOST_RTO_Successful() throws Exception {

        String orderID = lmsHelper.createMockOrder(EnumSCM.DL, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        //validate OrderToShip status
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        String hubCode = orderResponse.getOrders().get(0).getDispatchHubCode();
        Assert.assertTrue(orderResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.retrievedOrder), "ORDER(s) wasn't retrieved successfully");
        storeValidator.validateOrderStatus(orderResponse, ShipmentStatus.DELIVERED);
        String deliveryCenterCode = orderResponse.getOrders().get(0).getDeliveryCenterCode();
        String rtoHub = orderResponse.getOrders().get(0).getRtoHubCode();
        String courierCode = orderResponse.getOrders().get(0).getCourierOperator();
        Long deliveryCenterId = orderResponse.getOrders().get(0).getDeliveryCenterId();
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.DELIVERED.toString());

        //mark order as LOST
        lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.LOST, EnumSCM.LOST_IN_TRANSIT, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.LOST);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, ShipmentStatus.LOST.toString());
        //mark order as RTO
        lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.RTO_CONFIRMED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, ShipmentStatus.RTO_CONFIRMED.toString());

        //create Master bag
        MasterbagDomain createMasterBagResponse = masterBagServiceHelper.createMasterBag(deliveryCenterCode, rtoHub, ShippingMethod.NORMAL, courierCode, LASTMILE_CONSTANTS.TENANT_ID);
        Long reverseMBId = createMasterBagResponse.getId();
        storeValidator.isNull(String.valueOf(reverseMBId));
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId, com.myntra.lms.client.status.ShipmentStatus.NEW.toString());
        //Add order to MB
        masterBagServiceHelper.addShipmentsToMasterBag(reverseMBId, trackingNo, ShipmentType.DL, LASTMILE_CONSTANTS.TENANT_ID);
        //close Reverse MB
        MasterbagUpdateResponse masterbagUpdateResponse=masterBagServiceHelper.closeMasterBag(reverseMBId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertEquals(masterbagUpdateResponse.getStatus().getStatusType().toString(),EnumSCM.SUCCESS,"Master bag is not closed, reason - "+masterbagUpdateResponse.getStatus().getStatusMessage());
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId, com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());
        //Receive MB in TMS
        TMSMasterbagReponse tmsMasterbagReponse = (TMSMasterbagReponse) tmsServiceHelper.receiveMasterBagInTMS.apply(deliveryCenterCode, reverseMBId, LASTMILE_CONSTANTS.TENANT_ID);
        Assert.assertTrue(tmsMasterbagReponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Master bag is not revceived in TMS. Reason " + tmsMasterbagReponse.getStatus().getStatusMessage());
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseMBId), com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //get mapped Transport hub details
        HubToTransportHubConfigResponse hubToTransportHubConfigResponse = storeHelper.getMappedTransportHubDetails(LASTMILE_CONSTANTS.TENANT_ID, deliveryCenterCode);
        Assert.assertTrue(hubToTransportHubConfigResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success), "Transporter hub for mentioned Dc or Wh is not retrieved successfully");
        String transporterHub = hubToTransportHubConfigResponse.getHubToTransportHubConfigEntries().get(0).getTransportHubCode();
        //get lane configurations
        long laneId = ((LaneResponse) tmsServiceHelper.getSupportedLanes.apply(deliveryCenterCode, transporterHub)).getLaneEntries().get(0).getId();
        //get transport configurations
        long transporterId = ((TransporterResponse) tmsServiceHelper.getSupportedTransportersForLane.apply(laneId)).getTransporterEntries().get(0).getId();

        //Create container
        ContainerResponse containerResponse = ((ContainerResponse) tmsServiceHelper.createContainer.apply(deliveryCenterCode, transporterHub, laneId, transporterId));
        Assert.assertTrue(containerResponse.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.addContainer));
        List<ContainerEntry> lstContainerEntry = containerResponse.getContainerEntries();
        long containerId = 0;
        if (!(lstContainerEntry.size() == 0)) {
            containerId = lstContainerEntry.get(0).getId();
        } else {
            Assert.fail("MB entry is empty");
        }
        storeValidator.isNull(String.valueOf(containerId));

        //Add MB to container
        ContainerResponse containerResponse2 = (ContainerResponse) tmsServiceHelper.addMasterBagToContainer.apply(containerId, reverseMBId);
        Assert.assertEquals(containerResponse2.getStatus().getStatusType().toString(), ResponseMessageConstants.success1, "Master bag is not added to a container");
        //validate MB status
        statusPollingValidator.validateShipmentBagStatus(Long.valueOf(reverseMBId), com.myntra.lms.client.status.ShipmentStatus.CLOSED.toString());

        //Ship container
        ContainerResponse containerResponse1 = (ContainerResponse) tmsServiceHelper.shipContainer.apply(containerId);
        Assert.assertTrue(containerResponse1.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.success1), "Container is not shipped");
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());

        //Receive Container In Transport Hub
        ContainerResponse containerResponse3 = (ContainerResponse) storeHelper.receiveContainerInTransportHub.apply(containerId, transporterHub);
        Assert.assertTrue(containerResponse3.getStatus().getStatusMessage().equalsIgnoreCase(ResponseMessageConstants.receiveMasterBag), "Container is not received in TRansport hub");

        //MasterBag inscan in WH
        String mbInscanResponse = null;
        mbInscanResponse = storeHelper.masterBagInscanProcess(trackingNo, reverseMBId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED,
                com.myntra.lms.client.status.ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);

        if (!(mbInscanResponse.contains(ResponseMessageConstants.success))) {
            //TODO calling this API twice, since it fails 1st time. - need to check
            mbInscanResponse = storeHelper.masterBagInscanProcess(trackingNo, reverseMBId, ShipmentType.DL, OrderShipmentAssociationStatus.RECEIVED,
                    com.myntra.lms.client.status.ShipmentStatus.RECEIVED, PremisesType.HUB, 20l);
            Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success), " Master bag inscan failed in WH");
        }
        Assert.assertTrue(mbInscanResponse.contains(ResponseMessageConstants.success));
        //Validate ML shipment status in DC
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.RTO_DISPATCHED);
        //Validate the MB status
        statusPollingValidator.validateShipmentBagStatus(reverseMBId, com.myntra.lms.client.status.ShipmentStatus.IN_TRANSIT.toString());
    }
}
