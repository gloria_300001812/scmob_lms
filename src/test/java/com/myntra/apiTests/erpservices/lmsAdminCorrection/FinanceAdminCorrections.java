package com.myntra.apiTests.erpservices.lmsAdminCorrection;

import com.myntra.apiTests.common.Constants.EnumSCM;
import com.myntra.apiTests.erpservices.lastmile.constants.LASTMILE_CONSTANTS;
import com.myntra.apiTests.erpservices.lms.validators.StatusPoller;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.logistics.platform.domain.ShipmentStatus;
import org.testng.annotations.Test;

public class FinanceAdminCorrections implements StatusPoller {


    @Test(enabled = true, priority = 1, description = "C22050 : Do Finance admin correction from LOST state to DL state")
    public void testFinanceAdminCorrectionLOST_To_DL() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.LOST, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.LOST);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.LOST);
        //do finance admin correction
        lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.DL, EnumSCM.DELIVERED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, ShipmentStatus.DELIVERED.toString());

    }

    @Test(enabled = true, priority = 2, description = "C22050 : Do Finance admin correction from RTO state to DL state")
    public void testFinanceAdminCorrectionRTO_To_DL() throws Exception {
        String orderID = lmsHelper.createMockOrder(EnumSCM.LOST, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.openBoxPincode, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.courierType, LASTMILE_CONSTANTS.Lastmile_ReturnManagement_constants.warehouseId, EnumSCM.NORMAL, "cod", false, true);
        String packetId = omsServiceHelper.getPacketId(orderID);
        statusPollingValidator.validateOrderStatus(packetId, EnumSCM.LOST);
        OrderResponse orderResponse = lmsClient.getOrderByOrderId(packetId);
        String trackingNo = orderResponse.getOrders().get(0).getTrackingNumber();
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, EnumSCM.LOST);
        //mark order as RTO
        lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.RTO, EnumSCM.RTO_FOUND_ORDER, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.RTO_CONFIRMED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, ShipmentStatus.RTO_CONFIRMED.toString());

        //do finance admin correction
        lmsServiceHelper.forceUpdateForForward(packetId, EnumSCM.DL, EnumSCM.DELIVERED, LASTMILE_CONSTANTS.TENANT_ID, LASTMILE_CONSTANTS.CLIENT_ID);
        statusPollingValidator.validateOrderStatus(packetId, ShipmentStatus.DELIVERED);
        statusPollingValidator.validateML_ShipmentStatus(trackingNo, LASTMILE_CONSTANTS.TENANT_ID, ShipmentStatus.DELIVERED.toString());
    }
}
