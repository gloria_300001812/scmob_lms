#!/bin/bash

sleep 120

echo "${REPO}-${BRANCH}.tgz file from artifactory"

wget -O "${REPO}-${BRANCH}.tgz" "http://artifactory.myntra.com/artifactory/reports/${REPO}-${BRANCH}.tgz"

flag=1

exit_code=$?

if [ $exit_code -eq 0 ]; then

	tar -zxvf "${REPO}-${BRANCH}.tgz" -C /tf
	rm -rf "${REPO}-${BRANCH}.tgz"
	
	cd /tf

	echo "downloading jetpack from artifactory"
	wget -O /usr/bin/jetpack http://artifactory.myntra.com/artifactory/packages/jetpack
	
	if [ $exit_code -eq 0 ]; then
		chmod +x /usr/bin/jetpack
		echo "started - execution of component tests..."
	
		cd /tf
		
		if [ ${IS_JACOCO_ENABLED} == true ]; then
			mvn -Dmaven.repo.local=/tf/.repository -P ${PROFILE} test -Dis.cicd=true -DsuiteXmlFile=${SUITE_XML_FILE} -Denvironment=${ENVIRONMENT} -DisDockinsEnabled=${IS_DOCKINS_ENABLED} -DdockEnvName=${CLUSTER_NAME} -Djacoco.address=${JACOCO_HOST} -Djacoco.port=${JACOCO_PORT} -Dmaven.test.failure.ignore=true
		    /usr/bin/jetpack "jacoco.exec" "${ARTIFACTORY_PATH}/lotr"
		else
			mvn -Dmaven.repo.local=/tf/.repository -P ${PROFILE} test -DsuiteXmlFile=${SUITE_XML_FILE} -Denvironment=${ENVIRONMENT} -DisDockinsEnabled=${IS_DOCKINS_ENABLED} -DdockEnvName=${CLUSTER_NAME}		
		fi
		mvn -P ${PROFILE} allure:report -Dallure.results.directory=$PWD/allure-results
		
		/usr/bin/jetpack "/tf/target/site/allure-maven-plugin" "${ARTIFACTORY_PATH}/lotr/allure-results"	
		redis-cli -h dockinsredis.myntra.com -p 6379 set ${CB_KEY} 'Completed'
	else
		echo "dowloading jetpack failed with error code ${exit_code}"
	fi	
else
	echo "error occurred while downloading repo from artifactory with error code '$exit_code'"
	flag=0
fi

if [ $flag -eq 0 ]; then
	echo "error occurred - exiting with code 1"
	redis-cli -h dockinsredis.myntra.com -p 6379 set ${CB_KEY} 'Failed'
	exit 1
fi

tail -f /dev/null
